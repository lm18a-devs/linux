#
# MStar Platform Configuration
#
menu "MStar Platform Configuration"

choice
	prompt "MStar Chip Selection"
	default None

config MSTAR_CLIPPERS
	bool "CLIPPERS"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_MUJI
	bool "MUJI"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the Muji 64 bit mode kernel.

config MSTAR_MONACO
	bool "MONACO"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_MUNICH
	bool "MUNICH"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_MUSTANG
	bool "MUSTANG"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_MAXIM
	bool "MAXIM"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_M7621
	bool "M7621"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.
	  
config MSTAR_MONET
	bool "MONET"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_MIAMI
	bool "MIAMI"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_MADISON
	bool "MADISON"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_EINSTEIN
	bool "EINSTEIN"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_NAPOLI
	bool "NAPOLI"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_KAISER
	bool "KAISER"
	select ARCH_WANT_OPTIONAL_GPIOLIB
	select ARM_AMBA
	select ARM_TIMER_SP804
	select COMMON_CLKDEV
	select HAVE_CLK
	select ICST
	select PLAT_VERSATILE
	help
	  This enables support for the ARM Ltd Versatile Express boards.

config MSTAR_KRIS
	bool "Kris"
	select BOOT_ELF32
	select ARCH_SPARSEMEM_ENABLE
	select DMA_NONCOHERENT
	select MIPS_BOARDS_GEN
	select CEVT_R4K
	select CSRC_R4K
	select IRQ_CPU
	select SYS_HAS_CPU_MIPS32_R2
	select SYS_HAS_EARLY_PRINTK
	select SYS_SUPPORTS_32BIT_KERNEL
	select SYS_SUPPORTS_BIG_ENDIAN
	select SYS_SUPPORTS_LITTLE_ENDIAN
	select SYS_SUPPORTS_MULTITHREADING
	select HIGHMEM
	select ZONE_DMA
	select MS_USB_MIU1_PATCH
	select SYS_SUPPORTS_HIGHMEM
	select CPU_HAS_SYNC
endchoice

config MSTAR_CHIP_TYPE
	int
	default 1 if MSTAR_TITANIA
	default 2 if MSTAR_TRITON
	default 3 if MSTAR_TITANIA2
	default 4 if MSTAR_OBERON
	default 5 if MSTAR_EUCLID
	default 0

config MSTAR_CHIP_NAME
	string
	default "einstein" if MSTAR_EINSTEIN
	default "napoli" if MSTAR_NAPOLI
	default "madison" if MSTAR_MADISON
	default "miami" if MSTAR_MIAMI
	default "clippers" if MSTAR_CLIPPERS
	default "monaco" if MSTAR_MONACO
	default "munich" if MSTAR_MUNICH
	default "mustang" if MSTAR_MUSTANG
	default "maxim" if MSTAR_MAXIM
	default "m7621" if MSTAR_M7621
	default "kaiser" if MSTAR_KAISER
	default "muji" if MSTAR_MUJI
	default "muji" if MSTAR_MUJI_32
	default "monet" if MSTAR_MONET
	default "monet" if MSTAR_MONET_32
	default "kris" if MSTAR_KRIS
	default "error"

if MSTAR_TITANIA2
choice
	prompt "-- Titania 2 Chip Revision"
	default None

config MSTAR_TITANIA2_REV_NONE
	bool "None"

config MSTAR_TITANIA2_REV_U02
	bool "U02"

config MSTAR_TITANIA2_REV_U03
	bool "U03"
endchoice
endif # MSTAR_TITANIA2

config MSTAR_ARM
	bool "Enable ARM BASE CPU"
	default n

config MSTAR_MIPS
	bool "Enable MIPS BASE CPU"
	default n

config MSTAR_DRIVER
	bool
	default y if MSTAR_ARM
	default y if MSTAR_MIPS
	help
	  mandatory config to enable MStar driver
endmenu # MStar Platform Configuration

source "arch/Kconfig.lgexpand"
source "arch/Kconfig.mstarboard"
source "arch/Kconfig.mstarmmap"
source "arch/Kconfig.mstarkernel"
