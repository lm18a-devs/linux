/*
 * Mstar HWI2C Support
 *
 *  Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __I2C_MSTAR_H__
#define __I2C_MSTAR_H__

#include <linux/platform_device.h>

#ifndef BIT
#define BIT(x) (1UL << (x))
#endif

#define BIT0    BIT(0)
#define BIT1    BIT(1)
#define BIT2    BIT(2)
#define BIT3    BIT(3)
#define BIT4    BIT(4)
#define BIT5    BIT(5)
#define BIT6    BIT(6)
#define BIT7    BIT(7)
#define BIT8    BIT(8)
#define BIT9    BIT(9)
#define BIT10   BIT(10)
#define BIT11   BIT(11)
#define BIT12   BIT(12)
#define BIT13   BIT(13)
#define BIT14   BIT(14)
#define BIT15   BIT(15)

#define HWI2C_SET_RW_BIT(bRead, val) ((bRead) ? ((val << 1) | BIT0) : (val << 1))

typedef enum _HAL_HWI2C_CLKSEL
{
    E_HAL_HWI2C_CLKSEL_VHIGH = 0,
    E_HAL_HWI2C_CLKSEL_HIGH,
    E_HAL_HWI2C_CLKSEL_NORMAL,
    E_HAL_HWI2C_CLKSEL_SLOW,
    E_HAL_HWI2C_CLKSEL_VSLOW,
    E_HAL_HWI2C_CLKSEL_USLOW,
    E_HAL_HWI2C_CLKSEL_UVSLOW,
    E_HAL_HWI2C_CLKSEL_NOSUP
}HAL_HWI2C_CLKSEL;

typedef enum _HAL_HWI2C_STATE
{
	E_HAL_HWI2C_STATE_IDEL = 0,
	E_HAL_HWI2C_STATE_START,
	E_HAL_HWI2C_STATE_WRITE,
	E_HAL_HWI2C_STATE_READ,
	E_HAL_HWI2C_STATE_INT,
	E_HAL_HWI2C_STATE_WAIT,
	E_HAL_HWI2C_STATE_STOP
} HAL_HWI2C_STATE;

typedef enum _HAL_HWI2C_PORT
{
    E_HAL_HWI2C_PORT0_0 = 0, //disable port 0
    E_HAL_HWI2C_PORT0_1,
    E_HAL_HWI2C_PORT0_2,
    E_HAL_HWI2C_PORT0_3,
    E_HAL_HWI2C_PORT0_4,
    E_HAL_HWI2C_PORT0_5,
    E_HAL_HWI2C_PORT0_6,
    E_HAL_HWI2C_PORT0_7,

    E_HAL_HWI2C_PORT1_0 = 8,  //disable port 1
    E_HAL_HWI2C_PORT1_1,
    E_HAL_HWI2C_PORT1_2,
    E_HAL_HWI2C_PORT1_3,
    E_HAL_HWI2C_PORT1_4,
    E_HAL_HWI2C_PORT1_5,
    E_HAL_HWI2C_PORT1_6,
    E_HAL_HWI2C_PORT1_7,

    E_HAL_HWI2C_PORT2_0 = 16,  //disable port 2
    E_HAL_HWI2C_PORT2_1,
    E_HAL_HWI2C_PORT2_2,
    E_HAL_HWI2C_PORT2_3,
    E_HAL_HWI2C_PORT2_4,
    E_HAL_HWI2C_PORT2_5,
    E_HAL_HWI2C_PORT2_6,
    E_HAL_HWI2C_PORT2_7,

    E_HAL_HWI2C_PORT3_0 = 24, //disable port 3
    E_HAL_HWI2C_PORT3_1,
    E_HAL_HWI2C_PORT3_2,
    E_HAL_HWI2C_PORT3_3,
    E_HAL_HWI2C_PORT3_4,
    E_HAL_HWI2C_PORT3_5,
    E_HAL_HWI2C_PORT3_6,
    E_HAL_HWI2C_PORT3_7,

    E_HAL_HWI2C_PORT4_0 = 32, //disable port 4
    E_HAL_HWI2C_PORT4_1,
    E_HAL_HWI2C_PORT4_2,
    E_HAL_HWI2C_PORT4_3,
    E_HAL_HWI2C_PORT4_4,
    E_HAL_HWI2C_PORT4_5,
    E_HAL_HWI2C_PORT4_6,
    E_HAL_HWI2C_PORT4_7,

    E_HAL_HWI2C_PORT5_0 = 40, //disable port 5
    E_HAL_HWI2C_PORT5_1,
    E_HAL_HWI2C_PORT5_2,
    E_HAL_HWI2C_PORT5_3,
    E_HAL_HWI2C_PORT5_4,
    E_HAL_HWI2C_PORT5_5,
    E_HAL_HWI2C_PORT5_6,
    E_HAL_HWI2C_PORT5_7,

    E_HAL_HWI2C_PORT6_0 = 48, //disable port 6
    E_HAL_HWI2C_PORT6_1,
    E_HAL_HWI2C_PORT6_2,
    E_HAL_HWI2C_PORT6_3,
    E_HAL_HWI2C_PORT6_4,
    E_HAL_HWI2C_PORT6_5,
    E_HAL_HWI2C_PORT6_6,
    E_HAL_HWI2C_PORT6_7,

    E_HAL_HWI2C_PORT_NOSUP
}HAL_HWI2C_PORT;

//pad config
#define CHIP_REG_BASE                   (0x101E00)
#define PMSLEEP_REG_BASE                (0x0E00)

//for port 0
#define CHIP_REG_HWI2C_MIIC0            (CHIP_REG_BASE+ (0x6E*2)) //0x1EDC
    #define CHIP_MIIC0_PAD_0            (0)
    #define CHIP_MIIC0_PAD_1            (BIT0) //PAD_GPIO28/PAD_GPIO29
    #define CHIP_MIIC0_PAD_MSK          (BIT0)

//for port 1
#define CHIP_REG_HWI2C_MIIC1            (CHIP_REG_BASE+ (0x6E*2)) //0x1EDC
    #define CHIP_MIIC1_PAD_0            (0)
    #define CHIP_MIIC1_PAD_1            (BIT1)
    #define CHIP_MIIC1_PAD_2            (BIT2)
    #define CHIP_MIIC1_PAD_MSK          (BIT2|BIT1)

//for port 2
#define CHIP_REG_HWI2C_MIIC2            (CHIP_REG_BASE+ (0x6E*2)) //0x1EDC
    #define CHIP_MIIC2_PAD_0            (0)
    #define CHIP_MIIC2_PAD_1            (BIT3)
    #define CHIP_MIIC2_PAD_MSK          (BIT3)

//for port 3-1
#define CHIP_REG_HWI2C_DDCR             (CHIP_REG_BASE+ (0x57*2)) //0x1EAE
    #define CHIP_DDCR_PAD_0             (0)
    #define CHIP_DDCR_PAD_1             (BIT1)
    #define CHIP_DDCR_PAD_MSK           (BIT1|BIT0)
//for port3-2
#define CHIP_REG_HWI2C_MIIC3            (CHIP_REG_BASE+ (0x6F*2)) //0x1EDC
    #define CHIP_MIIC3_PAD_0            (0)
    #define CHIP_MIIC3_PAD_1            (BIT9)
    #define CHIP_MIIC3_PAD_MSK          (BIT10|BIT9)

//for port 4
#define CHIP_REG_HWI2C_MIIC4            (CHIP_REG_BASE+ (0x6F*2)) //0x1EDC
    #define CHIP_MIIC4_PAD_0            (0)
    #define CHIP_MIIC4_PAD_1            (BIT0)  //PAD_GPIO30/PAD_GPIO31
    #define CHIP_MIIC4_PAD_MSK          (BIT0)

//for port 5
#define CHIP_REG_HWI2C_MIIC5            (CHIP_REG_BASE+ (0x6F*2)) //0x1EDC
    #define CHIP_MIIC5_PAD_0            (0)
    #define CHIP_MIIC5_PAD_1            (BIT1) //PAD_GPIO32/PAD_GPIO33
    #define CHIP_MIIC5_PAD_MSK          (BIT1)

//for port 6
#define CHIP_REG_HWI2C_MIIC6            (PMSLEEP_REG_BASE+ (0x64*2))
    #define CHIP_MIIC6_PAD_0            (0)
    #define CHIP_MIIC6_PAD_1            (BIT14)
    #define CHIP_MIIC6_PAD_2            (BIT15)
    #define CHIP_MIIC6_PAD_MSK          (BIT15|BIT14)

//pad mux configuration
#define CHIP_REG_ALLPADIN               (CHIP_REG_BASE+0xA1)
    #define CHIP_ALLPAD_IN              (BIT7)

//Standard mode
#define REG_HWI2C_MIIC_CFG              (0x00)
    #define _MIIC_CFG_RESET             (BIT0)
    #define _MIIC_CFG_EN_DMA            (BIT1)
    #define _MIIC_CFG_EN_INT            (BIT2)
    #define _MIIC_CFG_EN_CLKSTR         (BIT3)
    #define _MIIC_CFG_EN_TMTINT         (BIT4)
    #define _MIIC_CFG_EN_FILTER         (BIT5)
    #define _MIIC_CFG_EN_PUSH1T         (BIT6)
    #define _MIIC_CFG_RESERVED          (BIT7)
#define REG_HWI2C_CMD                   (0x01)
    #define _CMD_START                  (BIT0)
    #define _CMD_STOP                   (BIT0) //BIT8
#define REG_HWI2C_WDATA                 (0x02)
    #define _WDATA_GET_ACKBIT           (BIT8)
#define REG_HWI2C_RDATA                 (0x03)
    #define _RDATA_CFG_TRIG             (BIT8)
    #define _RDATA_CFG_ACKBIT           (BIT9)
#define REG_HWI2C_INT_CTL               (0x04)
    #define _INT_CTL                    (BIT0) //write this register to clear int
#define REG_HWI2C_CUR_STATE             (0x05) //For Debug
    #define _CUR_STATE_MSK              (BIT4|BIT3|BIT2|BIT1|BIT0)
    #define _CUR_WAIT_STATE             (BIT3|BIT2)
#define REG_HWI2C_INT_STATUS            (0x05) //For Debug
    #define _INT_STARTDET               (BIT8)
    #define _INT_STOPDET                (BIT9)
    #define _INT_RXDONE                 (BIT10)
    #define _INT_TXDONE                 (BIT11)
    #define _INT_CLKSTR                 (BIT12)
    #define _INT_SCLERR                 (BIT13)
#define REG_HWI2C_PAD_LEV               (0x06) //Check Pad Level
    #define _GPI_SCL                    (BIT0)
    #define _GPI_SDA                    (BIT1)
#define REG_HWI2C_STP_CNT               (0x08)
#define REG_HWI2C_CKH_CNT               (0x09)
#define REG_HWI2C_CKL_CNT               (0x0A)
#define REG_HWI2C_SDA_CNT               (0x0B)
#define REG_HWI2C_STT_CNT               (0x0C)
#define REG_HWI2C_LTH_CNT               (0x0D)
#define REG_HWI2C_TMT_CNT               (0x0E)
#define REG_HWI2C_SCLI_DELAY            (0x0F)
    #define _SCLI_DELAY                 (BIT2|BIT1|BIT0)
#define REG_HWI2C_ADV_CTL               (0x10)
    #define _ADV_DMA_PASS_INT               	(BIT0)
    //start to byte delay
    #define _ADV_BYTE2BYTE_DELAY        		(BIT3)
    //byte to stop delay
    #define _ADV_START2B_DELAY               	(BIT4)
    //byte to byte delay
    #define _ADV_B2STOP_DELAY              		(BIT5)
    //clock stregch mode and push oen 1T
    #define _ADV_CLK_STRETCH_OEN              	(BIT6)
    #define _ADV_I2C_RSTZ                     	(BIT8)
//byte to byte delay counter
#define REG_HWI2C_START2B_DELAY_COUNTER         (0x11)
//byte to stop delay counter
#define REG_HWI2C_B2STOP_DELAY_COUNTER       	(0x12)
//start to byte delay counter
#define REG_HWI2C_BYTE2BYTE_DELAY_COUNTER   	(0x13)

#define REG_HWI2C_DMA_CMDDAT7                   (0x28)
    #define _DMA_CMDDAT7_MSK                    (BIT15)//SDA delay 2T

struct miic_config_data {
    u8  bus_port;
    HAL_HWI2C_CLKSEL bus_speed;
    const u8 *bus_name;
};

#endif /* __I2C_MSTAR_H__ */
