/*
 * eHCI host controller driver
 *
 * Copyright (C) 2012 MStar Inc.
 *
 *
 * Date: 2015.12.11
 *   1. software patch of VFall state machine mistake is removed, all chips
 *      run with kernel 4.4.3 should be VFall hardware ECO
 */

#ifndef _EHCI_MSTAR_H
#define _EHCI_MSTAR_H

#include <ehci-mstar-40403.h>

#define EHCI_MSTAR_VERSION_DATE "20180118"

#if defined(LGE_USB_RESET_RESUME_PATCH)
	#define CUSTOMIZE_INFO "LGE_ResetResume_"
	#define EHCI_MSTAR_VERSION CUSTOMIZE_INFO EHCI_MSTAR_VERSION_DATE
#else
	#define EHCI_MSTAR_VERSION EHCI_MSTAR_VERSION_DATE
#endif

#endif	/* _EHCI_MSTAR_H */
