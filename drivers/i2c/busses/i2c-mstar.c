////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2007 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (¡§MStar Confidential Information¡¨) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    i2c_mstar.c
/// @brief  IIC Driver Adaptor Layer
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------

#include <linux/module.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/completion.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/io.h>
#if IS_ENABLED(CONFIG_OF)
#include <linux/of.h>
#include <linux/of_i2c.h>
#include <linux/of_irq.h>
#include <linux/of_device.h>
#endif
#include <linux/slab.h>
#include <linux/pm_runtime.h>
#include <linux/i2c/i2c-mstar.h>

#define HWI2C_HAL_RETRY_TIMES       3
#define HWI2C_HAL_WAIT_SP_TIMEOUT   120000
#define HWI2C_HAL_WAIT_TIMEOUT      300000
#define HWI2C_DEVICE_TIMEOUT        160 /*ms*/
#define HWI2C_XFER_TIMEOUT          20  /*ms*/

#if IS_ENABLED(CONFIG_OF)
/*static*/ const struct of_device_id mstar_iic_match[] = {
    { .compatible = "Mstar-iic", .data = (void *)0 }, /* hardwave i2c */
    { /* sentinel */ },
};
MODULE_DEVICE_TABLE(of, mstar_iic_match);

static void
mstar_i2c_parse_dt(struct device_node *np, struct mstar_i2c_priv *priv)
{
    struct miic_config_data *pdata = &priv->config;

    if (!pdata)
        return;

    of_property_read_u32(np, "mstar,i2c-bus-speed", &pdata->bus_speed);
    of_property_read_string(np, "mstar,i2c-bus-name", &pdata->bus_name);
}
#endif

#define HW_I2C_NUM  10
static bool g_bLastByte[HW_I2C_NUM] = {0,};

/* timeout waiting for the controller to respond */
#define MSTAR_I2C_TIMEOUT (msecs_to_jiffies(1000))

struct mstar_i2c_priv {
    struct i2c_adapter  adap;
    struct device   *dev;
    struct completion   cmd_complete;
    struct clk  *clk;
    struct mutex    sem;
    struct miic_config_data config;
    int bus_id;
    int irq;
    void __iomem *base;  /* virtual */
    spinlock_t lock;
    wait_queue_head_t wait;
    u16 status;
};

static inline void iic_write_reg8(struct mstar_i2c_priv *priv, int reg, u8 val)
{
    __raw_writeb(val, priv->base + (reg << 2));
}

static inline void iic_write_reg8h(struct mstar_i2c_priv *priv, int reg, u8 val)
{
    __raw_writeb(val, priv->base + (reg << 2) + 1);
}

static inline u8 iic_read_reg8(struct mstar_i2c_priv *priv, int reg)
{
    return __raw_readb(priv->base + (reg << 2));
}

static inline u8 iic_read_reg8h(struct mstar_i2c_priv *priv, int reg)
{
    return __raw_readb(priv->base + (reg << 2) + 1);
}

static inline void iic_write_reg16(struct mstar_i2c_priv *priv, int reg, u16 val)
{
    __raw_writew(val, priv->base + (reg << 2));
}

static inline u16 iic_read_reg16(struct mstar_i2c_priv *priv, int reg)
{
    return __raw_readw(priv->base + (reg << 2));
}

static inline int iic_invalid_address(const struct i2c_msg* p)
{
	return (p->addr > 0x3ff) || (!(p->flags & I2C_M_TEN) && (p->addr > 0x7f));
}


void HAL_HWI2C_ExtraDelay(u32 u32Us)
{
#if 0
    // volatile is necessary to avoid optimization
    u32 volatile u32Dummy = 0;
    //MS_U32 u32Loop;
    u32 volatile u32Loop;

    u32Loop = (u32)(50 * u32Us);
    while (u32Loop--)
    {
        u32Dummy++;
    }
#else
    udelay(u32Us);
#endif
}


bool HAL_HWI2C_SelectPort(struct mstar_i2c_priv *priv)
{
    u16 reg = 0;
    u16 value = 0;
    HAL_HWI2C_PORT ePort = (HAL_HWI2C_PORT)priv->config.bus_port;

    dev_dbg(&priv->adap.dev,
        "<%s> bus%d_PadInfo : %d\n", __func__, priv->bus_id, (int)ePort);

    //decide port mask
    if((ePort>=E_HAL_HWI2C_PORT0_0)&&(ePort<=E_HAL_HWI2C_PORT0_4))//port 0
    {
        switch(ePort)
        {
            case E_HAL_HWI2C_PORT0_0: //miic0 disable
                value = CHIP_MIIC0_PAD_0;
                break;
            case E_HAL_HWI2C_PORT0_1: ////miic0 using PAD_GPIO28/PAD_GPIO29
                value = CHIP_MIIC0_PAD_1;
                break;
            default:
                return false;
        }
        reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC0 << 1)) & ~CHIP_MIIC0_PAD_MSK;
        reg |= value;
        reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC0 << 1)));
    }
    else if((ePort>=E_HAL_HWI2C_PORT1_0)&&(ePort<=E_HAL_HWI2C_PORT1_3))//port 1
    {
        switch(ePort)
        {
            case E_HAL_HWI2C_PORT1_0: //miic1 disable
                value = CHIP_MIIC1_PAD_0;
                break;
            case E_HAL_HWI2C_PORT1_1: //miic1 using PAD_TGPIO2/PAD_TGPIO3
                value = CHIP_MIIC1_PAD_1;
                break;
            default:
                return false;
        }
        reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC1 << 1)) & ~CHIP_MIIC1_PAD_MSK;
        reg |= value;
        reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC1 << 1)));
    }
    else if((ePort>=E_HAL_HWI2C_PORT2_0)&&(ePort<=E_HAL_HWI2C_PORT2_4))//port 2
    {
        //Disable first
        reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC2 << 1)) & ~CHIP_MIIC2_PAD_MSK;
        reg |= CHIP_MIIC2_PAD_0;
        reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC2 << 1)));

        reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC3 << 1)) & ~CHIP_MIIC3_PAD_MSK;
        reg |= CHIP_MIIC3_PAD_0;
        reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC3 << 1)));

        switch(ePort)
        {
            case E_HAL_HWI2C_PORT2_0: //miic2 disable
                break;
            case E_HAL_HWI2C_PORT2_1: //miic2 using PAD_I2S_IN_BCK/PAD_I2S_IN_SD
                reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC2 << 1)) & ~CHIP_MIIC2_PAD_MSK;
                reg |= CHIP_MIIC2_PAD_1;
                reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC2 << 1)));
                break;
            case E_HAL_HWI2C_PORT2_2: //miic3 using PAD_GPIO36/PAD_GPIO37
                reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC3 << 1)) & ~CHIP_MIIC3_PAD_MSK;
                reg |= CHIP_MIIC3_PAD_1;
                reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC3 << 1)));
		        break;
            default:
                return false;
        }
    }
    else if((ePort>=E_HAL_HWI2C_PORT3_0)&&(ePort<=E_HAL_HWI2C_PORT3_1))//port 3
    {
        reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_DDCR << 1)) & ~CHIP_DDCR_PAD_MSK;
        reg |= CHIP_DDCR_PAD_0;
        reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_DDCR << 1)));
        switch(ePort)
        {
            case E_HAL_HWI2C_PORT3_0: //miic3 disable
                reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_DDCR << 1)) & ~CHIP_DDCR_PAD_MSK;
                reg |= CHIP_DDCR_PAD_0;
                reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_DDCR << 1)));
                break;
            case E_HAL_HWI2C_PORT3_1: //ddcr_miic using PAD_DDCR_CK/PAD_DDCR_DA
                reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_DDCR << 1)) & ~CHIP_DDCR_PAD_MSK;
                reg |= CHIP_DDCR_PAD_1;
                reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_DDCR << 1)));
                break;
            default:
                return false;
        }
    }
    else if((ePort>=E_HAL_HWI2C_PORT4_0)&&(ePort<=E_HAL_HWI2C_PORT4_1))//port 4
    {
        switch(ePort)
        {
            case E_HAL_HWI2C_PORT4_0: //miic4 disable
                value = CHIP_MIIC4_PAD_0;
                break;
            case E_HAL_HWI2C_PORT4_1: //miic2 using PAD_GPIO30/PAD_GPIO31
                value = CHIP_MIIC4_PAD_1;
                break;
            default:
                return false;
        }
        reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC4 << 1)) & ~CHIP_MIIC4_PAD_MSK;
        reg |= value;
        reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC4 << 1)));
    }
    else if((ePort>=E_HAL_HWI2C_PORT5_0)&&(ePort<=E_HAL_HWI2C_PORT5_1))//port 5
    {
        switch(ePort)
        {
            case E_HAL_HWI2C_PORT5_0: //miic5 disable
                value = CHIP_MIIC5_PAD_0;
                break;
            case E_HAL_HWI2C_PORT5_1: //miic5 using PAD_GPIO32/PAD_GPIO33
                value = CHIP_MIIC5_PAD_1;
                break;
            default:
                return false;
        }
        //HAL_HWI2C_WriteByteMask(CHIP_REG_HWI2C_MIIC5, u8Value, CHIP_MIIC5_PAD_MSK);
        reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC5 << 1)) & ~CHIP_MIIC5_PAD_MSK;
        reg |= value;
        reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC5 << 1)));
    }
	else if((ePort>=E_HAL_HWI2C_PORT6_0)&&(ePort<=E_HAL_HWI2C_PORT6_2))//port 6
    {
        switch(ePort)
        {
            case E_HAL_HWI2C_PORT6_0: //miic6 disable
                value = CHIP_MIIC6_PAD_0;
                break;
            case E_HAL_HWI2C_PORT6_1: //miic6 using PAD_GPIO2_PM/PAD_GPIO9_PM
                value = CHIP_MIIC6_PAD_1;
                break;
            case E_HAL_HWI2C_PORT6_2: //miic6 using PAD_VID0/PAD_VID1
                value = CHIP_MIIC6_PAD_2;
                break;
            default:
                return false;
        }
        reg = reg_readw(0x1f000000 + (CHIP_REG_HWI2C_MIIC6 << 1)) & ~CHIP_MIIC6_PAD_MSK;
        reg |= value;
        reg_writew(reg,(0x1f000000 + (CHIP_REG_HWI2C_MIIC6 << 1)));
    }
    else
    {
        return false;
    }
    return true;
}

static void mstar_i2c_init(struct mstar_i2c_priv *priv)
{
    iic_write_reg8(priv, REG_HWI2C_MIIC_CFG, _MIIC_CFG_RESET);
    iic_write_reg8(priv, REG_HWI2C_INT_CTL, _INT_CTL);
    iic_write_reg8(priv, REG_HWI2C_MIIC_CFG, _MIIC_CFG_EN_FILTER|_MIIC_CFG_EN_PUSH1T|_MIIC_CFG_EN_CLKSTR);
    iic_write_reg8(priv, REG_HWI2C_ADV_CTL, _ADV_BYTE2BYTE_DELAY);
}

#if 0
/*
 * Waiting on Bus Busy
 */
static int mstar_i2c_wait_for_bb(struct mstar_i2c_priv *priv)
{
    unsigned long timeout;

    timeout = jiffies + MSTAR_I2C_TIMEOUT;
    while ( iic_read_reg8(priv, REG_HWI2C_CUR_STATE) & _CUR_STATE_MSK ) {
        if (time_after(jiffies, timeout)) {
            dev_warn(&priv->adap.dev, "timeout waiting for bus ready, state:%d\n",
                (int)(iic_read_reg8(priv, REG_HWI2C_CUR_STATE) & _CUR_STATE_MSK));
            iic_write_reg8(priv, REG_HWI2C_INT_CTL, _INT_CTL);
            return -ETIMEDOUT;
        }
        msleep(1);
    }
    iic_write_reg8(priv, REG_HWI2C_INT_CTL, _INT_CTL);
    return 0;
}
#endif

static void HWI2C_SetClk(struct mstar_i2c_priv *priv, HAL_HWI2C_CLKSEL eClkSel)
{
    u16 u16ClkHCnt=0,u16ClkLCnt=0;
    u16 u16StpCnt=0,u16SdaCnt=0,u16SttCnt=0,u16LchCnt=0;

    if(eClkSel>=E_HAL_HWI2C_CLKSEL_NOSUP){
    dev_dbg(&priv->adap.dev,
                "<%s> CLK=0x%X not support!!!\n", __func__, eClkSel);
        return;
    }

    //if(priv->bus_id == 6)
    //    eClkSel = E_HAL_HWI2C_CLKSEL_HIGH;

    switch(eClkSel)//use Xtal = 12M Hz
    {
        case E_HAL_HWI2C_CLKSEL_VHIGH: // 800 KHz
            u16ClkHCnt = 5; u16ClkLCnt = 6; break;
        case E_HAL_HWI2C_CLKSEL_HIGH: // 400 KHz
            u16ClkHCnt = 12; u16ClkLCnt = 14; break;
        case E_HAL_HWI2C_CLKSEL_NORMAL: //300 KHz
            u16ClkHCnt =  17; u16ClkLCnt =   19; break;
        case E_HAL_HWI2C_CLKSEL_SLOW: //200 KHz
            u16ClkHCnt =  25; u16ClkLCnt =   27; break;
        case E_HAL_HWI2C_CLKSEL_VSLOW: //100 KHz
            u16ClkHCnt =  57; u16ClkLCnt =   59; break;
        case E_HAL_HWI2C_CLKSEL_USLOW: //50 KHz
            u16ClkHCnt =  117; u16ClkLCnt = 119; break;
        case E_HAL_HWI2C_CLKSEL_UVSLOW: //25 KHz
            u16ClkHCnt =  237; u16ClkLCnt = 239; break;
        default:
            u16ClkHCnt =  57; u16ClkLCnt =  59; break;
    }

    if( eClkSel >= E_HAL_HWI2C_CLKSEL_VSLOW )
    {
        u16SttCnt = 48; u16StpCnt = 48; u16SdaCnt = 5; u16LchCnt = 5;
    }
    else
    {
        u16SttCnt=8; u16StpCnt=8; u16SdaCnt=5; u16LchCnt=5;
    }

    iic_write_reg16(priv, REG_HWI2C_CKH_CNT, u16ClkHCnt);
    iic_write_reg16(priv, REG_HWI2C_CKL_CNT, u16ClkLCnt);
    iic_write_reg16(priv, REG_HWI2C_STP_CNT, u16StpCnt);
    iic_write_reg16(priv, REG_HWI2C_SDA_CNT, u16SdaCnt);
    iic_write_reg16(priv, REG_HWI2C_STT_CNT, u16SttCnt);
    iic_write_reg16(priv, REG_HWI2C_LTH_CNT, u16LchCnt);
}

void HAL_HWI2C_SetClk(struct mstar_i2c_priv *priv, struct i2c_msg *msgs)
{
    u16 clkinfo = (msgs->flags & (BIT1|BIT2|BIT3)) >> 1;
    HAL_HWI2C_CLKSEL clk = E_HAL_HWI2C_CLKSEL_NOSUP;

    switch(clkinfo)//use Xtal = 12M Hz
    {
        case 1:
            clk = E_HAL_HWI2C_CLKSEL_USLOW; break;
        case 2:
            clk = E_HAL_HWI2C_CLKSEL_VSLOW; break;
        case 3:
            clk = E_HAL_HWI2C_CLKSEL_HIGH; break;
        case 4:
            clk = E_HAL_HWI2C_CLKSEL_VHIGH; break;
        default:
            clk = E_HAL_HWI2C_CLKSEL_VSLOW; break;
    }
    HWI2C_SetClk(priv, clk);
}

u8 HAL_HWI2C_GetState(struct mstar_i2c_priv *priv)
{
	u8 cur_state = iic_read_reg8(priv, REG_HWI2C_CUR_STATE) & _CUR_STATE_MSK;

	if (cur_state <= 0) // 0: idle
		return E_HAL_HWI2C_STATE_IDEL;
	else if (cur_state <= 2) // 1~2:start
		return E_HAL_HWI2C_STATE_START;
	else if (cur_state <= 6) // 3~6:write
		return E_HAL_HWI2C_STATE_WRITE;
	else if (cur_state <= 10) // 7~10:read
		return E_HAL_HWI2C_STATE_READ;
	else if (cur_state <= 11) // 11:interrupt
		return E_HAL_HWI2C_STATE_INT;
	else if (cur_state <= 12) // 12:wait
		return E_HAL_HWI2C_STATE_WAIT;
	else  // 13~15:stop
		return E_HAL_HWI2C_STATE_STOP;
}

bool HAL_HWI2C_Is_Idle(struct mstar_i2c_priv *priv)
{
    return ((HAL_HWI2C_GetState(priv)==E_HAL_HWI2C_STATE_IDEL) ? true : false);
}

bool HAL_HWI2C_Is_INT(struct mstar_i2c_priv *priv)
{
    return ((iic_read_reg8(priv, REG_HWI2C_INT_CTL) & _INT_CTL) ? true : false);
}

void HAL_HWI2C_Clear_INT(struct mstar_i2c_priv *priv)
{
    iic_write_reg8(priv, REG_HWI2C_INT_CTL, _INT_CTL);
}

void HAL_HWI2C_Reset(struct mstar_i2c_priv *priv)
{
    iic_write_reg8(priv, REG_HWI2C_MIIC_CFG, iic_read_reg8(priv, REG_HWI2C_MIIC_CFG) & ~_MIIC_CFG_RESET);
    iic_write_reg8(priv, REG_HWI2C_MIIC_CFG, iic_read_reg8(priv, REG_HWI2C_MIIC_CFG) | _MIIC_CFG_RESET);
    iic_write_reg8(priv, REG_HWI2C_MIIC_CFG, iic_read_reg8(priv, REG_HWI2C_MIIC_CFG) & ~_MIIC_CFG_RESET);
    msleep_interruptible(1);
}

bool HAL_HWI2C_Start(struct mstar_i2c_priv *priv)
{
    u32 u32Count = HWI2C_HAL_WAIT_SP_TIMEOUT;

    //let SDA delay 2T
    iic_write_reg16(priv, REG_HWI2C_DMA_CMDDAT7, iic_read_reg16(priv, REG_HWI2C_DMA_CMDDAT7) | _DMA_CMDDAT7_MSK);

    //reset I2C
    iic_write_reg8(priv, REG_HWI2C_CMD, _CMD_START);
    while((!HAL_HWI2C_Is_INT(priv))&&(u32Count > 0))
        u32Count--;
    HAL_HWI2C_Clear_INT(priv);

    udelay(5);

    return (u32Count) ? 0 : -ETIMEDOUT;
}

bool HAL_HWI2C_Stop(struct mstar_i2c_priv *priv)
{
    u32 u32Count = HWI2C_HAL_WAIT_SP_TIMEOUT;

    iic_write_reg8h(priv, REG_HWI2C_CMD, _CMD_STOP);
    while((!HAL_HWI2C_Is_Idle(priv))&&(!HAL_HWI2C_Is_INT(priv))&&(u32Count > 0))
        u32Count--;
    HAL_HWI2C_Clear_INT(priv);

    udelay(10);

    return (u32Count) ? 0 : -ETIMEDOUT;
}

void HAL_HWI2C_ReadRdy(struct mstar_i2c_priv *priv)
{
    u16 u16Value = 0;
    u16Value = (g_bLastByte[priv->bus_id]) ? (_RDATA_CFG_TRIG|_RDATA_CFG_ACKBIT) : (_RDATA_CFG_TRIG);
    g_bLastByte[priv->bus_id] = false;
    iic_write_reg16(priv,REG_HWI2C_RDATA, u16Value);
}

bool HAL_HWI2C_SendData(struct mstar_i2c_priv *priv, u8 u8Data)
{
    iic_write_reg8(priv, REG_HWI2C_WDATA, u8Data);
    return true;
}

u8 HAL_HWI2C_RecvData(struct mstar_i2c_priv *priv)
{
    return iic_read_reg8(priv, REG_HWI2C_RDATA);
}

bool HAL_HWI2C_Get_SendAck(struct mstar_i2c_priv *priv)
{
    return (iic_read_reg16(priv, REG_HWI2C_WDATA) & _WDATA_GET_ACKBIT) ? false : true;
}

void HAL_HWI2C_NoAck(struct mstar_i2c_priv *priv)
{
    g_bLastByte[priv->bus_id] = true;
}

void HAL_HWI2C_Ack(struct mstar_i2c_priv *priv)
{
    g_bLastByte[priv->bus_id] = false;
}

bool HAL_HWI2C_SCL_Level(struct mstar_i2c_priv *priv)
{
    return (iic_read_reg8(priv, REG_HWI2C_PAD_LEV) & _GPI_SCL) ? true : false;
}

bool HAL_HWI2C_Send_Byte(struct mstar_i2c_priv *priv, u8 u8Data)
{
    u8 u8Retry = HWI2C_HAL_RETRY_TIMES;
    u32 u32Count = HWI2C_HAL_WAIT_TIMEOUT;

    while(u8Retry--)
    {
        HAL_HWI2C_Clear_INT(priv);
        if (HAL_HWI2C_SendData(priv,u8Data))
        {
            u32Count = HWI2C_HAL_WAIT_TIMEOUT;
            while(u32Count--)
            {
                if (HAL_HWI2C_Is_INT(priv))
                {
                    HAL_HWI2C_Clear_INT(priv);
                    if (HAL_HWI2C_Get_SendAck(priv))
                    {
                        #if 1
                        HAL_HWI2C_ExtraDelay(1);
                        #else
                        MsOS_DelayTaskUs(1);
                        #endif
                        return 0;
                    }
                    break;
                }
            }
        }
    }
    dev_dbg(&priv->adap.dev,
                "<%s> Send byte 0x%X fail!\n", __func__, u8Data);
    return -ETIMEDOUT;
}

bool HAL_HWI2C_Recv_Byte(struct mstar_i2c_priv *priv, u8 *pData)
{
    u32 u32Count = HWI2C_HAL_WAIT_TIMEOUT;
    unsigned long timeout = 0;

    if (!pData)
        return -EINVAL;

    HAL_HWI2C_ReadRdy(priv);
    while((!HAL_HWI2C_Is_INT(priv))&&(u32Count > 0))
        u32Count--;

    /*  To handle the abnormal case, timeout happened.
    *   We found abmicom will hold the scl to low for 140-160 ms.
    *   Keep re-trying to wait for byte-read done.
    */
    if(u32Count == 0){
        timeout = jiffies + msecs_to_jiffies(HWI2C_DEVICE_TIMEOUT);
        do{
            msleep(4);
            if(HAL_HWI2C_Is_INT(priv))
                break;
            if (unlikely(time_after(jiffies, timeout)))
                goto iic_dev_timeout;
        }while(1);
    }

    HAL_HWI2C_Clear_INT(priv);

    //if (u16Count)
    {
        //get data before clear int and stop
        *pData = HAL_HWI2C_RecvData(priv);
        //clear interrupt
        HAL_HWI2C_Clear_INT(priv);
        HAL_HWI2C_ExtraDelay(1);
        return 0;
    }

iic_dev_timeout:
    HAL_HWI2C_Clear_INT(priv);
    dev_info(&priv->adap.dev,
                "<%s> timeout happened.\n", __func__);
    return -ETIMEDOUT;
}

static int mstar_i2c_write(struct mstar_i2c_priv *priv, struct i2c_msg *msgs)
{
    int i;

    dev_dbg(&priv->adap.dev, "<%s> addr=0x%04x, len: %d, flags: 0x%x\n",
            __func__, msgs->addr << 1, msgs->len, msgs->flags);
    if (unlikely(msgs->len == 0))
        return -EINVAL;

    if (unlikely(HAL_HWI2C_Send_Byte(priv,HWI2C_SET_RW_BIT(0, msgs->addr))))
        return -EIO;

    /* write data */
    for (i = 0; i < msgs->len; i++) {
        dev_dbg(&priv->adap.dev,
                "<%s> write byte: B%d=0x%X\n",
                __func__, i, msgs->buf[i]);
        if (HAL_HWI2C_Send_Byte(priv, msgs->buf[i]))
            return -EIO;
    }

    return 0;
}

static int mstar_i2c_read(struct mstar_i2c_priv *priv, struct i2c_msg *msgs)
{
    u16 uSize;
    u8 *pData = msgs->buf;

    dev_dbg(&priv->adap.dev,
            "<%s> addr=0x%04x, len: %d, flags: 0x%x\n",
            __func__, (msgs->addr << 1) | 0x01, msgs->len, msgs->flags);
    if (unlikely(msgs->len == 0))
        return -EINVAL;

    if (unlikely(HAL_HWI2C_Send_Byte(priv,HWI2C_SET_RW_BIT(1, msgs->addr))))
        return -EIO;

    uSize = msgs->len;
    g_bLastByte[priv->bus_id] = false;
    while(uSize)
    {
        uSize--;
        if (uSize==0)
            HAL_HWI2C_NoAck(priv);
        if (unlikely(HAL_HWI2C_Recv_Byte(priv, pData)))
        {
            dev_info(&priv->adap.dev,
            "<%s> addr=0x%04x, len: %d, flags: 0x%x\n",
            __func__, (msgs->addr << 1) | 0x01, msgs->len, msgs->flags);
            return -EIO;
        }
        pData++;
    }

    return 0;
}

static int
i2c_mstar_xfer(struct i2c_adapter *adap, struct i2c_msg msgs[], int num)
{
    struct mstar_i2c_priv *priv = (struct mstar_i2c_priv *)i2c_get_adapdata(adap);
    int i, ret = 0;
    unsigned long timeout = 0;

    dev_dbg(&priv->adap.dev, "%s:(%d), %d msg(s)\n", __func__, priv->adap.nr, num);

    if (!num)
        return -EINVAL;
    if (unlikely(iic_invalid_address(&msgs[0]))){
        dev_dbg(&priv->adap.dev, "%s:(%d): invalid address 0x%03x (%d-bit)\n", __func__, priv->adap.nr,
            msgs[0].addr, msgs[0].flags & I2C_M_TEN ? 10 : 7);
        return -EINVAL;
    }

    mutex_lock(&priv->sem);

    HAL_HWI2C_SetClk(priv, &msgs[0]);

    timeout = jiffies + msecs_to_jiffies(HWI2C_XFER_TIMEOUT);
#if 0
    /* Check bus state */
    ret = mstar_i2c_wait_for_bb(priv);
    if (ret)
        goto fail0;
#endif

    /* Check SCL, it should be high before i2c operatation */
    if(unlikely(HAL_HWI2C_SCL_Level(priv) == 0)){
        // clear hwi2c status to release SCL.
        HAL_HWI2C_Reset(priv);
        // if fixed, do i2c-op or jump the end.
        if(HAL_HWI2C_SCL_Level(priv) == 0){
            dev_dbg(&priv->adap.dev,
                        "<%s> bus error: %x\n", __func__, (u8)HAL_HWI2C_GetState(priv));
            ret = -EBUSY;
            goto fail_busbusy;
        }
    }
    ret = HAL_HWI2C_Start(priv);
    if (ret)
        goto fail0;

    /* read/write data */
    for (i = 0; i < num; i++) {
        if ((i) && (!(msgs[i].flags & I2C_M_NOSTART))) {

            HAL_HWI2C_ExtraDelay(20);

            dev_dbg(&priv->adap.dev,
                "<%s> repeated start\n", __func__);
            ret = HAL_HWI2C_Start(priv);
            if (ret)
                goto fail0;
        }
        dev_dbg(&priv->adap.dev,
            "<%s> transfer message: %d\n", __func__, i);

        /* write/read data */
        if (msgs[i].flags & I2C_M_RD)
            ret = mstar_i2c_read(priv, &msgs[i]);
        else
            ret = mstar_i2c_write(priv, &msgs[i]);
        if (ret)
            goto fail0;
    }

fail0:
    /* Stop I2C transfer */
    HAL_HWI2C_Stop(priv);
fail_busbusy:

    if (unlikely(time_after(jiffies, timeout)))
    {
        dev_info(&priv->adap.dev,
        "[warn]<%s> takes %d ms -> addr=0x%04x, len: %d, flags: 0x%x\n",
        __func__, (jiffies_to_msecs((jiffies - timeout))+HWI2C_XFER_TIMEOUT), (msgs->addr << 1) | 0x01, msgs->len, msgs->flags);
    }
    mutex_unlock(&priv->sem);

    if(ret < 0)
    {
        dev_info(&priv->adap.dev,
        "[err]<%s> fail with return value: %d -> addr=0x%04x, len: %d, flags: 0x%x\n",
        __func__, ret, (msgs->addr << 1) | 0x01, msgs->len, msgs->flags);

    }
    return (ret < 0) ? ret : num;
}

#ifdef CONFIG_PM_SLEEP
static int mst_i2c_suspend(struct device *dev)
{
    struct mstar_i2c_priv *priv = dev_get_drvdata(dev);
    HAL_HWI2C_Clear_INT(priv);
    HAL_HWI2C_Reset(priv);
    return 0;
}

static int mst_i2c_resume(struct device *dev)
{
    struct mstar_i2c_priv *priv = dev_get_drvdata(dev);

    /* Set up clock */
    HWI2C_SetClk(priv, priv->config.bus_speed);

    if(HAL_HWI2C_SelectPort(priv)==false){
        dev_err(dev, "[%s] Pad not support!!!\n", __func__);
        return -EINVAL;
    }
    mstar_i2c_init(priv);

    return 0;
}

static struct dev_pm_ops const mst_i2c_pm_ops = {
	SET_LATE_SYSTEM_SLEEP_PM_OPS(mst_i2c_suspend, mst_i2c_resume)
};
#define MSTAR_I2C_PM_OPS (&mst_i2c_pm_ops)
#else
#define MSTAR_I2C_PM_OPS NULL
#endif /* CONFIG_PM_SLEEP */

static u32
i2c_mstar_func(struct i2c_adapter *adap)
{
    return I2C_FUNC_I2C | I2C_FUNC_10BIT_ADDR | I2C_FUNC_NOSTART | (I2C_FUNC_SMBUS_EMUL & ~I2C_FUNC_SMBUS_QUICK);
}

static struct i2c_algorithm i2c_mstar_algo = {
    .master_xfer	= i2c_mstar_xfer,
    .functionality	= i2c_mstar_func,
};

static int
mstar_i2c_probe(struct platform_device *pdev)
{
    struct miic_config_data *pdata = NULL;
    struct mstar_i2c_priv *priv = NULL;
    struct resource *res = NULL;
    struct device *dev = NULL;
    int ret = 0;

	if (pdev != NULL) {
		pdata = pdev->dev.platform_data;
		dev = &pdev->dev;
	} else {
		printk(KERN_ERR "%s: line %d, pdev is NULL\n", __func__, __LINE__);
		return -ENODEV;
	}

    if (!pdev->dev.of_node) {
        pdata = pdev->dev.platform_data;
        if (!pdata) {
            dev_err(&pdev->dev, "no platform data\n");
            return -EINVAL;
        }
    }

    priv = devm_kzalloc(dev, sizeof(struct mstar_i2c_priv), GFP_KERNEL);
    if (!priv) {
        dev_err(dev, "no mem for private data\n");
        return -ENOMEM;
    }

    /* deal with driver instance configuration */
    if (pdata)
        memcpy(&priv->config, pdata, sizeof(*pdata));
#if IS_ENABLED(CONFIG_OF)
    else
        mstar_i2c_parse_dt(pdev->dev.of_node, priv);
#else
    else{
        dev_err(dev, "no pdata\n");
        return -EINVAL;
    }
#endif

#if 0
	priv->clk = devm_clk_get(dev, NULL);
	if (IS_ERR(priv->clk)) {
		dev_err(&pdev->dev, "Could not get clock\n");
		return PTR_ERR(priv->clk);
	}
#endif

    res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    if (!res) {
        dev_err(dev, "no mmio resources\n");
        return -ENODEV;
    }

    priv->base = devm_ioremap_resource(dev, res);
    if (IS_ERR(priv->base))
        return PTR_ERR(priv->base);

#if IS_ENABLED(CONFIG_OF)
    of_property_read_u32(pdev->dev.of_node, "interrupts", &priv->irq);
#else
    priv->irq = platform_get_irq(pdev, 0);
#endif
    if(priv->irq <= 0){
        dev_dbg(&priv->adap.dev, "%s:, %s.%d didn't support irq.\n", __func__, pdev->name, priv->adap.nr);
        //return -ENODEV;
    }

    init_waitqueue_head(&priv->wait);
    mutex_init(&priv->sem);
    spin_lock_init(&priv->lock);

    /* Setup miic driver structure */
    strlcpy(priv->adap.name, pdev->name, sizeof(priv->adap.name));
    priv->bus_id = pdev->id;
    priv->dev = dev;
    priv->adap.owner = THIS_MODULE;
    priv->adap.algo = &i2c_mstar_algo;
    priv->adap.dev.parent = dev;
    priv->adap.nr = pdev->id;
    priv->adap.retries = 3;
    priv->adap.dev.of_node	= pdev->dev.of_node;
    /* Set up adapter data */
    i2c_set_adapdata(&priv->adap, priv);
#if 0
    ret = devm_request_irq(dev, priv->irq, mstar_i2c_irq, 0,
              dev_name(dev), priv);
    if (ret < 0) {
        dev_err(dev, "cannot get irq %d\n", priv->irq);
        return ret;
    }
#endif
    ret = i2c_add_numbered_adapter(&priv->adap);
    if (ret < 0) {
        dev_err(dev, "reg adap failed: %d\n", ret);
        return ret;
    }
    platform_set_drvdata(pdev, priv);
#if IS_ENABLED(CONFIG_OF)
    of_i2c_register_devices(&priv->adap);
#endif
    init_completion(&priv->cmd_complete);
    mstar_i2c_init(priv);

    /* Set up clock */
    HWI2C_SetClk(priv, priv->config.bus_speed);

    /* Set up PadConfig */
    if(HAL_HWI2C_SelectPort(priv)==false){
        dev_err(dev, "[%s] Pad not support!!!\n", __func__);
        return -EINVAL;
    }
    dev_info(dev, "probed\n");

    return 0;
}

static int
mstar_i2c_remove(struct platform_device *pdev)
{
    struct mstar_i2c_priv *priv = platform_get_drvdata(pdev);
    struct device *dev = &pdev->dev;

    platform_set_drvdata(pdev, NULL);
    i2c_del_adapter(&priv->adap);
    put_device(dev);

    return 0;
}

static struct platform_driver mstar_i2c_driver = {
    .driver	= {
        .name = "mstar-iic",
        .owner = THIS_MODULE,
        .pm	= MSTAR_I2C_PM_OPS,
        .of_match_table = of_match_ptr(mstar_iic_match),
    },
    .probe = mstar_i2c_probe,
    .remove = mstar_i2c_remove,
};

module_platform_driver(mstar_i2c_driver);

int mod_iic_init(void)
{
    return platform_driver_register(&mstar_i2c_driver);
}
EXPORT_SYMBOL(mod_iic_init);

void __exit mod_iic_exit(void)
{
    platform_driver_unregister(&mstar_i2c_driver);
}
EXPORT_SYMBOL(mod_iic_exit);

MODULE_AUTHOR("Hank Lai <hank.lai@mstarsemi.com>");
MODULE_DESCRIPTION("HWI2C driver for MSTAR chipsets");
MODULE_LICENSE("GPL");
