/*
 * xHCI host controller driver
 *
 * Copyright (C) 2017 MStar Inc.
 *
 *            Warning
 * This file is only for LM17A project
 */

#ifndef _XHCI_MSTAR_40403_H
#define _XHCI_MSTAR_40403_H

/*
 * It's confirmed that LM17A project won't use USB 3.0 (xHCI)
 * We should remove all content in xhci-mstar-40403.h
 * and disable CONFIG_USB_XHCI_HCD in lm17a_defconfig
 */

#endif	/* _XHCI_MSTAR_40403_H */
