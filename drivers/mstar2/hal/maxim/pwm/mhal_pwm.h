//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _HAL_PWM_H_
#define _HAL_PWM_H_

#include "mdrv_types.h"

////////////////////////////////////////////////////////////////////////////////
/// @file HALPWM.h
/// @author MStar Semiconductor Inc.
/// @brief Pulse Width Modulation hal
////////////////////////////////////////////////////////////////////////////////

#define PWM_Num     5       /* Max. 6 */

////////////////////////////////////////////////////////////////////////////////
// Macro for utility
////////////////////////////////////////////////////////////////////////////////
#define MST_MACRO_START     do {
#define MST_MACRO_END       } while (0)

#define UNUSED( var )       ((void)(var))

////////////////////////////////////////////////////////////////////////////////
// Macro for bitwise
////////////////////////////////////////////////////////////////////////////////
#define _BITMASK(loc_msb, loc_lsb) ((1U << (loc_msb)) - (1U << (loc_lsb)) + (1U << (loc_msb)))
#define BITMASK(x) _BITMASK(1?x, 0?x)
#define BITFLAG(loc) (1U << (loc))

#define SETBIT(REG, BIT)   ((REG) |= (1UL << (BIT)))
#define CLRBIT(REG, BIT)   ((REG) &= ~(1UL << (BIT)))
#define GETBIT(REG, BIT)   (((REG) >> (BIT)) & 0x01UL)
#define COMPLEMENT(a)      (~(a))
#define BITS(_bits_, _val_)         ((BIT(((1)?_bits_)+1)-BIT(((0)?_bits_))) & (_val_<<((0)?_bits_)))
#define BMASK(_bits_)               (BIT(((1)?_bits_)+1)-BIT(((0)?_bits_)))
#define READ_WORD(_reg)             (*(volatile U16*)(_reg))
#define WRITE_BYTE(_reg, _val)      { (*((volatile U8*)(_reg))) = (U8)(_val); }
#define WRITE_WORD(_reg, _val)      { (*((volatile U16*)(_reg))) = (U16)(_val); }

////////////////////////////////////////////////////////////////////////////////////////
// Extern function
////////////////////////////////////////////////////////////////////////////////
BOOL HAL_PWM_Init(void);
BOOL HAL_PWM_Oen(PWM_ChNum index, BOOL letch);
BOOL HAL_PWM_UnitDiv(U16 u16DivPWM);
void HAL_PWM_Period(PWM_ChNum index, U32 u32PeriodPWM);
void HAL_PWM_DutyCycle(PWM_ChNum index, U32 u32DutyPWM);
void HAL_PWM_Div(PWM_ChNum index, U16 u16DivPWM);
void HAL_PWM_Polarity(PWM_ChNum index, BOOL bPolPWM);
void HAL_PWM_VDBen(PWM_ChNum index, BOOL bVdbenPWM);
void HAL_PWM_Vrest(PWM_ChNum index, BOOL bRstPWM);
void HAL_PWM_DBen(PWM_ChNum index, BOOL bdbenPWM);
void HAL_PWM_RstMux(PWM_ChNum index, BOOL bMuxPWM);
void HAL_PWM_RstCnt(PWM_ChNum index, U8 u8RstCntPWM);
void HAL_PWM_BypassUnit(PWM_ChNum index, BOOL bBypassPWM);
BOOL HAL_PWM01_CntMode(PWM_CntMode CntMode);
BOOL HAL_PWM23_CntMode(PWM_CntMode CntMode);
BOOL HAL_PWM67_CntMode(PWM_CntMode CntMode);
BOOL HAL_PWM_Shift(PWM_ChNum index, U32 u32ShiftPWM);
void HAL_PWM_IMPULSE_EN(PWM_ChNum index, BOOL bdbenPWM);
void HAL_PWM_ODDEVEN_SYNC(PWM_ChNum index, BOOL bdbenPWM);
void HAL_PWM_Nvsync(PWM_ChNum index, BOOL bNvsPWM);
void HAL_PWM_Align(PWM_ChNum index, BOOL bAliPWM);

void HAL_PM_PWM_Enable(void);
void HAL_PM_PWM_Period(U16 u16PeriodPWM);
void HAL_PM_PWM_DutyCycle(U16 u16DutyPWM);
void HAL_PM_PWM_Div(U8 u8DivPWM);
void HAL_PM_PWM_Polarity(BOOL bPolPWM);
void HAL_PM_PWM_DBen(BOOL bdbenPWM);

#endif // _HAL_PWM_H_
