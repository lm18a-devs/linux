/*
 * Copyright (c) 2010-2011 MStar Semiconductor, Inc.
 * All rights reserved.
 *
 * Unless otherwise stipulated in writing, any and all information contained
 * herein regardless in any format shall remain the sole proprietary of
 * MStar Semiconductor Inc. and be kept in strict confidence
 * ("MStar Confidential Information") by the recipient.
 * Any unauthorized act including without limitation unauthorized disclosure,
 * copying, use, reproduction, sale, distribution, modification, disassembling,
 * reverse engineering and compiling of the contents of MStar Confidential
 * Information is unlawful and strictly prohibited. MStar hereby reserves the
 * rights to any and all damages, losses, costs and expenses resulting therefrom.
 *
 * MStar's Audio Driver for ALSA.
 * Author: Darren Chen <darren.chen@mstarsemi.com>
 *
 */

#ifndef _MHAL_ALSA_DRIVER_HEADER
#define _MHAL_ALSA_DRIVER_HEADER

/*
 * ============================================================================
 * Include Headers
 * ============================================================================
 */
#include "mdrv_public.h"
#include "mhal_version.h"


/*
 * ============================================================================
 * Constant Definitions
 * ============================================================================
 */
/* Define a Ring Buffer data structure for MStar Audio DSP */
struct MStar_Ring_Buffer_Struct {
	unsigned char *addr;
	unsigned int size;
	unsigned char *w_ptr;
	unsigned char *r_ptr;
};

/* Define a STR (Suspend To Ram) data structure for MStar Audio DSP */
 struct MStar_STR_MODE_Struct {
	unsigned int status;
	ptrdiff_t physical_addr;
	ptrdiff_t bus_addr;
	ptrdiff_t virtual_addr;
};

/* Define a DMA Reader data structure for MStar Audio DSP */
struct MStar_DMA_Reader_Struct {
	struct MStar_Ring_Buffer_Struct buffer;
	struct MStar_STR_MODE_Struct str_mode_info;
	unsigned int initialized_status;
	unsigned int channel_mode;
	unsigned int sample_rate;
	unsigned int period_size;
	unsigned int high_threshold;
	unsigned int remain_size;
	unsigned int written_size;
};

/* Define a DMA Reader data structure for MStar Audio DSP */
struct MStar_PCM_Capture_Struct {
	struct MStar_Ring_Buffer_Struct buffer;
	struct MStar_STR_MODE_Struct str_mode_info;
	unsigned int initialized_status;
	unsigned int channel_mode;
	unsigned int sample_rate;
};

/* Define a PCM Mixer Element Info data structure for MStar Audio DSP */
struct MStar_PCM_Info_Struct {
	unsigned int struct_version;
	unsigned int struct_size;
	unsigned char connect_flag;
	unsigned char start_flag;
	unsigned char name[32];
	unsigned char non_blocking_flag;
	unsigned char multi_channel_flag;
	unsigned char mixing_flag;
	unsigned int mixing_group;
	unsigned int buffer_duration;
	unsigned int channel;
	unsigned int sample_rate;
	unsigned int bit_width;
	unsigned int big_endian;
	unsigned int timestamp;
	unsigned int weighting;
	unsigned int volume;
	unsigned int buffer_level;
	unsigned char capture_flag;
	unsigned char delay_flag;
};

/* Define a PCM buffer structure for MStar Audio DSP */
struct MStar_PCM_Buffer_Info_Struct {
	unsigned char *phy_addr;
	unsigned int r_offset;
	unsigned int w_offset;
	unsigned int size;
	unsigned int high_threshold;
	unsigned int remain_size;
	unsigned int next_r_offset;
};

/* Define a PCM Mixer data structure for MStar Audio DSP */
struct MStar_PCM_SWMixer_Client_Struct {
	struct MStar_STR_MODE_Struct str_mode_info[3];
	struct MStar_PCM_Info_Struct *pcm_info;
	struct MStar_PCM_Buffer_Info_Struct *pcm_buffer_info;
	unsigned char *addr;
	unsigned int client_id;
	unsigned int initialized_status;
	unsigned int channel_mode;
	unsigned int sample_rate;
	unsigned int period_size;
	unsigned int written_size;
	unsigned int group_id;
};


/*
 * ============================================================================
 * Forward Declarations
 * ============================================================================
 */
extern void Chip_Flush_Memory(void);

#endif /* _MHAL_ALSA_DRIVER_HEADER */
