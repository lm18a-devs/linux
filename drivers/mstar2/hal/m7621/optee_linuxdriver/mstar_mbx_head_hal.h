#ifndef __MSTAR_MBX_HEAD_HAL_H__
#define __MSTAR_MBX_HEAD_HAL_H__

#define TRIGGER_MBX_INTERRUPT \
		do { \
			printk("\033[0;32;31m [SECARM] %s %d Not Impelement\033[m\n",__func__,__LINE__); \
		} while(0)

#define CLEAN_MBX_INTERRUPT \
		do { \
			printk("\033[0;32;31m [SECARM] %s %d Not Impelement\033[m\n",__func__,__LINE__); \
		} while(0)

#define ENABLE_FIQ_MASK \
		do { \
			printk("\033[0;32;31m [SECARM] %s %d Not Impelement\033[m\n",__func__,__LINE__); \
		} while(0)

#define MBX_BASE 0xffffffff
#define MBX_LEN 0x0

#define SECARM_IRQ 0xffff

#define MAX_SEC_ARM_NUM 0

#endif
