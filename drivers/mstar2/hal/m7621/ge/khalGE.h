//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _HAL_GE_H_
#define _HAL_GE_H_

#include "mdrv_mstypes.h"
#include "mdrv_types.h"

//	#ifndef MS_U8
//	typedef U8 MS_U8;
//	#endif
//
//	#ifndef MS_U16
//	typedef U16 MS_U16;
//	#endif
//
//	#ifndef MS_U32
//	typedef U32 MS_U32;
//	#endif
//
//	#ifndef MS_S32
//	typedef S32 MS_S32;
//	#endif
//
#ifndef MS_PHYADDR
typedef U32 MS_PHYADDR;
#endif
//
//	#ifndef MS_BOOL
//	typedef BOOL MS_BOOL;
//	#endif

//	#ifndef MS_PHY
//	typedef U32 MS_PHY;
//	#endif


#include "kdrvGE.h"
#include "chip_int.h"

#define E_GE_INT_NUMBER_DEF  E_IRQHYPL_GE

#define GE_VQADDR_LOCK_PATCH 1
#define GE_VQADDR_ENHANCE_LOCK_TIMES 5

#define GE_TABLE_REGNUM             0x80
#define GE_PALETTE_NUM              		    256UL
#define GE_WordUnit                 16
#define GE_MAX_MIU                  2
#define GE_THRESHOLD_SETTING        0x7
#define bNeedSetActiveCtrlMiu1      FALSE


#define GE_FMT_I1                 0x0
#define GE_FMT_I2                 0x1
#define GE_FMT_I4                 0x2
#define GE_FMT_I8                 0x4
#define GE_FMT_FaBaFgBg2266       0x6
#define GE_FMT_1ABFgBg12355       0x7
#define GE_FMT_RGB565             0x8
#define GE_FMT_ARGB1555           0x9
#define GE_FMT_ARGB4444           0xA
#define GE_FMT_ARGB1555_DST       0xC
#define GE_FMT_YUV422             0xE
#define GE_FMT_ARGB8888           0xF
#define GE_FMT_RGBA5551           0x10
#define GE_FMT_ABGR1555           0x11
#define GE_FMT_BGRA5551           0x12
#define GE_FMT_RGBA4444           0x13
#define GE_FMT_ABGR4444           0x14
#define GE_FMT_BGRA4444           0x15
#define GE_FMT_BGR565             0x16
#define GE_FMT_RGBA8888           0x1D
#define GE_FMT_ABGR8888           0x1E
#define GE_FMT_BGRA8888           0x1F
#define GE_FMT_GENERIC            0xFFFF

#if (GE_WordUnit==32)
#define GE_VQ_4K                  0x0UL
#define GE_VQ_8K                  0x0UL
#define GE_VQ_16K                 0x0UL
#define GE_VQ_32K                 0x1UL
#define GE_VQ_64K                 0x2UL
#define GE_VQ_128K                0x3UL
#define GE_VQ_256K                0x4UL
#define GE_VQ_512K                0x5UL
#define GE_VQ_1024K               0x6UL
#define GE_VQ_2048K               0x7UL
#else
#define GE_VQ_4K                  0x0UL
#define GE_VQ_8K                  0x0UL
#define GE_VQ_16K                 0x1UL
#define GE_VQ_32K                 0x2UL
#define GE_VQ_64K                 0x3UL
#define GE_VQ_128K                0x4UL
#define GE_VQ_256K                0x5UL
#define GE_VQ_512K                0x6UL
#define GE_VQ_1024K               0x7UL
#endif

/*the following is for parameters for shared between multiple process context*/
typedef struct
{
   MS_BOOL bGE_DirectToReg;
   MS_U16 global_tagID;
   MS_U16 u16ShareRegImage[GE_TABLE_REGNUM];
   MS_U16 u16ShareRegImageEx[GE_TABLE_REGNUM];
#ifdef GE_VQ_MIU_HANG_PATCH
   MS_U8  u8VCmdQMiu;
#endif
}GE_CTX_HAL_SHARED;

/*the following is for parameters for used in local process context*/
typedef struct
{
   GE_CTX_HAL_SHARED *pHALShared;
   GE_CHIP_PROPERTY  *pGeChipPro;
   MS_U32            u32_mmio_base;
   MS_U32            u32_mmio_base2;
   MS_U16            u16RegGETable[GE_TABLE_REGNUM];                 //Store for GE RegInfo
   MS_U16            u16RegGETableEX[GE_TABLE_REGNUM];                 //Store for GE RegInfo
   MS_BOOL           bIsComp;
   MS_BOOL           bPaletteDirty;
   MS_U32            u32Palette[GE_PALETTE_NUM];
   MS_BOOL          bYScalingPatch;
}GE_CTX_HAL_LOCAL;


void        GE_Chip_Proprity_Init(GE_CTX_HAL_LOCAL *pGEHalLocal);
GE_Result   GE_SetRotate(GE_CTX_HAL_LOCAL *pGEHalLocal, GE_RotateAngle geRotAngle);
MS_U16      GE_ReadReg(GE_CTX_HAL_LOCAL *pGEHalLocal, MS_U16 addr);
GE_Result   GE_SetBltScaleRatio(GE_CTX_HAL_LOCAL *pGEHalLocal,GE_Rect *src, GE_DstBitBltType *dst, GE_Flag flags, GE_ScaleInfo* scaleinfo);
void        GE_WriteReg(GE_CTX_HAL_LOCAL *pGEHalLocal, MS_U16 addr, MS_U16 value);
GE_Result   GE_SetOnePixelMode(GE_CTX_HAL_LOCAL *pGEHalLocal, MS_BOOL enable);
GE_Result HAL_GE_EnableCalcSrc_WidthHeight(GE_CTX_HAL_LOCAL *pGEHalLocal, MS_BOOL bEnable);
GE_Result   HAL_GE_AdjustDstWin( GE_CTX_HAL_LOCAL *pGEHalLocal, MS_BOOL bDstXInv );
GE_Result   HAL_GE_AdjustRotateDstWin(GE_CTX_HAL_LOCAL *pGEHalLocal, MS_U8 u8Rotate);
MS_BOOL     GE_NonOnePixelModeCaps(GE_CTX_HAL_LOCAL *pGEHalLocal, PatchBitBltInfo* patchInfo);
MS_U16      GE_GetNextTAGID(GE_CTX_HAL_LOCAL *pGEHalLocal, MS_BOOL bStepTagBefore);
GE_Result   GE_SetDFBBldOP(GE_CTX_HAL_LOCAL *pGEHalLocal, GE_DFBBldOP geSrcBldOP, GE_DFBBldOP geDstBldOP);
void        GE_Init_HAL_Context(GE_CTX_HAL_LOCAL *pGEHalLocal, GE_CTX_HAL_SHARED *pHALShared, MS_BOOL bNeedInitShared);
void        GE_Init(GE_CTX_HAL_LOCAL *pGEHalLocal, GE_Config *cfg);
GE_Result   GE_WaitTAGID(GE_CTX_HAL_LOCAL *pGEHalLocal, MS_U16 tagID, MS_BOOL bWaitForever, MS_BOOL * bwaitSuccess);
GE_Result   GE_GetFmtCaps(GE_CTX_HAL_LOCAL *pGEHalLocal, GE_BufFmt fmt, GE_BufType type, GE_FmtCaps *caps);
MS_U32      GE_ConvertAPIAddr2HAL(GE_CTX_HAL_LOCAL *pGEHalLocal, MS_U8 u8MIUId, MS_U32 u32GE_APIAddrInMIU);
GE_Result   GE_SetAlpha(GE_CTX_HAL_LOCAL *pGEHalLocal, GE_AlphaSrc eAlphaSrc);
GE_Result   GE_SetBlend(GE_CTX_HAL_LOCAL *pGEHalLocal, GE_BlendOp eBlendOp);
GE_Result   HAL_GE_EnableTagIntr( GE_CTX_HAL_LOCAL *pGEHalLocal, MS_BOOL enable);
GE_Result   HAL_GE_MaskIRQ( GE_CTX_HAL_LOCAL *pGEHalLocal, U16 u16GeInterface, MS_BOOL enable);
GE_Result   HAL_GE_ClearIRQ( GE_CTX_HAL_LOCAL *pGEHalLocal, U16 u16GeInterface);

#define      GE_SetSrcBufMIUId(pGEHalLocal, u8MIUId)  do{}while(0)
#define      GE_SetDstBufMIUId(pGEHalLocal, u8MIUId)  do{}while(0)
#define      GE_SetVQBufMIUId(pGEHalLocal, u8MIUId)  do{}while(0)


#endif // _HAL_GE_H_
