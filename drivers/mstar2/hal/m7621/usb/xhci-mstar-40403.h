/*
 * xHCI host controller driver
 *
 * Copyright (C) 2017 MStar Inc.
 *
 *            Warning
 * This file is only for LM18A project
 */

#ifndef _XHCI_MSTAR_40403_H
#define _XHCI_MSTAR_40403_H

/*
 * LM18A doesn't have USB 3.0 (xHCI) controller
 * We should remove all content in xhci-mstar-40403.h
 * and disable CONFIG_USB_XHCI_HCD in lm18a_defconfig
 */

#endif	/* _XHCI_MSTAR_40403_H */
