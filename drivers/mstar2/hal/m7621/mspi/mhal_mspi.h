//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2009-2012 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef _MHAL_MSPI_H_
#define _MHAL_MSPI_H_

//-------------------------------------------------------------------------------------------------
//  Macro and Define
//-------------------------------------------------------------------------------------------------
#define MSPI_READ_INDEX                0x0
#define MSPI_WRITE_INDEX               0x1
/* check if chip support MSPI*/
//#define HAL_MSPI_HW_Support()          TRUE
#define DEBUG_MSPI(debug_level, x)     do { if (_u8MSPIDbgLevel >= (debug_level)) (x); } while(0)

//-------------------------------------------------------------------------------------------------
//  Type and Structure
//-------------------------------------------------------------------------------------------------



typedef enum {
    E_MSPI_MODE0, //CPOL = 0,CPHA =0
    E_MSPI_MODE1, //CPOL = 0,CPHA =1
    E_MSPI_MODE2, //CPOL = 1,CPHA =0
    E_MSPI_MODE3, //CPOL = 1,CPHA =1
    E_MSPI_MODE_MAX,
} MSPI_Mode_Config_e;

typedef enum
{
    E_MSPI1,
    E_MSPI2,
    E_MSPI_MAX,
}MSPI_CH;

typedef enum
{
    E_MSPI_ChipSelect_0,
    E_MSPI_ChipSelect_1,
    E_MSPI_ChipSelect_2,
    E_MSPI_ChipSelect_3,
    E_MSPI_ChipSelect_4,
    E_MSPI_ChipSelect_5,
    E_MSPI_ChipSelect_6,
    E_MSPI_ChipSelect_7,
    E_MSPI_ChipSelect_MAX
}MSPI_ChipSelect_e;

typedef enum
{
    E_MSPI_BIT_MSB_FIRST,
    E_MSPI_BIT_LSB_FIRST,
}MSPI_BitSeq_e;

typedef enum _HAL_CLK_Config
{
    E_MSPI_POL,
    E_MSPI_PHA,
    E_MSPI_CLK
}eCLK_config;

typedef enum _HAL_DC_Config
{
    E_MSPI_TRSTART,
    E_MSPI_TREND,
    E_MSPI_TB,
    E_MSPI_TRW
}eDC_config;

typedef struct
{
    MSPI_CH eCurrentCH;;
    MS_U32 VirtMspBaseAddr;
    MS_U32 VirtClkBaseAddr;
} MSPI_BaseAddr_st;

typedef struct
{
    MS_U8 u8TrStart;      //time from trigger to first SPI clock
    MS_U8 u8TrEnd;        //time from last SPI clock to transferred done
    MS_U8 u8TB;           //time between byte to byte transfer
    MS_U8 u8TRW;          //time between last write and first read
} MSPI_DCConfig;

typedef struct
{
    MS_U8 u8WBitConfig[8];      //bits will be transferred in write buffer
    MS_U8 u8RBitConfig[8];      //bits Will be transferred in read buffer
} MSPI_FrameConfig;

typedef struct
{
    MS_U32 u32Clock;
    MS_U8 U8Clock;
    MS_BOOL BClkPolarity;
    MS_BOOL BClkPhase;
    MS_U32 u32MAXClk;
} MSPI_CLKConfig;


typedef struct
{
    MS_U8 u8ClkSpi_P1;
    MS_U8 u8ClkSpi_P2;
    MS_U8 u8ClkSpi_DIV;
    MS_U32 u32ClkSpi;
}ST_DRV_LD_MSPI_CLK;


typedef struct
{
    MS_BOOL bEnable;
    MSPI_CH eChannel;
    MSPI_Mode_Config_e eMSPIMode;
    MSPI_BaseAddr_st stBaseAddr;
    MSPI_CLKConfig tMSPI_ClockConfig;
    MSPI_DCConfig  tMSPI_DCConfig;
    MSPI_FrameConfig  tMSPI_FrameConfig;
    MSPI_ChipSelect_e eChipSel;
    MSPI_BitSeq_e eBLsbFirst;
    MS_U8 u8MspiBuffSizes;              //spi write buffer size
    void (*MSPIIntHandler)(void);       // interrupt handler
    MS_BOOL BIntEnable;                 // interrupt mode enable or polling mode
    //MS_U8 U8BitMapofConfig;
    //MS_U32 u32DevId;
} MSPI_config;


typedef enum
{
    E_MSPI_DBGLV_NONE,    //disable all the debug message
    E_MSPI_DBGLV_INFO,    //information
    E_MSPI_DBGLV_NOTICE,  //normal but significant condition
    E_MSPI_DBGLV_WARNING, //warning conditions
    E_MSPI_DBGLV_ERR,     //error conditions
    E_MSPI_DBGLV_CRIT,    //critical conditions
    E_MSPI_DBGLV_ALERT,   //action must be taken immediately
    E_MSPI_DBGLV_EMERG,   //system is unusable
    E_MSPI_DBGLV_DEBUG,   //debug-level messages
} MSPI_DbgLv;


typedef enum _MSPI_ERRORNOn {
     E_MSPI_OK = 0
    ,E_MSPI_INIT_FLOW_ERROR =1
    ,E_MSPI_DCCONFIG_ERROR =2
    ,E_MSPI_CLKCONFIG_ERROR =4
    ,E_MSPI_FRAMECONFIG_ERROR =8
    ,E_MSPI_OPERATION_ERROR = 0x10
    ,E_MSPI_PARAM_OVERFLOW = 0x20
    ,E_MSPI_MMIO_ERROR = 0x40
    ,E_MSPI_HW_NOT_SUPPORT = 0x80
    ,E_MSPI_NULL
} MSPI_ErrorNo;

//-------------------------------------------------------------------------------------------------
//  Function and Variable
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Global Functions
//-------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// Description : MSPI initial
/// @return void:
//------------------------------------------------------------------------------
void HAL_MSPI_Init(MSPI_CH eChannel);
void HAL_MSPI_IntEnable(MSPI_CH eChannel,MS_BOOL bEnable);
void HAL_MSPI_SetChipSelect(MSPI_CH eChannel, MS_BOOL Enable, MSPI_ChipSelect_e eCS);
MS_BOOL HAL_MSPI_Read(MSPI_CH eChannel, MS_U8 *pData, MS_U16 u16Size);
MS_BOOL HAL_MSPI_Write(MSPI_CH eChannel, MS_U8 *pData, MS_U16 u16Size);
MS_BOOL HAL_MSPI_SetReadBufferSize(MSPI_CH eChannel,  MS_U8 u8Size);
MS_BOOL HAL_MSPI_SetWriteBufferSize(MSPI_CH eChannel,  MS_U8 u8Size);
MS_U8 HAL_MSPI_Read_Write(MSPI_CH eChannel,  MS_U8 *pReadData,U8 *pWriteData, MS_U8 u8WriteSize);
MS_BOOL HAL_MSPI_Reset_DCConfig(MSPI_CH eChannel);
MS_BOOL HAL_MSPI_Reset_FrameConfig(MSPI_CH eChannel);
MS_BOOL HAL_MSPI_Reset_CLKConfig(MSPI_CH eChannel);
MS_BOOL HAL_MSPI_Trigger(void);
void HAL_MSPI_SlaveEnable(MSPI_CH eChannel, MS_BOOL Enable);
void HAL_MSPI_SetDcTiming (MSPI_CH eChannel, eDC_config eDCField, MS_U8 u8DCtiming);
void HAL_MSPI_SetCLKTiming(MSPI_CH eChannel, eCLK_config eCLKField, MS_U8 u8CLKVal);
MS_BOOL HAL_MSPI_LD_CLK_Config(MS_U8 u8Chanel,MS_U32 u32MspiClk);

void HAL_MSPI_SetPerFrameSize(MSPI_CH eChannel, MS_BOOL bDirect, MS_U8 u8BufOffset, MS_U8 u8PerFrameSize);
MS_BOOL HAL_MSPI_CLOCK_Config(MSPI_CH eChannel, MS_U32 u32MaxClock);
#endif

