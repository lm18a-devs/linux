//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (!¡±MStar Confidential Information!¡L) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
//==============================================================================
#ifndef _MHAL_XC_MENULOAD_C
#define _MHAL_XC_MENULOAD_C

// Common Definition
#include <linux/kernel.h>
#include "chip_int.h"
#include "mdrv_mstypes.h"
#include "mhal_xc_chip_config.h"
#include "mdrv_xc_st.h"
#include "mhal_menuload.h"
#include "mhal_xc.h"

MS_U32 _XC_Device_Offset[]={0,128};//MAX_XC_DEVICE_NUM

MS_BOOL KHal_XC_MLoad_GetCaps(void)
{
    return TRUE;
}

MS_U16 KHal_XC_MLoad_get_status(EN_MLOAD_CLIENT_TYPE enClient)
{
    return ((SC_R2BYTE(enClient, XC_ADDR_L(0x1F,0x02)) & 0x8000)>>15);
}

void KHal_XC_MLoad_set_on_off(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn)
{
    if(bEn)
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x02), 0x8000, 0x8000);
    else
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x02), 0x0000, 0x8000);
}

void KHal_XC_MLoad_set_len(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16Len)
{
    u16Len &= 0x7FF;
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x02), u16Len, 0x7FF);
}

void KHal_XC_MLoad_set_depth(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16depth)
{
    SC_W2BYTE(enClient, XC_ADDR_L(0x1F,0x01), u16depth);
}

void KHal_XC_MLoad_set_miusel(EN_MLOAD_CLIENT_TYPE enClient,MS_U8 u8MIUSel)
{
    if (u8MIUSel == 0)
    {
      SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x1E), 0x0000, 0x0003);
    }
    else if (u8MIUSel == 1)
    {
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x1E), 0x0001, 0x0003);
    }
}

void KHal_XC_MLoad_set_base_addr(EN_MLOAD_CLIENT_TYPE enClient,MS_PHY u32addr)
{
    u32addr /= MS_MLOAD_MEM_BASE_UNIT;

    SC_W2BYTE(enClient, XC_ADDR_L(0x1F,0x03), (MS_U16)(u32addr & 0xFFFF));
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x04), (MS_U16)((u32addr & 0x3FF0000)>>16), 0x003FF);
}

void KHal_XC_MLoad_Set_riu(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn)
{
    if (bEn)
    {
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x10), 0x1000, 0x1000);
    }
    else
    {
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x10), 0x0000, 0x1000);
    }
}

void KHal_XC_MLoad_set_trigger_timing(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16sel)
{
    u16sel = (u16sel & 0x0003)<<12;
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x20,0x19), u16sel, 0x3000);
}

void KHal_XC_MLoad_set_opm_lock(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16sel)
{
    u16sel = (u16sel & 0x0003)<<8;
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x20,0x19), u16sel, 0x0300);
}

void KHal_XC_MLoad_set_trigger_delay(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16delay)
{
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x20,0x22), u16delay, 0x0FFF);
}

//___|T_________________........__|T____ VSync
//__________|T__________________         ATP(refer the size befor memory to cal the pip sub and main length)
//_________________|T___________         Disp

//Generate TRAIN_TRIG_P from delayed line of Vsync(Setting the delay line for Auto tune area)
//Generate DISP_TRIG_P from delayed line of Vsync(Setting the delay line for Display area)
void KHal_XC_MLoad_set_trig_p(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16train, MS_U16 u16disp)
{
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x20,0x1A), u16train, 0x0FFF);
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x20,0x1B), u16disp,  0x0FFF);
}

//Get the delay line for Auto tune area
//Get the delay line for Display area
MS_BOOL KHal_XC_MLoad_get_trig_p(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 *pu16Train, MS_U16 *pu16Disp)
{
    *pu16Train = SC_R2BYTEMSK(enClient, XC_ADDR_L(0x20,0x1A), 0x0FFF);
    *pu16Disp = SC_R2BYTEMSK(enClient, XC_ADDR_L(0x20,0x1B), 0x0FFF);
    return TRUE;
}

void KHal_XC_MLoad_set_riu_cs(MS_BOOL bEn)
{
    if(bEn)
    {
        MDrv_WriteByteMask(0x100104, 0x10, 0x10);
    }
    else
    {
        MDrv_WriteByteMask(0x100104, 0x00, 0x10);
    }
}


void KHal_XC_MLoad_set_sw_dynamic_idx_en(MS_BOOL ben)
{
    ben = ben;
}

void KHal_XC_MLoad_set_miu_bus_sel(EN_MLOAD_CLIENT_TYPE enClient,MS_U8 u8BitMode)
{
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x13), (u8BitMode << 14), 0xC000); //00: 64bit, 01:128bit, 11:256bit, ML/DS is use 0x00, DS seperate mode is use 0x01
}

void KHal_XC_MLoad_enable_watch_dog(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn)
{
    if(bEn)
    {
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x05), 0xC000, 0xF000);
    }
    else
    {
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x05), 0x0000, 0xF000);
    }
}

void KHal_XC_MLoad_enable_watch_dog_reset(EN_MLOAD_CLIENT_TYPE enClient,MLoad_WD_Timer_Reset_Type enMLWDResetType)
{
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x06), (enMLWDResetType << 8), BIT(8)|BIT(9));
}

void KHal_XC_MLoad_set_watch_dog_time_delay(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 value)
{
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x05), value, 0x03FF);
}

void KHal_XC_MLoad_set_opm_arbiter_bypass(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL ben)
{
    if (ben)
    {
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x12,0x70), BIT(2), BIT(2));
    }
    else
    {
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x12,0x70), 0x00, BIT(2));
    }
}

void KHal_XC_MLoad_Enable_64BITS_COMMAND(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn)
{
    SC_W2BYTEMSK(enClient,XC_ADDR_L(0x1F,0x70), bEn?BIT(0):0x00, BIT(0));
}

void KHal_XC_MLoad_Enable_64BITS_SPREAD_MODE(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn)
{
    SC_W2BYTEMSK(enClient,XC_ADDR_L(0x1F,0x70), bEn?BIT(15):0x00, BIT(15));
}

void KHal_XC_MLoad_Set_64Bits_MIU_Bus_Sel(EN_MLOAD_CLIENT_TYPE enClient)
{
    MS_U16 u16sel = 0x00;

    if( MS_MLOAD_BUS_WIDTH == 8 )
    {
        u16sel = 0x00;
    }
    else if( MS_MLOAD_BUS_WIDTH == 16 )
    {
        u16sel = 0x01;
    }
    else if( MS_MLOAD_BUS_WIDTH == 32 )
    {
        u16sel = 0x3;
    }
    else
    {
        printk("MIU Bus not support !!!!!!!!!!!!!!!!!\n");
        u16sel = 0x00;
    }

    u16sel = (u16sel & 0x0003)<<14;
    SC_W2BYTEMSK(enClient,XC_ADDR_L(0x1F,0x13), u16sel, 0xC000);
}

MS_U8 KHal_XC_MLoad_Get_64Bits_MIU_Bus_Sel(EN_MLOAD_CLIENT_TYPE enClient)
{
    return (SC_R2BYTEMSK(enClient,XC_ADDR_L(0x1F,0x13), 0xC000) >>14);
}

void KHal_XC_MLoad_Command_Format_initial(EN_MLOAD_CLIENT_TYPE enClient)
{
    if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
    {
        KHal_XC_MLoad_Enable_64BITS_COMMAND(enClient,TRUE);
        if(ENABLE_64BITS_SPREAD_MODE)
        {
            KHal_XC_MLoad_Enable_64BITS_SPREAD_MODE(enClient,TRUE);
        }
        //select MIU Bus : 00: 64bit, 01:128bit, 11:256bit
        KHal_XC_MLoad_Set_64Bits_MIU_Bus_Sel(enClient);
    }
    else
    {
        KHal_XC_MLoad_set_miu_bus_sel(enClient,MS_MLOAD_MIU_BUS_SEL);
    }
}

MS_U64 KHal_XC_MLoad_Gen_64bits_spreadMode(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_U64 u64CmdTemp = 0;
    MS_U8  u8AddrTemp = 0;
    MS_U16 u16BankTemp = 0;
    MS_U16 u16DataTemp = 0;
    MS_U16 u16MaskTemp = 0;

    u16MaskTemp = 0xFFFF;
    u16DataTemp = (SC_R2BYTE(enClient,(0x130000 | u32Addr)) & ~u16Mask) | (u16Data & u16Mask);

    u8AddrTemp= (u32Addr & 0xFF) >> 1;
    //patch for fix that img shoke when paly magnifying. 
    if((enClient == E_CLIENT_SUB_XC)
        &&((u32Addr == REG_SC_BK0F_07_L)
        ||(u32Addr == REG_SC_BK0F_08_L)
        ||(u32Addr == REG_SC_BK0F_09_L)
        ||(u32Addr == REG_SC_BK0F_0A_L)))
    {
        u16BankTemp= (0x1300 | ((u32Addr >> 8) & 0xFF)) + _XC_Device_Offset[E_CLIENT_MAIN_XC/CLIENT_NUM];
    }
    else
    {
        u16BankTemp= (0x1300 | ((u32Addr >> 8) & 0xFF)) + _XC_Device_Offset[enClient/CLIENT_NUM];
    }

    u64CmdTemp|= (MS_U64)u16DataTemp;
    u64CmdTemp|= ((MS_U64)u8AddrTemp<<16);
    u64CmdTemp|= ((MS_U64)u16BankTemp<<23);
    u64CmdTemp|= ((MS_U64)u16MaskTemp<<48);
    return u64CmdTemp;
}

MS_BOOL KHal_XC_MLoad_parsing_64bits_spreadMode_NonXC(MS_U64 u64Cmd, MS_U32 *u32Addr, MS_U16 *u16Data)
{
    MS_U8  u8AddrTemp = 0;
    MS_U16 u16BankTemp = 0;

    *u16Data = (MS_U16)(0xFFFF&(MS_U16)u64Cmd);
    u8AddrTemp= (MS_U8)((u64Cmd>>16 & 0x7F) << 1);
    u16BankTemp= (MS_U16)((u64Cmd >> 23) & 0xFFFF);
    *u32Addr = (MS_U32)(u8AddrTemp|u16BankTemp<<8);

    return TRUE;
}

MS_U64 KHal_XC_MLoad_Gen_64bits_spreadMode_NonXC(MS_U32 u32Bank,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    //Use HW bitmask for Sam's request
    MS_U64 u64CmdTemp = 0;
    //MS_U16 u16DataTemp = (MHal_XC_R2BYTE(u32Bank<<8|u32Addr) & ~u16Mask) | (u16Data & u16Mask);
    //u64CmdTemp|= (MS_U64)u16DataTemp;
    u64CmdTemp|= (MS_U64)u16Data;
    u64CmdTemp|= ((MS_U64) ((u32Addr<<16) >>1));
    u64CmdTemp|= ((MS_U64)u32Bank<<23);
    //u64CmdTemp|= ((MS_U64)u16Mask<<48);
    u64CmdTemp|= ((MS_U64) (~u16Mask)<<48);
    return u64CmdTemp;
}

MS_U64 KHal_XC_MLoad_Gen_64bits_subBankMode(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_MLoad_Data_64Bits_SubBank data_SBank_Mode;
    data_SBank_Mode.u32NoUse = 0x0;
    data_SBank_Mode.u8Addr = (u32Addr & 0xFF) >> 1;
    //patch for fix that img shoke when paly magnifying. 
    if((enClient == E_CLIENT_SUB_XC)
        &&((u32Addr == REG_SC_BK0F_07_L)
        ||(u32Addr == REG_SC_BK0F_08_L)
        ||(u32Addr == REG_SC_BK0F_09_L)
        ||(u32Addr == REG_SC_BK0F_0A_L)))
    {
        data_SBank_Mode.u8Bank = ((u32Addr >> 8) & 0xFF) + _XC_Device_Offset[E_CLIENT_MAIN_XC/CLIENT_NUM];
    }
    else
    {
        data_SBank_Mode.u8Bank = ((u32Addr >> 8) & 0xFF) + _XC_Device_Offset[enClient/CLIENT_NUM];
    }

    if( u16Mask == 0xFFFF )
    {
        data_SBank_Mode.u16Data = u16Data;
    }
    else
    {
        data_SBank_Mode.u16Data = (SC_R2BYTE(enClient,(0x130000 | u32Addr)) & ~u16Mask) | (u16Data & u16Mask);
    }
    return data_SBank_Mode.u64Cmd;
}

MS_BOOL KHal_XC_MLoad_parsing_32bits_subBankMode(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32MloadData, MS_U32 *pu32Addr, MS_U16 *pu16Data)
{
    MS_MLoad_Data data;
    data.u32Cmd = u32MloadData;
    *pu32Addr = (((MS_U32)(data.u8Addr))<<1) + ((((MS_U32)(data.u8Bank)) - _XC_Device_Offset[enClient/CLIENT_NUM])<<8);
    *pu16Data = data.u16Data;

    return TRUE;
}

MS_U32 KHal_XC_MLoad_Gen_32bits_subBankMode(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_MLoad_Data data;

    data.u8Addr = (u32Addr & 0xFF) >> 1;
    //patch for fix that img shoke when paly magnifying. 
    if((enClient == E_CLIENT_SUB_XC)
        &&((u32Addr == REG_SC_BK0F_07_L)
        ||(u32Addr == REG_SC_BK0F_08_L)
        ||(u32Addr == REG_SC_BK0F_09_L)
        ||(u32Addr == REG_SC_BK0F_0A_L)))
    {
        data.u8Bank = ((u32Addr >> 8) & 0xFF) + _XC_Device_Offset[E_CLIENT_MAIN_XC/CLIENT_NUM];
    }
    else
    {
        data.u8Bank = ((u32Addr >> 8) & 0xFF) + _XC_Device_Offset[enClient/CLIENT_NUM];
    }

    if( u16Mask == 0xFFFF )
    {
        data.u16Data = u16Data;
    }
    else
    {
        data.u16Data = (SC_R2BYTE(enClient, (0x130000|u32Addr)) & ~u16Mask) | (u16Data & u16Mask);
    }
    return data.u32Cmd;
}

MS_U16 KHal_XC_MLoad_Get_Depth(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16CmdCnt)
{
    MS_U16 result = u16CmdCnt;
    if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
    {
        MS_U16 u16CmdLength = 0;
        MS_U16 u16CmdNum = 0;

        u16CmdLength = 8;//64 bits command = 8 bytes
        u16CmdNum = MS_MLOAD_BUS_WIDTH / u16CmdLength;
        if((u16CmdCnt%u16CmdNum)!=0)
        {
           printk("KickOff: Commands are not full!!\n");
        }
        result = u16CmdCnt/u16CmdNum;
    }
    else
    {
        result = u16CmdCnt;
    }

    return result;
}

void KHal_XC_MLoad_set_trigger_sync(EN_MLOAD_CLIENT_TYPE enClient,EN_MLOAD_TRIG_SYNC eTriggerSync)
{
    switch(eTriggerSync)
    {
        case E_MLOAD_TRIGGER_BY_IP_MAIN_SYNC:
        {//trigger by IP_Main Vsync
           SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x1F), 0x2000, 0x7000);
        }
        break;
        case E_MLOAD_TRIGGER_BY_IP_SUB_SYNC:
        {//trigger by IP_Sub Vsync
           SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x1F), 0x3000, 0x7000);
        }
        break;
        case E_MLOAD_TRIGGER_BY_OP2_SYNC:
        {
            SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x1F), 0x4000, 0x7000);
        }
        break;
        default:
        {
            //default: trigger by OP Vsync
            #if 1
            if( SC_R2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x1F), 0x7000)!=0x0000)
            {
                //HW patch: menuload (triggered by ip vsync) can not stop
                SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x1F), 0x1000, 0x7000);
                SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x02), 0x0000, 0x8000);
                SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x02), 0x8000, 0x8000);
                SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x02), 0x0000, 0x8000);
            }
            #endif

            SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x1F), 0x0000, 0x7000);
        }
        break;
    }
}
void KHal_XC_MLoad_set_BitMask(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL enable)
{
    if(enable)
    {
        //Init XIU
        MDrv_WriteByteMask(0x10012E,BIT(0),BIT(0));
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x07), 0x8000, 0x8000);
    }else
    {
        MDrv_WriteByteMask(0x10012E,0,BIT(0));
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x07), 0x0000, 0x8000);
    }
}

#if ENABLE_NMLOAD
MS_BOOL KHal_XC_NMLoad_GetCaps(EN_MLOAD_TYPE enMloadType)
{
    if (enMloadType == E_MLOAD2)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

MS_BOOL KHal_XC_NMLoad_set_trigger_sync(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enclient)
{
    if (enMloadType == E_MLOAD2)
    {
        SC_W2BYTEMSK(enclient, XC_ADDR_L(0x1F,0x25), 0x0000, 0x7000);
    }

    return TRUE;
}

MS_BOOL KHal_XC_MLoad_EX_get_status(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enclient)
{
    if (enMloadType == E_MLOAD2)
    {
        return ((SC_R2BYTE(enclient, XC_ADDR_L(0x1F,0x22)) & 0x8000) >> 15) ? TRUE: FALSE;
    }

    return FALSE;
}

void KHal_XC_NMLoad_set_base_addr(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY u32Addr)
{
    if (enMloadType == E_MLOAD2)
    {
        u32Addr /= MS_MLOAD_MEM_BASE_UNIT;

        SC_W2BYTE(enClient, XC_ADDR_L(0x1F,0x23), (MS_U16)(u32Addr & 0xFFFF));
        SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x24), (MS_U16)((u32Addr & 0x3FF0000)>>16), 0x003FF);
    }
}

void KHal_XC_NMLoad_set_depth(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U16 u16Depth)
{
    if (enMloadType == E_MLOAD2)
    {
        SC_W2BYTE(enClient, XC_ADDR_L(0x1F,0x21), u16Depth);
    }
}

void KHal_XC_NMLoad_set_on_off(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_BOOL bEn)
{
    if (enMloadType == E_MLOAD2)
    {
        if(bEn)
            SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x22), 0x8000, 0x8000);
        else
            SC_W2BYTEMSK(enClient, XC_ADDR_L(0x1F,0x22), 0x0000, 0x8000);
    }
}
#endif
#endif
