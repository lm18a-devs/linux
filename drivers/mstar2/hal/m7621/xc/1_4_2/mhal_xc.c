////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2008 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (¡§MStar Confidential Information¡¨) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------


#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/uaccess.h>
#include <linux/vmalloc.h>
#include "mdrv_mstypes.h"
#include "mdrv_xc_st.h"
//#include "mhal_swdr.h"
#include "mhal_tmo.h"
#include "mhal_xc.h"
#include "../../emac/mhal_emac.h"
#include "chip_int.h"
#include "mstar/mstar_chip.h"
#include <linux/time.h>
#include <linux/delay.h>
#include "dolby_vision_driver.h"
#include "mhal_dynamicscaling.h"
#include <asm/cacheflush.h>
#include "mhal_xc_chip_config.h"
#include "color_format_ip.h"
#include "mdrv_xc_dynamicscaling.h"
#include "mhal_dynamicscaling.h"
#include "openHDR_seamless_hdr_mem.h"
#include "color_format_ip_write.h"
//do not include this header, definition of Marco MAIN_WINDOW is 1
//#include "mhal_pq_adaptive.h"

extern struct mutex _cfd_mutex;
extern EN_KDRV_XC_HDR_TYPE _enHDRType;
extern EN_KDRV_XC_INPUT_SOURCE_TYPE _enInputSourceType;
extern void KHal_SC_WriteSWDSCommand(EN_KDRV_SC_DEVICE u32DeviceID,EN_KDRV_WINDOW eWindow,E_DS_CLIENT client, MS_U32 u32CmdRegAddr, MS_U16 u16CmdRegValue,k_ds_reg_ip_op_sel IPOP_Sel,k_ds_reg_source_sel Source_Select,K_XC_DS_CMDCNT *pstXC_DS_CmdCnt,MS_U8 u8DSIndex);
extern void KHal_SC_WriteSWDSCommand_Mask(EN_KDRV_SC_DEVICE u32DeviceID,EN_KDRV_WINDOW eWindow,E_DS_CLIENT client, MS_U32 u32CmdRegAddr, MS_U16 u16CmdRegValue,k_ds_reg_ip_op_sel IPOP_Sel,k_ds_reg_source_sel Source_Select,K_XC_DS_CMDCNT *pstXC_DS_CmdCnt,MS_U8 u8DSIndex,MS_U16 u16Mask);
extern void msTmoHandler(MS_BOOL bWindow);

extern STU_CFDAPI_MM_PARSER PrimeSingle_result;
extern MS_BOOL bUpdateCFDPara_from_TCH;
extern MS_BOOL IsMVOP_TCH;
MS_BOOL _bTCH_init = FALSE;
extern void cfd_GetCmdCnt(K_XC_DS_CMDCNT *stXCDSCmdCnt);

#ifdef      XC_DEBUG_ENABLE
#define     XC_KDBG(_fmt, _args...)        printk("[XC (Hal)][%s:%05d] " _fmt, __FUNCTION__, __LINE__, ##_args)
#define     XC_ASSERT(_con)   do {\
                                                            if (!(_con)) {\
                                                                printk(KERN_CRIT "BUG at %s:%d assert(%s)\n",\
                                                                                                    __FILE__, __LINE__, #_con);\
                                                                BUG();\
                                                            }\
                                                          } while (0)
#else
#define     XC_KDBG(fmt, args...)
#define     XC_ASSERT(arg)
#endif

static DEFINE_MUTEX(_adl_mutex);
//static MS_BOOL bFireing3DLut = FALSE;
extern MS_BOOL KApi_XC_MLoad_WriteCmds_And_Fire(EN_MLOAD_CLIENT_TYPE _client_type,MS_U32 *pu32Addr, MS_U16 *pu16Data, MS_U16 *pu16Mask, MS_U16 u16CmdCnt);
extern MS_BOOL KApi_XC_MLoad_WriteCmd(EN_MLOAD_CLIENT_TYPE _client_type,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask);
extern MS_BOOL KApi_XC_MLoad_Fire(EN_MLOAD_CLIENT_TYPE _client_type, MS_BOOL bImmeidate);
extern void KDrv_XC_MLoad_set_trigger_sync(EN_MLOAD_CLIENT_TYPE _client_type,EN_MLOAD_TRIG_SYNC eTriggerSync);
extern void _get_shm_version(MS_U8 *u8Version);
extern ST_KDRV_XC_CFD_INIT _stCfdInit[2];
extern void Chip_Flush_Cache_Range_VA_PA(unsigned long u32VAddr,unsigned long u32PAddr,unsigned long u32Size);
extern void MHal_TMO_WriteTmoCurve(MS_BOOL bWindow);

//for Mdebug
extern MS_U8 u8MMHdrVersion;
extern MS_U16 u16SetDSAverTime, u16SetDSPeakTime;
extern MS_U64 u64FrameNumber;
//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------
#define _miu_offset_to_phy(MiuSel, Offset, PhysAddr) if (MiuSel == E_CHIP_MIU_0) \
                                                        {PhysAddr = Offset;} \
                                                     else if (MiuSel == E_CHIP_MIU_1) \
                                                         {PhysAddr = Offset + ARM_MIU1_BASE_ADDR;} \
                                                     else \
                                                         {PhysAddr = Offset + ARM_MIU2_BASE_ADDR;}

#define AUTODOWNLOAD_CLIENT_HDR_MEM_SIZE     0x180000
#define AUTODOWNLOAD_CLIENT_OP2LUT_MEM_SIZE  0x8000

#define DEBUG_HDR 0
#define AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR     512
#define AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR   4944
#define AUTO_DOWNLOAD_HDR_GAMMA_BLANKING        224
#define DMA_STR_PROTECT_REGISTER_NUMBER         10

#define AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR   8192
#define PQ_BIN_REG_ALIGNMENT     3
#define PQ_BIN_ADDR_ALIGNMENT    1
#define PQ_BIN_MASK_ALIGNMENT    2

/*
---------------------
|    DeGamma Data   |
---------------------
| 168*Byte_Per_word |
|      blanking     |
---------------------
|     Gamma Data    |
---------------------
*/

#define XVYCC_GAMMA_ENTRY   256
#define XVYCC_DEGAMMA_ENTRY 600
#define XVYCC_GAMMA_BLANKING 168 //For HW design

#define OP2LUT_ENTRY           1024
#define OP2LUT_ENTRY_DATA_NUM  3
#define OP2LUT_SRAM_ENTRY      128

#define AUTO_DOWNLOAD_WAIT_COUNT    10  // 25
#define AUTO_DOWNLOAD_WAIT_TIME     5//10  // 1


MS_U8 u8HDMI_case;
#define SkipFireADLMLDBG    //printk

// HDR client of auto download has these following formats, include format0 and format1.
//
// Format0:
//127      125          116 115         112                             60        57                                   0
// | [x[2]] | [addr(10)] | 0 | [sel0(3)] |           [data1]             | sel1(3) |              [data0]              |
//                               sel0 = 1                            x(52) sel1 = 1              wd0(19),wd1(19),wd2(19)
//                               sel0 = 2     x(4),wd0(16),wd1(16),wd2(16) sel1 = 2         x(9),wd0(16),wd1(16),wd2(16)
//                               sel0 = 3                    x(40),wd0(12) sel1 = 3                        x(45),wd0(12)
//                               sel0 = 4   idx(4),wd0(16),wd1(16),wd2(16) sel1 = 4  x(5),idx(4),wd0(16),wd1(16),wd2(16)
// sel:   0: disable
//        1: De-gamma
//        2: Gamma
//        3: TMO
//        4: 3D-lut

// Format1:
//    255   254  253   252  251    244              209       200               173       164               137       127
//  |  1   |  1  |   [x[2]]   |   sel(8)   |[x[10]]|[dataR[10]]|[x[10]]|[dataG[10]]|[x[10]]|[dataB[10]]|
//
//                                  sel : SRAM Sel
//
//
//
//     127        126   125       116    115     114    112 111                          60  59     57 56                                    0
//  |ctrl0(1) |   0   | [addr(10)] |     1    | [ctrl1(3)] |           [data1]             | sel1(3) |              [data0]              |
//
// data:                                                   data1 : idx(4),wd0(16),wd1(16),wd2(16) sel1 = 4     data0 :  x(10),degam(19),gam(16),tmo(16)
// ctrl: bit: 127                                    bit: 114  113 112
//            |ootf|                                   |degam|gam|tmo|
//
// HW designer recommand using format1.
// These following definitions write datas of tmo/gamma/de-gamma/3d-lut separately by format1,
// please refer to definition of WRITE_TMO_DATA_FORMAT_1, WRITE_GAMMA_DATA_FORMAT_1, WRITE_DEGAMMA_DATA_FORMAT_1 and WRITE_3DLUT_DATA_FORMAT_1.
#define WRITE_TMO_DATA_FORMAT_1(baseaddr, index, value) \
    *(baseaddr + 15) = (((*(baseaddr + 15)) & 0xC0) | (((index) >> 4) & 0x3F));\
    *(baseaddr + 14) = ((((index) << 4) & 0xF0) | ((*(baseaddr + 14)) & 0x0F) | 0x09);\
    *(baseaddr + 1) = ((*(baseaddr + 1) & 0xF0) | (((value) >> 8) & 0x0F));\
    *(baseaddr) = ((value) & 0xFF);

/*#define WRITE_XVYCC_GAMMA_DATA_FORMAT_1(baseaddr, index, value) \
*(baseaddr + 14) =0xFF;\
  *(baseaddr + 13) =0x80;\
  *(baseaddr + 10) =  0;(((value) >> 8) & 0x0F);\
  *(baseaddr + 9) = 0;((value) & 0xFF);\
  *(baseaddr + 5) =  (((value) >> 8) & 0x0F);\
  *(baseaddr + 4) = ((value) & 0xFF);\
  *(baseaddr + 1) =  (((value) >> 8) & 0x0F);\
  *(baseaddr) = ((value) & 0xFF);8*/


/*#define WRITE_XVYCC_GAMMA_DATA_FORMAT_1(baseaddr, index, value) \
        *(baseaddr + 15+ 16) = 0xFF;\
    *(baseaddr + 14 + 16) = 0xFF;\
     *(baseaddr + 13 + 16) =   0xFF;\
     *(baseaddr + 12 + 16) =  0xFF;\
    *(baseaddr + 11 + 16) =  0xFF;\
    *(baseaddr + 10 + 16) = 0xFF;\
     *(baseaddr + 9 + 16) =   0xFF;\
     *(baseaddr + 8 + 16) =  0xFF;\
     *(baseaddr + 7 + 16) =   0xFF;\
     *(baseaddr + 6 + 16) =  0xFF;\
    *(baseaddr + 5 + 16) =  0xFF;\
    *(baseaddr + 4 + 16) =  0xFF;\
     *(baseaddr + 3 + 16) =  0xFF;\
     *(baseaddr + 2 + 16) =   0xFF;\
    *(baseaddr + 1 + 16) =   0xFF;\
    *(baseaddr + 16) =  0xFF; \
    *(baseaddr + 15) = 0xFF;\
    *(baseaddr + 14) = 0xFF;\
     *(baseaddr + 13) =  0x00;\
     *(baseaddr + 12) = 0x00;\
    *(baseaddr + 11) = 0x00;\
    *(baseaddr + 10) =(((value) >> 8) & 0x0F);\
     *(baseaddr + 9) =  ((value) & 0xFF);\
     *(baseaddr + 8) = 0x00;\
     *(baseaddr + 7) =  0x00;\
     *(baseaddr + 6) = 0x00;\
    *(baseaddr + 5) = (((value) >> 4) & 0xFF);\
    *(baseaddr + 4) = ((value<<4) & 0xF0);\
     *(baseaddr + 3) = 0x00;\
     *(baseaddr + 2) =  0x00;\
    *(baseaddr + 1) =  (((value) >> 8) & 0x0F);\
    *(baseaddr) = ((value) & 0xFF); */


// XVYCC_GAMMA:
//----------------------------------------------------------//
// | 114     113      112     [83:72]     [47:36]     [11:0]
// | R_WR    G_WR    B_WR      R_in         G_in       B_in
//----------------------------------------------------------//
#define WRITE_XVYCC_GAMMA_DATA_FORMAT_1(baseaddr,  value) \
    *(baseaddr + 15) = 0xFF;\
    *(baseaddr + 14) = 0xFF;\
     *(baseaddr + 13) =  0x00;\
     *(baseaddr + 12) = 0xFF;\
    *(baseaddr + 11) = 0xFF;\
    *(baseaddr + 10) =(((value) >> 8) & 0x0F);\
     *(baseaddr + 9) =  ((value) & 0xFF);\
     *(baseaddr + 8) = 0xFF;\
     *(baseaddr + 7) =  0xFF;\
     *(baseaddr + 6) = 0xFF;\
    *(baseaddr + 5) = (((value) >> 4) & 0xFF);\
    *(baseaddr + 4) = ((value<<4) & 0xF0);\
     *(baseaddr + 3) = 0xFF;\
     *(baseaddr + 2) =  0xFF;\
    *(baseaddr + 1) =  (((value) >> 8) & 0x0F);\
    *(baseaddr) = ((value) & 0xFF);

/*      #define WRITE_XVYCC_DEGAMMA_DATA_FORMAT_1(baseaddr, index, value) \
      *(baseaddr + 15+ 16) = 0xFF;\
  *(baseaddr + 14 + 16) = 0xFF;\
   *(baseaddr + 13 + 16) =   0xFF;\
   *(baseaddr + 12 + 16) =  0xFF;\
  *(baseaddr + 11 + 16) =  0xFF;\
  *(baseaddr + 10 + 16) = 0xFF;\
   *(baseaddr + 9 + 16) =   0xFF;\
   *(baseaddr + 8 + 16) =  0xFF;\
   *(baseaddr + 7 + 16) =   0xFF;\
   *(baseaddr + 6 + 16) =  0xFF;\
  *(baseaddr + 5 + 16) =  0xFF;\
  *(baseaddr + 4 + 16) =  0xFF;\
   *(baseaddr + 3 + 16) =  0xFF;\
   *(baseaddr + 2 + 16) =   0xFF;\
  *(baseaddr + 1 + 16) =   0xFF;\
  *(baseaddr + 16) =  0xFF; \
  *(baseaddr + 15) = 0xFF;\
  *(baseaddr + 14) = 0x00;\
   *(baseaddr + 13) =  0xFF;\
   *(baseaddr + 12) = 0xFF;\
  *(baseaddr + 11) = (((value) >> 16) & 0x07);\
  *(baseaddr + 10) =(((value) >> 8) & 0xFF);\
   *(baseaddr + 9) =  ((value) & 0xFF);\
   *(baseaddr + 8) = 0xFF;\
   *(baseaddr + 7) =  0xFF;\
   *(baseaddr + 6) = (((value) >> 12) & 0x7F);\
  *(baseaddr + 5) = (((value) >> 4) & 0xFF);\
  *(baseaddr + 4) = ((value<<4) & 0xF0);\
   *(baseaddr + 3) = 0xFF;\
   *(baseaddr + 2) =  (((value) >> 16) & 0x07);\
  *(baseaddr + 1) =  (((value) >> 8) & 0xFF);\
  *(baseaddr) = ((value) & 0xFF);*/

// XVYCC_DEGAMMA:
//----------------------------------------------------------//
// | 110     109     108     [90:72]     [54:36]     [18:0]
// | R_WR    G_WR    B_WR      R_in       G_in        B_in
//----------------------------------------------------------//

#define WRITE_XVYCC_DEGAMMA_DATA_FORMAT_1(baseaddr, value) \
    *(baseaddr + 15) = 0xFF;\
    *(baseaddr + 14) = 0x00;\
     *(baseaddr + 13) =  0xFF;\
     *(baseaddr + 12) = 0xFF;\
    *(baseaddr + 11) = (((value) >> 16) & 0x07);\
    *(baseaddr + 10) =(((value) >> 8) & 0xFF);\
     *(baseaddr + 9) =  ((value) & 0xFF);\
     *(baseaddr + 8) = 0xFF;\
     *(baseaddr + 7) =  0xFF;\
     *(baseaddr + 6) = (((value) >> 12) & 0x7F);\
    *(baseaddr + 5) = (((value) >> 4) & 0xFF);\
    *(baseaddr + 4) = ((value<<4) & 0xF0);\
     *(baseaddr + 3) = 0xFF;\
     *(baseaddr + 2) =  (((value) >> 16) & 0x07);\
    *(baseaddr + 1) =  (((value) >> 8) & 0xFF);\
    *(baseaddr) = ((value) & 0xFF);

#define WRITE_GAMMA_DATA_FORMAT_1(baseaddr, index, value) \
    *(baseaddr + 15) = (((*(baseaddr + 15)) & 0xC0) | (((index) >> 4) & 0x3F));\
    *(baseaddr + 15) = ((*(baseaddr + 15)) & 0x7F);\
    *(baseaddr + 14) = ((((index) << 4) & 0xF0) | ((*(baseaddr + 14)) & 0x0F) | 0x0A);\
    *(baseaddr + 1) = ((*(baseaddr + 1) & 0x0F) | (((value) << 4) & 0xF0));\
    *(baseaddr + 2) = (((value) >> 4) & 0xFF);\
    *(baseaddr + 3) = ((*(baseaddr + 3) & 0xF0) | (((value) >> 12) & 0x0F));

#define WRITE_OOTF_DATA_FORMAT_1(baseaddr, index, value) \
    *(baseaddr + 15) = (((*(baseaddr + 15)) & 0xC0) | (((index) >> 4) & 0x3F));\
    *(baseaddr + 15) = ((*(baseaddr + 15)) | 0x80);\
    *(baseaddr + 14) = ((((index) << 4) & 0xF0) | ((*(baseaddr + 14)) & 0x0F) | 0x0A);\
    *(baseaddr + 1) = ((*(baseaddr + 1) & 0x0F) | (((value) << 4) & 0xF0));\
    *(baseaddr + 2) = (((value) >> 4) & 0xFF);\
    *(baseaddr + 3) = ((*(baseaddr + 3) & 0xF0) | (((value) >> 12) & 0x0F));

#define WRITE_DEGAMMA_DATA_FORMAT_1(baseaddr, index, value) \
    *(baseaddr + 15) = (((*(baseaddr + 15)) & 0xC0) | (((index) >> 4) & 0x3F));\
    *(baseaddr + 14) = ((((index) << 4) & 0xF0) | ((*(baseaddr + 14)) & 0x0F) | 0x0C);\
    *(baseaddr + 3) = ((*(baseaddr + 3) & 0x0F) | (((value) << 4) & 0xF0));\
    *(baseaddr + 4) = (((value) >> 4) & 0xFF);\
    *(baseaddr + 5) = ((*(baseaddr + 5) & 0x80) | (((value) >> 12) & 0x7F));

#define WRITE_3DLUT_DATA_FORMAT_1(baseaddr, index, subindex, rval, gval, bval) \
    *(baseaddr + 15) = (((*(baseaddr + 15)) & 0xC0) | (((index) >> 4) & 0x3F));\
    *(baseaddr + 14) = ((((index) << 4) & 0xF0) | ((*(baseaddr + 14)) & 0x0F) | 0x08);\
    *(baseaddr + 7) = ((*(baseaddr + 7) & 0xF1) | 0x08);\
    *(baseaddr + 7) = ((*(baseaddr + 7) & 0x0F) | (((bval) << 4) & 0xF0));\
    *(baseaddr + 8) = (((bval) >> 4) & 0xFF);\
    *(baseaddr + 9) = ((*(baseaddr + 9) & 0xF0) | (((bval) >> 12) & 0x0F));\
    *(baseaddr + 9) = ((*(baseaddr + 9) & 0x0F) | (((gval) << 4) & 0xF0));\
    *(baseaddr + 10) = (((gval) >> 4) & 0xFF);\
    *(baseaddr + 11) = ((*(baseaddr + 11) & 0xF0) | (((gval) >> 12) & 0x0F));\
    *(baseaddr + 11) = ((*(baseaddr + 11) & 0x0F) | (((rval) << 4) & 0xF0));\
    *(baseaddr + 12) = (((rval) >> 4) & 0xFF);\
    *(baseaddr + 13) = ((*(baseaddr + 13) & 0xF0) | (((rval) >> 12) & 0x0F));\
    *(baseaddr + 13) = ((*(baseaddr + 13) & 0x0F) | (((subindex) << 4) & 0xF0));

#define WRITE_RGB3DLUT_DATA_FORMAT_1(baseaddr, subindex, rval, gval, bval) \
    *(baseaddr + 31) = ((((subindex)>>4) & 0x0F) | 0xC0);\
    *(baseaddr + 30) = (((subindex)<<4) & 0xF0);\
    *(baseaddr + 26) = (((rval)>>8) & 0x03);\
    *(baseaddr + 25) = ((rval) & 0xFF);\
    *(baseaddr + 21) = (((gval)>>4) & 0x3F);\
    *(baseaddr + 20) = (((gval)<<4) & 0xF0);\
    *(baseaddr + 17) = (((bval)>>8) & 0x03);\
    *(baseaddr + 16) = ((bval) & 0xFF);

// The following definition clear bits of format1's ctrl & sel.
#define CLEAR_HDR_DATA_FORMAT_1(baseaddr) \
    *(baseaddr + 14) = ((*(baseaddr + 14)) & 0xF8);\
    *(baseaddr + 7) = (*(baseaddr + 7) & 0xF1);


#define WRITE_XVYCC_GAMMA_RED_DATA_FORMAT(baseaddr,  value) \
     *(baseaddr + 14) = ((*(baseaddr + 14) & 0xF8) | ((*(baseaddr + 14)) & 0x07) | 0x04);\
     *(baseaddr + 10) = ((*(baseaddr + 10) & 0xF0) | (((value) >> 8) & 0x0F));\
     *(baseaddr + 9) = ((value) & 0xFF);

#define WRITE_XVYCC_GAMMA_GREEN_DATA_FORMAT(baseaddr,  value) \
     *(baseaddr + 14) = ((*(baseaddr + 14) & 0xF8) | ((*(baseaddr + 14)) & 0x07) | 0x02);\
     *(baseaddr + 5) = (((value) >> 4) & 0xFF);\
     *(baseaddr + 4) = ((*(baseaddr + 4) & 0x0F) | (((value) << 4) & 0xF0));

#define WRITE_XVYCC_GAMMA_BLUE_DATA_FORMAT(baseaddr,  value) \
     *(baseaddr + 14) = ((*(baseaddr + 14) & 0xF8) | ((*(baseaddr + 14)) & 0x07) | 0x01);\
     *(baseaddr + 1) = ((*(baseaddr + 1) & 0xF0) | (((value) >> 8) & 0x0F));\
     *(baseaddr) = ((value) & 0xFF);

#define WRITE_XVYCC_DEGAMMA_RED_DATA_FORMAT(baseaddr,  value) \
     *(baseaddr + 13) = ((*(baseaddr + 13) & 0x0F) | ((*(baseaddr + 13)) & 0xF0) | 0x40);\
     *(baseaddr + 11) = ((*(baseaddr + 11) & 0xF8) | (((value) >> 16) & 0x07));\
     *(baseaddr + 10) = (((value) >> 8) & 0xFF);\
     *(baseaddr + 9) = (value & 0xFF);

#define WRITE_XVYCC_DEGAMMA_GREEN_DATA_FORMAT(baseaddr,  value) \
     *(baseaddr + 13) = ((*(baseaddr + 13) & 0x0F) | ((*(baseaddr + 13)) & 0xF0) | 0x20);\
     *(baseaddr + 6) = ((*(baseaddr + 6) & 0x80) | (((value) >> 12) & 0x7F));\
     *(baseaddr + 5) = ((value >> 4) & 0xFF);\
     *(baseaddr + 4) = ((*(baseaddr + 4) & 0x0F) | (((value) << 4) & 0xF0));

#define WRITE_XVYCC_DEGAMMA_BLUE_DATA_FORMAT(baseaddr,  value) \
     *(baseaddr + 13) = ((*(baseaddr + 13) & 0x0F) | ((*(baseaddr + 13)) & 0xF0) | 0x10);\
     *(baseaddr + 2) = ((*(baseaddr + 2) & 0xF0) | (((value) >> 16) & 0x0F));\
     *(baseaddr + 1) = ((value >> 8) & 0xFF);\
     *(baseaddr) = ((value) & 0xFF);

#define WRITE_OP2LUT_DATA_FORMAT_1(baseaddr, sramindex, rval, gval, bval) \
    *(baseaddr + 15) = (*(baseaddr + 15) & 0xF0) | (((1 << (sramindex)) >> 4) & 0x0F);\
    *(baseaddr + 14) = (*(baseaddr + 14) & 0x0F) | (((1 << (sramindex)) << 4) & 0xF0);\
    *(baseaddr + 10) = (*(baseaddr + 10) & 0xFC) | ((rval >> 8) & 0x03);\
    *(baseaddr + 9) = ((rval) & 0xFF);\
    *(baseaddr + 5) = (*(baseaddr + 5) & 0xC0) | (((gval) >> 4) & 0x3F);\
    *(baseaddr + 4) = (*(baseaddr + 4) & 0x0F) | (((gval) << 4) & 0xF0);\
    *(baseaddr + 1) = (*(baseaddr + 1) & 0xFC) | ((bval >> 8) & 0x03);\
    *(baseaddr) = ((bval) & 0xFF);

static MS_U32 _au32_3dlut_entry_num[8] = {736, 656, 656, 576, 656, 576, 576, 512};
MS_BOOL bTmoFireEnable;
MS_BOOL _bEnableHDRCLK;
MS_BOOL _bEnableHDRCLK_for_STR = FALSE;
MS_BOOL _bTimingChanged = FALSE;
MS_BOOL bCFDInitFinished[MAX_WINDOW_NUM] = {FALSE};
MS_BOOL _bCFDInitFinished = FALSE;
MS_BOOL _bSTRResumed = FALSE;
static MS_U32 _u32SWDesrbErrCount = 0;
static MS_U16 _au16TmoVals[24] = {0};
static MS_BOOL _abVRInit = FALSE;

#if OTT_AUTO_PAUSE_DETECT
#define PAUSE_COUNT_THRESHOLD (600000000)   // 600ms
static MS_BOOL bOTTPausing = FALSE;
static struct timespec stPauseDetectStartTs;
#endif
#if HDMI_AUTO_NOSIGNAL_DETECT
#define NOSIGNAL_COUNT_THRESHOLD (200000000)   // 200ms
static MS_BOOL bNoSignal = FALSE;
static struct timespec stNoSignalDetectStartTs;
#endif
MS_U8 _u8LastEntryIdx = 0;
//-------------------------------------------------------------------------------------------------
//  Local Structures
//-------------------------------------------------------------------------------------------------
typedef enum
{
    E_KDRV_XC_AUTO_DOWNLOAD_NONE,
    E_KDRV_XC_AUTO_DOWNLOAD_CONFIGURED,
    E_KDRV_XC_AUTO_DOWNLOAD_WRITED,
    E_KDRV_XC_AUTO_DOWNLOAD_FIRED,
} EN_KDRV_XC_AUTO_DOWNLOAD_STATUS;

typedef struct
{
    MS_PHY phyBaseAddr;                 /// baseaddr
    MS_U32 u32Size;                     /// size
    MS_U32 u32MiuNo;                    /// miu no
    MS_BOOL bEnable;                    /// enable/disable the client
    EN_KDRV_XC_AUTODOWNLOAD_MODE enMode;/// work mode
    MS_U32 u32StartAddr;                /// sram start addr
    MS_U32 u32Depth;                    /// data length
} ST_KDRV_XC_AUTODOWNLOAD_CLIENT_INFO;

typedef enum
{
    E_CHIP_MIU_0 = 0,
    E_CHIP_MIU_1,
    E_CHIP_MIU_2,
    E_CHIP_MIU_3,
    E_CHIP_MIU_NUM,
} CHIP_MIU_ID;
//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------
static MS_U16 u16BK7A_42_buf = 0x0;
static MS_U16 u16BK7A_54_buf = 0x0;
static MS_U16 u16BK7A_55_buf = 0x0;
static MS_BOOL bHDRMute = FALSE;

#define DOLBY_1_4_2

#ifndef DOLBY_1_4_2
static DoVi_Config_t* _pstDmConfig = NULL;
static DoVi_Comp_ExtConfig_t* _pstCompConfExt = NULL;
static MsHdr_RegTable_t* _pstDmRegTable = NULL;
static MsHdr_Comp_Regtable_t* _pstCompRegTable = NULL;
DoVi_TargetDisplayConfig_t*  _pstDoVi_TargetDisplayConfig = NULL;
static DoVi_PQ_control_t*   _pstDoVi_PQ_control = NULL;
#endif
static ST_KDRV_XC_AUTODOWNLOAD_CLIENT_INFO _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX];
static ST_KDRV_XC_AUTODOWNLOAD_CLIENT_INFO _stXVYCCClientInfo;
static EN_KDRV_XC_AUTO_DOWNLOAD_STATUS _enAutoDownloadStatus[E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX];
static MS_U8 *pu8XVYCCAutoDownloadDRAMBaseAddr = NULL;
static MS_U8 *pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX] = {NULL};
static MS_BOOL _bUnmapAutoDownloadDRAM[E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX] = {FALSE};
static STU_CFDAPI_MM_PARSER gstMMParam = {0};
static ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info gstDRAMFormatInfo[2] = {0};

// CFD local variables
ST_KDRV_XC_CFD_INIT _stCfdInit[2] = {{0, 0, 0, 12}, {0, 0, 1, 12}};

ST_KDRV_XC_CFD_PANEL _stCfdPanel = {0, 0, 0, 0, 0, {0, 0, 0}, {0, 0, 0}, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0};

ST_KDRV_XC_CFD_HDMI _stCfdHdmi[2] =
{
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, {0, 0, 0}, {0, 0, 0}, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, {0, 0, 0}, {0, 0, 0}, 0, 0, 0, 0, 0, 0},
};


STU_CFDAPI_MM_PARSER _stCfdMm[2] =
{
    {0, 0,  0, 0, 0, 0, 0, 0, {{0, 0, 0}, {0, 0, 0}, 0, 0} ,0},
    {0, 0,  0, 0, 0, 0, 0, 0, {{0, 0, 0}, {0, 0, 0}, 0, 0} ,0},
};


ST_KDRV_XC_CFD_ANALOG _stCfdAnalog[2] =
{
    {0, 0, 0, 0, 0, 0, 2, 2, 2},
    {0, 0, 1, 0, 0, 0, 2, 2, 2}
};


ST_KDRV_XC_CFD_HDR _stCfdHdr[2] =
{
    {0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0},
};

ST_KDRV_XC_CFD_OSD _stCfdOsd[2] =
{
    {CFD_OSD_VERSION, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 100, 100, 0, 0, 0, 0, 0, 0, 0},
    {CFD_OSD_VERSION, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 100, 100, 1, 0, 0, 0, 0, 0, 0},
};

ST_KDRV_XC_CFD_HDMI _stCfdHdmi_Out[2] =
{
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, {0, 0, 0}, {0, 0, 0}, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, {0, 0, 0}, {0, 0, 0}, 0, 0, 0, 0, 0, 0},
};

ST_KDRV_XC_CFD_LINEAR_RGB _stCfdLinearRgb = {0, 0, 0, 0};

ST_KDRV_XC_CFD_FIRE _stCfdFire[2] = {{0, 0, 0, 0, 0, 0}, {0, 0, 1, 0, 0, 0}};

ST_KDRV_XC_CFD_TMO _stCfdTmo = {0, 0, {0, 0, 0}, {0, 0, 0},
    {0, 0, 0}, {0, 0, 0},
    {0, 0, 0}, {0, 0, 0},
    {0, 0, 0}, {0, 0, 0},
    {0, 0, 0}, {0, 0, 0},
    {0, 0, 0}, {0, 0, 0},0, 0
};
MS_U16 u16DumySource[11];
MS_U16 u16DumyTarget[11];

ST_KDRV_XC_PQ_QMAPDATA* pstTMO_Qmap = NULL;
E_CFD_OUTPUT_SOURCE _eCfdOutputType = E_CFD_OUTPUT_SOURCE_PANEL;

static ST_KDRV_XC_CFD_DLC _stCfdDlc = {0, 0, FALSE};
static ST_KDRV_XC_AUTODOWNLOAD_CLIENT_INFO _stHDRClientInfo;
static MS_U8 *pu8HDRAutoDownloadDRAMBaseAddr = NULL;
static MS_BOOL _bUnmapHDRAutoDownloadDRAM = FALSE;
static MS_U16 _au16DmaRegValue[DMA_STR_PROTECT_REGISTER_NUMBER]= {0};
static MS_S16 _u16DolbySWBondStatus = -1;
static MS_S16 _u16DolbyHWBondStatus = -1;
static MS_BOOL _bCfdInited = FALSE;
static STU_CFDAPI_MAIN_CONTROL _stCfdMainControl;
#define SWDR_DATA_SIZE_MAX 51
static MS_U16  u16SWDRData[SWDR_DATA_SIZE_MAX] = {0};
#if !defined (__aarch64__)
static ST_KDRV_XC_SWDR_INFO _stSWDRInfo = {0,0,u16SWDRData,NULL,0,0,E_KDRV_MAIN_WINDOW};
#else
static ST_KDRV_XC_SWDR_INFO _stSWDRInfo = {0,0,u16SWDRData,0,0,E_KDRV_MAIN_WINDOW};
#endif
static MS_BOOL _bCfdOsdSetting = FALSE;

MS_U8 u8EOSflag = 0;
MS_U8 u8count =0;
MS_BOOL _bGenBlackScreen = FALSE;
extern MS_BOOL _bPreHDR;
#if DOLBY_GD_ENABLE
extern ST_KDRV_XC_HDR_GD_INFO _stHDRGDInfos;
extern MS_U8 _u8PreDSOPMIdx;
MS_U8 _u16LastGDWdPtr = 0;
#endif
#define MAX_PWM_PORT 5
#define XC_INPUT_MD_MAX_SIZE 0x150000
#define DOLBY_OTT_CACHED_BUFFER 1
MS_U8 _u8CurrentIndex=0;
ST_KDRV_XC_SHARE_MEMORY_INFO _stShareMemInfo_ = {0, 0, 0};
extern ST_KDRV_XC_HDR_DOLBY_MEMORY_FORMAT_EX *_pstDolbyHDRShareMem;
extern ST_KDRV_XC_SHARE_MEMORY_INFO _stShareMemInfo;
static MS_U8 _u8Version = 0;
extern spinlock_t _spinlock_xc_dolby_hdr;
extern MS_U8 *_pu8RegSetAddr;
extern MS_U8 *_pu8LutsAddr;
extern MS_U8 *_pu8InputMDAddr;
#if DOLBY_OTT_CACHED_BUFFER
extern MS_U8 *_pu8InputMDAddr_Cached;
extern MS_U8 *_pu8RegSetAddr_Cached;
extern MS_U8 *_pu8LutsAddr_Cached;
#endif

extern void Chip_Flush_Cache_Range(unsigned long u32Addr, unsigned long u32Size); //Clean & Invalid L1/L2 cache
extern void Chip_Inv_Cache_Range(unsigned long u32Addr, unsigned long u32Size);  //Invalid L1/L2 cache
static volatile MS_U8 _u8DolbyStatus = 0; //BIT 0 for MM dolby, BIT 1 for HDMI dolby

#ifdef DYNAMIC_SWITCH_SDR_OPENHDR
MS_BOOL _bHDRTOSDR_CFD_DONE  = FALSE;
#endif

typedef enum
{
    EN_DOLBY_PQ_BACKLIGHT,
} EN_DOLBY_PQ_TYPE;

static MS_U8 u8SharedMemVersion[MAX_WINDOW_NUM] = {0};

//HDMI HDR SDR Seamless flag
static MS_U8 u8HDRSeamlessIPDSIndex = 0;
static MS_U8 u8HDRSeamlessOPDSIndex = 0;
static MS_BOOL bADLChangeBasedAddress = FALSE;
static MS_BOOL bSeamlessFirst = TRUE;
static MS_U8 u8LastEOTFflag = 0;
static MS_U8 u8LastExtendedColorimetryflag = 0;
static MS_U8 uds8Index = 0;

//HDMI HDR SDR Freeze flag
MS_BOOL bHDMIFreezeGOPDone = FALSE;
MS_BOOL bHDMIEOTFChange = FALSE;
MS_BOOL bHDMIEOTFChangeByRest = FALSE;
MS_BOOL bHDMISeamlessCFDDone = FALSE;
MS_BOOL bHDMIChangeDuringMute = FALSE;


//MVOP source HDR SDR with Freeze flag
MS_BOOL _bMVOPFreezeGOPDone = FALSE;
static MS_U8 _u8LastTransfer_Characteristics = 0x01;//initial SDR
static Seamless_Status _eSeamlessStatus = SEAMLESS_NONE;
static Seamless_Status _eHDMISeamlessStatus = SEAMLESS_NONE;
static MS_U8 u8Timer = 0;
MS_BOOL _bXCFreezeDone = TRUE;
static EN_PICTURE_MODE_VPQ_TYPE _ePictureModeVPQType = E_KDRV_XC_PICTURE_MODE_VPQ_NONE;
//DTV HDR SDR Seamless Flag
static MS_BOOL bDTVUpdateCFDPara = FALSE;
//-------------------------------------------------------------------------------------------------
//  Debug Functions
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Local Functions
//-------------------------------------------------------------------------------------------------
static void _MHal_XC_CFD_Convert_XCDRAM_to_CFDMMParam(ST_KDRV_XC_CFD_FIRE *pstCfdFire, STU_CFDAPI_MM_PARSER *pstMMParam, ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info *pstFormatInfo)
{
    if ((pstFormatInfo == NULL) || (pstMMParam == NULL) || (pstCfdFire == NULL))
    {
        return;
    }

    pstMMParam->u32Version                                                  = 0;
    pstMMParam->u16Length                                                   = sizeof(STU_CFDAPI_MM_PARSER);
    pstMMParam->u8Video_Full_Range_Flag                                     = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8Video_Full_Range_Flag;

    if (pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.bVUIValid == FALSE)
    {
        // Algorithm team's suggestion, when VUI is not valid, please assign them the value of following
        if(pstCfdFire->bIsHdMode == TRUE)
        {
            // HD default value
            pstMMParam->u8Colour_primaries         = 1;
            pstMMParam->u8Transfer_Characteristics = 1;
            pstMMParam->u8Matrix_Coeffs            = 1;
        }
        else
        {
            // SD default value
            pstMMParam->u8Colour_primaries         = 5;
            pstMMParam->u8Transfer_Characteristics = 6;
            pstMMParam->u8Matrix_Coeffs            = 5;
        }
    }
    else
    {
        pstMMParam->u8Colour_primaries         = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8Colour_primaries;
        pstMMParam->u8Transfer_Characteristics = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics;
        pstMMParam->u8Matrix_Coeffs            = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8Matrix_Coeffs;
    }


    pstMMParam->stu_Cfd_MM_MasterPanel_ColorMetry.u16Display_Primaries_x[0] = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[0];
    pstMMParam->stu_Cfd_MM_MasterPanel_ColorMetry.u16Display_Primaries_x[1] = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[1];
    pstMMParam->stu_Cfd_MM_MasterPanel_ColorMetry.u16Display_Primaries_x[2] = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[2];
    pstMMParam->stu_Cfd_MM_MasterPanel_ColorMetry.u16Display_Primaries_y[0] = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[0];
    pstMMParam->stu_Cfd_MM_MasterPanel_ColorMetry.u16Display_Primaries_y[1] = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[1];
    pstMMParam->stu_Cfd_MM_MasterPanel_ColorMetry.u16Display_Primaries_y[2] = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[2];
    pstMMParam->stu_Cfd_MM_MasterPanel_ColorMetry.u16White_point_x          = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16White_point_x;
    pstMMParam->stu_Cfd_MM_MasterPanel_ColorMetry.u16White_point_y          = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16White_point_y;
    pstMMParam->u32Master_Panel_Max_Luminance                               = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u32Master_Panel_Max_Luminance;
    pstMMParam->u32Master_Panel_Min_Luminance                               = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u32Master_Panel_Min_Luminance;
    pstMMParam->u8Mastering_Display_Infor_Valid                             = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.bSEIValid;
    pstMMParam->u8MM_HDR_ContentLightMetaData_Valid                         = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.bContentLightLevelEnabled;
    pstMMParam->u16Max_content_light_level                                  = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16maxContentLightLevel;
    pstMMParam->u16Max_pic_average_light_level                              = pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16maxPicAverageLightLevel;

    memcpy(&gstMMParam,pstMMParam,sizeof(STU_CFDAPI_MM_PARSER));
}

static void _MHal_XC_DolbyPatch(void)
{
    if (IS_OTT_DOLBY)
    {
        // IP CSC off
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(4));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(5));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(3));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(9));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(2));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(1));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(0));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(12));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(15));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(11));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(13));
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2B_L,0 ,BIT(14));

        MHal_XC_W2BYTEMSK(REG_SC_BK02_2D_L,0 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2E_L,0 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK02_2F_L,0 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK02_30_L,0 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK02_31_L,0 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK02_32_L,0 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK02_33_L,0 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK02_34_L,0 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK02_35_L,0 ,0x1FFF);


        // VOP 3x3 default value
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2F_L,BIT(4) ,BIT(4));
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2F_L,BIT(5) ,BIT(5));
        MHal_XC_W2BYTEMSK(REG_SC_BK0F_18_L,0 ,BIT(9));
        MHal_XC_W2BYTEMSK(REG_SC_BK0F_57_L,BIT(6) ,BIT(6));
        MHal_XC_W2BYTEMSK(REG_SC_BK0F_18_L,0 ,BIT(3));
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2F_L,BIT(2) ,BIT(2));
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2F_L,0 ,BIT(1));
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2F_L,BIT(0) ,BIT(0));
        MHal_XC_W2BYTEMSK(REG_SC_BK0F_6B_L,0 ,BIT(15));
        MHal_XC_W2BYTEMSK(REG_SC_BK10_50_L,0 ,BIT(7));
        MHal_XC_W2BYTEMSK(REG_SC_BK0F_6B_L,0 ,BIT(14));
        MHal_XC_W2BYTEMSK(REG_SC_BK10_50_L,0 ,BIT(5));
        MHal_XC_W2BYTEMSK(REG_SC_BK10_50_L,0 ,BIT(6));

        // mark this setting for controlling 3x3 para by UI
        // BUT this para should be set when dolby verification stage
#if 0
        MHal_XC_W2BYTEMSK(REG_SC_BK10_26_L,0x0731 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK10_27_L,0x04AC ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK10_28_L,0x0000 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK10_29_L,0x1DDD ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2A_L,0x04AC ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2B_L,0x1F25 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2C_L,0x0000 ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2D_L,0x04AC ,0x1FFF);
        MHal_XC_W2BYTEMSK(REG_SC_BK10_2E_L,0x0879 ,0x1FFF);
#endif

    }
}


//-------------------------------------------------------------------------------------------------
//  Global Functions
//-------------------------------------------------------------------------------------------------
E_CFD_CFIO MHal_XC_HDMI_Color_Format(MS_U8 u8PixelFormat, MS_U8 u8Colorimetry, MS_U8 u8ExtendColorimetry)
{
    if (u8PixelFormat== 1 || u8PixelFormat == 2 || u8PixelFormat == 3)
    {
        if (u8Colorimetry == 0)
        {
            return E_CFD_CFIO_YUV_BT709;
        }
        else if (u8Colorimetry == 1)
        {
            return E_CFD_CFIO_YUV_BT601_525;
        }
        else if (u8Colorimetry == 2)
        {
            return E_CFD_CFIO_YUV_BT709;
        }
        else if (u8Colorimetry == 3)
        {
            if (u8ExtendColorimetry == 0)
            {
                return E_CFD_CFIO_XVYCC_601;
            }
            else if (u8ExtendColorimetry == 1)
            {
                return E_CFD_CFIO_XVYCC_709;
            }
            else if (u8ExtendColorimetry == 2)
            {
                return E_CFD_CFIO_SYCC601;
            }
            else if (u8ExtendColorimetry == 3)
            {
                return E_CFD_CFIO_ADOBE_YCC601;
            }
            else if (u8ExtendColorimetry == 5)
            {
                return E_CFD_CFIO_YUV_BT2020_CL;
            }
            else if (u8ExtendColorimetry == 6)
            {
                return E_CFD_CFIO_YUV_BT2020_NCL;
            }
            else
            {
                return E_CFD_CFIO_RESERVED_START;
            }
        }
    }
    else
    {
        if (u8Colorimetry == 0)
        {
            return E_CFD_CFIO_RGB_BT709;
        }
        else if (u8Colorimetry == 1)
        {
            return E_CFD_CFIO_RGB_BT601_525;
        }
        else if (u8Colorimetry == 2)
        {
            return E_CFD_CFIO_RGB_BT709;
        }
        else if (u8Colorimetry == 3)
        {
            if (u8ExtendColorimetry == 0)
            {
                return E_CFD_CFIO_RGB_BT709;
            }
            else if (u8ExtendColorimetry == 1)
            {
                return E_CFD_CFIO_RGB_BT709;
            }
            else if (u8ExtendColorimetry == 2)
            {
                return E_CFD_CFIO_RGB_BT709;
            }
            else if (u8ExtendColorimetry == 3)
            {
                return E_CFD_CFIO_ADOBE_RGB;
            }
            else if (u8ExtendColorimetry == 4)
            {
                return E_CFD_CFIO_ADOBE_RGB;
            }
            else if (u8ExtendColorimetry == 5)
            {
                return E_CFD_CFIO_RGB_BT2020;
            }
            else if (u8ExtendColorimetry == 6)
            {
                return E_CFD_CFIO_RGB_BT2020;
            }
            else
            {
                return E_CFD_CFIO_RESERVED_START;
            }
        }
    }

    return E_CFD_CFIO_RESERVED_START;
}

E_CFD_MC_FORMAT MHal_XC_HDMI_Color_Data_Format(MS_U8 u8PixelFormat)
{
    if (u8PixelFormat == 1)
    {
        return E_CFD_MC_FORMAT_YUV422;
    }
    else if (u8PixelFormat == 2)
    {
        return  E_CFD_MC_FORMAT_YUV444;
    }
    else if (u8PixelFormat == 3)
    {
        return E_CFD_MC_FORMAT_YUV420;
    }
    else
    {
        return E_CFD_MC_FORMAT_RGB;
    }
}

MS_U16 MHal_XC_CFD_GetInitParam(ST_KDRV_XC_CFD_INIT *pstCfdInit)
{
    if (pstCfdInit == NULL)
    {
        return E_CFD_MC_ERR_INPUT_MAIN_CONTROLS;
    }

    memcpy (pstCfdInit, &_stCfdInit[pstCfdInit->u8Win], sizeof(ST_KDRV_XC_CFD_INIT));
    return E_CFD_MC_ERR_NOERR;
}

MS_U16 MHal_XC_CFD_GetHdmiParam(ST_KDRV_XC_CFD_HDMI *pstCfdHdmi)
{
    if (pstCfdHdmi == NULL)
    {
        return E_CFD_MC_ERR_INPUT_MAIN_CONTROLS;
    }

    memcpy (pstCfdHdmi, &_stCfdHdmi[pstCfdHdmi->u8Win], sizeof(ST_KDRV_XC_CFD_HDMI));
    return E_CFD_MC_ERR_NOERR;
}

MS_U16 MHal_XC_CFD_SetPanelParam(ST_KDRV_XC_CFD_PANEL *pstCfdPanel)
{
    STU_CFDAPI_PANEL_FORMAT stPanelParam;
    MS_U16 u16RetVal = 0;
    memset(&stPanelParam, 0, sizeof(STU_CFDAPI_PANEL_FORMAT));
    stPanelParam.u32Version = 0;
    stPanelParam.u16Length = sizeof(STU_CFDAPI_PANEL_FORMAT);
    stPanelParam.u16Panel_Med_Luminance = pstCfdPanel->u16MedLuminance;          //data * 1 nits
    stPanelParam.u16Panel_Max_Luminance = pstCfdPanel->u16MaxLuminance;          //data * 1 nits
    stPanelParam.u16Panel_Min_Luminance = pstCfdPanel->u16MinLuminance;          //data * 0.0001 nits
    stPanelParam.stu_Cfd_Panel_ColorMetry.u16Display_Primaries_x[0] = pstCfdPanel->u16Display_Primaries_x[0];
    stPanelParam.stu_Cfd_Panel_ColorMetry.u16Display_Primaries_x[1] = pstCfdPanel->u16Display_Primaries_x[1];
    stPanelParam.stu_Cfd_Panel_ColorMetry.u16Display_Primaries_x[2] = pstCfdPanel->u16Display_Primaries_x[2];
    stPanelParam.stu_Cfd_Panel_ColorMetry.u16Display_Primaries_y[0] = pstCfdPanel->u16Display_Primaries_y[0];
    stPanelParam.stu_Cfd_Panel_ColorMetry.u16Display_Primaries_y[1] = pstCfdPanel->u16Display_Primaries_y[1];
    stPanelParam.stu_Cfd_Panel_ColorMetry.u16Display_Primaries_y[2] = pstCfdPanel->u16Display_Primaries_y[2];
    stPanelParam.stu_Cfd_Panel_ColorMetry.u16White_point_x = pstCfdPanel->u16White_point_x;
    stPanelParam.stu_Cfd_Panel_ColorMetry.u16White_point_y= pstCfdPanel->u16White_point_y;

    u16RetVal = Mapi_Cfd_inter_PANEL_Param_Check(&stPanelParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_inter_PANEL_Param_Set(&stPanelParam);
    }
    else
    {
        printk("Mapi_Cfd_inter_PANEL_Param_Check fail, errCode: %d\n", u16RetVal);
    }

    return u16RetVal;
}

MS_U16 MHal_XC_CFD_SetEdidParam(ST_KDRV_XC_CFD_EDID *pstCfdEdid)
{
    STU_CFDAPI_HDMI_EDID_PARSER stEdidParam;
    MS_U16 u16RetVal = 0;
    memset(&stEdidParam, 0, sizeof(STU_CFDAPI_HDMI_EDID_PARSER));

    stEdidParam.u32Version = CFD_HDMI_EDID_ST_VERSION;
    stEdidParam.u16Length = sizeof(STU_CFDAPI_HDMI_EDID_PARSER);
    stEdidParam.u8HDMISink_HDRData_Block_Valid = pstCfdEdid->u8HDMISinkHDRDataBlockValid;
    stEdidParam.u8HDMISink_EOTF = pstCfdEdid->u8HDMISinkEOTF;
    stEdidParam.u8HDMISink_SM = pstCfdEdid->u8HDMISinkSM;
    stEdidParam.u8HDMISink_Desired_Content_Max_Luminance = pstCfdEdid->u8HDMISinkDesiredContentMaxLuminance;
    stEdidParam.u8HDMISink_Desired_Content_Max_Frame_Avg_Luminance = pstCfdEdid->u8HDMISinkDesiredContentMaxFrameAvgLuminance;
    stEdidParam.u8HDMISink_Desired_Content_Min_Luminance = pstCfdEdid->u8HDMISinkDesiredContentMinLuminance;
    stEdidParam.u8HDMISink_HDRData_Block_Length = pstCfdEdid->u8HDMISinkHDRDataBlockLength;
    stEdidParam.stu_Cfd_HDMISink_Panel_ColorMetry.u16Display_Primaries_x[0] = pstCfdEdid->u16Display_Primaries_x[0];
    stEdidParam.stu_Cfd_HDMISink_Panel_ColorMetry.u16Display_Primaries_x[1] = pstCfdEdid->u16Display_Primaries_x[1];
    stEdidParam.stu_Cfd_HDMISink_Panel_ColorMetry.u16Display_Primaries_x[2] = pstCfdEdid->u16Display_Primaries_x[2];
    stEdidParam.stu_Cfd_HDMISink_Panel_ColorMetry.u16Display_Primaries_y[0] = pstCfdEdid->u16Display_Primaries_y[0];
    stEdidParam.stu_Cfd_HDMISink_Panel_ColorMetry.u16Display_Primaries_y[1] = pstCfdEdid->u16Display_Primaries_y[1];
    stEdidParam.stu_Cfd_HDMISink_Panel_ColorMetry.u16Display_Primaries_y[2] = pstCfdEdid->u16Display_Primaries_y[2];
    stEdidParam.stu_Cfd_HDMISink_Panel_ColorMetry.u16White_point_x = pstCfdEdid->u16White_point_x;
    stEdidParam.stu_Cfd_HDMISink_Panel_ColorMetry.u16White_point_y = pstCfdEdid->u16White_point_y;
    stEdidParam.u8HDMISink_EDID_base_block_version = pstCfdEdid->u8HDMISinkEDIDBaseBlockVersion;
    stEdidParam.u8HDMISink_EDID_base_block_reversion = pstCfdEdid->u8HDMISinkEDIDBaseBlockReversion;
    stEdidParam.u8HDMISink_EDID_CEA_block_reversion = pstCfdEdid->u8HDMISinkEDIDCEABlockReversion;
    stEdidParam.u8HDMISink_VCDB_Valid = pstCfdEdid->u8HDMISinkVCDBValid;
    stEdidParam.u8HDMISink_Support_YUVFormat = pstCfdEdid->u8HDMISinkSupportYUVFormat;
    stEdidParam.u8HDMISink_Extended_Colorspace = pstCfdEdid->u8HDMISinkExtendedColorspace;
    stEdidParam.u8HDMISink_EDID_Valid = pstCfdEdid->u8HDMISinkEDIDValid;

    u16RetVal = Mapi_Cfd_inter_HDMI_EDID_Param_Check(&stEdidParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_inter_HDMI_EDID_Param_Set(&stEdidParam);
    }
    else
    {
        printk("Mapi_Cfd_inter_HDMI_EDID_Param_Check fail, errCode: %d\n", u16RetVal);
    }

    return u16RetVal;
}

MS_U16 MHal_XC_CFD_SetOsdParam(ST_KDRV_XC_CFD_OSD *pstCfdOsd )
{
    STU_CFDAPI_OSD_CONTROL stOSDParam;
    MS_U16 u16RetVal = 0;
    memset(&stOSDParam, 0, sizeof(STU_CFDAPI_OSD_CONTROL));

    stOSDParam.u32Version = 0;
    stOSDParam.u16Length = sizeof(STU_CFDAPI_OSD_CONTROL);
    if (pstCfdOsd->bSkipPictureSetting == TRUE)
    {
        stOSDParam.u16Hue = 50;
        stOSDParam.u16Saturation = 128;
    }
    else
    {
        stOSDParam.u16Hue = pstCfdOsd->u16Hue;
        stOSDParam.u16Saturation = pstCfdOsd->u16Saturation;
    }
    stOSDParam.u16Contrast = pstCfdOsd->u16Contrast;
    stOSDParam.s32ColorCorrectionMatrix[0][0] = pstCfdOsd->s16ColorCorrectionMatrix[0];
    stOSDParam.s32ColorCorrectionMatrix[0][1] = pstCfdOsd->s16ColorCorrectionMatrix[1];
    stOSDParam.s32ColorCorrectionMatrix[0][2] = pstCfdOsd->s16ColorCorrectionMatrix[2];
    stOSDParam.s32ColorCorrectionMatrix[1][0] = pstCfdOsd->s16ColorCorrectionMatrix[3];
    stOSDParam.s32ColorCorrectionMatrix[1][1] = pstCfdOsd->s16ColorCorrectionMatrix[4];
    stOSDParam.s32ColorCorrectionMatrix[1][2] = pstCfdOsd->s16ColorCorrectionMatrix[5];
    stOSDParam.s32ColorCorrectionMatrix[2][0] = pstCfdOsd->s16ColorCorrectionMatrix[6];
    stOSDParam.s32ColorCorrectionMatrix[2][1] = pstCfdOsd->s16ColorCorrectionMatrix[7];
    stOSDParam.s32ColorCorrectionMatrix[2][2] = pstCfdOsd->s16ColorCorrectionMatrix[8];

    stOSDParam.u8ColorCorrection_En = 1;
    stOSDParam.u8OSD_UI_En = pstCfdOsd->u8OSDUIEn;
    stOSDParam.u8OSD_UI_Mode = pstCfdOsd->u8OSDUIMode;

    Mapi_Cfd_OSD_H2SUI_Get(&(stOSDParam.u8HDR_UI_H2SMode));

    u16RetVal = Mapi_Cfd_inter_OSD_Param_Check(&stOSDParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_inter_OSD_Param_Set(&stOSDParam);
    }
    else
    {
        printk("Mapi_Cfd_inter_OSD_Param_Check fail, errCode: %d\n", u16RetVal);
    }
    return u16RetVal;
}

MS_U16 MHal_XC_CFD_InitIp(ST_KDRV_XC_CFD_FIRE *pstCfdFire)
{
    MS_U16 u16RetVal = 0;
    MS_U8 u8HWMainSubMode = 0;
    ST_KDRV_XC_CFD_HW_IPS stHwIpsParam;
    memset(&stHwIpsParam, 0, sizeof(ST_KDRV_XC_CFD_HW_IPS));
    u8HWMainSubMode = pstCfdFire->u8Win;

    Mapi_Cfd_Maserati_DLCIP_Param_Init(&stHwIpsParam.stDLCInput.stMaseratiDLCParam);
    Mapi_Cfd_Maserati_TMOIP_Param_Init(&stHwIpsParam.stTMOInput.stMaseratiTMOParam);
    Mapi_Cfd_Maserati_SDRIP_Param_Init(&stHwIpsParam.stSDRIPInput.stMaseratiSDRIPParam);
    Mapi_Cfd_Maserati_HDRIP_Param_Init(&stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam);
    Mapi_Cfd_Maserati_LGEAPI_Param_Init(&stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam, &stHwIpsParam.stSDRIPInput.stMaseratiSDRIPParam);

    if(CFD_IS_MM(pstCfdFire->u8InputSource) || CFD_IS_DTV(pstCfdFire->u8InputSource)) // MM and DTV source
    {
        ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info stFormatInfo;
        MHal_XC_CFD_WithdrawMMParam(&stFormatInfo, pstCfdFire->u8Win);
        if (MHal_XC_GetDolbyStatus() && (_stCfdHdr[pstCfdFire->u8Win].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_DOLBY))
        {
            //stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam.u8HDR_enable_Mode = 0xC0;
            stHwIpsParam.stTMOInput.stMaseratiTMOParam.u8HDR_TMO_curve_enable_Mode = 0xC0;
        }
        else if((MHal_XC_GetDolbyStatus() == 0) && (MHal_XC_GetTCHHDRStatus()))
        {
            stHwIpsParam.stTMOInput.stMaseratiTMOParam.u8HDR_TMO_curve_enable_Mode = 0xC0;
        }
        else if ((MHal_XC_GetDolbyStatus() == 0) && (_stCfdHdr[pstCfdFire->u8Win].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_OPEN)
                 && ((stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.bVUIValid == TRUE) && (stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics == 16)))
        {
            //stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam.u8HDR_enable_Mode = 0xC0;
            stHwIpsParam.stTMOInput.stMaseratiTMOParam.u8HDR_TMO_curve_enable_Mode = 0xC0;
        }
        else if ((MHal_XC_GetDolbyStatus() == 0) && (_stCfdHdr[pstCfdFire->u8Win].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_HLG)
                 && ((stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.bVUIValid == TRUE) && (stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics == 18)))
        {
            //stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam.u8HDR_enable_Mode = 0xC0;
            stHwIpsParam.stTMOInput.stMaseratiTMOParam.u8HDR_TMO_curve_enable_Mode = 0xC0;
        }
        else
        {
            //stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam.u8HDR_enable_Mode = 0x40;
            stHwIpsParam.stTMOInput.stMaseratiTMOParam.u8HDR_TMO_curve_enable_Mode = 0x40;
        }
    }
    else if(CFD_IS_HDMI(pstCfdFire->u8InputSource)) // HDMI source
    {
        if(((_stCfdHdmi[pstCfdFire->u8Win].u8EOTF == 2) && (_stCfdHdr[pstCfdFire->u8Win].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_OPEN))  // if HDMI content is HDR10, we nedd flow UI option to on/off.
           || ((_stCfdHdmi[pstCfdFire->u8Win].u8EOTF == 3) && (_stCfdHdr[pstCfdFire->u8Win].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_HLG)) // if HDMI content is HLG, we nedd flow UI option to on/off.
           || ((_stCfdHdmi[pstCfdFire->u8Win].u8EOTF != 2) && (_stCfdHdmi[pstCfdFire->u8Win].u8EOTF != 3))) // HDMI content is not HDR10 nor HLG,always set HDR ip enable (TMO bypass)
        {
            //stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam.u8HDR_enable_Mode = 0xC0;
            stHwIpsParam.stTMOInput.stMaseratiTMOParam.u8HDR_TMO_curve_enable_Mode = 0xC0;
        }
        else
        {
            //stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam.u8HDR_enable_Mode = 0x40;
            stHwIpsParam.stTMOInput.stMaseratiTMOParam.u8HDR_TMO_curve_enable_Mode = 0x40;
        }
    }
    else // otherwise input source
    {
        //stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam.u8HDR_enable_Mode = 0x40;
        stHwIpsParam.stTMOInput.stMaseratiTMOParam.u8HDR_TMO_curve_enable_Mode = 0x40;
    }

    u16RetVal = Mapi_Cfd_Maserati_TMOIP_Param_Check(&stHwIpsParam.stTMOInput.stMaseratiTMOParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_Maserati_TMOIP_Param_Set(u8HWMainSubMode, &stHwIpsParam.stTMOInput.stMaseratiTMOParam);
    }
    else
    {
        printk("Mapi_Cfd_Maserati_TMOIP_Param_Check fail, errCode: %d\n", u16RetVal);
        return u16RetVal;
    }

    u16RetVal = Mapi_Cfd_Maserati_DLCIP_Param_Check(&stHwIpsParam.stDLCInput.stMaseratiDLCParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_Maserati_DLCIP_Param_Set(u8HWMainSubMode, &stHwIpsParam.stDLCInput.stMaseratiDLCParam);
    }
    else
    {
        printk("Mapi_Cfd_Maserati_DLCIP_Param_Check fail, errCode: %d\n", u16RetVal);
        return u16RetVal;
    }

    u16RetVal = Mapi_Cfd_Maserati_SDRIP_Param_Check(&stHwIpsParam.stSDRIPInput.stMaseratiSDRIPParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_Maserati_SDRIP_Param_Set(u8HWMainSubMode, &stHwIpsParam.stSDRIPInput.stMaseratiSDRIPParam);
    }
    else
    {
        printk("Mapi_Cfd_Maserati_SDRIP_Param_Check fail, errCode: %d\n", u16RetVal);
        return u16RetVal;
    }

    u16RetVal = Mapi_Cfd_Maserati_HDRIP_Param_Check(&stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_Maserati_HDRIP_Param_Set(u8HWMainSubMode, &stHwIpsParam.stHDRIPInput.stMaseratiHDRIPParam);
    }
    else
    {
        printk("Mapi_Cfd_Maserati_HDRIP_Param_Check fail, errCode: %d\n", u16RetVal);
        return u16RetVal;
    }

    return u16RetVal;
}

BOOL MHal_XC_EnableHDRCLK(MS_BOOL bEnable, MS_BOOL bImmediate)
{
    if (_bEnableHDRCLK == bEnable)
        return TRUE;

    _bEnableHDRCLK = bEnable;

    if (bEnable)
    {
        // hdr clk setting enable
        if (bImmediate)
        {
            if(IS_DOLBY_HDR(MAIN_WINDOW))
            {
                //MHal_XC_SetHDR_DMARequestOFF(DISABLE, bImmediate); //Move DMARequest control to init by sourcetype
            }

            MHal_XC_W2BYTE(REG_SC_BK79_02_L, 0);
            MHal_XC_W2BYTEMSK(REG_SC_BK79_7E_L, BIT(1)|BIT(0), BIT(1)|BIT(0));
        }
        else
        {
            if(IS_DOLBY_HDR(MAIN_WINDOW))
            {
                //MHal_XC_SetHDR_DMARequestOFF(DISABLE, bImmediate); //Move DMARequest control to init by sourcetype
            }
            KApi_XC_MLoad_WriteCmd(E_CLIENT_MAIN_HDR, REG_SC_BK79_02_L, 0, 0xFFFF);
            KApi_XC_MLoad_WriteCmd(E_CLIENT_MAIN_HDR, REG_SC_BK79_7E_L, BIT(1)|BIT(0), BIT(1)|BIT(0));
        }
    }
    else
    {
        // hdr clk setting disable
        if (bImmediate)
        {
            //MHal_XC_SetHDR_DMARequestOFF(ENABLE, bImmediate); //Move DMARequest control to init by sourcetype
            MHal_XC_W2BYTE(REG_SC_BK79_02_L, 0xFFFF);
            MHal_XC_W2BYTEMSK(REG_SC_BK79_7E_L, 0, BIT(1)|BIT(0));
        }
        else
        {
            //MHal_XC_SetHDR_DMARequestOFF(ENABLE, bImmediate); //Move DMARequest control to init by sourcetype
            KApi_XC_MLoad_WriteCmd(E_CLIENT_MAIN_HDR, REG_SC_BK79_02_L, 0xFFFF, 0xFFFF);
            KApi_XC_MLoad_WriteCmd(E_CLIENT_MAIN_HDR, REG_SC_BK79_7E_L, 0, BIT(1)|BIT(0));
        }
    }

    return TRUE;
}

void MHal_XC_CFD_SetTmoVal(void)
{
    memset(_au16TmoVals, 0, sizeof(_au16TmoVals));
    //array[0, 12] = 0 defalut, don't use
    memcpy(_au16TmoVals + 1, u16DumySource, sizeof(u16DumySource));
    memcpy(_au16TmoVals + 13, u16DumyTarget, sizeof(u16DumyTarget));
}

BOOL MHal_XC_CFD_IsTmoChanged(void)
{
    if ((memcmp(_au16TmoVals + 1, u16DumySource, sizeof(u16DumySource)) == 0) &&
        (memcmp(_au16TmoVals + 13, u16DumyTarget, sizeof(u16DumyTarget)) == 0))
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

MS_U16 MHal_XC_CFD_SetMmParam_FromTCH(STU_CFDAPI_MM_PARSER *pstMMParam, STU_CFDAPI_MM_PARSER *pPrimeSingle_result)
{
    MS_U16 u16RetVal = 0;

    if ((pstMMParam == NULL) || (pPrimeSingle_result == NULL))
    {
        return -1;
    }

    memcpy(pstMMParam, pPrimeSingle_result, sizeof(STU_CFDAPI_MM_PARSER));

    return u16RetVal;
}

MS_U16 MHal_XC_CFD_SetMainCtrl(ST_KDRV_XC_CFD_FIRE *pstCfdFire)
{
    STU_CFDAPI_MAIN_CONTROL stMainControl;
    MS_U16 u16RetVal = 0;
    MS_U16 u16RetVal2 = 0;
    MS_U8 u8TmoLevel = 0;
    memset(&stMainControl, 0, sizeof(STU_CFDAPI_MAIN_CONTROL));

    stMainControl.u32Version = 0;
    stMainControl.u16Length = sizeof(STU_CFDAPI_MAIN_CONTROL);
    stMainControl.u8Input_Source = pstCfdFire->u8InputSource;
    u8HDMI_case = pstCfdFire->u8InputSource;

    if (CFD_IS_HDMI(pstCfdFire->u8InputSource))
    {
        // HDMI
        stMainControl.u16DolbySupportStatus = !MHal_XC_DolbyHWBonded();     //check is support DolbyHDR
        stMainControl.u8Input_Format = MHal_XC_HDMI_Color_Format(_stCfdHdmi[pstCfdFire->u8Win].u8PixelFormat, _stCfdHdmi[pstCfdFire->u8Win].u8Colorimetry, _stCfdHdmi[pstCfdFire->u8Win].u8ExtendedColorimetry);
        stMainControl.u8Input_DataFormat = MHal_XC_HDMI_Color_Data_Format(_stCfdHdmi[pstCfdFire->u8Win].u8PixelFormat);
        if ((_stCfdOsd[pstCfdFire->u8Win].u8UltraWhiteLevel == 0) && (_stCfdOsd[pstCfdFire->u8Win].u8UltraBlackLevel == 0))
        {
            if (_stCfdOsd[pstCfdFire->u8Win].u8ColorRange == 0)
            {
                stMainControl.u8Input_IsFullRange = _stCfdHdmi[pstCfdFire->u8Win].bIsFullRange;
            }
            else
            {
                stMainControl.u8Input_IsFullRange = (_stCfdOsd[pstCfdFire->u8Win].u8ColorRange==1)?(1):(0);
            }
        }
        else
        {
            stMainControl.u8Input_IsFullRange =  1;
        }

        if (MHal_XC_GetDolbyStatus())
        {
            //Dolby HDR
            stMainControl.u8Input_HDRMode = E_CFIO_MODE_HDR1;
        }
        else
        {
            if (_stCfdHdmi[pstCfdFire->u8Win].u8EOTF == 2)
            {
                // HDR10
                stMainControl.u8Input_HDRMode = E_CFIO_MODE_HDR2;
            }
            else if (_stCfdHdmi[pstCfdFire->u8Win].u8EOTF == 3)
            {
                // HLG
                stMainControl.u8Input_HDRMode = E_CFIO_MODE_HDR3;
            }
            else
            {
                stMainControl.u8Input_HDRMode = E_CFIO_MODE_SDR;
            }
        }
    }
    else if (CFD_IS_MM(pstCfdFire->u8InputSource) || CFD_IS_DTV(pstCfdFire->u8InputSource))
    {
        // MM/DTV
        STU_CFDAPI_MM_PARSER stMMParam;
        ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info stFormatInfo;
        memset(&stMMParam, 0, sizeof(STU_CFDAPI_MM_PARSER));

        MHal_XC_CFD_WithdrawMMParam(&stFormatInfo, pstCfdFire->u8Win);
        _MHal_XC_CFD_Convert_XCDRAM_to_CFDMMParam(pstCfdFire, &stMMParam,&stFormatInfo);

        //printk("[%s][%d][TCH] IsMVOP_TCH:%d bUpdateCFDPara_from_TCH:%d  \n" ,__FUNCTION__,__LINE__,IsMVOP_TCH,bUpdateCFDPara_from_TCH);

        //update CFD's MM parameters
        if(IsMVOP_TCH)
        {
            if (bUpdateCFDPara_from_TCH)
            {
                u16RetVal2 = MHal_XC_CFD_SetMmParam_FromTCH(&stMMParam,&PrimeSingle_result);

                if (u16RetVal2 != E_CFD_MC_ERR_NOERR)
                {
                    printk("[TCH] update TCH MM param fail, errCode: %d\n", u16RetVal2);
                }
            }
            else
            {
                printk("[TCH] MM parameters not be updated from TCH driver. \n");
            }
        }

        stMainControl.u8Input_Format = 7;
        stMainControl.u8Input_DataFormat = 2;
        if (_stCfdOsd[pstCfdFire->u8Win].u8ColorRange == 0)
        {
            stMainControl.u8Input_IsFullRange = stMMParam.u8Video_Full_Range_Flag;
        }
        else
        {
            stMainControl.u8Input_IsFullRange = (_stCfdOsd[pstCfdFire->u8Win].u8ColorRange==1)?(1):(0);
        }

        if (MHal_XC_GetDolbyStatus())
        {
            //Dolby HDR
            stMainControl.u8Input_HDRMode = E_CFIO_MODE_HDR1;
        }
        else if(MHal_XC_GetTCHHDRStatus())
        {
            stMainControl.u8Input_HDRMode = E_CFIO_MODE_HDR4;
        }
        else
        {
            if (stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.bVUIValid == TRUE)
            {
                if (stMMParam.u8Transfer_Characteristics == 16)
                {
                    // HDR10
                    stMainControl.u8Input_HDRMode = E_CFIO_MODE_HDR2;
                }
                else if (stMMParam.u8Transfer_Characteristics == 18)
                {
                    // HLG
                    stMainControl.u8Input_HDRMode = E_CFIO_MODE_HDR3;
                }
                else
                {
                    stMainControl.u8Input_HDRMode = E_CFIO_MODE_SDR;
                }
            }
            else
            {
                stMainControl.u8Input_HDRMode = E_CFIO_MODE_SDR;
            }
        }
        // HDR/SDR flag be not controlled by the customer flow when DTV seamless
        // instead, controlled by DTV metadata.
        if( ((MHal_XC_Get_SharedMemVersion(0) != getOpenHDRSeamlessVersion()) && (E_SEAMLESS_MVOPSOURCE_WITH_FREEZE != MHal_XC_Get_HDRSeamless_MVOPSOURCE_Path())) ||
            ( !(CFD_IS_DTV(pstCfdFire->u8InputSource)||CFD_IS_MM(pstCfdFire->u8InputSource) ) ))
        {
            if((stMainControl.u8Input_HDRMode==E_CFIO_MODE_HDR2) && (IS_OPEN_HDR(pstCfdFire->u8Win)==0))
            {
                stMainControl.u8Input_HDRMode = E_CFIO_MODE_SDR;
            }
            else if((stMainControl.u8Input_HDRMode==E_CFIO_MODE_HDR3) && (IS_HLG_HDR(pstCfdFire->u8Win)==0))
            {
                stMainControl.u8Input_HDRMode = E_CFIO_MODE_SDR;
            }
        }
    }
    else
    {
        // Analog , wait Dixon modify
        stMainControl.u8Input_Format = _stCfdAnalog[pstCfdFire->u8Win].u8ColorFormat;
        stMainControl.u8Input_DataFormat = _stCfdAnalog[pstCfdFire->u8Win].u8ColorDataFormat;
        if (_stCfdOsd[pstCfdFire->u8Win].u8ColorRange == 0)
        {
            stMainControl.u8Input_IsFullRange = _stCfdAnalog[pstCfdFire->u8Win].bIsFullRange;
        }
        else
        {
            stMainControl.u8Input_IsFullRange = (_stCfdOsd[pstCfdFire->u8Win].u8ColorRange==1)?(1):(0);
        }
        stMainControl.u8Input_ext_Colour_primaries = _stCfdAnalog[pstCfdFire->u8Win].u8ColorPrimaries;
        stMainControl.u8Input_ext_Transfer_Characteristics = _stCfdAnalog[pstCfdFire->u8Win].u8TransferCharacteristics;
        stMainControl.u8Input_ext_Matrix_Coeffs = _stCfdAnalog[pstCfdFire->u8Win].u8MatrixCoefficients;
        stMainControl.u8Input_HDRMode = E_CFIO_MODE_SDR;
    }

    //stMainControl.u8Input_HDRMode = _stCfdHdr[pstCfdFire->u8Win].u8HdrType;
    stMainControl.u8Input_IsRGBBypass = pstCfdFire->bIsRgbBypass;
    stMainControl.u8Input_SDRIPMode = 1;
    stMainControl.u8Input_HDRIPMode = 1;
    /* No idea.
    stMainControl.stu_Middle_Format[0].u8Mid_Format_Mode = ;
    stMainControl.stu_Middle_Format[0].u8Mid_Format =;
    stMainControl.stu_Middle_Format[0].u8Mid_DataFormat = ;
    stMainControl.stu_Middle_Format[0].u8Mid_IsFullRange = ;
    stMainControl.stu_Middle_Format[0].u8Mid_HDRMode = ;
    stMainControl.stu_Middle_Format[0].u8Mid_Colour_primaries = ;
    stMainControl.stu_Middle_Format[0].u8Mid_Transfer_Characteristics = ;
    stMainControl.stu_Middle_Format[0].u8Mid_Matrix_Coeffs = ;
    */
    //_eCfdOutputType = E_CFD_OUTPUT_SOURCE_PANEL;

    if (_stCfdTmo.u16Length > 0)
    {
        MS_U8 i, j;

        u8TmoLevel = _stCfdHdr[pstCfdFire->u8Win].u8TmoLevel;

        if(u8TmoLevel >= _stCfdTmo.u16LevelCount)
        {
            printk("[%s %d]ERROR level is invalid\n", __FUNCTION__,__LINE__);
        }

        if(stMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2)
        {
            for(i=0; i<_stCfdTmo.u16LevelCount; i++)
            {
                if((E_KDRV_XC_CFD_HDR_TYPE_OPEN == (_stCfdTmo.pstCfdTmoLevel + i)->u16HdrType) && u8TmoLevel == (_stCfdTmo.pstCfdTmoLevel + i)->u16Level)
                {
                    if((_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize <= 48)    //12*2*2 = 48  just support less 12 points control TMO
                    {
                        MHal_XC_W2BYTEMSK(REG_SC_BK4E_21_L,0x0003, 0x0003);

                        for(j=0; j<(_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize/4 - 1; j++)
                        {
                            // each point data is MS_U16, source data is before target data;
                            u16DumySource[j] = (*((_stCfdTmo.pstCfdTmoLevel + i)->pu8data + 2*j + 2) << 8) |  *((_stCfdTmo.pstCfdTmoLevel + i)->pu8data + 2*j + 3);
                            u16DumyTarget[j] = (*((_stCfdTmo.pstCfdTmoLevel + i)->pu8data + 2*j + 2 + (_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize/2) << 8) |  *((_stCfdTmo.pstCfdTmoLevel + i)->pu8data + 2*j + 3 + (_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize/2);
                        }
                        break;
                    }
                }
            }
        }
        else if(stMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3)
        {
            for(i=0; i<_stCfdTmo.u16LevelCount; i++)
            {
                if((E_KDRV_XC_CFD_HDR_TYPE_HLG == (_stCfdTmo.pstCfdTmoLevel + i)->u16HdrType) && u8TmoLevel == (_stCfdTmo.pstCfdTmoLevel + i)->u16Level)
                {
                    if((_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize <= 48)    //12*2*2 = 48  just support less 12 points control TMO
                    {
                        MHal_XC_W2BYTEMSK(REG_SC_BK4E_21_L,0x0003, 0x0003);

                        for(j=0; j<(_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize/4 - 1; j++)
                        {
                            // each point data is MS_U16, source data is before target data;
                            u16DumySource[j] = (*((_stCfdTmo.pstCfdTmoLevel + i)->pu8data + 2*j + 2) << 8) |  *((_stCfdTmo.pstCfdTmoLevel + i)->pu8data + 2*j + 3);
                            u16DumyTarget[j] = (*((_stCfdTmo.pstCfdTmoLevel + i)->pu8data + 2*j + 2 + (_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize/2) << 8) |  *((_stCfdTmo.pstCfdTmoLevel + i)->pu8data + 2*j + 3 + (_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize/2);
                        }
                        break;
                    }
                }
            }
        }
    }

    if (_eCfdOutputType == E_CFD_OUTPUT_SOURCE_HDMI)
    {
        stMainControl.u8Output_Source = E_CFD_OUTPUT_SOURCE_HDMI;
        stMainControl.u8Output_Format = E_CFD_CFIO_RGB_BT709;
        stMainControl.u8Output_DataFormat = E_CFD_MC_FORMAT_RGB;
        stMainControl.u8Output_IsFullRange = 1;
        stMainControl.u8Output_HDRMode = E_CFIO_MODE_SDR;
        stMainControl.u8PanelOutput_GammutMapping_Mode = 0;
        stMainControl.u8HDMIOutput_GammutMapping_Mode = 1;
    }
    else
    {
        stMainControl.u8Output_Source = E_CFD_OUTPUT_SOURCE_PANEL;
        stMainControl.u8Output_Format = _stCfdPanel.u8ColorFormat;
        stMainControl.u8Output_DataFormat = _stCfdPanel.u8ColorDataFormat;
        stMainControl.u8Output_IsFullRange = _stCfdPanel.bIsFullRange;
        stMainControl.u8Output_HDRMode = E_CFIO_MODE_SDR;
        if (stMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR1)
        {
            stMainControl.u8PanelOutput_GammutMapping_Mode = 0;
        }
        else if ((stMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) || (stMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR4))
        {
            stMainControl.u8PanelOutput_GammutMapping_Mode = _stCfdPanel.bLinearRgb;
        }
        else
        {
            stMainControl.u8PanelOutput_GammutMapping_Mode = _stCfdPanel.bLinearRgb & _stCfdLinearRgb.bEnable;
        }
        stMainControl.u8HDMIOutput_GammutMapping_Mode = 0;
    }

    stMainControl.u8HDMIOutput_GammutMapping_MethodMode = 0;
    stMainControl.u8MMInput_ColorimetryHandle_Mode = 0;
    stMainControl.u8TMO_TargetRefer_Mode = 1;
    stMainControl.u16Source_Max_Luminance = 0;    //data * 1 nits
    stMainControl.u16Source_Med_Luminance = 0;   //data * 1 nits
    stMainControl.u16Source_Min_Luminance = 0;    //data * 0.0001 nits
    stMainControl.u16Target_Med_Luminance = 0;   //data * 1 nits
    stMainControl.u16Target_Max_Luminance = 0;    //data * 1 nits
    stMainControl.u16Target_Min_Luminance = 0;    //data * 0.0001 nits

    u16RetVal = Mapi_Cfd_inter_Main_Control_Param_Check(&stMainControl);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_inter_Main_Control_Param_Set(&stMainControl);
        _stCfdMainControl = stMainControl;
    }
    else
    {
        printk("Mapi_Cfd_inter_Main_Control_Param_Check fail, errCode: %d\n", u16RetVal);
    }

    return u16RetVal;
}

MS_S32 MHal_XC_CFD_DepositeMMParam(ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info *pstFormatInfo, MS_U8 u8Win)
{
    if (pstFormatInfo==NULL)
    {
        return -1;
    }

    memcpy(&gstDRAMFormatInfo[u8Win], pstFormatInfo, sizeof(ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info));
    return E_CFD_MC_ERR_NOERR;
}

MS_S32 MHal_XC_CFD_WithdrawMMParam(ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info *pstFormatInfo, MS_U8 u8Win)
{
    if (pstFormatInfo == NULL)
    {
        return -1;
    }
    memcpy(pstFormatInfo, &gstDRAMFormatInfo[u8Win], sizeof(ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info));

    return E_CFD_MC_ERR_NOERR;
}

MS_U16 MHal_XC_CFD_SetMmParam(ST_KDRV_XC_CFD_FIRE *pstCfdFire, MS_BOOL *pbChanged)
{
    STU_CFDAPI_MM_PARSER stMMParam;
    ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info stFormatInfo;
    MS_U16 u16RetVal = 0;
    MS_U16 u16RetVal2 = 0;
    memset(&stMMParam, 0, sizeof(STU_CFDAPI_MM_PARSER));

    MHal_XC_CFD_WithdrawMMParam(&stFormatInfo, pstCfdFire->u8Win);
    _MHal_XC_CFD_Convert_XCDRAM_to_CFDMMParam(pstCfdFire, &stMMParam,&stFormatInfo);

    //printk("[%s][%d][TCH] IsMVOP_TCH:%d bUpdateCFDPara_from_TCH:%d  \n" ,__FUNCTION__,__LINE__,IsMVOP_TCH,bUpdateCFDPara_from_TCH);

    if(IsMVOP_TCH)
    {
        //update CFD's MM parameters
        if (bUpdateCFDPara_from_TCH)
        {
            u16RetVal2 = MHal_XC_CFD_SetMmParam_FromTCH(&stMMParam,&PrimeSingle_result);

            if (u16RetVal2 != E_CFD_MC_ERR_NOERR)
            {
                printk("[TCH] update TCH MM param fail, errCode: %d\n", u16RetVal2);
            }
        }
        else
        {
            printk("[TCH] MM parameters not be updated from TCH driver. \n");
        }
    }

    *pbChanged = FALSE;
    if (memcmp(&stMMParam, &_stCfdMm[pstCfdFire->u8Win], sizeof(STU_CFDAPI_MM_PARSER)) != 0)
    {
        *pbChanged = TRUE;
    }
    memcpy(&_stCfdMm[pstCfdFire->u8Win], &stMMParam, sizeof(STU_CFDAPI_MM_PARSER));

    u16RetVal = Mapi_Cfd_inter_MM_Param_Check(&stMMParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_inter_MM_Param_Set(&stMMParam);
    }
    else
    {
        printk("Mapi_Cfd_inter_MM_Param_Check fail, errCode: %d\n", u16RetVal);
    }

    return u16RetVal;
}

MS_U16 MHal_XC_CFD_SetHdmiParam(ST_KDRV_XC_CFD_FIRE *pstCfdFire)
{
    STU_CFDAPI_HDMI_INFOFRAME_PARSER stHDMIInfoFrameParam;
    MS_U16 u16RetVal = 0;
    memset(&stHDMIInfoFrameParam, 0, sizeof(STU_CFDAPI_HDMI_INFOFRAME_PARSER));
    stHDMIInfoFrameParam.u32Version = 0;
    stHDMIInfoFrameParam.u16Length = sizeof(STU_CFDAPI_HDMI_INFOFRAME_PARSER);
    stHDMIInfoFrameParam.u8HDMISource_HDR_InfoFrame_Valid = _stCfdHdmi[pstCfdFire->u8Win].bHDRInfoFrameValid;
    stHDMIInfoFrameParam.u8HDMISource_EOTF = _stCfdHdmi[pstCfdFire->u8Win].u8EOTF;
    stHDMIInfoFrameParam.u8HDMISource_SMD_ID = _stCfdHdmi[pstCfdFire->u8Win].u8SMDID;
    stHDMIInfoFrameParam.u16Master_Panel_Max_Luminance = _stCfdHdmi[pstCfdFire->u8Win].u16MasterPanelMaxLuminance;    //data * 1 nits
    stHDMIInfoFrameParam.u16Master_Panel_Min_Luminance = _stCfdHdmi[pstCfdFire->u8Win].u16MasterPanelMinLuminance;    //data * 0.0001 nits
    stHDMIInfoFrameParam.u16Max_Content_Light_Level = _stCfdHdmi[pstCfdFire->u8Win].u16MaxContentLightLevel;       //data * 1 nits
    stHDMIInfoFrameParam.u16Max_Frame_Avg_Light_Level = _stCfdHdmi[pstCfdFire->u8Win].u16MaxFrameAvgLightLevel;     //data * 1 nits
    stHDMIInfoFrameParam.stu_Cfd_HDMISource_MasterPanel_ColorMetry.u16Display_Primaries_x[0] = _stCfdHdmi[pstCfdFire->u8Win].u16Display_Primaries_x[0];
    stHDMIInfoFrameParam.stu_Cfd_HDMISource_MasterPanel_ColorMetry.u16Display_Primaries_x[1] = _stCfdHdmi[pstCfdFire->u8Win].u16Display_Primaries_x[1];
    stHDMIInfoFrameParam.stu_Cfd_HDMISource_MasterPanel_ColorMetry.u16Display_Primaries_x[2] = _stCfdHdmi[pstCfdFire->u8Win].u16Display_Primaries_x[2];
    stHDMIInfoFrameParam.stu_Cfd_HDMISource_MasterPanel_ColorMetry.u16Display_Primaries_y[0] = _stCfdHdmi[pstCfdFire->u8Win].u16Display_Primaries_y[0];
    stHDMIInfoFrameParam.stu_Cfd_HDMISource_MasterPanel_ColorMetry.u16Display_Primaries_y[1] = _stCfdHdmi[pstCfdFire->u8Win].u16Display_Primaries_y[1];
    stHDMIInfoFrameParam.stu_Cfd_HDMISource_MasterPanel_ColorMetry.u16Display_Primaries_y[2] = _stCfdHdmi[pstCfdFire->u8Win].u16Display_Primaries_y[2];
    stHDMIInfoFrameParam.stu_Cfd_HDMISource_MasterPanel_ColorMetry.u16White_point_x = _stCfdHdmi[pstCfdFire->u8Win].u16White_point_x;
    stHDMIInfoFrameParam.stu_Cfd_HDMISource_MasterPanel_ColorMetry.u16White_point_y = _stCfdHdmi[pstCfdFire->u8Win].u16White_point_y;
    stHDMIInfoFrameParam.u8Mastering_Display_Infor_Valid = _stCfdHdmi[pstCfdFire->u8Win].u8SMDID==0?1:0;
    stHDMIInfoFrameParam.u8HDMISource_Support_Format = (_stCfdHdmi[pstCfdFire->u8Win].u8RgbQuantizationRange<<5) | (_stCfdHdmi[pstCfdFire->u8Win].u8YccQuantizationRange<<3) |(_stCfdHdmi[pstCfdFire->u8Win].u8PixelFormat);
    stHDMIInfoFrameParam.u8HDMISource_Colorspace = MHal_XC_HDMI_Color_Format(_stCfdHdmi[pstCfdFire->u8Win].u8PixelFormat, _stCfdHdmi[pstCfdFire->u8Win].u8Colorimetry, _stCfdHdmi[pstCfdFire->u8Win].u8ExtendedColorimetry);

    u16RetVal = Mapi_Cfd_inter_HDMI_InfoFrame_Param_Check(&stHDMIInfoFrameParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_inter_HDMI_InfoFrame_Param_Set(&stHDMIInfoFrameParam);
    }
    else
    {
        printk("Mapi_Cfd_inter_HDMI_InfoFrame_Param_Check fail, errCode: %d\n", u16RetVal);
    }

    return u16RetVal;
}

MS_U16 MHal_XC_CFD_SetHdrParam(ST_KDRV_XC_CFD_FIRE *pstCfdFire)
{
    MS_U16 u16RetVal = 0;
    STU_CFDAPI_HDR_METADATA_FORMAT stHDRMetadataFormatParam;
    memset(&stHDRMetadataFormatParam, 0, sizeof(STU_CFDAPI_HDR_METADATA_FORMAT));
    stHDRMetadataFormatParam.u32Version = CFD_HDMI_HDR_METADATA_VERSION;
    stHDRMetadataFormatParam.u16Length = sizeof(STU_CFDAPI_HDR_METADATA_FORMAT);
    if (MHal_XC_GetDolbyStatus())
    {
        stHDRMetadataFormatParam.stu_Cfd_Dolby_HDR_Param.u8IsDolbyHDREn = 1;
    }
    else
    {
        stHDRMetadataFormatParam.stu_Cfd_Dolby_HDR_Param.u8IsDolbyHDREn = 0;
    }
    //stHDRMetadataFormatParam.stu_Cfd_Dolby_HDR_Param.u8IsDolbyHDREn = _stCfdHdr[pstCfdFire->u8Win].u8HdrType==1?1:0;

    u16RetVal = Mapi_Cfd_inter_HDR_Metadata_Param_Check(&stHDRMetadataFormatParam);
    if (u16RetVal == E_CFD_MC_ERR_NOERR)
    {
        Mapi_Cfd_inter_HDR_Metadata_Param_Set(&stHDRMetadataFormatParam);
    }
    else
    {
        printk("Mapi_Cfd_inter_HDR_Metadata_Param_Check fail, errCode: %d\n", u16RetVal);
    }

    return u16RetVal;
}

//eCFIO ref to E_CFD_CFIO, u8MCFormat ref to E_CFD_MC_FORMAT
void MHal_XC_HDMIColorFormatOut(MS_U8 eCFIO, MS_U8 u8MCFormat, MS_U8 *u8PixelFormat, MS_U8 *u8Colorimetry, MS_U8 *u8ExtendColorimetry)
{
    switch(eCFIO)
    {
        case E_CFD_CFIO_RGB_BT2020:
            *u8PixelFormat = 0;
            *u8Colorimetry = 3;
            *u8ExtendColorimetry = 6;
            break;
        case E_CFD_CFIO_ADOBE_RGB:
            *u8PixelFormat = 0;
            *u8Colorimetry = 3;
            *u8ExtendColorimetry = 4;
            break;
        case E_CFD_CFIO_YUV_BT601_625:
            *u8PixelFormat = u8MCFormat;
            *u8Colorimetry = 1;
            *u8ExtendColorimetry = 0;
            break;
        case E_CFD_CFIO_YUV_BT709:
            *u8PixelFormat = u8MCFormat;
            *u8Colorimetry = 0;
            *u8ExtendColorimetry = 0;
            break;
        case E_CFD_CFIO_YUV_BT2020_NCL:
            *u8PixelFormat = u8MCFormat;
            *u8Colorimetry = 3;
            *u8ExtendColorimetry = 6;
            break;
        case E_CFD_CFIO_YUV_BT2020_CL:
            *u8PixelFormat = u8MCFormat;
            *u8Colorimetry = 3;
            *u8ExtendColorimetry = 5;
            break;
        case E_CFD_CFIO_XVYCC_601:
            *u8PixelFormat = u8MCFormat;
            *u8Colorimetry = 3;
            *u8ExtendColorimetry = 0;
            break;
        case E_CFD_CFIO_XVYCC_709:
            *u8PixelFormat = u8MCFormat;
            *u8Colorimetry = 3;
            *u8ExtendColorimetry = 1;
            break;
        case E_CFD_CFIO_SYCC601:
            *u8PixelFormat = u8MCFormat;
            *u8Colorimetry = 3;
            *u8ExtendColorimetry = 2;
            break;
        case E_CFD_CFIO_ADOBE_YCC601:
            *u8PixelFormat = u8MCFormat;
            *u8Colorimetry = 3;
            *u8ExtendColorimetry = 3;
            break;
        case E_CFD_CFIO_SRGB:
        case E_CFD_CFIO_RGB_BT709:
        case E_CFD_CFIO_YUV_BT601_525:
        default:
            *u8PixelFormat = 0;
            *u8Colorimetry = 0;
            *u8ExtendColorimetry = 0;
            break;
    }
    return;
}

MS_U16 MHal_XC_GetHdmiOutParam(ST_KDRV_XC_CFD_HDMI *pstXcCfdHdmi, MS_U8 u8Win)
{
    /// HDR type (0: SDR, 1: Dolby HDR, 2: Open HDR)
    EN_KDRV_XC_CFD_HDR_TYPE eVideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_NONE ;
    ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info stFormatInfo;
    MS_U8 u8HDRUIH2SMode = 0;
    MS_BOOL bHDRBypass = FALSE;

    if (MHal_XC_GetDolbyStatus())
    {
        return E_CFD_MC_ERR_NOERR;
    }
    else
    {
        if (CFD_IS_MM(_stCfdInit[u8Win].u8InputSource))
        {
            MHal_XC_CFD_WithdrawMMParam(&stFormatInfo, u8Win);
            if ((stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.bVUIValid == TRUE) && (stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics == 16))
            {
                // MM Open HDR
                eVideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_OPEN;
            }
            else if ((stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.bVUIValid == TRUE) && (stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics == 18))
            {
                // MM HLG HDR
                eVideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_HLG;
            }
        }
        else if (CFD_IS_HDMI(_stCfdInit[u8Win].u8InputSource))
        {
            if ((_stCfdHdmi[u8Win].bHDRInfoFrameValid == TRUE) && (_stCfdHdmi[u8Win].u8EOTF == 2))
            {
                // HDMI Open HDR
                eVideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_OPEN;
            }
            else if ((_stCfdHdmi[u8Win].bHDRInfoFrameValid == TRUE) && (_stCfdHdmi[u8Win].u8EOTF == 3))
            {
                // HDMI HLG HDR
                eVideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_HLG;
            }
        }
    }

    if (eVideoHdrType == E_KDRV_XC_CFD_HDR_TYPE_OPEN)
    {
        if (CFD_IS_MM(_stCfdInit[u8Win].u8InputSource))
        {
            _stCfdHdmi_Out[u8Win].bHDRInfoFrameValid = TRUE;
            _stCfdHdmi_Out[u8Win].u8EOTF = 2;
            _stCfdHdmi_Out[u8Win].u8SMDID = E_CFD_HDMI_META_TYPE1;
            _stCfdHdmi_Out[u8Win].u16MasterPanelMaxLuminance = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u32Master_Panel_Max_Luminance;    //data * 1 nits
            _stCfdHdmi_Out[u8Win].u16MasterPanelMinLuminance = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u32Master_Panel_Min_Luminance;    //data * 0.0001 nits
            _stCfdHdmi_Out[u8Win].u16MaxContentLightLevel = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16maxContentLightLevel;       //data * 1 nits
            _stCfdHdmi_Out[u8Win].u16MaxFrameAvgLightLevel = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16maxPicAverageLightLevel;     //data * 1 nits
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_x[0] = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[0];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_x[1] = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[1];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_x[2] = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[2];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_y[0] = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[0];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_y[1] = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[1];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_y[2] = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[2];
            _stCfdHdmi_Out[u8Win].u16White_point_x = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16White_point_x;
            _stCfdHdmi_Out[u8Win].u16White_point_y = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u16White_point_y;
            _stCfdHdmi_Out[u8Win].bIsFullRange = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u8Video_Full_Range_Flag;
            _stCfdHdmi_Out[u8Win].u8RgbQuantizationRange = _stCfdHdmi_Out[u8Win].bIsFullRange;
            _stCfdHdmi_Out[u8Win].u8YccQuantizationRange = _stCfdHdmi_Out[u8Win].bIsFullRange;
            MHal_XC_HDMIColorFormatOut(E_CFD_CFIO_RGB_BT2020, E_CFD_MC_FORMAT_RGB, &_stCfdHdmi_Out[u8Win].u8PixelFormat, &_stCfdHdmi_Out[u8Win].u8Colorimetry, &_stCfdHdmi_Out[u8Win].u8ExtendedColorimetry);
        }
        else if (CFD_IS_HDMI(_stCfdInit[u8Win].u8InputSource))
        {
            _stCfdHdmi_Out[u8Win].bHDRInfoFrameValid = _stCfdHdmi[u8Win].bHDRInfoFrameValid;
            _stCfdHdmi_Out[u8Win].u8EOTF = _stCfdHdmi[u8Win].u8EOTF;
            _stCfdHdmi_Out[u8Win].u8SMDID = _stCfdHdmi[u8Win].u8SMDID;
            _stCfdHdmi_Out[u8Win].u16MasterPanelMaxLuminance = _stCfdHdmi[u8Win].u16MasterPanelMaxLuminance;    //data * 1 nits
            _stCfdHdmi_Out[u8Win].u16MasterPanelMinLuminance = _stCfdHdmi[u8Win].u16MasterPanelMinLuminance;    //data * 0.0001 nits
            _stCfdHdmi_Out[u8Win].u16MaxContentLightLevel = _stCfdHdmi[u8Win].u16MaxContentLightLevel;       //data * 1 nits
            _stCfdHdmi_Out[u8Win].u16MaxFrameAvgLightLevel = _stCfdHdmi[u8Win].u16MaxFrameAvgLightLevel;     //data * 1 nits
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_x[0] = _stCfdHdmi[u8Win].u16Display_Primaries_x[0];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_x[1] = _stCfdHdmi[u8Win].u16Display_Primaries_x[1];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_x[2] = _stCfdHdmi[u8Win].u16Display_Primaries_x[2];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_y[0] = _stCfdHdmi[u8Win].u16Display_Primaries_y[0];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_y[1] = _stCfdHdmi[u8Win].u16Display_Primaries_y[1];
            _stCfdHdmi_Out[u8Win].u16Display_Primaries_y[2] = _stCfdHdmi[u8Win].u16Display_Primaries_y[2];
            _stCfdHdmi_Out[u8Win].u16White_point_x = _stCfdHdmi[u8Win].u16White_point_x;
            _stCfdHdmi_Out[u8Win].u16White_point_y = _stCfdHdmi[u8Win].u16White_point_y;
            _stCfdHdmi_Out[u8Win].bIsFullRange = _stCfdHdmi[u8Win].bIsFullRange;
            _stCfdHdmi_Out[u8Win].u8PixelFormat = _stCfdHdmi[u8Win].u8PixelFormat;
            _stCfdHdmi_Out[u8Win].u8Colorimetry = _stCfdHdmi[u8Win].u8Colorimetry;
            _stCfdHdmi_Out[u8Win].u8ExtendedColorimetry = _stCfdHdmi[u8Win].u8ExtendedColorimetry;
            _stCfdHdmi_Out[u8Win].u8RgbQuantizationRange = _stCfdHdmi[u8Win].u8RgbQuantizationRange;
            _stCfdHdmi_Out[u8Win].u8YccQuantizationRange = _stCfdHdmi[u8Win].u8YccQuantizationRange;
        }
    }

    Mapi_Cfd_OSD_H2SUI_Get(&u8HDRUIH2SMode);

    if((u8HDRUIH2SMode == 2) && (_stCfdOsd[0].u8HDR_UI_H2SMode == 0))
    {
        bHDRBypass = TRUE;
    }

    if(!bHDRBypass)
    {
        _stCfdHdmi_Out[u8Win].bHDRInfoFrameValid = FALSE;
        _stCfdHdmi_Out[u8Win].u8EOTF = 1;
    }
    return E_CFD_MC_ERR_NOERR;
}

//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
void MHal_XC_W2BYTE(DWORD u32Reg, WORD u16Val )
{
    REG_W2B(u32Reg, u16Val);
}

//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
WORD MHal_XC_R2BYTE(DWORD u32Reg )
{
    return REG_RR(u32Reg) ;
}

//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
void MHal_XC_W2BYTEMSK(DWORD u32Reg, WORD u16Val, WORD u16Mask )
{
    WORD u16Data=0 ;
    u16Data = REG_RR(u32Reg);
    u16Data = (u16Data & (0xFFFF-u16Mask))|(u16Val &u16Mask);
    REG_W2B(u32Reg, u16Data);
}

//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
WORD MHal_XC_R2BYTEMSK(DWORD u32Reg, WORD u16Mask )
{
    return (REG_RR(u32Reg) & u16Mask);
}

void MHal_XC_WriteByte(DWORD u32Reg, BYTE u8Val)
{
    if(u32Reg%2)
    {
        REG_WH((u32Reg-1), u8Val);
    }
    else
    {
        REG_WL(u32Reg, u8Val);
    }
}

void MHal_XC_WriteByteMask(DWORD u32Reg, BYTE u8Val, WORD u16Mask)
{
    if(u32Reg%2)
    {
        REG_WHMSK((u32Reg-1), u8Val, u16Mask);
    }
    else
    {
        REG_WLMSK(u32Reg, u8Val, u16Mask);
    }
}

BYTE MHal_XC_ReadByte(DWORD u32Reg)
{
    if(u32Reg%2)
    {
        u32Reg = u32Reg-1 ;
        return ((REG_RR(u32Reg) & 0xFF00)>>8);
    }
    else
    {
        return (REG_RR(u32Reg) & 0xFF);
    }
}


typedef struct __attribute__((packed))
{
    MS_PHY phyVRAddr;
    MS_VIRT va_phy;
    phys_addr_t u32VRlen;
}MS_VR_Info;

MS_VR_Info _VR_client;

BYTE MHal_XC_VR_ReadByte(DWORD u32Reg)
{
        MS_U8 u8ret=0;
    if(_abVRInit==TRUE)
    {
        MS_U8 *pu8Addr = NULL;
        MS_PHYADDR tmp_diff=0;
        if(u32Reg<= _VR_client.u32VRlen)
        {
            tmp_diff = u32Reg;
            pu8Addr = (MS_U8 *)(_VR_client.va_phy + tmp_diff);
        }
        u8ret = (MS_U8)(*(MS_U8 *)pu8Addr);
    }
        return u8ret;
}

WORD MHal_XC_VR_R2BYTE(DWORD u32Reg )
{

    MS_U16 u16ret=0;
    u16ret=MHal_XC_VR_ReadByte(u32Reg+1);
    u16ret<<= 8;
    u16ret|=MHal_XC_VR_ReadByte(u32Reg);
    return u16ret;

}

WORD MHal_XC_VR_R2BYTEMSK(DWORD u32Reg, WORD u16Mask )
{
    return (MHal_XC_VR_R2BYTE(u32Reg) & u16Mask);
}

void MHal_XC_VR_WriteByte(DWORD u32Reg, BYTE u8Val )
{
    if(_abVRInit==TRUE)
    {
    MS_U8 *pu8Addr = NULL;
    MS_PHYADDR tmp_diff=0,DstAddr=0;
    if(u32Reg<= _VR_client.u32VRlen)
    {
        tmp_diff = u32Reg;
        DstAddr = _VR_client.va_phy + tmp_diff;
        pu8Addr = (MS_U8 *)DstAddr;
       *pu8Addr = u8Val;
    }
}
}

void MHal_XC_VR_W2BYTE(DWORD u32Reg, WORD u16Val )
{
    MS_U8 u8Value = 0;
    u8Value = u16Val&0xFF;
    MHal_XC_VR_WriteByte(u32Reg, u8Value);
    u8Value = (u16Val&0xFF00)>>8;
    MHal_XC_VR_WriteByte(u32Reg+1, u8Value);
}

void MHal_XC_VR_W2BYTEMSK(DWORD u32Reg, WORD u16Val, WORD u16Mask )
{
    MS_U16 u16Value = 0;
    u16Value = MHal_XC_VR_R2BYTE(u32Reg );
    u16Value= (u16Value& ~(u16Mask) ) | ((u16Val) & (u16Mask)) ;
    MHal_XC_VR_W2BYTE(u32Reg, u16Value );
}

//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
BOOL MHal_XC_IsBlackVideoEnable( MS_BOOL bWindow )
{
    MS_BOOL bReturn=FALSE;

    if( MAIN_WINDOW == bWindow)
    {
        if(MHal_XC_R2BYTEMSK(REG_SC_BK10_19_L,BIT1))
        {
            bReturn = TRUE;
        }
        else
        {
            bReturn = FALSE;
        }
    }

    return bReturn;
}

//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
#if (CONFIG_MSTAR_FRC_SOFTWARE_TOGGLE == 1)
void MHal_XC_FRCR2SoftwareToggle( void )
{
    MHal_XC_W2BYTEMSK(REG_SC_BK00_14_L, 0x00, BIT4);

    if (MHal_XC_R2BYTEMSK(REG_SC_BK00_10_L, BIT4) == BIT4) // output Vsync case
    {
        //MHal_XC_W2BYTEMSK(REG_SC_BK00_12_L, BIT4, BIT4); // Clean job will be done in scaler
        FRC_CPU_INT_REG(REG_FRCINT_HKCPUFIRE) = INT_HKCPU_FRCR2_INPUT_SYNC;     //to FRC-R2
        FRC_CPU_INT_REG(REG_FRCINT_HKCPUFIRE) = INT_HKCPU_FRCR2_OUTPUT_SYNC;    //to FRC-R2
        mb();
        FRC_CPU_INT_REG(REG_FRCINT_HKCPUFIRE) = 0;
    }
}
#endif

//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
static MS_U16 _HAL_XC_ReadMetadata2BYTE(MS_U16 u16Addr)
{
    MS_U16 u16FlagValue = 0;
    MS_U16 u16Value = 0;
    struct timespec startTs;
    struct timespec endTs;
    // set descrb read back addr
    MHal_XC_W2BYTEMSK(REG_SC_BK79_12_L, u16Addr, 0x00FF);
    // set check flag
    MHal_XC_W2BYTEMSK(REG_SC_BK79_12_L, (u16Addr % 16) << 12, 0xF000);
    // read check flag
    u16FlagValue = MHal_XC_R2BYTEMSK(REG_SC_BK79_14_L, 0xF000) >> 12;


    getnstimeofday(&startTs);
    // wait read-back flag == set flag, longest time = 4/350ms
    while (u16FlagValue != (u16Addr % 16))
    {
        long long diff = 0;
        // timeout is 1s
        getnstimeofday(&endTs);
        diff = endTs.tv_nsec + ((endTs.tv_sec - startTs.tv_sec) * 1000000000 - startTs.tv_nsec);
        if (diff > 1000000000)
        {
            printk("[XC][%s:%05d] wait read-back flag == set flag timeout!!!", __FUNCTION__, __LINE__);
            break;
        }
        u16FlagValue = MHal_XC_R2BYTEMSK(REG_SC_BK79_14_L, 0xF000) >> 12;
    }
    // read back metadata
    u16Value = MHal_XC_R2BYTE(REG_SC_BK79_13_L);

    return u16Value;
}

BOOL MHal_XC_GetMiuOffset(MS_U32 u32MiuNo, MS_U64 *pu64Offset)
{
    if (u32MiuNo == 0)
    {
        *pu64Offset = MSTAR_MIU0_BUS_BASE;
    }
    else if (u32MiuNo == 1)
    {
        *pu64Offset = MSTAR_MIU1_BUS_BASE;
    }
    else
    {
        return FALSE;
    }

    return TRUE;
}

#define DOLBY_VISION_SW_DESCRAMBLE_ENABLE 1
#if (DOLBY_VISION_SW_DESCRAMBLE_ENABLE == 1)
typedef struct
{
    MS_U8 metadata_length_hi;
    MS_U8 metadata_length_lo;
    MS_U8 metadata_body[119];
} FirstPacketBody;

typedef struct
{
    MS_U8 metadata_body[121];
} FollowingPacketBody;

typedef struct
{
    MS_U8 header0;
    MS_U8 header1;
    MS_U8 header2;
    union
    {
        /* this is used by packet type 0 or 1 */
        FirstPacketBody metadata_first;
        /* this is used by packet type 2 or 3 */
        FollowingPacketBody metadata_following;
    } metadata;
    MS_U8 crc3;
    MS_U8 crc2;
    MS_U8 crc1;
    MS_U8 crc0;
} EDRMetaDataPacket;

#define PACKET_TYPE_SINGLE      0
#define PACKET_TYPE_FIRST       1
#define PACKET_TYPE_INTERM      2
#define PACKET_TYPE_LAST        3

#define FIELD_MASK_PACKET_TYPE 0xc0 /*Metadata is hide in */
#define BitSet(c, mask, bit_value)  (((bit_value) * (mask)) | ((c) & ~(mask)))
#define input_frame_size (1920*1080*3)
#define PACKET_REPEAT    3
#define interval  (sizeof(EDRMetaDataPacket) * 8 * u8FrameNumber * 3)
#define MAX_DM_EXT_BLOCKS 255

typedef struct dm_metadata_base_s
{
    /* signal attributes */
    unsigned char dm_metadata_id; // affected_dm_metadata_id<<4|current_dm_metadata_id
    unsigned char scene_refresh_flag;
    unsigned char YCCtoRGB_coef0_hi;
    unsigned char YCCtoRGB_coef0_lo;
    unsigned char YCCtoRGB_coef1_hi;
    unsigned char YCCtoRGB_coef1_lo;
    unsigned char YCCtoRGB_coef2_hi;
    unsigned char YCCtoRGB_coef2_lo;
    unsigned char YCCtoRGB_coef3_hi;
    unsigned char YCCtoRGB_coef3_lo;
    unsigned char YCCtoRGB_coef4_hi;
    unsigned char YCCtoRGB_coef4_lo;
    unsigned char YCCtoRGB_coef5_hi;
    unsigned char YCCtoRGB_coef5_lo;
    unsigned char YCCtoRGB_coef6_hi;
    unsigned char YCCtoRGB_coef6_lo;
    unsigned char YCCtoRGB_coef7_hi;
    unsigned char YCCtoRGB_coef7_lo;
    unsigned char YCCtoRGB_coef8_hi;
    unsigned char YCCtoRGB_coef8_lo;
    unsigned char YCCtoRGB_offset0_byte3;
    unsigned char YCCtoRGB_offset0_byte2;
    unsigned char YCCtoRGB_offset0_byte1;
    unsigned char YCCtoRGB_offset0_byte0;
    unsigned char YCCtoRGB_offset1_byte3;
    unsigned char YCCtoRGB_offset1_byte2;
    unsigned char YCCtoRGB_offset1_byte1;
    unsigned char YCCtoRGB_offset1_byte0;
    unsigned char YCCtoRGB_offset2_byte3;
    unsigned char YCCtoRGB_offset2_byte2;
    unsigned char YCCtoRGB_offset2_byte1;
    unsigned char YCCtoRGB_offset2_byte0;
    unsigned char RGBtoLMS_coef0_hi;
    unsigned char RGBtoLMS_coef0_lo;
    unsigned char RGBtoLMS_coef1_hi;
    unsigned char RGBtoLMS_coef1_lo;
    unsigned char RGBtoLMS_coef2_hi;
    unsigned char RGBtoLMS_coef2_lo;
    unsigned char RGBtoLMS_coef3_hi;
    unsigned char RGBtoLMS_coef3_lo;
    unsigned char RGBtoLMS_coef4_hi;
    unsigned char RGBtoLMS_coef4_lo;
    unsigned char RGBtoLMS_coef5_hi;
    unsigned char RGBtoLMS_coef5_lo;
    unsigned char RGBtoLMS_coef6_hi;
    unsigned char RGBtoLMS_coef6_lo;
    unsigned char RGBtoLMS_coef7_hi;
    unsigned char RGBtoLMS_coef7_lo;
    unsigned char RGBtoLMS_coef8_hi;
    unsigned char RGBtoLMS_coef8_lo;
    unsigned char signal_eotf_hi;
    unsigned char signal_eotf_lo;
    unsigned char signal_eotf_param0_hi;
    unsigned char signal_eotf_param0_lo;
    unsigned char signal_eotf_param1_hi;
    unsigned char signal_eotf_param1_lo;
    unsigned char signal_eotf_param2_byte3;
    unsigned char signal_eotf_param2_byte2;
    unsigned char signal_eotf_param2_byte1;
    unsigned char signal_eotf_param2_byte0;
    unsigned char signal_bit_depth;
    unsigned char signal_color_space;
    unsigned char signal_chroma_format;
    unsigned char signal_full_range_flag;
    /* source display attributes */
    unsigned char source_min_PQ_hi;
    unsigned char source_min_PQ_lo;
    unsigned char source_max_PQ_hi;
    unsigned char source_max_PQ_lo;
    unsigned char source_diagonal_hi;
    unsigned char source_diagonal_lo;
    /* extended metadata */
    unsigned char num_ext_blocks;
} dm_metadata_base_t;

typedef struct ext_level_2_s
{
    unsigned char target_max_PQ_hi       ;
    unsigned char target_max_PQ_lo       ;
    unsigned char trim_slope_hi          ;
    unsigned char trim_slope_lo          ;
    unsigned char trim_offset_hi         ;
    unsigned char trim_offset_lo         ;
    unsigned char trim_power_hi          ;
    unsigned char trim_power_lo          ;
    unsigned char trim_chroma_weight_hi  ;
    unsigned char trim_chroma_weight_lo  ;
    unsigned char trim_saturation_gain_hi;
    unsigned char trim_saturation_gain_lo;
    unsigned char ms_weight_hi           ;
    unsigned char ms_weight_lo           ;
} ext_level_2_t;

typedef struct ext_level_1_s
{
    unsigned char min_PQ_hi;
    unsigned char min_PQ_lo;
    unsigned char max_PQ_hi;
    unsigned char max_PQ_lo;
    unsigned char avg_PQ_hi;
    unsigned char avg_PQ_lo;
} ext_level_1_t;


typedef struct dm_metadata_ext_s
{
    unsigned char ext_block_length_byte3;
    unsigned char ext_block_length_byte2;
    unsigned char ext_block_length_byte1;
    unsigned char ext_block_length_byte0;
    unsigned char ext_block_level;
    union
    {
        ext_level_1_t level_1;
        ext_level_2_t level_2;
    } l;
} dm_metadata_ext_t;

typedef struct dm_metadata_s
{
    dm_metadata_base_t base;
    dm_metadata_ext_t ext[MAX_DM_EXT_BLOCKS];
} dm_metadata_t;

static MS_U32 crc32_lut[256] =
{
    0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,
    0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,
    0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd, 0x4c11db70, 0x48d0c6c7,
    0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
    0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3,
    0x709f7b7a, 0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
    0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58, 0xbaea46ef,
    0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
    0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb,
    0xceb42022, 0xca753d95, 0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,
    0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,
    0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
    0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4,
    0x0808d07d, 0x0cc9cdca, 0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
    0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08,
    0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
    0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc,
    0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,
    0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a, 0xe0b41de7, 0xe4750050,
    0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
    0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,
    0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,
    0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb, 0x4f040d56, 0x4bc510e1,
    0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
    0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5,
    0x3f9b762c, 0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
    0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e, 0xf5ee4bb9,
    0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
    0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd,
    0xcda1f604, 0xc960ebb3, 0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,
    0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,
    0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
    0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2,
    0x470cdd2b, 0x43cdc09c, 0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
    0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e,
    0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
    0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a,
    0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,
    0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c, 0xe3a1cbc1, 0xe760d676,
    0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
    0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,
    0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,
    0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
};


MS_U32 crc32_dolby(MS_U32 crc, const void *buf, size_t size)
{
    const MS_U8 *p = (MS_U8 *)buf;

    crc = ~crc;

    while(size)
    {
        crc = (crc << 8) ^ crc32_lut[((crc >> 24) ^ *p) & 0xff];
        p++;
        size--;
    }

    return crc;
}

/*
    SetNextBitRev : Set bit in a byte sequence in reverse order inside the byte, update the bit poition to next bit, and update
    byte position if necessary
    cur_pos: pointer to the current position in the byte where the bit will be retrived, should be in range [0, 7]. Advance one in this function.
    cur_byte: pointer to the current byte position where the bit needs to be retrieved, will advance one byte if cur_pos is the last bit
*/
void SetNextBitRev(MS_U8 **cur_byte, MS_U32 *cur_pos, MS_U8 bit)
{
    (**cur_byte) = BitSet((**cur_byte), (1 << (*cur_pos)), bit);

    if ((*cur_pos) > 0)
    {
        *cur_pos = (*cur_pos) - 1;
    }
    else
    {
        *cur_pos = 7;
        (*cur_byte) = (*cur_byte) + 1;
    }

}

/* precomputed lookup table for xor-ed bits in one byte */
MS_U8 XorMatrix[] =
{
    0,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  1,  0,  1,  1,  0,
    1,  0,  0,  1,  0,  1,  1,  0,  0,  1,  1,  0,  1,  0,  0,  1,
    1,  0,  0,  1,  0,  1,  1,  0,  0,  1,  1,  0,  1,  0,  0,  1,
    0,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  1,  0,  1,  1,  0,
    1,  0,  0,  1,  0,  1,  1,  0,  0,  1,  1,  0,  1,  0,  0,  1,
    0,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  1,  0,  1,  1,  0,
    0,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  1,  0,  1,  1,  0,
    1,  0,  0,  1,  0,  1,  1,  0,  0,  1,  1,  0,  1,  0,  0,  1,
    1,  0,  0,  1,  0,  1,  1,  0,  0,  1,  1,  0,  1,  0,  0,  1,
    0,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  1,  0,  1,  1,  0,
    0,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  1,  0,  1,  1,  0,
    1,  0,  0,  1,  0,  1,  1,  0,  0,  1,  1,  0,  1,  0,  0,  1,
    0,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  1,  0,  1,  1,  0,
    1,  0,  0,  1,  0,  1,  1,  0,  0,  1,  1,  0,  1,  0,  0,  1,
    1,  0,  0,  1,  0,  1,  1,  0,  0,  1,  1,  0,  1,  0,  0,  1,
    0,  1,  1,  0,  1,  0,  0,  1,  1,  0,  0,  1,  0,  1,  1,  0
};

MS_U8 GetByteField(MS_U8 meta_byte, MS_U8 field_mask)
{
    MS_U8 field_lsb_pos = 0;
    MS_U8 fm = field_mask;

    /* find where the lsb of field is */
    while (!(fm & 1))
    {
        fm >>= 1;
        field_lsb_pos++;
    }

    return ((meta_byte & field_mask) >> field_lsb_pos);
}

MS_U8 GetPacketType(EDRMetaDataPacket *packet)
{
    return GetByteField(packet->header0, FIELD_MASK_PACKET_TYPE);
}

MS_U16 GetMetadataLength(EDRMetaDataPacket *packet)
{
    return (packet->metadata.metadata_first.metadata_length_hi << 8)
           + packet->metadata.metadata_first.metadata_length_lo;
}


MS_U32 DescrambleMetaDataImplMStar(const MS_U8 * const buf_scrambled
                                   , EDRMetaDataPacket * const output_metadata_packet
                                   , MS_U8 u8FrameNumber
                                   , MS_U8 byte_per_word)
{
    MS_U32 idx = 0;                                /* buffer index for buf_scrambled */
    MS_U8 *p_buf_metadata = (MS_U8 *)output_metadata_packet;
    MS_U8 **cur_metadata_byte = &p_buf_metadata;
    MS_U32 cur_metadata_pos = 7;
    MS_U16 metadata_bytes = sizeof(EDRMetaDataPacket);
    MS_U32 len_meta = metadata_bytes * 8;  /* how many bits in metadata packet */
    MS_U8 metabit;
    MS_U8 xored_bit = 0;
    MS_U32 crc;
    MS_U32 i32 = 0, i;

    const MS_U8 * const buf_in_r = buf_scrambled;
    const MS_U8 * const buf_in_g = buf_scrambled + u8FrameNumber * byte_per_word;
    const MS_U8 * const buf_in_b = buf_scrambled + u8FrameNumber * byte_per_word * 2;

    memset(output_metadata_packet, 0, metadata_bytes);

    /* process pixel data in multiples of 4 bytes, or 1 bit of metadata at a time */
    while (len_meta)
    {
        while ((idx < byte_per_word) && (len_meta--))
        {
            i = i32 + idx;
            xored_bit  = XorMatrix[buf_in_r[i]];
            xored_bit ^= XorMatrix[buf_in_g[i]];
            xored_bit ^= XorMatrix[buf_in_b[i]];
            metabit = xored_bit;
            SetNextBitRev(cur_metadata_byte, &cur_metadata_pos, metabit);
            idx++;
        }

        i32 += (u8FrameNumber * byte_per_word * 3);
        idx = 0;
    }

    /* crc check */
    crc = crc32_dolby(0, output_metadata_packet, metadata_bytes);

    if (crc != 0)
        return 0xFFFFFFFF;

    return 0;
}

MS_U32 DescrambleMetaDataFrame(
    const MS_U8 *buf_in_start,  // start point of frame
    MS_U8 *buf_metadata,        // buffer of storing metadata
    MS_U16 *p_metadata_len,     // size of metadata
    MS_U8 u8FrameNumber,        // total frame
    MS_U8 byte_per_word)
{
    MS_U32 i;
    int offset;
    EDRMetaDataPacket output_packet;
    MS_U8 packet_type;
    unsigned short metadata_len, cur_metadata_len, remain_metadata_len;
    const MS_U8 *buf_in = buf_in_start;

    /* first packet */
    for (i=0; i< PACKET_REPEAT; ++i)
    {
        if (!DescrambleMetaDataImplMStar(buf_in_start, &output_packet,u8FrameNumber, byte_per_word))
            break;
        buf_in_start += interval;
    }
    if (PACKET_REPEAT == i)
    {
        if (_u32SWDesrbErrCount > 1)
        {
            printk("Error: packet crc error, can not receover\n");
        }
        return 0xFFFFFFFF;
    }

    /* get packet type and metadata length */
    packet_type = GetPacketType(&output_packet);
    switch (packet_type)
    {
        case PACKET_TYPE_SINGLE:
            metadata_len = cur_metadata_len = GetMetadataLength(&output_packet);
            if (metadata_len > 119)
            {
                printk("Error: metadata type is %d but metadata length is %d\n", PACKET_TYPE_SINGLE, metadata_len);
                return 0xFFFFFFFF;
            }
            remain_metadata_len = 0;
            break;
        case PACKET_TYPE_FIRST:
            metadata_len = GetMetadataLength(&output_packet);
            if (metadata_len <= 119)
            {
                printk("Error: metadata type is %d but metadata length is %d\n", PACKET_TYPE_FIRST, metadata_len);
                return 0xFFFFFFFF;
            }
            cur_metadata_len = 119;
            remain_metadata_len = metadata_len - 119;
            break;
        default:
            printk("Error: unexpected packet type for first packet %d\n", packet_type);
            return 0xFFFFFFFF;
    }

    /* copy metadata from packet */
    memcpy(buf_metadata,
           output_packet.metadata.metadata_first.metadata_body,
           cur_metadata_len
          );
    offset = cur_metadata_len;

    /* handle more packets */
    if (packet_type == PACKET_TYPE_FIRST)
    {
        buf_in_start = buf_in + interval * PACKET_REPEAT;
        do
        {
            for (i=0; i<PACKET_REPEAT; ++i)
            {
                if (!DescrambleMetaDataImplMStar(buf_in_start + i*interval, &output_packet, u8FrameNumber, byte_per_word))
                    break;
            }
            if (PACKET_REPEAT == i)
            {
                if (_u32SWDesrbErrCount > 1)
                {
                    printk("Error: subsequent packet crc error, can not receover\n");
                }
                return 0xFFFFFFFF;
            }
            buf_in_start += PACKET_REPEAT* interval;

            /* get packet type and metadata length */
            packet_type = GetPacketType(&output_packet);
            switch (packet_type)
            {
                case PACKET_TYPE_INTERM:
                    cur_metadata_len = 121;
                    remain_metadata_len -= 121;
                    break;
                case PACKET_TYPE_LAST:
                    if (remain_metadata_len > 121)
                    {
                        printk("Error: unexpected packet end\n");
                        return 0xFFFFFFFF;
                    }
                    cur_metadata_len = remain_metadata_len;
                    remain_metadata_len = 0;
                    break;
                default:
                    printk("Error: unexpected packet type for subsequent packet %d\n", packet_type);
                    return 0xFFFFFFFF;
            }

            /* copy metadata from packet */
            memcpy(buf_metadata + offset,
                   output_packet.metadata.metadata_following.metadata_body,
                   cur_metadata_len
                  );
            offset += cur_metadata_len;
        }
        while (packet_type != PACKET_TYPE_LAST);
    }

    *p_metadata_len = metadata_len;
    return 0;
}

MS_U32 DescrambleMetaData(void* pRawData, void* pDescrambledData, MS_U8 u8FrameNumber, MS_U8 byte_per_word)//EDRHDMIMetaDataConfig *config)
{
    MS_U8 *buf_in;
    MS_U8 *buf_in_start;
    MS_U8 *buf_metadata;
    unsigned short metadata_len;
    int frame_nr = 0;
    //MS_U64 fs;
    unsigned int k;
    int more_frames = 1;

    // 1 step:
    // get start point & size & frame number of scaler dram
    /* open input scrambled frame file, get its size, read into buffer */

#if 0
    /* sanity check */
    if (config->interval*config->repeat > config->input_frame_size)
    {
        printk("Error: input frame size too small to even hold first metadata packet\n");
        return -1;
    }
#endif

    // 2 step:
    // define a variable, pointer to start point of scaler frame buffer

    //buf_in = (MS_U8 *)malloc(input_frame_size * fn_per_read_frame);
    // read dram
#if 0
    {
        MS_U32 u32DNRBase0 = pXCResourcePrivate->stdrvXC_MVideo_Context.g_XC_InitData.u32Main_FB_Start_Addr;
        u32DNRBase0 = u32DNRBase0
                      + 1920         //IPM offset
                      * 22           // 22 lines per Dolby request
                      * 3            // byte per pixel
                      * fn_per_read_frame;   // frame
        MS_U32 *pu32Addr;
        MS_PHYADDR DstAddr;
        DstAddr = u32DNRBase0;
        pu32Addr = (MS_U32 *)MS_PA2KSEG1(DstAddr);
        buf_in = (MS_U8 *) pu32Addr;
        printk("Dolby [%s,%5d] buf_in address(%p)\n",__FUNCTION__,__LINE__,buf_in);
        printk("Dolby [%s,%5d] buf_in[0] = %x, buf_in[1] = %x\n",__FUNCTION__,__LINE__, *buf_in,*(buf_in+1));
    }
#else
    {
        buf_in = (MS_U8 *) pRawData;
        //printk("Dolby [%s,%5d] buf_in address(%p)\n",__FUNCTION__,__LINE__,buf_in);
        //printk("Dolby [%s,%5d] buf_in[0] = %x, buf_in[1] = %x\n",__FUNCTION__,__LINE__, *buf_in,*(buf_in+1));
    }
#endif
    // 3 step:
    // define a variable, pointer to descrambled metadata from scaler frame buffer.


    // 4 step:(Not sure if necessary)
    // define a variable, pointer to size of descrambled metadata from scaler frame buffer.
    /* open other output files */


    // 5 step:
    // allocate buffer to store one metadata
    buf_metadata = (MS_U8 *)kmalloc(sizeof(dm_metadata_t), GFP_KERNEL);


    // 6 step:
    // start to descramble datas from frame buffer, read one frame buffer one time.
    while (more_frames)
    {
        for (k = 0; k <= u8FrameNumber; k++)
        {

            // if reading all frames done, jump out.
            if (k == u8FrameNumber)
            {
                more_frames = 0;
                break;
            }

            /* Descamble meta data from pixel data */
            buf_in_start = buf_in + k* byte_per_word;    /* for mstar, k=0 for other format */

            if (DescrambleMetaDataFrame(/*config,*/ buf_in_start, buf_metadata, &metadata_len, u8FrameNumber, byte_per_word) == 0xFFFFFFFF)
            {
                if (buf_metadata)
                    kfree(buf_metadata);

                return 0xFFFFFFFF;
            }
            //printk("Dolby [%s,%5d] metadata_len(%d)\n",__FUNCTION__,__LINE__,metadata_len);

            frame_nr++;
        } /* end for k */
    }

    //printk("Descrambled %u frames\n", frame_nr);

    /* deallocate buffers */
    if (buf_metadata)
        kfree(buf_metadata);

    return 0;
}
#endif // DOLBY_VISION_SW_DESCRAMBLE_ENABLE

BOOL MHal_XC_SwDescrambleMetadata(MS_U8 *pu8Metadata, MS_U16 *pu16MetadataLength)
{
    // F2 field number
    MS_U8 u8FrameNumber = MHal_XC_R2BYTEMSK(REG_SC_BK42_19_L, 0x001F);
    // F2 MIU select
    MS_U8 u8MiuSel = MHal_XC_R2BYTEMSK(REG_SC_BK42_05_L, 0x0030) >> 4;
    MS_U8 u8IPMCurFrameNum = MHal_XC_R2BYTEMSK(REG_SC_BK42_3A_L, 0x000F);
    // F2 IP frame buffer base address
    MS_U32 u32AddrL = MHal_XC_R2BYTE(REG_SC_BK42_08_L);
    MS_U32 u32AddrH = MHal_XC_R2BYTE(REG_SC_BK42_09_L) & 0x01FF;
    MS_U32 u32Addr = (u32AddrL | (u32AddrH << 16)) * BYTE_PER_WORD;
    MS_U64 u64Offset = 0;
    MS_U8 *pRawData = NULL;
    MS_U8 *pu8RawBufStart = NULL;
    //MS_U8 *pu8MetadataBuf = NULL;
    MS_BOOL bUnmapDRAM = FALSE;
    MS_BOOL bRetVal = TRUE;
    if (MHal_XC_GetMiuOffset(u8MiuSel, &u64Offset) != TRUE)
    {
        return FALSE;
    }

    if (pfn_valid(__phys_to_pfn((MS_U64)u32Addr + u64Offset)))
    {
        pRawData = __va((MS_U64)u32Addr + u64Offset);
        bUnmapDRAM = FALSE;
    }
    else
    {
        pRawData = (MS_U8 __iomem *)ioremap_cache((MS_U64)u32Addr + u64Offset, 3840 * 2160 * 3);
        bUnmapDRAM = TRUE;
    }

    // start point of current frame from HDR DMA buffer.
    pu8RawBufStart = pRawData + u8IPMCurFrameNum * BYTE_PER_WORD;

    if (DescrambleMetaDataFrame(pu8RawBufStart, pu8Metadata, pu16MetadataLength, u8FrameNumber, BYTE_PER_WORD) == 0xFFFFFFFF)
    {
        _u32SWDesrbErrCount++;
        if (_u32SWDesrbErrCount > 1)
        {
            printk("Sw descramble metadata fail.\n");
        }
        bRetVal = FALSE;
    }
    else
    {
        _u32SWDesrbErrCount = 0;
    }

    if (bUnmapDRAM)
    {
        iounmap(pRawData);
        pRawData = NULL;
    }

    return bRetVal;
}

BOOL MHal_XC_IsCRCPass(MS_U8 u8PkgIdx)
{
    MS_BOOL bCRCPass = FALSE;
#if 0
    MS_U32 au32CrcReg[6] = {REG_SC_BK79_1A_L, REG_SC_BK79_1B_L, REG_SC_BK79_1C_L, REG_SC_BK79_1D_L, REG_SC_BK79_1E_L, REG_SC_BK79_1F_L};

    // crc valid
    MS_U32 u32CrcLow = MHal_XC_R2BYTE(au32CrcReg[2 * u8PkgIdx]);
    MS_U32 u32CrcHigh = MHal_XC_R2BYTE(au32CrcReg[2 * u8PkgIdx+1]);
#else
    MS_U32 u32CrcLow=0, u32CrcHigh=0;
    switch(u8PkgIdx)
    {
        case 0:
            u32CrcLow = MHal_XC_R2BYTE(REG_SC_BK79_1A_L);
            u32CrcHigh = MHal_XC_R2BYTE(REG_SC_BK79_1B_L);
            break;
        case 1:
            u32CrcLow = MHal_XC_R2BYTE(REG_SC_BK79_1C_L);
            u32CrcHigh = MHal_XC_R2BYTE(REG_SC_BK79_1D_L);
            break;
        case 2:
            u32CrcLow = MHal_XC_R2BYTE(REG_SC_BK79_1E_L);
            u32CrcHigh = MHal_XC_R2BYTE(REG_SC_BK79_1F_L);
            break;
        default:
            printk("%s %d: u8PkgIdx=%d is illegal!!!\n", __FUNCTION__, __LINE__, u8PkgIdx);
            return FALSE;
    }
#endif
    if ((u32CrcLow == 0) && (u32CrcHigh == 0))
    {
        bCRCPass = TRUE;
    }

    return bCRCPass;
}

MS_BOOL MHal_XC_VSIF_Dolby_Status(void)
{
    // step 1: check length
    if( DOLBY_VSIF_LEN != MHal_XC_R2BYTEMSK(REG_HDMI_DUAL_0_31_L,0x00FF) )
    {
        return FALSE;
    }

    // step 2: check PB6 - PB24
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTEMSK(REG_HDMI_DUAL_0_34_L,0xFF00) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_35_L) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_36_L) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_37_L) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_38_L) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_39_L) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_3A_L) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_3B_L) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_3C_L) )
    {
        return FALSE;
    }
    if( DOLBY_VSIF_PB_VALUE != MHal_XC_R2BYTE(REG_HDMI_DUAL_0_3D_L) )
    {
        return FALSE;
    }

    return TRUE;
}


BOOL MHAL_XC_IsCFDInitFinished(MS_U8 eWin)
{
    return bCFDInitFinished[eWin];
}

BOOL MHal_XC_GetHdmiMetadata(MS_U8 *pu8Metadata, MS_U16 *pu16MetadataLength)
{
    MS_BOOL bReturn = FALSE;
    int i = 0, j = 0;
    MS_U8 *pu8Data = pu8Metadata;
    MS_U16 u16MetadataLength = 0;
    MS_U8 u8MetadataLengthLow = 0;
    MS_U8 u8MetadataLengthHigh = 0;
    MS_U8 u8PacketType = 0;
    MS_U8 u8MetadataType = 0;

#define DOLBY_FIRST_PACKET_REMAIN_MAX_LENGTH (XC_HDR_DOLBY_PACKET_LENGTH - XC_HDR_DOLBY_PACKET_HEADER - XC_HDR_DOLBY_PACKET_TAIL - XC_HDR_DOLBY_METADATA_LENGTH_BIT)
#define DOLBY_OTHER_PACKET_REMAIN_MAX_LENGTH (XC_HDR_DOLBY_PACKET_LENGTH - XC_HDR_DOLBY_PACKET_HEADER - XC_HDR_DOLBY_PACKET_TAIL)
#if (DOLBY_VISION_SW_DESCRAMBLE_ENABLE == 1)
    struct timespec startTs;
    struct timespec endTs;
    getnstimeofday(&startTs);
#endif


    // enable descrb read back
    MHal_XC_W2BYTEMSK(REG_SC_BK79_12_L, 1 << 11, BIT(11));
    for (i = 0; i < 3; i++)
    {
        if (MHal_XC_IsCRCPass(i))
        {
            MS_U16 u16Value = _HAL_XC_ReadMetadata2BYTE(i * XC_HDR_DOLBY_PACKET_LENGTH / 2);
            u8PacketType = ((u16Value & 0xFF) >> 6) & 0x3;
            u8MetadataType = ((u16Value & 0xFF) >> 4) & 0x3;

            u8MetadataLengthHigh = _HAL_XC_ReadMetadata2BYTE(1 + i * XC_HDR_DOLBY_PACKET_LENGTH / 2) >> 8;
            u8EOSflag =  (((MS_U8)(_HAL_XC_ReadMetadata2BYTE(1 + i * XC_HDR_DOLBY_PACKET_LENGTH / 2)))&0x01);
            u16Value = _HAL_XC_ReadMetadata2BYTE(2 + i * XC_HDR_DOLBY_PACKET_LENGTH / 2);
            u8MetadataLengthLow = u16Value & 0xFF;
            u16MetadataLength = (u8MetadataLengthHigh << 8) | u8MetadataLengthLow;
#if (DOLBY_VISION_SW_DESCRAMBLE_ENABLE == 1)
            if (u16MetadataLength > (DOLBY_FIRST_PACKET_REMAIN_MAX_LENGTH + DOLBY_OTHER_PACKET_REMAIN_MAX_LENGTH))
            {
                // exceed SRAM max length, go to sw descramble.
                break;
            }
#endif
            *pu8Data = (u16Value >> 8);
            pu8Data++;

            for (j = (XC_HDR_DOLBY_PACKET_HEADER + XC_HDR_DOLBY_METADATA_LENGTH_BIT) / 2 + 1; j < XC_HDR_DOLBY_PACKET_LENGTH / 2; j++)
            {
                *((MS_U16 *)pu8Data) = _HAL_XC_ReadMetadata2BYTE(j + i * XC_HDR_DOLBY_PACKET_LENGTH / 2);
                pu8Data = ((MS_U16 *)pu8Data) + 1;
            }
            break;
        }
    }

    // all crcs is fail
    if (i >= 3)
    {
        //printk("Get hdmi metadata, crc check fail!\n");
        bReturn = FALSE;
        goto EXIT;
    }

    // metadata_type = 0b00: Dolby Vision metadata
    // metadata_type = 0b01~0b11: Reserved for future use (for example, Dolby AS3D)
    if (u8MetadataType != 0)
    {
        printk("Get hdmi metadata, metadata type is incorrect!\n");
        bReturn = FALSE;
        goto EXIT;
    }

    // packet_type = 0b00: A single packet carries an entire Dolby Vision metadata structure.
    // packet_type = 0b01: The first packet if multiple packets carry one Dolby Vision metadata structure.
    // packet_type = 0b10: Intermediate packets if multiple packets carry one Dolby Vision metadata structure.
    // packet_type = 0b11: The last packet if multiple packets carry one Dolby Vision metadata structure.
    if ((u8PacketType != 0x00) && (u8PacketType != 0x01))
    {
        printk("Get hdmi metadata, packet type is incorrect!\n");
        bReturn = FALSE;
        goto EXIT;
    }

    if (u16MetadataLength <= DOLBY_FIRST_PACKET_REMAIN_MAX_LENGTH)
    {
        bReturn = TRUE;
    }
    else if (u16MetadataLength > (DOLBY_FIRST_PACKET_REMAIN_MAX_LENGTH + DOLBY_OTHER_PACKET_REMAIN_MAX_LENGTH))
    {
#if (DOLBY_VISION_SW_DESCRAMBLE_ENABLE == 1)
        long long diff = 0;
        MS_U16 u16SWMetadataLen = 0;
        getnstimeofday(&endTs);
        diff = endTs.tv_nsec + ((endTs.tv_sec - startTs.tv_sec) * 1000000000 - startTs.tv_nsec);
        // HW suggest that SW descramble wait 1.6ms after descrambling done.
        while (diff < 1600000)
        {
            getnstimeofday(&endTs);
            diff = endTs.tv_nsec + ((endTs.tv_sec - startTs.tv_sec) * 1000000000 - startTs.tv_nsec);
        }
        u16SWMetadataLen = 0;
        if (MHal_XC_SwDescrambleMetadata(pu8Metadata, &u16SWMetadataLen) != TRUE)
        {
            bReturn = FALSE;
            goto EXIT;
        }

        if (u16SWMetadataLen != u16MetadataLength)
        {
            printk("Metadata length of HW/SW descramble is inconsistent.\n");
            bReturn = FALSE;
            goto EXIT;
        }
        else
        {
            bReturn = TRUE;
        }
#else
        printk("Get hdmi metadata, exceed max length, hw don't support!\n");
        bReturn = FALSE;
        goto EXIT;
#endif
    }
    else
    {
        *pu8Data = _HAL_XC_ReadMetadata2BYTE(1 + 3 * XC_HDR_DOLBY_PACKET_LENGTH / 2) & 0xFF;
        pu8Data++;

        for (j = XC_HDR_DOLBY_PACKET_HEADER / 2 + 1; j < (u16MetadataLength - DOLBY_FIRST_PACKET_REMAIN_MAX_LENGTH) / 2; j++)
        {
            *((MS_U16 *)pu8Data) = _HAL_XC_ReadMetadata2BYTE(j + 3 * XC_HDR_DOLBY_PACKET_LENGTH / 2);
            pu8Data = ((MS_U16 *)pu8Data) + 1;
        }
        bReturn = TRUE;
    }

    *pu16MetadataLength = u16MetadataLength;

EXIT:

    // disable descrb read back
    MHal_XC_W2BYTEMSK(REG_SC_BK79_12_L, 0, BIT(11));

    return bReturn;
}

BOOL MHal_XC_SetInputSourceType(EN_KDRV_XC_INPUT_SOURCE_TYPE enInputSourceType)
{
    _enInputSourceType = enInputSourceType;

    return TRUE;
}

BOOL MHal_XC_SetHDR_DMARequestOFF(MS_BOOL bEnable, MS_BOOL bImmediately)
{
    if (bEnable == TRUE)
    {
        if (bImmediately)
        {
            MHal_XC_W2BYTEMSK(REG_SC_BK42_50_L, BIT(1)|BIT(2), BIT(1)|BIT(2));   //Maxim HDR DMArequest has Left && Right
        }
        else
        {
            KApi_XC_MLoad_WriteCmd(E_CLIENT_MAIN_HDR, REG_SC_BK42_50_L, BIT(1)|BIT(2), BIT(1)|BIT(2));
        }
    }
    else
    {
        if (bImmediately)
        {
            MHal_XC_W2BYTEMSK(REG_SC_BK42_50_L, 0, BIT(1)|BIT(2));
        }
        else
        {
            KApi_XC_MLoad_WriteCmd(E_CLIENT_MAIN_HDR, REG_SC_BK42_50_L, 0, BIT(1)|BIT(2));
        }
    }

    return TRUE;
}

BOOL MHal_XC_SetDMPQBypass(MS_BOOL bEnable)
{
    // BIT(0) = 1 --> SW DM PQ bypass
    //        = 0 --> SW DM PQ go-through
    // BIT(1) = 1 --> SW DM PQ bypass ENABLE
    //        = 0 --> SW DM PQ bypass DISABLE
    if (bEnable == TRUE)
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, BIT(0)|BIT(1), BIT(0)|BIT(1));
    }
    else
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(0)|BIT(1));
    }

    return TRUE;
}

void MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_PATH enDMAPath)
{
    if (enDMAPath == EN_KDRV_XC_HDR_DMA_BYPASS)
    {
        // dma path select
        MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(6));
        // dma path user mode
        MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(7));
    }
    else if (enDMAPath == EN_KDRV_XC_HDR_DMA_ENABLE)
    {
        // dma path select
        MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 6, BIT(6));
        // dma path user mode
        MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 7, BIT(7));
    }
    else
    {
        // impossible, should not go in here
        printk(" impossible setting, please check function variable = %d\n",enDMAPath);
    }
}

BOOL MHal_XC_SetHDRType(EN_KDRV_XC_HDR_TYPE enHDRType)
{
    if (_enHDRType == enHDRType)
    {
        return TRUE;
    }

    _enHDRType = enHDRType;

    if (IS_HDMI_DOLBY)
    {
        MHal_XC_UpdatePath(E_KDRV_XC_HDR_PATH_DOLBY_HDMI);
        MHal_XC_DisableHDMI422To444();
        MHal_XC_EnableAutoSeamless(TRUE);
        if (MHal_XC_SupportDolbyHDR() && IS_DOLBY_HDR(MAIN_WINDOW))
        {
#ifdef DOLBY_1_4_2
            DoVi_Set_HDRInputType(/*_pstDmConfig*/NULL, DOVI_DM_INPUT_TYPE_HDMI);
#else
            DoVi_Set_HDRInputType(_pstDmConfig, DOVI_DM_INPUT_TYPE_HDMI);
#endif
        }
    }
    else if (IS_OTT_DOLBY)
    {
        MHal_XC_UpdatePath(E_KDRV_XC_HDR_PATH_DOLBY_OTT_DUAL);
        MHal_XC_EnableAutoSeamless(FALSE);
        if (MHal_XC_SupportDolbyHDR() && IS_DOLBY_HDR(MAIN_WINDOW))
        {
#ifdef DOLBY_1_4_2
            DoVi_Set_HDRInputType(/*_pstDmConfig*/NULL, DOVI_DM_INPUT_TYPE_OTT);
#else
            DoVi_Set_HDRInputType(_pstDmConfig, DOVI_DM_INPUT_TYPE_OTT);
#endif
        }
    }
    else if (IS_HDMI_OPEN)
    {
        MHal_XC_UpdatePath(E_KDRV_XC_HDR_PATH_OPEN_HDMI);
        MHal_XC_EnableAutoSeamless(FALSE);
        if (MHal_XC_SupportDolbyHDR() && IS_DOLBY_HDR(MAIN_WINDOW))
        {
#ifdef DOLBY_1_4_2
            DoVi_Set_HDRInputType(/*_pstDmConfig*/NULL, DOVI_DM_INPUT_TYPE_HDMI);
#else
            DoVi_Set_HDRInputType(_pstDmConfig, DOVI_DM_INPUT_TYPE_HDMI);
#endif
        }
    }
    else if (IS_OTT_OPEN)
    {
        MHal_XC_UpdatePath(E_KDRV_XC_HDR_PATH_OPEN_OTT);
        MHal_XC_EnableAutoSeamless(FALSE);
        if (MHal_XC_SupportDolbyHDR() && IS_DOLBY_HDR(MAIN_WINDOW))
        {
#ifdef DOLBY_1_4_2
            DoVi_Set_HDRInputType(/*_pstDmConfig*/NULL, DOVI_DM_INPUT_TYPE_OTT);
#else
            DoVi_Set_HDRInputType(_pstDmConfig, DOVI_DM_INPUT_TYPE_OTT);
#endif
        }
    }
    else if(IS_MVOP_TCH)
    {
       MHal_XC_UpdatePath(E_KDRV_XC_HDR_PATH_TCH_MVOP);
    }

    return TRUE;
}

extern MS_BOOL _Is_HDR10_In_Dolby;
extern MS_BOOL _Is_HDR10_In_Dolby_HDMI;
void MHal_XC_Hdr10_Enable(MS_BOOL bIsOn)
{
    if( MHal_XC_R2BYTEMSK(REG_SC_BK7A_42_L,0xFFFF) == 0x0000)
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_42_L, 0x1000, 0xFFFF);
    }

    if( MHal_XC_R2BYTEMSK(REG_SC_BK7A_54_L,0xFFFF) == 0x0000)
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_54_L, 0x01EE, 0xFFFF);
    }

    if( MHal_XC_R2BYTEMSK(REG_SC_BK7A_55_L,0xFFFF) == 0x0000)
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_55_L, 0x4108, 0xFFFF);
    }
    if(0 == bIsOn)
    {
        MHal_XC_WriteByteMask(REG_SC_BK79_06_H, 0x80, 0x80);
        MHal_XC_WriteByteMask(REG_SC_BK79_07_L, 0x00, 0x23);
        MHal_XC_WriteByteMask(REG_SC_BK79_07_H, 0x00, 0x8C);
        MHal_XC_WriteByteMask(REG_SC_BK79_03_H, 0x00, 0xC0);
        MHal_XC_WriteByteMask(REG_SC_BK79_04_H, 0x05, 0x1F);
        MHal_XC_WriteByteMask(REG_SC_BK7A_01_H, 0x00, 0xFF);
        MHal_XC_WriteByteMask(REG_SC_BK7B_02_L, 0x00, 0x02);
        //_Is_HDR10_In_Dolby=FALSE;
    }
    else
    {
        MHal_XC_WriteByteMask(REG_SC_BK79_06_H, 0x00, 0x80);
        //MHal_XC_WriteByteMask(REG_SC_BK79_07_L, 0x22, 0x23);
        MHal_XC_WriteByteMask(REG_SC_BK79_07_L, 0x02, 0xFF);
        MHal_XC_WriteByteMask(REG_SC_BK79_07_H, 0x80, 0x8C);
        MHal_XC_WriteByteMask(REG_SC_BK79_03_H, 0x00, 0xC0);
        MHal_XC_WriteByteMask(REG_SC_BK79_04_H, 0x05, 0x1F);
        MHal_XC_WriteByteMask(REG_SC_BK7A_01_H, 0xA0, 0xFF);
        MHal_XC_WriteByteMask(REG_SC_BK7B_02_L, 0x02, 0x02);
        MHal_XC_WriteByteMask(REG_SC_BK7A_01_L, 0x00, 0x01);
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_02_L, 0x8003, 0xFFFF);
        // _Is_HDR10_In_Dolby=TRUE;
    }

}
void Mha_XC_HDR10_Switch_Case(EN_KDRV_XC_HDR_PATH eTypeHDR10,MS_BOOL bIsOn)
{
    if( eTypeHDR10 == E_KDRV_XC_HDR_PATH_OPEN_OTT )
    {
        if(bIsOn)
            _Is_HDR10_In_Dolby = TRUE;
        else
            _Is_HDR10_In_Dolby = FALSE;

    }
    else if( eTypeHDR10 == E_KDRV_XC_HDR_PATH_OPEN_HDMI )
    {
        if(bIsOn)
            _Is_HDR10_In_Dolby_HDMI = TRUE;
        else
            _Is_HDR10_In_Dolby_HDMI = FALSE;
    }
}

void MHal_XC_HLGPatch(MS_BOOL bEnable)
{
    if(bEnable)
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_42_L, 0x1000, 0x1000);
    }
}

BOOL MHal_XC_UpdatePath(EN_KDRV_XC_HDR_PATH enPath)
{
    switch (enPath)
    {
        case E_KDRV_XC_HDR_PATH_DOLBY_HDMI:
        {
            // hdmi422 to 14b for dm: 00: msb  01: lsb  11: lsb/C-signed
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 0, 0x0300);
            // hdr to ip2 focre ack
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 0, BIT(15));
            // dc path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(0));
            // ip2 path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 1, BIT(1));
            // ipsrc_out_pac
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(4));
            // ipsrc to dm data pac user mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 5, BIT(5));
            MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_ENABLE);
            // EL / BL off
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 10, BIT(10));
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 11, BIT(11));
            // open hdr enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(15));
            // PQ bypass control
            MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0, BIT(0));
            MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0x8000, 0xF000);
            if (MHal_XC_SupportDolbyHDR() == TRUE)
            {
                MHal_XC_W2BYTEMSK(REG_SC_BK7A_02_L, 0x3, 0x007F);
                MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0x0, 0x007C);
            }
            break;
        }
        case E_KDRV_XC_HDR_PATH_DOLBY_OTT_SINGLE:
        {
            // hdmi422 to 14b for dm: 00: msb  01: lsb  11: lsb/C-signed
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 0, 0x0300);
            // hdr to ip2 focre ack
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 1 << 15, BIT(15));
            // dc path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1, BIT(0));
            // ip2 path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 1, BIT(1));
            // ipsrc_out_pac
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(4));
            // ipsrc to dm data pac user mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 5, BIT(5));
            MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_BYPASS);
            // EL / BL off
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 10, BIT(10));
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(11));
            // open hdr enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(15));
            // PQ bypass control
            MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0, BIT(0));
            MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0xB000, 0xF000);
            if (MHal_XC_SupportDolbyHDR() == TRUE)
            {
                MHal_XC_W2BYTEMSK(REG_SC_BK7A_02_L, 0x3, 0x007F);
                MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0x0, 0x007C);
            }
            // bypass XVYCC control
            MHal_XC_W2BYTEMSK(REG_SC_BK25_01_L, BIT(8), BIT(8));
            break;
        }
        case E_KDRV_XC_HDR_PATH_DOLBY_OTT_UI_OFF:
        {
            // hdr to ip2 focre ack
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 1 << 15, BIT(15));
            // dc path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(0));
            // ip2 path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(1));
            MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_BYPASS);
            // disable FRC mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(14));
            // hdr dma seamless with rfbl mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(15));
            // hdr auto seamless md
            MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, 0, BIT(15));
            break;
        }
        case E_KDRV_XC_HDR_PATH_DOLBY_HDMI_UI_OFF:
        {
            // hdr to ip2 focre ack
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 1 << 15, BIT(15));
            // dc path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(0));
            // ip2 path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(1));
            MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_BYPASS);
            // disable FRC mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(14));
            // hdr dma seamless with rfbl mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(15));
            // hdr auto seamless md
            MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, 0, BIT(15));
            break;
        }
        case E_KDRV_XC_HDR_PATH_DOLBY_OTT_DUAL:
        {
            // hdmi422 to 14b for dm: 00: msb  01: lsb  11: lsb/C-signed
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 0, 0x0300);
            // hdr to ip2 focre ack
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 1 << 15, BIT(15));
            // dc path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1, BIT(0));
            // ip2 path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 1, BIT(1));
            // ipsrc_out_pac
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(4));
            // ipsrc to dm data pac user mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 5, BIT(5));
            MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_BYPASS);
            // EL / BL off
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(10));
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(11));
            // open hdr enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(15));
            // PQ bypass control
            MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0, BIT(0));
            MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0xB000, 0xF000);
            if (MHal_XC_SupportDolbyHDR() == TRUE)
            {
                MHal_XC_W2BYTEMSK(REG_SC_BK7A_02_L, 0x3, 0x007F);
                MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0x0, 0x007C);
            }
            // bypass XVYCC control
            MHal_XC_W2BYTEMSK(REG_SC_BK25_01_L, BIT(8), BIT(8));
            break;
        }
        case E_KDRV_XC_HDR_PATH_OPEN_HDMI:
        case E_KDRV_XC_HDR_PATH_OPEN_OTT:
        {
            // patch here, after CFD finishes we have to remove all these registers
            {
                //MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(5));
                //MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0, BIT(0));
                if (MHal_XC_SupportDolbyHDR() == TRUE)
                {
                    //MHal_XC_W2BYTEMSK(REG_SC_BK7A_02_L, 0x18, 0x007F);
                    if (MHal_XC_HDMI_Color_Data_Format(_stCfdHdmi[0].u8PixelFormat) == E_CFD_MC_FORMAT_YUV422)
                    {
                        //MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(4));
                        //MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0x8000, 0xF000);
                    }
                    else
                    {
                        //MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 1 << 4, BIT(4));
                        //MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0x0, 0xF000);
                    }
#ifdef HDR10_DOLBY
                    MHal_XC_W2BYTEMSK(REG_SC_BK25_01_L, 0, BIT(8));
                    MHal_XC_Hdr10_Enable(TRUE);
                    Mha_XC_HDR10_Switch_Case(E_KDRV_XC_HDR_PATH_OPEN_OTT,TRUE);
#endif
                }
            }
            break;
        }
        case E_KDRV_XC_HDR_PATH_ORIGIN:
        {
            // hdr to ip2 focre ack
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 1 << 15, BIT(15));
            // dc path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(0));
            // ip2 path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(1));
            MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_BYPASS);
            // disable FRC mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(14));
            // hdr dma seamless with rfbl mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(15));
            // hdr auto seamless md
            MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, 0, BIT(15));
            break;
        }
        case E_KDRV_XC_HDR_PATH_NON_HDR_HDMI:
        {
            // hdr to ip2 focre ack
            MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 1 << 15, BIT(15));
            // dc path enable
            MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(0));
            // disable FRC mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(14));
            // hdr dma seamless with rfbl mode
            MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(15));
            // hdr auto seamless md
            MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, 0, BIT(15));
            break;
        }
        case E_KDRV_XC_HDR_PATH_TCH_MVOP:
        {
            // enable scaler to mvop hand shaking mode
            MHal_XC_W2BYTEMSK(REG_SC_BK02_10_L, 0x25, 0x25);

            break;
        }
        default:
        {
            break;
        }
    }

    return TRUE;
}

BOOL MHal_XC_EnableEL(MS_BOOL bEnable)
{
    MS_U16 u16Value = (bEnable == TRUE) ? 0 : 1;
    MS_U16 u16CurVal = MHal_XC_R2BYTEMSK(REG_SC_BK79_07_L, BIT(10)) >> 10;
    if (u16CurVal != u16Value)
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, u16Value << 10, BIT(10));
    }

    return TRUE;
}

BOOL MHal_XC_EnableAutoSeamless(MS_BOOL bEnable)
{
    // hdr auto seamless md
    if (bEnable)
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, 1 << 15, BIT(15));
    }
    else
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, 0, BIT(15));
    }

    return TRUE;
}

BOOL MHal_XC_DisableHDMI422To444()
{
    MHal_XC_W2BYTEMSK(REG_SC_BK01_61_L, 1, BIT(0));
    MHal_XC_W2BYTEMSK(REG_SC_BK01_61_L, 0, BIT(1));
    MHal_XC_W2BYTEMSK(REG_SC_BK01_61_L, 0, BIT(2));

    return TRUE;
}

BOOL MHal_XC_SetHDRWindow(MS_U16 u16Width, MS_U16 u16Height)
{
    // this control is now moved to utopia, which should be
    // set with DS
    //MHal_XC_W2BYTEMSK(REG_SC_BK79_08_L, u16Width, 0x1FFF);
    //MHal_XC_W2BYTEMSK(REG_SC_BK79_09_L, u16Height, 0x0FFF);
    return TRUE;
}

BOOL MHal_XC_Set3DLutInfo(MS_U8* pu8Data, MS_U32 u32Size)
{
#ifdef DOLBY_1_4_2
    int iResult = DoVi_Replace_3DLut(pu8Data, u32Size);
#else
    int iResult = DoVi_Replace_3DLut(&(_pstDmRegTable->st3D), pu8Data, u32Size);
#endif
    if (iResult < 0)
    {
        printk("3dLut has problems!\n");
        return FALSE;
    }

    return TRUE;
}

void MHal_XC_SetShareMemInfo(ST_KDRV_XC_SHARE_MEMORY_INFO _stShareMemInfo)
{
    _stShareMemInfo_=_stShareMemInfo;

}

void MHal_XC_SetCurrentIndex(MS_U8 u8CurrentIndex)
{
    _u8CurrentIndex=u8CurrentIndex;
}

void MHal_XC_CalculateFrameHDRInfo(MS_U32 u32RPtr, ST_KDRV_XC_HDR_INFO_ENTRY *pstEntry, MS_U8 *pu8InputMDAddr,
                                   MS_U8 *pu8InputMDAddr_Cached, MS_U8 *pu8LutsAddr, MS_U8 *pu8LutsAddr_Cached, MS_U8 *pu8RegSetAddr,
                                   MS_U8 *pu8RegSetAddr_Cached, ST_KDRV_XC_HDR_DOLBY_MEMORY_FORMAT_EX *pstDolbyHDRShareMem,
                                   ST_KDRV_XC_SHARE_MEMORY_INFO *pstShareMemInfo,MS_BOOL UserMode)
{
#if DOLBY_OTT_CACHED_BUFFER
    unsigned long ulFlushAddr[2]= {0,0};
    unsigned long ulFlushLen[2]= {0,0};
#endif
    do
    {
        MS_BOOL bRetVal = FALSE;
        // Get InputMD data
        MHal_XC_PrepareDolbyInfo();

        MS_U16 u16MDMiuSel = getDolbyHDRMDMiuSel(_pstDolbyHDRShareMem);
        MS_U32 u32InputMDAddr = pstEntry->u32InputMDAddr;
        MS_U64 u64Offset = 0;

        if (MHal_XC_GetMiuOffset(u16MDMiuSel, &u64Offset) != TRUE)
        {
            printk("GetMiuOffset u16MDMiuSel: %d fail.\n", u16MDMiuSel);
            continue;
        }

        if (u32InputMDAddr != 0)
        {
            MS_U8 *pu8InputMDData = NULL;
            ST_KDRV_XC_HDR_INPUT_MD_FORMAT stFormatInfo;

            pu8InputMDData = ((MS_U8 *)_pstDolbyHDRShareMem) + (u32InputMDAddr - _stShareMemInfo.phyBaseAddr);

            if (pu8InputMDData == NULL)
            {
                printk("metadata mmap pa fail.\n");
                continue;
            }

            bRetVal = getDolbyHDRInputMDFormatInfo(pu8InputMDData, &stFormatInfo);

            if (bRetVal == TRUE)
            {
                MS_U32 u32RegsetAddr = 0;
                MS_U16 u16LutMiuSel = 0;
                KHAL_DS_STORED_ADDR_INFO stDSStoredAddr;
                MS_U32 u32LutAddr = 0;
                MS_U32 u32LutSize = 0;
                MS_U8 *pu8LutAddr = NULL;

                if ((stFormatInfo.u32ComposerAddr == 0) && ((stFormatInfo.u32DmAddr == 0)))
                {
                    printk("%d: addr fail.\n", __LINE__);
                    continue;
                }

                XC_KDBG("0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x %d %d\n", stFormatInfo.u8CurrentIndex, stFormatInfo.u32DmLength, stFormatInfo.u32DmAddr,
                        stFormatInfo.u8DmMiuNo, stFormatInfo.u32ComposerLength, stFormatInfo.u32ComposerAddr, stFormatInfo.u8ComposerMiuNo,
                        stFormatInfo.bEnableComposer, stFormatInfo.bEnableDM);
                // get stored regset addr
                u32RegsetAddr = pstEntry->u32HDRRegsetAddr;
                if (u32RegsetAddr == 0)
                {
                    printk("%d: u32HDRRegsetAddr fail.\n", __LINE__);
                    continue;
                }

                u16LutMiuSel = getDolbyHDRLutMiuSel(_pstDolbyHDRShareMem);
                if (MHal_XC_GetMiuOffset(u16LutMiuSel, &u64Offset) != TRUE)
                {
                    printk("%d: MHal_XC_GetMiuOffset fail.\n", __LINE__);
                    continue;
                }
                // set stored regset addr into DS

                memset(&stDSStoredAddr, 0, sizeof(KHAL_DS_STORED_ADDR_INFO));
                stDSStoredAddr.stIPMAddr.phyDSAddr = (MS_PHY)((MS_U64)u32RegsetAddr + u64Offset);
                stDSStoredAddr.stIPMAddr.u32DSSize = HDR_OUTPUT_REG_SIZE;
#if DOLBY_OTT_CACHED_BUFFER
                stDSStoredAddr.stIPMAddr.virtDSAddr = (MS_VIRT)(pu8RegSetAddr_Cached+ (u32RegsetAddr - (_stShareMemInfo.phyBaseAddr + HDR_MEM_COMMON_ENTRY_SIZE + HDR_TOTAL_INPUT_MD_SIZE)));
                ulFlushAddr[0]=stDSStoredAddr.stIPMAddr.virtDSAddr;
                ulFlushLen[0]=stDSStoredAddr.stIPMAddr.u32DSSize;
#else
                stDSStoredAddr.stIPMAddr.virtDSAddr = (MS_VIRT)(_pu8RegSetAddr + (u32RegsetAddr - (_stShareMemInfo.phyBaseAddr + HDR_MEM_COMMON_ENTRY_SIZE + HDR_TOTAL_INPUT_MD_SIZE)));
#endif
                KHal_SC_Set_DS_StoredAddr(&stDSStoredAddr);

                u32LutAddr = pstEntry->u32HDRLutAddr;
                u32LutSize = getDolbyHDRLutSize(u32RPtr);
#if DOLBY_OTT_CACHED_BUFFER
                pu8LutAddr = (MS_U8 *)(pu8LutsAddr_Cached+ (u32LutAddr - (_stShareMemInfo.phyBaseAddr + HDR_MEM_COMMON_ENTRY_SIZE + HDR_TOTAL_INPUT_MD_SIZE + HDR_TOTAL_REGSET_SIZE)));
                ulFlushAddr[1]=pu8LutAddr;
                ulFlushLen[1]=u32LutSize;
#else
                pu8LutAddr = (MS_U8 *)(_pu8LutsAddr + (u32LutAddr - (_stShareMemInfo.phyBaseAddr + HDR_MEM_COMMON_ENTRY_SIZE + HDR_TOTAL_INPUT_MD_SIZE + HDR_TOTAL_REGSET_SIZE)));
#endif
                bRetVal = MHal_XC_ConfigHDRAutoDownloadStoredInfo((MS_PHY)((MS_U64)u32LutAddr + u64Offset), pu8LutAddr, u32LutSize);
                if (bRetVal == FALSE)
                {
                    printk("MHal_XC_ConfigAutoDownload E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR fail.\n");
                    continue;
                }

                // update composer
                if (stFormatInfo.u32ComposerAddr != 0)
                {
                    MS_U8 *pu8CompData = NULL;

                    if (stFormatInfo.u32ComposerLength == 0)
                    {
                        printk("%d: u32ComposerLength fail.\n", __LINE__);
                        continue;
                    }

                    if (MHal_XC_GetMiuOffset(stFormatInfo.u8ComposerMiuNo, &u64Offset) != TRUE)
                    {
                        printk("%d: MHal_XC_GetMiuOffset fail.\n", __LINE__);
                        continue;
                    }
#if DOLBY_OTT_CACHED_BUFFER
                    pu8CompData = pu8InputMDAddr_Cached+ (stFormatInfo.u32ComposerAddr - pstDolbyHDRShareMem->u32MDAddr);
                    Chip_Inv_Cache_Range(pu8CompData, stFormatInfo.u32ComposerLength);
#else
                    pu8CompData = _pu8InputMDAddr + (stFormatInfo.u32ComposerAddr - pstDolbyHDRShareMem->u32MDAddr);
#endif

                    if (pu8CompData == NULL)
                    {
                        printk("composer mmap pa fail.\n");
                        continue;
                    }

                    // set compser data into color-format hw
                    if(MHal_XC_SetDolbyCompData(pu8CompData, stFormatInfo.u32ComposerLength)== FALSE)
                    {
                        printk("MHal_XC_SetDolbyCompData fail, u32RPtr: %d, CFDReady: 0x%llx\n", _pstDolbyHDRShareMem->u32RdPtr, _pstDolbyHDRShareMem->u64Reserved);
                    }
                }

                if (stFormatInfo.u32DmLength == 0)
                {
                    printk("%d: u32DmLength fail.\n", __LINE__);
                    continue;
                }

                if (MHal_XC_GetMiuOffset(stFormatInfo.u8DmMiuNo, &u64Offset) != TRUE)
                {
                    printk("%d: MHal_XC_GetMiuOffset fail.\n", __LINE__);
                    continue;
                }
                // turn on settings
                MHal_XC_SetInputSourceType(E_KDRV_XC_INPUT_SOURCE_OTT);

                //implement UI Dolby HDR on/off
                //so this control be moved to MDrv_XC_ProcessCFDIRQ()
                //MHal_XC_SetHDRType(E_KDRV_XC_HDR_DOLBY);

                // update metadata
                if ((stFormatInfo.bEnableDM) && (stFormatInfo.u32DmAddr != 0))
                {
                    MS_U8 *pu8Metadata = NULL;
#if DOLBY_OTT_CACHED_BUFFER
                    pu8Metadata = pu8InputMDAddr_Cached+ (stFormatInfo.u32DmAddr - pstDolbyHDRShareMem->u32MDAddr);
                    Chip_Inv_Cache_Range(pu8Metadata, stFormatInfo.u32DmLength);
#else
                    pu8Metadata = _pu8InputMDAddr + (stFormatInfo.u32DmAddr - pstDolbyHDRShareMem->u32MDAddr);
#endif

                    if (pu8Metadata == NULL)
                    {
                        printk("metadata mmap pa fail.\n");
                        continue;
                    }
#ifdef HDR10_DOLBY
                    if(_Is_HDR10_In_Dolby)
                    {
                        MHal_XC_SetDolbyMetaData(pu8Metadata, stFormatInfo.u32DmLength,E_KDRV_XC_CFD_HDR_TYPE_OPEN_OTT_IN_DOLBY);
                    }
                    else
                    {
                        MHal_XC_SetDolbyMetaData(pu8Metadata, stFormatInfo.u32DmLength,E_KDRV_XC_CFD_HDR_TYPE_DOLBY);
                    }
#else
                    // set metadata into color-format hw
                    if (MHal_XC_SetDolbyMetaData(pu8Metadata, stFormatInfo.u32DmLength) == FALSE)
                    {
                        printk("MHal_XC_SetDolbyMetaData fail.\n");
                    }
#endif
                }

                pstEntry->u32HDRLutSize = MHal_XC_GetHDRAutoDownloadStoredSize();
                pstEntry->u32HDRRegsetSize = MHal_XC_GetRegsetCnt();
            }
        }
        else
        {
            pstEntry->u32HDRLutSize = MHal_XC_GetHDRAutoDownloadStoredSize();
            pstEntry->u32HDRRegsetSize = MHal_XC_GetRegsetCnt();
        }

        pstEntry->u16CFDReady = TRUE;
#if DOLBY_OTT_CACHED_BUFFER
        if (ulFlushAddr[0] != 0)
        {
            Chip_Flush_Cache_Range(ulFlushAddr[0],ulFlushLen[0]);
        }
        if (ulFlushAddr[1] != 0)
        {
            Chip_Flush_Cache_Range(ulFlushAddr[1],ulFlushLen[1]);
        }
#endif
#if DOLBY_GD_ENABLE
        if (MHal_XC_IsGDEnabled())
        {
            MS_U16 u16GDVal = MHal_XC_GetGDValue();
            ST_KDRV_XC_HDR_GD_FORMAT stHDRGDInfo;
            memset(&stHDRGDInfo, 0, sizeof(ST_KDRV_XC_HDR_GD_FORMAT));
            stHDRGDInfo.u16GDValue = u16GDVal;
            stHDRGDInfo.u16GDReady = TRUE;
            setDolbyHDRGDInfo(u32RPtr, &stHDRGDInfo);
        }
#endif
        setDolbyHDRInfoEntry(_pstDolbyHDRShareMem, u32RPtr, pstEntry);
        if(UserMode)
        {
            incDolbyHDRMemRPtr(_pstDolbyHDRShareMem,pstEntry->u16Entry_Skip);
        }
    }
    while(0);
}
BOOL MHal_XC_UpdateDolbyPQSetting(MS_U32 enType, void* pParam)
{
#ifdef DOLBY_1_4_2
    switch (enType)
    {
        case EN_DOLBY_PQ_BACKLIGHT:
        {
            MS_U16* pu16Backlight = (MS_U16*) pParam;
            DoVi_UpdateBackLight(*pu16Backlight);

            break;
        }

        default:
            // do nothing
            break;
    }

#endif
    return TRUE;
}

void MHal_XC_SetDolbyStatus(MS_U8 u8Status, MS_U8 u8Mask)
{
    _u8DolbyStatus = (_u8DolbyStatus & ~u8Mask) | (u8Status & u8Mask);
}

BOOL MHal_XC_GetDolbyStatus(void)
{
    return _u8DolbyStatus;

}

#ifdef HDR10_DOLBY
BOOL MHal_XC_SetDolbyMetaData(MS_U8* pu8Data, MS_U32 u32Size,EN_KDRV_XC_CFD_HDR_TYPE u32Hdr_Type)
#else
BOOL MHal_XC_SetDolbyMetaData(MS_U8* pu8Data, MS_U32 u32Size)
#endif
{
#ifdef DOLBY_1_4_2
    // DV_1_4_1
    int dm_md_size = -1;
#ifdef HDR10_DOLBY
    if(u32Hdr_Type == E_KDRV_XC_CFD_HDR_TYPE_OPEN_OTT_IN_DOLBY)
    {
        DoVi_isOpenHdr(1);
        printk("This is DOLBY HDR10 OTT\n");
        //please add your paser here for HDR10
        //return TRUE;  //YOU NEED TO CHECK IS PART.
    }
    else if(u32Hdr_Type == E_KDRV_XC_CFD_HDR_TYPE_OPEN_HDMI_IN_DOLBY)
    {
        DoVi_isOpenHdr(1);
        printk("This is DOLBY HDR10 HDMI\n");

        //please add your paser here for HDR10
        // return TRUE;  //YOU NEED TO CHECK IS PART.
    }
    else
#endif
    {
        DoVi_isOpenHdr(0);
        dm_md_size = DoVi_DmSetDolbyMetaData(pu8Data, u32Size);

        if (dm_md_size < 0)
        {
            printk("Metadata has problems! iResult: %d\n", dm_md_size);
            return FALSE;
        }

    }
    // Handle error if metadata is corrupted

    DoVi_setHdrMetadata(pu8Data);
    // DV_1_4_1

    DoVi_DmFrameDeCalculate();

    DoVi_DmBlankingUpdate();

    return TRUE;
#else
    DoVi_MdsExt_t stMdsExt;
    int iResult = 0;

    memset((MS_U8*)&stMdsExt, 0, sizeof(DoVi_MdsExt_t));
    // Read metadata of this frame
    iResult = DoVi_DmReadMetadata(&stMdsExt, pu8Data, u32Size, _pstDmConfig);
    // Handle error if metadata is corrupted
    if (iResult < 0)
    {
        printk("Metadata has problems! iResult: %d\n", iResult);
        return FALSE;
    }
    else
    {
        //DoVi_DmDumpMetadata(&stMdsExt);
        // Calculate Registers and LUT contents during frame DE
        DoVi_DmFrameDeCalculate(_pstDmRegTable, _pstDmConfig, &stMdsExt, _pstCompConfExt);
        // Update Registers and LUT contents at blanking
        DoVi_DmBlankingUpdate(_pstDmRegTable);
    }
#endif
    return TRUE;
}

BOOL MHal_XC_SetDolbyCompData(MS_U8* pu8Data, MS_U32 u32Size)
{
#ifdef DOLBY_1_4_2
    DoVi_isOpenHdr(0);
    int iResult = DoVi_CompReadMetadata(pu8Data, u32Size);

    if (iResult < 0)
    {
        printk("Composer has problems!\n");
        return FALSE;
    }
    else
    {
        //DoVi_CompDumpConfig(_pstCompConfExt);
        //set compser into color-fomat-ctrl hw

        DoVi_CompFrameDeCalculate();

        DoVi_CompBlankingUpdate();

    }
#else
    int iResult = DoVi_CompReadMetadata(_pstCompConfExt, pu8Data, u32Size);
    if (iResult < 0)
    {
        printk("Composer has problems!\n");
        return FALSE;
    }
    else
    {
        //DoVi_CompDumpConfig(_pstCompConfExt);
        //set compser into color-fomat-ctrl hw
        DoVi_CompFrameDeCalculate(_pstCompRegTable, _pstCompConfExt);
        DoVi_CompBlankingUpdate(_pstCompRegTable);
    }
#endif
    return TRUE;
}

MS_U32 MHal_XC_GetRegsetCnt()
{
    K_XC_DS_CMDCNT stXCDSCmdCnt;

    DoVi_GetCmdCnt(&stXCDSCmdCnt);

    return stXCDSCmdCnt.u16CMDCNT_IPM;
}

void MHal_XC_GetRegsetCnt_fromCFD(K_XC_DS_CMDCNT *stXCDSCmdCnt)
{
    cfd_GetCmdCnt(stXCDSCmdCnt);
}

void MHal_XC_PrepareDolbyInfo()
{
    DoVi_Prepare_DS(DOLBY_DS_VERSION);
    _u8Version=DOLBY_DS_VERSION;
}

BOOL MHal_XC_EnableHDR(MS_BOOL bEnableHDR)
{
    if (!bEnableHDR)
    {
        // hdr to ip2 focre ack
        MHal_XC_W2BYTEMSK(REG_SC_BK79_06_L, 1 << 15, BIT(15));
        // dc path enable
        MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(0));
        // ip2 path enable
        MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, 0, BIT(1));
        // disable FRC mode
        MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(14));
        // hdr dma seamless with rfbl mode
        MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(15));
        // hdr auto seamless md
        MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, 0, BIT(15));

        MHal_XC_W2BYTE(REG_SC_BK79_7C_L, 0x0000);
        MHal_XC_W2BYTE(REG_SC_BK79_7D_L, 0x0000);

        _enHDRType = E_KDRV_XC_HDR_NONE;
        _enInputSourceType = E_KDRV_XC_INPUT_SOURCE_NONE;
    }
    else
    {
        MHal_XC_W2BYTE(REG_SC_BK79_7C_L, 0xFFFF);
        // BIT 14: debug for dump SRAM
        MHal_XC_W2BYTE(REG_SC_BK79_7D_L, 0xBFFF);

        //When enable auto_dl , the init value should be 0x600 :
        // auto_dl trigger mode0 delay
        // 0x600 is a experimental value
        // this value must set after DS trigger point, for
        // ADL to trigger at the right frame since ADL trigger is controlled by DS
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_71_L, 0x600, 0x7FFF);
    }

    return TRUE;
}

BOOL MHal_XC_SetVRAddress(MS_PHY phyVRAddress, MS_U32 u32VRSize)
{
    if(_abVRInit==FALSE) //prevent vmalloc leak for ioremap
    {
        phys_addr_t tmp_phy;

        tmp_phy = phyVRAddress + ARM_MIU0_BUS_BASE ;

        _VR_client.u32VRlen = u32VRSize;
        _VR_client.phyVRAddr=phyVRAddress;
        if (pfn_valid(__phys_to_pfn(tmp_phy)))
        {
            _VR_client.va_phy = __va(tmp_phy);
        }
        else
        {
            _VR_client.va_phy =  ioremap(tmp_phy, u32VRSize);
        }

        _abVRInit=TRUE;
    }

    return TRUE;
}

BOOL MHal_XC_MuteHDR()
{
    if(bHDRMute==FALSE)
    {
        bHDRMute = TRUE;
        u16BK7A_42_buf = MHal_XC_R2BYTE(REG_SC_BK7A_42_L);
        u16BK7A_54_buf = MHal_XC_R2BYTE(REG_SC_BK7A_54_L);
        u16BK7A_55_buf = MHal_XC_R2BYTE(REG_SC_BK7A_55_L);

        MHal_XC_W2BYTEMSK(REG_SC_BK7A_02_L, BIT(4), BIT(4));
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_42_L, 0, BIT(4));
        MHal_XC_W2BYTE(REG_SC_BK7A_54_L, 0);
        MHal_XC_W2BYTE(REG_SC_BK7A_55_L, 0);
        MHal_XC_W2BYTE(REG_SC_BK7A_59_L, 0x0800);
    }

    return TRUE;
}

BOOL MHal_XC_UnMuteHDR()
{
    if(bHDRMute)
    {
        bHDRMute = FALSE;

        if( MHal_XC_R2BYTE(REG_SC_BK7A_42_L)==0x0000 && MHal_XC_R2BYTE(REG_SC_BK7A_54_L)==0x0000 && MHal_XC_R2BYTE(REG_SC_BK7A_55_L)==0x0000 )
        {
            MHal_XC_W2BYTEMSK(REG_SC_BK7A_42_L, u16BK7A_42_buf, BIT(4));
            MHal_XC_W2BYTE(REG_SC_BK7A_54_L, u16BK7A_54_buf);
            MHal_XC_W2BYTE(REG_SC_BK7A_55_L, u16BK7A_55_buf);
        }
        u16BK7A_42_buf = 0x0;
        u16BK7A_54_buf = 0x0;
        u16BK7A_55_buf = 0x0;
    }

    return TRUE;
}

BOOL MHal_XC_SupportTCH()
{
    return TRUE;
}

BOOL MHal_XC_SupportDolbyHDR()
{
    BOOL bSupportDolbyHdr = FALSE;

    if (!(MHal_XC_DolbySWBonded() || MHal_XC_DolbyHWBonded()))
    {
        bSupportDolbyHdr = TRUE;
    }
    else
    {
        bSupportDolbyHdr = FALSE;
    }

    return bSupportDolbyHdr;
}

BOOL MHal_XC_DolbySWBonded()
{
    MS_U16 u16OldVal = 0;
    MS_U16 u16Val = 0;
    struct timespec startTs;
    struct timespec endTs;
    if (_u16DolbySWBondStatus != -1)
        return _u16DolbySWBondStatus;

    _u16DolbySWBondStatus = FALSE;

    return _u16DolbySWBondStatus;
}

BOOL MHal_XC_DolbyHWBonded()
{
    if (_u16DolbyHWBondStatus != -1)
        return _u16DolbyHWBondStatus;

    if (MHal_XC_R2BYTEMSK(REG_SC_BK79_3F_L, BIT(14)))  //BIT (14) = 1: HW bond or not support Dolby
    {
        _u16DolbyHWBondStatus = TRUE;
    }
    else    //BIT (14) = 0: HW support Dolby
    {
        _u16DolbyHWBondStatus = FALSE;
    }

    return _u16DolbyHWBondStatus;
}

static void _MHal_XC_SetInterrupt(MS_U8 u8IntSrc, MS_BOOL bEnable)
{
    if(bEnable)
    {
        if(u8IntSrc < 16)
        {
            MHal_XC_W2BYTEMSK(REG_SC_BK00_12_L, 0x0, BIT(u8IntSrc));
            MHal_XC_W2BYTEMSK(REG_SC_BK00_14_L, 0x0, BIT(u8IntSrc));
        }
        else
        {
            MHal_XC_W2BYTEMSK(REG_SC_BK00_13_L, 0x0, BIT(u8IntSrc - 16));
            MHal_XC_W2BYTEMSK(REG_SC_BK00_15_L, 0x0, BIT(u8IntSrc - 16));
        }
    }
    else
    {
        if(u8IntSrc < 16)
        {
            MHal_XC_W2BYTEMSK(REG_SC_BK00_12_L, BIT(u8IntSrc), BIT(u8IntSrc));
            MHal_XC_W2BYTEMSK(REG_SC_BK00_14_L, BIT(u8IntSrc), BIT(u8IntSrc));
        }
        else
        {
            MHal_XC_W2BYTEMSK(REG_SC_BK00_13_L, BIT(u8IntSrc - 16), BIT(u8IntSrc - 16));
            MHal_XC_W2BYTEMSK(REG_SC_BK00_15_L, BIT(u8IntSrc - 16), BIT(u8IntSrc - 16));
        }
    }
}

BOOL MHal_XC_InitHDR()
{
    printk("Dolby HDR version is 1.4.2\n");
    // descrb byte reverse
    MHal_XC_W2BYTEMSK(REG_SC_BK79_10_L, 1 << 5, BIT(5));
    // descrb metadata length, 0x180
    MHal_XC_W2BYTEMSK(REG_SC_BK79_11_L, 0x180, 0x1FFF);

    // metadata num
    MHal_XC_W2BYTEMSK(REG_SC_BK79_18_L, 0x3, 0x0003);
    // crc byte reverse
    MHal_XC_W2BYTEMSK(REG_SC_BK79_18_L, 1 << 5, BIT(5));
    // metadata package length, 0x80
    MHal_XC_W2BYTEMSK(REG_SC_BK79_19_L, 0x80, 0x1FFF);
    // force crc error cnt off
    MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, 0x6 << 8, 0x1F00);

    // enable lut use auto_dl
    MHal_XC_W2BYTEMSK(REG_SC_BK7A_70_L, 1 << 15, BIT(15));
    // clear error flag
    MHal_XC_W2BYTEMSK(REG_SC_BK7A_70_L, 0, BIT(9));
    // auto_dl protect
    MHal_XC_W2BYTEMSK(REG_SC_BK7A_70_L, 0x0, 0x001F);
    // auto_dl trigger mode0 delay
    // 0x600 is a experimental value
    // this value must set after DS trigger point, for
    // ADL to trigger at the right frame since ADL trigger is controlled by DS
    MHal_XC_W2BYTEMSK(REG_SC_BK7A_71_L, 0x600, 0x7FFF);
    // auto_dl trigger mode
    MHal_XC_W2BYTEMSK(REG_SC_BK7A_71_L, 0, BIT(15));
    // auto_dl clk select mask
    MHal_XC_W2BYTEMSK(REG_SC_BK7A_72_L, 0, 0x0003);
    // auto_dl trigger vsync select
    MHal_XC_W2BYTEMSK(REG_SC_BK7A_72_L, 1 << 15, BIT(15));

    // HDMI HDR auto seamless mode switch HW decision
    //  EDR and CRC
    //  0 : logic = OR  --> EDR || CRC
    //  1 : logic = AND --> EDR && CRC
    //  HW suggest default value 1
    MHal_XC_W2BYTEMSK(REG_SC_BK79_04_L, BIT(4), BIT(4));

    // disable FRC mode
    MHal_XC_W2BYTEMSK(REG_SC_BK79_03_L, 0, BIT(14));
    // F2 field number
    MHal_XC_W2BYTEMSK(REG_SC_BK42_07_L, 0x2000, 0xE000);
    // hdr dma miu request off
    MHal_XC_W2BYTEMSK(REG_SC_BK42_50_L, BIT(1), BIT(1));
    // hdr clk setting disable
    MHal_XC_W2BYTE(REG_SC_BK79_02_L, 0xFFFF);
    MHal_XC_W2BYTEMSK(REG_SC_BK79_7E_L, 0, BIT(1)|BIT(0));
    // enable irq
    _MHal_XC_SetInterrupt(IRQ_INT_DISP_IP_VS, TRUE);
    _MHal_XC_SetInterrupt(IRQ_INT_DISP_DESCRB, TRUE);
#ifdef IPT
    _MHal_XC_SetInterrupt(IRQ_INT_DISP_IP1, TRUE);
#endif

    _bEnableHDRCLK = FALSE;
    _enHDRType = E_KDRV_XC_HDR_NONE;
    MHal_XC_HDR_Status_Record(FALSE);

    // init color format hw
#ifndef DOLBY_1_4_2
    if (_pstDoVi_PQ_control == NULL)
    {
        _pstDoVi_PQ_control = DoVi_PQSettingControlAllocConfig(); // configuration file
        _pstDoVi_PQ_control->bDovi_UsePQen = 1;
    }

    if (_pstDoVi_TargetDisplayConfig == NULL)
    {
        _pstDoVi_TargetDisplayConfig  = DoVi_PQSettingAllocConfig(); // configuration file
    }

    if (_pstDmRegTable == NULL)
    {
        _pstDmRegTable = MsHdr_DmAllocRegTable(); // register table
    }
    if (_pstDmConfig == NULL)
    {
        _pstDmConfig = DoVi_DmAllocConfig(); // configuration file
    }
    if (_pstDmConfig != NULL)
    {
        DoVi_DmSetDefaultConfig(_pstDmConfig);
    }
    if (_pstCompRegTable == NULL)
    {
        _pstCompRegTable = MsHdr_CompAllocRegTable(); // register table
    }
    if (_pstCompConfExt == NULL)
    {
        _pstCompConfExt = DoVi_CompAllocConfig(); // configuration file
    }
    if (_pstCompConfExt != NULL)
    {
        DoVi_CompSetDefaultConfig(_pstCompConfExt);
    }
#if OTT_AUTO_PAUSE_DETECT
    MHal_XC_ClearPauseStatus();
#endif
#else
    MsHdr_DmAllocRegTable(); // register table
    MsHdr_CompAllocRegTable(); // register table
    DoVi_DmSetDefaultConfig();
    DoVi_CompSetDefaultConfig();
#endif
    return TRUE;
}

BOOL MHal_XC_ExitHDR()
{
    if ((pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR] != NULL) && (_bUnmapAutoDownloadDRAM[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR] == TRUE))
    {
        iounmap(pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR]);
        pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR] = NULL;
    }
#ifndef DOLBY_1_4_2
    // Release resource
    if (_pstDmRegTable != NULL)
    {
        MsHdr_DmFreeRegTable(_pstDmRegTable);
    }
    if (_pstDmConfig != NULL)
    {
        DoVi_DmFreeConfig(_pstDmConfig);
    }
    if (_pstCompRegTable != NULL)
    {
        MsHdr_CompFreeRegTable(_pstCompRegTable);
    }
    if (_pstCompConfExt != NULL)
    {
        DoVi_CompFreeConfig(_pstCompConfExt);
    }
    if (_pstDoVi_TargetDisplayConfig != NULL)
    {
        DoVi_PQSettingFreeConfig(_pstDoVi_TargetDisplayConfig);
    }
    if (_pstDoVi_PQ_control != NULL)
    {
        DoVi_PQSettingControlFreeConfig(_pstDoVi_PQ_control);
    }
#else
    MsHdr_DmFreeRegTable();
    MsHdr_CompFreeRegTable();
#endif
    return TRUE;
}

static BOOL _Hal_XC_Set_Auto_Download_WorkMode(MS_U32 u32RegAddr, MS_U8 u8Bit, EN_KDRV_XC_AUTODOWNLOAD_MODE enMode)
{
    switch (enMode)
    {
        case E_KDRV_XC_AUTODOWNLOAD_TRIGGER_MODE:
        {
            MHal_XC_W2BYTEMSK(u32RegAddr, 0, BIT(u8Bit));
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_ENABLE_MODE:
        {
            MHal_XC_W2BYTEMSK(u32RegAddr, BIT(u8Bit), BIT(u8Bit));
            break;
        }
        default:
            return FALSE;
    }

    return TRUE;
}

BOOL _MHal_XC_PA2VA(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient)
{
    MS_U64 u64Offset = 0;
    MS_BOOL bEnable = _stClientInfo[enClient].bEnable;
    MS_U32  u32MiuNo = _stClientInfo[enClient].u32MiuNo;
    MS_U32  u32Size = _stClientInfo[enClient].u32Size;
    MS_PHY  phyBaseAddr = _stClientInfo[enClient].phyBaseAddr;

    if (MHal_XC_GetMiuOffset(u32MiuNo, &u64Offset) != TRUE)
    {
        printk("Invalid miuno.\n");
        return FALSE;
    }

    if (bEnable == TRUE)
    {
        // reset memory
        if (pfn_valid(__phys_to_pfn(phyBaseAddr + u64Offset)))
        {
            pu8AutoDownloadDRAMBaseAddr[enClient] = __va(phyBaseAddr + u64Offset);
            _bUnmapAutoDownloadDRAM[enClient] = FALSE;
        }
        else
        {
            pu8AutoDownloadDRAMBaseAddr[enClient] = (MS_U8 __iomem *)ioremap_cache(phyBaseAddr + u64Offset, u32Size);
            _bUnmapAutoDownloadDRAM[enClient] = TRUE;
        }

        if (pu8AutoDownloadDRAMBaseAddr[enClient] != NULL)
        {
            memset(pu8AutoDownloadDRAMBaseAddr[enClient], 0, u32Size);
        }
        else
        {
            printk("%s: %d, auto downlaod mmap pa fail.\n", __FUNCTION__, __LINE__);
            return FALSE;
        }
    }
    else
    {
        printk("%s: %d, bEnable = FALSE.\n", __FUNCTION__, __LINE__);
        _enAutoDownloadStatus[enClient] = E_KDRV_XC_AUTO_DOWNLOAD_NONE;
    }

        return TRUE;
    }

BOOL MHal_XC_ConfigAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient, MS_BOOL bEnable,
                                EN_KDRV_XC_AUTODOWNLOAD_MODE enMode, MS_PHY phyBaseAddr, MS_U32 u32Size, MS_U32 u32MiuNo)
{
    mutex_lock(&_adl_mutex);

    switch(enClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR:
        {
            MS_U32 u32RemainMemSize = u32Size;

            // -------HDR client config start------------
            if (bEnable == TRUE && (u32RemainMemSize < AUTODOWNLOAD_CLIENT_HDR_MEM_SIZE))
            {
                printk("ERROR!!! Alloc HDR client memory(0x%x) fail, please enlarge it(now: 0x%x).\n", AUTODOWNLOAD_CLIENT_HDR_MEM_SIZE, u32Size);
                mutex_unlock(&_adl_mutex);
                return FALSE;
            }
            u32RemainMemSize = u32RemainMemSize - AUTODOWNLOAD_CLIENT_HDR_MEM_SIZE;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].phyBaseAddr = phyBaseAddr;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32Size = AUTODOWNLOAD_CLIENT_HDR_MEM_SIZE;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].bEnable = bEnable;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32StartAddr = 0xFFFFFFFF;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32Depth = 0;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].enMode = enMode;

            _MHal_XC_PA2VA(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR);

            //set baseaddr
            MHal_XC_W2BYTE(REG_SC_BK67_29_L, (_stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].phyBaseAddr / BYTE_PER_WORD) & 0x0000FFFF);
            MHal_XC_W2BYTEMSK(REG_SC_BK67_2A_L, ((_stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].phyBaseAddr / BYTE_PER_WORD) >> 16 & 0x0000FFFF), 0x07FF);

            //set work mode
            _Hal_XC_Set_Auto_Download_WorkMode(REG_SC_BK67_28_L, 1, enMode);
            // -------HDR client config end------------

            // -------op2lut client config start------------
            if (bEnable == TRUE && (u32RemainMemSize < AUTODOWNLOAD_CLIENT_OP2LUT_MEM_SIZE))
            {
                printk("ERROR!!! Alloc OP2LUT client memory(0x%x) fail, please enlarge it(now: 0x%x).\n", AUTODOWNLOAD_CLIENT_OP2LUT_MEM_SIZE, u32Size);
                mutex_unlock(&_adl_mutex);
                return FALSE;
            }
            u32RemainMemSize = u32RemainMemSize - AUTODOWNLOAD_CLIENT_OP2LUT_MEM_SIZE;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].phyBaseAddr = phyBaseAddr + AUTODOWNLOAD_CLIENT_HDR_MEM_SIZE;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].u32Size = AUTODOWNLOAD_CLIENT_OP2LUT_MEM_SIZE;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].bEnable = bEnable;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].u32StartAddr = 0xFFFFFFFF;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].u32Depth = 0;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].enMode = enMode;

            _MHal_XC_PA2VA(E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT);

            //set baseaddr
            MHal_XC_W2BYTE(REG_SC_BK67_23_L, (_stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].phyBaseAddr / BYTE_PER_WORD) & 0x0000FFFF);
            MHal_XC_W2BYTEMSK(REG_SC_BK67_24_L, ((_stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].phyBaseAddr / BYTE_PER_WORD) >> 16 & 0x0000FFFF), 0x07FF);

            //set work mode
            _Hal_XC_Set_Auto_Download_WorkMode(REG_SC_BK67_22_L, 1, enMode);
            // -------op2lut client config end------------

            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC:
        {
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].phyBaseAddr = phyBaseAddr;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].u32Size = u32Size;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].bEnable = bEnable;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].u32StartAddr = 0xFFFFFFFF;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].u32Depth = 0;
            _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].enMode = enMode;

            _MHal_XC_PA2VA(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);

            //set baseaddr
            MHal_XC_W2BYTE(REG_SC_BK67_16_L, (phyBaseAddr / BYTE_PER_WORD) & 0x0000FFFF);
            MHal_XC_W2BYTEMSK(REG_SC_BK67_17_L, ((phyBaseAddr / BYTE_PER_WORD) >> 16 & 0x0000FFFF), 0x07FF);

            //set work mode
            _Hal_XC_Set_Auto_Download_WorkMode(REG_SC_BK67_01_L, 15, enMode);
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCOP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE2:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_DEMURA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_0:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCSPTPOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FOOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX:
        default:
        {
            mutex_unlock(&_adl_mutex);
            return FALSE;
        }

    }

    _enAutoDownloadStatus[enClient] = E_KDRV_XC_AUTO_DOWNLOAD_CONFIGURED;
    mutex_unlock(&_adl_mutex);

    return TRUE;
}

static BOOL _Hal_XC_Auto_Download_Format_Hdr_Data(EN_KDRV_XC_AUTODOWNLOAD_SUB_CLIENT enSubClient,
        MS_U8 *pu8AutodownloadDRAMAddr, MS_U8* pu8Data, MS_U32 u32Size, MS_U16 u16StartAddr, MS_U32 *pu32Depth)
{
    MS_U32 u32Depth = *pu32Depth;
    MS_U8* pVirtBaseAddr = NULL;
    MS_U8 *pu8BaseAddr = NULL;
    unsigned int i = 0;
    MS_U32 u32Index = u16StartAddr;
    MS_U32 u32WriteNum = 0;
    MS_U32 u32DataSize = 0;

    if(pu8AutodownloadDRAMAddr == NULL)
    {
        printk("%s: %d pu8AutoDownloadDRAMBaseAddr is NULL.\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

    pVirtBaseAddr = pu8AutodownloadDRAMAddr;
    pu8BaseAddr = (MS_U8 *)pVirtBaseAddr;
    pu8BaseAddr += BYTE_PER_WORD * u16StartAddr;

    u32DataSize = 0;
    switch (enSubClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_TMO:
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_OOTF:
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_RGB3DLUT:
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_3DLUT:
        {
            u32DataSize = u32Size / sizeof(MS_U16);
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_DEGAMMA:
        {
            u32DataSize = u32Size / sizeof(MS_U32);
            break;
        }
        default:
        {
            printk("Vaild sub client.\n");
            return FALSE;
        }
    }

    //format&write datas into DRAM
    for (i = 0; i < u32DataSize; )
    {
        switch (enSubClient)
        {
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_TMO:
            {
                MS_U16 u16Value = *(((MS_U16 *)pu8Data) + i);
                if (u16Value & 0xF000)
                {
                    printk("The %dth data is 0x%x, exceed max value, please check!!!\n", i + 1, u16Value);
                    return FALSE;
                }
                WRITE_TMO_DATA_FORMAT_1(pu8BaseAddr, u32Index, u16Value);
                i++;
                break;
            }
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_GAMMA:
            {
                MS_U16 u16Value = *(((MS_U16 *)pu8Data) + i);
                //WRITE_GAMMA_DATA_FORMAT_1(pu8BaseAddr, u32Index, u16Value);

                // patch for HW hdr_gamma table issue.
                // Write gamma/degamma simultaneously, gamma may be broken.
                WRITE_GAMMA_DATA_FORMAT_1(pu8BaseAddr + ((AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR + AUTO_DOWNLOAD_HDR_GAMMA_BLANKING) * BYTE_PER_WORD), u32Index, u16Value);
                i++;
                break;
            }
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_DEGAMMA:
            {
                MS_U32 u32Value = *(((MS_U32 *)pu8Data) + i);
                if (u32Value & 0xFFF80000)
                {
                    printk("The %dth data is 0x%x, exceed max value, please check!!!\n", i + 1, (unsigned int)u32Value);
                    return FALSE;
                }
                WRITE_DEGAMMA_DATA_FORMAT_1(pu8BaseAddr, u32Index, u32Value);
                i++;
                break;
            }
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_3DLUT:
            {
                MS_U16 u16BVal = *(((MS_U16 *)pu8Data) + i);
                MS_U16 u16GVal = *(((MS_U16 *)pu8Data) + i + 1);
                MS_U16 u16RVal = *(((MS_U16 *)pu8Data) + i + 2);
                MS_U16 u16Subindex = 0;
                if((i / 3 == _au32_3dlut_entry_num[0]) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5] + _au32_3dlut_entry_num[6])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5] + _au32_3dlut_entry_num[6] + _au32_3dlut_entry_num[7])))
                {
                    u32Index = 0;
                }

                if(i / 3 < _au32_3dlut_entry_num[0])
                {
                    u16Subindex = 0;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1]))
                {
                    u16Subindex = 1;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2]))
                {
                    u16Subindex = 2;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3]))
                {
                    u16Subindex = 3;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4]))
                {
                    u16Subindex = 4;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5]))
                {
                    u16Subindex = 5;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5] + _au32_3dlut_entry_num[6]))
                {
                    u16Subindex = 6;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5] + _au32_3dlut_entry_num[6] + _au32_3dlut_entry_num[7]))
                {
                    u16Subindex = 7;
                }
                WRITE_3DLUT_DATA_FORMAT_1(pu8BaseAddr, u32Index, u16Subindex, u16RVal, u16GVal, u16BVal);
                i += 3;
                break;
            }
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_OOTF:
            {
                MS_U16 u16Value = *(((MS_U16 *)pu8Data) + i);
                WRITE_OOTF_DATA_FORMAT_1(pu8BaseAddr, u32Index, u16Value);

                // patch for HW hdr_gamma table issue.
                // Write gamma/degamma simultaneously, gamma may be broken.
                //WRITE_OOTF_DATA_FORMAT_1(pu8BaseAddr + ((AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR + AUTO_DOWNLOAD_HDR_GAMMA_BLANKING) * BYTE_PER_WORD), u32Index, u16Value);
                i++;
                break;
            }
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_RGB3DLUT:
            {
                MS_U16 u16BVal = *(((MS_U16 *)pu8Data) + i);
                MS_U16 u16GVal = *(((MS_U16 *)pu8Data) + i + 1);
                MS_U16 u16RVal = *(((MS_U16 *)pu8Data) + i + 2);
                MS_U16 u16Subindex = 0;
                if((i / 3 == _au32_3dlut_entry_num[0]) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5] + _au32_3dlut_entry_num[6])) ||
                   (i / 3 == (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5] + _au32_3dlut_entry_num[6] + _au32_3dlut_entry_num[7])))
                {
                    u32Index = 0;
                }

                if(i / 3 < _au32_3dlut_entry_num[0])
                {
                    u16Subindex = 0;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1]))
                {
                    u16Subindex = 1;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2]))
                {
                    u16Subindex = 2;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3]))
                {
                    u16Subindex = 3;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4]))
                {
                    u16Subindex = 4;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5]))
                {
                    u16Subindex = 5;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5] + _au32_3dlut_entry_num[6]))
                {
                    u16Subindex = 6;
                }
                else if(i / 3 < (_au32_3dlut_entry_num[0] + _au32_3dlut_entry_num[1] + _au32_3dlut_entry_num[2] + _au32_3dlut_entry_num[3] + _au32_3dlut_entry_num[4] + _au32_3dlut_entry_num[5] + _au32_3dlut_entry_num[6] + _au32_3dlut_entry_num[7]))
                {
                    u16Subindex = 7;
                }
                WRITE_RGB3DLUT_DATA_FORMAT_1(pu8BaseAddr, 1 << u16Subindex, u16RVal, u16GVal, u16BVal);

                while(u32Index<1023 && u32Index>=(_au32_3dlut_entry_num[u16Subindex]-1))
                {
                    u32WriteNum++;
                    pu8BaseAddr += BYTE_PER_WORD;
                    u32Index++;
                }
                i += 3;
                break;
            }
        }
        pu8BaseAddr += BYTE_PER_WORD;
        u32WriteNum++;
        u32Index++;
    }

    if (enSubClient == E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_GAMMA)
    {
        u32WriteNum += AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR + AUTO_DOWNLOAD_HDR_GAMMA_BLANKING;
    }

    u32Depth = u32Depth > u32WriteNum ? u32Depth : u32WriteNum;
    *pu32Depth = u32Depth;
#if DEBUG_HDR
    int j = 0;
    int l = 0;

    printk("\n#####################Dump Input Data####################\n");
    for (j = 0; j < u32DataSize;)
    {
        printk("The %04dth row: ", u16StartAddr++);
        switch (enSubClient)
        {
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_TMO:
            {
                MS_U16 u16Value = *(((MS_U16 *)pu8Data) + j);
                printk("%02X \n", u16Value);
                j++;
                break;
            }
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_GAMMA:
            {
                MS_U16 u16Value = *(((MS_U16 *)pu8Data) + j);
                printk("%02X \n", u16Value);
                j++;
                break;
            }
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_DEGAMMA:
            {
                MS_U32 u32Value = *(((MS_U32 *)pu8Data) + j);
                printk("%04X \n", u32Value);
                j++;
                break;
            }
            case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_3DLUT:
            {
                MS_U16 u16RVal = *(((MS_U16 *)pu8Data) + j);
                MS_U16 u16GVal = *(((MS_U16 *)pu8Data) + j + 1);
                MS_U16 u16BVal = *(((MS_U16 *)pu8Data) + j + 2);
                printk("%02X %02X %02X \n", u16RVal, u16GVal, u16BVal);
                j += 3;
                break;
            }
        }
        printk("\n");
    }
    printk("\n#####################Dump End####################\n\n");

    int k = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR;
    printk("\n#####################Dump DRAM Buffer####################\n");
    for (j = 0; j < k; j++)
    {
        MS_U8 *pu8BaseAddr = (MS_U8 *)pVirtBaseAddr;
        printk("\nThe %04dth row: ", j);
        for (l = 0; l < BYTE_PER_WORD; l++)
        {
            switch (enSubClient)
            {
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_TMO:
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_GAMMA:
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_DEGAMMA:
                {
                    printk("%02X ", *(pu8BaseAddr + BYTE_PER_WORD * j + l));
                    break;
                }
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_3DLUT:
                {
                    printk("%02X ", *(pu8BaseAddr + BYTE_PER_WORD * j + l));
                    k = AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR;
                    break;
                }
            }
        }
    }
    printk("\n#####################Dump End####################\n");
#endif

    return TRUE;
}

static BOOL _Hal_XC_Auto_Download_Format_Xvycc_Data(EN_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_CLIENT enSubClient,
        MS_U8* pu8Data, MS_U32 u32Size,MS_U8 *pu8AutodownloadDRAMAddr,MS_U32 *pu32Depth)
{
    MS_U8* pVirtBaseAddr = NULL;
    MS_U8 *pu8BaseAddr = NULL;
    unsigned int i = 0;
    MS_U32 u32WriteNum = 0;
    MS_U32 u32DataSize = 0;
    MS_U32 u32Depth = *pu32Depth;

    if(pu8AutodownloadDRAMAddr == NULL)
    {
        printk("%s: %d pu8AutoDownloadDRAMBaseAddr is NULL.\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

    if(bADLChangeBasedAddress)
    {
        pVirtBaseAddr = pu8AutodownloadDRAMAddr+u8HDRSeamlessOPDSIndex * (XVYCC_GAMMA_ENTRY+XVYCC_DEGAMMA_ENTRY+XVYCC_GAMMA_BLANKING) * BYTE_PER_WORD;
    }
    else
    {
        pVirtBaseAddr = pu8AutodownloadDRAMAddr;
    }

    pu8BaseAddr = (MS_U8 *)pVirtBaseAddr;

    switch (enSubClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_GAMMA:
        {
            u32DataSize = u32Size / sizeof(MS_U16);
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_DEGAMMA:
        {
            u32DataSize = u32Size / sizeof(MS_U32);
            break;
        }
        default:
        {
            printk("InVaild sub client.\n");
            return FALSE;
        }
    }

    switch (enSubClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_GAMMA:
        {
            MS_U32 u32GammaDataStart = 0;

            u32GammaDataStart = (XVYCC_DEGAMMA_ENTRY + XVYCC_GAMMA_BLANKING);

            pu8BaseAddr += (u32GammaDataStart * BYTE_PER_WORD);

            for (i = 0; i < u32DataSize; i++)
            {
                MS_U16 u16Value = *((MS_U16 *)pu8Data + i);

                if (u16Value & 0xF000)
                {
                    printk("The %dth data is 0x%x, T exceed max value, please check!!!\n", i + 1, u16Value);
                    return FALSE;
                }

                WRITE_XVYCC_GAMMA_DATA_FORMAT_1(pu8BaseAddr, u16Value);

                pu8BaseAddr += BYTE_PER_WORD;
                u32WriteNum++;
            }
            break;
        }

        case E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_DEGAMMA:
        {
            for (i = 0; i < u32DataSize; i++)
            {
                MS_U32 u32Value = *(((MS_U32 *)pu8Data) + i);

                if (u32Value & 0xFFF00000)
                {
                    printk("The %dth data is 0x%x, exceed max value, please check!!!\n", i + 1, u32Value);
                    return FALSE;
                }

                WRITE_XVYCC_DEGAMMA_DATA_FORMAT_1(pu8BaseAddr, u32Value);

                pu8BaseAddr += BYTE_PER_WORD;
                u32WriteNum++;
            }
            break;
        }

        default:
        {
            printk("InVaild sub client.\n");
            return FALSE;
        }

    }

    if (enSubClient == E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_GAMMA)
    {
        u32WriteNum += XVYCC_GAMMA_BLANKING + XVYCC_DEGAMMA_ENTRY;
    }

    u32Depth = u32Depth > u32WriteNum ? u32Depth : u32WriteNum;
    _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].u32Depth = u32Depth;
    *pu32Depth = u32Depth;
    return TRUE;
}

static BOOL _Hal_XC_Auto_Download_Format_Op2Lut_Data(MS_U8* pu8Data, MS_U32 u32Size)
{
    MS_U8* pVirtBaseAddr = NULL;
    MS_U8 *pu8BaseAddr = NULL;
    unsigned int i = 0;
    MS_U32 u32WriteNum = 0;
    MS_U32 u32DataSize = 0;

    if(pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT] == NULL)
    {
        printk("%s: %d pu8AutoDownloadDRAMBaseAddr is NULL.\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

    pVirtBaseAddr = pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT];
    pu8BaseAddr = (MS_U8 *)pVirtBaseAddr;

    u32DataSize = u32Size / sizeof(MS_U16);

    for (i = 0; i < u32DataSize; )
    {
        MS_U16 u16BVal = *(((MS_U16 *)pu8Data) + i);
        MS_U16 u16GVal = *(((MS_U16 *)pu8Data) + i + 1);
        MS_U16 u16RVal = *(((MS_U16 *)pu8Data) + i + 2);
        MS_U8 u8SRAMindex = 0;

        if ((u16BVal & 0xFC00) || (u16GVal & 0xFC00) || (u16RVal & 0xFC00))
        {
            printk("The %dth data is RGB(0x%x,0x%x,0x%x), T exceed max value, please check!!!\n", i + 1, u16RVal, u16GVal, u16BVal);
            return FALSE;
        }

        u8SRAMindex = i / (OP2LUT_ENTRY_DATA_NUM * OP2LUT_SRAM_ENTRY);

        WRITE_OP2LUT_DATA_FORMAT_1(pu8BaseAddr, u8SRAMindex, u16RVal, u16GVal, u16BVal);
        i += OP2LUT_ENTRY_DATA_NUM;
        pu8BaseAddr += BYTE_PER_WORD;
        u32WriteNum++;
    }

    _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].u32Depth = u32WriteNum;
    return TRUE;
}

BOOL MHal_XC_WriteAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient, MS_U8* pu8Data, MS_U32 u32Size, void* pParam)
{
    MS_U32 u32Depth = 0;
    MS_U8 *pu8BaseAddr = NULL;
    MS_U32 u32StartAddr = 0;
    MS_U8 u8ErrorCont = 0;
    MS_U8 u8WaitCont = 0;

    // for openHDR/SDR seamless, autodownload write be directed to MHal_XC_StoreHDRAutoDownload
    if((CFD_IS_MM(_stCfdInit[0].u8InputSource) || CFD_IS_DTV(_stCfdInit[0].u8InputSource)) && ( MHal_XC_Get_SharedMemVersion(0) == getOpenHDRSeamlessVersion()))
    {
        if (E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR == enClient)
        {
            MHal_XC_StoreHDRAutoDownload(pu8Data,u32Size,pParam);
        }
        else if(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC == enClient)
        {
            MHal_XC_StoreXVYCCAutoDownload(pu8Data,u32Size,pParam);
        }
        else
        {
            printk("!!!!!!!! not support this client [%s][%05d] \n",__FUNCTION__, __LINE__);
        }

        return TRUE;
    }

    mutex_lock(&_adl_mutex);

    u32Depth = _stClientInfo[enClient].u32Depth;
    u32StartAddr = _stClientInfo[enClient].u32StartAddr;

    MS_BOOL bFireing[E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX] = {0};

    if (_stClientInfo[enClient].bEnable == FALSE)
    {
        mutex_unlock(&_adl_mutex);
        printk("Auto downlaod client[%d] is disabled, please enable first.\n", enClient);
        return FALSE;
    }

    if(pu8AutoDownloadDRAMBaseAddr[enClient] == NULL)
    {
        mutex_unlock(&_adl_mutex);
        printk("%s: %d pu8AutoDownloadDRAMBaseAddr is NULL.\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

    pu8BaseAddr = pu8AutoDownloadDRAMBaseAddr[enClient];
    if (u32StartAddr != 0xFFFFFFFF)
    {
        pu8BaseAddr += BYTE_PER_WORD * u32StartAddr;
    }

    if (_stClientInfo[enClient].enMode == E_KDRV_XC_AUTODOWNLOAD_TRIGGER_MODE)
    {
        if(enClient == E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR)
        {
            bFireing[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR] = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_28_L) & BIT(0));
        }
        else if(enClient == E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC)
        {
            bFireing[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC] = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_11_L) & BIT(2));
        }
        else if(enClient == E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT)
        {
            bFireing[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT] = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_22_L) & BIT(0));
        }
        else
        {
            //printk("Need add other cases.\n");
        }

        // Wait writing data finished
        while (bFireing[enClient] == TRUE)
        {
            msleep(AUTO_DOWNLOAD_WAIT_TIME);

            u8WaitCont++;
            if(u8WaitCont > AUTO_DOWNLOAD_WAIT_COUNT)
            {
                mutex_unlock(&_adl_mutex);
                printk("Wait a moment, the lastest datas is writing.\n");
                return FALSE;
            }

            if(enClient == E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR)
            {
                bFireing[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR] = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_28_L) & BIT(0));
            }
            else if(enClient == E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC)
            {
                bFireing[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC] = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_11_L) & BIT(2));
            }
            else
            {
                //printk("Need add other cases.\n");
            }
        }

        // the lasteset datas is writing done.
        if (_enAutoDownloadStatus[enClient] == E_KDRV_XC_AUTO_DOWNLOAD_FIRED)
        {
            // clear buffer
            memset(pu8BaseAddr, 0, (u32Depth * BYTE_PER_WORD));
            _stClientInfo[enClient].u32StartAddr = 0xFFFFFFFF;
            _stClientInfo[enClient].u32Depth = 0;
            _enAutoDownloadStatus[enClient] = E_KDRV_XC_AUTO_DOWNLOAD_WRITED;
        }
        else
        {
            _enAutoDownloadStatus[enClient] = E_KDRV_XC_AUTO_DOWNLOAD_WRITED;
        }
    }

    switch(enClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR:
        {
            ST_KDRV_XC_AUTODOWNLOAD_FORMAT_INFO* pstFormatInfo = (ST_KDRV_XC_AUTODOWNLOAD_FORMAT_INFO *)pParam;
            MS_U16 u16StartAddr = 0;
            MS_U32 u32MaxSize = 0;
            MS_U32 u32MaxAddr = 0;
            MS_U32 u32WriteNum = 0;
            MS_BOOL bRet = 0;
            switch (pstFormatInfo->enSubClient)
            {
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_TMO:
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_GAMMA:
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_OOTF:
                {
                    u32MaxSize = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR * sizeof(MS_U16);
                    u32MaxAddr = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR;
                    u32WriteNum = u32Size / sizeof(MS_U16);
                    break;
                }
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_DEGAMMA:
                {
                    u32MaxSize = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR * sizeof(MS_U32);
                    u32MaxAddr = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR;
                    u32WriteNum = u32Size / sizeof(MS_U32);
                    break;
                }
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_RGB3DLUT:
                {
                    u32MaxSize = AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR * 3 * sizeof(MS_U16);
                    u32MaxAddr = AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR;
                    u32WriteNum = u32Size / (3 * sizeof(MS_U16));
                    break;
                }
                case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_3DLUT:
                {
                    u32MaxSize = AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR * 3 * sizeof(MS_U16);
                    u32MaxAddr = AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR;
                    u32WriteNum = u32Size / (3 * sizeof(MS_U16));
                    break;
                }
                default:
                {
                    mutex_unlock(&_adl_mutex);
                    printk("Write auto download fail, invaild paramters, subClient: %d\n", pstFormatInfo->enSubClient);
                    return FALSE;
                }
            }
            if (pstFormatInfo->bEnableRange == TRUE)
            {
                if ((pstFormatInfo->u16StartAddr <= pstFormatInfo->u16EndAddr) && (pstFormatInfo->u16EndAddr < u32MaxAddr)
                    && (pstFormatInfo->u16StartAddr + u32WriteNum - 1) < u32MaxAddr)
                {
                    u16StartAddr = pstFormatInfo->u16StartAddr;
                }
                else
                {
                    mutex_unlock(&_adl_mutex);
                    printk("Write auto download fail, invaild paramters, subClient: %d, size: %d, addr range(enable, start, end) = (%d, %d, %d)\n",
                           pstFormatInfo->enSubClient, u32Size, pstFormatInfo->bEnableRange, pstFormatInfo->u16StartAddr, pstFormatInfo->u16EndAddr);
                    return FALSE;
                }
            }
            else
            {
                if (u32Size > u32MaxSize)
                {
                    mutex_unlock(&_adl_mutex);
                    printk("Write auto download fail, invaild paramters, subClient: %d, size: %d\n", pstFormatInfo->enSubClient, u32Size);
                    return FALSE;
                }
            }

            // update start addr of DRAM
            u32StartAddr = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32StartAddr;
            if (u32StartAddr == 0xFFFFFFFF)
            {
                _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32StartAddr = u16StartAddr;
            }
            else
            {
                _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32StartAddr = u32StartAddr > u16StartAddr ? u16StartAddr : u32StartAddr;
            }

            MS_U8 *pVirtBaseAddr;
            if(bADLChangeBasedAddress)
            {
                pVirtBaseAddr = pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR]+u8HDRSeamlessIPDSIndex * AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR * BYTE_PER_WORD;
            }
            else
            {
                pVirtBaseAddr = pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR];
            }

            bRet = _Hal_XC_Auto_Download_Format_Hdr_Data(pstFormatInfo->enSubClient, pVirtBaseAddr, pu8Data, u32Size, u16StartAddr, &_stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32Depth);
            mutex_unlock(&_adl_mutex);

            return bRet;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC:
        {
            ST_KDRV_XC_AUTODOWNLOAD_XVYCC_FORMAT_INFO* pstFormatInfo = (ST_KDRV_XC_AUTODOWNLOAD_XVYCC_FORMAT_INFO *)pParam;
            MS_BOOL bRet = FALSE;
            MS_U32 u32MaxSize = 0;
            switch (pstFormatInfo->enSubClient)
            {
                case E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_GAMMA:
                {
                    u32MaxSize = XVYCC_GAMMA_ENTRY * sizeof(MS_U16);
                    break;
                }
                case E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_DEGAMMA:
                {
                    u32MaxSize = XVYCC_DEGAMMA_ENTRY * sizeof(MS_U32);
                    break;
                }
                default:
                {
                    mutex_unlock(&_adl_mutex);
                    printk("Write auto download fail, invaild paramters, subClient: %d\n", pstFormatInfo->enSubClient);
                    return FALSE;
                }

            }

            if (u32Size > u32MaxSize)
            {
                mutex_unlock(&_adl_mutex);
                printk("Write auto download fail, invaild paramters, subClient: %d, size: %d\n", pstFormatInfo->enSubClient, u32Size);
                return FALSE;
            }

            bRet = _Hal_XC_Auto_Download_Format_Xvycc_Data(pstFormatInfo->enSubClient, pu8Data, u32Size, pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC], &_stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].u32Depth);
            mutex_unlock(&_adl_mutex);

            return bRet;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT:
        {
            MS_BOOL bRet = FALSE;
            MS_U32 u32MaxSize = OP2LUT_ENTRY * OP2LUT_ENTRY_DATA_NUM * sizeof(MS_U16);

            if (u32Size > u32MaxSize)
            {
                mutex_unlock(&_adl_mutex);
                printk("Write auto download fail, invaild paramters, size: %d\n", u32Size);
                return FALSE;
            }

            bRet = _Hal_XC_Auto_Download_Format_Op2Lut_Data(pu8Data, u32Size);
            mutex_unlock(&_adl_mutex);

            return bRet;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCOP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE2:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE3:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_DEMURA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_0:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCSPTPOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FOOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX:
        default:
        {
            mutex_unlock(&_adl_mutex);
            return FALSE;
        }

    }

    mutex_unlock(&_adl_mutex);
    return TRUE;
}

BOOL MHal_XC_FireAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient)
{
    if(CFD_IS_HDMI(_stCfdInit[0].u8InputSource) && (MHAL_XC_IsCFDInitFinished(0)==TRUE) && ( E_SEAMLESS_HDMI_WITHOUT_MUTE_AND_FREEZE == MHal_XC_Get_HDRSeamless_HDMI_Path()) )
    {
        return TRUE;
    }

    // for openHDR/SDR seamless, autodownload fire be triggered by DSinfo function
    // so skip this function
    if((CFD_IS_MM(_stCfdInit[0].u8InputSource) || CFD_IS_DTV(_stCfdInit[0].u8InputSource)) && ( MHal_XC_Get_SharedMemVersion(0) == getOpenHDRSeamlessVersion()) )
    {
        return TRUE;
    }

    mutex_lock(&_adl_mutex);

    if (_stClientInfo[enClient].bEnable == FALSE)
    {
        mutex_unlock(&_adl_mutex);
        printk("Auto downlaod client[%d] is disabled, please enable first.\n", enClient);
        return FALSE;
    }

    switch(enClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR:
        {
            MS_PHY phyBaseAddr = 0;
            MS_U32 u32Depth = 0;
            MS_U32 u32StartAddr = 0;
            MS_U16 u16inputVTT = MHal_XC_R2BYTEMSK(REG_SC_BK01_1F_L,0x1FFF);
            MS_U16 u16inputHperiod = MHal_XC_R2BYTEMSK(REG_SC_BK01_20_L,0x3FFF);
            MS_U8 u8WaitCont = 0;
            MS_BOOL bFireing = FALSE;

            if(u16inputVTT==0 || u16inputHperiod==0 || u16inputHperiod==0x3FFF || u16inputVTT==0x1FFF)
            {
                if(!CFD_IS_MM(_stCfdInit[0].u8InputSource))    //To avoid dolby MM garbage
                {
                    mutex_unlock(&_adl_mutex);
                    printk("Auto downlaod HDR fail, because there is no input signal.\n");
                    return FALSE;
                }
            }
            phyBaseAddr = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].phyBaseAddr;
            u32Depth = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32Depth;
            u32StartAddr = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32StartAddr;

            // set baseaddr
            MHal_XC_W2BYTE(REG_SC_BK67_29_L, ((phyBaseAddr + BYTE_PER_WORD * u32StartAddr) / BYTE_PER_WORD) & 0x0000FFFF);
            MHal_XC_W2BYTEMSK(REG_SC_BK67_2A_L, (((phyBaseAddr + BYTE_PER_WORD * u32StartAddr) / BYTE_PER_WORD) >> 16 & 0x0000FFFF), 0x07FF);

            // set depth
            MHal_XC_W2BYTE(REG_SC_BK67_2B_L, u32Depth);
            // fine-tune length of client8 DMA's request
            MHal_XC_W2BYTE(REG_SC_BK67_2C_L, 0x10);

            Chip_Flush_Cache_Range_VA_PA(pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR], phyBaseAddr, u32Depth * BYTE_PER_WORD);
            // enable auto download
            MHal_XC_W2BYTEMSK(REG_SC_BK67_28_L, BIT(0), BIT(0));

            bFireing = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_28_L) & BIT(0));
            // wait for writing data finished

            while (bFireing)
            {
                msleep(AUTO_DOWNLOAD_WAIT_TIME);

                u8WaitCont++;
                if (u8WaitCont > AUTO_DOWNLOAD_WAIT_COUNT)
                {
                    printk(" E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR [%s %d]waiting adl firing done time out\n", __FUNCTION__,__LINE__);
                    break;
                }

                bFireing = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_28_L) & BIT(0));
            }

            break;
        }

        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC:
        {
            MS_PHY phyBaseAddr = 0;
            MS_U32 u32Depth = 0;
            MS_U8 u8WaitCont = 0;
            MS_BOOL bFireing = FALSE;

            phyBaseAddr = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].phyBaseAddr;
            u32Depth = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].u32Depth;

            // set baseaddr
            MHal_XC_W2BYTE(REG_SC_BK67_16_L, (phyBaseAddr / BYTE_PER_WORD) & 0x0000FFFF);
            MHal_XC_W2BYTEMSK(REG_SC_BK67_17_L, ((phyBaseAddr / BYTE_PER_WORD) >> 16 & 0x0000FFFF), 0x07FF);

            //reg_client6_depth
            MHal_XC_W2BYTE(REG_SC_BK67_1A_L, min(u32Depth,0xFFFF));

            //reg_client6_req_len
            MHal_XC_W2BYTE(REG_SC_BK67_1D_L, min(u32Depth,0xFFFF));

            Chip_Flush_Cache_Range_VA_PA(pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC], phyBaseAddr, u32Depth * BYTE_PER_WORD);

            // enable auto download
            MHal_XC_W2BYTEMSK(REG_SC_BK67_11_L, BIT(2), BIT(2));

            bFireing = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_11_L) & BIT(2));
            // wait for writing data finished
            while (bFireing)
            {
                msleep(AUTO_DOWNLOAD_WAIT_TIME);

                u8WaitCont++;
                if (u8WaitCont > AUTO_DOWNLOAD_WAIT_COUNT)
                {
                    printk(" E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC [%s %d]waiting adl firing done time out\n", __FUNCTION__,__LINE__);
                    break;
                }

                bFireing = (MS_BOOL)(MHal_XC_R2BYTE(REG_SC_BK67_11_L) & BIT(2));
            }

            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT:
        {
            MS_PHY phyBaseAddr = 0;
            MS_U32 u32Depth = 0;

            phyBaseAddr = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].phyBaseAddr;
            u32Depth = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT].u32Depth;
            // set baseaddr
            MHal_XC_W2BYTE(REG_SC_BK67_23_L, (phyBaseAddr / BYTE_PER_WORD) & 0x0000FFFF);
            MHal_XC_W2BYTEMSK(REG_SC_BK67_24_L, ((phyBaseAddr / BYTE_PER_WORD) >> 16 & 0x0000FFFF), 0x07FF);

            //reg_client6_depth
            MHal_XC_W2BYTE(REG_SC_BK67_25_L, u32Depth);

            //reg_client6_req_len
            MHal_XC_W2BYTE(REG_SC_BK67_26_L, u32Depth);

            Chip_Flush_Cache_Range_VA_PA(pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT], phyBaseAddr, u32Depth * BYTE_PER_WORD);
            // enable auto download
            MHal_XC_W2BYTEMSK(REG_SC_BK67_22_L, BIT(0), BIT(0));

            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCOP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE2:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE3:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_DEMURA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_0:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCSPTPOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FOOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX:
        default:
        {
            mutex_unlock(&_adl_mutex);
            return FALSE;
        }

    }

    _enAutoDownloadStatus[enClient] = E_KDRV_XC_AUTO_DOWNLOAD_FIRED;
    mutex_unlock(&_adl_mutex);

    return TRUE;
}

BOOL MHal_XC_ResetAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient)
{
    mutex_lock(&_adl_mutex);
    MS_BOOL bFireDone = FALSE;

    if (_stClientInfo[enClient].bEnable == FALSE)
    {
        mutex_unlock(&_adl_mutex);
        printk("Auto downlaod client[%d] is disabled, please enable first.\n", enClient);
        return FALSE;
    }

    switch(enClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR:
        {
            bFireDone = MHal_XC_R2BYTE(REG_SC_BK67_28_L) & 0x0001 ? FALSE : TRUE;
            if (!bFireDone)
            {
                // reset HDR adl
                MHal_XC_W2BYTEMSK(REG_SC_BK67_38_L, BIT(9), BIT(9));
                MHal_XC_W2BYTEMSK(REG_SC_BK67_39_L, BIT(9), BIT(9));
                MS_U8 u8Time = 0;
                do
                {
                    u8Time++;
                    bFireDone = MHal_XC_R2BYTE(REG_SC_BK67_28_L) & 0x0001 ? FALSE : TRUE;
                    if (bFireDone || (u8Time > 3))
                    {
                        MHal_XC_W2BYTEMSK(REG_SC_BK67_38_L, 0, BIT(9));
                        break;
                    }
                    msleep(10);
                }
                while (!bFireDone);
            }
            break;
        }
        default:
        {
            break;
        }
    }

    mutex_unlock(&_adl_mutex);

    return bFireDone;
}

BOOL MHal_XC_ConfigHDRAutoDownloadStoredInfo(MS_PHY phyLutBaseAddr, MS_U8 *pu8VirtLutBaseAddr, MS_U32 u32Size)
{
    // init client info
    _stHDRClientInfo.phyBaseAddr = phyLutBaseAddr;
    _stHDRClientInfo.u32Size = u32Size;
    _stHDRClientInfo.u32StartAddr = 0xFFFFFFFF;
    _stHDRClientInfo.u32Depth = 0;

    pu8HDRAutoDownloadDRAMBaseAddr = pu8VirtLutBaseAddr;

    if (pu8HDRAutoDownloadDRAMBaseAddr != NULL)
    {
        memset(pu8HDRAutoDownloadDRAMBaseAddr, 0, u32Size);
    }
    else
    {
        printk("%s: %d, auto downlaod mmap pa fail.\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

    return TRUE;
}

BOOL MHal_XC_ConfigXVYCCAutoDownloadStoredInfo(MS_PHY phyLutBaseAddr, MS_U8 *pu8VirtLutBaseAddr, MS_U32 u32Size)
{
    // init client info
    _stXVYCCClientInfo.phyBaseAddr = phyLutBaseAddr;
    _stXVYCCClientInfo.u32Size = u32Size;
    _stXVYCCClientInfo.u32StartAddr = 0xFFFFFFFF;
    _stXVYCCClientInfo.u32Depth = 0;

    pu8XVYCCAutoDownloadDRAMBaseAddr = pu8VirtLutBaseAddr;

    if (pu8XVYCCAutoDownloadDRAMBaseAddr != NULL)
    {
        memset(pu8XVYCCAutoDownloadDRAMBaseAddr, 0, u32Size);
    }
    else
    {
        printk("%s: %d, auto downlaod mmap pa fail.\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

    return TRUE;
}

BOOL MHal_XC_StoreHDRAutoDownload(MS_U8* pu8Data, MS_U32 u32Size, void* pParam)
{
    MS_U8 *pu8BaseAddr = NULL;
    MS_U32 u32StartAddr = 0;
    ST_KDRV_XC_AUTODOWNLOAD_FORMAT_INFO* pstFormatInfo;
    MS_U16 u16StartAddr = 0;
    MS_U32 u32MaxSize = 0;
    MS_U32 u32MaxAddr = 0;
    MS_U32 u32WriteNum = 0;
    MS_BOOL bRet = 0;

    u32StartAddr = _stHDRClientInfo.u32StartAddr;
    pu8BaseAddr = NULL;

    if(pu8HDRAutoDownloadDRAMBaseAddr == NULL)
    {
        printk("%s: %d pu8HDRAutoDownloadDRAMBaseAddr is NULL.\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

    pu8BaseAddr = pu8HDRAutoDownloadDRAMBaseAddr;
    if (u32StartAddr != 0xFFFFFFFF)
    {
        pu8BaseAddr += BYTE_PER_WORD * u32StartAddr;
    }

    pstFormatInfo = (ST_KDRV_XC_AUTODOWNLOAD_FORMAT_INFO *)pParam;
    u16StartAddr = 0;
    u32MaxSize = 0;
    u32MaxAddr = 0;
    u32WriteNum = 0;
    switch (pstFormatInfo->enSubClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_TMO:
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_OOTF:
        {
            u32MaxSize = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR * sizeof(MS_U16);
            u32MaxAddr = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR;
            u32WriteNum = u32Size / sizeof(MS_U16);
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_DEGAMMA:
        {
            u32MaxSize = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR * sizeof(MS_U32);
            u32MaxAddr = AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR;
            u32WriteNum = u32Size / sizeof(MS_U32);
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_RGB3DLUT:
        {
            u32MaxSize = AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR * 3 * sizeof(MS_U16);
            u32MaxAddr = AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR;
            u32WriteNum = u32Size / (3 * sizeof(MS_U16));
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_HDR_SUB_3DLUT:
        {
            u32MaxSize = AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR * 3 * sizeof(MS_U16);
            u32MaxAddr = AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR;
            u32WriteNum = u32Size / (3 * sizeof(MS_U16));
            break;
        }
        default:
        {
            printk("Write auto download fail, invaild paramters, subClient: %d\n", pstFormatInfo->enSubClient);
            return FALSE;
        }
    }
    if (pstFormatInfo->bEnableRange == TRUE)
    {
        if ((pstFormatInfo->u16StartAddr <= pstFormatInfo->u16EndAddr) && (pstFormatInfo->u16EndAddr < u32MaxAddr)
            && (pstFormatInfo->u16StartAddr + u32WriteNum - 1) < u32MaxAddr)
        {
            u16StartAddr = pstFormatInfo->u16StartAddr;
        }
        else
        {
            printk("Write auto download fail, invaild paramters, subClient: %d, size: %d, addr range(enable, start, end) = (%d, %d, %d)\n",
                   pstFormatInfo->enSubClient, u32Size, pstFormatInfo->bEnableRange, pstFormatInfo->u16StartAddr, pstFormatInfo->u16EndAddr);
            return FALSE;
        }
    }
    else
    {
        if (u32Size > u32MaxSize)
        {
            printk("Write auto download fail, invaild paramters, subClient: %d, size: %d\n", pstFormatInfo->enSubClient, u32Size);
            return FALSE;
        }
    }

    // update start addr of DRAM
    u32StartAddr = _stHDRClientInfo.u32StartAddr;
    if (u32StartAddr == 0xFFFFFFFF)
    {
        _stHDRClientInfo.u32StartAddr = u16StartAddr;
    }
    else
    {
        _stHDRClientInfo.u32StartAddr = u32StartAddr > u16StartAddr ? u16StartAddr : u32StartAddr;
    }

    bRet = _Hal_XC_Auto_Download_Format_Hdr_Data(pstFormatInfo->enSubClient, pu8HDRAutoDownloadDRAMBaseAddr, pu8Data, u32Size, u16StartAddr, &_stHDRClientInfo.u32Depth);

    return bRet;
}

BOOL MHal_XC_StoreXVYCCAutoDownload(MS_U8* pu8Data, MS_U32 u32Size, void* pParam)
{
    ST_KDRV_XC_AUTODOWNLOAD_XVYCC_FORMAT_INFO* pstFormatInfo = (ST_KDRV_XC_AUTODOWNLOAD_XVYCC_FORMAT_INFO *)pParam;
    MS_BOOL bRet = FALSE;
    MS_U32 u32MaxSize = 0;

    if(pu8XVYCCAutoDownloadDRAMBaseAddr == NULL)
    {
        printk("%s: %d pu8XVYCCAutoDownloadDRAMBaseAddr is NULL.\n", __FUNCTION__, __LINE__);
        return FALSE;
    }
    switch (pstFormatInfo->enSubClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_GAMMA:
        {
            u32MaxSize = XVYCC_GAMMA_ENTRY * sizeof(MS_U16);
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_XVYCC_SUB_DEGAMMA:
        {
            u32MaxSize = XVYCC_DEGAMMA_ENTRY * sizeof(MS_U32);
            break;
        }
        default:
        {
            printk("Write auto download fail, invaild paramters, subClient: %d in XVYCC\n", pstFormatInfo->enSubClient);
            return FALSE;
        }

    }

    if (u32Size > u32MaxSize)
    {
        printk("Write auto download fail, invaild paramters, subClient: %d, size: %d in XVYCC\n", pstFormatInfo->enSubClient, u32Size);
        return FALSE;
    }
    bRet = _Hal_XC_Auto_Download_Format_Xvycc_Data(pstFormatInfo->enSubClient, pu8Data, u32Size, pu8XVYCCAutoDownloadDRAMBaseAddr, &_stXVYCCClientInfo.u32Depth);
    return bRet;
}

static MS_PHY _Hal_XC_GetAutoDownlaodMemOffset(MS_U8 u8Index)
{
#if 0
    return (AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR + u8Index * AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR) * BYTE_PER_WORD;
#else
    return u8Index * AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR * BYTE_PER_WORD;
#endif
}

static MS_PHY _Hal_XC_GetXVYCCAutoDownloadMemOffset(MS_U8 u8Index)
{
    return (u8Index*OPENHDR_SEAMLESS_XVYCC_NUM)*BYTE_PER_WORD;
}

BOOL MHal_XC_WriteStoredHDRAutoDownload(MS_U8 *pu8LutData, MS_U32 u32Size, MS_U8 u8Index,
                                        MS_PHY *pphyFireAdlAddr, MS_U32 *pu32Depth)
{
    MS_PHY phyBaseAddr = 0;
#ifdef XC_REPLACE_MEMCPY_BY_BDMA
    MS_U32 u32MiuNo = 0;
#endif
    mutex_lock(&_adl_mutex);

    phyBaseAddr = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].phyBaseAddr;
#ifdef XC_REPLACE_MEMCPY_BY_BDMA
    u32MiuNo = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].u32MiuNo;
#endif

    // copy data into adl mem

    if ((pu8LutData != NULL) && (pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR] != NULL))
    {
#ifdef XC_REPLACE_MEMCPY_BY_BDMA
        MS_U8 *pu8DstAddr = 0;
        MS_U32 u32IdxOffset = _Hal_XC_GetAutoDownlaodMemOffset(u8Index);
        _miu_offset_to_phy(u32MiuNo, phyBaseAddr, pu8DstAddr);
        pu8DstAddr = pu8DstAddr + u32IdxOffset;
        MHal_XC_MemCpy_by_BDMA((MS_U32)pu8LutData, pu8DstAddr, u32Size);
#else
        MS_U8 *pu8DstAddr = pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR] + _Hal_XC_GetAutoDownlaodMemOffset(u8Index);
        memcpy(pu8DstAddr, pu8LutData, u32Size);
#endif
    }
    else
    {
        mutex_unlock(&_adl_mutex);
        printk("%s, malloc memory fail.\n", __FUNCTION__);
        return FALSE;
    }

#if 0
    *pphyFireAdlAddr = phyBaseAddr + ((u8Index == 0) ? 0 : AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR + u8Index * AUTO_DOWNLOAD_HDR_TMO_SRAM_MAX_ADDR) * BYTE_PER_WORD;
    *pu32Depth = u32Size / BYTE_PER_WORD + ((u8Index == 0) ? AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR : 0);
#else
    *pphyFireAdlAddr = phyBaseAddr + u8Index * AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR * BYTE_PER_WORD;
    *pu32Depth = AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR;
#endif
    mutex_unlock(&_adl_mutex);

    return TRUE;
}

BOOL MHal_XC_WriteStoredXVYCCAutoDownload(MS_U8 *pu8LutData, MS_U32 u32Size, MS_U8 u8Index,
                                        MS_PHY *pphyFireAdlAddr, MS_U32 *pu32Depth)
{
    MS_PHY phyBaseAddr = 0;
    MS_U32 u32MiuNo = 0;

    mutex_lock(&_adl_mutex);
    phyBaseAddr = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].phyBaseAddr; //xvycc pa from mmap
    u32MiuNo = _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC].u32MiuNo; //from mmap

    // copy data into adl mem
    if ((pu8LutData != NULL) && (pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC] != NULL))
    {
#ifdef XC_REPLACE_MEMCPY_BY_BDMA
        MS_U8 *pu8DstAddr = 0;
        MS_U32 u32IdxOffset = _Hal_XC_GetXVYCCAutoDownloadMemOffset(u8Index);
        _miu_offset_to_phy(u32MiuNo, phyBaseAddr, pu8DstAddr);
        pu8DstAddr = pu8DstAddr + u32IdxOffset;
        MHal_XC_MemCpy_by_BDMA((MS_U32)pu8LutData, pu8DstAddr, u32Size);
#else
        MS_U8 *pu8DstAddr = pu8AutoDownloadDRAMBaseAddr[E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC] + _Hal_XC_GetXVYCCAutoDownloadMemOffset(u8Index);
        memcpy(pu8DstAddr, pu8LutData, u32Size);
#endif
    }
    else
    {
        mutex_unlock(&_adl_mutex);
        printk("%s, malloc memory fail.\n", __FUNCTION__);
        return FALSE;
    }

    // autodownload based address ( for register )
    *pphyFireAdlAddr = phyBaseAddr + (u8Index * OPENHDR_SEAMLESS_XVYCC_NUM) * BYTE_PER_WORD;

    // autodownload depth ( for register )
    *pu32Depth = OPENHDR_SEAMLESS_XVYCC_NUM;
    mutex_unlock(&_adl_mutex);
    return TRUE;
}


MS_U32 MHal_XC_GetHDRAutoDownloadStoredSize()
{
    return _stHDRClientInfo.u32Depth * BYTE_PER_WORD;
}

BOOL MHal_XC_GetAutoDownloadCaps(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient, MS_BOOL *pbSupported)
{
    switch(enClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCOP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE2:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE3:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_DEMURA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_0:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCSPTPOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FOOPM:
        {
            *pbSupported = TRUE;
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX:
        default:
        {
            *pbSupported = FALSE;
            break;
        }

    }

    return TRUE;
}

BOOL MHal_XC_SetColorFormat(MS_BOOL bHDMI422)
{
    MS_U16 u16Value = (bHDMI422 == TRUE) ? 0 : 1;

    MHal_XC_W2BYTEMSK(REG_SC_BK79_07_L, u16Value << 4, BIT(4));
    // PQ bypass control
    if (bHDMI422 == TRUE)
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0x8000, 0xF000);
    }
    else
    {
        MHal_XC_W2BYTEMSK(REG_SC_BK7A_01_L, 0x0, 0xF000);
    }

    return TRUE;
}

#if DOLBY_GD_ENABLE
MS_U16 MHal_XC_GetGDValue(void)
{
    return DoVi_Backlight_Value();
}

MS_BOOL MHal_XC_IsGDEnabled(void)
{
    MS_BOOL bGDEnabled = FALSE;

    if (_stCfdPanel.bGlobalDimming)
    {
        bGDEnabled = DoVi_IsSupportGD();
    }

    return bGDEnabled;
}

MS_S8 MHal_XC_GetBackendDelayFrame(void)
{
    return _stCfdPanel.s8DelayFrame;
}

MS_S8 MHal_XC_GetPWMDelayFrame(void)
{
    if (CFD_IS_MM(_stCfdInit[0].u8InputSource))
    {
        return 0;
    }
    else
    {
        return 1;
    }

    return 1;
}

MS_U8 MHal_XC_GetPWMPort(void)
{
    return _stCfdPanel.u8PWMPort;
}

MS_U8 MHal_XC_GetSCMIFrameCount(void)
{
    return MHal_XC_R2BYTEMSK(REG_SC_BK12_19_L, 0x001F);
}

MS_U8 MHal_XC_GetHDRDMAFrameCount(void)
{
    return MHal_XC_R2BYTEMSK(REG_SC_BK42_19_L, 0x001F);
}

void MHal_XC_SetLastGDWdPtr(MS_U16 u16LastGDWdPtr)
{
    _u16LastGDWdPtr = u16LastGDWdPtr;
}
#endif

void MHal_XC_SetLastEntryIndex(MS_U8 u8LastEntryIdx)
{
    _u8LastEntryIdx = u8LastEntryIdx;
}

void _MHal_XC_CheckCFDStatus(void)
{
    int i = 0;

    for (i = 0; i < HDR_MEM_ENTRY_NUM; i++)
    {
        MS_BOOL bRetVal = FALSE, bCFDReady = FALSE;
        ST_KDRV_XC_HDR_INFO_ENTRY stEntry;
        memset(&stEntry,0,sizeof(ST_KDRV_XC_HDR_INFO_ENTRY));

        // Get current entry
        bRetVal = getDolbyHDRInfoEntry(_pstDolbyHDRShareMem, i, &stEntry);
        bCFDReady = isCFDDone(_pstDolbyHDRShareMem, i);
        if (bCFDReady)
        {
            MHal_XC_CalculateFrameHDRInfo(i, &stEntry, _pu8InputMDAddr, _pu8InputMDAddr_Cached,
                                          _pu8LutsAddr, _pu8LutsAddr_Cached, _pu8RegSetAddr, _pu8RegSetAddr_Cached,
                                          _pstDolbyHDRShareMem, &_stShareMemInfo,FALSE);
        }

#if OTT_AUTO_PAUSE_DETECT
        if (MHal_XC_IsOTTPausing() && (i == _u8LastEntryIdx) && KHAL_SC_Get_DynamicScaling_Status(E_KDRV_XC_SC0))
        {
            // store IP DS
            MS_BOOL bIP_DSOn = (MHal_XC_R2BYTEMSK(REG_SC_BK1F_10_L, BIT(11)) != 0);
            // Disable IP DS if on
            if (bIP_DSOn)
            {
                MHal_XC_W2BYTEMSK(REG_SC_BK1F_10_L, 0, BIT(11)|BIT(9));
            }

            K_XC_DS_CMDCNT stXCDSIPCmdCnt;
            K_XC_DS_CMDCNT stXCDSOPCmdCnt;
            MS_U8 u8DSIdx = 0;
            MS_U32 u32Depth = 0;
            MS_U32 u32RegsetSize = 0;
            MS_U32 u32LutSize = 0;
            MS_PHY phyFireAdlAddr  = 0;
            MS_U8 *pu8LutAddr = NULL;
            MS_U8 *pu8RegsetAddr = NULL;

            u32RegsetSize = stEntry.u32HDRRegsetSize;
            u32LutSize = stEntry.u32HDRLutSize;

            // write stored adl data
            u8DSIdx = (MHal_XC_R2BYTEMSK(REG_SC_BK1F_79_L, 0xFF00)>>8);//get ds index of pause frame
            if (u8DSIdx < DS_BUFFER_NUM_EX)
            {
#if DOLBY_GD_ENABLE
                if (MHal_XC_IsGDEnabled())
                {
                    ST_KDRV_XC_HDR_GD_FORMAT stGDInfo;
                    getDolbyHDRGDInfo(_u8LastEntryIdx, &stGDInfo);
                    _stHDRGDInfos.astGDInfo[_u16LastGDWdPtr] = stGDInfo;
                    memset(&stXCDSOPCmdCnt, 0, sizeof(K_XC_DS_CMDCNT));
                    MS_U8 u8PwmChNum = MHal_XC_GetPWMPort();
                    MS_U16 u16PWMMask = 0x0;
                    MS_U32 u32Duty = 0;
                    MS_U8 u8GDIdx = _u16LastGDWdPtr;
                    MS_U8 u8GDDSIdx = u8DSIdx;
                    if (u8PwmChNum == 0)
                    {
                        u16PWMMask = 0x3;
                    }
                    MS_U32 u32Period = (MHal_XC_R2BYTEMSK(REG_SC_BKF4_20_L, u16PWMMask) << 16) | MHal_XC_R2BYTE(REG_SC_BKF4_02_L);

                    if (u8GDIdx < GD_BUFFER_SIZE)
                    {
                        if (_stHDRGDInfos.astGDInfo[u8GDIdx].u16GDReady)
                        {
                            u32Duty = DOLBY_GD_TO_DUTY(_stHDRGDInfos.astGDInfo[u8GDIdx].u16GDValue, u32Period);
                            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BKF4_03_L, u32Duty&0xFFFF, K_DS_OP, K_DS_XC, &stXCDSOPCmdCnt, u8GDDSIdx);
                            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BKF4_21_L, (u32Duty>>16)&0x3, K_DS_OP, K_DS_XC, &stXCDSOPCmdCnt, u8GDDSIdx);
                            KHal_SC_Add_NullCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, K_DS_OP, &stXCDSOPCmdCnt, u8GDDSIdx);
                            _stHDRGDInfos.astGDInfo[u8GDIdx].u16GDReady = FALSE;
                            XC_KDBG("OTT GD: %d, %d, %d, %d\n", _u8LastEntryIdx, u8GDDSIdx, _stHDRGDInfos.astGDInfo[u8GDIdx].u16GDValue, u32Duty);
                        }

                        int i = 0, j = (MHal_XC_GetBackendDelayFrame() - MHal_XC_GetPWMDelayFrame());
                        for (i = 0; i < j; i++)
                        {
                            _stHDRGDInfos.astGDInfo[(GD_BUFFER_SIZE + u8GDIdx - i) % DS_BUFFER_NUM_EX].u16GDReady = FALSE;
                        }
                    }
                    else
                    {
                        printk("%s: %d: u8GDIdx[%d] is error, BackendDelayFrame: %d, PWMDelayFrame: %d\n", __func__, __LINE__, u8GDIdx, MHal_XC_GetBackendDelayFrame(), MHal_XC_GetPWMDelayFrame());
                    }
                }
#endif

#ifdef XC_REPLACE_MEMCPY_BY_BDMA
                pu8LutAddr = stEntry.u32HDRLutAddr;
#else
                pu8LutAddr = (MS_U8 *)(_pu8LutsAddr + (stEntry.u32HDRLutAddr - (_stShareMemInfo.phyBaseAddr + HDR_MEM_COMMON_ENTRY_SIZE + HDR_TOTAL_INPUT_MD_SIZE + HDR_TOTAL_REGSET_SIZE)));
#endif

                MHal_XC_WriteStoredHDRAutoDownload(pu8LutAddr, u32LutSize, u8DSIdx, &phyFireAdlAddr, &u32Depth);

                XC_KDBG("%s: 0x%llx, %d\n", __FUNCTION__, phyFireAdlAddr, u32Depth);
                // write fire adl cmd into ds memory

                memset(&stXCDSIPCmdCnt, 0, sizeof(K_XC_DS_CMDCNT));

                //set phyaddr
                KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BK67_29_L, (phyFireAdlAddr / BYTE_PER_WORD) & 0x0000FFFF, K_DS_IP, K_DS_XC, &stXCDSIPCmdCnt, u8DSIdx);
                KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BK67_2A_L, (phyFireAdlAddr / BYTE_PER_WORD) >> 16 & 0x0000FFFF, K_DS_IP, K_DS_XC, &stXCDSIPCmdCnt, u8DSIdx);
                // set depth
                KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BK67_2B_L, u32Depth, K_DS_IP, K_DS_XC, &stXCDSIPCmdCnt, u8DSIdx);
                KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BK67_2C_L, u32Depth, K_DS_IP, K_DS_XC, &stXCDSIPCmdCnt, u8DSIdx);
                // enable auto download
                KHal_SC_WriteSWDSCommand_Mask(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BK67_38_L, BIT(9), K_DS_IP, K_DS_XC, &stXCDSIPCmdCnt, u8DSIdx, BIT(9));
                KHal_SC_WriteSWDSCommand_Mask(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BK67_39_L, BIT(9), K_DS_IP, K_DS_XC, &stXCDSIPCmdCnt, u8DSIdx, BIT(9));
                //KHal_SC_WriteSWDSCommand_Mask(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, REG_SC_BK67_28_L, BIT(0), K_DS_IP, K_DS_XC, &stXCDSIPCmdCnt, u8DSIdx, BIT(0));


                // write stored ds cmd into ds memory
#ifdef XC_REPLACE_MEMCPY_BY_BDMA
                pu8RegsetAddr = stEntry.u32HDRRegsetAddr;
#else
                pu8RegsetAddr = _pu8RegSetAddr + (stEntry.u32HDRRegsetAddr - (_stShareMemInfo.phyBaseAddr + HDR_MEM_COMMON_ENTRY_SIZE + HDR_TOTAL_INPUT_MD_SIZE));
#endif

                KHal_XC_WriteStoredSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, pu8RegsetAddr, u32RegsetSize, K_DS_IP, K_DS_XC, &stXCDSIPCmdCnt, u8DSIdx);
                KHal_SC_Add_NullCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW, E_DS_CLIENT_HDR, K_DS_IP, &stXCDSIPCmdCnt, u8DSIdx);
            }

            // restore IP DS if on
            if (bIP_DSOn)
            {
                MHal_XC_W2BYTEMSK(REG_SC_BK1F_10_L, BIT(11)|BIT(9), BIT(11)|BIT(9));
            }
        }
#endif
    }
}

BOOL MHal_XC_CFDControl(ST_KDRV_XC_CFD_CONTROL_INFO *pstKdrvCFDCtrlInfo)
{
    EN_KDRV_XC_CFD_CTRL_TYPE enCtrlType = pstKdrvCFDCtrlInfo->enCtrlType;
    //MS_U32 u32ParamLen = pstKdrvCFDCtrlInfo->u32ParamLen;
    static MS_BOOL bInitStatus[2] = {FALSE, FALSE};
    static MS_BOOL bHueChange[2] = {FALSE, FALSE};
    static MS_BOOL bContrastChange[2] = {FALSE, FALSE};
    static MS_BOOL bSaturationChange[2] = {FALSE, FALSE};
    static MS_BOOL bColorRangeChange[2] = {FALSE, FALSE};
    static MS_BOOL bLinearRgbChange[2] = {FALSE, FALSE};
    static MS_BOOL bHdrChange[2] = {FALSE, FALSE};
    static MS_BOOL bHdmiChange[2] = {FALSE, FALSE};
    static MS_BOOL bMmChange[2] = {FALSE, FALSE};
    static MS_BOOL bAnalogChange[2] = {FALSE, FALSE};
    static MS_BOOL bSkipPictureSettingChange[2] = {FALSE, FALSE};
    static MS_BOOL bColorCorrectionMatrixChange[2] = {FALSE, FALSE};
    static MS_BOOL bUiH2SModeChange[2] = {FALSE, FALSE};
    static MS_BOOL bEdidSupportPQ = FALSE;
    static MS_BOOL bEdidSupportHLG = FALSE;
    static MS_BOOL bEdidSupportBT2020RGB = FALSE;
    static MS_BOOL bEdidSupportBT2020YCC = FALSE;
    static MS_BOOL bEdidSupportBT2020cYCC = FALSE;

    //MS_U16 u16RetVal = 0;

    switch(enCtrlType)
    {
        case E_KDRV_XC_CFD_CTRL_SET_INIT:
        {
            ST_KDRV_XC_CFD_INIT *pstXcCfdInit = (ST_KDRV_XC_CFD_INIT*)pstKdrvCFDCtrlInfo->pParam;
            XC_KDBG("\033[31mCFD initalize \033[m\n");
            XC_KDBG("\033[31m  Window is %d \033[m\n", pstXcCfdInit->u8Win);
            XC_KDBG("\033[31m  Input source is %d \033[m\n", pstXcCfdInit->u8InputSource);
            bCFDInitFinished[pstXcCfdInit->u8Win] = FALSE;
            memcpy(&_stCfdInit[pstXcCfdInit->u8Win], pstXcCfdInit, sizeof(ST_KDRV_XC_CFD_INIT));
            MHal_XC_EnableHDR(ENABLE);

            if(MAIN_WINDOW == pstXcCfdInit->u8Win)
            {
                // this is for first black frame is not black in dolby ott mode
                // but in normal pip case, we have to avoid sub win mute main win
                // if sub is no signal, then cfd code flow just call cfd_init
                MHal_XC_MuteHDR();
                _enHDRType = 0 ;
            }

            if (CFD_IS_MM(_stCfdInit[pstXcCfdInit->u8Win].u8InputSource) || CFD_IS_DTV(_stCfdInit[pstXcCfdInit->u8Win].u8InputSource))
            {
                MS_U8 u8Version = 0;
                _get_shm_version(&u8Version);
                if ((u8Version == 0) || (u8Version == 2))
                {
                    MHal_XC_SetDolbyStatus(BIT(0), BIT(0));
                }
                else if (u8Version == 3)
                {
                    IsMVOP_TCH = TRUE;
                }
                else
                {
                    MHal_XC_SetDolbyStatus(0, BIT(0));
                    IsMVOP_TCH = FALSE;
                }
            }
            _eSeamlessStatus = SEAMLESS_NONE;
            _eHDMISeamlessStatus = SEAMLESS_NONE;
            _bMVOPFreezeGOPDone = FALSE;
            _bXCFreezeDone = TRUE;
            bTmoFireEnable = FALSE;
            _bCfdInited = TRUE;
            _bTimingChanged = TRUE;
            bInitStatus[pstXcCfdInit->u8Win] = TRUE;
            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            _stCfdHdr[pstXcCfdInit->u8Win].u8HdrType = E_KDRV_XC_CFD_HDR_TYPE_NONE;
            // dolby HDMI to openHDR OTT, video display garbage
            // because dma does not be closed
            MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_BYPASS);
#if DOLBY_GD_ENABLE
            memset(&_stHDRGDInfos, 0, sizeof(_stHDRGDInfos));
            _u8PreDSOPMIdx = 0xFF;
#endif

            bHueChange[pstXcCfdInit->u8Win] = TRUE;
            bContrastChange[pstXcCfdInit->u8Win] = TRUE;
            bSaturationChange[pstXcCfdInit->u8Win] = TRUE;
            bColorRangeChange[pstXcCfdInit->u8Win] = TRUE;
            bLinearRgbChange[pstXcCfdInit->u8Win] = TRUE;
            bHdrChange[pstXcCfdInit->u8Win] = TRUE;
            bHdmiChange[pstXcCfdInit->u8Win] = TRUE;
            bMmChange[pstXcCfdInit->u8Win] = TRUE;
            bAnalogChange[pstXcCfdInit->u8Win] = TRUE;
            bSkipPictureSettingChange[pstXcCfdInit->u8Win] = TRUE;
            bColorCorrectionMatrixChange[pstXcCfdInit->u8Win] = TRUE;
            _bTCH_init = TRUE;
            //for Mdebug Info
            u16SetDSAverTime = 0;
            u16SetDSPeakTime = 0;
            u64FrameNumber = 0;
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_HDMI:
        {
            ST_KDRV_XC_CFD_HDMI *pstXcCfdHdmi = (ST_KDRV_XC_CFD_HDMI*)pstKdrvCFDCtrlInfo->pParam;
            XC_KDBG("\033[31mHDMI input source \033[m\n");
            XC_KDBG("\033[31m  Window is %d \033[m\n", pstXcCfdHdmi->u8Win);
            XC_KDBG("\033[31m  HDMI %s full range\033[m\n", pstXcCfdHdmi->bIsFullRange?"is":"isn't");
            XC_KDBG("\033[31m  AVI infoframe is (%d, %d, %d, %d, %d) \033[m\n", pstXcCfdHdmi->u8PixelFormat, pstXcCfdHdmi->u8Colorimetry, pstXcCfdHdmi->u8ExtendedColorimetry, pstXcCfdHdmi->u8RgbQuantizationRange, pstXcCfdHdmi->u8YccQuantizationRange);
            XC_KDBG("\033[31m  HDR infoframe %s exists\033[m\n", pstXcCfdHdmi->bHDRInfoFrameValid?"is":"isn't");

            if (memcmp(pstXcCfdHdmi, &_stCfdHdmi[pstXcCfdHdmi->u8Win], sizeof(ST_KDRV_XC_CFD_HDMI)) != 0)
            {
                bHdmiChange[pstXcCfdHdmi->u8Win] |= TRUE;
            }

            // If HDMI HDR/SDR be changed at the beginning of the stream.
            // Adapation layer will get the older HDMI information.
            // HDR/SDR freeze code will get the current HDMI information.
            // So we will follow the information that HDR/SDR freeze get.
            if((bSeamlessFirst == FALSE) &&
                ( E_SEAMLESS_HDMI_WITH_FREEZE == MHal_XC_Get_HDRSeamless_HDMI_Path()) && (pstXcCfdHdmi->u8Win == MAIN_WINDOW))
            {
                _stCfdHdmi[pstXcCfdHdmi->u8Win].u32Version = pstXcCfdHdmi-> u32Version;
                _stCfdHdmi[pstXcCfdHdmi->u8Win].u16Length = pstXcCfdHdmi->u16Length;
                _stCfdHdmi[pstXcCfdHdmi->u8Win].u8Win = pstXcCfdHdmi->u8Win;
                _stCfdHdmi[pstXcCfdHdmi->u8Win].bHDRInfoFrameValid = pstXcCfdHdmi->bHDRInfoFrameValid;
            }
            else
            {
                memcpy(&_stCfdHdmi[pstXcCfdHdmi->u8Win], pstXcCfdHdmi, sizeof(ST_KDRV_XC_CFD_HDMI));
            }

            // for both Open and Dolby HDR
            // we must set intial auto seamless to FALSE
            // otherwise HDMI open HDR will be PQ bypssed due to HW auto seamless
            MHal_XC_EnableAutoSeamless(FALSE);
            MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_BYPASS);

#ifdef DYNAMIC_SWITCH_SDR_OPENHDR
            if ((CFD_IS_HDMI(_stCfdInit[pstXcCfdHdmi->u8Win].u8InputSource)) && bInitStatus[pstXcCfdHdmi->u8Win] == FALSE)
            {
                MS_BOOL bEnable = 0;

                // when hdmi seamless, then IRQ CAN NOT use mutex
                if( ( E_SEAMLESS_HDMI_WITHOUT_MUTE_AND_FREEZE != MHal_XC_Get_HDRSeamless_HDMI_Path()) &&
                    ( E_SEAMLESS_HDMI_WITH_FREEZE != MHal_XC_Get_HDRSeamless_HDMI_Path()))
                {
                    // mutex for fix RR2077 continue and fast packet change
                    mutex_lock(&_cfd_mutex);
                }

                //update CFD para.
                ST_KDRV_XC_CFD_FIRE *pstCfdFire = &_stCfdFire[pstXcCfdHdmi->u8Win];
                // Main Control
                pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
                if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                {
                }

                pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetHdmiParam(pstCfdFire);
                if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                {
                    //return FALSE;
                }

                // tell dolby code, you should recall CFD
                _bHDRTOSDR_CFD_DONE  = TRUE;

                // when hdmi seamless, then IRQ CAN NOT use mutex
                if( ( E_SEAMLESS_HDMI_WITHOUT_MUTE_AND_FREEZE != MHal_XC_Get_HDRSeamless_HDMI_Path()) &&
                    ( E_SEAMLESS_HDMI_WITH_FREEZE != MHal_XC_Get_HDRSeamless_HDMI_Path()))
                {
                    mutex_unlock(&_cfd_mutex);
                }

                // when hdmi seamless, then IRQ CAN NOT use mutex
                if( ( E_SEAMLESS_HDMI_WITHOUT_MUTE_AND_FREEZE != MHal_XC_Get_HDRSeamless_HDMI_Path()) &&
                    ( E_SEAMLESS_HDMI_WITH_FREEZE != MHal_XC_Get_HDRSeamless_HDMI_Path()))
                {
                    // wait for CFD done(Dolby)
                    MS_U8 u8Timer = 0;
                    while(_bHDRTOSDR_CFD_DONE && u8Timer <3)
                    {
                        u8Timer ++;
                        msleep(50);
                    }
                }
            }
#endif
            MHal_XC_W2BYTEMSK(REG_SC_BK79_10_L, BIT(8), BIT(8));
            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_VGA:
        case E_KDRV_XC_CFD_CTRL_SET_TV:
        case E_KDRV_XC_CFD_CTRL_SET_CVBS:
        case E_KDRV_XC_CFD_CTRL_SET_SVIDEO:
        case E_KDRV_XC_CFD_CTRL_SET_YPBPR:
        case E_KDRV_XC_CFD_CTRL_SET_SCART:
        case E_KDRV_XC_CFD_CTRL_SET_DVI:
        {
            ST_KDRV_XC_CFD_ANALOG *pstXcCfdAnalog = (ST_KDRV_XC_CFD_ANALOG*)pstKdrvCFDCtrlInfo->pParam;
            XC_KDBG("\033[31mAnalog input source \033[m\n");
            XC_KDBG("\033[31m  Window is %d \033[m\n", pstXcCfdAnalog->u8Win);
            XC_KDBG("\033[31m  Color format is %d\033[m\n", pstXcCfdAnalog->u8ColorFormat);
            XC_KDBG("\033[31m  Color data format is %d\033[m\n", pstXcCfdAnalog->u8ColorDataFormat);
            XC_KDBG("\033[31m  Analog %s full range\033[m\n", pstXcCfdAnalog->bIsFullRange?"is":"isn't");
            XC_KDBG("\033[31m  Color primaries is %d\033[m\n", pstXcCfdAnalog->u8ColorPrimaries);
            XC_KDBG("\033[31m  Transfer characteristics  is %d\033[m\n", pstXcCfdAnalog->u8TransferCharacteristics);
            XC_KDBG("\033[31m  Matrix coefficients is %d\033[m\n", pstXcCfdAnalog->u8MatrixCoefficients);

            if (!CFD_IS_HDMI(_stCfdInit[pstXcCfdAnalog->u8Win].u8InputSource) &&
                !CFD_IS_MM(_stCfdInit[pstXcCfdAnalog->u8Win].u8InputSource) &&
                !CFD_IS_DTV(_stCfdInit[pstXcCfdAnalog->u8Win].u8InputSource))
            {
                if (memcmp(pstXcCfdAnalog, &_stCfdAnalog[pstXcCfdAnalog->u8Win], sizeof(ST_KDRV_XC_CFD_ANALOG)) != 0)
                {
                    bAnalogChange[pstXcCfdAnalog->u8Win] |= TRUE;
                }
            }
            memcpy(&_stCfdAnalog[pstXcCfdAnalog->u8Win], pstXcCfdAnalog, sizeof(ST_KDRV_XC_CFD_ANALOG));

            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_PANEL:
        {
            //MS_U32 *pu32Version = (MS_U32*)pstKdrvCFDCtrlInfo->pParam;
            MS_U16 *pu16Length = (MS_U16*)(pstKdrvCFDCtrlInfo->pParam + sizeof(MS_U32));

            MS_U16 u16CpLength = sizeof(ST_KDRV_XC_CFD_PANEL);

            if ((*pu16Length) < u16CpLength)
            {
                u16CpLength = (*pu16Length) ;
            }

            memset(&_stCfdPanel, 0, sizeof(ST_KDRV_XC_CFD_PANEL));
            memcpy(&_stCfdPanel, pstKdrvCFDCtrlInfo->pParam, u16CpLength);

            if ((_stCfdPanel.u32Version < 2) || _stCfdPanel.u8PWMPort > MAX_PWM_PORT)
            {
                _stCfdPanel.u8PWMPort = 0x0;
                _stCfdPanel.bGlobalDimming = TRUE;
                _stCfdPanel.s8DelayFrame = 0x0;
            }

            XC_KDBG("\033[31mPanel info \033[m\n");
            XC_KDBG("\033[31m  R is (%d, %d)\033[m\n", _stCfdPanel.u16Display_Primaries_x[0], _stCfdPanel.u16Display_Primaries_y[0]);
            XC_KDBG("\033[31m  G is (%d, %d)\033[m\n", _stCfdPanel.u16Display_Primaries_x[1], _stCfdPanel.u16Display_Primaries_y[1]);
            XC_KDBG("\033[31m  B is (%d, %d)\033[m\n", _stCfdPanel.u16Display_Primaries_x[2], _stCfdPanel.u16Display_Primaries_y[2]);
            XC_KDBG("\033[31m  W is (%d, %d)\033[m\n", _stCfdPanel.u16White_point_x, _stCfdPanel.u16White_point_y);
            XC_KDBG("\033[31m  Luminanceis (%d, %d, %d)\033[m\n", _stCfdPanel.u16MaxLuminance, _stCfdPanel.u16MedLuminance, _stCfdPanel.u16MinLuminance);
            XC_KDBG("\033[31m  Linear RGB %d\033[m\n", _stCfdPanel.bLinearRgb);
            XC_KDBG("\033[31m  PWM Port %d\033[m\n", _stCfdPanel.u8PWMPort);
            XC_KDBG("\033[31m  bGlobalDimming %d\033[m\n", _stCfdPanel.bGlobalDimming);
            XC_KDBG("\033[31m  s8DelayFrame %d\033[m\n", _stCfdPanel.s8DelayFrame);

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetPanelParam(&_stCfdPanel);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_HDR:
        {
            ST_KDRV_XC_CFD_HDR stCfdHdr;

            MS_U16 *pu16Length = (MS_U16*)(pstKdrvCFDCtrlInfo->pParam + sizeof(MS_U32));

            MS_U16 u16CpLength = sizeof(ST_KDRV_XC_CFD_HDR);

            if ((*pu16Length) < u16CpLength)
            {
                u16CpLength = (*pu16Length) ;
            }

            memset(&stCfdHdr, 0, sizeof(ST_KDRV_XC_CFD_HDR));
            memcpy(&stCfdHdr, pstKdrvCFDCtrlInfo->pParam, u16CpLength);

            XC_KDBG("\033[31mHDR info \033[m\n");
            XC_KDBG("\033[31m  Window is %d \033[m\n", stCfdHdr.u8Win);
            XC_KDBG("\033[31m  HDR type is %d \033[m\n", stCfdHdr.u8HdrType);
            XC_KDBG("\033[31m  HDR Level is %d \033[m\n", stCfdHdr.u8TmoLevel);

            if (stCfdHdr.u8HdrType != _stCfdHdr[stCfdHdr.u8Win].u8HdrType)
            {
                bHdrChange[stCfdHdr.u8Win] |= TRUE;
            }

            memcpy(&_stCfdHdr[stCfdHdr.u8Win], &stCfdHdr, sizeof(ST_KDRV_XC_CFD_HDR));

            // also enable HDR clk, auther: jonathan.yen
            MHal_XC_EnableHDRCLK(TRUE,TRUE);

            if (IS_DOLBY_HDR(stCfdHdr.u8Win))
            {
                if((CFD_IS_HDMI(_stCfdInit->u8InputSource)) || (CFD_IS_DVI(_stCfdInit->u8InputSource)))
                {
                    MHal_XC_SetHDR_DMARequestOFF(DISABLE, TRUE);
                }
                else
                {
                    MHal_XC_SetHDR_DMARequestOFF(ENABLE, TRUE);
                }
            }
            else
            {
                MHal_XC_SetHDR_DMARequestOFF(ENABLE, TRUE);
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_EDID:
        {
            ST_KDRV_XC_CFD_EDID *pstCfdEdid = (ST_KDRV_XC_CFD_EDID*)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;
            XC_KDBG("\033[31mEDID info \033[m\n");
            XC_KDBG("u8HDMISinkHDRDataBlockValid is %d\n", pstCfdEdid->u8HDMISinkHDRDataBlockValid);
            XC_KDBG("u8HDMISinkEOTF is %d\n", pstCfdEdid->u8HDMISinkEOTF);
            XC_KDBG("u8HDMISinkSM is %d\n", pstCfdEdid->u8HDMISinkSM);
            XC_KDBG("u8HDMISinkDesiredContentMaxLuminance is %d\n", pstCfdEdid->u8HDMISinkDesiredContentMaxLuminance);
            XC_KDBG("u8HDMISinkDesiredContentMaxFrameAvgLuminance is %d\n", pstCfdEdid->u8HDMISinkDesiredContentMaxFrameAvgLuminance);
            XC_KDBG("u8HDMISinkDesiredContentMinLuminance is %d\n", pstCfdEdid->u8HDMISinkDesiredContentMinLuminance);
            XC_KDBG("u8HDMISinkHDRDataBlockLength is %d\n", pstCfdEdid->u8HDMISinkHDRDataBlockLength);
            XC_KDBG("u16Display_Primaries_x[0] is %d\n", pstCfdEdid->u16Display_Primaries_x[0]);
            XC_KDBG("u16Display_Primaries_x[1] is %d\n", pstCfdEdid->u16Display_Primaries_x[1]);
            XC_KDBG("u16Display_Primaries_x[2] is %d\n", pstCfdEdid->u16Display_Primaries_x[2]);
            XC_KDBG("u16Display_Primaries_y[0] is %d\n", pstCfdEdid->u16Display_Primaries_y[0]);
            XC_KDBG("u16Display_Primaries_y[1] is %d\n", pstCfdEdid->u16Display_Primaries_y[1]);
            XC_KDBG("u16Display_Primaries_y[2] is %d\n", pstCfdEdid->u16Display_Primaries_y[2]);
            XC_KDBG("u16White_point_x is %d\n", pstCfdEdid->u16White_point_x);
            XC_KDBG("u16White_point_y is %d\n", pstCfdEdid->u16White_point_y);
            XC_KDBG("u8HDMISinkEDIDBaseBlockVersion is %d\n", pstCfdEdid->u8HDMISinkEDIDBaseBlockVersion);
            XC_KDBG("u8HDMISinkEDIDBaseBlockReversion is %d\n", pstCfdEdid->u8HDMISinkEDIDBaseBlockReversion);
            XC_KDBG("u8HDMISinkEDIDCEABlockReversion is %d\n", pstCfdEdid->u8HDMISinkEDIDCEABlockReversion);
            XC_KDBG("u8HDMISinkVCDBValid is %d\n", pstCfdEdid->u8HDMISinkVCDBValid);
            XC_KDBG("u8HDMISinkSupportYUVFormat is %d\n", pstCfdEdid->u8HDMISinkSupportYUVFormat);
            XC_KDBG("u8HDMISinkExtendedColorspace is %d\n", pstCfdEdid->u8HDMISinkExtendedColorspace);
            XC_KDBG("u8HDMISinkEDIDValid is %d\n", pstCfdEdid->u8HDMISinkEDIDValid);

            if ((E_CFD_VALID == pstCfdEdid->u8HDMISinkEDIDValid)
                && (E_CFD_VALID == pstCfdEdid->u8HDMISinkHDRDataBlockValid)
                && (2 <= pstCfdEdid->u8HDMISinkHDRDataBlockLength))
            {
                if (0x04 == (pstCfdEdid->u8HDMISinkEOTF&0x04)) //SMPTE 2084
                {
                    bEdidSupportPQ = TRUE;
                }

                if (0x08 == (pstCfdEdid->u8HDMISinkEOTF&0x08)) // HLG
                {
                    bEdidSupportHLG = TRUE;
                }
            }

            if (0x20 == (pstCfdEdid->u8HDMISinkExtendedColorspace&0x20))
            {
                bEdidSupportBT2020cYCC = TRUE;
            }

            if (0x40 == (pstCfdEdid->u8HDMISinkExtendedColorspace&0x40))
            {
                bEdidSupportBT2020YCC = TRUE;
            }

            if (0x80 == (pstCfdEdid->u8HDMISinkExtendedColorspace&0x80))
            {
                bEdidSupportBT2020RGB = TRUE;
            }

            XC_KDBG("\033[31m  bEdidSupportPQ is %d, bEdidSupportBT2020RGB is %d \033[m\n", bEdidSupportPQ, bEdidSupportBT2020RGB);

            //0:auto depends on STB rule
            //1:always do HDR2SDR for HDR input
            //2:always not do HDR2SDR for HDR input
            MS_U8 u8HDRUIH2SMode = _stCfdOsd[0].u8HDR_UI_H2SMode;
            if (_stCfdOsd[0].u8HDR_UI_H2SMode == 0)
            {
                if (bEdidSupportPQ && bEdidSupportBT2020RGB)
                {
                    u8HDRUIH2SMode = 2; //for tv, force bypass
                }
            }
            Mapi_Cfd_OSD_H2SUI_Set(u8HDRUIH2SMode);
            XC_KDBG("\033[31m  HDR AUTO/ON/OFF UI Setting is %d, CFD Setting is %d \033[m\n", _stCfdOsd[0].u8HDR_UI_H2SMode, u8HDRUIH2SMode);

            _eCfdOutputType = E_CFD_OUTPUT_SOURCE_HDMI;

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetEdidParam(pstCfdEdid);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstCfdFire = &_stCfdFire[0];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
                Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_OSD:
        {
            MS_U8 u8Win = 0;
            MS_U16 *pu16Length = (MS_U16*)(pstKdrvCFDCtrlInfo->pParam + sizeof(MS_U32));

            MS_U16 u16CpLength = sizeof(ST_KDRV_XC_CFD_OSD);
            ST_KDRV_XC_CFD_OSD stCfdOsd;

            if ((*pu16Length) < u16CpLength)
            {
                u16CpLength = (*pu16Length) ;
            }

            memset(&stCfdOsd, 0, sizeof(ST_KDRV_XC_CFD_OSD));
            memcpy(&stCfdOsd, pstKdrvCFDCtrlInfo->pParam, u16CpLength);

            u8Win = stCfdOsd.u32Version>=2?stCfdOsd.u8Win:0;

            if (stCfdOsd.u32Version == 0)
            {
                if (stCfdOsd.u16Hue != _stCfdOsd[u8Win].u16Hue)
                {
                    bHueChange[u8Win] |= TRUE;
                }

                if (stCfdOsd.u16Saturation != _stCfdOsd[u8Win].u16Saturation)
                {
                    bSaturationChange[u8Win] |= TRUE;
                }

                if (stCfdOsd.u16Contrast != _stCfdOsd[u8Win].u16Contrast)
                {
                    bContrastChange[u8Win] |= TRUE;
                }

                _stCfdOsd[u8Win].u16Hue = stCfdOsd.u16Hue;
                _stCfdOsd[u8Win].u16Saturation = stCfdOsd.u16Saturation;
                _stCfdOsd[u8Win].u16Contrast = stCfdOsd.u16Contrast;
            }
            else if (stCfdOsd.u32Version >= 1)
            {

                if (stCfdOsd.bHueValid)
                {
                    if (stCfdOsd.u16Hue != _stCfdOsd[u8Win].u16Hue)
                    {
                        // u8Win start at version 2.
                        bHueChange[u8Win] |= TRUE;
                    }
                    _stCfdOsd[u8Win].u16Hue = stCfdOsd.u16Hue;
                }

                if (stCfdOsd.bSaturationValid)
                {
                    if (stCfdOsd.u16Saturation != _stCfdOsd[u8Win].u16Saturation)
                    {
                        // u8Win start at version 2.
                        bSaturationChange[u8Win] |= TRUE;
                    }
                    _stCfdOsd[u8Win].u16Saturation = stCfdOsd.u16Saturation;
                }

                if (stCfdOsd.bContrastValid)
                {
                    if (stCfdOsd.u16Contrast != _stCfdOsd[u8Win].u16Contrast)
                    {
                        // u8Win start at version 2.
                        bContrastChange[u8Win] |= TRUE;
                    }
                    _stCfdOsd[u8Win].u16Contrast = stCfdOsd.u16Contrast;
                }

                if (stCfdOsd.bBacklightValid)
                {
                    MS_BOOL bBacklightChanged = FALSE;
                    static MS_U32 u32DolbyBacklight = 0xFFFFFFFF;

                    _stCfdOsd[u8Win].u32Backlight = stCfdOsd.u32Backlight;
                    _stCfdOsd[u8Win].u32MinBacklight = stCfdOsd.u32MinBacklight;
                    _stCfdOsd[u8Win].u32MaxBacklight = stCfdOsd.u32MaxBacklight;

                    if (_stCfdOsd[u8Win].u32Backlight != u32DolbyBacklight)
                    {
                        bBacklightChanged = TRUE;
                    }
                    u32DolbyBacklight = _stCfdOsd[u8Win].u32Backlight;

                    if (bBacklightChanged)
                    {
                        MS_BOOL bSpinLocked = FALSE;

                        MHal_XC_UpdateDolbyPQSetting(EN_DOLBY_PQ_BACKLIGHT, &_stCfdOsd[u8Win].u32Backlight);

                        if (MHal_XC_GetDolbyStatus() & BIT(0))
                        {
                            spin_lock_irq(&_spinlock_xc_dolby_hdr);
                            bSpinLocked = TRUE;
                            _MHal_XC_CheckCFDStatus();
                        }

                        if (bSpinLocked)
                        {
                            spin_unlock_irq(&_spinlock_xc_dolby_hdr);
                        }
                    }
                }

                if ((stCfdOsd.bColorRangeValid) && ((_stCfdOsd[u8Win].u8UltraBlackLevel == 0) && (_stCfdOsd[u8Win].u8UltraWhiteLevel == 0)))
                {
                    if (stCfdOsd.u8ColorRange != _stCfdOsd[u8Win].u8ColorRange)
                    {
                        bColorRangeChange[u8Win] |= TRUE;
                    }
                    _stCfdOsd[u8Win].u8ColorRange = stCfdOsd.u8ColorRange;
                }

                if ((stCfdOsd.bUltraBlackLevelValid || stCfdOsd.bUltraWhiteLevelValid) &&
                    ((CFD_IS_HDMI(_stCfdInit[u8Win].u8InputSource)) && (_stCfdHdmi[u8Win].bIsFullRange == false) && (_stCfdOsd[u8Win].u8ColorRange == 0)))
                {
                    if (stCfdOsd.bUltraBlackLevelValid)
                    {
                        _stCfdOsd[u8Win].u8UltraBlackLevel = stCfdOsd.u8UltraBlackLevel;
                    }

                    if (stCfdOsd.bUltraWhiteLevelValid)
                    {
                        _stCfdOsd[u8Win].u8UltraWhiteLevel = stCfdOsd.u8UltraWhiteLevel;
                    }

                    Api_UltraBlackAndWhite(_stCfdOsd[u8Win].u8UltraBlackLevel, _stCfdOsd[u8Win].u8UltraWhiteLevel);
                }

                if (stCfdOsd.bSkipPictureSettingValid)
                {
                    if (stCfdOsd.bSkipPictureSetting!= _stCfdOsd[u8Win].bSkipPictureSetting)
                    {
                        bSkipPictureSettingChange[u8Win] |= TRUE;
                    }
                    _stCfdOsd[u8Win].bSkipPictureSetting = stCfdOsd.bSkipPictureSetting;
                }
#ifdef DOLBY_1_4_2
                if (stCfdOsd.bViewModeValid)
                {
                    MS_BOOL bSpinLocked = FALSE;

                    MS_BOOL bViewModeChanged = FALSE;
                    static MS_U8 u8DolbyViewMode = 0xFFFF;

                    _stCfdOsd[u8Win].u8ViewMode = stCfdOsd.u8ViewMode;
                    _stCfdOsd[u8Win].bViewModeValid = stCfdOsd.bViewModeValid;

                    if (_stCfdOsd[u8Win].u8ViewMode != u8DolbyViewMode)
                    {
                        bViewModeChanged = TRUE;
                    }
                    u8DolbyViewMode = _stCfdOsd[u8Win].u8ViewMode;

                    if (bViewModeChanged)
                    {
                        DoVi_UpdateViewMode(stCfdOsd.u8ViewMode);
                        if (MHal_XC_GetDolbyStatus() & BIT(0))
                        {
                            spin_lock_irq(&_spinlock_xc_dolby_hdr);
                            bSpinLocked = TRUE;
                            _MHal_XC_CheckCFDStatus();
                        }

                        if (bSpinLocked)
                        {
                            spin_unlock_irq(&_spinlock_xc_dolby_hdr);
                        }

                    }

                }
#endif

                if (stCfdOsd.bColorCorrectionValid)
                {
                    if (memcmp(stCfdOsd.s16ColorCorrectionMatrix, _stCfdOsd[u8Win].s16ColorCorrectionMatrix, sizeof(stCfdOsd.s16ColorCorrectionMatrix)) != 0)
                    {
                        bColorCorrectionMatrixChange[u8Win] |= TRUE;
                    }
                    memcpy(_stCfdOsd[u8Win].s16ColorCorrectionMatrix, stCfdOsd.s16ColorCorrectionMatrix, sizeof(stCfdOsd.s16ColorCorrectionMatrix));
                }
            }

            XC_KDBG("  u8Win is %d\n", _stCfdOsd[u8Win].u8Win);
            XC_KDBG("  u16Hue is %d\n", _stCfdOsd[u8Win].u16Hue);
            XC_KDBG("  u16Saturation is %d\n", _stCfdOsd[u8Win].u16Saturation);
            XC_KDBG("  u16Contrast is %d\n", _stCfdOsd[u8Win].u16Contrast);
            XC_KDBG("  u32Backlight is %d\n", _stCfdOsd[u8Win].u32Backlight);
            XC_KDBG("  u32MinBacklight is %d\n", _stCfdOsd[u8Win].u32MinBacklight);
            XC_KDBG("  u32MaxBacklight is %d\n", _stCfdOsd[u8Win].u32MaxBacklight);
            XC_KDBG("  u8ColorRange is %d\n", _stCfdOsd[u8Win].u8ColorRange);
            XC_KDBG("  u8UltraBlackLevel is %d\n", _stCfdOsd[u8Win].u8UltraBlackLevel);
            XC_KDBG("  u8UltraWhiteLevel is %d\n", _stCfdOsd[u8Win].u8UltraWhiteLevel);
            XC_KDBG("  bSkipPictureSetting is %d\n", _stCfdOsd[u8Win].bSkipPictureSetting);
            XC_KDBG("  bViewModeValid is %d\n", _stCfdOsd[u8Win].bViewModeValid);
            XC_KDBG("  u8ViewMode is %d\n", _stCfdOsd[u8Win].u8ViewMode);

	      K_XC_GETDS_Info stDsInfo;
	      KApi_XC_GetDSInfo(0, &stDsInfo, E_KDRV_MAIN_WINDOW);
		if(stDsInfo.bDynamicScalingEnable == TRUE)
		{
	     		MHal_XC_SetOSDSettingFlag (TRUE);
	       }

            if (((stCfdOsd.bColorRangeValid) && (bInitStatus[u8Win] == FALSE) && (bColorRangeChange[u8Win] == TRUE)) ||
                ((stCfdOsd.bSkipPictureSettingValid) && (bInitStatus[u8Win] == FALSE) && (bSkipPictureSettingChange[u8Win] == TRUE)) ||
                ((stCfdOsd.bColorCorrectionValid) && (bInitStatus[u8Win] == FALSE) && (bColorCorrectionMatrixChange[u8Win] == TRUE)))
            {
                ST_KDRV_XC_CFD_FIRE *pstCfdFire = &_stCfdFire[u8Win];

                _stCfdOsd[u8Win].u8OSDUIEn = 1;
                _stCfdOsd[u8Win].u8OSDUIMode = (bColorRangeChange[u8Win]==TRUE)?0:1;

                // Init Ip
                pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
                if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                {
                }

                // Main Control
                pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
                if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                {
                }

                // OSD
                pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetOsdParam(&_stCfdOsd[u8Win]);
                if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                {
                    return FALSE;
                }
                Mapi_Cfd_Maserati_DLCIP_CurveMode_Set(pstCfdFire->u8Win, _stCfdDlc.bUseCustomerDlcCurve);


                // for skip CFD when OSD control in the case of openHDR SDR seamless
                if((MHal_XC_Get_SharedMemVersion(0) != getOpenHDRSeamlessVersion()) ||
                   (!(CFD_IS_DTV(pstCfdFire->u8InputSource) ||CFD_IS_MM(pstCfdFire->u8InputSource) )))
                {
                    // Fire
                    // THIS IS A PATCH
                    if (MHal_XC_GetDolbyStatus() == 0)
                    {
                        Color_Format_Driver();
                        KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                        MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                    }
                    else
                    {
                        //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                        MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                    }
                }

                bColorRangeChange[u8Win] = FALSE;
                bSkipPictureSettingChange[u8Win] = FALSE;
                bColorCorrectionMatrixChange[u8Win] = FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_DLC:
        {
            ST_KDRV_XC_CFD_DLC *pstCfdDlc = (ST_KDRV_XC_CFD_DLC*)pstKdrvCFDCtrlInfo->pParam;
            XC_KDBG("\033[31mDLC info \033[m\n");
            XC_KDBG("\033[31m  Customer DLC is %d \033[m\n", pstCfdDlc->bUseCustomerDlcCurve);
            memcpy(&_stCfdDlc, pstCfdDlc, sizeof(ST_KDRV_XC_CFD_DLC));

            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_LINEAR_RGB:
        {
            ST_KDRV_XC_CFD_LINEAR_RGB *pstCfdLinearRgb = (ST_KDRV_XC_CFD_LINEAR_RGB*)pstKdrvCFDCtrlInfo->pParam;
            XC_KDBG("\033[31mLinear RGB info \033[m\n");
            XC_KDBG("\033[31m  Window is %d \033[m\n", pstCfdLinearRgb->u8Win);
            XC_KDBG("\033[31m  Enable is %d\033[m\n", pstCfdLinearRgb->bEnable);
            if (pstCfdLinearRgb->bEnable != _stCfdLinearRgb.bEnable)
            {
                bLinearRgbChange[pstCfdLinearRgb->u8Win] |= TRUE;
            }
            memcpy(&_stCfdLinearRgb, pstCfdLinearRgb, sizeof(ST_KDRV_XC_CFD_LINEAR_RGB));

            if (bInitStatus[pstCfdLinearRgb->u8Win] == TRUE)
            {
                pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
                break;
            }

            if (bLinearRgbChange[pstCfdLinearRgb->u8Win] == TRUE)
            {
                ST_KDRV_XC_CFD_FIRE *pstCfdFire = &_stCfdFire[pstCfdLinearRgb->u8Win];

                _stCfdOsd[pstCfdLinearRgb->u8Win].u8OSDUIEn = 1;
                _stCfdOsd[pstCfdLinearRgb->u8Win].u8OSDUIMode = 0;
                MHal_XC_CFD_SetOsdParam(&_stCfdOsd[pstCfdLinearRgb->u8Win]);

                pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
                if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                {
                    return FALSE;
                }

                // Fire
                // THIS IS A PATCH
                if (MHal_XC_GetDolbyStatus() == 0)
                {
                    Color_Format_Driver();
                    if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_START)
                    {
                        SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_SET_LINEAR_RGB][SKIP_FRIE]\n", __LINE__, __FUNCTION__);
                    }
                    else
                    {
                        KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                        MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                    }
                }
                else
                {
                    //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                    MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                }
            }
            bLinearRgbChange[pstCfdLinearRgb->u8Win] = FALSE;

            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }

        case E_KDRV_XC_CFD_CTRL_GET_STATUS:
        {
            ST_KDRV_XC_CFD_STATUS *pstCfdStatus = (ST_KDRV_XC_CFD_STATUS*)pstKdrvCFDCtrlInfo->pParam;
            if (MHal_XC_GetDolbyStatus())
            {
                pstCfdStatus->u8VideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_DOLBY;
            }
            else
            {
                if (CFD_IS_MM(_stCfdInit[pstCfdStatus->u8Win].u8InputSource)|| CFD_IS_DTV(_stCfdInit[pstCfdStatus->u8Win].u8InputSource))
                {
                    ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info stFormatInfo;
                    MHal_XC_CFD_WithdrawMMParam(&stFormatInfo, pstCfdStatus->u8Win);
                    if (((stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.bVUIValid == TRUE) && (stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics == 16))
                        || _stCfdHdr[pstCfdStatus->u8Win].u8HdrType == E_KDRV_XC_CFD_HDR_TYPE_OPEN)
                        {
                        // MM Open HDR
                        pstCfdStatus->u8VideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_OPEN;

                        if(pstCfdStatus->u32Version >= 3)
                        {
                            // get MM open HDR metadata
                            memcpy(&pstCfdStatus->HDRMetadata.stHDRMemFormatCFD, &stFormatInfo.HDRMemFormat.stHDRMemFormatCFD, sizeof(ST_KDRV_XC_HDR_CFD_MEMORY_FORMAT));
                        }
                    }
                    else if (((stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.bVUIValid == TRUE) && (stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics == 18))
                        || _stCfdHdr[pstCfdStatus->u8Win].u8HdrType == E_KDRV_XC_CFD_HDR_TYPE_HLG)
                    {
                        // MM HLG HDR
                        pstCfdStatus->u8VideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_HLG;
                    }
                    else
                    {
                        // SDR
                        pstCfdStatus->u8VideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_NONE;
                    }
                }
                else if (CFD_IS_HDMI(_stCfdInit[pstCfdStatus->u8Win].u8InputSource))
                {
                    if ((_stCfdHdmi[pstCfdStatus->u8Win].bHDRInfoFrameValid == TRUE) && (_stCfdHdmi[pstCfdStatus->u8Win].u8EOTF == 2))
                    {
                        // HDMI Open HDR
                        pstCfdStatus->u8VideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_OPEN;

                        if(pstCfdStatus->u32Version >= 3)
                        {
                            memcpy(&pstCfdStatus->HDRMetadata.stHdmiInfoCFD, &_stCfdHdmi[pstCfdStatus->u8Win], sizeof(ST_KDRV_XC_CFD_HDMI));
                        }
                    }
                    else if ((_stCfdHdmi[pstCfdStatus->u8Win].bHDRInfoFrameValid == TRUE) && (_stCfdHdmi[pstCfdStatus->u8Win].u8EOTF == 3))
                    {
                        // HDMI HLG HDR
                        pstCfdStatus->u8VideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_HLG;
                    }
                    else
                    {
                        // SDR
                        pstCfdStatus->u8VideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_NONE;
                    }
                }
                else
                {
                    // SDR
                    pstCfdStatus->u8VideoHdrType = E_KDRV_XC_CFD_HDR_TYPE_NONE;
                }
            }

            if ((pstCfdStatus->u8VideoHdrType == E_KDRV_XC_CFD_HDR_TYPE_DOLBY) && (_stCfdHdr[pstCfdStatus->u8Win].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_DOLBY))
            {
                // Dolby HDR running
                pstCfdStatus->bIsHdrRunning = TRUE;
            }
            else if ((pstCfdStatus->u8VideoHdrType == E_KDRV_XC_CFD_HDR_TYPE_OPEN) && (_stCfdHdr[pstCfdStatus->u8Win].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_OPEN))
            {
                // Open HDR running
                pstCfdStatus->bIsHdrRunning = TRUE;
            }
            else if ((pstCfdStatus->u8VideoHdrType == E_KDRV_XC_CFD_HDR_TYPE_HLG) && (_stCfdHdr[pstCfdStatus->u8Win].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_HLG))
            {
                // HLG running
                pstCfdStatus->bIsHdrRunning = TRUE;
            }
            else
            {
                pstCfdStatus->bIsHdrRunning = FALSE;
            }

            if (pstCfdStatus->u32Version >= 1)
            {
                if (CFD_IS_HDMI(_stCfdInit[pstCfdStatus->u8Win].u8InputSource))
                {
                    // HDMI
                    pstCfdStatus->bIsFullRange = _stCfdHdmi[pstCfdStatus->u8Win].bIsFullRange;
                }
                else if (CFD_IS_MM(_stCfdInit[pstCfdStatus->u8Win].u8InputSource) || CFD_IS_DTV(_stCfdInit[pstCfdStatus->u8Win].u8InputSource))
                {
                    // MM/DTV
                    if ((IS_HDMI_DOLBY) || (IS_OTT_DOLBY))
                    {
                        pstCfdStatus->bIsFullRange = 0;
                    }
                    else
                    {
                        ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info stFormatInfo;
                        MHal_XC_CFD_WithdrawMMParam(&stFormatInfo, pstCfdStatus->u8Win);
                        pstCfdStatus->bIsFullRange = stFormatInfo.HDRMemFormat.stHDRMemFormatCFD.u8Video_Full_Range_Flag;
                    }
                }
                else
                {
                    // Analog
                    pstCfdStatus->bIsFullRange = _stCfdAnalog[pstCfdStatus->u8Win].bIsFullRange;
                }

                if (_stCfdOsd[pstCfdStatus->u8Win].u8UltraBlackLevel != 0 || _stCfdOsd[pstCfdStatus->u8Win].u8UltraWhiteLevel != 0)
                {
                    pstCfdStatus->bUltraBlackWhiteActive = true;
                }
                else
                {
                    pstCfdStatus->bUltraBlackWhiteActive = false;
                }
            }
            if (pstCfdStatus->u32Version >= 2)
            {
                pstCfdStatus->u8ColorType=Mapi_Cfd_Share_information(E_Gamut,pstCfdStatus->u8Win);
                pstCfdStatus->u8TMOColorType=Mapi_Cfd_Share_information(E_TransFunction,pstCfdStatus->u8Win);
            }

            if ( (pstCfdStatus->u32Version >= 6) &&
                 //(CFD_IS_HDMI(_stCfdInit[pstCfdStatus->u8Win].u8InputSource)) &&
                 ((E_SEAMLESS_HDMI_WITH_FREEZE == MHal_XC_Get_HDRSeamless_HDMI_Path()) ||
                  (E_SEAMLESS_MVOPSOURCE_WITH_FREEZE == MHal_XC_Get_HDRSeamless_MVOPSOURCE_Path())) )
            {
                if(CFD_IS_HDMI(_stCfdInit[pstCfdStatus->u8Win].u8InputSource))
                {
                    if(_eHDMISeamlessStatus == SEAMLESS_DATACHANGED)
                    {
                        pstCfdStatus->bHDMIPacketChange = TRUE;
                        pstCfdStatus->bSeamlessCFDDone = FALSE;
                    }
                    else if(_eHDMISeamlessStatus == SEAMLESS_CALCULATING)
                    {
                        pstCfdStatus->bHDMIPacketChange = FALSE;
                    }
                    else if(_eHDMISeamlessStatus == SEAMLESS_UNFREEZE)
                    {
                        pstCfdStatus->bSeamlessCFDDone = TRUE;
                    }
                    else
                    {
                        pstCfdStatus->bHDMIPacketChange = FALSE;
                    }

                    bHDMIFreezeGOPDone = pstCfdStatus->bGOPDone;

                    // reset flag
                    if(pstCfdStatus->bResetHDMIFreezeFlag)
                    {
                        _eHDMISeamlessStatus = SEAMLESS_NONE;
                        bHDMIFreezeGOPDone = FALSE;
                    }
                    pstCfdStatus->bHDMIPacketChangeDuringMute = bHDMIChangeDuringMute;
                }
                else
                {
                    //if source change clear this flag
                    bHDMIChangeDuringMute = FALSE;
                    pstCfdStatus->bHDMIPacketChangeDuringMute = FALSE;
                }

                if(pstCfdStatus->bHDMIPacketChangeDuringMuteRest)
                {
                    bHDMIChangeDuringMute = FALSE;
                }

                if(CFD_IS_MM(_stCfdInit[pstCfdStatus->u8Win].u8InputSource)|| CFD_IS_DTV(_stCfdInit[pstCfdStatus->u8Win].u8InputSource))
                {
                    if(_eSeamlessStatus == SEAMLESS_DATACHANGED)
                    {
                        pstCfdStatus->bHDRDataChange = TRUE;
                        pstCfdStatus->bSeamlessCFDDone_MVOP = FALSE;
                    }
                    else if(_eSeamlessStatus == SEAMLESS_CALCULATING)
                    {
                        pstCfdStatus->bHDRDataChange = FALSE;
                    }
                    else if(_eSeamlessStatus == SEAMLESS_UNFREEZE)
                    {
                        pstCfdStatus->bSeamlessCFDDone_MVOP = TRUE;
                    }
                    else
                    {
                        pstCfdStatus->bHDRDataChange = FALSE;
                    }
                    //DTV/MM trnasfer char
                    //pstCfdStatus->bHDRDataChange = _bHDRDataChange;
                    //Seamless CFD Done
                    //pstCfdStatus->bSeamlessCFDDone_MVOP = _bGoToUnfreezeGOP;

                    _bMVOPFreezeGOPDone = pstCfdStatus->bGOPDone;

                    // reset flag
                    if(pstCfdStatus->bResetHDMIFreezeFlag)
                    {
                        _eSeamlessStatus = SEAMLESS_NONE;
                        _bMVOPFreezeGOPDone = FALSE;
                    }
                 }
            }
            else
            {
                if(CFD_IS_HDMI(_stCfdInit[pstCfdStatus->u8Win].u8InputSource))
                {
                    pstCfdStatus->bHDMIPacketChange = FALSE;
                    pstCfdStatus->bSeamlessCFDDone = FALSE;
                }
                if(CFD_IS_MM(_stCfdInit[pstCfdStatus->u8Win].u8InputSource)|| CFD_IS_DTV(_stCfdInit[pstCfdStatus->u8Win].u8InputSource))
                {
                    pstCfdStatus->bHDRDataChange = FALSE;
                    pstCfdStatus->bSeamlessCFDDone_MVOP = FALSE;
                }
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            //for Mdebug
            if (pstCfdStatus->u32Version >= 4)
            {
                pstCfdStatus->bCRCStatus = (MHal_XC_IsCRCPass(0) || MHal_XC_IsCRCPass(1) || MHal_XC_IsCRCPass(2));
                pstCfdStatus->u8VdecVersion = u8MMHdrVersion;
                pstCfdStatus->u16SetDSAverageTime= u16SetDSAverTime;
                pstCfdStatus->u16SetDSPeakTime= u16SetDSPeakTime;
                pstCfdStatus->u64SetDSPeakFrame= u64FrameNumber;
            }
            break;
        }

        case E_KDRV_XC_CFD_CTRL_SET_TMO:
        {
            MS_U8 *pu8Data = NULL;
            MS_U8 i=0;
            MS_U16 u16CpLength = sizeof(ST_KDRV_XC_CFD_TMO);
            MS_U16 *pu16Length = (MS_U16*)(pstKdrvCFDCtrlInfo->pParam + sizeof(MS_U32));
            ST_KDRV_XC_CFD_TMO *pstKdrvCFDTmo = (ST_KDRV_XC_CFD_TMO*)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_TMO_LEVEL *pstKdrvTmoLevel = NULL;

            if(NULL != _stCfdTmo.pstCfdTmoLevel)
            {
                for(i=0; i<_stCfdTmo.u16LevelCount; i++)
                {
                    if(NULL != (_stCfdTmo.pstCfdTmoLevel + i)->pu8data)
                    {
                        vfree((_stCfdTmo.pstCfdTmoLevel + i)->pu8data);
                    }
                }
                vfree(_stCfdTmo.pstCfdTmoLevel);
            }

            if ((*pu16Length) < u16CpLength)
            {
                u16CpLength = (*pu16Length) ;
            }
            memset(&_stCfdTmo, 0, sizeof(ST_KDRV_XC_CFD_TMO));
            memcpy(&_stCfdTmo, pstKdrvCFDTmo, u16CpLength);

            if((0 == pstKdrvCFDTmo->u16LevelCount) || (NULL == pstKdrvCFDTmo->pstCfdTmoLevel))
            {
                printk("[%s %d]ERROR input TMO parameter\n",__FUNCTION__,__LINE__);
                return FALSE;
            }
            pu8Data = vmalloc(pstKdrvCFDTmo->u16LevelCount * sizeof(ST_KDRV_XC_CFD_TMO_LEVEL));
            if(NULL == pu8Data)
            {
                printk("[%s %d]Error malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8Data, 0, pstKdrvCFDTmo->u16LevelCount * sizeof(ST_KDRV_XC_CFD_TMO_LEVEL));

            //copy_from_user(pu8Data, (MS_U8 __user *)pstKdrvCFDTmo->pstCfdTmoLevel, pstKdrvCFDTmo->u16LevelCount * sizeof(ST_KDRV_XC_CFD_TMO_LEVEL));
            memcpy(pu8Data, pstKdrvCFDTmo->pstCfdTmoLevel, pstKdrvCFDTmo->u16LevelCount * sizeof(ST_KDRV_XC_CFD_TMO_LEVEL));
            _stCfdTmo.pstCfdTmoLevel = pu8Data;

            for(i=0; i<_stCfdTmo.u16LevelCount; i++)
            {
                if(0 == (_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize)
                {
                    printk("[%s %d]ERROR Level %d controlsize is error\n", __FUNCTION__,__LINE__, i);
                    continue;
                }
                pu8Data = vmalloc((_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize);
                //copy_from_user(pu8Data, (MS_U8 __user *)(_stCfdTmo.pstCfdTmoLevel + i)->pu8data, (_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize);
                memcpy(pu8Data, (MS_U8 __user *)(_stCfdTmo.pstCfdTmoLevel + i)->pu8data, (_stCfdTmo.pstCfdTmoLevel + i)->u16ControlSize);
                (_stCfdTmo.pstCfdTmoLevel + i)->pu8data = pu8Data;
            }
        }
        break;

        case E_KDRV_XC_CFD_CTRL_SET_FIRE:
        {
            MS_BOOL bFireChange = FALSE;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire = (ST_KDRV_XC_CFD_FIRE*)pstKdrvCFDCtrlInfo->pParam;
            XC_KDBG("\033[31mFire info \033[m\n");
            XC_KDBG("\033[31m  Window is %d \033[m\n", pstCfdFire->u8Win);
            XC_KDBG("\033[31m  Input source is %d \033[m\n", pstCfdFire->u8InputSource);
            XC_KDBG("\033[31m  Is RGB bypass is %d \033[m\n", pstCfdFire->bIsRgbBypass);
            XC_KDBG("\033[31m  Is HD mode is %d \033[m\n", pstCfdFire->bIsHdMode);
            XC_KDBG("\033[31m  Update type is %d \033[m\n", pstCfdFire->u8UpdateType);

            if ((pstCfdFire->u8UpdateType == 1) && (bInitStatus[pstCfdFire->u8Win] == TRUE))
            {
                // In init status, skip OSD update action
                pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
                XC_KDBG("\033[31m  In init status, skip OSD update action \033[m\n");
                break;
            }

            if ((pstCfdFire->u8UpdateType == 1) &&
                (bHueChange[pstCfdFire->u8Win] == FALSE) &&
                (bContrastChange[pstCfdFire->u8Win] == FALSE) &&
                (bSaturationChange[pstCfdFire->u8Win] == FALSE))
            {
                // OSD doesn't change
                pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
                XC_KDBG("\033[31m Hue / Contrast / Saturation doesn't change, skip CFD fire \033[m\n");
                break;
            }

            if (pstCfdFire->u8UpdateType == 1)
            {
                pstCfdFire = &_stCfdFire[pstCfdFire->u8Win];
                pstCfdFire->u8UpdateType = 1;
                _stCfdOsd[pstCfdFire->u8Win].u8OSDUIEn = 1;
                _stCfdOsd[pstCfdFire->u8Win].u8OSDUIMode = 1;
            }
            else
            {
                if (_stCfdFire[pstCfdFire->u8Win].u8InputSource != pstCfdFire->u8InputSource)
                {
                    bFireChange |= TRUE;
                }

                if (_stCfdFire[pstCfdFire->u8Win].bIsRgbBypass!= pstCfdFire->bIsRgbBypass)
                {
                    bFireChange |= TRUE;
                }

                if (_stCfdFire[pstCfdFire->u8Win].bIsHdMode!= pstCfdFire->bIsHdMode)
                {
                    bFireChange |= TRUE;
                }

                memcpy(&_stCfdFire[pstCfdFire->u8Win], pstCfdFire, sizeof(ST_KDRV_XC_CFD_FIRE));
                _stCfdOsd[pstCfdFire->u8Win].u8OSDUIEn = 1;
                _stCfdOsd[pstCfdFire->u8Win].u8OSDUIMode = 0;
            }

            // Init IP
            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Main Control
            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // MM/DTV
            if (CFD_IS_MM(pstCfdFire->u8InputSource) || CFD_IS_DTV(pstCfdFire->u8InputSource))
            {
                pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMmParam(pstCfdFire, &bMmChange[pstCfdFire->u8Win]);
                if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                {
                    //return FALSE;
                }
            }

            // HDMI
            if (CFD_IS_HDMI(pstCfdFire->u8InputSource))
            {
                pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetHdmiParam(pstCfdFire);
                if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                {
                    //return FALSE;
                }
            }

            // HDR
            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetHdrParam(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                //return FALSE;
            }

            // OSD
            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetOsdParam(&_stCfdOsd[pstCfdFire->u8Win]);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }
            Mapi_Cfd_Maserati_DLCIP_CurveMode_Set(pstCfdFire->u8Win, _stCfdDlc.bUseCustomerDlcCurve);

            if (bHueChange[pstCfdFire->u8Win] || bContrastChange[pstCfdFire->u8Win] || bSaturationChange[pstCfdFire->u8Win] || bColorRangeChange[pstCfdFire->u8Win] ||
                bLinearRgbChange[pstCfdFire->u8Win] || bHdrChange[pstCfdFire->u8Win] || bHdmiChange[pstCfdFire->u8Win] || bMmChange[pstCfdFire->u8Win] ||
                bAnalogChange[pstCfdFire->u8Win] || bSkipPictureSettingChange[pstCfdFire->u8Win] || bColorCorrectionMatrixChange[pstCfdFire->u8Win] ||
                bUiH2SModeChange[pstCfdFire->u8Win] ||
                bFireChange)
            {
                // Fire
                // THIS IS A PATCH
                if (MHal_XC_GetDolbyStatus() == 0)
                {
                    if( (MHal_XC_Get_SharedMemVersion(0) == getOpenHDRSeamlessVersion()) &&
                        (CFD_IS_DTV(pstCfdFire->u8InputSource) || CFD_IS_MM(pstCfdFire->u8InputSource)) )
                    {
                        if(pstCfdFire->u8Win == 1)
                        {


                            int timer = 0;
                            while (bDTVUpdateCFDPara)
                            {
                                timer++;
                                if (timer > 20)
                    {
                                    printk("!!!DTV Seamless main/sub race condition!\n");
                                    break;
                                }
                                msleep(5);
                            }
                            setCFDSeamlessWin(1);
                            Color_Format_Driver();
                            setCFDSeamlessWin(0);
                        }
                    }
                    else
                    {
                        if((!_bXCFreezeDone) && (!_bMVOPFreezeGOPDone) && (E_SEAMLESS_MVOPSOURCE_WITH_FREEZE == MHal_XC_Get_HDRSeamless_MVOPSOURCE_Path()))
                        {
                            return TRUE;
                        }
                        Color_Format_Driver();

                        if (((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                             (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3) || (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR4)) &&
                            (pstCfdFire->u8UpdateType == 0))
                        {
                            MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                        }
                        KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                        MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                        if (((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) || (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR4) ||
                             (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3) &&
                             (pstCfdFire->u8UpdateType == 0)) ||
                            (CFD_IS_HDMI(u8HDMI_case) && (pstCfdFire->u8UpdateType == 0)))
                        {
                            if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                            {
                                printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                            }
                        }
                        MHal_XC_CFD_SetTmoVal();
                        bTmoFireEnable = TRUE;
                        _enHDRType = E_KDRV_XC_HDR_NONE;
                    }
                }
                else
                {
                    //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                    MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                    _MHal_XC_DolbyPatch();
                    bTmoFireEnable = FALSE;
                    // for controlling 3x3 para by UI
                    if(pstCfdFire->u8UpdateType == 1)
                    {
                        if(bHueChange[pstCfdFire->u8Win] || bContrastChange[pstCfdFire->u8Win] || bSaturationChange[pstCfdFire->u8Win])
                        {
                            // call Main Control again in order to let _stu_CFD_Main_Control.u8Input_HDRMode == E_CFIO_MODE_HDR1
                            // in this setting CFD will just control 3x3 para.
                            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
                            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
                            {
                                printk("[%s %d] dolby Hue/Contrast/Saturation update main ctrl fail\n",__FUNCTION__,__LINE__);
                                return FALSE;
                            }
                            Color_Format_Driver();
                            KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                        }
                    }
                }

                //waiting adl done.
                if (_bCfdInited && ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) || (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR4)))
                {
                    // waiting adl firing done, timeout 100ms
                    int timer = 0;
                    while ((MHal_XC_R2BYTE(REG_SC_BK67_28_L) & 0x0001))
                    {
                        timer++;
                        if (timer > 10)
                        {
                            break;
                        }
                        msleep(10);
                    }

                    if (timer >= 10)
                    {
                        printk("[%s %d]waiting adl firing done time out\n", __FUNCTION__,__LINE__);
                    }
                    _bCfdInited = FALSE;
                }
            }
#if OTT_AUTO_PAUSE_DETECT
            MHal_XC_ClearPauseStatus();
#endif

#if HDMI_AUTO_NOSIGNAL_DETECT
            MHal_XC_ClearNoSignalStatus();
#endif
            bCFDInitFinished[pstCfdFire->u8Win] = TRUE;
            bInitStatus[pstCfdFire->u8Win] = FALSE;
            bHueChange[pstCfdFire->u8Win] = FALSE;
            bContrastChange[pstCfdFire->u8Win] = FALSE;
            bSaturationChange[pstCfdFire->u8Win] = FALSE;
            bColorRangeChange[pstCfdFire->u8Win] = FALSE;
            bLinearRgbChange[pstCfdFire->u8Win] = FALSE;
            bHdrChange[pstCfdFire->u8Win] = FALSE;
            bHdmiChange[pstCfdFire->u8Win] = FALSE;
            bMmChange[pstCfdFire->u8Win] = FALSE;
            bAnalogChange[pstCfdFire->u8Win] = FALSE;
            bSkipPictureSettingChange[pstCfdFire->u8Win] = FALSE;
            bColorCorrectionMatrixChange[pstCfdFire->u8Win] = FALSE;
            bUiH2SModeChange[pstCfdFire->u8Win] = FALSE;
            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }
        case E_KDRV_XC_CFD_CTRL_UPDATE_STATUS:
        {
            static MS_BOOL _bHasBooted = FALSE;
            ST_KDRV_XC_CFD_UPDATE_STATUS *pstXcCfdUpdateStatus = (ST_KDRV_XC_CFD_UPDATE_STATUS*)pstKdrvCFDCtrlInfo->pParam;
            switch(pstXcCfdUpdateStatus->enCFD_status)
            {
                case E_KDRV_XC_CFD_STATUS_SYNC_GEN_BLACK_SCREEN_ENABLE:
                {
                    _bGenBlackScreen = TRUE;
                    u8count = 0;
                    if(_bHasBooted)
                    {
                        _bPreHDR = TRUE;
                    }
                    break;
                }
                case E_KDRV_XC_CFD_STATUS_SYNC_GEN_BLACK_SCREEN_DISABLE:
                {
                    _bGenBlackScreen = FALSE;
                    if(!_bHasBooted)
                    {
                        _bHasBooted = TRUE;
                        if(MHal_XC_GetDolbyStatus() & BIT(1))
                        {
                            _bPreHDR = TRUE;
                        }
                    }
                    else
                    {
                        if(!(MHal_XC_GetDolbyStatus() & BIT(1)))
                        {
                            _bPreHDR = FALSE;
                        }
                    }
                    break;
                }
            }
            break;
        }
        case E_KDRV_XC_CFD_CTRL_PICTURE_ATTRIBUTE:
        {
            ST_KDRV_XC_CFD_PICTURE_ATTRIBUTE* pstPictureAttr = (ST_KDRV_XC_CFD_PICTURE_ATTRIBUTE*)pstKdrvCFDCtrlInfo->pParam;
            switch(pstPictureAttr->u8ControlType)
            {
                case E_KDRV_XC_CFD_PICTURE_ATTR_GET_HUE:
                {
                    pstPictureAttr->u16AttrValue = MHal_XC_GetHue();
                    //printk("\033[31m  CFD Picture attribute get Hue %d\033[m\n", pstPictureAttr->u16AttrValue);
                    pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
                    break;
                }
                case E_KDRV_XC_CFD_PICTURE_ATTR_GET_SAT:
                {
                    pstPictureAttr->u16AttrValue = MHal_XC_GetSat();
                    //printk("\033[31m  CFD Picture attribute get Sat %d\033[m\n", pstPictureAttr->u16AttrValue);
                    pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
                    break;
                }
                default:
                {
                    //printk("\033[31m  CFD Picture attribute unknow control type\033[m\n");
                    break;
                }
            }

            break;
        }
        case E_XC_PQ_SET_QMAPDATA:
        {
            if (pstTMO_Qmap != NULL)
            {
                vfree(pstTMO_Qmap->pu16Quality_Map_Aray);
                vfree(pstTMO_Qmap->pu8table_data_Aray);
                vfree(pstTMO_Qmap->pu16table_Map_Aray);
                vfree(pstTMO_Qmap);
            }
            if (pstTMO_Qmap == NULL)
            {
                ST_KDRV_XC_PQ_QMAPDATA* pstuserqmapdata = (ST_KDRV_XC_PQ_QMAPDATA*)pstKdrvCFDCtrlInfo->pParam;
                pstTMO_Qmap = kmalloc(sizeof(ST_KDRV_XC_PQ_QMAPDATA), GFP_KERNEL);
                pstTMO_Qmap->pu16Quality_Map_Aray = kmalloc((pstuserqmapdata->u32Quality_InputType_Num * pstuserqmapdata->u32Quality_IP_Num * sizeof(MS_U16)), GFP_KERNEL);
                pstTMO_Qmap->pu8table_data_Aray = kmalloc((pstuserqmapdata->u32table_data_size * sizeof(MS_U8)), GFP_KERNEL);
                pstTMO_Qmap->pu16table_Map_Aray = kmalloc((pstuserqmapdata->u32Quality_IP_Num * pstuserqmapdata->u32table_IP_Num * sizeof(MS_U16)), GFP_KERNEL);

                pstTMO_Qmap->u32Version = pstuserqmapdata->u32Version;
                pstTMO_Qmap->u16Length = pstuserqmapdata->u16Length;
                pstTMO_Qmap->u32qmapid = pstuserqmapdata->u32qmapid;
                pstTMO_Qmap->u32Quality_InputType_Num = pstuserqmapdata->u32Quality_InputType_Num;
                pstTMO_Qmap->u32Quality_IP_Num = pstuserqmapdata->u32Quality_IP_Num;
                pstTMO_Qmap->u32table_data_size = pstuserqmapdata->u32table_data_size;
                pstTMO_Qmap->u32table_IP_Num = pstuserqmapdata->u32table_IP_Num;
                memcpy(pstTMO_Qmap->pu16Quality_Map_Aray, pstuserqmapdata->pu16Quality_Map_Aray, (pstuserqmapdata->u32Quality_InputType_Num * pstuserqmapdata->u32Quality_IP_Num * sizeof(MS_U16)));
                memcpy(pstTMO_Qmap->pu8table_data_Aray, pstuserqmapdata->pu8table_data_Aray, (pstuserqmapdata->u32table_data_size * sizeof(MS_U8)));
                memcpy(pstTMO_Qmap->pu16table_Map_Aray, pstuserqmapdata->pu16table_Map_Aray, (pstuserqmapdata->u32Quality_IP_Num * pstuserqmapdata->u32table_IP_Num * sizeof(MS_U16)));
            }
            break;
        }
        case E_KDRV_XC_CFD_CTRL_GET_HDMI_STATUS:
        {
            ST_KDRV_XC_CFD_HDMI *pstXcCfdHdmi = (ST_KDRV_XC_CFD_HDMI*)pstKdrvCFDCtrlInfo->pParam;
            memcpy(&_stCfdHdmi_Out[pstXcCfdHdmi->u8Win], pstXcCfdHdmi, sizeof(ST_KDRV_XC_CFD_HDMI));
            MHal_XC_GetHdmiOutParam(pstXcCfdHdmi, pstXcCfdHdmi->u8Win);
            memcpy(pstXcCfdHdmi, &_stCfdHdmi_Out[pstXcCfdHdmi->u8Win], sizeof(ST_KDRV_XC_CFD_HDMI));
            XC_KDBG("\033[31mHDMI out status \033[m\n");
            XC_KDBG("\033[31m  Window is %d \033[m\n", pstXcCfdHdmi->u8Win);
            XC_KDBG("\033[31m  HDMI %s full range\033[m\n", pstXcCfdHdmi->bIsFullRange?"is":"isn't");
            XC_KDBG("\033[31m  AVI infoframe is (%d, %d, %d, %d, %d) \033[m\n", pstXcCfdHdmi->u8PixelFormat, pstXcCfdHdmi->u8Colorimetry, pstXcCfdHdmi->u8ExtendedColorimetry, pstXcCfdHdmi->u8RgbQuantizationRange, pstXcCfdHdmi->u8YccQuantizationRange);
            XC_KDBG("\033[31m  HDR infoframe %s exists\033[m\n", pstXcCfdHdmi->bHDRInfoFrameValid?"is":"isn't");
            XC_KDBG("\033[31m  HDR EOTF %d\033[m\n", pstXcCfdHdmi->u8EOTF);
            pstKdrvCFDCtrlInfo->u16ErrCode = E_CFD_MC_ERR_NOERR;
            break;
        }
        case E_KDRV_XC_CFD_CTRL_SET_HDR_ONOFF_SETTING:
        {
            MS_U32* pValue = (MS_U32*)pstKdrvCFDCtrlInfo->pParam;
            MS_U8 u8SetValue = 0;
            MS_U8 u8GetValue = 0;

            //0:auto depends on STB rule
            //1:always do HDR2SDR for HDR input
            //2:always not do HDR2SDR for HDR input
            _stCfdOsd[0].u8HDR_UI_H2SMode = (*pValue)&0xFF;

            u8SetValue = _stCfdOsd[0].u8HDR_UI_H2SMode;

            if (_stCfdOsd[0].u8HDR_UI_H2SMode == 0)
            {
                if (bEdidSupportPQ && bEdidSupportBT2020RGB)
                {
                    u8SetValue = 2; //for tv, force bypass
                }
            }

            Mapi_Cfd_OSD_H2SUI_Get(&u8GetValue);
            if(u8GetValue != u8SetValue)
            {
                bUiH2SModeChange[0] = TRUE;
            }

            Mapi_Cfd_OSD_H2SUI_Set(u8SetValue);
            XC_KDBG("\033[31m  HDR AUTO/ON/OFF UI Setting is %d, CFD Setting is %d \033[m\n", _stCfdOsd[0].u8HDR_UI_H2SMode, u8SetValue);
            break;
        }
        case E_KDRV_XC_CFD_CTRL_3DLUT:
        {
            static MS_U8 *pu83DLutData = NULL;
            ST_KDRV_XC_CFD_3DLUT *pstXcCfd3DLUT = (ST_KDRV_XC_CFD_3DLUT*)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            if(pu83DLutData != NULL)
            {
                vfree(pu83DLutData);
                pu83DLutData = NULL;
            }

            pu83DLutData = vmalloc(pstXcCfd3DLUT->u32Size);
            if(NULL == pu83DLutData)
            {
                printk("[%s %d]Error malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu83DLutData, 0, pstXcCfd3DLUT->u32Size);
            copy_from_user(pu83DLutData, (MS_U8 __user *)pstXcCfd3DLUT->pLUT, pstXcCfd3DLUT->u32Size);

            Mapi_Cfd_Control_Mode_Select(E_MODULE_3DLUT);
            //HAL_VPQ_SetHDR3DLUT(pu83DLutData);

            pstCfdFire = &_stCfdFire[pstXcCfd3DLUT->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
#if 1
                HAL_VPQ_SetHDR3DLUT(pu83DLutData);
                if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_START)
                {
                    SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_3DLUT][SKIP_FRIE]\n", __LINE__, __FUNCTION__);
                }
                else if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_END)
                {
                    SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_3DLUT][MLOAD_SET_FALSE]\n", __LINE__, __FUNCTION__);
                    KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, FALSE);
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                else
                {
                    KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
#else
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
#endif
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }
            break;
        }
        case E_KDRV_XC_CFD_CTRL_EOTF:
        {
            static MS_U8 *pu8Data = NULL;
            ST_KDRV_XC_CFD_EOTF *pstXcCfdEOTF = (ST_KDRV_XC_CFD_EOTF*)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            if(pu8Data != NULL)
            {
                vfree(pu8Data);
                pu8Data = NULL;
            }

            pu8Data = vmalloc(pstXcCfdEOTF->u32Size);
            if(NULL == pu8Data)
            {
                printk("[%s %d]Error malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8Data, 0, pstXcCfdEOTF->u32Size);
            copy_from_user(pu8Data, (MS_U8 __user *)pstXcCfdEOTF->pu8Data, pstXcCfdEOTF->u32Size);

            Mapi_Cfd_Control_Mode_Select(E_MODULE_EOTF);
            //HAL_VPQ_SetHDREotf(pu8Data);

            pstCfdFire = &_stCfdFire[pstXcCfdEOTF->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
#if 1
                HAL_VPQ_SetHDREotf(pu8Data);
                if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_START)
                {
                    SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_EOTF][SKIP_FRIE]\n", __LINE__, __FUNCTION__);
                }
                else
                {
                    KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
#else
                HAL_VPQ_SetHDREotf(pu8Data);
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
#endif
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }
            break;

            break;
        }
        case E_KDRV_XC_CFD_CTRL_OETF:
        {
            static MS_U8 *pu8Data = NULL;
            ST_KDRV_XC_CFD_OETF *pstXcCfdOETF = (ST_KDRV_XC_CFD_OETF*)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            if(pu8Data != NULL)
            {
                vfree(pu8Data);
                pu8Data = NULL;
            }

            pu8Data = vmalloc(pstXcCfdOETF->u32Size);
            if(NULL == pu8Data)
            {
                printk("[%s %d]Error malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8Data, 0, pstXcCfdOETF->u32Size);
            copy_from_user(pu8Data, (MS_U8 __user *)pstXcCfdOETF->pu8Data, pstXcCfdOETF->u32Size);

            Mapi_Cfd_Control_Mode_Select(E_MODULE_OETF);
            //HAL_VPQ_SetHDROetf(pu8Data);

            pstCfdFire = &_stCfdFire[pstXcCfdOETF->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
#if 1
                HAL_VPQ_SetHDROetf(pu8Data);
                if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_START)
                {
                    SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_OETF][SKIP_FRIE]\n", __LINE__, __FUNCTION__);
                }
                else
                {
                    KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
#else
                HAL_VPQ_SetHDROetf(pu8Data);
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
#endif
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }
            break;

            break;
        }

        case E_KDRV_XC_CFD_CTRL_HLGGAIN:
        {
            static MS_U8 *pu8Data = NULL;
            ST_KDRV_XC_CFD_HLGYGAIN *pstXcCfdHLGGain = (ST_KDRV_XC_CFD_HLGYGAIN*)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            if(pu8Data != NULL)
            {
                vfree(pu8Data);
                pu8Data = NULL;
            }

            pu8Data = vmalloc(pstXcCfdHLGGain->u32Size);
            if(NULL == pu8Data)
            {
                printk("[%s %d]Error malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8Data, 0, pstXcCfdHLGGain->u32Size);
            copy_from_user(pu8Data, (MS_U8 __user *)pstXcCfdHLGGain->pu8Data, pstXcCfdHLGGain->u32Size);

            Mapi_Cfd_Control_Mode_Select(E_MODULE_OOTF);
            //HAL_VPQ_SetHDROotf(pu8Data);

            pstCfdFire = &_stCfdFire[pstXcCfdHLGGain->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
#if 1
                HAL_VPQ_SetHDROotf(pu8Data);
                if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_START)
                {
                    SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_HLGGAIN][SKIP_FRIE]\n", __LINE__, __FUNCTION__);
                }
                else
                {
                    KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
#else
                HAL_VPQ_SetHDROotf(pu8Data);
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
#endif
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }
            break;
        }
        case E_KDRV_XC_CFD_CTRL_GAMMA_ENABLE:
        {
            ST_KDRV_XC_CFD_INVGAMMAEN *pstXcCfdGammanEn = (ST_KDRV_XC_CFD_INVGAMMAEN *)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            Mapi_Cfd_Control_GammaEnable(pstXcCfdGammanEn->bOnOff);

            pstCfdFire = &_stCfdFire[pstXcCfdGammanEn->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_START)
                {
                    SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_GAMMA_ENABLE][SKIP_FRIE]\n", __LINE__, __FUNCTION__);
                }
                else
                {
                    KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                    MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                }
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }

            break;
        }
        case E_KDRV_XC_CFD_CTRL_GAMUT_PREVIOUS:
        {
            ST_KDRV_XC_CFD_GAMUTLUT_PREVIOUS *pstXcCfdGamutLutPrevious = (ST_KDRV_XC_CFD_GAMUTLUT_PREVIOUS *)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            HAL_VPQ_SetGamutMatrixPre(pstXcCfdGamutLutPrevious->as32GamutLutData);
            Mapi_Cfd_Control_Mode_Select(E_MODULE_GAMUT_PREVIOUS);

            pstCfdFire = &_stCfdFire[pstXcCfdGamutLutPrevious->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_START)
                {
                    SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_GAMUT_PREVIOUS][SKIP_FRIE]\n", __LINE__, __FUNCTION__);
                }
                else
                {
                    KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                    MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                }
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }
            break;
        }

        case E_KDRV_XC_CFD_CTRL_GAMUT_POST:
        {
            ST_KDRV_XC_CFD_GAMUTLUT_POST *pstXcCfdGamutLutPost = (ST_KDRV_XC_CFD_GAMUTLUT_POST *)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            HAL_VPQ_SetGamutMatrixPost(pstXcCfdGamutLutPost->as32GamutLutData);
            Mapi_Cfd_Control_Mode_Select(E_MODULE_GAMUT_POST);
            Mapi_Cfd_Control_SDRGammaEnable(pstXcCfdGamutLutPost->bSDRGammaEnable);
            Mapi_Cfd_Control_SDRDegammaEnable(pstXcCfdGamutLutPost->bSDRDegammaEnable);
            Mapi_Cfd_Control_SDR3DLUTEnable(pstXcCfdGamutLutPost->bSDR3DLUTEnable);
            pstCfdFire = &_stCfdFire[pstXcCfdGamutLutPost->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                if(MHal_XC_GetPicturemodeVPQType() == E_KDRV_XC_PICTURE_MODE_VPQ_START)
                {
                    SkipFireADLMLDBG("[%d][%s][E_KDRV_XC_CFD_CTRL_GAMUT_POST][SKIP_FRIE]\n", __LINE__, __FUNCTION__);
                }
                else
                {
                    KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                    MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                }
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }
            break;
        }
        case E_KDRV_XC_CFD_CTRL_SDR_GAMMA:
        {
            static MS_U8 *pu8GammaRData = NULL;
            static MS_U8 *pu8GammaGData = NULL;
            static MS_U8 *pu8GammaBData = NULL;
            ST_KDRV_XC_CFD_SDRGAMMA *pstSDRGamma = (ST_KDRV_XC_CFD_SDRGAMMA *)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            if(pu8GammaRData != NULL)
            {
                vfree(pu8GammaRData);
                pu8GammaRData = NULL;
            }
            if(pu8GammaGData != NULL)
            {
                vfree(pu8GammaGData);
                pu8GammaGData = NULL;
            }
            if(pu8GammaBData != NULL)
            {
                vfree(pu8GammaBData);
                pu8GammaBData = NULL;
            }

            pu8GammaRData = vmalloc(pstSDRGamma->u32GammaRSize * sizeof(MS_U32));
            if(NULL == pu8GammaRData)
            {
                printk("[%s %d]Errorsda malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8GammaRData, 0, pstSDRGamma->u32GammaRSize * sizeof(MS_U32));
            copy_from_user(pu8GammaRData, (MS_U8 __user *)pstSDRGamma->pu8GammaR, pstSDRGamma->u32GammaRSize * sizeof(MS_U32));

            if(pu8GammaGData != NULL)
            {
                vfree(pu8GammaGData);
                pu8GammaGData = NULL;
            }

            pu8GammaGData = vmalloc(pstSDRGamma->u32GammaGSize * sizeof(MS_U32));
            if(NULL == pu8GammaGData)
            {
                printk("[%s %d]Errorsda malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8GammaGData, 0, pstSDRGamma->u32GammaGSize * sizeof(MS_U32));
            copy_from_user(pu8GammaGData, (MS_U8 __user *)pstSDRGamma->pu8GammaG, pstSDRGamma->u32GammaGSize * sizeof(MS_U32));

            if(pu8GammaBData != NULL)
            {
                vfree(pu8GammaBData);
                pu8GammaBData = NULL;
            }

            pu8GammaBData = vmalloc(pstSDRGamma->u32GammaBSize * sizeof(MS_U32));
            if(NULL == pu8GammaBData)
            {
                printk("[%s %d]Errorsda malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8GammaBData, 0, pstSDRGamma->u32GammaBSize * sizeof(MS_U32));
            copy_from_user(pu8GammaBData, (MS_U8 __user *)pstSDRGamma->pu8GammaB, pstSDRGamma->u32GammaBSize * sizeof(MS_U32));

            Mapi_Cfd_Control_Mode_Select(E_MODULE_SDR_GAMMA);
            HAL_VPQ_SetGammaLUT((MS_U32 *)pu8GammaRData, (MS_U32 *)pu8GammaGData, (MS_U32 *)pu8GammaBData);

            pstCfdFire = &_stCfdFire[pstSDRGamma->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }
            break;
        }
        case E_KDRV_XC_CFD_CTRL_SDR_DEGAMMA:
        {
            static MS_U8 *pu8DegammaRData = NULL;
            static MS_U8 *pu8DegammaGData = NULL;
            static MS_U8 *pu8DegammaBData = NULL;
            ST_KDRV_XC_CFD_SDRDEGAMMA *pstSDRDegamma = (ST_KDRV_XC_CFD_SDRDEGAMMA *)pstKdrvCFDCtrlInfo->pParam;
            ST_KDRV_XC_CFD_FIRE *pstCfdFire;

            if(pu8DegammaRData != NULL)
            {
                vfree(pu8DegammaRData);
                pu8DegammaRData = NULL;
            }
            if(pu8DegammaGData != NULL)
            {
                vfree(pu8DegammaGData);
                pu8DegammaGData = NULL;
            }
            if(pu8DegammaBData != NULL)
            {
                vfree(pu8DegammaBData);
                pu8DegammaBData = NULL;
            }

            pu8DegammaRData = vmalloc(pstSDRDegamma->u32DegammaRSize * sizeof(MS_U32));
            if(NULL == pu8DegammaRData)
            {
                printk("[%s %d]Errorsda malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8DegammaRData, 0, pstSDRDegamma->u32DegammaRSize * sizeof(MS_U32));
            copy_from_user(pu8DegammaRData, (MS_U8 __user *)pstSDRDegamma->pu8DegammaR, pstSDRDegamma->u32DegammaRSize * sizeof(MS_U32));

            if(pu8DegammaGData != NULL)
            {
                vfree(pu8DegammaGData);
                pu8DegammaGData = NULL;
            }

            pu8DegammaGData = vmalloc(pstSDRDegamma->u32DegammaGSize * sizeof(MS_U32));
            if(NULL == pu8DegammaGData)
            {
                printk("[%s %d]Errorsda malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8DegammaGData, 0, pstSDRDegamma->u32DegammaGSize * sizeof(MS_U32));
            copy_from_user(pu8DegammaGData, (MS_U8 __user *)pstSDRDegamma->pu8DegammaG, pstSDRDegamma->u32DegammaGSize * sizeof(MS_U32));

            if(pu8DegammaBData != NULL)
            {
                vfree(pu8DegammaBData);
                pu8DegammaBData = NULL;
            }

            pu8DegammaBData = vmalloc(pstSDRDegamma->u32DegammaBSize * sizeof(MS_U32));
            if(NULL == pu8DegammaBData)
            {
                printk("[%s %d]Errorsda malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }
            memset(pu8DegammaBData, 0, pstSDRDegamma->u32DegammaBSize * sizeof(MS_U32));
            copy_from_user(pu8DegammaBData, (MS_U8 __user *)pstSDRDegamma->pu8DegammaB, pstSDRDegamma->u32DegammaBSize * sizeof(MS_U32));

            Mapi_Cfd_Control_Mode_Select(E_MODULE_SDR_DEGAMMA);
            int i = 0;

            HAL_VPQ_DeGammaLUT((MS_U32 *)pu8DegammaRData, (MS_U32 *)pu8DegammaGData, (MS_U32 *)pu8DegammaBData);

            pstCfdFire = &_stCfdFire[pstSDRDegamma->u8Win];

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_InitIp(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            pstKdrvCFDCtrlInfo->u16ErrCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (pstKdrvCFDCtrlInfo->u16ErrCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }

            // Fire
            // THIS IS A PATCH
            if (MHal_XC_GetDolbyStatus() == 0)
            {
                //Color_Format_Driver();
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    MHal_TMO_WriteTmoCurve(pstCfdFire->u8Win);
                }
                KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);
                MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
                if ((_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR2) ||
                    (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_HDR3))
                {
                    if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                    {
                        printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                    }
                }
                bTmoFireEnable = TRUE;
            }
            else
            {
                //MHal_XC_W2BYTEMSK(_PK_H_(0x7a,0x4a),0x80,0x00); //reg_b04_out_clamp_en
                MHal_XC_W2BYTEMSK(0x137A94,0,0x3FFF);//reg_b04_uv_offs_0
                bTmoFireEnable = FALSE;
            }
            break;
        }
        case E_KDRV_XC_CFD_CTRL_YCOFFSETGAININ:
        {
            ST_KDRV_XC_YC_OffsetGain *pstYCOffsetGainIn = (ST_KDRV_XC_YC_OffsetGain *)pstKdrvCFDCtrlInfo->pParam;

            HAL_VPQ_SetDlcYcOffsetGainIn(pstYCOffsetGainIn->u16YGain, pstYCOffsetGainIn->s16YOffset, pstYCOffsetGainIn->u16CGain, pstYCOffsetGainIn->s16COffset);
            break;
        }
        case E_KDRV_XC_CFD_CTRL_YCOFFSETGAINOUT:
        {
            ST_KDRV_XC_YC_OffsetGain *pstYCOffsetGainOut = (ST_KDRV_XC_YC_OffsetGain *)pstKdrvCFDCtrlInfo->pParam;

            HAL_VPQ_SetDlcYcOffsetGainOut(pstYCOffsetGainOut->u16YGain, pstYCOffsetGainOut->s16YOffset, pstYCOffsetGainOut->u16CGain, pstYCOffsetGainOut->s16COffset);
            break;
        }
        case E_KDRV_XC_CFD_CTRL_YCOFFSETGAININSUB:
        {
            ST_KDRV_XC_YC_OffsetGain *pstYCOffsetGainInSub = (ST_KDRV_XC_YC_OffsetGain *)pstKdrvCFDCtrlInfo->pParam;
            HAL_VPQ_SetDlcYcOffsetGainInSub(pstYCOffsetGainInSub->u16YGain, pstYCOffsetGainInSub->s16YOffset, pstYCOffsetGainInSub->u16CGain, pstYCOffsetGainInSub->s16COffset);
            break;
        }
        case E_KDRV_XC_CFD_CTRL_YCOFFSETGAINBOTH:
        {
            ST_KDRV_XC_YC_OffsetGainBoth *pstYCOffsetGainBoth = (ST_KDRV_XC_YC_OffsetGainBoth *)pstKdrvCFDCtrlInfo->pParam;

            HAL_VPQ_SetDlcYcOffsetGainBoth(pstYCOffsetGainBoth->u16YGainIn, pstYCOffsetGainBoth->s16YOffsetIn, pstYCOffsetGainBoth->u16CGainIn, pstYCOffsetGainBoth->s16COffsetIn
                ,pstYCOffsetGainBoth->u16YGainOut, pstYCOffsetGainBoth->s16YOffsetOut, pstYCOffsetGainBoth->u16CGainOut, pstYCOffsetGainBoth->s16COffsetOut);
            break;
        }
        case E_KDRV_XC_CFD_CTRL_SDR_3DLUT:
        {
            static MS_U16 *pu16GammutLut = NULL;
            ST_KDRV_XC_CFD_SDRGAMMUT *pstSDRGammut = (ST_KDRV_XC_CFD_SDRGAMMUT *)pstKdrvCFDCtrlInfo->pParam;

            if(pu16GammutLut != NULL)
            {
                vfree(pu16GammutLut);
                pu16GammutLut = NULL;
            }

            pu16GammutLut = (MS_U16*)vmalloc(pstSDRGammut->u32GammutSize * sizeof(MS_U16));
            if(NULL == pu16GammutLut)
            {
                printk("[%s %d]Errorsda malloc fail\n", __FUNCTION__,__LINE__);
                return FALSE;
            }

            memset(pu16GammutLut, 0, pstSDRGammut->u32GammutSize * sizeof(MS_U16));
            copy_from_user(pu16GammutLut, (MS_U8 __user *)pstSDRGammut->pu16SDRGammut, pstSDRGammut->u32GammutSize * sizeof(MS_U16));

            Mapi_Cfd_Control_Mode_Select(E_MODULE_SDR_3DLUT);
            HAL_VPQ_SetGamutLUT(pu16GammutLut);
            MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT);
            break;
        }
        case E_KDRV_XC_CFD_CTRL_PICTURE_MODE:
        {
            ST_KDRV_XC_CFD_SET_PICTURE_MODE_VPQ *pstPictureModeVPQ = (ST_KDRV_XC_CFD_SET_PICTURE_MODE_VPQ *)pstKdrvCFDCtrlInfo->pParam;
            MHal_XC_SetPicturemodeVPQType(pstPictureModeVPQ->en_PICTURE_MODE_VPQ);
            SkipFireADLMLDBG("[%d][%s][en_PICTURE_MODE_VPQ = %d]\n", __LINE__, __FUNCTION__, pstPictureModeVPQ->en_PICTURE_MODE_VPQ);
            //when AP set HAL_VPQ_MEMC_SetMotionPro, driver got VPQ_END status, then fire menuload and autodownload.
            if(pstPictureModeVPQ->en_PICTURE_MODE_VPQ == E_KDRV_XC_PICTURE_MODE_VPQ_END)
            {
                KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, FALSE);
                if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
                {
                    printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
                }
            }
            break;
        }
        default:
            break;
    }

    return TRUE;
}

MS_BOOL MHal_XC_CheckMuteStatusByRegister(MS_U8 u8Window)
{
    MS_BOOL bRet = FALSE;
    ///TODO: check need consider device 1
#if (1)//PIP_PATCH_USING_SC1_MAIN_AS_SC0_SUB)
    if (SUB_WINDOW == u8Window)
    {
        bRet = (MHal_XC_R2BYTEMSK(REG_SC_BK10_19_L, BIT(5)) ? TRUE: FALSE);
    }

#else
    if(SUB_WINDOW == eWindow)
    {
        bRet = (SC_R2BYTEMSK(psXCInstPri->u32DeviceID, REG_SC_BK10_19_L, BIT(5)) ? TRUE: FALSE);
    }
#endif
    else if(MAIN_WINDOW == u8Window)
    {
        bRet = (MHal_XC_R2BYTEMSK(REG_SC_BK10_19_L, BIT(1)) ? TRUE: FALSE);
    }
    return bRet;
}

MS_BOOL MHal_XC_UpdateTMOPara(MS_U8 u8Win,MS_U32 u32index)
{
    if(pstTMO_Qmap == NULL)
    {
        printk("Error,Do Not Have TMO Qmap!!!!!!\n");
        return FALSE;
    }

    if(u32index >= pstTMO_Qmap->u32Quality_InputType_Num)
    {
        return FALSE;
    }

    MS_U8 u8Mask,u8Addr,u8Value,u8Bank;
    MS_U32 u32offset=0, u32iptabcnt;
    MS_U16 u16GropIndex = 0, u16RegNum = 0, u16Regcnt, u16TbIndex;

    for(u32iptabcnt=0; u32iptabcnt<pstTMO_Qmap->u32Quality_IP_Num;u32iptabcnt++)
    {
        ///Get Table IP index
        u16TbIndex=pstTMO_Qmap->pu16Quality_Map_Aray[(u32index * pstTMO_Qmap->u32Quality_IP_Num +u32iptabcnt)];

        //Get Data
        u16GropIndex=pstTMO_Qmap->pu16table_Map_Aray[(u32iptabcnt * pstTMO_Qmap->u32table_IP_Num)];
        u16RegNum=pstTMO_Qmap->pu16table_Map_Aray[(u32iptabcnt * pstTMO_Qmap->u32table_IP_Num)+1];

        for(u16Regcnt=0;u16Regcnt< u16RegNum;u16Regcnt++)
        {
            u8Bank= pstTMO_Qmap->pu8table_data_Aray[(u32offset+(u16Regcnt*(u16GropIndex+PQ_BIN_REG_ALIGNMENT)))];
            u8Addr= pstTMO_Qmap->pu8table_data_Aray[(u32offset+(u16Regcnt*(u16GropIndex+PQ_BIN_REG_ALIGNMENT)))+PQ_BIN_ADDR_ALIGNMENT];
            u8Mask= pstTMO_Qmap->pu8table_data_Aray[(u32offset+(u16Regcnt*(u16GropIndex+PQ_BIN_REG_ALIGNMENT)))+PQ_BIN_MASK_ALIGNMENT];
            u8Value = pstTMO_Qmap->pu8table_data_Aray[(u32offset+(u16Regcnt*(u16GropIndex+PQ_BIN_REG_ALIGNMENT)))+PQ_BIN_REG_ALIGNMENT+u16TbIndex];
            MHal_XC_VR_WriteByte((0x100*u8Bank+u8Addr),u8Value);
        }
        u32offset+=((PQ_BIN_REG_ALIGNMENT+u16GropIndex)*u16RegNum);
    }
    return TRUE;
}

MS_BOOL MHal_XC_UpdateCFDPara(MS_U8 u8Win)
{
    MS_BOOL bRet = TRUE;
    bDTVUpdateCFDPara = TRUE;
    if(u8Win>=MAX_WINDOW_NUM)
    {
        printk("ERROR:window size overflow in CFD update para\n");
        return FALSE;
    }
    static MS_BOOL bMmChange[MAX_WINDOW_NUM];
    MS_U8 u8Temp = 0;
    for(u8Temp=0;u8Temp<MAX_WINDOW_NUM;u8Temp++)
    {
        bMmChange[u8Temp] = FALSE;
    }
    // get CFD fire SN para according Window
    ST_KDRV_XC_CFD_FIRE pstCfdFire;
    pstCfdFire.u8Win = _stCfdFire[u8Win].u8Win;
    if(pstCfdFire.u8Win != u8Win)
    {
        printk("ERROR:cfdFire win does not match\n");
        return FALSE;
    }
    #if 0
    pstCfdFire.u32Version = _stCfdFire[u8Win].u32Version;
    pstCfdFire.u16Length = _stCfdFire[u8Win].u16Length;
    pstCfdFire.u8InputSource = _stCfdFire[u8Win].u8InputSource;
    pstCfdFire.u8UpdateType = _stCfdFire[u8Win].u8UpdateType;
    pstCfdFire.bIsRgbBypass = _stCfdFire[u8Win].bIsRgbBypass;
    pstCfdFire.bIsHdMode = _stCfdFire[u8Win].bIsHdMode;
    #endif
    pstCfdFire.u32Version = KDRV_XC_CFD_FIRE_VERSION;
    pstCfdFire.u16Length = sizeof(ST_KDRV_XC_CFD_FIRE);
    pstCfdFire.u8InputSource = _stCfdInit[u8Win].u8InputSource;
    pstCfdFire.u8UpdateType = FALSE;
    pstCfdFire.bIsRgbBypass = FALSE;
    pstCfdFire.bIsHdMode = TRUE;
    // Init IP
    MS_U16 u16ErrCode = E_CFD_MC_ERR_NOERR;
    u16ErrCode = MHal_XC_CFD_InitIp(&pstCfdFire);
    if (u16ErrCode != E_CFD_MC_ERR_NOERR)
    {
        return FALSE;
    }

    // Main Control
    u16ErrCode = MHal_XC_CFD_SetMainCtrl(&pstCfdFire);
    if (u16ErrCode != E_CFD_MC_ERR_NOERR)
    {
        return FALSE;
    }

    // MM/DTV
    if (CFD_IS_MM(pstCfdFire.u8InputSource) || CFD_IS_DTV(pstCfdFire.u8InputSource))
    {
        u16ErrCode = MHal_XC_CFD_SetMmParam(&pstCfdFire, &bMmChange[pstCfdFire.u8Win]);
        if (u16ErrCode != E_CFD_MC_ERR_NOERR)
        {
            //return FALSE;
            bRet = FALSE;
        }
    }

    // HDMI
    if (CFD_IS_HDMI(pstCfdFire.u8InputSource))
    {
        u16ErrCode = MHal_XC_CFD_SetHdmiParam(&pstCfdFire);
        if (u16ErrCode != E_CFD_MC_ERR_NOERR)
        {
            //return FALSE;
            bRet = FALSE;
        }
    }

    // HDR
    u16ErrCode = MHal_XC_CFD_SetHdrParam(&pstCfdFire);
    if (u16ErrCode != E_CFD_MC_ERR_NOERR)
    {
        //return FALSE;
        bRet = FALSE;
    }

    // OSD
    u16ErrCode = MHal_XC_CFD_SetOsdParam(&_stCfdOsd[pstCfdFire.u8Win]);
    if (u16ErrCode != E_CFD_MC_ERR_NOERR)
    {
        return FALSE;
    }

    Mapi_Cfd_Maserati_DLCIP_CurveMode_Set(pstCfdFire.u8Win, _stCfdDlc.bUseCustomerDlcCurve);
    MS_Cfd_Maserati_Set_CFD_Noskip_Flags(TRUE);
    //Color_Format_Driver();
    Color_Format_Driver_Parsing();
    ///Do CFD
    Color_Format_Driver_Decision();
    MS_Cfd_Maserati_Set_CFD_Noskip_Flags(FALSE);

#if 0   // there is no tmo inm3/lm18a
/// TMO

    //Get TMO index
    ST_KDRV_XC_CFD_STATUS stCfdStatus;
    memset(&stCfdStatus, 0, sizeof(ST_KDRV_XC_CFD_STATUS));
    stCfdStatus.u32Version=CFD_STATUS_VERSION;
    stCfdStatus.u16Length=sizeof(ST_KDRV_XC_CFD_STATUS);
    stCfdStatus.u8Win=u8Win;

    ST_KDRV_XC_CFD_CONTROL_INFO stCFDCtrlInfo;
    memset(&stCFDCtrlInfo, 0, sizeof(ST_KDRV_XC_CFD_CONTROL_INFO));
    stCFDCtrlInfo.enCtrlType = E_KDRV_XC_CFD_CTRL_GET_STATUS;
    stCFDCtrlInfo.pParam = &stCfdStatus;
    stCFDCtrlInfo.u32ParamLen = sizeof(ST_KDRV_XC_CFD_STATUS);
    MHal_XC_CFDControl(&stCFDCtrlInfo);

    //Update TMO Info
    MHal_XC_UpdateTMOPara(u8Win,stCfdStatus.u8TMOColorType);
    //Do TMO
    bTmoFireEnable = TRUE;
    MHal_TMO_TmoHandler(_stCfdFire[u8Win].u8Win);
#endif
    bDTVUpdateCFDPara = FALSE;
    return bRet;
}

void MHal_XC_Set_SharedMemVersion(MS_U8 u8Win,MS_U8 u8Version)
{
    u8SharedMemVersion[u8Win] = u8Version;
}

MS_U8 MHal_XC_Get_SharedMemVersion(MS_U8 u8Win)
{
    return u8SharedMemVersion[u8Win];
}

MS_BOOL MHal_Get_HDRSeamless_Status(MS_U8 u8Win)
{
    if(CFD_IS_MM(_stCfdInit[u8Win].u8InputSource) || CFD_IS_DTV(_stCfdInit[u8Win].u8InputSource))
    {
        if( MHal_XC_Get_SharedMemVersion(u8Win) == getOpenHDRSeamlessVersion())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
        return FALSE;
    }
}


void MHal_XC_Suspend(void)
{
    MS_U8 u8RegIndex = 0;

    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_08_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_09_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_10_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_11_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_05_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_50_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_52_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_53_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_54_L);
    _au16DmaRegValue[u8RegIndex++] = MHal_XC_R2BYTE(REG_SC_BK42_55_L);
    if (u8RegIndex != DMA_STR_PROTECT_REGISTER_NUMBER)
    {
        printk("\033[31m[%s %d]Warning:DMA need protect %d registers, actually protect %d\033[m\n",__FUNCTION__,__LINE__,DMA_STR_PROTECT_REGISTER_NUMBER,u8RegIndex);
    }

    // DC off when MM playing dolby video, it shows MM UI after STR resume

    // dolbyHDR HDMI -> DC OFF/ON -> SDR ( purple, cause sdr does not run cfd )
    MHal_XC_SetDolbyStatus(0, BIT(0)|BIT(1));
    IsMVOP_TCH = FALSE;

    // store status for future STR
    _bEnableHDRCLK_for_STR = _bEnableHDRCLK;
}

void MHal_XC_Resume(void)
{
    MHal_XC_Init();

    ///HDR----
    MHal_XC_InitHDR();
    MHal_XC_EnableHDR(ENABLE);
    if (_bEnableHDRCLK_for_STR == ENABLE)
    {
        MHal_XC_EnableHDRCLK(TRUE,TRUE);
    }
    //----

    ///hdr auto download----
    ///TODO:need refine: xvYcc need  resume if it use autodownload in the future.
    //set baseaddr
    MHal_XC_W2BYTE(REG_SC_BK67_29_L, (_stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].phyBaseAddr / BYTE_PER_WORD) & 0x0000FFFF);
    MHal_XC_W2BYTEMSK(REG_SC_BK67_2A_L, ((_stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].phyBaseAddr / BYTE_PER_WORD) >> 16 & 0x0000FFFF), 0x07FF);
    //set work mode
    _Hal_XC_Set_Auto_Download_WorkMode(REG_SC_BK67_28_L, 1, _stClientInfo[E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR].enMode);
    //----

    ///DMA----
    {
        MS_U8 u8RegIndex = 0;
        // set IPM/OPM addr
        MHal_XC_W2BYTE(REG_SC_BK42_08_L, _au16DmaRegValue[u8RegIndex++]);
        MHal_XC_W2BYTEMSK(REG_SC_BK42_09_L, _au16DmaRegValue[u8RegIndex++], 0x01FF);
        MHal_XC_W2BYTE(REG_SC_BK42_10_L, _au16DmaRegValue[u8RegIndex++]);
        MHal_XC_W2BYTEMSK(REG_SC_BK42_11_L, _au16DmaRegValue[u8RegIndex++], 0x01FF);
        // set miu
        MHal_XC_W2BYTEMSK(REG_SC_BK42_05_L, _au16DmaRegValue[u8RegIndex++], 0x0030);
        // set limit addr
        // enable limit
        MHal_XC_W2BYTEMSK(REG_SC_BK42_50_L, _au16DmaRegValue[u8RegIndex++], 0x0001);
        // min addr
        MHal_XC_W2BYTE(REG_SC_BK42_52_L, _au16DmaRegValue[u8RegIndex++]);
        MHal_XC_W2BYTEMSK(REG_SC_BK42_53_L, _au16DmaRegValue[u8RegIndex++], 0x01FF);
        // max addr
        MHal_XC_W2BYTE(REG_SC_BK42_54_L, _au16DmaRegValue[u8RegIndex++]);
        MHal_XC_W2BYTEMSK(REG_SC_BK42_55_L, _au16DmaRegValue[u8RegIndex++], 0x01FF);
        if (u8RegIndex != DMA_STR_PROTECT_REGISTER_NUMBER)
        {
            printk("\033[31m[%s %d]Warning:DMA need restore %d registers, actually restore %d\033[m\n",__FUNCTION__,__LINE__,DMA_STR_PROTECT_REGISTER_NUMBER,u8RegIndex);
        }
    }
    _bSTRResumed = TRUE;
    //----
}

void MHal_XC_Init(void)
{
    // Disable fastswitch IRQ first
    MHal_XC_W2BYTE(REG_SC_BKF8_40_L, 0xFFFF);
    MHal_XC_W2BYTE(REG_SC_BKF9_40_L, 0xFFFF);
    MHal_XC_W2BYTE(REG_SC_BKFA_40_L, 0xFFFF);
    MHal_XC_W2BYTE(REG_SC_BKFB_40_L, 0xFFFF);
    MHal_XC_W2BYTE(REG_SC_BKFC_40_L, 0xFFFF);
    MHal_XC_W2BYTE(REG_SC_BKFD_40_L, 0xFFFF);
    MHal_XC_W2BYTE(REG_SC_BKFE_40_L, 0xFFFF);

    //patch for maxim Dolby HDR regular point garbage
    MHal_XC_W2BYTEMSK(REG_SC_BK1F_1D_L, 0x0040, 0x00C0);
    MHal_XC_W2BYTEMSK(REG_SC_BK1F_1D_L, 0x8000, 0x8000);

    // diable IRQ for input Vsync IPVS_SB_INT_F2
    MHal_XC_W2BYTEMSK(REG_SC_BK00_14_L, 0x0000, 0x0800);
}
#ifdef XC_REPLACE_MEMCPY_BY_BDMA
// dwsrc_addr , dwdes_addr must be physical address
// bymode = [7:4] = sel des MIU
//          [3:0] = sel src MIU
static void _bdma(MS_U32 dwsrc_addr, MS_U32 dwdes_addr, MS_U32 dwsize, MS_U8 bymode)
{
    if(dwsize==0) return;
    MS_U16 temp;
    if (((bymode & 0xf0)>>4) == 0)
    {
        temp = *(volatile MS_U16 *)(mstar_pm_base + (0x100924 << 1));
        temp &= ~0xFF00;
        temp |= 0x4000;
        *(volatile MS_U16 *)(mstar_pm_base + (0x100924 << 1)) = temp;
    }
    else
    {
        temp = *(volatile MS_U16 *)(mstar_pm_base + (0x100924 << 1));
        temp &= ~0xFF00;
        temp |= 0x4100;
        *(volatile MS_U16 *)(mstar_pm_base + (0x100924 << 1)) = temp;
    }
    if ((bymode & 0x0f) == 0)
    {
        temp = *(volatile MS_U16 *)(mstar_pm_base + (0x100924 << 1));
        temp &= ~0x00ff;
        temp |= 0x0040;
        *(volatile MS_U16 *)(mstar_pm_base + (0x100924 << 1)) = temp;
    }
    else
    {
        temp = *(volatile MS_U16 *)(mstar_pm_base + (0x100924 << 1));
        temp &= ~0x00ff;
        temp |= 0x0041;
        *(volatile MS_U16 *)(mstar_pm_base + (0x100924 << 1)) = temp;
    }

    // destination address
    *(volatile MS_U16 *)(mstar_pm_base + (0x10092c << 1)) = dwdes_addr & 0xffff;
    *(volatile MS_U16 *)(mstar_pm_base + (0x10092e << 1)) = (dwdes_addr >> 16) & 0xffff;

    // source address
    *(volatile MS_U16 *)(mstar_pm_base + (0x100928 << 1)) = dwsrc_addr& 0xffff;
    *(volatile MS_U16 *)(mstar_pm_base + (0x10092a << 1)) = (dwsrc_addr >> 16) & 0xffff;

    // size
    *(volatile MS_U16 *)(mstar_pm_base + (0x100930 << 1)) = dwsize & 0xffff;
    *(volatile MS_U16 *)(mstar_pm_base + (0x100932 << 1)) = (dwsize >> 16) & 0xffff;

    // trigger
    *(volatile MS_U16 *)(mstar_pm_base + (0x100920 << 1)) |= 1;

    // make sure it is finished.
    while ( ((*(volatile MS_U16 *) (mstar_pm_base + (0x100922 << 1)))&0x08) == 0  );
}

MS_PHY _GETPA_WITHOUT_BASE(phys_addr_t phy, MS_U8 * u8MIU_No)
{
    phys_addr_t tmp_phy = phy;

    if(phy<ARM_MIU1_BASE_ADDR)
    {
        *u8MIU_No = 0;
    }
    else if((phy >= ARM_MIU1_BASE_ADDR) && (phy<ARM_MIU2_BASE_ADDR))
    {
        tmp_phy = tmp_phy - ARM_MIU1_BASE_ADDR;
        *u8MIU_No = 1;
    }
    else
    {
        tmp_phy = tmp_phy - ARM_MIU2_BASE_ADDR;
        *u8MIU_No = 2;
    }

    return tmp_phy;
}
extern MS_BOOL MDrv_XC_GetShareMemInfo(ST_KDRV_XC_SHARE_MEMORY_INFO *pstShmemInfo);
void MHal_XC_MemCpy_by_BDMA(MS_U32 u32src_addr, MS_U32 u32dest_addr, MS_U32 u32Size)
{
    MS_U8 src_MIU_NO = 0;
    MS_U8 dest_MIU_NO = 0;
    MS_U8 u8MIU_Sel = 0;
    MS_U32 src_addr_without_base = 0;
    MS_U32 dest_addr_without_base = 0;
    ST_KDRV_XC_SHARE_MEMORY_INFO stShmemInfo;
    MDrv_XC_GetShareMemInfo(&stShmemInfo);

    src_addr_without_base = u32src_addr;
    src_MIU_NO = stShmemInfo.u32MiuNo;
    dest_addr_without_base = _GETPA_WITHOUT_BASE(u32dest_addr, &dest_MIU_NO);

    u8MIU_Sel = ((src_MIU_NO & 0x0F) | ((dest_MIU_NO&0x0F) << 4));

    _bdma(src_addr_without_base,
          dest_addr_without_base,
          u32Size,
          u8MIU_Sel);
}
#endif

#if OTT_AUTO_PAUSE_DETECT
void MHal_XC_ClearPauseStatus(void)
{
    if (bOTTPausing)
    {
        printk("[%s %d]OTT is Playing!\n",__FUNCTION__,__LINE__);
    }
    bOTTPausing = FALSE;
    getnstimeofday(&stPauseDetectStartTs);
}

MS_BOOL MHal_XC_IncOTT_OPVsyncWithoutDSInfoCount(void)
{
    struct timespec endTs;
    getnstimeofday(&endTs);
    long long diff = endTs.tv_nsec + ((endTs.tv_sec - stPauseDetectStartTs.tv_sec) * 1000000000 - stPauseDetectStartTs.tv_nsec);
    if (diff >= PAUSE_COUNT_THRESHOLD)
    {
        if (!bOTTPausing)
        {
            printk("[%s %d]OTT is Pausing!\n",__FUNCTION__,__LINE__);
        }
        bOTTPausing = TRUE;
    }
    else
    {
        bOTTPausing = FALSE;
    }
    return bOTTPausing;
}

MS_BOOL MHal_XC_IsOTTPausing(void)
{
    return bOTTPausing;
}

MS_BOOL MHal_XC_SetSWDRInfo(ST_KDRV_XC_SWDR_INFO *pstSWDRInfo)
{
    MS_U16 i = 0;
    memset(&u16SWDRData,0,(sizeof(MS_U16)*SWDR_DATA_SIZE_MAX));

    _stSWDRInfo.bDRE_En = pstSWDRInfo->bDRE_En;
    _stSWDRInfo.u16NumofData = pstSWDRInfo->u16NumofData;
    _stSWDRInfo.u16DataIndex = pstSWDRInfo->u16DataIndex;
    _stSWDRInfo.eWindow = pstSWDRInfo->eWindow;

    //set SWDR enable
    if (_stCfdMainControl.u8Input_HDRMode == E_CFIO_MODE_SDR)
    {
        if (_stSWDRInfo.bDRE_En == TRUE)
        {
            _stSWDRInfo.bDRE_SWDR_En = pstSWDRInfo->bDRE_SWDR_En;
        }
        else
        {
            _stSWDRInfo.bDRE_SWDR_En = FALSE;
        }
    }
    else
    {
        _stSWDRInfo.bDRE_SWDR_En = FALSE;
    }

    //set Data Size
    if(_stSWDRInfo.u16NumofData > SWDR_DATA_SIZE_MAX-1)
    {
        _stSWDRInfo.u16NumofData = SWDR_DATA_SIZE_MAX-1;
        printk("\033[31m[%s %d]Warning:Set SWDR Data Out of Max Size\033[m\n",__FUNCTION__,__LINE__);
    }

    //set Data
    for(i = 0 ; i < _stSWDRInfo.u16NumofData ; i++)
    {
        u16SWDRData[i] = *(pstSWDRInfo->pu16Data+i);
    }
    u16SWDRData[15] = (_stSWDRInfo.bDRE_SWDR_En == 0);

    //MAPI_DRE_En(_stSWDRInfo.bDRE_En);
    //MAPI_DRE_SWDR_En(_stSWDRInfo.bDRE_SWDR_En);

    if ((pstSWDRInfo->u16NumofData > 0) && (pstSWDRInfo->pu16Data != NULL))
    {
        //MAPI_DRE_SetValues(&u16SWDRData[0],_stSWDRInfo.u16NumofData);
    }
    else
    {
        printk("\033[31m[%s %d]Warning:SWDR Data Incorrect\033[m\n",__FUNCTION__,__LINE__);
    }

    u16SWDRData[_stSWDRInfo.u16NumofData] = _stCfdMainControl.u8Input_HDRMode;
    _stSWDRInfo.u16NumofData += 1;

    return TRUE;
}

MS_BOOL MHal_XC_GetSWDRInfo(ST_KDRV_XC_SWDR_INFO *pstSWDRInfo)
{
    MS_U16 i = 0;

    pstSWDRInfo->bDRE_En = _stSWDRInfo.bDRE_En;
    pstSWDRInfo->bDRE_SWDR_En = _stSWDRInfo.bDRE_SWDR_En;
    pstSWDRInfo->u16DataIndex = _stSWDRInfo.u16DataIndex;
    pstSWDRInfo->eWindow = _stSWDRInfo.eWindow;

    if(pstSWDRInfo->u16NumofData > SWDR_DATA_SIZE_MAX)
    {
        pstSWDRInfo->u16NumofData = SWDR_DATA_SIZE_MAX;
        printk("\033[31m[%s %d]Warning:Get SWDR Data Out of Max Size\033[m\n",__FUNCTION__,__LINE__);
    }
    for(i = 0 ; i < pstSWDRInfo->u16NumofData ; i++)
    {
        *(pstSWDRInfo->pu16Data+i) = u16SWDRData[i];
    }

    return TRUE;
}

#endif

#if HDMI_AUTO_NOSIGNAL_DETECT
void MHal_XC_ClearNoSignalStatus(void)
{
    if (bNoSignal)
    {
        printk("[%s %d]HDMI signal input!\n",__FUNCTION__,__LINE__);
    }
    bNoSignal = FALSE;
    getnstimeofday(&stNoSignalDetectStartTs);
}

MS_BOOL MHal_XC_IncHDMI_OPVsyncWithoutSignalCount(void)
{
    struct timespec endTs;
    getnstimeofday(&endTs);
    long long diff = endTs.tv_nsec + ((endTs.tv_sec - stNoSignalDetectStartTs.tv_sec) * 1000000000 - stNoSignalDetectStartTs.tv_nsec);
    if (diff >= NOSIGNAL_COUNT_THRESHOLD)
    {
        if (!bNoSignal)
        {
            printk("[%s %d]No HDMI signal!\n",__FUNCTION__,__LINE__);
        }
        bNoSignal = TRUE;
    }
    else
    {
        bNoSignal = FALSE;
    }
    return bNoSignal;
}

MS_BOOL MHal_XC_IsHDMINoSignal(void)
{
    return bNoSignal;
}
#endif

extern unsigned int xc_proc_wake_up(void);
void MHal_XC_HDR_Status_Record(MS_BOOL bstatus)
{
    static MS_BOOL bChange = FALSE;
    if(bChange != bstatus)
    {
        //printk(KERN_CRIT"\033[1;32m[%s:%d]bChange=%d:bstatus=%d\033[m\n",__FUNCTION__,__LINE__,bChange,bstatus);
        xc_proc_wake_up();
        bChange = bstatus;
    }
}

#ifdef DYNAMIC_SWITCH_SDR_OPENHDR
void MHal_XC_Get_HDRTOSDR_CFDStatus(MS_BOOL *pstHDRTOSDR_CFD_DONE)
{
    *pstHDRTOSDR_CFD_DONE =_bHDRTOSDR_CFD_DONE;
}

void MHal_XC_Set_HDRTOSDR_CFDStatus(MS_BOOL bHDRTOSDR_CFD_DONE)
{
    _bHDRTOSDR_CFD_DONE  = bHDRTOSDR_CFD_DONE;
}
#endif
// Report Hue/Sat for TCL
MS_U32 MHal_XC_WinSize_init(void)
{
    MS_U16 winHeight, winWidth;
    MS_U32 winSize;
    winHeight = MHal_XC_R2BYTEMSK(REG_SC_BK01_06_L , 0x1FFF);
    winWidth  = MHal_XC_R2BYTEMSK(REG_SC_BK01_07_L , 0x3FFF);
    winSize = (MS_U32)winHeight * (MS_U32)winWidth;
    MHal_XC_WriteByteMask(REG_SC_BK64_11_H, 0x80, 0x80);            // Enable report
    MHal_XC_W2BYTEMSK(REG_SC_BK64_11_L, 0x0, 0x1FFF);               // Set H start
    MHal_XC_W2BYTEMSK(REG_SC_BK64_12_L, (winWidth - 1), 0x1FFF);     // Set H end
    MHal_XC_W2BYTEMSK(REG_SC_BK64_13_L, 0x0, 0x1FFF);               // Set V start
    MHal_XC_W2BYTEMSK(REG_SC_BK64_14_L, (winHeight - 1), 0x1FFF);   // Set V end

    //printk("[Ruei-Jyun][WinSize] Height: %d Width: %d Size: %d\n", winHeight, winWidth, winSize);

    return winSize;
}
MS_U16 MHal_XC_RGB2Hue(MS_U16 inR, MS_U16 inG, MS_U16 inB)
{
    MS_U8 maxIdx; // R:0 G:1 B:2
    MS_U16 maxRGB, minRGB, deltaRGB;

    maxIdx = (inR > inG) ? ( (inR > inB) ? 0 : 2 ) : ( (inG > inB) ? 1 : 2 );
    maxRGB = max(max(inR, inG), inB);
    minRGB = min(min(inR, inG), inB);
    deltaRGB = maxRGB - minRGB;

    if( deltaRGB <= 2 )
    {
        return 400;
    }
    else if( (maxIdx == 0) && (inG>=inB) )
    {
        return 60*(inG-inB)/deltaRGB;
    }
    else if( (maxIdx == 0) && (inG<inB) )
    {
        return 360 - 60*(inB-inG)/deltaRGB; //60*(inG-inB)/deltaRGB + 360;
    }
    else if( maxIdx == 1 )
    {
        return 60*(inB-inR)/deltaRGB + 120;
    }
    else if( maxIdx == 2 )
    {
        return 60*(inR-inG)/deltaRGB + 240;
    }
    else
    {
        return 400;
    }

}
MS_U16 MHal_XC_RGB2Sat(MS_U16 inR, MS_U16 inG, MS_U16 inB)
{
    MS_U16 maxRGB, minRGB, inY, item1, item2;
    maxRGB = max(max(inR, inG), inB);
    minRGB = min(min(inR, inG), inB);
    inY = (MS_U16)(( 218 * (MS_U32)inR + 732 * (MS_U32)inG +  74 * (MS_U32)inB + (1<<9) ) >> 10);

    if((1023-inY) == 0)
    {
        item1 = 0;
    }
    else
    {
        item1 = min( ((maxRGB-inY)<<10) / (1023-inY), 1023 );
    }
    if(inY == 0)
    {
        item2 = 0;
    }
    else
    {
        item2 = min( ((inY-minRGB)<<10) / inY, 1023 );
    }
    return max(item1, item2);
}
MS_U16 MHal_XC_GetHue(void)
{
    MS_U16 sumR_H, sumR_M, sumR_L, sumG_H, sumG_M, sumG_L, sumB_H, sumB_M, sumB_L;
    MS_U16 avgR, avgG, avgB, Hue;
    MS_U32 winSize;
    MS_U64 sumR, sumG, sumB;

    winSize = max( MHal_XC_WinSize_init(), 1 );

    sumR_H = MHal_XC_R2BYTEMSK(REG_SC_BK64_17_L, 0x0003);
    sumR_M = MHal_XC_R2BYTE(REG_SC_BK64_16_L);
    sumR_L = MHal_XC_R2BYTE(REG_SC_BK64_15_L);
    sumR = ((MS_U64)sumR_H << 32) + ((MS_U64)sumR_M << 16) + ((MS_U64)sumR_L);
    sumG_H = MHal_XC_R2BYTEMSK(REG_SC_BK64_1A_L, 0x0003);
    sumG_M = MHal_XC_R2BYTE(REG_SC_BK64_19_L);
    sumG_L = MHal_XC_R2BYTE(REG_SC_BK64_18_L);
    sumG = ((MS_U64)sumG_H << 32) + ((MS_U64)sumG_M << 16) + ((MS_U64)sumG_L);
    sumB_H = MHal_XC_R2BYTEMSK(REG_SC_BK64_1D_L, 0x0003);
    sumB_M = MHal_XC_R2BYTE(REG_SC_BK64_1C_L);
    sumB_L = MHal_XC_R2BYTE(REG_SC_BK64_1B_L);
    sumB = ((MS_U64)sumB_H << 32) + ((MS_U64)sumB_M << 16) + ((MS_U64)sumB_L);

    avgR = (MS_U16)(sumR / (MS_U64)winSize);
    avgG = (MS_U16)(sumG / (MS_U64)winSize);
    avgB = (MS_U16)(sumB / (MS_U64)winSize);

    Hue = MHal_XC_RGB2Hue(avgR, avgG, avgB);
    //printk("[Ruei-Jyun][Hue] R: %d G: %d B: %d Hue: %d\n", avgR, avgG, avgB, Hue);

    return Hue;
}
MS_U16 MHal_XC_GetSat(void)
{
    MS_U16 sumR_H, sumR_M, sumR_L, sumG_H, sumG_M, sumG_L, sumB_H, sumB_M, sumB_L;
    MS_U16 avgR, avgG, avgB, Sat;
    MS_U32 winSize;
    MS_U64 sumR, sumG, sumB;

    winSize = max( MHal_XC_WinSize_init(), 1 );

    sumR_H = MHal_XC_R2BYTEMSK(REG_SC_BK64_17_L, 0x0003);
    sumR_M = MHal_XC_R2BYTE(REG_SC_BK64_16_L);
    sumR_L = MHal_XC_R2BYTE(REG_SC_BK64_15_L);
    sumR = ((MS_U64)sumR_H << 32) + ((MS_U64)sumR_M << 16) + ((MS_U64)sumR_L);
    sumG_H = MHal_XC_R2BYTEMSK(REG_SC_BK64_1A_L, 0x0003);
    sumG_M = MHal_XC_R2BYTE(REG_SC_BK64_19_L);
    sumG_L = MHal_XC_R2BYTE(REG_SC_BK64_18_L);
    sumG = ((MS_U64)sumG_H << 32) + ((MS_U64)sumG_M << 16) + ((MS_U64)sumG_L);
    sumB_H = MHal_XC_R2BYTEMSK(REG_SC_BK64_1D_L, 0x0003);
    sumB_M = MHal_XC_R2BYTE(REG_SC_BK64_1C_L);
    sumB_L = MHal_XC_R2BYTE(REG_SC_BK64_1B_L);
    sumB = ((MS_U64)sumB_H << 32) + ((MS_U64)sumB_M << 16) + ((MS_U64)sumB_L);

    avgR = (MS_U16)(sumR / (MS_U64)winSize);
    avgG = (MS_U16)(sumG / (MS_U64)winSize);
    avgB = (MS_U16)(sumB / (MS_U64)winSize);

    Sat = MHal_XC_RGB2Sat(avgR, avgG, avgB);
    //printk("[Ruei-Jyun][Sat] R: %d G: %d B: %d Sat: %d\n", avgR, avgG, avgB, Sat);

    return Sat;
}

MS_U16 MHal_XC_CFD_GetPanelParam(ST_KDRV_XC_CFD_PANEL *pstCfdPanel)
{
    if (pstCfdPanel == NULL)
    {
        return E_CFD_MC_ERR_INPUT_MAIN_CONTROLS;
    }

    memcpy (pstCfdPanel, &_stCfdPanel, sizeof(ST_KDRV_XC_CFD_PANEL));

    return E_CFD_MC_ERR_NOERR;
}

void MHal_XC_EnableTCHHDR(MS_BOOL bEnableTCHHDR)
{
    MHal_XC_W2BYTEMSK(REG_SC_BK71_01_L, bEnableTCHHDR, BIT(0));
}

BOOL MHal_XC_GetTCHHDRStatus(void)
{
    return IsMVOP_TCH;
}

void MHal_XC_GetMaxRGBHist(ST_KDRV_XC_RGBHistogram *pstRGBHistogram)
{
    MS_U8 u8Idx, u8Addr;
    MS_U16 u16LowValue, u16HighValue;

    if(pstRGBHistogram == NULL)
    {
        printk("[%s %d]ERROR pstRGBHistogram point is NULL\n", __FUNCTION__,__LINE__);
        return;
    }
    if(pstRGBHistogram->pu32RGBHistogram == NULL)
    {
        printk("[%s %d]ERROR pu32RGBHistogram point is NULL\n", __FUNCTION__,__LINE__);
        return;
    }

    // Set hist to maxRGB mode
    if(_stCfdHdr[E_KDRV_MAIN_WINDOW].u8HdrType == E_KDRV_XC_CFD_HDR_TYPE_OPEN)
    {
        MHal_XC_W2BYTEMSK( REG_SC_BK5C_01_L, 0x4003, 0xC003 );
    }
    else if(_stCfdHdr[E_KDRV_MAIN_WINDOW].u8HdrType == E_KDRV_XC_CFD_HDR_TYPE_HLG)
    {
        MHal_XC_W2BYTEMSK( REG_SC_BK5C_01_L, 0x0003, 0xC003 );
    }
    MHal_XC_W2BYTEMSK( REG_SC_BK02_2A_L, 0x0, 0x100 );

    u8Addr = 0x10; // Hist start addr.
    for( u8Idx=0; u8Idx<32; ++u8Idx )
    {
        u16LowValue = MHal_XC_R2BYTE( (0x135C00 | u8Addr << 1) );
        u8Addr = u8Addr + 1;
        u16HighValue = MHal_XC_R2BYTE( (0x135C00 | u8Addr << 1) ) & 0xFF;
        pstRGBHistogram->pu32RGBHistogram[u8Idx] = ((MS_U32)u16HighValue << 16) + (MS_U32)u16LowValue;
        u8Addr = u8Addr + 1;
    }
}

MS_U8 MHal_HDMI_avi_infoframe_info(MS_U8 ucHDMIInfoSource, MS_U8 u8byte)
{
    MS_U16 u16Reg = 0;

    if(ucHDMIInfoSource == HDMI_INFO_SOURCE0)
    {

    }

    if(u8byte<1) //u8byte = 0 is invalide
        u8byte = 1;

    u16Reg = MHal_XC_R2BYTE(REG_HDMI_DUAL_0_40_L + ((u8byte - 1)/2)*2);

    return (MS_U8)(((u8byte-1)&0x01) ? (u16Reg>>8):(u16Reg&0xFF));
}
MS_HDMI_COLOR_FORMAT MHal_HDMI_Get_ColorFormat(MS_U8 ucHDMIInfoSource)
{
    MS_HDMI_COLOR_FORMAT pkt_ColorFormat=MS_HDMI_COLOR_UNKNOWN;
    switch(MHal_HDMI_avi_infoframe_info(ucHDMIInfoSource, _BYTE_1)& 0x60 )
    {
        case 0x00:
            pkt_ColorFormat = MS_HDMI_COLOR_RGB;
            break;
        case 0x40:
            pkt_ColorFormat = MS_HDMI_COLOR_YUV_444;
            break;
        case 0x20:
            pkt_ColorFormat = MS_HDMI_COLOR_YUV_422;
            break;
        case 0x60:
            pkt_ColorFormat = MS_HDMI_COLOR_YUV_420;
            break;
        default:
            break;
    };
    return pkt_ColorFormat;
}

MS_HDMI_EXT_COLORIMETRY_FORMAT MHal_HDMI_Get_ExtColorimetry(MS_U8 ucHDMIInfoSource)
{
    MS_HDMI_EXT_COLORIMETRY_FORMAT pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_UNKNOWN;
    MS_U8 u8AVI_info2, u8AVI_info3;

    u8AVI_info2 = MHal_HDMI_avi_infoframe_info(ucHDMIInfoSource, _BYTE_2);
    if((u8AVI_info2&0xc0) == 0xc0 )
    {
        u8AVI_info3= MHal_HDMI_avi_infoframe_info(ucHDMIInfoSource, _BYTE_3);
        switch (u8AVI_info3 & 0x70)
        {
            case 0x00:
                pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_XVYCC601;
                break;
            case 0x10:
                pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_XVYCC709;
                break;
            case 0x20:
                pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_SYCC601;
                break;
            case 0x30:
                pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_ADOBEYCC601;
                break;
            case 0x40:
                pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_ADOBERGB;
                break;
            case 0x50:
                pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_BT2020YcCbcCrc;
                break;
            case 0x60:
                pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_BT2020RGBYCbCr;
                break;
            default:
                pkt_ExtColorimetry = MS_HDMI_EXT_COLOR_UNKNOWN;
                break;
        }
    }
    return pkt_ExtColorimetry;
}

EN_HDMI_COLOR_RANGE MHal_HDMI_Get_ColorRange(MS_U8 ucHDMIInfoSource)
{
    EN_HDMI_COLOR_RANGE enPKTColorRange = E_HDMI_COLOR_RANGE_DEFAULT;
    if((MHal_HDMI_avi_infoframe_info(ucHDMIInfoSource, _BYTE_1)& 0x60) == 0x00)  // RGB
    {
        switch(MHal_HDMI_avi_infoframe_info(ucHDMIInfoSource, _BYTE_3) & 0x0C)
        {
            case 0x00:
                enPKTColorRange = E_HDMI_COLOR_RANGE_DEFAULT;
                break;
            case 0x04:
                enPKTColorRange = E_HDMI_COLOR_RANGE_LIMIT;
                break;
            case 0x08:
                enPKTColorRange = E_HDMI_COLOR_RANGE_FULL;
                break;
            case 0x0C:
                enPKTColorRange = E_HDMI_COLOR_RANGE_RESERVED;
                break;
            default:
                break;
        };
    }
    else // YUV
    {
        switch(MHal_HDMI_avi_infoframe_info(ucHDMIInfoSource, _BYTE_5) & 0xC0)
        {
            case 0x00:
                enPKTColorRange = E_HDMI_COLOR_RANGE_LIMIT;
                break;
            case 0x40:
                enPKTColorRange = E_HDMI_COLOR_RANGE_FULL;
                break;
            case 0x80:
                enPKTColorRange = E_HDMI_COLOR_RANGE_RESERVED;
                break;
            case 0xC0:
                enPKTColorRange = E_HDMI_COLOR_RANGE_RESERVED;
                break;
            default:
                break;
        };
    }
    return enPKTColorRange;
}

void MHal_XC_HDMI_ParseHDRInfoFrame(void *pPacket, MS_U8 u8Win)
{
    // HDR metadata structure version is the same.
    MS_U16 *pInfoFrame = (MS_U16*)pPacket;
    if(MHal_HDMI_Get_ColorRange(HDMI_INFO_SOURCE0) == E_HDMI_COLOR_RANGE_FULL)
    {
        _stCfdHdmi[u8Win].bIsFullRange = TRUE;
    }
    else
    {
        _stCfdHdmi[u8Win].bIsFullRange = FALSE;
    }
    _stCfdHdmi[u8Win].u8PixelFormat = (MS_U8)MHal_HDMI_Get_ColorFormat(HDMI_INFO_SOURCE0);
    _stCfdHdmi[u8Win].u8Colorimetry = (MS_U8)((MHal_HDMI_avi_infoframe_info(HDMI_INFO_SOURCE0, _BYTE_2) &  (BIT(7)|BIT(6)))>>6);
    _stCfdHdmi[u8Win].u8ExtendedColorimetry = (MS_U8)MHal_HDMI_Get_ExtColorimetry(HDMI_INFO_SOURCE0);
    _stCfdHdmi[u8Win].u8RgbQuantizationRange = (MS_U8)MHal_HDMI_Get_ColorRange(HDMI_INFO_SOURCE0);
    _stCfdHdmi[u8Win].u8YccQuantizationRange = (MS_U8)MHal_HDMI_Get_ColorRange(HDMI_INFO_SOURCE0);
    _stCfdHdmi[u8Win].u8EOTF = ((pInfoFrame[0]>>8)&0x07); // 8~15bits, 0:SDR gamma,  1:HDR gamma,  2:SMPTE ST2084,  3:Future EOTF,  4-7:Reserved
    _stCfdHdmi[u8Win].u8SMDID = (pInfoFrame[1]&0x0007);    // 0:StaticMetadata Type 1,  1-7:Reserved for future use
    _stCfdHdmi[u8Win].u16Display_Primaries_x[0] = (pInfoFrame[2]<<8) + (pInfoFrame[1]>>8);

    _stCfdHdmi[u8Win].u16Display_Primaries_y[0] = (pInfoFrame[3]<<8) + (pInfoFrame[2]>>8);
    _stCfdHdmi[u8Win].u16Display_Primaries_x[1] = (pInfoFrame[4]<<8) + (pInfoFrame[3]>>8);
    _stCfdHdmi[u8Win].u16Display_Primaries_y[1] = (pInfoFrame[5]<<8) + (pInfoFrame[4]>>8);
    _stCfdHdmi[u8Win].u16Display_Primaries_x[2] = (pInfoFrame[6]<<8) + (pInfoFrame[5]>>8);
    _stCfdHdmi[u8Win].u16Display_Primaries_y[2] = (pInfoFrame[7]<<8) + (pInfoFrame[6]>>8);
    _stCfdHdmi[u8Win].u16White_point_x = (pInfoFrame[8]<<8) + (pInfoFrame[7]>>8);
    _stCfdHdmi[u8Win].u16White_point_y = (pInfoFrame[9]<<8) + (pInfoFrame[8]>>8);
    _stCfdHdmi[u8Win].u16MasterPanelMaxLuminance = (pInfoFrame[10]<<8) + (pInfoFrame[9]>>8);
    _stCfdHdmi[u8Win].u16MasterPanelMinLuminance = (pInfoFrame[11]<<8) + (pInfoFrame[10]>>8);
    _stCfdHdmi[u8Win].u16MaxContentLightLevel = (pInfoFrame[12]<<8) + (pInfoFrame[11]>>8);
    _stCfdHdmi[u8Win].u16MaxFrameAvgLightLevel = (pInfoFrame[13]<<8) + (pInfoFrame[12]>>8);

    _stCfdHdmi[u8Win].bHDRInfoFrameValid = TRUE;
}

MS_BOOL MHal_HDMI_Get_InfoFrame(MS_U8 u8Win)
{
    MS_BOOL bRet = FALSE;
    MS_U8 uctemp = 0;
    MS_U16 usPacketContent = 0;
    MS_U16 usHDMIBankOffset = 0;

    // 1.get packed data
    MS_U16 *u16HdrPacket = NULL;
    u16HdrPacket = kmalloc(HDMI_HDR_PACKET_SIZE * sizeof(MS_U16), GFP_KERNEL);
    if(!u16HdrPacket)
    {
        return bRet;
    }
    else
    {
        memset(u16HdrPacket,0,HDMI_HDR_PACKET_SIZE * sizeof(MS_U16));

        //WOSQMST-4636 HDMI play AC on/off  detect HDR,but sw load SDR setting.
        //check HDR status  from dumming register
        if((MHal_XC_R2BYTEMSK(REG_SC_BK01_42_L, BIT(15))) == BIT(15))
        {
	        for(uctemp = 0; uctemp < HDMI_HDR_PACKET_SIZE; uctemp++)
	        {
	            usPacketContent = MHal_XC_R2BYTE(REG_HDMI2_DUAL_0_61_L +usHDMIBankOffset +uctemp *2);

	            u16HdrPacket[uctemp] = (usPacketContent << 8) |(usPacketContent >> 8);
	        }
        }
        // 2.ready for parsing
        MHal_XC_HDMI_ParseHDRInfoFrame(u16HdrPacket,u8Win);

        // 3.free memory
        if(u16HdrPacket)
        {
            kfree(u16HdrPacket);
        }

        bRet = TRUE;
    }
    return bRet;
}

void MHal_XC_changeADLBasedAddress(MS_BOOL bEnable)
{
    bADLChangeBasedAddress = bEnable;
}

void MHal_XC_updateADLBasedAddress(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient,MS_U8 u8Index,MS_PHY *pphyFireAdlAddr, MS_U32 *pu32Depth)

{
    //update ADL PA based address and depth in order to change register setting
    switch(enClient)
    {
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR:
        {
            u8HDRSeamlessIPDSIndex = u8Index;//update ds index in order toupdate ADL VA based address
            *pphyFireAdlAddr = _stClientInfo[enClient].phyBaseAddr + u8Index* AUTO_DOWNLOAD_HDR_3DLUT_RGB_SRAM_MAX_ADDR * BYTE_PER_WORD;
            *pu32Depth = AUTO_DOWNLOAD_HDR_3DLUT_SRAM_MAX_ADDR;
            break;
        }
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC:
        {
            u8HDRSeamlessOPDSIndex = u8Index;//update ds index in order toupdate ADL VA based address
            *pphyFireAdlAddr = _stClientInfo[enClient].phyBaseAddr + u8Index* (XVYCC_GAMMA_ENTRY+XVYCC_DEGAMMA_ENTRY+XVYCC_GAMMA_BLANKING) * BYTE_PER_WORD;
            *pu32Depth = (XVYCC_GAMMA_ENTRY+XVYCC_DEGAMMA_ENTRY+XVYCC_GAMMA_BLANKING);
            break;
        }


        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCOP2GAMMA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_ODTABLE2:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_DEMURA:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_OP2LUT:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_0:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_T3D_1:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FRCSPTPOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_FOOPM:
        case E_KDRV_XC_AUTODOWNLOAD_CLIENT_MAX:
        default:
            break;
    }
}

EN_KDRV_XC_HDR_SEAMLESS_PATH MHal_XC_Get_HDRSeamless_HDMI_Path()
{
    return HDMI_HDR_SEAMLESS_PATH;
}

void MHal_XC_HDMI_Seamless_Without_Freeze_Mute()
{
    if (FALSE == KHAL_SC_Get_DynamicScaling_Status(E_KDRV_XC_SC0))
    {
        printk("[%s,%5d] MHal_XC_HDMI_Seamless_Without_Freeze_Mute !! DynamicScaling_Status is OFF, why set DS command ??????\n", __func__, __LINE__);
        //dump_stack();
        return FALSE;
    }

    MS_U8 u8Win = 0;
    // step 1. update HDMI HDR packet
    if(MHal_HDMI_Get_InfoFrame(u8Win))
    {

        if(u8LastEOTFflag!=_stCfdHdmi[u8Win].u8EOTF)
        {
            u8LastEOTFflag=_stCfdHdmi[u8Win].u8EOTF;
            bHDMIEOTFChange = TRUE;
            setHDMIPacketChange(bHDMIEOTFChange);

            //step 2. prepare DS index, and update CFD DS index
            K_XC_DS_CMDCNT stXCDSCmdCnt;
            memset(&stXCDSCmdCnt, 0, sizeof(K_XC_DS_CMDCNT));
            KApi_XC_FireDynamicScalingIndex(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW);
            KApi_XC_GetDynamicScalingIndex(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,&uds8Index);
            setDSIndex(uds8Index);

            //step 3. update HDMI metadata
            ST_KDRV_XC_CFD_HDMI stCFDHDMIHDRPara;
            memcpy(&stCFDHDMIHDRPara,&_stCfdHdmi[u8Win], sizeof(ST_KDRV_XC_CFD_HDMI));
            ST_KDRV_XC_CFD_CONTROL_INFO stUpdateHDMIPara;
            stUpdateHDMIPara.enCtrlType = E_KDRV_XC_CFD_CTRL_SET_HDMI;
            stUpdateHDMIPara.pParam = &stCFDHDMIHDRPara;
            MHal_XC_CFDControl(&stUpdateHDMIPara);

            //gen ds command for autodwonload
            MHal_XC_changeADLBasedAddress(TRUE);
            MS_PHY pphyHDRFireAdlAddr;
            MS_PHY pphyXVYCCFireAdlAddr;
            MS_U32 pu32HDRDepth;
            MS_U32 pu32XVYCCDepth;
            MHal_XC_updateADLBasedAddress(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR,uds8Index,&pphyHDRFireAdlAddr, &pu32HDRDepth);
            MHal_XC_updateADLBasedAddress(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC,uds8Index,&pphyXVYCCFireAdlAddr, &pu32XVYCCDepth);

            //set HDR phyaddr
            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_29_L, (pphyHDRFireAdlAddr / BYTE_PER_WORD) &0x0000FFFF, K_DS_IP, K_DS_XC, &stXCDSCmdCnt, uds8Index);
            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_2A_L, (pphyHDRFireAdlAddr / BYTE_PER_WORD) >> 16& 0x0000FFFF, K_DS_IP, K_DS_XC, &stXCDSCmdCnt, uds8Index);
            // set HDR depth
            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_2B_L, pu32HDRDepth, K_DS_IP, K_DS_XC, &stXCDSCmdCnt, uds8Index);
            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_2C_L, pu32HDRDepth, K_DS_IP, K_DS_XC, &stXCDSCmdCnt, uds8Index);
            // enable HDR  auto download
            KHal_SC_WriteSWDSCommand_Mask(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_38_L, BIT(9), K_DS_IP, K_DS_XC, &stXCDSCmdCnt,uds8Index, BIT(9));
            KHal_SC_WriteSWDSCommand_Mask(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_39_L, BIT(9), K_DS_IP, K_DS_XC, &stXCDSCmdCnt,uds8Index, BIT(9));
            //KHal_SC_WriteSWDSCommand_Mask(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_28_L, BIT(0), K_DS_IP, K_DS_XC, &stXCDSCmdCnt,uds8Index, BIT(0));

            //set XVYCC phyaddr
            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_16_L, (pphyXVYCCFireAdlAddr / BYTE_PER_WORD) &0x0000FFFF, K_DS_OP, K_DS_XC, &stXCDSCmdCnt, uds8Index);
            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_17_L, (pphyXVYCCFireAdlAddr / BYTE_PER_WORD) >>16 & 0x0000FFFF, K_DS_OP, K_DS_XC, &stXCDSCmdCnt, uds8Index);
            //set XVYCC depth
            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_1A_L, pu32XVYCCDepth, K_DS_OP, K_DS_XC, &stXCDSCmdCnt, uds8Index);
            KHal_SC_WriteSWDSCommand(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_1D_L, pu32XVYCCDepth, K_DS_OP, K_DS_XC, &stXCDSCmdCnt, uds8Index);
            //enable XVYCC auto download
            KHal_SC_WriteSWDSCommand_Mask(E_KDRV_XC_SC0, E_KDRV_MAIN_WINDOW,E_DS_CLIENT_HDR, REG_SC_BK67_11_L, BIT(2), K_DS_OP, K_DS_XC, &stXCDSCmdCnt,uds8Index, BIT(2));

            //step 4. call CFD
            cfd_Prepare_DS(); //reset CFD ds command count to 0
            cfd_SetCmdCnt(stXCDSCmdCnt); // set CFD ds command
            Mapi_Cfd_Maserati_DLCIP_CurveMode_Set(u8Win, _stCfdDlc.bUseCustomerDlcCurve);
            MHal_XC_Seamless_FireCFD(u8Win);//call CFD

            //step 5. trigger sw ds
            KApi_XC_Set_DSForceIndex(E_KDRV_XC_SC0, TRUE, uds8Index,E_KDRV_MAIN_WINDOW);

            MHal_XC_changeADLBasedAddress(FALSE);

            bHDMIEOTFChange = FALSE;
            setHDMIPacketChange(bHDMIEOTFChange);

        }
        else
        {
            MHal_XC_changeADLBasedAddress(FALSE);
            bHDMIEOTFChange = FALSE;
            setHDMIPacketChange(bHDMIEOTFChange);
        }
    }
    else
    {
        MHal_XC_changeADLBasedAddress(FALSE);
        bHDMIEOTFChange = FALSE;
        setHDMIPacketChange(bHDMIEOTFChange);
    }
}

void MHal_XC_HDMI_Seamless_With_Freeze()
{
    MS_U8 u8Win = 0;
    static MS_BOOL bfirst = TRUE;
    static MS_BOOL bHDRDataChange = FALSE;
    static MS_BOOL bSeamlessCFDDone = FALSE;
    static MS_U8 u8XCframeCount = 0;
    MS_U8 u8FRCframeCount = 5;
    // step 1. update HDMI HDR packet
    if(CFD_IS_HDMI(_stCfdInit[u8Win].u8InputSource) && MHal_HDMI_Get_InfoFrame(u8Win))
    {
        if(bfirst)
        {
            u8LastEOTFflag = _stCfdHdmi[u8Win].u8EOTF;
            u8LastExtendedColorimetryflag = _stCfdHdmi[u8Win].u8ExtendedColorimetry;
            bfirst = FALSE;
            bSeamlessFirst = FALSE;
        }

        if((u8LastEOTFflag!=_stCfdHdmi[u8Win].u8EOTF) || ( u8LastExtendedColorimetryflag !=_stCfdHdmi[u8Win].u8ExtendedColorimetry))
        {
            u8LastEOTFflag=_stCfdHdmi[u8Win].u8EOTF;
            u8LastExtendedColorimetryflag = _stCfdHdmi[u8Win].u8ExtendedColorimetry;
                //dummy for seamless (freeze ~ cfd done)
                MHal_XC_W2BYTEMSK(REG_SC_BK1F_7D_L, BIT(0), BIT(0));
                _bXCFreezeDone = FALSE;
            if((MHal_XC_R2BYTE(REG_SC_BK10_19_L)&BIT(1))!=BIT(1))
            {
                // step 2. freeze image
                MHal_XC_W2BYTEMSK(REG_SC_BK12_01_L, BIT(11), BIT(11));
                //printk("\033[0;32;31m [%s][%d] freeze \n\033[m", __FUNCTION__, __LINE__);
            }
            else
            {
                bHDMIChangeDuringMute = TRUE;
            }
            _eHDMISeamlessStatus = SEAMLESS_DATACHANGED;
            bHDRDataChange = TRUE;
            bSeamlessCFDDone = FALSE;
            u8Timer = 0;
         }

        //printk("\033[0;32;31m [%s][%d] bHDMIFreezeGOPDone=%u, bHDMIEOTFChange=%u \n\033[m", __FUNCTION__, __LINE__, bHDMIFreezeGOPDone, bHDMIEOTFChange);
        // call CFD after GOP Capture image
        if(bHDMIFreezeGOPDone && bHDRDataChange && !bSeamlessCFDDone)
        {
            _bXCFreezeDone = TRUE;
            MHal_XC_W2BYTEMSK(REG_SC_BK12_01_L, 0x0, BIT(11));
            setHDMIPacketChange(bHDRDataChange);
            // step 3. update HDR type and hdmi packet info
            if (_stCfdHdmi[u8Win].u8EOTF == 2)
            {
                // HDR10
                _stCfdHdr[u8Win].u8HdrType = E_KDRV_XC_CFD_HDR_TYPE_OPEN;
                MHal_XC_W2BYTEMSK(REG_SC_BK4E_58_L, BIT(7), BIT(7));
            }
            else if (_stCfdHdmi[u8Win].u8EOTF == 3)
            {
                // HLG
                _stCfdHdr[u8Win].u8HdrType = E_KDRV_XC_CFD_HDR_TYPE_HLG;
                MHal_XC_W2BYTEMSK(REG_SC_BK4E_58_L, BIT(7), BIT(7));
            }
            else
            {
                _stCfdHdr[u8Win].u8HdrType = E_KDRV_XC_CFD_HDR_TYPE_NONE;
                MHal_XC_W2BYTEMSK(REG_SC_BK4E_58_L, 0x0 ,BIT(7));
            }
            ST_KDRV_XC_CFD_HDR stCFDHDRPara;
            memcpy(&stCFDHDRPara,&_stCfdHdr[u8Win], sizeof(ST_KDRV_XC_CFD_HDR));
            ST_KDRV_XC_CFD_CONTROL_INFO stUpdateHDRPara;
            stUpdateHDRPara.enCtrlType = E_KDRV_XC_CFD_CTRL_SET_HDR;
            stUpdateHDRPara.pParam = &stCFDHDRPara;
            MHal_XC_CFDControl(&stUpdateHDRPara);

            ST_KDRV_XC_CFD_HDMI stCFDHDMIHDRPara;
            memcpy(&stCFDHDMIHDRPara,&_stCfdHdmi[u8Win], sizeof(ST_KDRV_XC_CFD_HDMI));
            ST_KDRV_XC_CFD_CONTROL_INFO stUpdateHDMIPara;
            stUpdateHDMIPara.enCtrlType = E_KDRV_XC_CFD_CTRL_SET_HDMI;
            stUpdateHDMIPara.pParam = &stCFDHDMIHDRPara;
            MHal_XC_CFDControl(&stUpdateHDMIPara);
            // step 4. call CFD
            Mapi_Cfd_Maserati_DLCIP_CurveMode_Set(u8Win, _stCfdDlc.bUseCustomerDlcCurve);
            MHal_XC_Seamless_FireCFD(u8Win);//call CFD
            if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
            {
                printk("%s: %d: HDMI Freeze ADL fail!\n", __func__, __LINE__);
            }
            // step 5. update CFD done flag
            _eHDMISeamlessStatus = SEAMLESS_CALCULATING;
            bSeamlessCFDDone = TRUE;
            bHDRDataChange = FALSE;
            setHDMIPacketChange(bHDRDataChange);
            //dummy for seamless (freeze ~ cfd done)
            MHal_XC_W2BYTEMSK(REG_SC_BK1F_7D_L, 0, BIT(0));
        }
    }
    else if( CFD_IS_DVI(_stCfdInit[u8Win].u8InputSource) && bHDRDataChange)
    {
            bSeamlessCFDDone = TRUE;
            bHDRDataChange = FALSE;
            MHal_XC_W2BYTEMSK(REG_SC_BK12_01_L, 0x0, BIT(11));
            bfirst = TRUE;
            setHDMIPacketChange(bHDRDataChange);
    }
    else
    {
        _eHDMISeamlessStatus = SEAMLESS_NONE;
        //make sure freeze flow will be done.
        if(bHDMIFreezeGOPDone)
        {
            bSeamlessCFDDone = TRUE;
        }
        else
        {
            bSeamlessCFDDone = FALSE;
        }
        bHDMIFreezeGOPDone = FALSE;
        bHDRDataChange = FALSE;
        bfirst = TRUE;
        setHDMIPacketChange(bHDRDataChange);
    }
    if(bSeamlessCFDDone && (MHal_XC_R2BYTEMSK(REG_SC_BK67_28_L, BIT(0))==0) && (MHal_XC_R2BYTEMSK(REG_SC_BK67_11_L, BIT(2))==0))
    {
        //in case that the freeze bit didn't release
        MHal_XC_W2BYTEMSK(REG_SC_BK12_01_L, 0x0, BIT(11));
        u8XCframeCount = (MHal_XC_R2BYTE(REG_SC_BK12_19_L) & 0x001F);
        u8Timer++;
        if(u8Timer == u8XCframeCount+u8FRCframeCount)
        {
            _eHDMISeamlessStatus = SEAMLESS_UNFREEZE;
            bSeamlessCFDDone = FALSE;
        }
    }
}

EN_KDRV_XC_HDR_SEAMLESS_MVOPSOURCE_PATH MHal_XC_Get_HDRSeamless_MVOPSOURCE_Path()
{
    return MVOPSOURCE_HDR_SEAMLESS_PATH;
}

MS_BOOL IsHdrTransfer_Characteristics(MS_U8 _u8Transfer_Characteristics)
{
    MS_BOOL bHdrType = FALSE;
    if (_u8Transfer_Characteristics == E_CFD_CFIO_TR_SMPTE2084)
    {
        // HDR10
        bHdrType = TRUE;
    }
    else if (_u8Transfer_Characteristics == E_CFD_CFIO_TR_HLG)
    {
        // HLG
        bHdrType = TRUE;
    }
    else
    {
        bHdrType = FALSE;
    }
    return bHdrType;
}

void MHal_XC_MM_Seamless_With_Freeze(void)
{
    MS_U8 u8Win = 0;
    static MS_U8 u8XCframeCount = 0;
    MS_U8 u8FRCframeCount = 5;
    static MS_BOOL bfirst = TRUE;
    static MS_BOOL bHDRDataChange = FALSE;
    static MS_BOOL bSeamlessCFDDone = FALSE;

    if(MHal_XC_GetVersion() == 1)
    {
        if((IsHdrTransfer_Characteristics(_u8LastTransfer_Characteristics) == FALSE) &&
            (IsHdrTransfer_Characteristics(MHal_XC_GetTransfer_Characteristics()) == FALSE))
        {
            _u8LastTransfer_Characteristics = MHal_XC_GetTransfer_Characteristics();
        }

        if(bfirst)
        {
            bfirst = FALSE;
            _u8LastTransfer_Characteristics = MHal_XC_GetTransfer_Characteristics();
        }
        if(_u8LastTransfer_Characteristics != MHal_XC_GetTransfer_Characteristics())
        {
            //printk("[%s,%5d] Transfer change ! Now transfer:%d  Last transfer:%d \n",__func__,__LINE__,MHal_XC_GetTransfer_Characteristics(),_u8LastTransfer_Characteristics);
            _u8LastTransfer_Characteristics = MHal_XC_GetTransfer_Characteristics();

            if((MHal_XC_R2BYTE(REG_SC_BK10_19_L)&BIT(1))!=BIT(1))
            {
                MHal_XC_W2BYTEMSK(REG_SC_BK12_01_L, BIT(11), BIT(11));
            }
            // step 1. freeze image
            _bXCFreezeDone = FALSE;
            //dummy for seamless (freeze ~ cfd done)
            MHal_XC_W2BYTEMSK(REG_SC_BK1F_7D_L, BIT(0), BIT(0));
            //_bXCFreezeDone = TRUE;

            //step 2. set variables
            _eSeamlessStatus = SEAMLESS_DATACHANGED;
            bHDRDataChange = TRUE;
            bSeamlessCFDDone = FALSE;
            u8Timer = 0;
        }

        // call CFD after GOP Capture image
        if(_bMVOPFreezeGOPDone && bHDRDataChange && !bSeamlessCFDDone)
        {
            _bXCFreezeDone = TRUE;
            MHal_XC_W2BYTEMSK(REG_SC_BK12_01_L, 0x0, BIT(11));
            // step 1. Update MM CFD info
            MS_BOOL bMmChange[2] = {FALSE, FALSE};
            MS_U16 u16ErrorCode = 0;
            bMmChange[0] = TRUE;

            if (MHal_XC_GetTransfer_Characteristics() == 16)
            {
                // HDR10
                _stCfdHdr[u8Win].u8HdrType = E_KDRV_XC_CFD_HDR_TYPE_OPEN;
                MHal_XC_W2BYTEMSK(REG_SC_BK4E_58_L, BIT(7), BIT(7));
            }
            else if (MHal_XC_GetTransfer_Characteristics() == 18)
            {
                // HLG
                _stCfdHdr[u8Win].u8HdrType = E_KDRV_XC_CFD_HDR_TYPE_HLG;
                MHal_XC_W2BYTEMSK(REG_SC_BK4E_58_L, BIT(7), BIT(7));
            }
            else
            {
                _stCfdHdr[u8Win].u8HdrType = E_KDRV_XC_CFD_HDR_TYPE_NONE;
                MHal_XC_W2BYTEMSK(REG_SC_BK4E_58_L, 0x0, BIT(7));
            }
            ST_KDRV_XC_CFD_HDR stCFDHDRPara;
            memcpy(&stCFDHDRPara,&_stCfdHdr[u8Win], sizeof(ST_KDRV_XC_CFD_HDR));
            ST_KDRV_XC_CFD_CONTROL_INFO stUpdateHDRPara;
            stUpdateHDRPara.enCtrlType = E_KDRV_XC_CFD_CTRL_SET_HDR;
            stUpdateHDRPara.pParam = &stCFDHDRPara;
            MHal_XC_CFDControl(&stUpdateHDRPara);

            ST_KDRV_XC_CFD_FIRE *pstCfdFire = &_stCfdFire[0];
            u16ErrorCode = MHal_XC_CFD_SetMainCtrl(pstCfdFire);
            if (u16ErrorCode != E_CFD_MC_ERR_NOERR)
            {
                return;
            }
            u16ErrorCode = MHal_XC_CFD_SetMmParam(pstCfdFire, &bMmChange[pstCfdFire->u8Win]);
            if (u16ErrorCode != E_CFD_MC_ERR_NOERR)
            {
                return;
            }
            // OSD
            u16ErrorCode = MHal_XC_CFD_SetOsdParam(&_stCfdOsd[u8Win]);
            if (u16ErrorCode != E_CFD_MC_ERR_NOERR)
            {
                return FALSE;
            }
           // step 2. Call CFD
            Mapi_Cfd_Maserati_DLCIP_CurveMode_Set(u8Win, _stCfdDlc.bUseCustomerDlcCurve);
            MHal_XC_Seamless_FireCFD(u8Win);
            KApi_XC_MLoad_Fire(E_CLIENT_MAIN_HDR, TRUE);

            MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC);
            if(MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) == FALSE)
            {
                printk("%s: %d: FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR) Fail!\n", __func__, __LINE__);
            }

           // step 3. update CFD done flag
           _eSeamlessStatus = SEAMLESS_CALCULATING;
           bSeamlessCFDDone = TRUE;
           bHDRDataChange = FALSE;
            //dummy for seamless (freeze ~ cfd done)
           MHal_XC_W2BYTEMSK(REG_SC_BK1F_7D_L, 0, BIT(0));
        }

    }
    else
    {
        _eSeamlessStatus = SEAMLESS_NONE;
        //make sure freeze flow will be done.
        if(_bMVOPFreezeGOPDone)
        {
            bSeamlessCFDDone = TRUE;
        }
        else
        {
            bSeamlessCFDDone = FALSE;
        }
        _bMVOPFreezeGOPDone = FALSE;
        bHDRDataChange = FALSE;
        bfirst = TRUE;
    }
    if(bSeamlessCFDDone && (MHal_XC_R2BYTEMSK(REG_SC_BK67_28_L, BIT(0))==0) && (MHal_XC_R2BYTEMSK(REG_SC_BK67_11_L, BIT(2))==0))
    {
        //in case that the freeze bit didn't release
        MHal_XC_W2BYTEMSK(REG_SC_BK12_01_L, 0x0, BIT(11));
        u8XCframeCount = (MHal_XC_R2BYTE(REG_SC_BK12_19_L) & 0x001F);
        u8Timer++;
        if(u8Timer == (u8XCframeCount + u8FRCframeCount +3))
        {
            _eSeamlessStatus = SEAMLESS_UNFREEZE;
            bSeamlessCFDDone = FALSE;
        }
    }
}

MS_U8 MHal_XC_GetVersion(void)
{
    return gstDRAMFormatInfo[0].u8Version;
}

MS_U8 MHal_XC_GetTransfer_Characteristics(void)
{
    MS_U8 u8Transfer_Characteristics = gstDRAMFormatInfo[0].HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics ;

    if( ( E_CFD_CFIO_TR_RESERVED0 == u8Transfer_Characteristics) ||
        ( E_CFD_CFIO_TR_UNSPECIFIED == u8Transfer_Characteristics) ||
        ( E_CFD_CFIO_TR_RESERVED3 == u8Transfer_Characteristics) ||
        ( E_CFD_CFIO_TR_RESERVED_START <= u8Transfer_Characteristics))
    {
        u8Transfer_Characteristics = E_CFD_CFIO_TR_BT709;
    }

    return u8Transfer_Characteristics;
}
void MHal_XC_SetPicturemodeVPQType(EN_PICTURE_MODE_VPQ_TYPE en_PICTURE_MODE_VPQ)
{
    _ePictureModeVPQType = en_PICTURE_MODE_VPQ;
}

EN_PICTURE_MODE_VPQ_TYPE MHal_XC_GetPicturemodeVPQType(void)
{
    return _ePictureModeVPQType;
}

void MHal_XC_SetOSDSettingFlag(MS_BOOL bEnable)
{
	_bCfdOsdSetting = bEnable;
}
MS_BOOL MHal_XC_GetOSDSettingFlag(void)
{
	return _bCfdOsdSetting;
}
