// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
/// @file   color_format_ip.h
/// @brief  MStar XC Driver DDI HAL Level
/// @author MStar Semiconductor Inc.
/// @attention
/// <b>(OBSOLETED) <em></em></b>
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _HAL_COLOR_FORMAT_IP_H
#define _HAL_COLOR_FORMAT_IP_H

#ifdef _HAL_COLOR_FORMAT_IP_C
#define INTERFACE
#else
#define INTERFACE                               extern
#endif

#include "color_format_input.h"
//#include "color_format_ip_write.h"
#include "mdrv_xc_st.h"

typedef struct
{
    WORD wContrast;
    WORD wRCon;
    WORD wGCon;
    WORD wBCon;
    BYTE ucSaturation;
    BYTE ucHue;
    MS_S32 sContrastRGBMatrix[3][3];
    short sVideoSatHueMatrix[3][3];
    short* psColorCorrectionMatrix; // this variable must pointer to a array[3][3]
    short* psPCsRGBMatrix; // sRGB matrix for PC, this variable must pointer to a array[3][3]
    MS_S32* psYVUtoRGBMatrix; // this variable must pointer to a array[3][3]
    BYTE bForceYUVtoRGB : 1;
    BYTE ucYUVtoRGBMatrixSel : 2;
    MS_U8 u8CscIpType;
} StruAceInfo;

typedef struct
{
    MS_BOOL bExists;
    MS_U16 u16Bank;
    MS_U32 u32Mask;
    MS_U16 u16Address;
    MS_S32 s32Val;
} ST_CSC_REGISTERS;
//transferFunctionData;

typedef struct
{
    MS_U8 mode;
    MS_U8 matrixCoefficents;
    MS_U8 ratio;
    MS_U8 debugMatrixMode;
    MS_U8 registerDebugEn;
    MS_U8 IpType;
}ST_CSC_PARAMETERS;

typedef enum
{
    E_cscEnable,
    E_ditherEnable,
    E_rOrCrSub16,
    E_gOrYSub16,
    E_bOrCbSub16,
    E_rOrCrRangeSub128,
    E_gOrYRangeSub128,
    E_bOrCbRangeSub128,
    E_rOrCrAdd16,
    E_gOrYAdd16,
    E_bOrCbAdd16,
    E_rOrCrAdd128,
    E_bOrCbAdd128,
    E_cscCoeff0,
    E_cscCoeff1,
    E_cscCoeff2,
    E_cscCoeff3,
    E_cscCoeff4,
    E_cscCoeff5,
    E_cscCoeff6,
    E_cscCoeff7,
    E_cscCoeff8,
    E_bt2020En,
    E_bt2020_cr0_11,
    E_bt2020_cr1_11,
    E_bt2020_cb0_33,
    E_bt2020_cb1_33,
    E_CSC_REGISTERS_NUM,
}  E_CSC_REGISTERS;

typedef enum
{
    E_CSC_KANO_HDR_INPUT,
    E_CSC_KANO_HDR_OUTPUT,
    E_CSC_KANO_IP2_OUTPUT,
    E_CSC_KANO_VIP_CM,
    E_CSC_KANO_CONV420,
    E_CSC_KANO_HDMIRX,
    E_CSC_INPUT,
    E_CSC_MANHATTAN_VIP_CM,
    E_CSC_OUTPUT,
    E_CSC_HDR_INPUT,
    E_CSC_HDR_OUTPUT,
    E_CSC_INPUT_SUB,
    E_CSC_OUTPUT_SUB,
    E_CSC_HDR_MAXRGB,
    E_CSC_VIP_UVC,
    E_CSC_HDR_UVC,
    E_CSC_VIP_CGAIN,
    E_CSC_HDR_OOTF,
    E_CSC_YHSL_Y2R,
    E_CSC_YHSL_R2Y,
    EN_CSC_IP_TYPE_NUM,

} EN_CSC_IP_TYPE;

typedef struct
{
    MS_BOOL bipsrc_out_pac;
    MS_BOOL b420RepEn;
    MS_BOOL bCup420RepEn;
    MS_BOOL bCup420RepEnVs;
    MS_BOOL b422to444En;
} ST_HDR_CUP;

typedef struct
{
    MS_U8 u8yAvgGain;
    MS_U8 u8yAvgOffset;
    MS_U8 u8NlmLut[32];
} ST_HDR_NLM;

typedef struct
{
    MS_U8 u8uGain;
    MS_U16 u16max;
    MS_U16 u16min;
    MS_U32 u8acGainLut[24];
} ST_HDR_AC_GAIN;

typedef struct
{
    MS_BOOL bCiDitherEn;
    MS_BOOL bBiDitherEn;
    MS_U8 u8TmoInGain;
    MS_U8 u8TmoOutGain;
    MS_U8 u8UserAlpha;
} ST_HDR_TMO_SETTINGS;

typedef struct
{
    MS_BOOL bDitherEn;
    MS_U16 u16WeightEdge;
    MS_U16 u16Weight;
} ST_HDR_ACE;

typedef struct
{
    MS_U16 u16HuntGain;
    MS_U16 u16HuntOffset;
    MS_U16 u16SatGain;
    MS_U32 u32HkHightThreshold;
    MS_U32 u32HklowThreshold;
    MS_U32 u32HkZeroThreshold;
    MS_BOOL bOutClampEn;
    MS_U16 u16UvOffset;
    MS_BOOL bNormalizeEn;
    MS_U16 u16PostOffset;
    MS_U16 u16iMaxC;

} ST_HDR_UVC;

typedef struct
{
    MS_BOOL bDitherMode;
    //MS_BOOL bNormalizeEn; moved to UVC
    //MS_U16 u16PostOffset; moved to UVC
} ST_HDR_DITHER1;

typedef struct
{
    MS_U16 u16iMaxC; // moved to UVC
    MS_U16 u16iMinC;
    MS_U32 u32iDistiCInv;
    MS_U16 u16iMinY;
    MS_U16 u16iMaxY;
    MS_U32 u32iDistiYInv;
    MS_BOOL bAlphaRound;
    MS_U8 u8OutControl;
    MS_BOOL bOutControlRound;
    MS_BOOL bLutBlendMode;
    MS_BOOL b3dLutDitherMode;
} ST_HDR_3DLUT_SETTINGS;

typedef struct
{
    MS_BOOL bDitherForceWindow;
    MS_U8 u8DitherWindow;

} ST_HDR_DITHER2;

typedef struct
{
    MS_U16 u16ChromaWeight;
} ST_HDR_B107;

typedef enum
{
    E_IP,
    E_OP,
    E_VOP,
    E_FO,
} EN_CMD_TYPE;

typedef struct
{
    MS_U8  dePQclamp_en;
    MS_U16 dePQclamp_value;

}ST_DEGAMMA_PARAMETERS_EXTENSION;

typedef struct
{
    //param for HLG TMO
    //gamma , 0xffff = 1
    MS_U32 u32TMO_HLG_POW;

    //enable for HLG OOTF function
    MS_U8 u8hlg_ootf_en;

    //param for HLG OOTF
    //gamma , 0xffff = 1
    MS_U32 u32hlg_systemGamma;

    //0x1000 = 1
    MS_U16 u16hlg_ootf_alpha;

    //0x1000 = 1
    MS_U16 u16hlg_ootf_beta;

    //gamma , 0xffff = 1
    MS_U32 u32Gamma_afterOOTF;
}StuHDR_HLG_Param_Api;

typedef struct
{
    //TMO enable/disable
    // 0: disable
    // 1: enable
    MS_U16 u16HdrTmoEn;

    //input HDR format of source
    // 2: HDR10, SMPT2084, E_12P_TMO_HDR_MODE_HDR10
    // 3: HLG, E_12P_TMO_HDR_MODE_HLG
    MS_U8 u8InputHdrMode;

    //clip of TMO source
    //in nits
    MS_U16 u16Metadata;

    //HDR TMO manual/auto mode
    // 0: auto mode, E_12P_TMO_MANUAL_DISABLE
    // 1: manual mode, E_12P_TMO_MANUAL_ENABLE
    MS_U16 u16HdrTmoManualEn;

    //HDR TMO linear
    // 0: linear, E_12P_TMO_LINEAR_DISABLE
    // 1: linear, E_12P_TMO_LINEAR_ENABLE
    MS_U16 u16HdrTmoLinearEn;

    //source 12 points input for TMO
    //in nits
    MS_U16 u16SrcNits12ptsInput[12];

    //target 12 points input for TMO
    //in nits
    MS_U16 u16TgtNits12ptsInput[12];

    //source 12 points output for TMO
    //in nits
    MS_U16 u16SrcNits12ptsOutput[12];

    //target 12 points output for TMO
    //in nits
    MS_U16 u16TgtNits12ptsOutput[12];

    //TMO process domain
    // 0: in Y domain, E_12P_TMO_DOMAIN_Y
    // 1: in RGB domain, E_12P_TMO_DOMAIN_RGB
    MS_U8  u8TmoVersion;

    //Gamma table when TMO in RGB
    MS_U32 *u32Gamma513Lut;

    //TMO 512 entries LUT
    MS_U16 *u16Tmo512Lut;

    MS_U8 u8UiHdrOff;
    //param for HLG TMO
    StuHDR_HLG_Param_Api stuHdrHlgParam;
}StuHDR_12pTmo_Param_Api;

#define MENULOAD_ENABLE

MS_U8 gamma(MS_U8 transferCharacterstics,
     MS_U16 max_lum_code,
      MS_U16 u16White, MS_U16 u16Black, MS_U8 lut_en, U32* lut_idx, MS_U16 Lut_length, MS_U8 IpType,MS_U8 inputFormatFlag,MS_U8 debugMode);
MS_U8 ootf( MS_U8 u8Mode, MS_U16 Lw, MS_U8 lut_en, U32* lut_idx, MS_U16 Lut_length, MS_U8 u8EnMode );
MS_U8 hdrRgb3dLutSettings( MS_BOOL bWriteRegister, MS_U8 u8Mode, MS_U16 *LUT_RGB3D);
MS_U8 sdrRgb3dLutSettings( MS_U8 u8Mode, MS_U16 *LUT_RGB3D );
MS_U8 deGamma(MS_U8 transferCharacterstics,MS_U16 max_lum_code,MS_U8 ratio1, MS_U16 ratio2,MS_U8 lut_en, U32* lut_idx
    , MS_U16 Lut_length, MS_U8 IpType,MS_U8 inputFormatFlag,MS_U8 debugMode,ST_DEGAMMA_PARAMETERS_EXTENSION* pt_degamma_extension);
MS_U8 CSC(MS_U8 mode,MS_U8 matrixCoefficents,MS_U8 ratio,MS_U8 debugMatrixMode,MS_U8 registerDebugEn,MS_U8 IpType);
//MS_U8 outputCSC(MS_U8 mode,MS_U8 matrixCoefficents,MS_U8 ratio,MS_U8 debugMatrixMode,MS_U8 registerDebugEn);
MS_U8 gamutMapping(MS_U8 inputColorPrimaries, MS_U8 outputMode,STU_CFD_COLORIMETRY* pt_ouput_colorimetry , MS_U8 outputColorPriamaries, MS_U8 inputFormatFlag, MS_U16 ratio, MS_U8 Lut_En, MS_S32 *Lut_idx, MS_U8 IpType, MS_U8 debugMode);

MS_U8 YCbCrClip_beforeVOP3x3(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode);
MS_U8 YCbCrClip_beforeVOP3x3_Sub(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode);

MS_U8 YSHL_R2Y_Enable(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode);
MS_U8 YSHL_YLimitIn_Enable(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode);
MS_U8 HdrEnable(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode);
MS_U8 HdrB01En(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode);
MS_U8 HdrCupB0101(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_CUP* Struct);
MS_U8 b107Write2Register(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_B107* Struct);
MS_U8 NlmWrite2Register(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_NLM* Struct);
MS_U8 HdrAcGainWrite2Register(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_AC_GAIN* Struct);
MS_U8 HdrTmoSettings(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_TMO_SETTINGS* Struct);
MS_U8 HdrAceWrite2Register(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_ACE* Struct);
MS_U8 HdrUvcWrite2Register(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_UVC* Struct);
MS_U8 hdrDither1_b501(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_DITHER1* Struct);
MS_U8 hdr3dLutSettings_b502(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_3DLUT_SETTINGS* Struct);
MS_U8 HdrRangeConvertW2Reg( MS_U8 u8Mode );
MS_U8 Hdr444to442_b601(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode);
MS_U8 hdrDither2_b602(MS_BOOL bIsOn,MS_BOOL bWriteRegister, MS_U8 u8Mode, ST_HDR_DITHER2* Struct);
MS_U32 MDrv_HDR_PQ_ali(MS_U32 u16In);
MS_U32 MDrv_HDR_Gamma_ali(MS_U16 u16In, MS_U16 u16Gamma, MS_U16 u16Ginv, MS_U16 u16A, MS_U16 u16B, MS_U16 u16S);
MS_U32 Mhal_CFD_GammaSampling(MS_U32 u32In, MS_U32 u32Gamma, MS_U16 u16A, MS_U16 u16B, MS_U16 u16S, MS_U8 u8BitDepth, MS_U8 sMode);
U32 pow01_16In32Out(MS_U16 data_base, MS_U32 data_exp);
MS_BOOL AdjustHueSatContrast(MS_BOOL bwindow,STU_CFDAPI_OSD_CONTROL *pstu_OSD_Param);
MS_U32 MDrv_HDR_DeGamma_ali(MS_U16 u16In, MS_U16 u16Gamma, MS_U16 u16Ginv, MS_U16 u16A, MS_U16 u16B, MS_U16 u16S);
MS_U32 MDrv_HDR_Pow_ali(MS_U32 data_base, MS_U16 data_exp);
void MDrv_HDR_ACAdaptiveGainGen_lavend(ST_HDR_AC_GAIN* pStruct,
                                       MS_U8 u8Dlc_Mode,
                                       MS_U16* pCurve,
                                       MS_U16 u16MaxOutNits,
                                       MS_U16 u16MaxInNits);
U32 deGammaSampling(MS_U32 u32In, MS_U32 u32Ginv, MS_U16 u16A, MS_U16 u16B, MS_U16 u16S,MS_U8 u8BitDepth,MS_U8 sMode);

void setCGainVipCsc(U8 u8MatrixCoeff);
void setUvcHdrCsc(U8 u8MatrixCoeff);
void setUvcVipCsc(U8 u8MatrixCoeff);
void setAllPqCsc(U8 u8MatrixCoeff);

#define _BIT0                                   (0x0001)
#define _BIT1                                   (0x0002)
#define _BIT2                                   (0x0004)
#define _BIT3                                   (0x0008)
#define _BIT4                                   (0x0010)
#define _BIT5                                   (0x0020)
#define _BIT6                                   (0x0040)
#define _BIT7                                   (0x0080)

U32 newPow04(MS_U32 data_base, MS_U32 data_exp);
U32 newPow32(MS_U64 data_base, MS_U32 data_exp);

#define DOVI_3DLUT_SIZE     4944 // 0:736 1:656 2:656 3:576 4:656 5:576 6:576 7:512
#define DOVI_3DLUT_S0_SIZE  736
#define DOVI_3DLUT_S1_SIZE  656
#define DOVI_3DLUT_S2_SIZE  656
#define DOVI_3DLUT_S3_SIZE  576
#define DOVI_3DLUT_S4_SIZE  656
#define DOVI_3DLUT_S5_SIZE  576
#define DOVI_3DLUT_S6_SIZE  576
#define DOVI_3DLUT_S7_SIZE  512

#define VOP2_3DLUT_SIZE       1024
#define VOP2_3DLUT_SRAM_SIZE  128

typedef struct MsHdr_3D_RegTable_t_
{
    MS_BOOL bUpdate, bEnable;
    MS_U32 u32Size;
    MS_S16 s16MinY;
    MS_S16 s16MaxY;
    MS_U32 u32InvY;
    MS_S16 s16MinC;
    MS_S16 s16MaxC;
    MS_U32 u32InvC;
    MS_U16 au16Table[DOVI_3DLUT_SIZE*3];
} MsHdr_3D_RegTable_t;

typedef enum
{
    E_GAMUT_MAPPING_VOP2,
    E_GAMUT_MAPPING_MD,
    E_GAMUT_MAPPING_VOP2_SUB_MASERATI,
    EN_GAMUT_MAPPING_IP_TYPE_NUM

} EN_GAMUT_MAPPING_IP_TYPE;

typedef enum
{
    E_MAIN_WINDOW,
    E_SUB_WINDOW,
    E_MAIN_OR_SUB_WINDOW_NUM,

} EN_MAIN_OR_SUB_WINDOW;

typedef enum
{
    E_DEGAMMA_MANHATTAN_DELTA_VOP2,
    E_DEGAMMA_MASERATI_NORMAL_VOP2,
    E_DEGAMMA_MASERATI_MD,
    EN_DEGAMMA_TYPE_IP_TYPE_NUM

} EN_DEGAMMA_TYPE;

typedef enum
{
    E_GAMMA_MANHATTAN_VOP2,
    E_GAMMA_MASERATI_MD,
    EN_GAMMA_IP_TYPE_NUM

} EN_GAMMA_TYPE;

//this should be defined here

typedef struct
{
     MS_BOOL sourceValid; // source data valid
     MS_BOOL targetValid; // target data valid
     MS_U16 sourceRx; // source Rx
     MS_U16 sourceRy; // source Ry
     MS_U16 sourceGx; // source Gx
     MS_U16 sourceGy; // source Gy
     MS_U16 sourceBx; // source Bx
     MS_U16 sourceBy; // source By
     MS_U16 sourceWx; // source Wx
     MS_U16 sourceWy; // source Wy
     MS_U16 targetRx; // target Rx
     MS_U16 targetRy; // target Ry
     MS_U16 targetGx; // target Gx
     MS_U16 targetGy; // target Gy
     MS_U16 targetBx; // target Bx
     MS_U16 targetBy; // target By
     MS_U16 targetWx; // target Wx
     MS_U16 targetWy; // target Wy
} gamutMappingDataStruct;

typedef struct
{
     MS_BOOL sValid; // source data valid
     MS_BOOL tValid; // target data valid
     MS_U8 sMode; // 0:gamma, 1:DCI-P3, 2:PQ, 3:Log
     //U16 sG; // 2.14
     //U16 sB; // 0.16
     MS_U16 sData[4]; //[0]:sG [1]:sB
     MS_U32 u32Gamma;
     MS_U32 u32GammaInv;
     MS_U16 u16Beta;
     MS_U16 u16Alpha;
     MS_U16 u16LowerBoundGain;
     MS_BOOL bExtendEn;
     MS_U8 tMode; // 0:gamma, 1:DCI-P3, 2:PQ, 3:Log
     MS_U16 tData[4];
} ST_TRANSFER_FUNCTION_DATA;

typedef enum
{
    E_OFF,
    E_ON
} EN_OnOff;

typedef enum
{
     E_CSCOFF,
     E_KBKR,
     E_YCGCO,
     E_2020CL
} EN_CscMode;

typedef enum
{
     fullRange,
     limitedRange,
     RangeListNum,
} rangeList;


typedef enum
{
    Rgb,
    Ycbcr,
    otherColorType
} colorTypeList;

#endif
