////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
/// @file   mhal_tmo.h
/// @brief  MStar TMO Driver DDI HAL Level
/// @author MStar Semiconductor Inc.
/// @attention
/// <b>(OBSOLETED) <em></em></b>
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _HAL_TMO_H
#define _HAL_TMO_H

#ifdef _HAL_TMO_C
#define INTERFACE
#else
#define INTERFACE                               extern
#endif

//=============================================================================
// Includs
//=============================================================================

//=============================================================================
// Defines & Macros
//=============================================================================
//#define printf                                  //printk
#define TMO_FUNCTION                            1
#define UI_HDR_OFF 0x40

#define REG_SC_BK18_06_L                        (0x131800 | (0x06<< 1))
#define REG_SC_BK2E_25_L                        (0x132E00 | 0x25<< 1)
#define REG_SC_BK30_01_L                        (0x133000 | (0x01<< 1))
#define REG_SC_BK30_0E_L                        (0x133000 | (0x0E<< 1))
#define REG_SC_BK4E_21_L                        (0x134E00 | 0x21<< 1)
#define REG_SC_BK4E_22_L                        (0x134E00 | 0x22<< 1)
#define REG_SC_BK4E_23_L                        (0x134E00 | 0x23<< 1)
#define REG_SC_BK4E_24_L                        (0x134E00 | 0x24<< 1)
#define REG_SC_BK4E_25_L                        (0x134E00 | 0x25<< 1)
#define REG_SC_BK4E_26_L                        (0x134E00 | 0x26<< 1)
#define REG_SC_BK4E_27_L                        (0x134E00 | 0x27<< 1)
#define REG_SC_BK4E_28_L                        (0x134E00 | 0x28<< 1)
#define REG_SC_BK5F_5A_L                        (0x135F00 | (0x5A<< 1))
#define REG_SC_BK5F_5A_H                        (0x135F00 | ((0x5A<< 1)+1))
#define REG_SC_BK5F_65_L                        (0x135F00 | (0x65<< 1))
#define REG_SC_BK5F_65_H                        (0x135F00 | ((0x65<< 1)+1))
#define REG_SC_BK7A_3A_L                        (0x137A00 | (0x3A<< 1))

#define REG_SCALER_BASE                         0x130000
#define REG_SCALER_FSC_BASE                     0x140000
#define REG_SC_BK_DLC                           0x1A
#define L_BK_DLC(_x_)                           (REG_SCALER_BASE | (REG_SC_BK_DLC << 8) | (_x_ << 1))
#define REG_ADDR_DLC_DATA_START_MAIN            L_BK_DLC(0x30)
#define REG_ADDR_DLC_DATA_EXTEND_N0_MAIN        L_BK_DLC(0x76)
#define REG_ADDR_DLC_DATA_EXTEND_16_MAIN        L_BK_DLC(0x77)
#define REG_ADDR_DLC_DATA_LSB_START_MAIN        L_BK_DLC(0x78)

#define msDlc_FunctionExit()
#define msDlc_FunctionEnter()

#define REFERENCE_METADATA_ENABLE 1
#define REFERENCE_METADATA_DISABLE 0

#define OOTF_HW_EXIST 1
#define OOTF_HW_NOT_EXIST 0

//#define MS_DLC_DEBUG


#ifdef MS_DLC_DEBUG
#ifndef DLC_DEBUG
#define DLC_DEBUG(  _fmt, _args...)    printk(_fmt, ##_args);
#endif
#else
#ifndef DLC_DEBUG
#define DLC_DEBUG(  _fmt, _args...)
#endif
#endif

#ifdef CONFIG_64BIT
#define abs(x) ({                       \
        long ret;                   \
        if (sizeof(x) == sizeof(long)) {        \
            long __x = (x);             \
            ret = (__x < 0) ? -__x : __x;       \
        } else {                    \
            int __x = (x);              \
            ret = (__x < 0) ? -__x : __x;       \
        }                       \
        ret;                        \
    })
#else
#define abs(x) ({                       \
        long long ret;                  \
        if (sizeof(x) == sizeof(long long)) {       \
            long long __x = (x);                \
            ret = (__x < 0) ? -__x : __x;       \
        } else {                    \
            int __x = (x);              \
            ret = (__x < 0) ? -__x : __x;       \
        }                       \
        ret;                        \
    })
#endif
#ifndef min
#define min(x, y) ({                \
            typeof(x) _min1 = (x);          \
            typeof(y) _min2 = (y);          \
            (void) (&_min1 == &_min2);      \
            _min1 < _min2 ? _min1 : _min2; })
#endif
#define max(x, y) ({                \
            typeof(x) _max1 = (x);          \
            typeof(y) _max2 = (y);          \
            (void) (&_max1 == &_max2);      \
            _max1 > _max2 ? _max1 : _max2; })
//=============================================================================
// Enum
//=============================================================================
typedef enum _E_12P_TMO_PANEL_DISPLAY_LUMINANCE
{
    E_12P_TMO_PANEL_DISPLAY_LUMINANCE_THRD_1 = 0xC8,
    E_12P_TMO_PANEL_DISPLAY_LUMINANCE_THRD_2 = 0x1F4,
    E_12P_TMO_PANEL_DISPLAY_LUMINANCE_THRD_3 = 0x2EE,
    E_12P_TMO_PANEL_DISPLAY_LUMINANCE_THRD_4 = 0x3E8,
}E_12P_TMO_PANEL_DISPLAY_LUMINANCE;

typedef enum _E_12P_TMO_METADATA
{
    E_12P_TMO_METADATA_HIGH_METADATA_THRD = 0x1770,
    E_12P_TMO_METADATA_VALID_MAX          = 0x270F,
}E_12P_TMO_METADATA;

typedef enum _E_12P_TMO_CURVE_ENTRIES
{
    E_12P_TMO_CURVE_ENTRIES_INDEX_START        = 0x0,
    E_12P_TMO_CURVE_ENTRIES_NUMBER             = 0xC,
    E_12P_TMO_CURVE_ENTRIES_SEARCH_INDEX_START = 0x9,
    E_12P_TMO_CURVE_ENTRIES_SEARCH_INDEX_END   = 0x1,
    E_12P_TMO_CURVE_ENTRIES_SEARCH_INDEX_INIT  = 0x0,
}E_12P_TMO_CURVE_ENTRIES;

typedef enum _E_12P_TMO_MANUAL_MODE_RATIO
{
    E_12P_TMO_MANUAL_MODE_RATIO_START = 0x15,
    E_12P_TMO_MANUAL_MODE_RATIO_END   = 0x19,
}E_12P_TMO_MANUAL_MODE_RATIO;

typedef enum _E_12P_TMO_HDR_MODE
{
    E_12P_TMO_HDR_MODE_HDR10 = 0x2,
    E_12P_TMO_HDR_MODE_HLG   = 0x3,
}E_12P_TMO_HDR_MODE;

typedef enum _E_12P_TMO_MANUAL
{
    E_12P_TMO_MANUAL_DISABLE = 0x0,
    E_12P_TMO_MANUAL_ENABLE  = 0x1,
}E_12P_TMO_MANUAL;

typedef enum _E_12P_TMO_LINEAR
{
    E_12P_TMO_LINEAR_DISABLE = 0x0,
    E_12P_TMO_LINEAR_ENABLE  = 0x1,
}E_12P_TMO_LINEAR;

typedef enum _E_12P_TMO_VERSION
{
    E_12P_TMO_VERSION_0  = 0x0,
    E_12P_TMO_VERSION_1  = 0x1,
}E_12P_TMO_VERSION;

typedef enum _E_12P_TMO_TMO_CURVE_INT_START
{
    E_12P_TMO_TMO_CURVE_INT_START_VERSION_0  = 0x0,
    E_12P_TMO_TMO_CURVE_INT_START_VERSION_1  = 0x4,
    E_12P_TMO_TMO_CURVE_INT_START_VERSION_2  = 0x4,
}E_12P_TMO_TMO_CURVE_INT_START;

#ifndef _HAL_DLC_H
//=============================================================================
// Type and Structure Declaration
//=============================================================================

#ifdef CONFIG_64BIT
            /// data type unsigned int, data length 8 byte
#define MS_U64 unsigned long
            /// data type signed int, data length 8 byte
#define MS_S64 signed long
#else
            /// data type unsigned int, data length 8 byte
#define MS_U64 unsigned long long
            /// data type signed int, data length 8 byte
#define MS_S64 signed long long
#endif


    /// data type unsigned char, data length 1 byte
#define MS_U8 unsigned char                                             // 1 byte
            /// data type unsigned short, data length 2 byte
#define MS_U16 unsigned short                                           // 2 bytes
            /// data type unsigned int, data length 4 byte
#define MS_U32 unsigned int                                             // 4 bytes
                                  // 8 bytes
            /// data type signed char, data length 1 byte
#define MS_S8 signed char                                               // 1 byte
            /// data type signed short, data length 2 byte
#define MS_S16 signed short                                             // 2 bytes
            /// data type signed int, data length 4 byte
#define MS_S32 signed int                                               // 4 bytes
                                     // 8 bytes
            /// data type float, data length 4 byte
#define MS_FLOAT float                                                  // 4 bytes
            /// data type pointer content
#define MS_VIRT size_t                                                  // 8 bytes
            /// data type hardware physical address
#define MS_PHYADDR size_t                                               // 8 bytes
            /// data type 64bit physical address
#define MS_PHY MS_U64                                                   // 8 bytes
            /// data type size_t
#define MS_SIZE size_t                                                  // 8 bytes

/// print type  MPRI_PHY
#define                             MPRI_PHY                            "%x"
/// print type  MPRI_VIRT
#define                             MPRI_VIRT                           "%tx"


/// data type null pointer
#ifdef NULL
#undef NULL
#endif
#define NULL                        0

#ifdef CONFIG_MP_PURE_SN_32BIT
#define MS_PHY64 unsigned int                                         // 32bit physical address
#else
#define MS_PHY64 unsigned long long                                   // 64bit physical address
#endif


typedef struct __attribute__((packed))
{
    MS_U8      u8ColorPrimaries;
    MS_U8      u8TransferCharacteristics;
    MS_U8      u8MatrixCoefficients;
} StuDlc_HDRMetadataMpegVUI;

// Data format follows HDR_Metadata_MpegSEI_MasteringColorVolume
typedef struct __attribute__((packed))
{
    MS_U16     display_primaries_x[3]; // x = data*0.00002    [709] {15000, 7500, 32000}
    MS_U16     display_primaries_y[3]; // y = data*0.00002    [709] {30000, 3000, 16500}
    MS_U16     white_point_x; // x = data*0.00002    [709] 15635
    MS_U16     white_point_y; // y = data*0.00002    [709] 16450
    MS_U32     max_display_mastering_luminance; // data*0.0001 nits    [600nits] 6000000
    MS_U32     min_display_mastering_luminance; // data*0.0001 nits    [0.3nits] 3000
} StuDlc_HDRMetadataMpegSEIMasteringColorVolume;

typedef struct __attribute__((packed))
{
    MS_U16 u16Smin; // 0.10
    MS_U16 u16Smed; // 0.10
    MS_U16 u16Smax; // 0.10
    MS_U16 u16Tmin; // 0.10
    MS_U16 u16Tmed; // 0.10
    MS_U16 u16Tmax; // 0.10
    MS_U16 u16MidSourceOffset;
    MS_U16 u16MidTargetOffset;
    MS_U16 u16MidSourceRatio;
    MS_U16 u16MidTargetRatio;
} StuDlc_HDRToneMappingData;

typedef struct __attribute__((packed))
{
    MS_U16 u16tRx; // target Rx
    MS_U16 u16tRy; // target Ry
    MS_U16 u16tGx; // target Gx
    MS_U16 u16tGy; // target Gy
    MS_U16 u16tBx; // target Bx
    MS_U16 u16tBy; // target By
    MS_U16 u16tWx; // target Wx
    MS_U16 u16tWy; // target Wy
} StuDlc_HDRGamutMappingData;

// Document : CEA-861.3_V16BallotDraft
typedef struct __attribute__((packed))
{
    MS_U8      u8EOTF; // 0:SDR gamma, 1:HDR gamma, 2:SMPTE ST2084, 3:Future EOTF, 4-7:Reserved
    MS_U16     u16Rx; // display primaries Rx
    MS_U16     u16Ry; // display primaries Ry
    MS_U16     u16Gx; // display primaries Gx
    MS_U16     u16Gy; // display primaries Gy
    MS_U16     u16Bx; // display primaries Bx
    MS_U16     u16By; // display primaries By
    MS_U16     u16Wx; // display primaries Wx
    MS_U16     u16Wy; // display primaries Wy
    MS_U16     u16Lmax; // max display mastering luminance
    MS_U16     u16Lmin; // min display mastering luminance
    MS_U16     u16MaxCLL; // maximum content light level
    MS_U16     u16MaxFALL; // maximum frame-average light level
} StuDlc_HDRMetadataHdmiTxInfoFrame;

// HDR use customer DLC curve.
typedef struct __attribute__((packed))
{
    MS_BOOL bFixHdrCurve;
    MS_U16 u16DlcCurveSize;
    MS_U8 *pucDlcCurve;
} StuDLC_HDRCustomerDlcCurve;

// HDR use customer color primaries.
typedef struct __attribute__((packed))
{
    MS_BOOL bCustomerEnable;
    MS_U16 u16sWx;
    MS_U16 u16sWy;
} StuDlc_HDRCustomerColorPrimaries;

typedef struct __attribute__((packed))
{
    MS_U8 PixelFormat;              // Pixel Format
    MS_U8 Colorimetry;              // Color imetry
    MS_U8 ExtendedColorimetry;      // Extended Color imetry
    MS_U8 RgbQuantizationRange;     // Rgb Quantization Range
    MS_U8 YccQuantizationRange;     // Ycc Quantization Range
    MS_U8 StaticMetadataDescriptorID; //Static Metadata Descriptor ID
} StuDlc_HDRHdmiTxAviInfoFrame;

// HDR new tone mapping parameters.
typedef struct __attribute__((packed))
{
    // TMO
    MS_U16 u16SrcMinRatio;          //default 10
    MS_U16 u16SrcMedRatio;                   //default 512
    MS_U16 u16SrcMaxRatio;          //default 990

    MS_U8 u8TgtMinFlag;                         //default 1
    MS_U16 u16TgtMin;                   //default 500
    MS_U8 u8TgtMaxFlag;                        //default 0
    MS_U16 u16TgtMax;               //default 300
    MS_U16 u16TgtMed;

    MS_U16 u16FrontSlopeMin;            //default 256
    MS_U16 u16FrontSlopeMax;            //default 512
    MS_U16 u16BackSlopeMin;         //default 128
    MS_U16 u16BackSlopeMax;         //default 256

    MS_U16 u16SceneChangeThrd;      //default 1024
    MS_U16 u16SceneChangeRatioMax;  //default 1024

    MS_U8 u8IIRRatio;                   //default 31
    MS_U8 u8TMO_TargetMode;      // default 0. 0 : keeps the value in initial function  1 : from output source
    MS_U8 u8TMO_Algorithm;                // default 0.  0: 18 level TMO algorithm, 1: 512 level TMO algorithm.
    MS_U16 u16SDRPanelGain;        //

    MS_U16 u16Smin;
    MS_U16 u16Smed;
    MS_U16 u16Smax;
    MS_U16 u16Tmin;
    MS_U16 u16Tmed;
    MS_U16 u16Tmax;

    MS_U8 u8TMO_UseIniControls;
} StuDlc_HDRNewToneMapping;

typedef struct __attribute__((packed))
{
    MS_U32 pt_output_nits_array[512];
    MS_U16 pt_input_points_array[512];
    MS_U16 u16ControlPoints;
    MS_S16 s16LastLess1IndexOutputnits;
    MS_S16 s16LastIndexM10000Outputnits;
    MS_S16 s16LastIndexM100Outputnits;
}StuHDR_TmoApi;
/*!
 *  Initial  HDR   Settings
 */
typedef struct __attribute__((packed))
{
    /// HDR Enable
    MS_BOOL bHDREnable;
    /// HDR Function Select
    MS_U16 u16HDRFunctionSelect;
    /// HDR Metadata Mpeg VUI
    StuDlc_HDRMetadataMpegVUI DLC_HDRMetadataMpegVUI;
    //HDR Tone Mapping Data
    StuDlc_HDRToneMappingData DLC_HDRToneMappingData;
    //HDR Gamut Mapping Data
    StuDlc_HDRGamutMappingData DLC_HDRGamutMappingData;
    //HDR Metadata Hdmi Tx Info Frame
    StuDlc_HDRMetadataHdmiTxInfoFrame DLC_HDRMetadataHdmiTxInfoFrame;
    // Customer DLC Curve
    StuDLC_HDRCustomerDlcCurve DLC_HDRCustomerDlcCurve;
    // Customer color primarie.
    StuDlc_HDRCustomerColorPrimaries DLC_HDRCustomerColorPrimaries;
    //HDR Hdmi Tx Avi Info Frame
    StuDlc_HDRHdmiTxAviInfoFrame DLC_HDRHdmiTxAviInfoFrame;
    // HDR metadata MPEG SEI mastering color volume
    StuDlc_HDRMetadataMpegSEIMasteringColorVolume DLC_HDRMetadataMpegSEIMasteringColorVolume;
    // New tone mapping parameters.
    StuDlc_HDRNewToneMapping DLC_HDRNewToneMappingData;
    // TMO 1D lut from .ini
    StuHDR_TmoApi  HDR_tmoapi;
} StuDlc_HDRinit;
#endif

typedef struct __attribute__((packed))
{
    //param for HLG TMO
    //gamma , 0xffff = 1
    MS_U32 u32TMO_HLG_POW;

    //enable for HLG OOTF function
    MS_U8 u8hlg_ootf_en;

    //param for HLG OOTF
    //gamma , 0xffff = 1
    MS_U32 u32hlg_systemGamma;

    //0x1000 = 1
    MS_U16 u16hlg_ootf_alpha;

    //0x1000 = 1
    MS_U16 u16hlg_ootf_beta;

    //gamma , 0xffff = 1
    MS_U32 u32Gamma_afterOOTF;
}StuHDR_HLG_Param;

typedef struct __attribute__((packed))
{
    //pointer to OOTF LUT
    MS_U32 *pu32_OOTFLUT;

    //size of OOTF LUT
    MS_U16 u32_OOTFLUT_size;
}StuHDR_OOTF_Param;

typedef struct __attribute__((packed))
{
    //TMO enable/disable
    // 0: disable
    // 1: enable
    MS_U16 u16HdrTmoEn;

    //input HDR format of source
    // 2: HDR10, SMPT2084, E_12P_TMO_HDR_MODE_HDR10
    // 3: HLG, E_12P_TMO_HDR_MODE_HLG
    MS_U8 u8InputHdrMode;

    //clip of TMO source
    //in nits
    MS_U16 u16Metadata;

    //HDR TMO manual/auto mode
    // 0: auto mode, E_12P_TMO_MANUAL_DISABLE
    // 1: manual mode, E_12P_TMO_MANUAL_ENABLE
    MS_U16 u16HdrTmoManualEn;

    //HDR TMO linear
    // 0: linear, E_12P_TMO_LINEAR_DISABLE
    // 1: linear, E_12P_TMO_LINEAR_ENABLE
    MS_U16 u16HdrTmoLinearEn;

    //source 12 points input for TMO
    //in nits
    MS_U16 u16SrcNits12ptsInput[12];

    //target 12 points input for TMO
    //in nits
    MS_U16 u16TgtNits12ptsInput[12];

    //source 12 points output for TMO
    //in nits
    MS_U16 u16SrcNits12ptsOutput[12];

    //target 12 points output for TMO
    //in nits
    MS_U16 u16TgtNits12ptsOutput[12];

    //TMO process domain
    // 0: in Y domain, E_12P_TMO_VERSION_0
    // 1: in RGB domain, E_12P_TMO_VERSION_1
    MS_U8  u8TmoVersion;

    //Gamma table when TMO in RGB
    MS_U32 *u32Gamma513Lut;

    //TMO 512 entries LUT
    MS_U16 *u16Tmo512Lut;

    //param for HLG TMO
    StuHDR_HLG_Param stuHdrHlgParam;

    //param for OOTF
    StuHDR_OOTF_Param stuHdrOotfParam;
}StuHDR_12pTmo_Param;

//-------------------------------------------------------------------------------------------------
//  Software Data Type
//-------------------------------------------------------------------------------------------------

/// definition for MS_BOOL
#define MS_BOOL unsigned char
/// definition for VOID
#define VOID void
/// definition for FILEID
#define FILEID MS_S32


#if !defined(TRUE) && !defined(FALSE)
/// definition for TRUE
#define TRUE                        1
/// definition for FALSE
#define FALSE                       0
#endif

#ifndef true
/// definition for true
#define true                        1
/// definition for false
#define false                       0


#if !defined(TRUE) && !defined(FALSE)
/// definition for TRUE
#define TRUE                        1
/// definition for FALSE
#define FALSE                       0
#endif
#endif


#if !defined(ENABLE) && !defined(DISABLE)
/// definition for ENABLE
#define ENABLE                      1
/// definition for DISABLE
#define DISABLE                     0
#endif


#define BYTE                                    MS_U8
#define WORD                                    MS_U16
#define DWORD                                   MS_U32

//=============================================================================
// HAL Driver Function
//=============================================================================
INTERFACE MS_BOOL MHal_TMO_TMOinit(void);
INTERFACE MS_BOOL MHal_TMO_TMOSet(MS_U16 *u16TMOData , MS_U16 u16TMOSize);
INTERFACE void MHal_TMO_TmoHandler(MS_BOOL bWindow);
INTERFACE void MHal_TMO_WriteTmoCurve(MS_BOOL bWindow);


#undef INTERFACE
#endif //_HAL_TMO_H
