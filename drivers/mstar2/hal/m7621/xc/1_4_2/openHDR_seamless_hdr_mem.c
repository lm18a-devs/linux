//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//////////////////////////////////////////////////////////////////////////////////////////////////
//
// @file   hdr_share_mem.c
// @brief  XC KMD Driver Interface
// @author MStar Semiconductor Inc.
//////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(CONFIG_MIPS)
#elif defined(CONFIG_ARM) || defined(CONFIG_ARM64)
#include <asm/io.h>
#include <asm/cacheflush.h>
#endif
#include <linux/kernel.h>
#include "mdrv_mstypes.h"
#include "mhal_dynamicscaling.h"
#include "openHDR_seamless_hdr_mem.h"

#define DOLBY_OTT_CACHED_BUFFER                 1

extern void Chip_Flush_Cache_Range(unsigned long u32Addr, unsigned long u32Size); //Clean & Invalid L1/L2 cache
extern void Chip_Inv_Cache_Range(unsigned long u32Addr, unsigned long u32Size);  //Invalid L1/L2 cache
extern void cfd_Prepare_DS(void);
extern MS_U8 u8tmpCurrentIndex;
extern MS_U8 *_pu8XVYCCAddr_Cached;
MS_BOOL bJump_openHDR_seamless=FALSE;


static MS_BOOL _parseMetadata(MS_U8 *pu8Data, ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info *pstFormatInfo);


void initOpenHDRSeamlessMem(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    memset((void *)pstHDRShareMem, 0, sizeof(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX));
    pstHDRShareMem->u16Version = OPENHDR_SEAMLESS_MEM_VER;
    pstHDRShareMem->u16CommInfoSize = OPENHDR_SEAMLESS_COMMON_INFO_SIZE;
}

void flushOpenHDRSeamlessMemPtr(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    pstHDRShareMem->u32RdPtr = pstHDRShareMem->u32WdPtr = 0;
}

MS_BOOL isOpenHDRSeamlessInfoEntryEmpty(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    return (pstHDRShareMem->u32RdPtr == pstHDRShareMem->u32WdPtr);
}

MS_BOOL isOpenHDRSeamlessInfoEntryFull(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    MS_U32 u32NextWPtr = pstHDRShareMem->u32WdPtr + 1;
    if (u32NextWPtr >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
        u32NextWPtr = 0;

    if (u32NextWPtr == pstHDRShareMem->u32RdPtr)
        return TRUE; // Queue is full
    else
        return FALSE;
}

MS_U32 getOpenHDRSeamlessMemRPtr(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    return pstHDRShareMem->u32RdPtr;
}

MS_BOOL incOpenHDRSeamlessMemRPtr(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem,MS_U16 u16Skip)
{
    if (pstHDRShareMem->u32RdPtr == pstHDRShareMem->u32WdPtr)
        return FALSE; // Queue is empty
    MS_BOOL frame_count_protect = FALSE;
    if(pstHDRShareMem->u32RdPtr > pstHDRShareMem->u32WdPtr)
    {
        if(((pstHDRShareMem->u32WdPtr+OPENHDR_SEAMLESS_MEM_ENTRY_NUM)-pstHDRShareMem->u32RdPtr)>OPENHDR_SEAMLESS_VDEC_REORDER_NUM)
            frame_count_protect = TRUE;
    }else
    {
        if((pstHDRShareMem->u32WdPtr-pstHDRShareMem->u32RdPtr)>OPENHDR_SEAMLESS_VDEC_REORDER_NUM)
            frame_count_protect = TRUE;
    }

    if(u16Skip && u8tmpCurrentIndex && frame_count_protect && !bJump_openHDR_seamless)
    {
        if((u8tmpCurrentIndex-OPENHDR_SEAMLESS_VDEC_REORDER_NUM)<0)
            pstHDRShareMem->u32RdPtr=u8tmpCurrentIndex+OPENHDR_SEAMLESS_MEM_ENTRY_NUM-OPENHDR_SEAMLESS_VDEC_REORDER_NUM;
        else
            pstHDRShareMem->u32RdPtr=u8tmpCurrentIndex-OPENHDR_SEAMLESS_VDEC_REORDER_NUM;
        bJump_openHDR_seamless =TRUE;
    }
    else
        ++pstHDRShareMem->u32RdPtr;
    if (pstHDRShareMem->u32RdPtr >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
        pstHDRShareMem->u32RdPtr = 0;

    return TRUE;
}

MS_U32 getOpenHDRSeamlessMemWPtr(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    return pstHDRShareMem->u32WdPtr;
}

MS_BOOL incOpenHDRSeamlessMemWPtr(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    MS_U32 u32NextWPtr = pstHDRShareMem->u32WdPtr + 1;
    if (u32NextWPtr >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
        u32NextWPtr = 0;

    if (u32NextWPtr == pstHDRShareMem->u32RdPtr)
        return FALSE; // Queue is full

    pstHDRShareMem->u32WdPtr = u32NextWPtr;

    return TRUE;
}

MS_BOOL isOpenHDRSeamlessCFDDone(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem, MS_U32 u32Idx)
{
    if (u32Idx >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
    {
        printk("[ERROR] %s invalid idx %d\n", __FUNCTION__, u32Idx);
        return FALSE;
    }

    if (pstHDRShareMem->stEntries[u32Idx].u16CFDReady)
        return TRUE;
    else
        return FALSE;
}

MS_U16 getOpenHDRSeamlessMDMiuSel(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    return pstHDRShareMem->u16MDMiuSel;
}

MS_U16 getOpenHDRSeamlessLutMiuSel(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem)
{
    return pstHDRShareMem->u16LutMiuSel;
}

void setOpenHDRSeamlessInfoEntry(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem, MS_U32 u32Idx, ST_KDRV_XC_HDR_OPEN_SEAMLESS_INFO_ENTRY *pstHDREntry)
{
    if (u32Idx >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
    {
        printk("[ERROR] %s invalid idx %d\n", __FUNCTION__, u32Idx);
        return;
    }

    pstHDRShareMem->stEntries[u32Idx].u16CFDReady = pstHDREntry->u16CFDReady;
    pstHDRShareMem->stEntries[u32Idx].u16Entry_Skip = pstHDREntry->u16Entry_Skip;
    pstHDRShareMem->stEntries[u32Idx].u32InputMDAddr = pstHDREntry->u32InputMDAddr;
    pstHDRShareMem->stEntries[u32Idx].u32HDRRegsetAddr = pstHDREntry->u32HDRRegsetAddr;
    pstHDRShareMem->stEntries[u32Idx].u32HDRRegsetSize = pstHDREntry->u32HDRRegsetSize;
    pstHDRShareMem->stEntries[u32Idx].u32HDRLutAddr = pstHDREntry->u32HDRLutAddr;
    pstHDRShareMem->stEntries[u32Idx].u32HDRLutSize = pstHDREntry->u32HDRLutSize;

    //pstHDRShareMem->u64TaskStatus ??
    //pstHDRShareMem->u64Reserved |= (((MS_U64)0x1) << u32Idx);
}

MS_BOOL getOpenHDRSeamlessInfoEntry(ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX* pstHDRShareMem, MS_U32 u32Idx, ST_KDRV_XC_HDR_OPEN_SEAMLESS_INFO_ENTRY *pstHDREntry)
{
    if (u32Idx >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
    {
        printk("[ERROR] %s invalid idx %d\n", __FUNCTION__, u32Idx);
        return FALSE;
    }

    pstHDREntry->u16CFDReady = pstHDRShareMem->stEntries[u32Idx].u16CFDReady;
    pstHDREntry->u16Entry_Skip = pstHDRShareMem->stEntries[u32Idx].u16Entry_Skip;
    pstHDREntry->u32InputMDAddr = pstHDRShareMem->stEntries[u32Idx].u32InputMDAddr;
    pstHDREntry->u32HDRRegsetAddr = pstHDRShareMem->stEntries[u32Idx].u32HDRRegsetAddr;
    pstHDREntry->u32HDRRegsetSize = pstHDRShareMem->stEntries[u32Idx].u32HDRRegsetSize;
    pstHDREntry->u32HDRLutAddr = pstHDRShareMem->stEntries[u32Idx].u32HDRLutAddr;
    pstHDREntry->u32HDRLutSize = pstHDRShareMem->stEntries[u32Idx].u32HDRLutSize;

    return TRUE;
}

MS_U32 getOpenHDRSeamlessInputMDAddr(MS_U32 u32ShareMemBaseAddr, MS_U32 u32Idx)
{
    MS_U32 u32RetAddr = NULL;

    if (u32Idx >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
    {
        printk("[ERROR] %s invalid idx %d\n", __FUNCTION__, u32Idx);
        return u32RetAddr;
    }

    u32RetAddr = (u32ShareMemBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + u32Idx * OPENHDR_SEAMLESS_INPUT_MD_SIZE);

    return u32RetAddr;
}

MS_U32 geOpenHDRSeamlessRegsetAddr(MS_U32 u32ShareMemBaseAddr, MS_U32 u32Idx)
{
    MS_U32 u32RetAddr = NULL;

    if (u32Idx >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
    {
        printk("[ERROR] %s invalid idx %d\n", __FUNCTION__, u32Idx);
        return u32RetAddr;
    }

    u32RetAddr = (u32ShareMemBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE + u32Idx * OPENHDR_SEAMLESS_OUTPUT_REG_SIZE);

    return u32RetAddr;
}

MS_U32 getOpenHDRSeamlessLutAddr(MS_U32 u32ShareMemBaseAddr, MS_U32 u32Idx)
{
    MS_U32 u32RetAddr = NULL;

    if (u32Idx >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
    {
        printk("[ERROR] %s invalid idx %d\n", __FUNCTION__, u32Idx);
        return u32RetAddr;
    }

    if (u32Idx == 0)
    {
        u32RetAddr = u32ShareMemBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE + OPENHDR_SEAMLESS_TOTAL_REGSET_SIZE;
    }
    else
    {
        u32RetAddr = u32ShareMemBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE + OPENHDR_SEAMLESS_TOTAL_REGSET_SIZE + OPENHDR_SEAMLESS_3DLUT_NUM * BYTE_PER_WORD + (u32Idx - 1) * OPENHDR_SEAMLESS_TMO_NUM * BYTE_PER_WORD;
    }

    return u32RetAddr;
}

MS_U32 getOpenHDRSeamlessLutSize(MS_U32 u32Idx)
{
    MS_U32 u32RetSize = 0;

    if (u32Idx >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
    {
        printk("[ERROR] %s invalid idx %d\n", __FUNCTION__, u32Idx);
        return u32RetSize;
    }

    u32RetSize = OPENHDR_SEAMLESS_3DLUT_NUM * BYTE_PER_WORD;

    return u32RetSize;
}

MS_U32 getOpenHDRSeamlessXVYCCSize(MS_U32 u32Idx)
{
    MS_U32 u32RetSize = 0;

    if (u32Idx >= OPENHDR_SEAMLESS_MEM_ENTRY_NUM)
    {
        printk("[ERROR] %s invalid idx %d\n", __FUNCTION__, u32Idx);
        return u32RetSize;
    }

    u32RetSize = OPENHDR_SEAMLESS_XVYCC_NUM * BYTE_PER_WORD;

    return u32RetSize;
}

MS_BOOL getOpenHDRSeamlessInputMDFormatInfo(MS_U8 *pu8Data, ST_KDRV_XC_HDR_OPEN_SEAMLESS_INPUT_MD_FORMAT *pstFormatInfo)
{
    if (pu8Data != NULL)
    {
        pstFormatInfo->u8Version = *(pu8Data);
        pu8Data += 1;
        pstFormatInfo->u8CurrentIndex = *(pu8Data);
        pu8Data += 1;
        pstFormatInfo->u32HDRMetaLength = (*pu8Data) | (*(pu8Data + 1) << 8) | (*(pu8Data + 2) << 16) | (*(pu8Data + 3) << 24);
        pu8Data += 4;
        pstFormatInfo->u32HDRMetaAddr = (*pu8Data) | (*(pu8Data + 1) << 8) | (*(pu8Data + 2) << 16) | (*(pu8Data + 3) << 24);
        pu8Data += 4;
        pstFormatInfo->u8HDRMiuNo = *(pu8Data);
        pu8Data += 1;
        pstFormatInfo->bEnableDM = *(pu8Data);
        pu8Data += 1;
        pstFormatInfo->u32xvyccSize = (*pu8Data) | (*(pu8Data + 1) << 8) | (*(pu8Data + 2) << 16) | (*(pu8Data + 3) << 24);
        pu8Data += 4;
        pstFormatInfo->u32xvyccAddr = (*pu8Data) | (*(pu8Data + 1) << 8) | (*(pu8Data + 2) << 16) | (*(pu8Data + 3) << 24);
        pu8Data += 4;
        pstFormatInfo->u8TaskID = *(pu8Data);
        pu8Data += 1;

        pstFormatInfo->stUltraBlackWhite.u8Eable = *(pu8Data);
        pu8Data += 1;
        pstFormatInfo->stUltraBlackWhite.u8Black_level = *(pu8Data);
        pu8Data += 1;
        pstFormatInfo->stUltraBlackWhite.u8White_level = *(pu8Data);
        pu8Data += 1;
        pstFormatInfo->stUltraBlackWhite.u8HWdataPath = *(pu8Data);
        pu8Data += 1;
        pstFormatInfo->stUltraBlackWhite.u8IsFullRange = *(pu8Data);
        pu8Data += 1;

        pstFormatInfo->u8DLCCurveMode = *(pu8Data);
        pu8Data += 1;
    }
    else
    {
        return FALSE;
    }
    return TRUE;
}

MS_BOOL setOpenHDRSeamlessInputMDFormatInfo(MS_U8 *pu8Data, ST_KDRV_XC_HDR_OPEN_SEAMLESS_INPUT_MD_FORMAT pstFormatInfo)
{
    if (pu8Data != NULL)
    {
        *(pu8Data) = pstFormatInfo.u8Version;
        pu8Data += 1;
         *(pu8Data) = pstFormatInfo.u8CurrentIndex;
        pu8Data += 1;

        *(pu8Data) = (pstFormatInfo.u32HDRMetaLength & 0x000000FF);
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32HDRMetaLength & 0x0000FF00)>>8;
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32HDRMetaLength & 0x00FF0000)>>16;
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32HDRMetaLength & 0xFF000000)>>24;
        pu8Data += 1;

        *(pu8Data) = (pstFormatInfo.u32HDRMetaAddr & 0x000000FF);
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32HDRMetaAddr & 0x0000FF00)>>8;
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32HDRMetaAddr & 0x00FF0000)>>16;
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32HDRMetaAddr & 0xFF000000)>>24;
        pu8Data += 1;

        *(pu8Data) = pstFormatInfo.u8HDRMiuNo;
        pu8Data += 1;
        *(pu8Data) = pstFormatInfo.bEnableDM;
        pu8Data += 1;

        *(pu8Data) = (pstFormatInfo.u32xvyccSize & 0x000000FF);
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32xvyccSize & 0x0000FF00)>>8;
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32xvyccSize & 0x00FF0000)>>16;
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32xvyccSize & 0xFF000000)>>24;
        pu8Data += 1;

        *(pu8Data) = (pstFormatInfo.u32xvyccAddr & 0x000000FF);
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32xvyccAddr & 0x0000FF00)>>8;
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32xvyccAddr & 0x00FF0000)>>16;
        pu8Data += 1;
        *(pu8Data) = (pstFormatInfo.u32xvyccAddr & 0xFF000000)>>24;
        pu8Data += 1;

        *(pu8Data) = pstFormatInfo.u8TaskID;
        pu8Data += 1;
        *(pu8Data) = pstFormatInfo.stUltraBlackWhite.u8Eable;
        pu8Data += 1;
        *(pu8Data) = pstFormatInfo.stUltraBlackWhite.u8Black_level;
        pu8Data += 1;
        *(pu8Data) = pstFormatInfo.stUltraBlackWhite.u8White_level;
        pu8Data += 1;
        *(pu8Data) = pstFormatInfo.stUltraBlackWhite.u8HWdataPath;
        pu8Data += 1;
        *(pu8Data) = pstFormatInfo.stUltraBlackWhite.u8IsFullRange;
        pu8Data += 1;
        *(pu8Data) = pstFormatInfo.u8DLCCurveMode;
        pu8Data += 1;
    }
    else
    {
        return FALSE;
    }
    return TRUE;
}


MS_U64 getOpenHDRSeamlessVersion(void)
{
    return OPENHDR_SEAMLESS_MEM_VER;
}

static MS_BOOL _parseMetadata(MS_U8 *pu8Data, ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info *pstFormatInfo)
{
    pstFormatInfo->u8Version = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8CurrentIndex = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8InputFormat = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8InputDataFormat = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8Video_Full_Range_Flag = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.bVUIValid = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8Colour_primaries = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u8Matrix_Coeffs = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.bSEIValid = *(pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[0] = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[1] = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[2] = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[0] = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[1] = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[2] = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16White_point_x = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16White_point_y = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u32Master_Panel_Max_Luminance = (*pu8Data) | (*(pu8Data + 1) << 8) | (*(pu8Data + 2) << 16) | (*(pu8Data + 3) << 24);
    pu8Data += 4;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u32Master_Panel_Min_Luminance = (*pu8Data) | (*(pu8Data + 1) << 8) | (*(pu8Data + 2) << 16) | (*(pu8Data + 3) << 24);
    pu8Data += 4;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.bContentLightLevelEnabled = (*pu8Data);
    pu8Data += 1;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16maxContentLightLevel = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;
    pstFormatInfo->HDRMemFormat.stHDRMemFormatCFD.u16maxPicAverageLightLevel = (*pu8Data) | (*(pu8Data + 1) << 8);
    pu8Data += 2;

    return TRUE;
}

void MHal_XC_CalculateFrameHDRInfo_forOpenHDRSeamless( MS_U8 u8Win,MS_U32 u32RPtr, ST_KDRV_XC_HDR_OPEN_SEAMLESS_INFO_ENTRY *pstEntry, MS_U8 *_pu8InputMDAddr, MS_PHY _phyMDAddr,MS_U8 *_pu8XVYCCAddr, MS_U8 *_pu8LutsAddr, MS_U8 *_pu8RegSetAddr, ST_KDRV_XC_HDR_OPEN_SEAMLESS_MEMORY_FORMAT_EX *_pstOpenHDRSeamlessHDRShareMem,
                                   ST_KDRV_XC_SHARE_MEMORY_INFO _stShareMemInfo, MS_U8 *_pu8RegSetAddr_Cached, MS_U8 *_pu8LutsAddr_Cached,MS_U8 *_pu8XVYCCAddr_Cached, MS_U8 *_pu8InputMDAddr_Cached, MS_BOOL UserMode)
{
#if DOLBY_OTT_CACHED_BUFFER
    unsigned long ulFlushAddr_IPM[3]= {0,0,0};
    unsigned long ulFlushLen_IPM[3]= {0,0,0};
    unsigned long ulFlushAddr_OPM[3]= {0,0,0};
    unsigned long ulFlushLen_OPM[3]= {0,0,0};
#endif
    do
    {
        MS_BOOL bRetVal = FALSE;
        // Get InputMD data
        cfd_Prepare_DS();

        MS_U16 u16MDMiuSel = getOpenHDRSeamlessMDMiuSel(_pstOpenHDRSeamlessHDRShareMem);
        MS_U32 u32InputMDAddr = pstEntry->u32InputMDAddr;
        MS_U32 u32Offset = 0;

        if (MHal_XC_GetMiuOffset(u16MDMiuSel, &u32Offset) != TRUE)
        {
            printk("GetMiuOffset u16MDMiuSel: %d fail.\n", u16MDMiuSel);
            continue;
        }

        if (u32InputMDAddr != 0)
        {
            // get metadata
            MS_U8 *pu8InputMDData = NULL;
            ST_KDRV_XC_HDR_OPEN_SEAMLESS_INPUT_MD_FORMAT stFormatInfo;
            ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info stMetaData;

            pu8InputMDData = ((MS_U8 *)_pstOpenHDRSeamlessHDRShareMem) + (u32InputMDAddr - _stShareMemInfo.phyBaseAddr);

            if (pu8InputMDData == NULL)
            {
                printk("metadata mmap pa fail.\n");
                continue;
            }
            bRetVal = getOpenHDRSeamlessInputMDFormatInfo(pu8InputMDData, &stFormatInfo); //get data md
            if (bRetVal == TRUE)
            {
                    // step 1. parse metadata
                    MS_U8 *pu8CompData = NULL;

                    if (stFormatInfo.u32HDRMetaLength == 0)
                    {
                        printk("%d: u32ComposerLength fail.\n", __LINE__);
                        continue;
                    }

#if DOLBY_OTT_CACHED_BUFFER
                    pu8CompData = _pu8InputMDAddr_Cached+ (stFormatInfo.u32HDRMetaAddr - _phyMDAddr);
                    Chip_Inv_Cache_Range(pu8CompData, stFormatInfo.u32HDRMetaLength);
#else
                    pu8CompData = _pu8InputMDAddr + (stFormatInfo.u32HDRMetaAddr - _phyMDAddr);
#endif

                    if (pu8CompData == NULL)
                    {
                        printk("openHDR/SDR mmap pa fail.\n");
                        continue;
                    }

                _parseMetadata(pu8CompData,&stMetaData);//get metadata

if(0)
{

printk(KERN_CRIT"version=0x%x 0x%x 0x%x 0x%x 0x%x , VUIenable=0x%x 0x%x 0x%x 0x%x [%s][%05d] \n",
stMetaData.u8Version,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u8CurrentIndex,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u8InputFormat,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u8InputDataFormat,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u8Video_Full_Range_Flag,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.bVUIValid,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u8Colour_primaries,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u8Matrix_Coeffs,__FUNCTION__, __LINE__);

printk(KERN_CRIT"bSEIValid=0x%x 0x%x 0x%x 0x%x 0x%x , u16Display_Primaries_y[1]=0x%x 0x%x 0x%x 0x%x [%s][%05d] \n",
stMetaData.HDRMemFormat.stHDRMemFormatCFD.bSEIValid,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[0],
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[1],
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_x[2],
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[0],
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[1],
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16Display_Primaries_y[2],
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16White_point_x,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16White_point_y,__FUNCTION__, __LINE__);


printk(KERN_CRIT"u32Master_Panel_Max_Luminance=0x%x 0x%x 0x%x 0x%x 0x%x  [%s][%05d] \n",
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u32Master_Panel_Max_Luminance,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u32Master_Panel_Min_Luminance,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.bContentLightLevelEnabled,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16maxContentLightLevel,
stMetaData.HDRMemFormat.stHDRMemFormatCFD.u16maxPicAverageLightLevel,__FUNCTION__, __LINE__);
}

                MHal_XC_CFD_DepositeMMParam(&stMetaData,u8Win);//update metadata to xc

                //step2. set xvycc autodownload setting
                MS_U32 u32XVYCCAddr = 0; // XVYCC PA
                MS_U32 u32XVYCCSize = 0;
                MS_U32 u32XVYCCoffset = 0; // MIU offset
                MS_U8 *pu8XVYCCAddr = NULL; // XVYCC VA

                //XVYCC MIU sel == HDR MIU sel
                if (MHal_XC_GetMiuOffset(stFormatInfo.u8HDRMiuNo, &u32XVYCCoffset) != TRUE)
                {
                    printk("%d: MHal_XC_GetMiuOffset fail.\n", __LINE__);
                    continue;
                }
                u32XVYCCAddr = stFormatInfo.u32xvyccAddr;
                u32XVYCCSize = getOpenHDRSeamlessXVYCCSize(u32RPtr);

if (MHal_XC_R2BYTEMSK(REG_SC_BK01_42_L, BIT(0)) == BIT(0))
printk(KERN_CRIT"!!!!!!!!point XVYCC PA =%x [%s][%05d] \n",u32XVYCCAddr,__FUNCTION__, __LINE__);

#if DOLBY_OTT_CACHED_BUFFER
                pu8XVYCCAddr = (MS_U8 *)(_pu8XVYCCAddr_Cached+ (u32XVYCCAddr - (_stShareMemInfo.phyBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE + OPENHDR_SEAMLESS_TOTAL_REGSET_SIZE + OPENHDR_SEAMLESS_TOTAL_LUT_DATA_SIZE)));
                ulFlushAddr_IPM[2]=pu8XVYCCAddr;
                ulFlushLen_IPM[2]=u32XVYCCSize;
#else
                pu8XVYCCAddr = (MS_U8 *)(_pu8XVYCCAddr + (u32XVYCCAddr - (_stShareMemInfo.phyBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE + OPENHDR_SEAMLESS_TOTAL_REGSET_SIZE + OPENHDR_SEAMLESS_TOTAL_LUT_DATA_SIZE)));
#endif
                //set autodownload para.
                bRetVal = MHal_XC_ConfigXVYCCAutoDownloadStoredInfo((MS_PHY)(u32XVYCCAddr + u32XVYCCoffset), pu8XVYCCAddr, u32XVYCCSize);
                if (bRetVal == FALSE)
                {
                    printk("MHal_XC_ConfigAutoDownload E_KDRV_XC_AUTODOWNLOAD_CLIENT_XVYCC fail.\n");
                    continue;
                }
            }

            if (bRetVal == TRUE)
            {
                MS_U32 u32RegsetAddr = 0;
                MS_U16 u16LutMiuSel = 0;
                KHAL_DS_STORED_ADDR_INFO stDSStoredAddr;
                MS_U32 u32LutAddr = 0;
                MS_U32 u32LutSize = 0;
                MS_U8 *pu8LutAddr = NULL;

                if (stFormatInfo.u32HDRMetaAddr == 0)
                {
                    printk("%d: addr fail.\n", __LINE__);
                    continue;
                }

                OPENHDR_SDR_KDBG("0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n", stFormatInfo.u8CurrentIndex, stFormatInfo.u32HDRMetaLength, stFormatInfo.u32HDRMetaAddr,
                        stFormatInfo.u8HDRMiuNo, stFormatInfo.bEnableDM, stFormatInfo.u8TaskID);
                // get stored regset addr
                u32RegsetAddr = pstEntry->u32HDRRegsetAddr;
                if (u32RegsetAddr == 0)
                {
                    printk("%d: u32HDRRegsetAddr fail.\n", __LINE__);
                    continue;
                }

                u16LutMiuSel = getOpenHDRSeamlessLutMiuSel(_pstOpenHDRSeamlessHDRShareMem);
                if (MHal_XC_GetMiuOffset(u16LutMiuSel, &u32Offset) != TRUE)
                {
                    printk("%d: MHal_XC_GetMiuOffset fail.\n", __LINE__);
                    continue;
                }
                // set stored regset addr into DS(IPM)
                memset(&stDSStoredAddr, 0, sizeof(KHAL_DS_STORED_ADDR_INFO));
                stDSStoredAddr.stIPMAddr.phyDSAddr = (MS_PHY)(u32RegsetAddr + u32Offset);
                stDSStoredAddr.stIPMAddr.u32DSSize = HDR_OUTPUT_REG_SIZE/2;
#if DOLBY_OTT_CACHED_BUFFER
                stDSStoredAddr.stIPMAddr.virtDSAddr = (MS_VIRT)(_pu8RegSetAddr_Cached+ (u32RegsetAddr - (_stShareMemInfo.phyBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE)));
                ulFlushAddr_IPM[0]=stDSStoredAddr.stIPMAddr.virtDSAddr;
                ulFlushLen_IPM[0]=stDSStoredAddr.stIPMAddr.u32DSSize;
//printk(KERN_CRIT"!!!!!!!!point _pu8RegSetAddr_Cached =%x u32RegsetAddr=%x (...)=%x  [%s][%05d] \n",_pu8RegSetAddr_Cached,u32RegsetAddr,(_stShareMemInfo.phyBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_INPUT_MD_SIZE),__FUNCTION__, __LINE__);


#else
                stDSStoredAddr.stIPMAddr.virtDSAddr = (MS_VIRT)(_pu8RegSetAddr + (u32RegsetAddr - (_stShareMemInfo.phyBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE)));
#endif

//printk(KERN_CRIT"!!!!!!!!point stDSStoredAddr.stIPMAddr.phyDSAddr pa =%x stDSStoredAddr.stIPMAddr.virtDSAddr  va=%x  [%s][%05d] \n",stDSStoredAddr.stIPMAddr.phyDSAddr,stDSStoredAddr.stIPMAddr.virtDSAddr,__FUNCTION__, __LINE__);


                // set stored regset addr into DS(OPM)
                stDSStoredAddr.stOPMAddr.phyDSAddr = (MS_PHY)(stDSStoredAddr.stIPMAddr.phyDSAddr + HDR_OUTPUT_REG_SIZE/2);
                stDSStoredAddr.stOPMAddr.u32DSSize = HDR_OUTPUT_REG_SIZE/2;
#if DOLBY_OTT_CACHED_BUFFER
                stDSStoredAddr.stOPMAddr.virtDSAddr = (MS_VIRT)(stDSStoredAddr.stIPMAddr.virtDSAddr + HDR_OUTPUT_REG_SIZE/2);
                ulFlushAddr_OPM[0]=stDSStoredAddr.stOPMAddr.virtDSAddr;
                ulFlushLen_OPM[0]=stDSStoredAddr.stOPMAddr.u32DSSize;
#else
                stDSStoredAddr.stOPMAddr.virtDSAddr = (MS_VIRT)(stDSStoredAddr.stIPMAddr.virtDSAddr + HDR_OUTPUT_REG_SIZE/2);
#endif
                KHal_SC_Set_DS_StoredAddr(&stDSStoredAddr);

                // get data LUT addr
                u32LutAddr = pstEntry->u32HDRLutAddr;
                u32LutSize = getOpenHDRSeamlessLutSize(u32RPtr);

#if DOLBY_OTT_CACHED_BUFFER
                pu8LutAddr = (MS_U8 *)(_pu8LutsAddr_Cached+ (u32LutAddr - (_stShareMemInfo.phyBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE + OPENHDR_SEAMLESS_TOTAL_REGSET_SIZE)));
                ulFlushAddr_IPM[1]=pu8LutAddr;
                ulFlushLen_IPM[1]=u32LutSize;
#else
                pu8LutAddr = (MS_U8 *)(_pu8LutsAddr + (u32LutAddr - (_stShareMemInfo.phyBaseAddr + OPENHDR_SEAMLESS_MEM_COMMON_ENTRY_SIZE + OPENHDR_SEAMLESS_TOTAL_INPUT_MD_SIZE + OPENHDR_SEAMLESS_TOTAL_REGSET_SIZE)));
#endif
                //set autodownload para.
                bRetVal = MHal_XC_ConfigHDRAutoDownloadStoredInfo((MS_PHY)(u32LutAddr + u32Offset), pu8LutAddr, u32LutSize);
                if (bRetVal == FALSE)
                {
                    printk("MHal_XC_ConfigAutoDownload E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR fail.\n");
                    continue;
                }

                //call CFD

#if 0
                // update composer
                if (stFormatInfo.u32ComposerAddr != 0)
                {
                    MS_U8 *pu8CompData = NULL;

                    if (stFormatInfo.u32ComposerLength == 0)
                    {
                        printk("%d: u32ComposerLength fail.\n", __LINE__);
                        continue;
                    }

                    if (MHal_XC_GetMiuOffset(stFormatInfo.u8ComposerMiuNo, &u32Offset) != TRUE)
                    {
                        printk("%d: MHal_XC_GetMiuOffset fail.\n", __LINE__);
                        continue;
                    }
#if DOLBY_OTT_CACHED_BUFFER
                    pu8CompData = _pu8InputMDAddr_Cached+ (stFormatInfo.u32ComposerAddr - _phyMDAddr);
                    Chip_Inv_Cache_Range(pu8CompData, stFormatInfo.u32ComposerLength);
#else
                    pu8CompData = _pu8InputMDAddr + (stFormatInfo.u32ComposerAddr - _phyMDAddr);
#endif

                    if (pu8CompData == NULL)
                    {
                        printk("composer mmap pa fail.\n");
                        continue;
                    }

                    // set compser data into color-format hw
                    if(MHal_XC_SetDolbyCompData(pu8CompData, stFormatInfo.u32ComposerLength)== FALSE)
                    {
                        printk("MHal_XC_SetDolbyCompData fail, u32RPtr: %d, CFDReady: 0x%llx\n", _pstOpenHDRSeamlessHDRShareMem->u32RdPtr, _pstOpenHDRSeamlessHDRShareMem->u64Reserved);
                    }
                }
#endif
#if 0
                if (stFormatInfo.u32HDRMetaLength == 0)
                {
                    printk("%d: u32HDRMetaLength fail.\n", __LINE__);
                    continue;
                }
#endif
                if (MHal_XC_GetMiuOffset(stFormatInfo.u8HDRMiuNo, &u32Offset) != TRUE)
                {
                    printk("%d: MHal_XC_GetMiuOffset fail.\n", __LINE__);
                    continue;
                }
                // turn on settings
                MHal_XC_SetInputSourceType(E_KDRV_XC_INPUT_SOURCE_OTT); //???

                //implement UI Dolby HDR on/off
                //so this control be moved to MDrv_XC_ProcessCFDIRQ()
                //MHal_XC_SetHDRType(E_KDRV_XC_HDR_DOLBY);

                // call CFD
                if (stFormatInfo.u32HDRMetaAddr != 0)
                {
#if 0
                    MS_U8 *pu8Metadata = NULL;
#if DOLBY_OTT_CACHED_BUFFER
                    pu8Metadata = _pu8InputMDAddr_Cached+ (stFormatInfo.u32HDRMetaAddr - _phyMDAddr);
                    Chip_Inv_Cache_Range(pu8Metadata, stFormatInfo.u32HDRMetaLength);
#else
                    pu8Metadata = _pu8InputMDAddr + (stFormatInfo.u32HDRMetaAddr - _phyMDAddr);
#endif

                    if (pu8Metadata == NULL)
                    {
                        printk("metadata mmap pa fail.\n");
                        continue;
                    }

                    // set metadata into color-format hw
                    if (MHal_XC_SetDolbyMetaData(pu8Metadata, stFormatInfo.u32HDRMetaLength) == FALSE)
                    {
                        printk("MHal_XC_SetDolbyMetaData fail.\n");
                    }
#endif

                    //update CFD para
                    if(MHal_XC_UpdateCFDPara(u8Win) == FALSE)
                    {
                        printk("ERROR: CFD update para fail.\n");
                    }

                    ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info stMetaDataInfo;
                    MHal_XC_CFD_WithdrawMMParam(&stMetaDataInfo, 0);
                    if ((stMetaDataInfo.HDRMemFormat.stHDRMemFormatCFD.bVUIValid == TRUE) &&
                        ((stMetaDataInfo.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics == 16)||(stMetaDataInfo.HDRMemFormat.stHDRMemFormatCFD.u8Transfer_Characteristics == 18)))
                    {
                        //HDR
                        stFormatInfo.bEnableDM = TRUE;
                    }
                    else
                    {
                        //SDR
                        stFormatInfo.bEnableDM = FALSE;
                    }
                }

                pstEntry->u32HDRLutSize = MHal_XC_GetHDRAutoDownloadStoredSize();

                K_XC_DS_CMDCNT stXCDSCmdCnt;
                MHal_XC_GetRegsetCnt_fromCFD(&stXCDSCmdCnt);
                pstEntry->u32HDRRegsetSize = (stXCDSCmdCnt.u16CMDCNT_IPM <<16) | (stXCDSCmdCnt.u16CMDCNT_OPM);

                //update metadata of shared memory
                setOpenHDRSeamlessInputMDFormatInfo(pu8InputMDData, stFormatInfo);
            }
        }
        else
        {
            pstEntry->u32HDRLutSize = MHal_XC_GetHDRAutoDownloadStoredSize();
            pstEntry->u32HDRRegsetSize = MHal_XC_GetRegsetCnt();
        }

        pstEntry->u16CFDReady = TRUE;
#if DOLBY_OTT_CACHED_BUFFER
        if (ulFlushAddr_IPM[0] != 0)
        {
            Chip_Flush_Cache_Range(ulFlushAddr_IPM[0],ulFlushLen_IPM[0]);
        }
        if (ulFlushAddr_IPM[1] != 0)
        {
            Chip_Flush_Cache_Range(ulFlushAddr_IPM[1],ulFlushLen_IPM[1]);
        }
        if (ulFlushAddr_OPM[0] != 0)
        {
            Chip_Flush_Cache_Range(ulFlushAddr_OPM[0],ulFlushLen_OPM[0]);
        }
#endif
        setOpenHDRSeamlessInfoEntry(_pstOpenHDRSeamlessHDRShareMem, u32RPtr, pstEntry);
        if(UserMode)
        {
            incOpenHDRSeamlessMemRPtr(_pstOpenHDRSeamlessHDRShareMem,pstEntry->u16Entry_Skip);
        }
    }
    while(0);
}