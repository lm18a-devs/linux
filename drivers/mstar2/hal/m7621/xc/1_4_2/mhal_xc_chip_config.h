#ifndef MHAL_XC_CONFIG_H
#define MHAL_XC_CONFIG_H

#define ENABLE_64BITS_COMMAND               1
#define ENABLE_64BITS_SPREAD_MODE           1 //need enable ENABLE_64BITS_COMMAND first

#define ENABLE_MLOAD_SAME_REG_COMBINE       1
#define DS_CMD_LEN_64BITS                   8
#define ENABLE_DS_4_BASEADDR_MODE           1 // need enable both ENABLE_64BITS_COMMAND and ENABLE_64BITS_SPREAD_MODE first
#define SUPPORT_DS_MULTI_USERS              1
#define DS_BUFFER_NUM_EX                    6
#define XC_REPLACE_MEMCPY_BY_BDMA
#define IS_SUPPORT_64BITS_COMMAND(bEnable64bitsCmd, u32DeviceID)           ((bEnable64bitsCmd == 1) && (u32DeviceID == 0))
#define ENABLE_NMLOAD                       1
//#define HDR10_DOLBY

//if you want to modify this define, please modify the same define in utopia (mhal_xc_chip_config.h)
//these irq will be used in both kernle and utopia
#define IRQ_REMAIN_IN_DUMMY                 (BIT(4))     //op vsync,BK00_10[4]

#define IRQ_INT_DISP_IP_VS                  (11)    //IP1 vsync,BK00_10[11]
#define IRQ_INT_DISP_OP_VS                  (4)     //op vsync,BK00_10[4]
#define IRQ_INT_DISP_DESCRB                 (22)    //hdr descrb,BK00_11[6]

#define INTERRUPT_DUMMY_REGISTER_L     REG_SC_BK30_4C_L
#define INTERRUPT_DUMMY_REGISTER_H     REG_SC_BK30_4D_L
#define INTERRUPT_DUMMY_REGISTER_EXT_L REG_SC_BK30_4E_L //0:10[3], 1:10[7], 2:10[11], 4:10[15], 5:11[3], 6:11[7], 8:11[11], 9:11[15]

#define IRQ_EXT_INDEX                  (BIT(3)|BIT(7)|BIT(11)|BIT(15)|BIT(19)|BIT(23)|BIT(27)|BIT(31))
#define u8DummyExtMappingTableArray    {0,0,0,0,0,0,0,1,0,0,0,2,0,0,0,4,0,0,0,5,0,0,0,6,0,0,0,8,0,0,0,9}

#define IPT
#define IPT_COUNT 1
#define IRQ_INT_DISP_IP1                 (11)    //IP1 vsync,BK00_10[11]

#define DYNAMIC_SWITCH_SDR_OPENHDR
#define MAX_WINDOW_NUM                   2

#define HDMI_VSDB_PACKET_SIZE 14
#define HDMI_HDR_PACKET_SIZE HDMI_VSDB_PACKET_SIZE

typedef enum
{
    E_SEAMLESS_HDMI_NO_SEAMLESS,
    E_SEAMLESS_HDMI_WITH_MUTE, //RR2077
    E_SEAMLESS_HDMI_WITH_FREEZE, //m5621 with freeze
    E_SEAMLESS_HDMI_WITHOUT_MUTE_AND_FREEZE, //m7621 hdmi seamless
    E_SEAMLESS_MAX,
} EN_KDRV_XC_HDR_SEAMLESS_PATH;

#define HDMI_HDR_SEAMLESS_PATH E_SEAMLESS_HDMI_WITH_FREEZE

typedef enum
{
    E_SEAMLESS_MVOPSOURCE_WITH_FREEZE,
    E_SEAMLESS_MVOPSOURCE_MAX,
} EN_KDRV_XC_HDR_SEAMLESS_MVOPSOURCE_PATH;

#define MVOPSOURCE_HDR_SEAMLESS_PATH E_SEAMLESS_MVOPSOURCE_WITH_FREEZE
#endif /* MHAL_XC_CONFIG_H */
