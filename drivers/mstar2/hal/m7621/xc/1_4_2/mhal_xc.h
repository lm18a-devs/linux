////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2008 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (¡§MStar Confidential Information¡¨) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _MHAL_XC_H_
#define _MHAL_XC_H_

#include "mdrv_xc_st.h"
#include "color_format_input.h"
#include "dolby_hdr_mem.h"
#include "mhal_xc_chip_config.h"

#ifdef _HAL_GFLIP_C
#define INTERFACE
#else
#define INTERFACE                               extern
#endif

//-------------------------------------------------------------------------------------------------
//  Driver Capability
//-------------------------------------------------------------------------------------------------
#define SUPPORT_TCH
//#if 0 first to skip compile error, need #define SUPPORT_TCH ready!!!
// need HQ refine #define SUPPORT_TCH flag !!!
#define SUPPORT_TCH_DEFINE_READY 0

//-------------------------------------------------------------------------------------------------
//  Macro and Define
//-------------------------------------------------------------------------------------------------

#define XC_FILM_DRIVER_VER               2

#define XC_FUNCTION                      1

#define XC_FRC_R2_SW_TOGGLE              1

#define IS_DOLBY_HDR(eWindow) ((_stCfdHdr[eWindow].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_DOLBY) == E_KDRV_XC_CFD_HDR_TYPE_DOLBY)
#define IS_OPEN_HDR(eWindow) ((_stCfdHdr[eWindow].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_OPEN) == E_KDRV_XC_CFD_HDR_TYPE_OPEN)
#define IS_HLG_HDR(eWindow) ((_stCfdHdr[eWindow].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_HLG) == E_KDRV_XC_CFD_HDR_TYPE_HLG)
//#define IS_HDMI_OPEN(eWindow) (((_stCfdHdr[eWindow].u8HdrType == E_KDRV_XC_CFD_HDR_TYPE_OPEN) && (_enInputSourceType == E_KDRV_XC_INPUT_SOURCE_HDMI)) ? 1 : 0)
//#define IS_OTT_OPEN(eWindow) (((_stCfdHdr[eWindow].u8HdrType == E_KDRV_XC_CFD_HDR_TYPE_OPEN) && (_enInputSourceType == E_KDRV_XC_INPUT_SOURCE_OTT)) ? 1 : 0)
#define IS_TC_HDR(eWindow) ((_stCfdHdr[eWindow].u8HdrType & E_KDRV_XC_CFD_HDR_TYPE_TCH) == E_KDRV_XC_CFD_HDR_TYPE_TCH)

#define IS_HDMI_DOLBY (((_enHDRType == E_KDRV_XC_HDR_DOLBY) && (_enInputSourceType == E_KDRV_XC_INPUT_SOURCE_HDMI)) ? 1 : 0)
#define IS_OTT_DOLBY (((_enHDRType == E_KDRV_XC_HDR_DOLBY) && (_enInputSourceType == E_KDRV_XC_INPUT_SOURCE_OTT)) ? 1 : 0)
#define IS_HDMI_OPEN (((_enHDRType == E_KDRV_XC_HDR_OPEN) && (_enInputSourceType == E_KDRV_XC_INPUT_SOURCE_HDMI)) ? 1 : 0)
#define IS_OTT_OPEN (((_enHDRType == E_KDRV_XC_HDR_OPEN) && (_enInputSourceType == E_KDRV_XC_INPUT_SOURCE_OTT)) ? 1 : 0)
#define IS_MVOP_TCH (((_enHDRType == E_KDRV_XC_HDR_TECHNICOLOR) && ((_enInputSourceType == E_KDRV_XC_INPUT_SOURCE_OTT)||(_enInputSourceType == E_KDRV_XC_INPUT_SOURCE_DTV))) ? 1 : 0)

#if XC_FUNCTION
#define BYTE                                    MS_U8
#define WORD                                    MS_U16
#define DWORD                                   MS_U32
//#define printf                                  //printk

#define REG_ADDR_VOP_SCREEN_CONTROL             L_BK_VOP(0x19)

#define REG_ADDR_OP_SW_SUB_ENABLE               L_BK_OP(0x10)

#define PRJ_MCNR

#define DOLBY_MUTE_CONTROLLED_BY_KERNEL 0
//#define AUTO_SEAMLESS_ALWAYS_ON_AFTER_DOLBY_HDMI
#ifndef AUTO_SEAMLESS_ALWAYS_ON_AFTER_DOLBY_HDMI
#if DOLBY_MUTE_CONTROLLED_BY_KERNEL
#define Dolby_SDR_HDR_MUTE                      1
#else
#define Dolby_SDR_HDR_MUTE                      0
#endif
#else
#define Dolby_SDR_HDR_MUTE                      0
#endif
#define DOLBY_GD_ENABLE                         1
#if DOLBY_GD_ENABLE
//value of global dimming is (0,255), when calculate percent, * 100 / 255 at first, then / 100.
#define DOLBY_GD_TO_DUTY(_gdval_, _period_)     (((_gdval_) * 100 / 255) * (_period_) / 100)
#endif
#define DOLBY_OTT_CACHED_BUFFER                 1
#define DOLBY_OTT_MULTI_TIMER                   1
#define OTT_AUTO_PAUSE_DETECT                   1

#define _BYTE_0  0x00
#define _BYTE_1  0x01
#define _BYTE_2  0x02
#define _BYTE_3  0x03
#define _BYTE_4  0x04
#define _BYTE_5  0x05
#define _BYTE_6  0x06
#define _BYTE_7  0x07
#define _BYTE_8  0x08
#define _BYTE_9  0x09
#define _BYTE_10  0x0A
#define _BYTE_11  0x0B
#define _BYTE_12  0x0C
#define _BYTE_13  0x0D

#define REG_LPLL_11_L                           (0x103100 | 0x11<< 1)
#define REG_LPLL_13_L                           (0x103100 | 0x13<< 1)

#define REG_SC_BK00_10_L                        (0x130000 | 0x10<< 1)
#ifndef REG_SC_BK00_11_L
#define REG_SC_BK00_11_L                        (0x130000 | 0x11<< 1)
#endif
#define REG_SC_BK00_12_L                        (0x130000 | 0x12<< 1)
#define REG_SC_BK00_13_L                        (0x130000 | 0x13<< 1)
#define REG_SC_BK00_14_L                        (0x130000 | 0x14<< 1)
#define REG_SC_BK00_15_L                        (0x130000 | 0x15<< 1)

#define REG_SC_BK01_06_L                        (0x130100 | 0x06<< 1)
#define REG_SC_BK01_07_L                        (0x130100 | 0x07<< 1)
#define REG_SC_BK01_1F_L                        (0x130100 | 0x1F<< 1)
#define REG_SC_BK01_20_L                        (0x130100 | 0x20<< 1)
#define REG_SC_BK01_40_L                        (0x130100 | 0x40<< 1)

#define REG_SC_BK01_42_L                        (0x130100 | 0x42<< 1)

#define REG_SC_BK01_61_L                        (0x130100 | 0x61<< 1)
#define REG_SC_BK01_61_H                        (0x130100 | ((0x61<< 1)+1))

#define REG_SC_BK02_10_L                        (0x130200 | (0x10<<1))
#define REG_SC_BK02_2A_L                        (0x130200 | (0x2A<< 1))
#define REG_SC_BK02_2B_L                        (0x130200 | (0x2B<< 1))
#define REG_SC_BK02_2D_L                        (0x130200 | (0x2D<< 1))
#define REG_SC_BK02_2E_L                        (0x130200 | (0x2E<< 1))
#define REG_SC_BK02_2F_L                        (0x130200 | (0x2F<< 1))
#define REG_SC_BK02_30_L                        (0x130200 | (0x30<< 1))
#define REG_SC_BK02_31_L                        (0x130200 | (0x31<< 1))
#define REG_SC_BK02_32_L                        (0x130200 | (0x32<< 1))
#define REG_SC_BK02_33_L                        (0x130200 | (0x33<< 1))
#define REG_SC_BK02_34_L                        (0x130200 | (0x34<< 1))
#define REG_SC_BK02_35_L                        (0x130200 | (0x35<< 1))

#define REG_SC_BK0A_02_L                        (0x130A00 | 0x02<< 1)
#define REG_SC_BK0A_05_L                        (0x130A00 | 0x05<< 1)
#define REG_SC_BK0A_06_L                        (0x130A00 | 0x06<< 1)
#define REG_SC_BK0A_07_L                        (0x130A00 | 0x07<< 1)
#define REG_SC_BK0A_0A_L                        (0x130A00 | 0x0A<< 1)
#define REG_SC_BK0A_0B_L                        (0x130A00 | 0x0B<< 1)
#define REG_SC_BK0A_0C_L                        (0x130A00 | 0x0C<< 1)
#define REG_SC_BK0A_0D_L                        (0x130A00 | 0x0D<< 1)
#define REG_SC_BK0A_0F_L                        (0x130A00 | 0x0F<< 1)

#define REG_SC_BK0A_10_L                        (0x130A00 | 0x10<< 1)
#define REG_SC_BK0A_15_L                        (0x130A00 | 0x15<< 1)
#define REG_SC_BK0A_16_L                        (0x130A00 | 0x16<< 1)
#define REG_SC_BK0A_17_L                        (0x130A00 | 0x17<< 1)
#define REG_SC_BK0A_1E_L                        (0x130A00 | 0x1E<< 1)
#define REG_SC_BK0A_1F_L                        (0x130A00 | 0x1F<< 1)

#define REG_SC_BK0A_20_L                        (0x130A00 | 0x20<< 1)
#define REG_SC_BK0A_21_L                        (0x130A00 | 0x21<< 1)
#define REG_SC_BK0A_23_L                        (0x130A00 | 0x23<< 1)
#define REG_SC_BK0A_24_L                        (0x130A00 | 0x24<< 1)
#define REG_SC_BK0A_25_L                        (0x130A00 | 0x25<< 1)
#define REG_SC_BK0A_41_L                        (0x130A00 | 0x41<< 1)
#define REG_SC_BK0A_4A_L                        (0x130A00 | 0x4A<< 1)
#define REG_SC_BK0A_4B_L                        (0x130A00 | 0x4B<< 1)

#define REG_SC_BK0A_57_L                        (0x130A00 | 0x57<< 1)
#define REG_SC_BK0A_5C_L                        (0x130A00 | 0x5C<< 1)
#define REG_SC_BK0A_5E_L                        (0x130A00 | 0x5E<< 1)

#define REG_SC_BK0F_18_L                        (0x130F00 | 0x18<< 1)
#define REG_SC_BK0F_57_L                        (0x130F00 | 0x57<< 1)
#define REG_SC_BK0F_6B_L                        (0x130F00 | 0x6B<< 1)

#define REG_SC_BK10_19_L                        (0x131000 | 0x19<< 1)
#define REG_SC_BK10_26_L                        (0x131000 | 0x26<< 1)
#define REG_SC_BK10_27_L                        (0x131000 | 0x27<< 1)
#define REG_SC_BK10_28_L                        (0x131000 | 0x28<< 1)
#define REG_SC_BK10_29_L                        (0x131000 | 0x29<< 1)
#define REG_SC_BK10_2A_L                        (0x131000 | 0x2A<< 1)
#define REG_SC_BK10_2B_L                        (0x131000 | 0x2B<< 1)
#define REG_SC_BK10_2C_L                        (0x131000 | 0x2C<< 1)
#define REG_SC_BK10_2D_L                        (0x131000 | 0x2D<< 1)
#define REG_SC_BK10_2E_L                        (0x131000 | 0x2E<< 1)
#define REG_SC_BK10_2F_L                        (0x131000 | 0x2F<< 1)
#define REG_SC_BK10_50_L                        (0x131000 | 0x50<< 1)

#define REG_SC_BK12_01_L                        (0x131200 | 0x01<< 1)
#define REG_SC_BK12_03_L                        (0x131200 | 0x03<< 1)
#define REG_SC_BK12_0F_L                        (0x131200 | 0x0F<< 1)
#define REG_SC_BK12_19_L                        (0x131200 | 0x19<< 1)
#define REG_SC_BK12_30_L                        (0x131200 | 0x30<< 1)
#define REG_SC_BK12_33_L                        (0x131200 | 0x33<< 1)
#define REG_SC_BK12_3A_L                        (0x131200 | 0x3A<< 1)

#define REG_SC_BK1F_02_L                        (0x131F00 | 0x02<< 1)
#define REG_SC_BK1F_10_L                        (0x131F00 | 0x10<< 1)
#define REG_SC_BK1F_10_H                        (0x131F00 | ((0x10<< 1)+1))
#define REG_SC_BK1F_1D_L                        (0x131F00 | 0x1D<< 1)
#define REG_SC_BK1F_79_L                        (0x131F00 | 0x79<< 1)
#define REG_SC_BK1F_79_H                        (0x131F00 | ((0x79<< 1)+1))

#define REG_SC_BK22_2A_L                        (0x132200 | 0x2A<< 1)
#define REG_SC_BK22_7A_L                        (0x132200 | 0x7A<< 1)
#define REG_SC_BK22_7C_L                        (0x132200 | 0x7C<< 1)
#define REG_SC_BK22_7E_L                        (0x132200 | 0x7E<< 1)

#define REG_SC_BK25_01_L                        (0x132500 | 0x01<< 1)

#define REG_SC_BK2A_02_L                        (0x132A00 | 0x02<< 1)
#define REG_SC_BK2A_29_L                        (0x132A00 | 0x29<< 1)
#define REG_SC_BK2A_2A_L                        (0x132A00 | 0x2A<< 1)
#define REG_SC_BK2A_2F_L                        (0x132A00 | 0x2F<< 1)
#define REG_SC_BK2A_7C_L                        (0x132A00 | 0x7C<< 1)
#define REG_SC_BK2A_7D_L                        (0x132A00 | 0x7D<< 1)
#define REG_SC_BK2A_7E_L                        (0x132A00 | 0x7E<< 1)

#define REG_SC_BK30_01_L                        (0x133000 | (0x01<< 1))
#define REG_SC_BK30_02_L                        (0x133000 | (0x02<< 1))
#define REG_SC_BK30_02_H                        (0x133000 | ((0x02<< 1)+1))
#define REG_SC_BK30_03_L                        (0x133000 | (0x03<< 1))
#define REG_SC_BK30_04_L                        (0x133000 | (0x04<< 1))

#define REG_SC_BK30_06_L                        (0x133000 | (0x06<< 1))
#define REG_SC_BK30_06_H                        (0x133000 | ((0x06<< 1)+1))
#define REG_SC_BK30_07_L                        (0x133000 | (0x07<< 1))
#define REG_SC_BK30_07_H                        (0x133000 | ((0x07<< 1)+1))
#define REG_SC_BK30_08_L                        (0x133000 | (0x08<< 1))
#define REG_SC_BK30_08_H                        (0x133000 | ((0x08<< 1)+1))
#define REG_SC_BK30_09_L                        (0x133000 | (0x09<< 1))
#define REG_SC_BK30_09_H                        (0x133000 | ((0x09<< 1)+1))
#define REG_SC_BK30_0A_L                        (0x133000 | (0x0A<< 1))
#define REG_SC_BK30_0A_H                        (0x133000 | ((0x0A<< 1)+1))
#define REG_SC_BK30_0B_L                        (0x133000 | (0x0B<< 1))
#define REG_SC_BK30_0B_H                        (0x133000 | ((0x0B<< 1)+1))
#define REG_SC_BK30_0C_L                        (0x133000 | (0x0C<< 1))
#define REG_SC_BK30_0C_H                        (0x133000 | ((0x0C<< 1)+1))
#define REG_SC_BK30_0D_L                        (0x133000 | (0x0D<< 1))
#define REG_SC_BK30_0D_H                        (0x133000 | ((0x0D<< 1)+1))
#define REG_SC_BK30_0E_L                        (0x133000 | (0x0E<< 1))
#define REG_SC_BK30_0E_H                        (0x133000 | ((0x0E<< 1)+1))
#define REG_SC_BK30_0F_L                        (0x133000 | (0x0F<< 1))
#define REG_SC_BK30_0F_H                        (0x133000 | ((0x0F<< 1)+1))

#define REG_SC_BK30_4C_L                        (0x133000 | (0x4C<< 1))
#define REG_SC_BK30_4C_H                        (0x133000 | ((0x4C<< 1)+1))
#define REG_SC_BK30_4D_L                        (0x133000 | (0x4D<< 1))
#define REG_SC_BK30_4D_H                        (0x133000 | ((0x4D<< 1)+1))

#define REG_SC_BK42_05_L                        (0x134200 | 0x05<< 1)
#define REG_SC_BK42_05_H                        (0x134200 | ((0x05<< 1)+1))
#define REG_SC_BK42_07_L                        (0x134200 | 0x07<< 1)
#define REG_SC_BK42_07_H                        (0x134200 | ((0x07<< 1)+1))
#define REG_SC_BK42_08_L                        (0x134200 | 0x08<< 1)
#define REG_SC_BK42_08_H                        (0x134200 | ((0x08<< 1)+1))
#define REG_SC_BK42_09_L                        (0x134200 | 0x09<< 1)
#define REG_SC_BK42_09_H                        (0x134200 | ((0x09<< 1)+1))
#define REG_SC_BK42_10_L                        (0x134200 | 0x10<< 1)
#define REG_SC_BK42_11_L                        (0x134200 | 0x11<< 1)
#define REG_SC_BK42_19_L                        (0x134200 | 0x19<< 1)
#define REG_SC_BK42_19_H                        (0x134200 | ((0x19<< 1)+1))
#define REG_SC_BK42_3A_L                        (0x134200 | 0x3A<< 1)
#define REG_SC_BK42_3A_H                        (0x134200 | ((0x3A<< 1)+1))
#define REG_SC_BK42_50_L                        (0x134200 | 0x50<< 1)
#define REG_SC_BK42_52_L                        (0x134200 | 0x52<< 1)
#define REG_SC_BK42_53_L                        (0x134200 | 0x53<< 1)
#define REG_SC_BK42_54_L                        (0x134200 | 0x54<< 1)
#define REG_SC_BK42_55_L                        (0x134200 | 0x55<< 1)

#define REG_SC_BK4E_21_L                        (0x134E00 | 0x21<< 1)
#define REG_SC_BK4E_58_L                        (0x134E00 | 0x58<< 1)
#define REG_SC_BK5C_01_L                        (0x135C00 | 0x01<< 1)

#define REG_SC_BK62_06_L                        (0x136200 | 0x06<< 1)
#define REG_SC_BK62_0E_L                        (0x136200 | 0x0E<< 1)
#define REG_SC_BK62_08_L                        (0x136200 | 0x08<< 1)
#define REG_SC_BK63_61_L                        (0x136300 | 0x61<< 1)

#define REG_SC_BK64_11_L                        (0x136400 | 0x11<< 1)
#define REG_SC_BK64_11_H                        (0x136400 | ((0x11<< 1)+1))
#define REG_SC_BK64_12_L                        (0x136400 | 0x12<< 1)
#define REG_SC_BK64_12_H                        (0x136400 | ((0x12<< 1)+1))
#define REG_SC_BK64_13_L                        (0x136400 | 0x13<< 1)
#define REG_SC_BK64_13_H                        (0x136400 | ((0x13<< 1)+1))
#define REG_SC_BK64_14_L                        (0x136400 | 0x14<< 1)
#define REG_SC_BK64_14_H                        (0x136400 | ((0x14<< 1)+1))
#define REG_SC_BK64_15_L                        (0x136400 | 0x15<< 1)
#define REG_SC_BK64_15_H                        (0x136400 | ((0x15<< 1)+1))
#define REG_SC_BK64_16_L                        (0x136400 | 0x16<< 1)
#define REG_SC_BK64_16_H                        (0x136400 | ((0x16<< 1)+1))
#define REG_SC_BK64_17_L                        (0x136400 | 0x17<< 1)
#define REG_SC_BK64_17_H                        (0x136400 | ((0x17<< 1)+1))
#define REG_SC_BK64_18_L                        (0x136400 | 0x18<< 1)
#define REG_SC_BK64_18_H                        (0x136400 | ((0x18<< 1)+1))
#define REG_SC_BK64_19_L                        (0x136400 | 0x19<< 1)
#define REG_SC_BK64_19_H                        (0x136400 | ((0x19<< 1)+1))
#define REG_SC_BK64_1A_L                        (0x136400 | 0x1A<< 1)
#define REG_SC_BK64_1A_H                        (0x136400 | ((0x1A<< 1)+1))
#define REG_SC_BK64_1B_L                        (0x136400 | 0x1B<< 1)
#define REG_SC_BK64_1B_H                        (0x136400 | ((0x1B<< 1)+1))
#define REG_SC_BK64_1C_L                        (0x136400 | 0x1C<< 1)
#define REG_SC_BK64_1C_H                        (0x136400 | ((0x1C<< 1)+1))
#define REG_SC_BK64_1D_L                        (0x136400 | 0x1D<< 1)
#define REG_SC_BK64_1D_H                        (0x136400 | ((0x1D<< 1)+1))

#define REG_SC_BK67_01_L                        (0x136700 | 0x01<< 1)
#define REG_SC_BK67_01_H                        (0x136700 | ((0x01<< 1)+1))
#define REG_SC_BK67_02_L                        (0x136700 | 0x02<< 1)
#define REG_SC_BK67_02_H                        (0x136700 | ((0x02<< 1)+1))
#define REG_SC_BK67_03_L                        (0x136700 | 0x03<< 1)
#define REG_SC_BK67_03_H                        (0x136700 | ((0x03<< 1)+1))
#define REG_SC_BK67_04_L                        (0x136700 | 0x04<< 1)
#define REG_SC_BK67_04_H                        (0x136700 | ((0x04<< 1)+1))
#define REG_SC_BK67_05_L                        (0x136700 | 0x05<< 1)
#define REG_SC_BK67_05_H                        (0x136700 | ((0x05<< 1)+1))
#define REG_SC_BK67_06_L                        (0x136700 | 0x06<< 1)
#define REG_SC_BK67_06_H                        (0x136700 | ((0x06<< 1)+1))
#define REG_SC_BK67_07_L                        (0x136700 | 0x07<< 1)
#define REG_SC_BK67_07_H                        (0x136700 | ((0x07<< 1)+1))
#define REG_SC_BK67_11_L                        (0x136700 | 0x11<< 1)
#define REG_SC_BK67_11_H                        (0x136700 | ((0x11<< 1)+1))
#define REG_SC_BK67_12_L                        (0x136700 | 0x12<< 1)
#define REG_SC_BK67_12_H                        (0x136700 | ((0x12<< 1)+1))
#define REG_SC_BK67_13_L                        (0x136700 | 0x13<< 1)
#define REG_SC_BK67_13_H                        (0x136700 | ((0x13<< 1)+1))
#define REG_SC_BK67_16_L                        (0x136700 | 0x16<< 1)
#define REG_SC_BK67_16_H                        (0x136700 | ((0x16<< 1)+1))
#define REG_SC_BK67_17_L                        (0x136700 | 0x17<< 1)
#define REG_SC_BK67_17_H                        (0x136700 | ((0x17<< 1)+1))

#define REG_SC_BK67_1A_L                        (0x136700 | 0x1A<< 1)
#define REG_SC_BK67_1A_H                        (0x136700 | ((0x1A<< 1)+1))

#define REG_SC_BK67_1D_L                        (0x136700 | 0x1D<< 1)
#define REG_SC_BK67_1D_H                        (0x136700 | ((0x1D<< 1)+1))

#define REG_SC_BK67_20_L                        (0x136700 | 0x20<< 1)
#define REG_SC_BK67_20_H                        (0x136700 | ((0x20<< 1)+1))

#define REG_SC_BK67_21_L                        (0x136700 | 0x21<< 1)
#define REG_SC_BK67_21_H                        (0x136700 | ((0x21<< 1)+1))
#define REG_SC_BK67_22_L                        (0x136700 | 0x22<< 1)
#define REG_SC_BK67_22_H                        (0x136700 | ((0x22<< 1)+1))
#define REG_SC_BK67_23_L                        (0x136700 | 0x23<< 1)
#define REG_SC_BK67_23_H                        (0x136700 | ((0x23<< 1)+1))
#define REG_SC_BK67_24_L                        (0x136700 | 0x24<< 1)
#define REG_SC_BK67_24_H                        (0x136700 | ((0x24<< 1)+1))
#define REG_SC_BK67_25_L                        (0x136700 | 0x25<< 1)
#define REG_SC_BK67_25_H                        (0x136700 | ((0x25<< 1)+1))
#define REG_SC_BK67_26_L                        (0x136700 | 0x26<< 1)
#define REG_SC_BK67_26_H                        (0x136700 | ((0x26<< 1)+1))
#define REG_SC_BK67_28_L                        (0x136700 | 0x28<< 1)
#define REG_SC_BK67_28_H                        (0x136700 | ((0x28<< 1)+1))
#define REG_SC_BK67_29_L                        (0x136700 | 0x29<< 1)
#define REG_SC_BK67_29_H                        (0x136700 | ((0x29<< 1)+1))
#define REG_SC_BK67_2A_L                        (0x136700 | 0x2A<< 1)
#define REG_SC_BK67_2A_H                        (0x136700 | ((0x2A<< 1)+1))
#define REG_SC_BK67_2B_L                        (0x136700 | 0x2B<< 1)
#define REG_SC_BK67_2B_H                        (0x136700 | ((0x2B<< 1)+1))
#define REG_SC_BK67_2C_L                        (0x136700 | 0x2C<< 1)
#define REG_SC_BK67_2C_H                        (0x136700 | ((0x2C<< 1)+1))
#define REG_SC_BK67_32_L                        (0x136700 | 0x32<< 1)
#define REG_SC_BK67_32_H                        (0x136700 | ((0x32<< 1)+1))
#define REG_SC_BK67_33_L                        (0x136700 | 0x33<< 1)
#define REG_SC_BK67_33_H                        (0x136700 | ((0x33<< 1)+1))
#define REG_SC_BK67_34_L                        (0x136700 | 0x34<< 1)
#define REG_SC_BK67_34_H                        (0x136700 | ((0x34<< 1)+1))
#define REG_SC_BK67_38_L                        (0x136700 | 0x38<< 1)
#define REG_SC_BK67_38_H                        (0x136700 | ((0x38<< 1)+1))
#define REG_SC_BK67_39_L                        (0x136700 | 0x39<< 1)
#define REG_SC_BK67_39_H                        (0x136700 | ((0x39<< 1)+1))

#define REG_SC_BK71_01_L                        (0x137100 | 0x01<< 1)

#define REG_SC_BK79_02_L                        (0x137900 | 0x02<< 1)
#define REG_SC_BK79_02_H                        (0x137900 | ((0x02<< 1)+1))
#define REG_SC_BK79_03_L                        (0x137900 | 0x03<< 1)
#define REG_SC_BK79_03_H                        (0x137900 | ((0x03<< 1)+1))
#define REG_SC_BK79_04_L                        (0x137900 | 0x04<< 1)
#define REG_SC_BK79_04_H                        (0x137900 | ((0x04<< 1)+1))
#define REG_SC_BK79_06_L                        (0x137900 | 0x06<< 1)
#define REG_SC_BK79_06_H                        (0x137900 | ((0x06<< 1)+1))
#define REG_SC_BK79_07_L                        (0x137900 | 0x07<< 1)
#define REG_SC_BK79_07_H                        (0x137900 | ((0x07<< 1)+1))
#define REG_SC_BK79_08_L                        (0x137900 | 0x08<< 1)
#define REG_SC_BK79_08_H                        (0x137900 | ((0x08<< 1)+1))
#define REG_SC_BK79_09_L                        (0x137900 | 0x09<< 1)
#define REG_SC_BK79_09_H                        (0x137900 | ((0x09<< 1)+1))
#define REG_SC_BK79_10_L                        (0x137900 | 0x10<< 1)
#define REG_SC_BK79_10_H                        (0x137900 | ((0x10<< 1)+1))
#define REG_SC_BK79_11_L                        (0x137900 | 0x11<< 1)
#define REG_SC_BK79_11_H                        (0x137900 | ((0x11<< 1)+1))
#define REG_SC_BK79_12_L                        (0x137900 | 0x12<< 1)
#define REG_SC_BK79_12_H                        (0x137900 | ((0x12<< 1)+1))
#define REG_SC_BK79_13_L                        (0x137900 | 0x13<< 1)
#define REG_SC_BK79_13_H                        (0x137900 | ((0x13<< 1)+1))
#define REG_SC_BK79_14_L                        (0x137900 | 0x14<< 1)
#define REG_SC_BK79_14_H                        (0x137900 | ((0x14<< 1)+1))
#define REG_SC_BK79_18_L                        (0x137900 | 0x18<< 1)
#define REG_SC_BK79_18_H                        (0x137900 | ((0x18<< 1)+1))
#define REG_SC_BK79_19_L                        (0x137900 | 0x19<< 1)
#define REG_SC_BK79_19_H                        (0x137900 | ((0x19<< 1)+1))
#define REG_SC_BK79_1A_L                        (0x137900 | 0x1A<< 1)
#define REG_SC_BK79_1A_H                        (0x137900 | ((0x1A<< 1)+1))
#define REG_SC_BK79_1B_L                        (0x137900 | 0x1B<< 1)
#define REG_SC_BK79_1B_H                        (0x137900 | ((0x1B<< 1)+1))
#define REG_SC_BK79_1C_L                        (0x137900 | 0x1C<< 1)
#define REG_SC_BK79_1C_H                        (0x137900 | ((0x1C<< 1)+1))
#define REG_SC_BK79_1D_L                        (0x137900 | 0x1D<< 1)
#define REG_SC_BK79_1D_H                        (0x137900 | ((0x1D<< 1)+1))
#define REG_SC_BK79_1E_L                        (0x137900 | 0x1E<< 1)
#define REG_SC_BK79_1E_H                        (0x137900 | ((0x1E<< 1)+1))
#define REG_SC_BK79_1F_L                        (0x137900 | 0x1F<< 1)
#define REG_SC_BK79_1F_H                        (0x137900 | ((0x1F<< 1)+1))
#define REG_SC_BK79_3F_L                        (0x137900 | 0x3F<< 1)
#define REG_SC_BK79_3F_H                        (0x137900 | ((0x3F<< 1)+1))
#define REG_SC_BK79_7C_L                        (0x137900 | 0x7C<< 1)
#define REG_SC_BK79_7C_H                        (0x137900 | ((0x7C<< 1)+1))
#define REG_SC_BK79_7D_L                        (0x137900 | 0x7D<< 1)
#define REG_SC_BK79_7D_H                        (0x137900 | ((0x7D<< 1)+1))
#define REG_SC_BK79_7E_L                        (0x137900 | 0x7E<< 1)
#define REG_SC_BK79_7E_H                        (0x137900 | ((0x7E<< 1)+1))
#define REG_SC_BK79_7F_L                        (0x137900 | 0x7F<< 1)
#define REG_SC_BK79_7F_H                        (0x137900 | ((0x7F<< 1)+1))

#define REG_SC_BK7A_01_L                        (0x137A00 | 0x01<< 1)
#define REG_SC_BK7A_01_H                        (0x137A00 | ((0x01<< 1)+1))
#define REG_SC_BK7A_02_L                        (0x137A00 | 0x02<< 1)
#define REG_SC_BK7A_02_H                        (0x137A00 | ((0x02<< 1)+1))
#define REG_SC_BK7A_42_L                        (0x137A00 | 0x42<< 1)
#define REG_SC_BK7A_42_H                        (0x137A00 | ((0x42<< 1)+1))
#define REG_SC_BK7A_50_L                        (0x137A00 | 0x50<< 1)
#define REG_SC_BK7A_50_H                        (0x137A00 | ((0x50<< 1)+1))
#define REG_SC_BK7A_51_L                        (0x137A00 | 0x51<< 1)
#define REG_SC_BK7A_51_H                        (0x137A00 | ((0x51<< 1)+1))
#define REG_SC_BK7A_52_L                        (0x137A00 | 0x52<< 1)
#define REG_SC_BK7A_52_H                        (0x137A00 | ((0x52<< 1)+1))
#define REG_SC_BK7A_53_L                        (0x137A00 | 0x53<< 1)
#define REG_SC_BK7A_53_H                        (0x137A00 | ((0x53<< 1)+1))
#define REG_SC_BK7A_54_L                        (0x137A00 | 0x54<< 1)
#define REG_SC_BK7A_54_H                        (0x137A00 | ((0x54<< 1)+1))
#define REG_SC_BK7A_55_L                        (0x137A00 | 0x55<< 1)
#define REG_SC_BK7A_55_H                        (0x137A00 | ((0x55<< 1)+1))
#define REG_SC_BK7A_56_L                        (0x137A00 | 0x56<< 1)
#define REG_SC_BK7A_56_H                        (0x137A00 | ((0x56<< 1)+1))
#define REG_SC_BK7A_57_L                        (0x137A00 | 0x57<< 1)
#define REG_SC_BK7A_57_H                        (0x137A00 | ((0x57<< 1)+1))
#define REG_SC_BK7A_59_L                        (0x137A00 | 0x59<< 1)
#define REG_SC_BK7A_59_H                        (0x137A00 | ((0x59<< 1)+1))
#define REG_SC_BK7A_60_L                        (0x137A00 | 0x60<< 1)
#define REG_SC_BK7A_60_H                        (0x137A00 | ((0x60<< 1)+1))
#define REG_SC_BK7A_61_L                        (0x137A00 | 0x61<< 1)
#define REG_SC_BK7A_61_H                        (0x137A00 | ((0x61<< 1)+1))
#define REG_SC_BK7A_68_L                        (0x137A00 | 0x68<< 1)
#define REG_SC_BK7A_68_H                        (0x137A00 | ((0x68<< 1)+1))
#define REG_SC_BK7A_6A_L                        (0x137A00 | 0x6A<< 1)
#define REG_SC_BK7A_6A_H                        (0x137A00 | ((0x6A<< 1)+1))
#define REG_SC_BK7A_6B_L                        (0x137A00 | 0x6B<< 1)
#define REG_SC_BK7A_6B_H                        (0x137A00 | ((0x6B<< 1)+1))
#define REG_SC_BK7A_6C_L                        (0x137A00 | 0x6C<< 1)
#define REG_SC_BK7A_6C_H                        (0x137A00 | ((0x6C<< 1)+1))
#define REG_SC_BK7A_6D_L                        (0x137A00 | 0x6D<< 1)
#define REG_SC_BK7A_6D_H                        (0x137A00 | ((0x6D<< 1)+1))
#define REG_SC_BK7A_6E_L                        (0x137A00 | 0x6E<< 1)
#define REG_SC_BK7A_6E_H                        (0x137A00 | ((0x6E<< 1)+1))
#define REG_SC_BK7A_6F_L                        (0x137A00 | 0x6F<< 1)
#define REG_SC_BK7A_6F_H                        (0x137A00 | ((0x6F<< 1)+1))

#define REG_SC_BK7A_70_L                        (0x137A00 | 0x70<< 1)
#define REG_SC_BK7A_70_H                        (0x137A00 | ((0x70<< 1)+1))
#define REG_SC_BK7A_71_L                        (0x137A00 | 0x71<< 1)
#define REG_SC_BK7A_71_H                        (0x137A00 | ((0x71<< 1)+1))
#define REG_SC_BK7A_72_L                        (0x137A00 | 0x72<< 1)
#define REG_SC_BK7A_72_H                        (0x137A00 | ((0x72<< 1)+1))
#define REG_SC_BK7B_01_L                        (0x137B00 | ((0x1<< 1)))
#define REG_SC_BK7B_02_L                        (0x137B00 | ((0x2<< 1)))
#define REG_SC_BK7F_10_L                        (0x137F00 | 0x10<< 1)
#define REG_SC_BK7F_10_H                        (0x137F00 | ((0x10<< 1)+1))

#define REG_SC_BKF4_02_L                        (0x13F400 | 0x02<< 1)
#define REG_SC_BKF4_02_H                        (0x13F400 | ((0x02<< 1)+1))
#define REG_SC_BKF4_03_L                        (0x13F400 | 0x03<< 1)
#define REG_SC_BKF4_03_H                        (0x13F400 | ((0x03<< 1)+1))
#define REG_SC_BKF4_20_L                        (0x13F400 | 0x20<< 1)
#define REG_SC_BKF4_20_H                        (0x13F400 | ((0x20<< 1)+1))
#define REG_SC_BKF4_21_L                        (0x13F400 | 0x21<< 1)
#define REG_SC_BKF4_21_H                        (0x13F400 | ((0x21<< 1)+1))

#define REG_SC_BKF8_40_L                        (0x13F800 | 0x40<< 1)
#define REG_SC_BKF9_40_L                        (0x13F900 | 0x40<< 1)
#define REG_SC_BKFA_40_L                        (0x13FA00 | 0x40<< 1)
#define REG_SC_BKFB_40_L                        (0x13FB00 | 0x40<< 1)
#define REG_SC_BKFC_40_L                        (0x13FC00 | 0x40<< 1)
#define REG_SC_BKFD_40_L                        (0x13FD00 | 0x40<< 1)
#define REG_SC_BKFE_40_L                        (0x13FE00 | 0x40<< 1)

#define REG_SC_VR_BK02_00_L                        (0x100*2 + (0x00<< 1))
#define REG_SC_VR_BK02_00_H                        (0x100*2 + ((0x00<< 1)+1))
#define REG_SC_VR_BK02_01_L                        (0x100*2 + (0x01<< 1))
#define REG_SC_VR_BK02_01_H                        (0x100*2 + ((0x01<< 1)+1))
#define REG_SC_VR_BK02_02_L                        (0x100*2 + (0x02<< 1))
#define REG_SC_VR_BK02_02_H                        (0x100*2 + ((0x02<< 1)+1))
#define REG_SC_VR_BK02_03_L                        (0x100*2 + (0x03<< 1))
#define REG_SC_VR_BK02_03_H                        (0x100*2 + ((0x03<< 1)+1))
#define REG_SC_VR_BK02_04_L                        (0x100*2 + (0x04<< 1))
#define REG_SC_VR_BK02_04_H                        (0x100*2 + ((0x04<< 1)+1))
#define REG_SC_VR_BK02_05_L                        (0x100*2 + (0x05<< 1))
#define REG_SC_VR_BK02_05_H                        (0x100*2 + ((0x05<< 1)+1))
#define REG_SC_VR_BK02_06_L                        (0x100*2 + (0x06<< 1))
#define REG_SC_VR_BK02_06_H                        (0x100*2 + ((0x06<< 1)+1))
#define REG_SC_VR_BK02_07_L                        (0x100*2 + (0x07<< 1))
#define REG_SC_VR_BK02_07_H                        (0x100*2 + ((0x07<< 1)+1))
#define REG_SC_VR_BK02_08_L                        (0x100*2 + (0x08<< 1))
#define REG_SC_VR_BK02_08_H                        (0x100*2 + ((0x08<< 1)+1))
#define REG_SC_VR_BK02_09_L                        (0x100*2 + (0x09<< 1))
#define REG_SC_VR_BK02_09_H                        (0x100*2 + ((0x09<< 1)+1))
#define REG_SC_VR_BK02_0A_L                        (0x100*2 + (0x0A<< 1))
#define REG_SC_VR_BK02_0A_H                        (0x100*2 + ((0x0A<< 1)+1))
#define REG_SC_VR_BK02_0B_L                        (0x100*2 + (0x0B<< 1))
#define REG_SC_VR_BK02_0B_H                        (0x100*2 + ((0x0B<< 1)+1))
#define REG_SC_VR_BK02_0C_L                        (0x100*2 + (0x0C<< 1))
#define REG_SC_VR_BK02_0C_H                        (0x100*2 + ((0x0C<< 1)+1))
#define REG_SC_VR_BK02_0D_L                        (0x100*2 + (0x0D<< 1))
#define REG_SC_VR_BK02_0D_H                        (0x100*2 + ((0x0D<< 1)+1))
#define REG_SC_VR_BK02_0E_L                        (0x100*2 + (0x0E<< 1))
#define REG_SC_VR_BK02_0E_H                        (0x100*2 + ((0x0E<< 1)+1))
#define REG_SC_VR_BK02_0F_L                        (0x100*2 + (0x0F<< 1))
#define REG_SC_VR_BK02_0F_H                        (0x100*2 + ((0x0F<< 1)+1))

#define REG_SC_VR_BK03_00_L                        (0x100*3 + (0x00<< 1))
#define REG_SC_VR_BK03_00_H                        (0x100*3 + ((0x00<< 1)+1))
#define REG_SC_VR_BK03_01_L                        (0x100*3 + (0x01<< 1))
#define REG_SC_VR_BK03_01_H                        (0x100*3 + ((0x01<< 1)+1))
#define REG_SC_VR_BK03_02_L                        (0x100*3 + (0x02<< 1))
#define REG_SC_VR_BK03_02_H                        (0x100*3 + ((0x02<< 1)+1))
#define REG_SC_VR_BK03_03_L                        (0x100*3 + (0x03<< 1))
#define REG_SC_VR_BK03_03_H                        (0x100*3 + ((0x03<< 1)+1))
#define REG_SC_VR_BK03_04_L                        (0x100*3 + (0x04<< 1))
#define REG_SC_VR_BK03_04_H                        (0x100*3 + ((0x04<< 1)+1))
#define REG_SC_VR_BK03_05_L                        (0x100*3 + (0x05<< 1))
#define REG_SC_VR_BK03_05_H                        (0x100*3 + ((0x05<< 1)+1))
#define REG_SC_VR_BK03_06_L                        (0x100*3 + (0x06<< 1))
#define REG_SC_VR_BK03_06_H                        (0x100*3 + ((0x06<< 1)+1))
#define REG_SC_VR_BK03_07_L                        (0x100*3 + (0x07<< 1))
#define REG_SC_VR_BK03_07_H                        (0x100*3 + ((0x07<< 1)+1))
#define REG_SC_VR_BK03_08_L                        (0x100*3 + (0x08<< 1))
#define REG_SC_VR_BK03_08_H                        (0x100*3 + ((0x08<< 1)+1))
#define REG_SC_VR_BK03_09_L                        (0x100*3 + (0x09<< 1))
#define REG_SC_VR_BK03_09_H                        (0x100*3 + ((0x09<< 1)+1))
#define REG_SC_VR_BK03_0A_L                        (0x100*3 + (0x0A<< 1))
#define REG_SC_VR_BK03_0A_H                        (0x100*3 + ((0x0A<< 1)+1))
#define REG_SC_VR_BK03_0B_L                        (0x100*3 + (0x0B<< 1))
#define REG_SC_VR_BK03_0B_H                        (0x100*3 + ((0x0B<< 1)+1))
#define REG_SC_VR_BK03_0C_L                        (0x100*3 + (0x0C<< 1))
#define REG_SC_VR_BK03_0C_H                        (0x100*3 + ((0x0C<< 1)+1))
#define REG_SC_VR_BK03_0D_L                        (0x100*3 + (0x0D<< 1))
#define REG_SC_VR_BK03_0D_H                        (0x100*3 + ((0x0D<< 1)+1))
#define REG_SC_VR_BK03_0E_L                        (0x100*3 + (0x0E<< 1))
#define REG_SC_VR_BK03_0E_H                        (0x100*3 + ((0x0E<< 1)+1))
#define REG_SC_VR_BK03_0F_L                        (0x100*3 + (0x0F<< 1))
#define REG_SC_VR_BK03_0F_H                        (0x100*3 + ((0x0F<< 1)+1))
#define REG_SC_VR_BK03_10_L                        (0x100*3 + (0x10<< 1))
#define REG_SC_VR_BK03_10_H                        (0x100*3 + ((0x10<< 1)+1))
#define REG_SC_VR_BK03_11_L                        (0x100*3 + (0x11<< 1))
#define REG_SC_VR_BK03_11_H                        (0x100*3 + ((0x11<< 1)+1))
#define REG_SC_VR_BK03_12_L                        (0x100*3 + (0x12<< 1))
#define REG_SC_VR_BK03_12_H                        (0x100*3 + ((0x12<< 1)+1))
#define REG_SC_VR_BK03_13_L                        (0x100*3 + (0x13<< 1))
#define REG_SC_VR_BK03_13_H                        (0x100*3 + ((0x13<< 1)+1))
#define REG_SC_VR_BK03_14_L                        (0x100*3 + (0x14<< 1))
#define REG_SC_VR_BK03_14_H                        (0x100*3 + ((0x14<< 1)+1))
#define REG_SC_VR_BK03_15_L                        (0x100*3 + (0x15<< 1))
#define REG_SC_VR_BK03_15_H                        (0x100*3 + ((0x15<< 1)+1))
#define REG_SC_VR_BK03_16_L                        (0x100*3 + (0x16<< 1))
#define REG_SC_VR_BK03_16_H                        (0x100*3 + ((0x16<< 1)+1))
#define REG_SC_VR_BK03_17_L                        (0x100*3 + (0x17<< 1))
#define REG_SC_VR_BK03_17_H                        (0x100*3 + ((0x17<< 1)+1))
#define REG_SC_VR_BK03_18_L                        (0x100*3 + (0x18<< 1))
#define REG_SC_VR_BK03_18_H                        (0x100*3 + ((0x18<< 1)+1))
#define REG_SC_VR_BK03_19_L                        (0x100*3 + (0x19<< 1))
#define REG_SC_VR_BK03_19_H                        (0x100*3 + ((0x19<< 1)+1))
#define REG_SC_VR_BK03_1A_L                        (0x100*3 + (0x1A<< 1))
#define REG_SC_VR_BK03_1A_H                        (0x100*3 + ((0x1A<< 1)+1))
#define REG_SC_VR_BK03_1B_L                        (0x100*3 + (0x1B<< 1))
#define REG_SC_VR_BK03_1B_H                        (0x100*3 + ((0x1B<< 1)+1))
#define REG_SC_VR_BK03_1C_L                        (0x100*3 + (0x1C<< 1))
#define REG_SC_VR_BK03_1C_H                        (0x100*3 + ((0x1C<< 1)+1))
#define REG_SC_VR_BK03_1D_L                        (0x100*3 + (0x1D<< 1))
#define REG_SC_VR_BK03_1D_H                        (0x100*3 + ((0x1D<< 1)+1))
#define REG_SC_VR_BK03_1E_L                        (0x100*3 + (0x1E<< 1))
#define REG_SC_VR_BK03_1E_H                        (0x100*3 + ((0x1E<< 1)+1))
#define REG_SC_VR_BK03_1F_L                        (0x100*3 + (0x1F<< 1))
#define REG_SC_VR_BK03_1F_H                        (0x100*3 + ((0x1F<< 1)+1))
#define REG_SC_VR_BK03_20_L                        (0x100*3 + (0x20<< 1))
#define REG_SC_VR_BK03_20_H                        (0x100*3 + ((0x20<< 1)+1))
#define REG_SC_VR_BK03_21_L                        (0x100*3 + (0x21<< 1))
#define REG_SC_VR_BK03_21_H                        (0x100*3 + ((0x21<< 1)+1))
#define REG_SC_VR_BK03_22_L                        (0x100*3 + (0x22<< 1))
#define REG_SC_VR_BK03_22_H                        (0x100*3 + ((0x22<< 1)+1))
#define REG_SC_VR_BK03_23_L                        (0x100*3 + (0x23<< 1))
#define REG_SC_VR_BK03_23_H                        (0x100*3 + ((0x23<< 1)+1))

#define REG_SC_VR_BK04_00_L                        (0x100*4 + (0x00<< 1))
#define REG_SC_VR_BK04_00_H                        (0x100*4 + ((0x00<< 1)+1))
#define REG_SC_VR_BK04_01_L                        (0x100*4 + (0x01<< 1))
#define REG_SC_VR_BK04_01_H                        (0x100*4 + ((0x01<< 1)+1))
#define REG_SC_VR_BK04_02_L                        (0x100*4 + (0x02<< 1))
#define REG_SC_VR_BK04_02_H                        (0x100*4 + ((0x02<< 1)+1))
#define REG_SC_VR_BK04_03_L                        (0x100*4 + (0x03<< 1))
#define REG_SC_VR_BK04_03_H                        (0x100*4 + ((0x03<< 1)+1))
#define REG_SC_VR_BK04_04_L                        (0x100*4 + (0x04<< 1))
#define REG_SC_VR_BK04_04_H                        (0x100*4 + ((0x04<< 1)+1))
#define REG_SC_VR_BK04_05_L                        (0x100*4 + (0x05<< 1))
#define REG_SC_VR_BK04_05_H                        (0x100*4 + ((0x05<< 1)+1))
#define REG_SC_VR_BK04_06_L                        (0x100*4 + (0x06<< 1))
#define REG_SC_VR_BK04_06_H                        (0x100*4 + ((0x06<< 1)+1))
#define REG_SC_VR_BK04_07_L                        (0x100*4 + (0x07<< 1))
#define REG_SC_VR_BK04_07_H                        (0x100*4 + ((0x07<< 1)+1))
#define REG_SC_VR_BK04_08_L                        (0x100*4 + (0x08<< 1))
#define REG_SC_VR_BK04_08_H                        (0x100*4 + ((0x08<< 1)+1))
#define REG_SC_VR_BK04_09_L                        (0x100*4 + (0x09<< 1))
#define REG_SC_VR_BK04_09_H                        (0x100*4 + ((0x09<< 1)+1))
#define REG_SC_VR_BK04_0A_L                        (0x100*4 + (0x0A<< 1))
#define REG_SC_VR_BK04_0A_H                        (0x100*4 + ((0x0A<< 1)+1))
#define REG_SC_VR_BK04_0B_L                        (0x100*4 + (0x0B<< 1))
#define REG_SC_VR_BK04_0B_H                        (0x100*4 + ((0x0B<< 1)+1))
#define REG_SC_VR_BK04_0C_L                        (0x100*4 + (0x0C<< 1))
#define REG_SC_VR_BK04_0C_H                        (0x100*4 + ((0x0C<< 1)+1))
#define REG_SC_VR_BK04_0D_L                        (0x100*4 + (0x0D<< 1))
#define REG_SC_VR_BK04_0D_H                        (0x100*4 + ((0x0D<< 1)+1))
#define REG_SC_VR_BK04_0E_L                        (0x100*4 + (0x0E<< 1))
#define REG_SC_VR_BK04_0E_H                        (0x100*4 + ((0x0E<< 1)+1))
#define REG_SC_VR_BK04_0F_L                        (0x100*4 + (0x0F<< 1))
#define REG_SC_VR_BK04_0F_H                        (0x100*4 + ((0x0F<< 1)+1))
#define REG_SC_VR_BK04_10_L                        (0x100*4 + (0x10<< 1))
#define REG_SC_VR_BK04_10_H                        (0x100*4 + ((0x10<< 1)+1))
#define REG_SC_VR_BK04_11_L                        (0x100*4 + (0x11<< 1))
#define REG_SC_VR_BK04_11_H                        (0x100*4 + ((0x11<< 1)+1))
#define REG_SC_VR_BK04_12_L                        (0x100*4 + (0x12<< 1))
#define REG_SC_VR_BK04_12_H                        (0x100*4 + ((0x12<< 1)+1))
#define REG_SC_VR_BK04_13_L                        (0x100*4 + (0x13<< 1))
#define REG_SC_VR_BK04_13_H                        (0x100*4 + ((0x13<< 1)+1))
#define REG_SC_VR_BK04_14_L                        (0x100*4 + (0x14<< 1))
#define REG_SC_VR_BK04_14_H                        (0x100*4 + ((0x14<< 1)+1))
#define REG_SC_VR_BK04_15_L                        (0x100*4 + (0x15<< 1))
#define REG_SC_VR_BK04_15_H                        (0x100*4 + ((0x15<< 1)+1))
#define REG_SC_VR_BK04_16_L                        (0x100*4 + (0x16<< 1))
#define REG_SC_VR_BK04_16_H                        (0x100*4 + ((0x16<< 1)+1))
#define REG_SC_VR_BK04_17_L                        (0x100*4 + (0x17<< 1))
#define REG_SC_VR_BK04_17_H                        (0x100*4 + ((0x17<< 1)+1))
#define REG_SC_VR_BK04_18_L                        (0x100*4 + (0x18<< 1))
#define REG_SC_VR_BK04_18_H                        (0x100*4 + ((0x18<< 1)+1))
#define REG_SC_VR_BK04_19_L                        (0x100*4 + (0x19<< 1))
#define REG_SC_VR_BK04_19_H                        (0x100*4 + ((0x19<< 1)+1))
#define REG_SC_VR_BK04_1A_L                        (0x100*4 + (0x1A<< 1))
#define REG_SC_VR_BK04_1A_H                        (0x100*4 + ((0x1A<< 1)+1))
#define REG_SC_VR_BK04_1B_L                        (0x100*4 + (0x1B<< 1))
#define REG_SC_VR_BK04_1B_H                        (0x100*4 + ((0x1B<< 1)+1))
#define REG_SC_VR_BK04_1C_L                        (0x100*4 + (0x1C<< 1))
#define REG_SC_VR_BK04_1C_H                        (0x100*4 + ((0x1C<< 1)+1))
#define REG_SC_VR_BK04_1D_L                        (0x100*4 + (0x1D<< 1))
#define REG_SC_VR_BK04_1D_H                        (0x100*4 + ((0x1D<< 1)+1))
#define REG_SC_VR_BK04_1E_L                        (0x100*4 + (0x1E<< 1))
#define REG_SC_VR_BK04_1E_H                        (0x100*4 + ((0x1E<< 1)+1))
#define REG_SC_VR_BK04_1F_L                        (0x100*4 + (0x1F<< 1))
#define REG_SC_VR_BK04_1F_H                        (0x100*4 + ((0x1F<< 1)+1))
#define REG_SC_VR_BK04_20_L                        (0x100*4 + (0x20<< 1))
#define REG_SC_VR_BK04_20_H                        (0x100*4 + ((0x20<< 1)+1))
#define REG_SC_VR_BK04_21_L                        (0x100*4 + (0x21<< 1))
#define REG_SC_VR_BK04_21_H                        (0x100*4 + ((0x21<< 1)+1))
#define REG_SC_VR_BK04_22_L                        (0x100*4 + (0x22<< 1))
#define REG_SC_VR_BK04_22_H                        (0x100*4 + ((0x22<< 1)+1))
#define REG_SC_VR_BK04_23_L                        (0x100*4 + (0x23<< 1))
#define REG_SC_VR_BK04_23_H                        (0x100*4 + ((0x23<< 1)+1))
#define REG_SC_VR_BK04_24_L                        (0x100*4 + (0x24<< 1))
#define REG_SC_VR_BK04_24_H                        (0x100*4 + ((0x24<< 1)+1))
#define REG_SC_VR_BK04_25_L                        (0x100*4 + (0x25<< 1))
#define REG_SC_VR_BK04_25_H                        (0x100*4 + ((0x25<< 1)+1))
#define REG_SC_VR_BK04_26_L                        (0x100*4 + (0x26<< 1))
#define REG_SC_VR_BK04_26_H                        (0x100*4 + ((0x26<< 1)+1))
#define REG_SC_VR_BK04_27_L                        (0x100*4 + (0x27<< 1))
#define REG_SC_VR_BK04_27_H                        (0x100*4 + ((0x27<< 1)+1))
#define REG_SC_VR_BK04_28_L                        (0x100*4 + (0x28<< 1))
#define REG_SC_VR_BK04_28_H                        (0x100*4 + ((0x28<< 1)+1))
#define REG_SC_VR_BK04_29_L                        (0x100*4 + (0x29<< 1))
#define REG_SC_VR_BK04_29_H                        (0x100*4 + ((0x29<< 1)+1))
#define REG_SC_VR_BK04_2A_L                        (0x100*4 + (0x2A<< 1))
#define REG_SC_VR_BK04_2A_H                        (0x100*4 + ((0x2A<< 1)+1))
#define REG_SC_VR_BK04_2B_L                        (0x100*4 + (0x2B<< 1))
#define REG_SC_VR_BK04_2B_H                        (0x100*4 + ((0x2B<< 1)+1))
#define REG_SC_VR_BK04_2C_L                        (0x100*4 + (0x2C<< 1))
#define REG_SC_VR_BK04_2C_H                        (0x100*4 + ((0x2C<< 1)+1))
#define REG_SC_VR_BK04_2D_L                        (0x100*4 + (0x2D<< 1))
#define REG_SC_VR_BK04_2D_H                        (0x100*4 + ((0x2D<< 1)+1))
#define REG_SC_VR_BK04_2E_L                        (0x100*4 + (0x2E<< 1))
#define REG_SC_VR_BK04_2E_H                        (0x100*4 + ((0x2E<< 1)+1))
#define REG_SC_VR_BK04_2F_L                        (0x100*4 + (0x2F<< 1))
#define REG_SC_VR_BK04_2F_H                        (0x100*4 + ((0x2F<< 1)+1))

#define REG_SC_VR_BK05_00_L                        (0x500 | (0x00<< 1))
#define REG_SC_VR_BK05_00_H                        (0x500 | ((0x00<< 1)+1))
#define REG_SC_VR_BK05_01_L                        (0x500 | (0x01<< 1))
#define REG_SC_VR_BK05_01_H                        (0x500 | ((0x01<< 1)+1))
#define REG_SC_VR_BK05_02_L                        (0x500 | (0x02<< 1))
#define REG_SC_VR_BK05_02_H                        (0x500 | ((0x02<< 1)+1))
#define REG_SC_VR_BK05_03_L                        (0x500 | (0x03<< 1))
#define REG_SC_VR_BK05_03_H                        (0x500 | ((0x03<< 1)+1))
#define REG_SC_VR_BK05_04_L                        (0x500 | (0x04<< 1))
#define REG_SC_VR_BK05_04_H                        (0x500 | ((0x04<< 1)+1))
#define REG_SC_VR_BK05_05_L                        (0x500 | (0x05<< 1))
#define REG_SC_VR_BK05_05_H                        (0x500 | ((0x05<< 1)+1))
#define REG_SC_VR_BK05_06_L                        (0x500 | (0x06<< 1))
#define REG_SC_VR_BK05_06_H                        (0x500 | ((0x06<< 1)+1))
#define REG_SC_VR_BK05_07_L                        (0x500 | (0x07<< 1))
#define REG_SC_VR_BK05_07_H                        (0x500 | ((0x07<< 1)+1))
#define REG_SC_VR_BK05_08_L                        (0x500 | (0x08<< 1))
#define REG_SC_VR_BK05_08_H                        (0x500 | ((0x08<< 1)+1))
#define REG_SC_VR_BK05_09_L                        (0x500 | (0x09<< 1))
#define REG_SC_VR_BK05_09_H                        (0x500 | ((0x09<< 1)+1))
#define REG_SC_VR_BK05_0A_L                        (0x500 | (0x0A<< 1))
#define REG_SC_VR_BK05_0A_H                        (0x500 | ((0x0A<< 1)+1))
#define REG_SC_VR_BK05_0B_L                        (0x500 | (0x0B<< 1))
#define REG_SC_VR_BK05_0B_H                        (0x500 | ((0x0B<< 1)+1))
#define REG_SC_VR_BK05_0C_L                        (0x500 | (0x0C<< 1))
#define REG_SC_VR_BK05_0C_H                        (0x500 | ((0x0C<< 1)+1))
#define REG_SC_VR_BK05_0D_L                        (0x500 | (0x0D<< 1))
#define REG_SC_VR_BK05_0D_H                        (0x500 | ((0x0D<< 1)+1))
#define REG_SC_VR_BK05_0E_L                        (0x500 | (0x0E<< 1))
#define REG_SC_VR_BK05_0E_H                        (0x500 | ((0x0E<< 1)+1))
#define REG_SC_VR_BK05_0F_L                        (0x500 | (0x0F<< 1))
#define REG_SC_VR_BK05_0F_H                        (0x500 | ((0x0F<< 1)+1))
#define REG_SC_VR_BK05_10_L                        (0x500 | (0x10<< 1))
#define REG_SC_VR_BK05_10_H                        (0x500 | ((0x10<< 1)+1))
#define REG_SC_VR_BK05_11_L                        (0x500 | (0x11<< 1))
#define REG_SC_VR_BK05_11_H                        (0x500 | ((0x11<< 1)+1))
#define REG_SC_VR_BK05_12_L                        (0x500 | (0x12<< 1))
#define REG_SC_VR_BK05_12_H                        (0x500 | ((0x12<< 1)+1))
#define REG_SC_VR_BK05_13_L                        (0x500 | (0x13<< 1))
#define REG_SC_VR_BK05_13_H                        (0x500 | ((0x13<< 1)+1))
#define REG_SC_VR_BK05_14_L                        (0x500 | (0x14<< 1))
#define REG_SC_VR_BK05_14_H                        (0x500 | ((0x14<< 1)+1))
#define REG_SC_VR_BK05_15_L                        (0x500 | (0x15<< 1))
#define REG_SC_VR_BK05_15_H                        (0x500 | ((0x15<< 1)+1))
#define REG_SC_VR_BK05_16_L                        (0x500 | (0x16<< 1))
#define REG_SC_VR_BK05_16_H                        (0x500 | ((0x16<< 1)+1))
#define REG_SC_VR_BK05_17_L                        (0x500 | (0x17<< 1))
#define REG_SC_VR_BK05_17_H                        (0x500 | ((0x17<< 1)+1))
#define REG_SC_VR_BK05_18_L                        (0x500 | (0x18<< 1))
#define REG_SC_VR_BK05_18_H                        (0x500 | ((0x18<< 1)+1))
#define REG_SC_VR_BK05_19_L                        (0x500 | (0x19<< 1))
#define REG_SC_VR_BK05_19_H                        (0x500 | ((0x19<< 1)+1))
#define REG_SC_VR_BK05_1A_L                        (0x500 | (0x1A<< 1))
#define REG_SC_VR_BK05_1A_H                        (0x500 | ((0x1A<< 1)+1))
#define REG_SC_VR_BK05_1B_L                        (0x500 | (0x1B<< 1))
#define REG_SC_VR_BK05_1B_H                        (0x500 | ((0x1B<< 1)+1))
#define REG_SC_VR_BK05_1C_L                        (0x500 | (0x1C<< 1))
#define REG_SC_VR_BK05_1C_H                        (0x500 | ((0x1C<< 1)+1))
#define REG_SC_VR_BK05_1D_L                        (0x500 | (0x1D<< 1))
#define REG_SC_VR_BK05_1D_H                        (0x500 | ((0x1D<< 1)+1))
#define REG_SC_VR_BK05_1E_L                        (0x500 | (0x1E<< 1))
#define REG_SC_VR_BK05_1E_H                        (0x500 | ((0x1E<< 1)+1))
#define REG_SC_VR_BK05_1F_L                        (0x500 | (0x1F<< 1))
#define REG_SC_VR_BK05_1F_H                        (0x500 | ((0x1F<< 1)+1))
#define REG_SC_VR_BK05_20_L                        (0x500 | (0x20<< 1))
#define REG_SC_VR_BK05_20_H                        (0x500 | ((0x20<< 1)+1))
#define REG_SC_VR_BK05_21_L                        (0x500 | (0x21<< 1))
#define REG_SC_VR_BK05_21_H                        (0x500 | ((0x21<< 1)+1))
#define REG_SC_VR_BK05_22_L                        (0x500 | (0x22<< 1))
#define REG_SC_VR_BK05_22_H                        (0x500 | ((0x22<< 1)+1))
#define REG_SC_VR_BK05_23_L                        (0x500 | (0x23<< 1))
#define REG_SC_VR_BK05_23_H                        (0x500 | ((0x23<< 1)+1))
#define REG_SC_VR_BK05_24_L                        (0x500 | (0x24<< 1))
#define REG_SC_VR_BK05_24_H                        (0x500 | ((0x24<< 1)+1))
#define REG_SC_VR_BK05_25_L                        (0x500 | (0x25<< 1))
#define REG_SC_VR_BK05_25_H                        (0x500 | ((0x25<< 1)+1))
#define REG_SC_VR_BK05_26_L                        (0x500 | (0x26<< 1))
#define REG_SC_VR_BK05_26_H                        (0x500 | ((0x26<< 1)+1))
#define REG_SC_VR_BK05_27_L                        (0x500 | (0x27<< 1))
#define REG_SC_VR_BK05_27_H                        (0x500 | ((0x27<< 1)+1))
#define REG_SC_VR_BK05_28_L                        (0x500 | (0x28<< 1))
#define REG_SC_VR_BK05_28_H                        (0x500 | ((0x28<< 1)+1))
#define REG_SC_VR_BK05_29_L                        (0x500 | (0x29<< 1))
#define REG_SC_VR_BK05_29_H                        (0x500 | ((0x29<< 1)+1))
#define REG_SC_VR_BK05_2A_L                        (0x500 | (0x2A<< 1))
#define REG_SC_VR_BK05_2A_H                        (0x500 | ((0x2A<< 1)+1))
#define REG_SC_VR_BK05_2B_L                        (0x500 | (0x2B<< 1))
#define REG_SC_VR_BK05_2B_H                        (0x500 | ((0x2B<< 1)+1))
#define REG_SC_VR_BK05_2C_L                        (0x500 | (0x2C<< 1))
#define REG_SC_VR_BK05_2C_H                        (0x500 | ((0x2C<< 1)+1))
#define REG_SC_VR_BK05_2D_L                        (0x500 | (0x2D<< 1))
#define REG_SC_VR_BK05_2D_H                        (0x500 | ((0x2D<< 1)+1))
#define REG_SC_VR_BK05_2E_L                        (0x500 | (0x2E<< 1))
#define REG_SC_VR_BK05_2E_H                        (0x500 | ((0x2E<< 1)+1))
#define REG_SC_VR_BK05_2F_L                        (0x500 | (0x2F<< 1))
#define REG_SC_VR_BK05_2F_H                        (0x500 | ((0x2F<< 1)+1))

#define REG_SC_VR_BK07_00_L                        (0x100*7 + (0x00<< 1))
#define REG_SC_VR_BK07_00_H                        (0x100*7 + ((0x00<< 1)+1))
#define REG_SC_VR_BK07_01_L                        (0x100*7 + (0x01<< 1))
#define REG_SC_VR_BK07_01_H                        (0x100*7 + ((0x01<< 1)+1))
#define REG_SC_VR_BK07_02_L                        (0x100*7 + (0x02<< 1))
#define REG_SC_VR_BK07_02_H                        (0x100*7 + ((0x02<< 1)+1))
#define REG_SC_VR_BK07_03_L                        (0x100*7 + (0x03<< 1))
#define REG_SC_VR_BK07_03_H                        (0x100*7 + ((0x03<< 1)+1))
#define REG_SC_VR_BK07_04_L                        (0x100*7 + (0x04<< 1))
#define REG_SC_VR_BK07_04_H                        (0x100*7 + ((0x04<< 1)+1))
#define REG_SC_VR_BK07_05_L                        (0x100*7 + (0x05<< 1))
#define REG_SC_VR_BK07_05_H                        (0x100*7 + ((0x05<< 1)+1))
#define REG_SC_VR_BK07_06_L                        (0x100*7 + (0x06<< 1))
#define REG_SC_VR_BK07_06_H                        (0x100*7 + ((0x06<< 1)+1))
#define REG_SC_VR_BK07_07_L                        (0x100*7 + (0x07<< 1))
#define REG_SC_VR_BK07_07_H                        (0x100*7 + ((0x07<< 1)+1))
#define REG_SC_VR_BK07_08_L                        (0x100*7 + (0x08<< 1))
#define REG_SC_VR_BK07_08_H                        (0x100*7 + ((0x08<< 1)+1))
#define REG_SC_VR_BK07_09_L                        (0x100*7 + (0x09<< 1))
#define REG_SC_VR_BK07_09_H                        (0x100*7 + ((0x09<< 1)+1))
#define REG_SC_VR_BK07_0A_L                        (0x100*7 + (0x0A<< 1))
#define REG_SC_VR_BK07_0A_H                        (0x100*7 + ((0x0A<< 1)+1))
#define REG_SC_VR_BK07_0B_L                        (0x100*7 + (0x0B<< 1))
#define REG_SC_VR_BK07_0B_H                        (0x100*7 + ((0x0B<< 1)+1))
#define REG_SC_VR_BK07_0C_L                        (0x100*7 + (0x0C<< 1))
#define REG_SC_VR_BK07_0C_H                        (0x100*7 + ((0x0C<< 1)+1))
#define REG_SC_VR_BK07_0D_L                        (0x100*7 + (0x0D<< 1))
#define REG_SC_VR_BK07_0D_H                        (0x100*7 + ((0x0D<< 1)+1))
#define REG_SC_VR_BK07_0E_L                        (0x100*7 + (0x0E<< 1))
#define REG_SC_VR_BK07_0E_H                        (0x100*7 + ((0x0E<< 1)+1))
#define REG_SC_VR_BK07_0F_L                        (0x100*7 + (0x0F<< 1))
#define REG_SC_VR_BK07_0F_H                        (0x100*7 + ((0x0F<< 1)+1))
#define REG_SC_VR_BK07_10_L                        (0x100*7 + (0x10<< 1))
#define REG_SC_VR_BK07_10_H                        (0x100*7 + ((0x10<< 1)+1))
#define REG_SC_VR_BK07_11_L                        (0x100*7 + (0x11<< 1))
#define REG_SC_VR_BK07_11_H                        (0x100*7 + ((0x11<< 1)+1))
#define REG_SC_VR_BK07_12_L                        (0x100*7 + (0x12<< 1))
#define REG_SC_VR_BK07_12_H                        (0x100*7 + ((0x12<< 1)+1))
#define REG_SC_VR_BK07_13_L                        (0x100*7 + (0x13<< 1))
#define REG_SC_VR_BK07_13_H                        (0x100*7 + ((0x13<< 1)+1))
#define REG_SC_VR_BK07_14_L                        (0x100*7 + (0x14<< 1))
#define REG_SC_VR_BK07_14_H                        (0x100*7 + ((0x14<< 1)+1))
#define REG_SC_VR_BK07_15_L                        (0x100*7 + (0x15<< 1))
#define REG_SC_VR_BK07_15_H                        (0x100*7 + ((0x15<< 1)+1))
#define REG_SC_VR_BK07_16_L                        (0x100*7 + (0x16<< 1))
#define REG_SC_VR_BK07_16_H                        (0x100*7 + ((0x16<< 1)+1))
#define REG_SC_VR_BK07_17_L                        (0x100*7 + (0x17<< 1))
#define REG_SC_VR_BK07_17_H                        (0x100*7 + ((0x17<< 1)+1))
#define REG_SC_VR_BK07_18_L                        (0x100*7 + (0x18<< 1))
#define REG_SC_VR_BK07_18_H                        (0x100*7 + ((0x18<< 1)+1))
#define REG_SC_VR_BK07_19_L                        (0x100*7 + (0x19<< 1))
#define REG_SC_VR_BK07_19_H                        (0x100*7 + ((0x19<< 1)+1))
#define REG_SC_VR_BK07_1A_L                        (0x100*7 + (0x1A<< 1))
#define REG_SC_VR_BK07_1A_H                        (0x100*7 + ((0x1A<< 1)+1))
#define REG_SC_VR_BK07_1B_L                        (0x100*7 + (0x1B<< 1))
#define REG_SC_VR_BK07_1B_H                        (0x100*7 + ((0x1B<< 1)+1))
#define REG_SC_VR_BK07_1C_L                        (0x100*7 + (0x1C<< 1))
#define REG_SC_VR_BK07_1C_H                        (0x100*7 + ((0x1C<< 1)+1))
#define REG_SC_VR_BK07_1D_L                        (0x100*7 + (0x1D<< 1))
#define REG_SC_VR_BK07_1D_H                        (0x100*7 + ((0x1D<< 1)+1))
#define REG_SC_VR_BK07_1E_L                        (0x100*7 + (0x1E<< 1))
#define REG_SC_VR_BK07_1E_H                        (0x100*7 + ((0x1E<< 1)+1))
#define REG_SC_VR_BK07_1F_L                        (0x100*7 + (0x1F<< 1))
#define REG_SC_VR_BK07_1F_H                        (0x100*7 + ((0x1F<< 1)+1))
#define REG_SC_VR_BK07_20_L                        (0x100*7 + (0x20<< 1))
#define REG_SC_VR_BK07_20_H                        (0x100*7 + ((0x20<< 1)+1))
#define REG_SC_VR_BK07_21_L                        (0x100*7 + (0x21<< 1))
#define REG_SC_VR_BK07_21_H                        (0x100*7 + ((0x21<< 1)+1))
#define REG_SC_VR_BK07_22_L                        (0x100*7 + (0x22<< 1))
#define REG_SC_VR_BK07_22_H                        (0x100*7 + ((0x22<< 1)+1))
#define REG_SC_VR_BK07_23_L                        (0x100*7 + (0x23<< 1))
#define REG_SC_VR_BK07_23_H                        (0x100*7 + ((0x23<< 1)+1))
#define REG_SC_VR_BK07_24_L                        (0x100*7 + (0x24<< 1))
#define REG_SC_VR_BK07_24_H                        (0x100*7 + ((0x24<< 1)+1))
#define REG_SC_VR_BK07_25_L                        (0x100*7 + (0x25<< 1))
#define REG_SC_VR_BK07_25_H                        (0x100*7 + ((0x25<< 1)+1))
#define REG_SC_VR_BK07_26_L                        (0x100*7 + (0x26<< 1))
#define REG_SC_VR_BK07_26_H                        (0x100*7 + ((0x26<< 1)+1))
#define REG_SC_VR_BK07_27_L                        (0x100*7 + (0x27<< 1))
#define REG_SC_VR_BK07_27_H                        (0x100*7 + ((0x27<< 1)+1))
#define REG_SC_VR_BK07_28_L                        (0x100*7 + (0x28<< 1))
#define REG_SC_VR_BK07_28_H                        (0x100*7 + ((0x28<< 1)+1))
#define REG_SC_VR_BK07_29_L                        (0x100*7 + (0x29<< 1))
#define REG_SC_VR_BK07_29_H                        (0x100*7 + ((0x29<< 1)+1))
#define REG_SC_VR_BK07_2A_L                        (0x100*7 + (0x2A<< 1))
#define REG_SC_VR_BK07_2A_H                        (0x100*7 + ((0x2A<< 1)+1))
#define REG_SC_VR_BK07_2B_L                        (0x100*7 + (0x2B<< 1))
#define REG_SC_VR_BK07_2B_H                        (0x100*7 + ((0x2B<< 1)+1))
#define REG_SC_VR_BK07_2C_L                        (0x100*7 + (0x2C<< 1))
#define REG_SC_VR_BK07_2C_H                        (0x100*7 + ((0x2C<< 1)+1))
#define REG_SC_VR_BK07_2D_L                        (0x100*7 + (0x2D<< 1))
#define REG_SC_VR_BK07_2D_H                        (0x100*7 + ((0x2D<< 1)+1))
#define REG_SC_VR_BK07_2E_L                        (0x100*7 + (0x2E<< 1))
#define REG_SC_VR_BK07_2E_H                        (0x100*7 + ((0x2E<< 1)+1))
#define REG_SC_VR_BK07_2F_L                        (0x100*7 + (0x2F<< 1))
#define REG_SC_VR_BK07_2F_H                        (0x100*7 + ((0x2F<< 1)+1))
#define REG_SC_VR_BK07_30_L                        (0x100*7 + (0x30<< 1))
#define REG_SC_VR_BK07_30_H                        (0x100*7 + ((0x30<< 1)+1))
#define REG_SC_VR_BK07_31_L                        (0x100*7 + (0x31<< 1))
#define REG_SC_VR_BK07_31_H                        (0x100*7 + ((0x31<< 1)+1))
#define REG_SC_VR_BK07_32_L                        (0x100*7 + (0x32<< 1))
#define REG_SC_VR_BK07_32_H                        (0x100*7 + ((0x32<< 1)+1))
#define REG_SC_VR_BK07_33_L                        (0x100*7 + (0x33<< 1))
#define REG_SC_VR_BK07_33_H                        (0x100*7 + ((0x33<< 1)+1))
#define REG_SC_VR_BK07_34_L                        (0x100*7 + (0x34<< 1))
#define REG_SC_VR_BK07_34_H                        (0x100*7 + ((0x34<< 1)+1))
#define REG_SC_VR_BK07_35_L                        (0x100*7 + (0x35<< 1))
#define REG_SC_VR_BK07_35_H                        (0x100*7 + ((0x35<< 1)+1))
#define REG_SC_VR_BK07_36_L                        (0x100*7 + (0x36<< 1))
#define REG_SC_VR_BK07_36_H                        (0x100*7 + ((0x36<< 1)+1))
#define REG_SC_VR_BK07_37_L                        (0x100*7 + (0x37<< 1))
#define REG_SC_VR_BK07_37_H                        (0x100*7 + ((0x37<< 1)+1))
#define REG_SC_VR_BK07_38_L                        (0x100*7 + (0x38<< 1))
#define REG_SC_VR_BK07_38_H                        (0x100*7 + ((0x38<< 1)+1))
#define REG_SC_VR_BK07_39_L                        (0x100*7 + (0x39<< 1))
#define REG_SC_VR_BK07_39_H                        (0x100*7 + ((0x39<< 1)+1))
#define REG_SC_VR_BK07_3A_L                        (0x100*7 + (0x3A<< 1))
#define REG_SC_VR_BK07_3A_H                        (0x100*7 + ((0x3A<< 1)+1))
#define REG_SC_VR_BK07_3B_L                        (0x100*7 + (0x3B<< 1))
#define REG_SC_VR_BK07_3B_H                        (0x100*7 + ((0x3B<< 1)+1))
#define REG_SC_VR_BK07_3C_L                        (0x100*7 + (0x3C<< 1))
#define REG_SC_VR_BK07_3C_H                        (0x100*7 + ((0x3C<< 1)+1))
#define REG_SC_VR_BK07_3D_L                        (0x100*7 + (0x3D<< 1))
#define REG_SC_VR_BK07_3D_H                        (0x100*7 + ((0x3D<< 1)+1))
#define REG_SC_VR_BK07_3E_L                        (0x100*7 + (0x3E<< 1))
#define REG_SC_VR_BK07_3E_H                        (0x100*7 + ((0x3E<< 1)+1))
#define REG_SC_VR_BK07_3F_L                        (0x100*7 + (0x3F<< 1))
#define REG_SC_VR_BK07_3F_H                        (0x100*7 + ((0x3F<< 1)+1))
#define REG_SC_VR_BK07_40_L                        (0x100*7 + (0x40<< 1))
#define REG_SC_VR_BK07_40_H                        (0x100*7 + ((0x40<< 1)+1))
#define REG_SC_VR_BK07_41_L                        (0x100*7 + (0x41<< 1))
#define REG_SC_VR_BK07_41_H                        (0x100*7 + ((0x41<< 1)+1))
#define REG_SC_VR_BK07_42_L                        (0x100*7 + (0x42<< 1))
#define REG_SC_VR_BK07_42_H                        (0x100*7 + ((0x42<< 1)+1))
#define REG_SC_VR_BK07_43_L                        (0x100*7 + (0x43<< 1))
#define REG_SC_VR_BK07_43_H                        (0x100*7 + ((0x43<< 1)+1))
#define REG_SC_VR_BK07_44_L                        (0x100*7 + (0x44<< 1))
#define REG_SC_VR_BK07_44_H                        (0x100*7 + ((0x44<< 1)+1))
#define REG_SC_VR_BK07_45_L                        (0x100*7 + (0x45<< 1))
#define REG_SC_VR_BK07_45_H                        (0x100*7 + ((0x45<< 1)+1))
#define REG_SC_VR_BK07_46_L                        (0x100*7 + (0x46<< 1))
#define REG_SC_VR_BK07_46_H                        (0x100*7 + ((0x46<< 1)+1))
#define REG_SC_VR_BK07_47_L                        (0x100*7 + (0x47<< 1))
#define REG_SC_VR_BK07_47_H                        (0x100*7 + ((0x47<< 1)+1))
#define REG_SC_VR_BK07_48_L                        (0x100*7 + (0x48<< 1))
#define REG_SC_VR_BK07_48_H                        (0x100*7 + ((0x48<< 1)+1))
#define REG_SC_VR_BK07_49_L                        (0x100*7 + (0x49<< 1))
#define REG_SC_VR_BK07_49_H                        (0x100*7 + ((0x49<< 1)+1))
#define REG_SC_VR_BK07_4A_L                        (0x100*7 + (0x4A<< 1))
#define REG_SC_VR_BK07_4A_H                        (0x100*7 + ((0x4A<< 1)+1))
#define REG_SC_VR_BK07_4B_L                        (0x100*7 + (0x4B<< 1))
#define REG_SC_VR_BK07_4B_H                        (0x100*7 + ((0x4B<< 1)+1))
#define REG_SC_VR_BK07_4C_L                        (0x100*7 + (0x4C<< 1))
#define REG_SC_VR_BK07_4C_H                        (0x100*7 + ((0x4C<< 1)+1))
#define REG_SC_VR_BK07_4D_L                        (0x100*7 + (0x4D<< 1))
#define REG_SC_VR_BK07_4D_H                        (0x100*7 + ((0x4D<< 1)+1))
#define REG_SC_VR_BK07_4E_L                        (0x100*7 + (0x4E<< 1))
#define REG_SC_VR_BK07_4E_H                        (0x100*7 + ((0x4E<< 1)+1))
#define REG_SC_VR_BK07_4F_L                        (0x100*7 + (0x4F<< 1))
#define REG_SC_VR_BK07_4F_H                        (0x100*7 + ((0x4F<< 1)+1))
#define REG_SC_VR_BK07_50_L                        (0x100*7 + (0x50<< 1))
#define REG_SC_VR_BK07_50_H                        (0x100*7 + ((0x50<< 1)+1))
#define REG_SC_VR_BK07_51_L                        (0x100*7 + (0x51<< 1))
#define REG_SC_VR_BK07_51_H                        (0x100*7 + ((0x51<< 1)+1))
#define REG_SC_VR_BK07_52_L                        (0x100*7 + (0x52<< 1))
#define REG_SC_VR_BK07_52_H                        (0x100*7 + ((0x52<< 1)+1))
#define REG_SC_VR_BK07_53_L                        (0x100*7 + (0x53<< 1))
#define REG_SC_VR_BK07_53_H                        (0x100*7 + ((0x53<< 1)+1))
#define REG_SC_VR_BK07_54_L                        (0x100*7 + (0x54<< 1))
#define REG_SC_VR_BK07_54_H                        (0x100*7 + ((0x54<< 1)+1))
#define REG_SC_VR_BK07_55_L                        (0x100*7 + (0x55<< 1))
#define REG_SC_VR_BK07_55_H                        (0x100*7 + ((0x55<< 1)+1))
#define REG_SC_VR_BK07_56_L                        (0x100*7 + (0x56<< 1))
#define REG_SC_VR_BK07_56_H                        (0x100*7 + ((0x56<< 1)+1))
#define REG_SC_VR_BK07_57_L                        (0x100*7 + (0x57<< 1))
#define REG_SC_VR_BK07_57_H                        (0x100*7 + ((0x57<< 1)+1))
#define REG_SC_VR_BK07_58_L                        (0x100*7 + (0x58<< 1))
#define REG_SC_VR_BK07_58_H                        (0x100*7 + ((0x58<< 1)+1))
#define REG_SC_VR_BK07_59_L                        (0x100*7 + (0x59<< 1))
#define REG_SC_VR_BK07_59_H                        (0x100*7 + ((0x59<< 1)+1))
#define REG_SC_VR_BK07_5A_L                        (0x100*7 + (0x5A<< 1))
#define REG_SC_VR_BK07_5A_H                        (0x100*7 + ((0x5A<< 1)+1))
#define REG_SC_VR_BK07_5B_L                        (0x100*7 + (0x5B<< 1))
#define REG_SC_VR_BK07_5B_H                        (0x100*7 + ((0x5B<< 1)+1))
#define REG_SC_VR_BK07_5C_L                        (0x100*7 + (0x5C<< 1))
#define REG_SC_VR_BK07_5C_H                        (0x100*7 + ((0x5C<< 1)+1))
#define REG_SC_VR_BK07_5D_L                        (0x100*7 + (0x5D<< 1))
#define REG_SC_VR_BK07_5D_H                        (0x100*7 + ((0x5D<< 1)+1))
#define REG_SC_VR_BK07_5E_L                        (0x100*7 + (0x5E<< 1))
#define REG_SC_VR_BK07_5E_H                        (0x100*7 + ((0x5E<< 1)+1))
#define REG_SC_VR_BK07_5F_L                        (0x100*7 + (0x5F<< 1))
#define REG_SC_VR_BK07_5F_H                        (0x100*7 + ((0x5F<< 1)+1))
#define REG_SC_VR_BK07_60_L                        (0x100*7 + (0x60<< 1))
#define REG_SC_VR_BK07_60_H                        (0x100*7 + ((0x60<< 1)+1))
#define REG_SC_VR_BK07_61_L                        (0x100*7 + (0x61<< 1))
#define REG_SC_VR_BK07_61_H                        (0x100*7 + ((0x61<< 1)+1))
#define REG_SC_VR_BK07_62_L                        (0x100*7 + (0x62<< 1))
#define REG_SC_VR_BK07_62_H                        (0x100*7 + ((0x62<< 1)+1))
#define REG_SC_VR_BK07_63_L                        (0x100*7 + (0x63<< 1))
#define REG_SC_VR_BK07_63_H                        (0x100*7 + ((0x63<< 1)+1))
#define REG_SC_VR_BK07_64_L                        (0x100*7 + (0x64<< 1))
#define REG_SC_VR_BK07_64_H                        (0x100*7 + ((0x64<< 1)+1))
#define REG_SC_VR_BK07_65_L                        (0x100*7 + (0x65<< 1))
#define REG_SC_VR_BK07_65_H                        (0x100*7 + ((0x65<< 1)+1))
#define REG_SC_VR_BK07_66_L                        (0x100*7 + (0x66<< 1))
#define REG_SC_VR_BK07_66_H                        (0x100*7 + ((0x66<< 1)+1))
#define REG_SC_VR_BK07_67_L                        (0x100*7 + (0x67<< 1))
#define REG_SC_VR_BK07_67_H                        (0x100*7 + ((0x67<< 1)+1))
#define REG_SC_VR_BK07_68_L                        (0x100*7 + (0x68<< 1))
#define REG_SC_VR_BK07_68_H                        (0x100*7 + ((0x68<< 1)+1))
#define REG_SC_VR_BK07_69_L                        (0x100*7 + (0x69<< 1))
#define REG_SC_VR_BK07_69_H                        (0x100*7 + ((0x69<< 1)+1))
#define REG_SC_VR_BK07_6A_L                        (0x100*7 + (0x6A<< 1))
#define REG_SC_VR_BK07_6A_H                        (0x100*7 + ((0x6A<< 1)+1))
#define REG_SC_VR_BK07_6B_L                        (0x100*7 + (0x6B<< 1))
#define REG_SC_VR_BK07_6B_H                        (0x100*7 + ((0x6B<< 1)+1))
#define REG_SC_VR_BK07_6C_L                        (0x100*7 + (0x6C<< 1))
#define REG_SC_VR_BK07_6C_H                        (0x100*7 + ((0x6C<< 1)+1))
#define REG_SC_VR_BK07_6D_L                        (0x100*7 + (0x6D<< 1))
#define REG_SC_VR_BK07_6D_H                        (0x100*7 + ((0x6D<< 1)+1))
#define REG_SC_VR_BK07_6E_L                        (0x100*7 + (0x6E<< 1))
#define REG_SC_VR_BK07_6E_H                        (0x100*7 + ((0x6E<< 1)+1))
#define REG_SC_VR_BK07_6F_L                        (0x100*7 + (0x6F<< 1))
#define REG_SC_VR_BK07_6F_H                        (0x100*7 + ((0x6F<< 1)+1))
#define REG_SC_VR_BK07_70_L                        (0x100*7 + (0x70<< 1))
#define REG_SC_VR_BK07_70_H                        (0x100*7 + ((0x70<< 1)+1))
#define REG_SC_VR_BK07_71_L                        (0x100*7 + (0x71<< 1))
#define REG_SC_VR_BK07_71_H                        (0x100*7 + ((0x71<< 1)+1))
#define REG_SC_VR_BK07_72_L                        (0x100*7 + (0x72<< 1))
#define REG_SC_VR_BK07_72_H                        (0x100*7 + ((0x72<< 1)+1))
#define REG_SC_VR_BK07_73_L                        (0x100*7 + (0x73<< 1))
#define REG_SC_VR_BK07_73_H                        (0x100*7 + ((0x73<< 1)+1))
#define REG_SC_VR_BK07_74_L                        (0x100*7 + (0x74<< 1))
#define REG_SC_VR_BK07_74_H                        (0x100*7 + ((0x74<< 1)+1))
#define REG_SC_VR_BK07_75_L                        (0x100*7 + (0x75<< 1))
#define REG_SC_VR_BK07_75_H                        (0x100*7 + ((0x75<< 1)+1))
#define REG_SC_VR_BK07_76_L                        (0x100*7 + (0x76<< 1))
#define REG_SC_VR_BK07_76_H                        (0x100*7 + ((0x76<< 1)+1))
#define REG_SC_VR_BK07_77_L                        (0x100*7 + (0x77<< 1))
#define REG_SC_VR_BK07_77_H                        (0x100*7 + ((0x77<< 1)+1))
#define REG_SC_VR_BK07_78_L                        (0x100*7 + (0x78<< 1))
#define REG_SC_VR_BK07_78_H                        (0x100*7 + ((0x78<< 1)+1))
#define REG_SC_VR_BK07_79_L                        (0x100*7 + (0x79<< 1))
#define REG_SC_VR_BK07_79_H                        (0x100*7 + ((0x79<< 1)+1))
#define REG_SC_VR_BK07_7A_L                        (0x100*7 + (0x7A<< 1))
#define REG_SC_VR_BK07_7A_H                        (0x100*7 + ((0x7A<< 1)+1))
#define REG_SC_VR_BK07_7B_L                        (0x100*7 + (0x7B<< 1))
#define REG_SC_VR_BK07_7B_H                        (0x100*7 + ((0x7B<< 1)+1))
#define REG_SC_VR_BK07_7C_L                        (0x100*7 + (0x7C<< 1))
#define REG_SC_VR_BK07_7C_H                        (0x100*7 + ((0x7C<< 1)+1))
#define REG_SC_VR_BK07_7D_L                        (0x100*7 + (0x7D<< 1))
#define REG_SC_VR_BK07_7D_H                        (0x100*7 + ((0x7D<< 1)+1))
#define REG_SC_VR_BK07_7E_L                        (0x100*7 + (0x7E<< 1))
#define REG_SC_VR_BK07_7E_H                        (0x100*7 + ((0x7E<< 1)+1))
#define REG_SC_VR_BK07_7F_L                        (0x100*7 + (0x7F<< 1))
#define REG_SC_VR_BK07_7F_H                        (0x100*7 + ((0x7F<< 1)+1))


#define REG_HDMI_DUAL_0_BASE                    0x173000UL
#define REG_HDMI2_DUAL_0_BASE                   0x173100UL
#define REG_HDMI_DUAL_0_30_L                    (REG_HDMI_DUAL_0_BASE + 0x60)
#define REG_HDMI_DUAL_0_31_L                    (REG_HDMI_DUAL_0_BASE + 0x62)
#define REG_HDMI_DUAL_0_32_L                    (REG_HDMI_DUAL_0_BASE + 0x64)
#define REG_HDMI_DUAL_0_33_L                    (REG_HDMI_DUAL_0_BASE + 0x66)
#define REG_HDMI_DUAL_0_34_L                    (REG_HDMI_DUAL_0_BASE + 0x68)
#define REG_HDMI_DUAL_0_35_L                    (REG_HDMI_DUAL_0_BASE + 0x6A)
#define REG_HDMI_DUAL_0_36_L                    (REG_HDMI_DUAL_0_BASE + 0x6C)
#define REG_HDMI_DUAL_0_37_L                    (REG_HDMI_DUAL_0_BASE + 0x6E)
#define REG_HDMI_DUAL_0_38_L                    (REG_HDMI_DUAL_0_BASE + 0x70)
#define REG_HDMI_DUAL_0_39_L                    (REG_HDMI_DUAL_0_BASE + 0x72)
#define REG_HDMI_DUAL_0_3A_L                    (REG_HDMI_DUAL_0_BASE + 0x74)
#define REG_HDMI_DUAL_0_3B_L                    (REG_HDMI_DUAL_0_BASE + 0x76)
#define REG_HDMI_DUAL_0_3C_L                    (REG_HDMI_DUAL_0_BASE + 0x78)
#define REG_HDMI_DUAL_0_3D_L                    (REG_HDMI_DUAL_0_BASE + 0x7A)
#define REG_HDMI_DUAL_0_40_L                    (REG_HDMI_DUAL_0_BASE + 0x80)

#define REG_HDMI2_DUAL_0_61_L                   (REG_HDMI2_DUAL_0_BASE + 0xC2)
#define DOLBY_VSIF_LEN                          0x18
#define DOLBY_VSIF_PB_VALUE                     0x0

#define _BIT0                                   (0x0001)
#define _BIT1                                   (0x0002)
#define _BIT2                                   (0x0004)
#define _BIT3                                   (0x0008)
#define _BIT4                                   (0x0010)
#define _BIT5                                   (0x0020)
#define _BIT6                                   (0x0040)
#define _BIT7                                   (0x0080)

#define L_BK_VOP(_x_)                           (REG_SCALER_BASE | (REG_SC_BK_VOP << 8) | (_x_ << 1))
#define L_BK_DLC(_x_)                           (REG_SCALER_BASE | (REG_SC_BK_DLC << 8) | (_x_ << 1))
#define H_BK_DLC(_x_)                           (REG_SCALER_BASE | (REG_SC_BK_DLC << 8) | ((_x_ << 1)+1))
#define L_BK_OP(_x_)                            (REG_SCALER_BASE | (REG_SC_BK_OP << 8) | (_x_ << 1))

#define REG_SCALER_BASE                         0x130000
#define REG_SC_BK_VOP                           0x10
#define REG_SC_BK_DLC                           0x1A
#define REG_SC_BK_OP                            0x20

#define REG_BK20_08_L                          (0x2000 | 0x08<< 1)
#define REG_BK20_28_L                          (0x2000 | 0x28<< 1)
#define REG_BK20_2C_L                          (0x2000 | 0x2C<< 1)

#if defined(CONFIG_ARM64)
extern ptrdiff_t mstar_pm_base;
#define REG_ADDR(addr)                          (*((volatile unsigned short int*)(mstar_pm_base + (addr << 1))))
#else
#define REG_ADDR(addr)                          (*((volatile unsigned short int*)(0xFD000000 + (addr << 1))))
#endif

#if XC_FRC_R2_SW_TOGGLE
#define REG_FRC_CPUINT_BASE                     (REG_INT_FRC_BASE + (0x640UL<<1))
#define FRC_CPU_INT_REG(address)                (*((volatile MS_U16 *)(REG_FRC_CPUINT_BASE + ((address)<<2) )))
#define REG_FRCINT_HKCPUFIRE                    0x0000UL //hst0 to hst1
#define INT_HKCPU_FRCR2_INPUT_SYNC              BIT1
#define INT_HKCPU_FRCR2_OUTPUT_SYNC             BIT2
#endif

// read 2 byte
#define REG_RR(_reg_)                           ({REG_ADDR(_reg_);})

// write low byte
#define REG_WL(_reg_, _val_)    \
        do{ REG_ADDR(_reg_) = (REG_ADDR(_reg_) & 0xFF00) | ((_val_) & 0x00FF); }while(0)

#define REG_WLMSK(_reg_, _val_, mask)    \
                    do{ REG_ADDR(_reg_) = (REG_ADDR(_reg_) & (0xFFFF - mask)) | (((_val_) & (mask)) & 0x00FF); }while(0)

// write high byte
#define REG_WH(_reg_, _val_)    \
        do{ REG_ADDR(_reg_) = (REG_ADDR(_reg_) & 0x00FF) | ((_val_) << 8); }while(0)

#define REG_WHMSK(_reg_, _val_, mask)    \
        do{ REG_ADDR(_reg_) = (REG_ADDR(_reg_) & (0xFFFF - (mask << 8))) | (((_val_) & (mask)) << 8); }while(0)

// write 2 byte
#define REG_W2B(_reg_, _val_)    \
        do{ REG_ADDR(_reg_) =(_val_) ; }while(0)

#define MAIN_WINDOW                             0
#define SUB_WINDOW                              1
#define msDlc_FunctionExit()
#define msDlc_FunctionEnter()

#define DLC_DEFLICK_BLEND_FACTOR                32UL
#define XC_DLC_ALGORITHM_KERNEL                 2

#define XC_DLC_SET_DLC_CURVE_BOTH_SAME          0   // 0:Main and Sub use the same DLC curve
#endif
// MIU Word (Bytes)
#define BYTE_PER_WORD                           (32)  // MIU 128: 16Byte/W, MIU 256: 32Byte/W
#define XC_HDR_DOLBY_PACKET_LENGTH              (128) //128 byte
#define XC_HDR_DOLBY_PACKET_HEADER              (3)
#define XC_HDR_DOLBY_PACKET_TAIL                (4)
#define XC_HDR_DOLBY_METADATA_LENGTH_BIT        (2) // 2 front bytes of dolby packet body is metadata length
#define XC_HDR_HW_SUPPORT_MAX_DOLBY_PACKET      (8)

/// FIX ME:consider sub window HDR in the future
/// when OTT is pausing, DS may not is processing index 0(with 3D-LUT).
/// User switch 3D-LUT won't work by DS this momnet.
/// so stop IP DS to stop ADL in DS first, then use ADL fire 3D-LUT manually,
/// restore DS status in the end.
#define OTT_AUTO_PAUSE_DETECT                   (1)
#define HDMI_AUTO_NOSIGNAL_DETECT             (1)


//-------------------------------------------------------------------------------------------------
//  Type and Structure
//-------------------------------------------------------------------------------------------------
typedef enum
{
    E_KDRV_XC_HDR_PATH_NONE,
    E_KDRV_XC_HDR_PATH_DOLBY_HDMI,
    E_KDRV_XC_HDR_PATH_DOLBY_OTT_SINGLE,
    E_KDRV_XC_HDR_PATH_DOLBY_OTT_DUAL,
    E_KDRV_XC_HDR_PATH_OPEN_HDMI,
    E_KDRV_XC_HDR_PATH_OPEN_OTT,
    E_KDRV_XC_HDR_PATH_ORIGIN,
    E_KDRV_XC_HDR_PATH_NON_HDR_HDMI,  // This is used when non HDR HDMI input source.
    E_KDRV_XC_HDR_PATH_DOLBY_OTT_UI_OFF,
    E_KDRV_XC_HDR_PATH_DOLBY_HDMI_UI_OFF,
    E_KDRV_XC_HDR_PATH_TCH_MVOP,
    E_KDRV_XC_HDR_PATH_MAX,
} EN_KDRV_XC_HDR_PATH;

// Communcation format of metadata with VDEC and XC driver.
// (Shared memory format):
// 32
// | Version | Current_Index | DM_Length | DM_Addr | DM_MIU_NO | Composer_Length | Composer_Addr | Composer_MIU_NO | Comp_En |Reserved_Area |
//    1byte        1byte         4byte      4byte     1byte         4byte              4byte            1byte         1byte       11byte
//

typedef struct
{
    MS_U8 u8CurrentIndex;       /// current metadata index, which is the same as DS index if DS is turned on.
    MS_U32 u32DmLength;         /// display management metadata length
    MS_U32 u32DmAddr;           /// where display management metadata lies
    MS_U8 u8DmMiuNo;            /// dm miu no
    MS_U32 u32ComposerLength;   /// Composer data length
    MS_U32 u32ComposerAddr;     /// where composer data lies
    MS_U8 u8ComposerMiuNo;      /// composer miu no
    MS_BOOL bEnableComposer;    /// composer enable
} ST_KDRV_XC_HDR_DOLBY_MEMORY_FORMAT;

typedef struct
{
    MS_U8 u8CurrentIndex;
    MS_U8 u8InputFormat;
    MS_U8 u8InputDataFormat;
    MS_BOOL u8Video_Full_Range_Flag;

    MS_BOOL bVUIValid;
    MS_U8 u8Colour_primaries;
    MS_U8 u8Transfer_Characteristics;
    MS_U8 u8Matrix_Coeffs;

    MS_BOOL bSEIValid;
    MS_U16 u16Display_Primaries_x[3];
    MS_U16 u16Display_Primaries_y[3];
    MS_U16 u16White_point_x;
    MS_U16 u16White_point_y;
    MS_U32 u32Master_Panel_Max_Luminance;
    MS_U32 u32Master_Panel_Min_Luminance;

    MS_BOOL bContentLightLevelEnabled;
    MS_U16  u16maxContentLightLevel;
    MS_U16  u16maxPicAverageLightLevel;
} ST_KDRV_XC_HDR_CFD_MEMORY_FORMAT;

typedef struct
{
    MS_U8 u8Version;
    union
    {
        ST_KDRV_XC_HDR_DOLBY_MEMORY_FORMAT stHDRMemFormatDolby;
        ST_KDRV_XC_HDR_CFD_MEMORY_FORMAT  stHDRMemFormatCFD;
    } HDRMemFormat;

} ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info;

typedef struct
{
    STU_CFDAPI_Maserati_DLCIP stMaseratiDLCParam;
} ST_KDRV_XC_CFD_MS_ALG_INTERFACE_DLC;

typedef struct
{
    MS_U8 u8Controls;
    // E_CFD_MC_MODE
    // 0 : bypass
    // 1 : normal
    // 2 : test
    STU_CFDAPI_Maserati_TMOIP    stMaseratiTMOParam;
} ST_KDRV_XC_CFD_MS_ALG_INTERFACE_TMO;

typedef struct
{
    STU_CFDAPI_Maserati_HDRIP  stMaseratiHDRIPParam;
} ST_KDRV_XC_CFD_MS_ALG_INTERFACE_HDRIP;

typedef struct
{
    MS_U8 u8Controls;
    // E_CFD_MC_MODE
    // 0 : bypass
    // 1 : normal
    // 2 : test
    STU_CFDAPI_Maserati_SDRIP  stMaseratiSDRIPParam;
} ST_KDRV_XC_CFD_MS_ALG_INTERFACE_SDRIP;

typedef struct
{
    //Main sub control mode
    MS_U8 u8HWMainSubMode;
    //0: current control is for SC0 (Main)
    //1: current control is for SC1 (Sub)

    //2-255 is reversed

    ST_KDRV_XC_CFD_MS_ALG_INTERFACE_DLC stDLCInput;
    ST_KDRV_XC_CFD_MS_ALG_INTERFACE_TMO stTMOInput;
    ST_KDRV_XC_CFD_MS_ALG_INTERFACE_HDRIP stHDRIPInput;
    ST_KDRV_XC_CFD_MS_ALG_INTERFACE_SDRIP stSDRIPInput;
} ST_KDRV_XC_CFD_HW_IPS;


#define CFD_INIT_VERSION 0
#define CFD_HDMI_VERSION 0
/// CFD Panel structure version
/// Version 0: Init structure
/// Version 1: Add Linear RGB setting and customer color primaries.
/// Version 2: Add PWM port & dolby global dimming on/off & dolby global delay frame count.
#define CFD_PANEL_VERSION 2
#define CFD_ANALOG_VERSION 0
#define CFD_HDR_VERSION 0
#define CFD_EDID_VERSION 0
/// CFD OSD structure version
/// Version 0: Init structure
/// Version 1: Add backlight and hue/contrast/saturation valid flag
/// Version 2: Add color range and main/sub window.
/// Version 3: Add ultra black / white
/// Version 4: Add skip picture setting (hue/ contrast/ saturation)
/// Version 5: Add color correction matrix
/// Version 6: Add view Mode (switch view mode for dolby)
#define CFD_OSD_VERSION 6
#define CFD_LINEAR_RGB_VERSION 0
#define CFD_FIRE_VERSION 0
/// Version 0: Init structure
/// Version 1: Add content is full range and ultra black & white active
/// Version 2: Add color type of the source
/// Version 3: Add HDR metadata of the source(ott or hdmi)
#define CFD_STATUS_VERSION 3
#define GD_BUFFER_SIZE (3 * DS_BUFFER_NUM_EX)
#define CFD_PICTURE_ATTRIBUTE_VERTION 0

/// CFD color format enum.
typedef enum
{
    /// RGB not specified
    E_KDRV_XC_CFD_FORMAT_RGB_NOT_SPECIFIED = 0x00,
    /// RGB BT.601_625
    E_KDRV_XC_CFD_FORMAT_RGB_BT601_625 = 0x01,
    /// RGB BT.601_525
    E_KDRV_XC_CFD_FORMAT_RGB_BT601_525 = 0x02,
    /// RGB BT.709
    E_KDRV_XC_CFD_FORMAT_RGB_BT709 = 0x03,
    /// RGB BT.2020
    E_KDRV_XC_CFD_FORMAT_RGB_BT2020 = 0x04,
    /// sRGB
    E_KDRV_XC_CFD_FORMAT_SRGB = 0x05,
    /// Adobe RGB
    E_KDRV_XC_CFD_FORMAT_ADOBE_RGB = 0x06,
    /// YUV net specified
    E_KDRV_XC_CFD_FORMAT_YUV_NOT_SPECIFIED = 0x07,
    /// YUV BT.601_625
    E_KDRV_XC_CFD_FORMAT_YUV_BT601_625 = 0x08,
    /// YUV BT.601_525
    E_KDRV_XC_CFD_FORMAT_YUV_BT601_525 = 0x09,
    /// YUV BT.709
    E_KDRV_XC_CFD_FORMAT_YUV_BT709 = 0x0a,
    /// YUV BT.2020 NCL
    E_KDRV_XC_CFD_FORMAT_YUV_BT2020NCL = 0x0b,
    /// YUV BT.2020 CL
    E_KDRV_XC_CFD_FORMAT_YUV_BT2020CL = 0x0c,
    /// xvYCC 601
    E_KDRV_XC_CFD_FORMAT_XVYCC_601 = 0x0d,
    /// xvYCC 709
    E_KDRV_XC_CFD_FORMAT_XVYCC_709 = 0x0e,
    /// sYCC 601
    E_KDRV_XC_CFD_FORMAT_SYCC_601 = 0x0f,
    /// Adobe YCC 601
    E_KDRV_XC_CFD_FORMAT_ADOBE_YCC601 = 0x10,
    /// HDR
    E_KDRV_XC_CFD_FORMAT_DOLBY_HDR_TEMP = 0x11,
    /// Reserved
    E_KDRV_XC_CFD_FORMAT_RESERVED_START,
    /// MAX
    E_KDRV_XC_CFD_FORMAT_MAX
} EN_KDRV_XC_CFD_COLOR_FORMAT;

/// CFD Color data format enum.
typedef enum
{
    /// RGB
    E_KDRV_XC_CFD_DATA_FORMAT_RGB = 0,
    /// YUV422
    E_KDRV_XC_CFD_DATA_FORMAT_YUV422 = 1,
    /// YUV444
    E_KDRV_XC_CFD_DATA_FORMAT_YUV444 = 2,
    /// YUV420
    E_KDRV_XC_CFD_DATA_FORMAT_YUV420 = 3,
    /// MAX
    E_KDRV_XC_CFD_DATA_FORMAT_MAX
} EN_KDRV_XC_CFD_COLOR_DATA_FORMAT;

/// HDR type
typedef enum
{
    /// None HDR
    E_KDRV_XC_CFD_HDR_TYPE_NONE = 0x0000,
    /// Dolby HDR
    E_KDRV_XC_CFD_HDR_TYPE_DOLBY = 0x0001,
    /// Open HDR (HDR10)
    E_KDRV_XC_CFD_HDR_TYPE_OPEN = 0x0002,
    /// TCH
    E_KDRV_XC_CFD_HDR_TYPE_TCH = 0x0004,
    /// HLG
    E_KDRV_XC_CFD_HDR_TYPE_HLG = 0x0008,
    /// OTT Open HDR in DV (Internal use)
    E_KDRV_XC_CFD_HDR_TYPE_OPEN_OTT_IN_DOLBY = 0x0100,
    /// HDMI Open HDR in DV (Internal use)
    E_KDRV_XC_CFD_HDR_TYPE_OPEN_HDMI_IN_DOLBY = 0x0101,
    /// Max
    E_KDRV_XC_CFD_HDR_TYPE_MAX = 0xffff
} EN_KDRV_XC_CFD_HDR_TYPE;

/// Update type
typedef enum
{
    /// Update all
    E_KDRV_XC_CFD_UPDATE_TYPE_ALL,
    /// Update OSD only
    E_KDRV_XC_CFD_UPDATE_TYPE_OSD_ONLY,
    /// Max
    E_KDRV_XC_CFD_UPDATE_TYPE_MAX
} EN_KDRV_XC_CFD_UPDATE_TYPE;

/// Picture Attribute control type
typedef enum
{
    E_KDRV_XC_CFD_PICTURE_ATTR_GET_HUE,
    E_KDRV_XC_CFD_PICTURE_ATTR_GET_SAT,
    E_KDRV_XC_CFD_PICTURE_ATTR_MAX
} EN_KDRV_XC_CFD_PICTURE_ATTR_CTRL_TYPE;

typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Window (main or sub window)
    MS_U8 u8Win;
    /// Input source
    MS_U8 u8InputSource;

    /// Is Get ShareMemory Position From VDEC stream ID
    MS_BOOL bIsSelectFromVdecID;
    /// Vdec Stream ID
    MS_U32  u32VdecStreamID;
} ST_KDRV_XC_CFD_INIT;

typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Window (main or sub window)
    MS_U8 u8Win;

    /// Full range
    MS_BOOL bIsFullRange;

    /// AVI infoframe
    /// Pixel Format
    MS_U8 u8PixelFormat;
    /// Color imetry
    MS_U8 u8Colorimetry;
    /// Extended Color imetry
    MS_U8 u8ExtendedColorimetry;
    /// Rgb Quantization Range
    MS_U8 u8RgbQuantizationRange;
    /// Ycc Quantization Range
    MS_U8 u8YccQuantizationRange;

    /// HDR infoframe
    /// HDR infoframe valid
    MS_BOOL bHDRInfoFrameValid;
    /// EOTF (/// 0:SDR gamma, 1:HDR gamma, 2:SMPTE ST2084, 3:Future EOTF, 4-7:Reserved)
    MS_U8 u8EOTF;
    /// Static metadata ID (0: Static Metadata Type 1, 1-7:Reserved for future use)
    MS_U8 u8SMDID;
    /// Display primaries x
    MS_U16 u16Display_Primaries_x[3];
    /// Display primaries y
    MS_U16 u16Display_Primaries_y[3];
    /// White point x
    MS_U16 u16White_point_x;
    /// White point y
    MS_U16 u16White_point_y;
    /// Panel max luminance
    MS_U16 u16MasterPanelMaxLuminance;
    /// Panel min luminance
    MS_U16 u16MasterPanelMinLuminance;
    /// Max content light level
    MS_U16 u16MaxContentLightLevel;
    /// Max frame average light level
    MS_U16 u16MaxFrameAvgLightLevel;

} ST_KDRV_XC_CFD_HDMI;

typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Window (main or sub window)
    MS_U8 u8Win;

    /// Color format, reference EN_KDRV_XC_CFD_COLOR_FORMAT.
    MS_U8 u8ColorFormat;
    /// Color data format, reference EN_KDRV_XC_CFD_COLOR_DATA_FORMAT.
    MS_U8 u8ColorDataFormat;
    /// Full range
    MS_BOOL bIsFullRange;
    /// Color primaries
    MS_U8 u8ColorPrimaries;
    /// Transfer characteristics
    MS_U8 u8TransferCharacteristics;
    /// Matrix coefficients
    MS_U8 u8MatrixCoefficients;

} ST_KDRV_XC_CFD_ANALOG;

typedef struct
{
    /// Structure version, refer to CFD_PANEL_VERSION
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Color format, reference EN_KDRV_XC_CFD_COLOR_FORMAT.
    MS_U8 u8ColorFormat;
    /// Color data format, reference EN_KDRV_XC_CFD_COLOR_DATA_FORMAT.
    MS_U8 u8ColorDataFormat;
    /// Full range
    MS_BOOL bIsFullRange;

    /// Display primaries x, data *0.00002 0xC350 = 1
    MS_U16 u16Display_Primaries_x[3];
    /// Display primaries y, data *0.00002 0xC350 = 1
    MS_U16 u16Display_Primaries_y[3];
    /// White point x, data *0.00002 0xC350 = 1
    MS_U16 u16White_point_x;
    /// White point y, data *0.00002 0xC350 = 1
    MS_U16 u16White_point_y;
    /// Max luminance, data * 1 nits
    MS_U16 u16MaxLuminance;
    /// Med luminance, data * 1 nits
    MS_U16 u16MedLuminance;
    /// Min luminance, data * 0.0001 nits
    MS_U16 u16MinLuminance;
    /// Linear RGB
    MS_BOOL bLinearRgb;
    /// Customer color primaries
    MS_BOOL bCustomerColorPrimaries;
    /// Source white x
    MS_U16 u16SourceWx;
    /// Source white y
    MS_U16 u16SourceWy;
    /// PWM port
    MS_U8 u8PWMPort;
    /// dolby global dimming on/off
    MS_BOOL bGlobalDimming;
    /// dolby global delay frame count
    MS_S8 s8DelayFrame;
} ST_KDRV_XC_CFD_PANEL;

typedef struct
{
    MS_U32 u32Version;   ///<Version of current structure. Please always set to "CFD_HDMI_EDID_ST_VERSION" as input
    MS_U16 u16Length;    ///<Length of this structure, u16Length=sizeof(STU_CFDAPI_HDMI_EDID_PARSER)

    MS_U8 u8HDMISinkHDRDataBlockValid;
    //assign by E_CFD_VALIDORNOT
    //0 :Not valid
    //1 :valid

    MS_U8 u8HDMISinkEOTF;
    //byte 3 in HDR static Metadata Data block

    MS_U8 u8HDMISinkSM;
    //byte 4 in HDR static Metadata Data block

    MS_U8 u8HDMISinkDesiredContentMaxLuminance;           //need a LUT to transfer
    MS_U8 u8HDMISinkDesiredContentMaxFrameAvgLuminance; //need a LUT to transfer
    MS_U8 u8HDMISinkDesiredContentMinLuminance;           //need a LUT to transfer
    //byte 5 ~ 7 in HDR static Metadata Data block

    MS_U8 u8HDMISinkHDRDataBlockLength;
    //byte 1[4:0] in HDR static Metadata Data block

    //order R->G->B
    //MS_U16 u16display_primaries_x[3];                       //data *1/1024 0x03FF = 0.999
    //MS_U16 u16display_primaries_y[3];                       //data *1/1024 0x03FF = 0.999
    //MS_U16 u16white_point_x;                                //data *1/1024 0x03FF = 0.999
    //MS_U16 u16white_point_y;                                //data *1/1024 0x03FF = 0.999
    /// Display primaries x, data *0.00002 0xC350 = 1
    MS_U16 u16Display_Primaries_x[3];
    /// Display primaries y, data *0.00002 0xC350 = 1
    MS_U16 u16Display_Primaries_y[3];
    /// White point x, data *0.00002 0xC350 = 1
    MS_U16 u16White_point_x;
    /// White point y, data *0.00002 0xC350 = 1
    MS_U16 u16White_point_y;
    //address 0x19h to 22h in base EDID

    MS_U8 u8HDMISinkEDIDBaseBlockVersion;                //for debug
    //address 0x12h in EDID base block

    MS_U8 u8HDMISinkEDIDBaseBlockReversion;              //for debug
    //address 0x13h in EDID base block

    MS_U8 u8HDMISinkEDIDCEABlockReversion;               //for debug
    //address 0x01h in EDID CEA block

    //table 59 Video Capability Data Block (VCDB)
    //0:VCDB is not avaliable
    //1:VCDB is avaliable
    MS_U8 u8HDMISinkVCDBValid;

    MS_U8 u8HDMISinkSupportYUVFormat;
    //bit 0:Support_YUV444
    //bit 1:Support_YUV422
    //bit 2:Support_YUV420

    //QY in Byte#3 in table 59 Video Capability Data Block (VCDB)
    //bit 3:RGB_quantization_range

    //QS in Byte#3 in table 59 Video Capability Data Block (VCDB)
    //bit 4:Y_quantization_range 0:no data(due to CE or IT video) ; 1:selectable


    MS_U8 u8HDMISinkExtendedColorspace;
    //byte 3 of Colorimetry Data Block
    //bit 0:xvYCC601
    //bit 1:xvYCC709
    //bit 2:sYCC601
    //bit 3:Adobeycc601
    //bit 4:Adobergb
    //bit 5:BT2020 cl
    //bit 6:BT2020 ncl
    //bit 7:BT2020 RGB

    MS_U8 u8HDMISinkEDIDValid;

} ST_KDRV_XC_CFD_EDID;

typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Window (main or sub window)
    MS_U8 u8Win;
    /// HDR type, reference EN_KDRV_XC_CFD_HDR_TYPE
    MS_U8 u8HdrType;
    ///  HDR10/HLG TMO level. 0: low, 1: middle, 2: high
    MS_U8 u8TmoLevel;
} ST_KDRV_XC_CFD_HDR;

typedef struct
{
    MS_U32 u16HdrType;
    MS_U16 u16Level;

    MS_U16 u16ControlSize;
    MS_U8 *pu8data;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy;
#endif
}ST_KDRV_XC_CFD_TMO_LEVEL;


typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    MS_U16 u16Hdr10Smin[3];
    MS_U16 u16Hdr10Smed[3];
    MS_U16 u16Hdr10Smax[3];
    MS_U16 u16Hdr10Tmin[3];
    MS_U16 u16Hdr10Tmed[3];
    MS_U16 u16Hdr10Tmax[3];

    MS_U16 u16HlgSmin[3];
    MS_U16 u16HlgSmed[3];
    MS_U16 u16HlgSmax[3];
    MS_U16 u16HlgTmin[3];
    MS_U16 u16HlgTmed[3];
    MS_U16 u16HlgTmax[3];

    MS_U16 u16LevelCount;
    ST_KDRV_XC_CFD_TMO_LEVEL *pstCfdTmoLevel;
#if !defined (__aarch64__)
        void *pDummy;
#endif

} ST_KDRV_XC_CFD_TMO;


typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Window (main or sub window)
    MS_U8 u8Win;
    /// Linear RGB enable
    MS_BOOL bEnable;
} ST_KDRV_XC_CFD_LINEAR_RGB;

typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Window (main or sub window)
    MS_U8 u8Win;
    /// Input source
    MS_U8 u8InputSource;
    /// Update type, reference EN_KDRV_XC_CFD_UPDATE_TYPE
    MS_U8 u8UpdateType;
    /// RGB bypass
    MS_BOOL bIsRgbBypass;
    /// HD mode
    MS_BOOL bIsHdMode;
} ST_KDRV_XC_CFD_FIRE;

typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// customer dlc curve
    MS_BOOL bUseCustomerDlcCurve;
} ST_KDRV_XC_CFD_DLC;
typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Window (main or sub window)
    MS_U8 u8Win;
    /// HDR type (0: SDR, 1: Dolby HDR, 2: Open HDR)
    MS_U8 u8VideoHdrType;
    /// HDR running
    MS_BOOL bIsHdrRunning;
    /// Is full range
    MS_BOOL bIsFullRange;
    /// Ultra black & white active
    MS_BOOL bUltraBlackWhiteActive;
    /// Color Type
    MS_U8 u8ColorType;
    /// TMO Color Type
    MS_U8 u8TMOColorType;

    /// open HDR metadata
    union
    {
        ST_KDRV_XC_CFD_HDMI stHdmiInfoCFD;
        ST_KDRV_XC_HDR_CFD_MEMORY_FORMAT  stHDRMemFormatCFD;
    } HDRMetadata;
    //CRC Status
    MS_BOOL bCRCStatus;
    //set_HDRDS_info average time
    MS_U16 u16SetDSAverageTime;
    //set_HDRDS_info peak time
    MS_U16 u16SetDSPeakTime;
    //MM HDR vdec version number
    MS_U8 u8VdecVersion;
    //set_HDRDS_info peak frame
    MS_U64 u64SetDSPeakFrame;

    //HDMI packet change
    MS_BOOL bHDMIPacketChange;
    //Seamless CFD Done
    MS_BOOL bSeamlessCFDDone;
    // GOP after done
    MS_BOOL bGOPDone;
    // Reset HDMI Freeze
    MS_BOOL bResetHDMIFreezeFlag;
    //MVOP source HDR type change
    MS_BOOL bHDRDataChange;
    //MVOP source Seamless CFD Done
    MS_BOOL bSeamlessCFDDone_MVOP;
    //Reset MVOP source Freeze
    MS_BOOL bResetMVOPFreezeFlag;
    // HDMI FREEZE CHANGE HDMI METADATA DURING MUTE
    MS_BOOL bHDMIPacketChangeDuringMute;
    // HDMI FREEZE CHANGE HDMI METADATA DURING MUTE RESET
    MS_BOOL bHDMIPacketChangeDuringMuteRest;
} ST_KDRV_XC_CFD_STATUS;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    /// Window (main or sub window)
    MS_U8 u8Win;
    /// pointer to data
    MS_U16* pLUT;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy;
#endif
    /// data size
    MS_U32 u32Size;
} ST_KDRV_XC_CFD_3DLUT;


typedef struct
{
    EN_PICTURE_MODE_VPQ_TYPE en_PICTURE_MODE_VPQ;
} ST_KDRV_XC_CFD_SET_PICTURE_MODE_VPQ;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    /// Window (main or sub window)
    MS_U8 u8Win;
    /// pointer to HDR EOTF data
    MS_U8* pu8Data;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy;
#endif
    /// data size
    MS_U32 u32Size;
} ST_KDRV_XC_CFD_EOTF;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    /// Window (main or sub window)
    MS_U8 u8Win;
    /// pointer to OETF data
    MS_U8* pu8Data;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy;
#endif
    /// data size
    MS_U32 u32Size;
} ST_KDRV_XC_CFD_OETF;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    /// Window (main or sub window)
    MS_U8 u8Win;
    /// pointer to HLG GAIN
    MS_U8* pu8Data;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy;
#endif
    /// data size
    MS_U32 u32Size;
} ST_KDRV_XC_CFD_HLGYGAIN;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    /// Window (main or sub window)
    MS_U8 u8Win;
    MS_BOOL bOnOff;
}ST_KDRV_XC_CFD_INVGAMMAEN;


typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;

    /// Picture attribute control type, reference EN_XC_CFD_PICTURE_ATTR_CTRL_TYPE
    MS_U8 u8ControlType;
    /// Picture attribute value
    MS_U16 u16AttrValue;
} ST_KDRV_XC_CFD_PICTURE_ATTRIBUTE;

typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;
    /// Window (main or sub window)
    MS_U8 u8Win;
    /// Gamut mmaping 3*3 data
    MS_S32 as32GamutLutData[9];
}ST_KDRV_XC_CFD_GAMUTLUT_PREVIOUS;

typedef struct
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;
    /// Window (main or sub window)
    MS_U8 u8Win;
    /// Gamma enable/disable
    MS_BOOL bSDRGammaEnable;
    /// Degamma enable/disable
    MS_BOOL bSDRDegammaEnable;
    /// SDR 3D LUT enable
    MS_BOOL bSDR3DLUTEnable;
    /// Gamut mmaping 3*3 data
    MS_U32 as32GamutLutData[9];
}ST_KDRV_XC_CFD_GAMUTLUT_POST;


typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    /// Window (main or sub window)
    MS_U8 u8Win;

    /// pointer to gamma R
    MS_U8* pu8GammaR;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy1;
#endif
    /// data size
    MS_U32 u32GammaRSize;

    /// pointer to gamma G
    MS_U8* pu8GammaG;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy2;
#endif
    /// data size
    MS_U32 u32GammaGSize;

    /// pointer to gamma B
    MS_U8* pu8GammaB;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy3;
#endif
    /// data size
    MS_U32 u32GammaBSize;
} ST_KDRV_XC_CFD_SDRGAMMA;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    /// Window (main or sub window)
    MS_U8 u8Win;

    /// pointer to gamma R
    MS_U8* pu8DegammaR;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy1;
#endif
    /// data size
    MS_U32 u32DegammaRSize;

    /// pointer to gamma G
    MS_U8* pu8DegammaG;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy2;
#endif
    /// data size
    MS_U32 u32DegammaGSize;

    /// pointer to gamma B
    MS_U8* pu8DegammaB;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy3;
#endif
    /// data size
    MS_U32 u32DegammaBSize;
} ST_KDRV_XC_CFD_SDRDEGAMMA;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    /// Window (main or sub window)
    MS_U8 u8Win;
    /// data size
    MS_U32 u32GammutSize;
    /// pointer to gamma B
    MS_U16* pu16SDRGammut;
#if !defined (__aarch64__)
    /// Dummy parameter
    void *pDummy;
#endif
}ST_KDRV_XC_CFD_SDRGAMMUT;



typedef struct
{
    MS_U16 u16RdPtr;
    MS_U16 u16WdPtr;
    ST_KDRV_XC_HDR_GD_FORMAT astGDInfo[GD_BUFFER_SIZE];
} ST_KDRV_XC_HDR_GD_INFO;

typedef struct
{
    MS_U16 s16MaxY;
    MS_U16 s16MinY;
    MS_U32 u32InvY;
    MS_U16 u16Count;
} ST_KDRV_XC_DOLBY_HDMI_INPUT_TRIG;

typedef struct __attribute__((packed))
{
    /// Structure version
    MS_U32 u32Version;
    /// Structure length
    MS_U16 u16Length;
    MS_U32 u32qmapid;
    MS_U32 u32Quality_InputType_Num;
    MS_U32 u32Quality_IP_Num;
    MS_U32 u32table_data_size;
    MS_U32 u32table_IP_Num;
    MS_U16 *pu16Quality_Map_Aray;
#if !defined (__aarch64__)
    /// Dummy parameter
    MS_U32 u32alignment;
#endif
    MS_U8 *pu8table_data_Aray;
#if !defined (__aarch64__)
    /// Dummy parameter
    MS_U32 u32alignment1;
#endif
    MS_U16 *pu16table_Map_Aray;
#if !defined (__aarch64__)
    /// Dummy parameter
    MS_U32 *u32alignment2;
#endif
} ST_KDRV_XC_PQ_QMAPDATA;

typedef enum
{
    /// Select main window standard PQ setting for each input source/resolution
    PQ_BIN_STD_MAIN,
    /// Select sub window standard PQ setting for each input source/resolution
    PQ_BIN_STD_SUB,
    /// Select main window extension PQ setting for each input source/resolution
    PQ_BIN_EXT_MAIN,
    /// Select sub window extension PQ setting for each input source/resolution
    PQ_BIN_EXT_SUB,
    /// Select main window extension PQ setting for each input source/resolution
    PQ_BIN_CUSTOMER_MAIN,
    /// Select sub window extension PQ setting for each input source/resolution
    PQ_BIN_CUSTOMER_SUB,
    /// Select UFSC extension PQ setting for each input source/resolution
    PQ_BIN_UFSC,
    /// Select main window CF extension PQ setting for each input source/resolution
    PQ_BIN_CF_MAIN,
    /// Select sub window CF extension PQ setting for each input source/resolution
    PQ_BIN_CF_SUB,
    /// Select TMO extension PQ setting for each input source/resolution
    PQ_BIN_TMO,
    /// The max number of PQ Bin
    MAX_PQ_BIN_NUM,
}PQ_BIN_TYPE;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    MS_S16 s16YOffset;
    MS_U16 u16YGain;
    MS_S16 s16COffset;
    MS_U16 u16CGain;
}ST_KDRV_XC_YC_OffsetGain;

typedef struct
{
    MS_U32 u32Version;
    MS_U32 u32Length;
    MS_S16 s16YOffsetIn;
    MS_U16 u16YGainIn;
    MS_S16 s16COffsetIn;
    MS_U16 u16CGainIn;
    MS_S16 s16YOffsetOut;
    MS_U16 u16YGainOut;
    MS_S16 s16COffsetOut;
    MS_U16 u16CGainOut;
}ST_KDRV_XC_YC_OffsetGainBoth;

typedef struct
{
    MS_U8  vid;
    MS_U8  deflickerEn;
    MS_U8  opDiffIdx;
    MS_U8  reserved;
    MS_U8  snrMotionEn;
    MS_U8  defaultSnrGain;
    MS_U32 flickerTh;
    MS_U32 flickerThH;
    MS_U8  dnrLowCplx;
    MS_U8  dnrLowCplxNorm;
    MS_U8  dnrHiCpLowFr;
    MS_U8  dnrHiCpLowFrNorm;
    MS_U8  dnrHiCpHiFr;
    MS_U8  dnrHiCpHiFrNorm;
}ST_KDRV_XC_DEFLICKER;

typedef enum
{
    SEAMLESS_NONE,
    SEAMLESS_DATACHANGED,
    SEAMLESS_CALCULATING,
    SEAMLESS_UNFREEZE,
    /// The max number of Seamless Status
    MAX_SEAMLESS_STATUS_NUM,
} Seamless_Status;

enum HDMI_INFO_SOURCE_TYPE
{
    HDMI_INFO_SOURCE0 = 0,
    HDMI_INFO_SOURCE_MAX,
    HDMI_INFO_SOURCE1,
};
/// HDMI COLOR FORMAT
typedef enum
{
    MS_HDMI_COLOR_RGB,    ///< HDMI RGB 444 Color Format
    MS_HDMI_COLOR_YUV_422,    ///< HDMI YUV 422 Color Format
    MS_HDMI_COLOR_YUV_444,    ///< HDMI YUV 444 Color Format
    MS_HDMI_COLOR_YUV_420,    ///< HDMI YUV 420 Color Format
    MS_HDMI_COLOR_RESERVED,    ///< Reserve
    MS_HDMI_COLOR_DEFAULT = MS_HDMI_COLOR_RGB,    ///< Default setting
    MS_HDMI_COLOR_UNKNOWN = 7,    ///< Unknow Color Format
} MS_HDMI_COLOR_FORMAT;
/// HDMI Extended COLORIMETRY
typedef enum
{
    MS_HDMI_EXT_COLOR_XVYCC601,        ///< xvycc 601
    MS_HDMI_EXT_COLOR_XVYCC709,        ///< xvycc 709
    MS_HDMI_EXT_COLOR_SYCC601,         ///< sYCC 601
    MS_HDMI_EXT_COLOR_ADOBEYCC601,     ///< Adobe YCC 601
    MS_HDMI_EXT_COLOR_ADOBERGB,        ///< Adobe RGB
    MS_HDMI_EXT_COLOR_BT2020YcCbcCrc,  /// ITU-F BT.2020 YcCbcCrc
    MS_HDMI_EXT_COLOR_BT2020RGBYCbCr,  /// ITU-R BT.2020 RGB or YCbCr
    MS_HDMI_EXT_COLOR_DEFAULT = MS_HDMI_EXT_COLOR_XVYCC601,    ///< Default setting
    MS_HDMI_EXT_COLOR_UNKNOWN = 7,    ///< Unknow
} MS_HDMI_EXT_COLORIMETRY_FORMAT;
///HDMI COLOR RANGE
typedef enum
{
    E_HDMI_COLOR_RANGE_DEFAULT, //
    E_HDMI_COLOR_RANGE_LIMIT,   //HDMI RGB Limited Range (16-235)
    E_HDMI_COLOR_RANGE_FULL,    //HDMI Full Range (0-255)
    E_HDMI_COLOR_RANGE_RESERVED
} EN_HDMI_COLOR_RANGE;
//-------------------------------------------------------------------------------------------------
//  Function and Variable
//-------------------------------------------------------------------------------------------------
INTERFACE MS_U16 MHal_XC_GetSat(void);
INTERFACE MS_U16 MHal_XC_GetHue(void);
INTERFACE void MHal_XC_GetMaxRGBHist(ST_KDRV_XC_RGBHistogram *pstRGBHistogram);

INTERFACE void MHal_XC_WriteByte(DWORD u32Reg, BYTE u8Val);
INTERFACE void MHal_XC_WriteByteMask(DWORD u32Reg, BYTE u8Val, WORD u16Mask);
INTERFACE BYTE MHal_XC_ReadByte(DWORD u32Reg);
INTERFACE void MHal_XC_W2BYTE(DWORD u32Reg, WORD u16Val );
INTERFACE WORD MHal_XC_R2BYTE(DWORD u32Reg );
INTERFACE void MHal_XC_W2BYTEMSK(DWORD u32Reg, WORD u16Val, WORD u16Mask );
INTERFACE WORD MHal_XC_R2BYTEMSK(DWORD u32Reg, WORD u16Mask );
INTERFACE BYTE MHal_XC_VR_ReadByte(DWORD u32Reg);
INTERFACE WORD MHal_XC_VR_R2BYTE(DWORD u32Reg );
INTERFACE WORD MHal_XC_VR_R2BYTEMSK(DWORD u32Reg, WORD u16Mask );
INTERFACE void MHal_XC_VR_WriteByte(DWORD u32Reg, BYTE u8Val);
INTERFACE void MHal_XC_VR_W2BYTE(DWORD u32Reg, WORD u16Val );
INTERFACE void MHal_XC_VR_W2BYTEMSK(DWORD u32Reg, WORD u16Val, WORD u16Mask  );
INTERFACE BOOL MHal_XC_IsBlackVideoEnable( MS_BOOL bWindow );
INTERFACE void MHal_XC_FRCR2SoftwareToggle( void );
INTERFACE BOOL MHal_XC_GetHdmiMetadata(MS_U8 *pu8Metadata, MS_U16 *pu16MetadataLength);
INTERFACE BOOL MHal_XC_GetMiuOffset(MS_U32 u32MiuNo, MS_U64 *pu64Offset);
INTERFACE BOOL MHal_XC_SetInputSourceType(EN_KDRV_XC_INPUT_SOURCE_TYPE enInputSourceType);
INTERFACE BOOL MHal_XC_SetHDR_DMARequestOFF(MS_BOOL bEnable, MS_BOOL bImmediately);
INTERFACE BOOL MHal_XC_SetDMPQBypass(MS_BOOL bEnable);
INTERFACE void MHal_XC_SetDMAPath(EN_KDRV_XC_HDR_DMA_PATH enDMAPath);
INTERFACE BOOL MHal_XC_SetHDRType(EN_KDRV_XC_HDR_TYPE enHDRType);
INTERFACE void MHal_XC_Hdr10_Enable(MS_BOOL bIsOn);
INTERFACE void Mha_XC_HDR10_Switch_Case(EN_KDRV_XC_HDR_PATH eTypeHDR10,MS_BOOL bIsOn);
INTERFACE void MHal_XC_HLGPatch(MS_BOOL bEnable);
INTERFACE BOOL MHal_XC_UpdatePath(EN_KDRV_XC_HDR_PATH enPath);
INTERFACE BOOL MHal_XC_EnableEL(MS_BOOL bEnable);
INTERFACE BOOL MHal_XC_EnableAutoSeamless(MS_BOOL bEnable);
INTERFACE BOOL MHal_XC_DisableHDMI422To444(void);
INTERFACE BOOL MHal_XC_SetHDRWindow(MS_U16 u16Width, MS_U16 u16Height);
INTERFACE BOOL MHal_XC_Set3DLutInfo(MS_U8* pu8Data, MS_U32 u32Size);
#ifdef HDR10_DOLBY
INTERFACE BOOL MHal_XC_SetDolbyMetaData(MS_U8* pu8Data, MS_U32 u32Size,EN_KDRV_XC_CFD_HDR_TYPE enHdr_Type);
#else
INTERFACE BOOL MHal_XC_SetDolbyMetaData(MS_U8* pu8Data, MS_U32 u32Size);
#endif
INTERFACE BOOL MHal_XC_SetDolbyCompData(MS_U8* pu8Data, MS_U32 u32Size);
INTERFACE BOOL MHal_XC_SetVRAddress(MS_PHY phyVRAddress, MS_U32 u32VRSize);
INTERFACE BOOL MHal_XC_EnableHDR(MS_BOOL bEnableHDR);
INTERFACE BOOL MHal_XC_MuteHDR(void);
INTERFACE BOOL MHal_XC_UnMuteHDR(void);
INTERFACE BOOL MHal_XC_SupportDolbyHDR(void);
INTERFACE BOOL MHal_XC_SupportTCH(void);
INTERFACE BOOL MHal_XC_DolbySWBonded(void);
INTERFACE BOOL MHal_XC_DolbyHWBonded(void);
INTERFACE BOOL MHal_XC_ConfigAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient, MS_BOOL bEnable,
        EN_KDRV_XC_AUTODOWNLOAD_MODE enMode, MS_PHY phyBaseAddr, MS_U32 u32Size, MS_U32 u32MiuNo);
INTERFACE BOOL MHal_XC_WriteAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient, MS_U8* pu8Data, MS_U32 u32Size, void* pParam);
INTERFACE BOOL MHal_XC_FireAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient);
INTERFACE BOOL MHal_XC_ResetAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient);
INTERFACE BOOL MHal_XC_ConfigHDRAutoDownloadStoredInfo(MS_PHY phyLutBaseAddr, MS_U8 *pu8VirtLutBaseAddr, MS_U32 u32Size);
INTERFACE BOOL MHal_XC_ConfigXVYCCAutoDownloadStoredInfo(MS_PHY phyLutBaseAddr, MS_U8 *pu8VirtLutBaseAddr, MS_U32 u32Size);
INTERFACE BOOL MHal_XC_StoreHDRAutoDownload(MS_U8* pu8Data, MS_U32 u32Size, void* pParam);
INTERFACE BOOL MHal_XC_StoreXVYCCAutoDownload(MS_U8* pu8Data, MS_U32 u32Size, void* pParam);
INTERFACE BOOL MHal_XC_WriteStoredHDRAutoDownload(MS_U8 *pu8LutData, MS_U32 u32Size, MS_U8 u8Index,
        MS_PHY *pphyFireAdlAddr, MS_U32 *pu32Depth);
INTERFACE BOOL MHal_XC_WriteStoredXVYCCAutoDownload(MS_U8 *pu8LutData, MS_U32 u32Size, MS_U8 u8Index,
        MS_PHY *pphyFireAdlAddr, MS_U32 *pu32Depth);
INTERFACE MS_U32 MHal_XC_GetHDRAutoDownloadStoredSize(void);
INTERFACE BOOL MHal_XC_GetAutoDownloadCaps(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient, MS_BOOL *pbSupported);
INTERFACE BOOL MHal_XC_SetColorFormat(MS_BOOL bHDMI422);
INTERFACE MS_U16 MHal_XC_CFD_GetInitParam(ST_KDRV_XC_CFD_INIT *pstCfdInit);
INTERFACE MS_U16 MHal_XC_CFD_GetHdmiParam(ST_KDRV_XC_CFD_HDMI *pstCfdHdmi);
INTERFACE MS_S32 MHal_XC_CFD_DepositeMMParam(ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info *pstFormatInfo, MS_U8 u8Win);
INTERFACE MS_S32 MHal_XC_CFD_WithdrawMMParam(ST_KDRV_XC_SHARE_MEMORY_FORMAT_Info *pstFormatInfo, MS_U8 u8Win);
INTERFACE BOOL MHal_XC_EnableHDRCLK(MS_BOOL bEnable, MS_BOOL bImmediate);
INTERFACE E_CFD_MC_FORMAT MHal_XC_HDMI_Color_Data_Format(MS_U8 u8PixelFormat);
INTERFACE BOOL MHal_XC_CFDControl(ST_KDRV_XC_CFD_CONTROL_INFO *pstKdrvCFDCtrlInfo);
INTERFACE BOOL MHal_XC_InitHDR(void);
INTERFACE BOOL MHal_XC_ExitHDR(void);
INTERFACE MS_U32 MHal_XC_GetRegsetCnt(void);
INTERFACE void MHal_XC_GetRegsetCnt_fromCFD(K_XC_DS_CMDCNT *stXCDSCmdCnt);
INTERFACE void MHal_XC_PrepareDolbyInfo(void);
INTERFACE void MHal_XC_Suspend(void);
INTERFACE void MHal_XC_Resume(void);
INTERFACE void Color_Format_Driver(void);
INTERFACE MS_BOOL MHal_XC_CheckMuteStatusByRegister(MS_U8 u8Window);
INTERFACE void MHal_XC_Init(void);
#ifdef XC_REPLACE_MEMCPY_BY_BDMA
INTERFACE void MHal_XC_MemCpy_by_BDMA(MS_U32 u32src_addr, MS_U32 u32dest_addr, MS_U32 u32Size);
#endif
INTERFACE BOOL MHal_XC_IsCRCPass(MS_U8 u8PkgIdx);
INTERFACE MS_BOOL MHal_XC_VSIF_Dolby_Status(void);
INTERFACE BOOL MHAL_XC_IsCFDInitFinished(MS_U8 eWin);
INTERFACE void MHal_XC_CFD_SetTmoVal(void);
INTERFACE BOOL MHal_XC_CFD_IsTmoChanged(void);
#if DOLBY_GD_ENABLE
INTERFACE MS_BOOL MHal_XC_IsGDEnabled(void);
INTERFACE MS_U16 MHal_XC_GetGDValue(void);
INTERFACE MS_S8 MHal_XC_GetBackendDelayFrame(void);
INTERFACE MS_S8 MHal_XC_GetPWMDelayFrame(void);
INTERFACE MS_U8 MHal_XC_GetPWMPort(void);
INTERFACE MS_U8 MHal_XC_GetSCMIFrameCount(void);
INTERFACE MS_U8 MHal_XC_GetHDRDMAFrameCount(void);
#endif
#if OTT_AUTO_PAUSE_DETECT
INTERFACE void MHal_XC_ClearPauseStatus(void);
INTERFACE MS_BOOL MHal_XC_IncOTT_OPVsyncWithoutDSInfoCount(void);
INTERFACE MS_BOOL MHal_XC_IsOTTPausing(void);
#endif
#if HDMI_AUTO_NOSIGNAL_DETECT
INTERFACE void MHal_XC_ClearNoSignalStatus(void);
INTERFACE MS_BOOL MHal_XC_IncHDMI_OPVsyncWithoutSignalCount(void);
INTERFACE MS_BOOL MHal_XC_IsHDMINoSignal(void);
#endif
INTERFACE void MHal_XC_CalculateFrameHDRInfo(MS_U32 u32RPtr, ST_KDRV_XC_HDR_INFO_ENTRY *pstEntry, MS_U8 *pu8InputMDAddr,
                                   MS_U8 *pu8InputMDAddr_Cached, MS_U8 *pu8LutsAddr, MS_U8 *pu8LutsAddr_Cached, MS_U8 *pu8RegSetAddr,
                                   MS_U8 *pu8RegSetAddr_Cached, ST_KDRV_XC_HDR_DOLBY_MEMORY_FORMAT_EX *pstDolbyHDRShareMem,
                                   ST_KDRV_XC_SHARE_MEMORY_INFO *pstShareMemInfo,MS_BOOL UserMode);
INTERFACE void MHal_XC_SetLastEntryIndex(MS_U8 u8LastEntryIdx);
INTERFACE void MHal_XC_SetLastGDWdPtr(MS_U16 u16LastGDWdPtr);
INTERFACE void MHal_XC_SetShareMemInfo(ST_KDRV_XC_SHARE_MEMORY_INFO _stShareMemInfo);
INTERFACE void MHal_XC_SetCurrentIndex(MS_U8 u8CurrentIndex);
INTERFACE MS_BOOL MHal_XC_SetSWDRInfo(ST_KDRV_XC_SWDR_INFO *pstSWDRInfo);
INTERFACE MS_BOOL MHal_XC_GetSWDRInfo(ST_KDRV_XC_SWDR_INFO *pstSWDRInfo);
INTERFACE void MHal_XC_SetDolbyStatus(MS_U8 u8Status, MS_U8 u8Mask);
INTERFACE BOOL MHal_XC_GetDolbyStatus(void);

#ifdef DYNAMIC_SWITCH_SDR_OPENHDR
INTERFACE void MHal_XC_Get_HDRTOSDR_CFDStatus(MS_BOOL *pstHDRTOSDR_CFD_DONE);
INTERFACE void MHal_XC_Set_HDRTOSDR_CFDStatus(MS_BOOL bHDRTOSDR_CFD_DONE);
#endif
INTERFACE void MHal_XC_HDR_Status_Record(MS_BOOL bstatus);
INTERFACE MS_U16 MHal_XC_CFD_GetPanelParam(ST_KDRV_XC_CFD_PANEL *pstCfdPanel);
INTERFACE void MHal_XC_EnableTCHHDR(MS_BOOL bEnableTCHHDR);
INTERFACE BOOL MHal_XC_GetTCHHDRStatus(void);

//for HDR SDR seamless start
INTERFACE MS_BOOL MHal_HDMI_Get_InfoFrame(MS_U8 u8Win);
INTERFACE void MHal_XC_changeADLBasedAddress(MS_BOOL bEnable);
INTERFACE void MHal_XC_updateADLBasedAddress(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient,MS_U8 u8Index,MS_PHY *pphyFireAdlAddr, MS_U32 *pu32Depth);
INTERFACE EN_KDRV_XC_HDR_SEAMLESS_PATH MHal_XC_Get_HDRSeamless_HDMI_Path(void);
INTERFACE void MHal_XC_HDMI_Seamless_Without_Freeze_Mute(void);
INTERFACE void MHal_XC_HDMI_Seamless_With_Freeze(void);
INTERFACE MS_BOOL MHal_XC_UpdateCFDPara(MS_U8 u8Win);
INTERFACE void MHal_XC_Set_SharedMemVersion(MS_U8 u8Win,MS_U8 u8Version);
INTERFACE MS_U8 MHal_XC_Get_SharedMemVersion(MS_U8 u8Win);
MS_BOOL MHal_Get_HDRSeamless_Status(MS_U8 u8Win);

INTERFACE EN_KDRV_XC_HDR_SEAMLESS_MVOPSOURCE_PATH MHal_XC_Get_HDRSeamless_MVOPSOURCE_Path(void);
INTERFACE void MHal_XC_MM_Seamless_With_Freeze(void);
INTERFACE MS_U8 MHal_XC_GetVersion(void);
INTERFACE MS_U8 MHal_XC_GetTransfer_Characteristics(void);
//for HDR SDR seamless end

INTERFACE void MHal_XC_SetPicturemodeVPQType(EN_PICTURE_MODE_VPQ_TYPE en_PICTURE_MODE_VPQ);
INTERFACE EN_PICTURE_MODE_VPQ_TYPE MHal_XC_GetPicturemodeVPQType(void);
INTERFACE void MHal_XC_SetOSDSettingFlag(MS_BOOL bEnable);
INTERFACE MS_BOOL MHal_XC_GetOSDSettingFlag(void);

#endif // _MHAL_TEMP_H_
