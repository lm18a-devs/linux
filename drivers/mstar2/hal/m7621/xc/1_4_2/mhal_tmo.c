////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    mhal_tmo.c
/// @brief  MStar GFLIP DDI HAL LEVEL
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////

#define _HAL_TMO_C

//=============================================================================
// Include Files
//=============================================================================
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/slab.h>
#include "mdrv_mstypes.h"
#include "color_format_ip.h"
#include "color_format_ip_write.h"
#include "mhal_tmo.h"
#include "mhal_xc.h"
#define FRAME_BUFFER_SIZE 16

#ifdef TMO_DBG_EN
#define TMO_DBG(x) x
#else
#define TMO_DBG(x)
#endif
#define DBG_LIGHT( row, col, val, mask ) MApi_GFLIP_XC_W2BYTEMSK( 0x133080 + (row*4+col)*2 , val, mask )

typedef struct
{
    // Default luma curve
    BYTE ucLumaCurve[16];
    BYTE ucLumaCurve2_a[16];
    BYTE ucLumaCurve2_b[16];

    BYTE ucDlcPureImageMode; // Compare difference of max and min bright
    BYTE ucDlcLevelLimit; // n = 0 ~ 4 => Limit n levels => ex. n=2, limit 2 level 0xF7, 0xE7
    BYTE ucDlcAvgDelta; // n = 0 ~ 50, default value: 12
    BYTE ucDlcAvgDeltaStill; // n = 0 ~ 15 => 0: disable still curve, 1 ~ 15 => 0.1 ~ 1.5 enable still curve
    BYTE ucDlcFastAlphaBlending; // min 17 ~ max 32
    BYTE ucDlcSlowEvent; // some event is triggered, DLC must do slowly // for PIP On/Off, msMultiPic.c
    BYTE ucDlcTimeOut; // for IsrApp.c
    BYTE ucDlcFlickAlphaStart; // for force to do fast DLC in a while
    BYTE ucDlcYAvgThresholdH; // default value: 128
    BYTE ucDlcYAvgThresholdL; // default value: 0
    BYTE ucDlcBLEPoint; // n = 24 ~ 64, default value: 48
    BYTE ucDlcWLEPoint; // n = 24 ~ 64, default value: 48
    BYTE bCGCCGainCtrl : 1; // 1: enable; 0: disable
    BYTE bEnableBLE : 1; // 1: enable; 0: disable
    BYTE bEnableWLE : 1; // 1: enable; 0: disable
    BYTE ucDlcYAvgThresholdM;
    BYTE ucDlcCurveMode;
    BYTE ucDlcCurveModeMixAlpha;
    BYTE ucDlcAlgorithmMode;
    BYTE ucDlcHistogramLimitCurve[17];
    BYTE ucDlcSepPointH;
    BYTE ucDlcSepPointL;
    WORD uwDlcBleStartPointTH;
    WORD uwDlcBleEndPointTH;
    BYTE ucDlcCurveDiff_L_TH;
    BYTE ucDlcCurveDiff_H_TH;
    WORD uwDlcBLESlopPoint_1;
    WORD uwDlcBLESlopPoint_2;
    WORD uwDlcBLESlopPoint_3;
    WORD uwDlcBLESlopPoint_4;
    WORD uwDlcBLESlopPoint_5;
    WORD uwDlcDark_BLE_Slop_Min;
    BYTE ucDlcCurveDiffCoringTH;
    BYTE ucDlcAlphaBlendingMin;
    BYTE ucDlcAlphaBlendingMax;
    BYTE ucDlcFlicker_alpha;
    BYTE ucDlcYAVG_L_TH;
    BYTE ucDlcYAVG_H_TH;
    BYTE ucDlcDiffBase_L;
    BYTE ucDlcDiffBase_M;
    BYTE ucDlcDiffBase_H;
    // NEW DLC
    BYTE ucLMaxThreshold;
    BYTE ucLMinThreshold;
    BYTE ucLMaxCorrection;
    BYTE ucLMinCorrection;
    BYTE ucRMaxThreshold;
    BYTE ucRMinThreshold;
    BYTE ucRMaxCorrection;
    BYTE ucRMinCorrection;
    BYTE ucAllowLoseContrast;
    BYTE ucKernelDlcAlgorithm;
    BYTE ucDlcHistogramSource;
    MS_U8 u8Dlc_Mode;
    MS_U8 u8Tmo_Mode;
}StuDlc_FinetuneParamaters;

typedef struct
{
    MS_U16 u16Control_points;
    MS_U32 *pt_input_nits_array;
    MS_U32 *pt_output_nits_array;

    MS_U16 *pt_input_PQcode;

    MS_S16 s16_last_less1_index_outputnits;
    MS_S16 s16_last_index_m10000_outputnits;
    MS_S16 s16_last_index_m100_outputnits;
} HDR_TMO_GEN_TABLE;

typedef struct
{
    MS_U16 smin; // 0.10
    MS_U16 smed; // 0.10
    MS_U16 smax; // 0.10
    MS_U16 tmin; // 0.10
    MS_U16 tmed; // 0.10
    MS_U16 tmax; // 0.10
    MS_U16 slope; // 4.12
    MS_U16 rolloff; // 4.12
} HDR_ToneMappingData;

//=============================================================================
// Compile options
//=============================================================================

//=============================================================================
// Debug Macros
//=============================================================================
static MS_U8 u8PrintfOneTime = TRUE;
extern MS_BOOL msTMOinit(void);
extern void msHDRFixDlcWriteCurve(bWindow);//HDR Fix DLC;
extern void msReferTMO(void);
extern void msNewTMO(MS_BOOL bWindow);
extern void msLinearTMO(void);
extern void msOldTMO(MS_BOOL bWindow);
extern void ms12pTMO(MS_BOOL bWindow);
extern void msAPITMO(void);
//=============================================================================
// Macros
//=============================================================================
extern StuDlc_FinetuneParamaters g_DlcParameters;
extern ST_KDRV_XC_CFD_TMO _stCfdTmo;
extern MS_BOOL bTmoFireEnable;
#define DLC_CURVE_SIZE   18
#define last_less1nit_entry 153
#define STATUS_SUCCESS 0x00
#define STATUS_ERROR (-1)
extern void msDlcWriteCurveLSB(MS_BOOL bWindow, BYTE ucIndex, BYTE ucValue);
extern MS_BOOL KApi_XC_MLoad_Fire(EN_MLOAD_CLIENT_TYPE _client_type, MS_BOOL bImmeidate);
extern BOOL MHal_XC_FireAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient);
extern HDR_CONTENT_LIGHT_MATADATA STU_HDR_METADATA;
extern StuHDR_12pTmo_Param_Api gStuHdr12pTmoParamApi;
//=============================================================================
// Local Variables
//=============================================================================
static MS_U8 u8log;
static MS_U16 u16tmo_lut[512]={0},u16code_in[512] = {0};
static volatile MS_U16 _u16Count=0;
static WORD HDR_Curve[18]; //YT ForHDR
static BYTE g_ucTable[16]; // Final target curve (8 bit)
static WORD g_uwTable[16]; // Final target curve (10 bit)
static HDR_ToneMappingData HDRtmp; //YT ForHDR
static MS_U16 u16DumySource[11];
static MS_U16 u16DumyTarget[11];
//=============================================================================
// Global Variables
//=============================================================================
extern StuDlc_HDRinit g_HDRinitParameters;
extern MS_U16 input_gamma_idx_10k[512];
extern MS_U16 input_gamma_idx_4k[512];
static StuHDR_12pTmo_Param gStuHdr12pTmoParam;

static MS_U32 TMOAPI_PQ2Nits_1024x32bits[1024] =
{
0x00000000, 0x0000677b, 0x00014fa7, 0x00029faa, 0x000450ae, 0x00065fec, 0x0008cc59, 0x000b95da,
0x000ebce2, 0x00124242, 0x00162710, 0x001a6c92, 0x001f1433, 0x00241f7e, 0x00299015, 0x002f67ae,
0x0035a812, 0x003c5317, 0x00436aa4, 0x004af0aa, 0x0052e724, 0x005b501b, 0x00642da0, 0x006d81cb,
0x00774ec2, 0x008196ae, 0x008c5bc5, 0x0097a041, 0x00a36667, 0x00afb081, 0x00bc80e3, 0x00c9d9e7,
0x00d7bdef, 0x00e62f63, 0x00f530b5, 0x0104c45c, 0x0114ecd7, 0x0125acad, 0x0137066a, 0x0148fca4,
0x015b91f8, 0x016ec908, 0x0182a47f, 0x01972711, 0x01ac5375, 0x01c22c6e, 0x01d8b4c5, 0x01efef49,
0x0207ded3, 0x02208641, 0x0239e87c, 0x02540872, 0x026ee91a, 0x028a8d73, 0x02a6f883, 0x02c42d59,
0x02e22f0d, 0x030100bc, 0x0320a58d, 0x034120b2, 0x0362755f, 0x0384a6d7, 0x03a7b862, 0x03cbad50,
0x03f088fc, 0x04164ec8, 0x043d021f, 0x0464a675, 0x048d3f48, 0x04b6d01c, 0x04e15c82, 0x050ce812,
0x0539766d, 0x05670b3d, 0x0595aa38, 0x05c5571a, 0x05f615ab, 0x0627e9bc, 0x065ad727, 0x068ee1d1,
0x06c40da9, 0x06fa5ea6, 0x0731d8ca, 0x076a8023, 0x07a458c8, 0x07df66d9, 0x081bae81, 0x085933f9,
0x0897fb7f, 0x08d8095f, 0x091961f0, 0x095c0993, 0x09a004b5, 0x09e557cb, 0x0a2c075a, 0x0a7417ee,
0x0abd8e21, 0x0b086e98, 0x0b54be02, 0x0ba2811c, 0x0bf1bcae, 0x0c42758a, 0x0c94b092, 0x0ce872af,
0x0d3dc0dc, 0x0d94a01b, 0x0ded157e, 0x0e472621, 0x0ea2d72f, 0x0f002ddf, 0x0f5f2f74, 0x0fbfe13e,
0x1022489b, 0x10866af6, 0x10ec4dc8, 0x1153f695, 0x11bd6af1, 0x1228b07d, 0x1295cce8, 0x1304c5ed,
0x1375a158, 0x13e86502, 0x145d16d1, 0x14d3bcbb, 0x154c5cc4, 0x15c6fcfe, 0x1643a38b, 0x16c2569c,
0x17431c70, 0x17c5fb55, 0x184af9a9, 0x18d21dd9, 0x195b6e64, 0x19e6f1d4, 0x1a74aec6, 0x1b04abe8,
0x1b96eff5, 0x1c2b81ba, 0x1cc26815, 0x1d5ba9f3, 0x1df74e52, 0x1e955c43, 0x1f35dae4, 0x1fd8d169,
0x207e4712, 0x21264334, 0x21d0cd35, 0x227dec8c, 0x232da8c3, 0x23e00974, 0x2495164c, 0x254cd70d,
0x26075387, 0x26c493a0, 0x000102fc, 0x000107f9, 0x00010d09, 0x0001122c, 0x00011762, 0x00011cab,
0x00012208, 0x00012779, 0x00012cfe, 0x00013297, 0x00013844, 0x00013e06, 0x000143dd, 0x000149c9,
0x00014fcb, 0x000155e2, 0x00015c0f, 0x00016252, 0x000168ac, 0x00016f1c, 0x000175a3, 0x00017c41,
0x000182f6, 0x000189c3, 0x000190a7, 0x000197a4, 0x00019eb9, 0x0001a5e7, 0x0001ad2d, 0x0001b48d,
0x0001bc06, 0x0001c398, 0x0001cb45, 0x0001d30c, 0x0001daed, 0x0001e2e9, 0x0001eb01, 0x0001f333,
0x0001fb81, 0x000203ec, 0x00020c72, 0x00021515, 0x00021dd4, 0x000226b1, 0x00022fab, 0x000238c3,
0x000241f9, 0x00024b4e, 0x000254c1, 0x00025e53, 0x00026804, 0x000271d5, 0x00027bc6, 0x000285d7,
0x00029009, 0x00029a5c, 0x0002a4d0, 0x0002af66, 0x0002ba1e, 0x0002c4f8, 0x0002cff5, 0x0002db15,
0x0002e658, 0x0002f1bf, 0x0002fd4a, 0x000308fa, 0x000314cf, 0x000320c8, 0x00032ce8, 0x0003392d,
0x00034599, 0x0003522c, 0x00035ee6, 0x00036bc7, 0x000378d1, 0x00038602, 0x0003935d, 0x0003a0e1,
0x0003ae8e, 0x0003bc65, 0x0003ca67, 0x0003d894, 0x0003e6ec, 0x0003f56f, 0x0004041f, 0x000412fb,
0x00042205, 0x0004313c, 0x000440a1, 0x00045034, 0x00045ff6, 0x00046fe8, 0x00048009, 0x0004905a,
0x0004a0dc, 0x0004b190, 0x0004c275, 0x0004d38c, 0x0004e4d6, 0x0004f654, 0x00050805, 0x000519ea,
0x00052c03, 0x00053e53, 0x000550d7, 0x00056392, 0x00057684, 0x000589ad, 0x00059d0e, 0x0005b0a8,
0x0005c47a, 0x0005d886, 0x0005eccc, 0x0006014c, 0x00061608, 0x00062aff, 0x00064033, 0x000655a4,
0x00066b52, 0x0006813f, 0x0006976a, 0x0006add4, 0x0006c47f, 0x0006db6a, 0x0006f296, 0x00070a04,
0x000721b4, 0x000739a7, 0x000751de, 0x00076a59, 0x0007831a, 0x00079c20, 0x0007b56c, 0x0007ceff,
0x0007e8da, 0x000802fd, 0x00081d6a, 0x00083820, 0x00085320, 0x00086e6c, 0x00088a03, 0x0008a5e7,
0x0008c218, 0x0008de97, 0x0008fb66, 0x00091883, 0x000935f1, 0x000953b0, 0x000971c0, 0x00099023,
0x0009aeda, 0x0009cde4, 0x0009ed44, 0x000a0cf9, 0x000a2d04, 0x000a4d67, 0x000a6e22, 0x000a8f36,
0x000ab0a4, 0x000ad26d, 0x000af490, 0x000b1711, 0x000b39ee, 0x000b5d2a, 0x000b80c4, 0x000ba4be,
0x000bc919, 0x000bedd6, 0x000c12f5, 0x000c3878, 0x000c5e5e, 0x000c84ab, 0x000cab5d, 0x000cd277,
0x000cf9f9, 0x000d21e4, 0x000d4a39, 0x000d72f9, 0x000d9c25, 0x000dc5be, 0x000defc5, 0x000e1a3c,
0x000e4522,0x000e707a, 0x000e9c44, 0x000ec881, 0x000ef532, 0x000f2259, 0x000f4ff6, 0x000f7e0a,
0x000fac98, 0x000fdb9f, 0x00100b21, 0x00103b1f, 0x00106b9a, 0x00109c93, 0x0010ce0c, 0x00110005,
0x00113281, 0x0011657f, 0x00119901, 0x0011cd09, 0x00120197, 0x001236ad, 0x00126c4c, 0x0012a276,
0x0012d92b, 0x0013106d, 0x0013483d, 0x0013809c, 0x0013b98c, 0x0013f30f, 0x00142d24, 0x001467cf,
0x0014a30f, 0x0014dee7, 0x00151b57, 0x00155862, 0x00159609, 0x0015d44c, 0x0016132e, 0x001652af,
0x001692d2, 0x0016d398, 0x00171502, 0x00175712, 0x001799c9, 0x0017dd28, 0x00182132, 0x001865e8,
0x0018ab4b, 0x0018f15d, 0x00193820, 0x00197f94, 0x0019c7bc, 0x001a109a, 0x001a5a2f, 0x001aa47c,
0x001aef83, 0x001b3b46, 0x001b87c7, 0x001bd507, 0x001c2308, 0x001c71cc, 0x001cc155, 0x001d11a4,
0x001d62ba, 0x001db49b, 0x001e0747, 0x001e5ac1, 0x001eaf0b, 0x001f0425, 0x001f5a12, 0x001fb0d5,
0x0020086e, 0x002060e0, 0x0020ba2d, 0x00211456, 0x00216f5f, 0x0021cb47, 0x00222813, 0x002285c3,
0x0022e45a, 0x002343d9, 0x0023a444, 0x0024059b, 0x002467e2, 0x0024cb19, 0x00252f44, 0x00259465,
0x0025fa7d, 0x0026618f, 0x0026c99d, 0x002732aa, 0x00279cb7, 0x002807c8, 0x002873dd, 0x0028e0fa,
0x00294f21, 0x0029be54, 0x002a2e96, 0x002a9fe9, 0x002b124f, 0x002b85cb, 0x002bfa60, 0x002c700f,
0x002ce6dc, 0x002d5ec9, 0x002dd7d8, 0x002e520d, 0x002ecd69, 0x002f49ef, 0x002fc7a2, 0x00304685,
0x0030c69a, 0x003147e3, 0x0031ca65, 0x00324e21, 0x0032d31a, 0x00335954, 0x0033e0d0, 0x00346992,
0x0034f39d, 0x00357ef3, 0x00360b98, 0x0036998e, 0x003728d8, 0x0037b97a, 0x00384b76, 0x0038ded0,
0x0039738a, 0x003a09a8, 0x003aa12d, 0x003b3a1c, 0x003bd478, 0x003c7045, 0x003d0d85, 0x003dac3c,
0x003e4c6e, 0x003eee1d, 0x003f914d, 0x00403601, 0x0040dc3d, 0x00418404, 0x00422d5a, 0x0042d842,
0x004384c0, 0x004432d7, 0x0044e28c, 0x004593e0, 0x004646d9, 0x0046fb7a, 0x0047b1c7, 0x004869c3,
0x00492372, 0x0049ded9, 0x004a9bfa, 0x004b5ada, 0x004c1b7d, 0x004cdde6, 0x004da21a, 0x004e681d,
0x004f2ff3, 0x004ff9a0, 0x0050c527, 0x0051928e, 0x005261d8, 0x0053330a, 0x00540628, 0x0054db36,
0x0055b239, 0x00568b35, 0x0057662f, 0x0058432a, 0x0059222c, 0x005a0338, 0x005ae655, 0x005bcb85,
0x005cb2cf, 0x005d9c36, 0x005e87c0, 0x005f7570, 0x0060654d, 0x0061575a, 0x00624b9e, 0x0063421c,
0x00643ada, 0x006535dc, 0x00663329, 0x006732c5, 0x006834b5, 0x006938fe, 0x006a3fa6, 0x006b48b2,
0x006c5428, 0x006d620d, 0x006e7265, 0x006f8538, 0x00709a8a, 0x0071b261, 0x0072ccc2, 0x0073e9b4,
0x0075093c, 0x00762b60, 0x00775026, 0x00787793, 0x0079a1ae, 0x007ace7d, 0x007bfe05, 0x007d304d,
0x007e655b, 0x007f9d36, 0x0080d7e2, 0x00821568, 0x008355cd, 0x00849917, 0x0085df4d, 0x00872877,
0x00887499, 0x0089c3bc, 0x008b15e5, 0x008c6b1c, 0x008dc367, 0x008f1ecd, 0x00907d55, 0x0091df07,
0x009343ea, 0x0094ac03, 0x0096175c, 0x009785fa, 0x0098f7e6, 0x009a6d27, 0x009be5c3, 0x009d61c4,
0x009ee130, 0x00a0640f, 0x00a1ea69, 0x00a37446, 0x00a501ad, 0x00a692a6, 0x00a8273a, 0x00a9bf71,
0x00ab5b52, 0x00acfae6, 0x00ae9e36, 0x00b04549, 0x00b1f028, 0x00b39edc, 0x00b5516d, 0x00b707e4,
0x00b8c249, 0x00ba80a6, 0x00bc4304, 0x00be096a, 0x00bfd3e4, 0x00c1a278, 0x00c37532, 0x00c54c1a,
0x00c72739, 0x00c90699, 0x00caea44, 0x00ccd243, 0x00cebea0, 0x00d0af65, 0x00d2a49c, 0x00d49e4e,
0x00d69c85, 0x00d89f4d, 0x00daa6af, 0x00dcb2b5, 0x00dec36a, 0x00e0d8d9, 0x00e2f30c, 0x00e5120d,
0x00e735e9, 0x00e95ea8, 0x00eb8c58, 0x00edbf02, 0x00eff6b2, 0x00f23373, 0x00f47550, 0x00f6bc56,
0x00f9088f, 0x00fb5a08, 0x00fdb0cc, 0x01000ce8, 0x01026e66, 0x0104d554, 0x010741bd, 0x0109b3af,
0x010c2b34, 0x010ea85b, 0x01112b2f, 0x0113b3be, 0x01164214, 0x0118d63f, 0x011b704b, 0x011e1046,
0x0120b63d, 0x0123623e, 0x01261456, 0x0128cc94, 0x012b8b04, 0x012e4fb5, 0x01311ab6, 0x0133ec13,
0x0136c3dd, 0x0139a220, 0x013c86ec, 0x013f7250, 0x0142645b, 0x01455d1b, 0x01485c9f, 0x014b62f8,
0x014e7034, 0x01518463, 0x01549f94, 0x0157c1d9, 0x015aeb3f, 0x015e1bd9, 0x016153b6, 0x016492e6,
0x0167d97a, 0x016b2783, 0x016e7d11, 0x0171da36, 0x01753f03, 0x0178ab8a, 0x017c1fdb, 0x017f9c08,
0x01832024, 0x0186ac40, 0x018a406e, 0x018ddcc1, 0x0191814b, 0x01952e1f, 0x0198e350, 0x019ca0f0,
0x01a06713, 0x01a435cc, 0x01a80d2e, 0x01abed4e, 0x01afd63e, 0x01b3c814,0x01b7c2e4, 0x01bbc6c1,
0x01bfd3c0, 0x01c3e9f7, 0x01c80979, 0x01cc325d, 0x01d064b7, 0x01d4a09e, 0x01d8e627, 0x01dd3567,
0x01e18e75, 0x01e5f167, 0x01ea5e55, 0x01eed554, 0x01f3567b, 0x01f7e1e2, 0x01fc77a0, 0x020117cd,
0x0205c281, 0x020a77d2, 0x020f37db, 0x021402b3, 0x0218d872, 0x021db933, 0x0222a50d, 0x02279c1a,
0x022c9e75, 0x0231ac36, 0x0236c578, 0x023bea55, 0x02411ae8, 0x0246574c, 0x024b9f9b, 0x0250f3f0,
0x02565469, 0x025bc11f, 0x02613a2f, 0x0266bfb7, 0x026c51d1, 0x0271f09b, 0x02779c32, 0x027d54b4,
0x02831a3e, 0x0288eced, 0x028ecce1, 0x0294ba38, 0x029ab510, 0x02a0bd88, 0x02a6d3c0, 0x02acf7d8,
0x02b329ef, 0x02b96a25, 0x02bfb89b, 0x02c61572, 0x02cc80cb, 0x02d2fac7, 0x02d98389, 0x02e01b31,
0x02e6c1e3, 0x02ed77c0, 0x02f43ced, 0x02fb118c, 0x0301f5c1, 0x0308e9b0, 0x030fed7d, 0x0317014e,
0x031e2546, 0x0325598b, 0x032c9e44, 0x0333f395, 0x033b59a6, 0x0342d09c, 0x034a58a0, 0x0351f1d9,
0x03599c6e, 0x03615888, 0x0369264f, 0x037105ec, 0x0378f789, 0x0380fb50, 0x03891169, 0x03913a01,
0x03997542, 0x03a1c357, 0x03aa246c, 0x03b298ae, 0x03bb2048, 0x03c3bb67, 0x03cc6a3a, 0x03d52cee,
0x03de03b0, 0x03e6eeb1, 0x03efee1f, 0x03f90229, 0x04022b00, 0x040b68d4, 0x0414bbd6, 0x041e2437,
0x0427a22a, 0x043135e0, 0x043adf8d, 0x04449f63, 0x044e7597, 0x0458625c, 0x046265e9, 0x046c8070,
0x0476b22a, 0x0480fb4b, 0x048b5c0b, 0x0495d4a1, 0x04a06544, 0x04ab0e2e, 0x04b5cf97, 0x04c0a9b8,
0x04cb9ccc, 0x04d6a90d, 0x04e1ceb5, 0x04ed0e02, 0x04f8672f, 0x0503da78, 0x050f681c, 0x051b1058,
0x0526d36a, 0x0532b193, 0x053eab11, 0x054ac025, 0x0556f110, 0x05633e14, 0x056fa773, 0x057c2d6f,
0x0588d04d, 0x05959050, 0x05a26dbe, 0x05af68db, 0x05bc81ee, 0x05c9b93e, 0x05d70f12, 0x05e483b3,
0x05f21769, 0x05ffca7e, 0x060d9d3c, 0x061b8fee, 0x0629a2e0, 0x0637d65f, 0x06462ab7, 0x0654a037,
0x0663372d, 0x0671efe9, 0x0680cabb, 0x068fc7f4, 0x069ee7e5, 0x06ae2ae3, 0x06bd913e, 0x06cd1b4d,
0x06dcc964, 0x06ec9bd9, 0x06fc9302, 0x070caf37, 0x071cf0d1, 0x072d5828, 0x073de597, 0x074e9979,
0x075f7429, 0x07707605, 0x07819f69, 0x0792f0b5, 0x07a46a49, 0x07b60c83, 0x07c7d7c7, 0x07d9cc76,
0x07ebeaf3, 0x07fe33a3, 0x0810a6eb, 0x08234531, 0x08360edd, 0x08490456, 0x085c2606, 0x086f7458,
0x0882efb6, 0x0896988e, 0x08aa6f4c, 0x08be745f, 0x08d2a837, 0x08e70b44, 0x08fb9df9, 0x091060c8,
0x09255425, 0x093a7886, 0x094fce61, 0x0965562e, 0x097b1066, 0x0990fd82, 0x09a71dfd, 0x09bd7255,
0x09d3fb06, 0x09eab890, 0x0a01ab72, 0x0a18d42f, 0x0a303348, 0x0a47c942, 0x0a5f96a2, 0x0a779bef,
0x0a8fd9b1, 0x0aa85070, 0x0ac100b8, 0x0ad9eb16, 0x0af31015, 0x0b0c7046, 0x0b260c38, 0x0b3fe47d,
0x0b59f9a9, 0x0b744c50, 0x0b8edd09, 0x0ba9ac6a, 0x0bc4bb0e, 0x0be0098f, 0x0bfb9889, 0x0c17689b,
0x0c337a63, 0x0c4fce83, 0x0c6c659d, 0x0c894057, 0x0ca65f55, 0x0cc3c33f, 0x0ce16cc0, 0x0cff5c81,
0x0d1d932f, 0x0d3c117a, 0x0d5ad812, 0x0d79e7a7, 0x0d9940f0, 0x0db8e4a0, 0x0dd8d370, 0x0df90e19,
0x0e199557, 0x0e3a69e6, 0x0e5b8c86, 0x0e7cfdf7, 0x0e9ebefd, 0x0ec0d05d, 0x0ee332de, 0x0f05e748,
0x0f28ee68, 0x0f4c4909, 0x0f6ff7fc, 0x0f93fc12, 0x0fb8561d, 0x0fdd06f5, 0x10020f71, 0x1027706a,
0x104d2abd, 0x10733f49, 0x1099aeef, 0x10c07a91, 0x10e7a316, 0x110f2965, 0x11370e68, 0x115f530c,
0x1187f840, 0x11b0fef6, 0x11da6822, 0x120434ba, 0x122e65b7, 0x1258fc15, 0x1283f8d3, 0x12af5cf0,
0x12db2972, 0x13075f5e, 0x1333ffbd, 0x13610b9b, 0x138e8406, 0x13bc6a0f, 0x13eabecc, 0x14198352,
0x1448b8bc, 0x14786027, 0x14a87ab2, 0x14d90981, 0x150a0db9, 0x153b8883, 0x156d7b0b, 0x159fe681,
0x15d2cc16, 0x16062d01, 0x163a0a7a, 0x166e65bd, 0x16a34009, 0x16d89aa2, 0x170e76ce, 0x1744d5d5,
0x177bb906, 0x17b321b0, 0x17eb1128, 0x182388c5, 0x185c89e3, 0x189615e0, 0x18d02e20, 0x190ad408,
0x19460903, 0x1981ce7f, 0x19be25ee, 0x19fb10c5, 0x1a38907e, 0x1a76a698, 0x1ab55494, 0x1af49bf8,
0x1b347e4d, 0x1b74fd23, 0x1bb61a0c, 0x1bf7d69e, 0x1c3a3476, 0x1c7d3531, 0x1cc0da75, 0x1d0525e9,
0x1d4a193b, 0x1d8fb61c, 0x1dd5fe43, 0x1e1cf36a, 0x1e649751, 0x1eacebbc, 0x1ef5f275, 0x1f3fad4b,
0x1f8a1e0f, 0x1fd5469b, 0x202128cb, 0x206dc683, 0x20bb21aa, 0x21093c2c, 0x215817fc, 0x21a7b712,
0x21f81b6a, 0x22494708, 0x229b3bf3, 0x22edfc39, 0x234189ed, 0x2395e729, 0x23eb160c, 0x244118bb,
0x2497f161, 0x24efa22e,0x25482d5a, 0x25a19522, 0x25fbdbca, 0x2657039b, 0x26b30ee5, 0x27100000,
};

static MS_U16 DeHLG_Gamma_index[512] = {
   0,   9,  16,  23,  30,  37,  44,  50,  57,  63,  70,  76,  82,  88,  94,
 101, 107, 113, 119, 125, 131, 137, 142, 148, 154, 160, 166, 172, 177, 183, 189,
 195, 200, 206, 212, 217, 223, 228, 234, 240, 245, 251, 256, 262, 267, 273, 278,
 284, 289, 295, 300, 306, 311, 317, 322, 328, 333, 338, 344, 349, 355, 360, 365,
 371, 376, 381, 387, 392, 397, 403, 408, 413, 418, 424, 429, 434, 440, 445, 450,
 455, 461, 466, 471, 476, 481, 487, 492, 497, 502, 507, 513, 518, 523, 528, 533,
 538, 544, 549, 554, 559, 564, 569, 574, 579, 585, 590, 595, 600, 605, 610, 615,
 620, 625, 630, 635, 641, 646, 651, 656, 661, 666, 671, 676, 681, 686, 691, 696,
 701, 706, 711, 716, 721, 726, 731, 736, 741, 746, 751, 756, 761, 766, 771, 776,
 781, 786, 791, 796, 801, 806, 811, 816, 820, 825, 830, 835, 840, 845, 850, 855,
 860, 865, 870, 875, 880, 884, 889, 894, 899, 904, 909, 914, 919, 924, 928, 933,
 938, 943, 948, 953, 958, 963, 967, 972, 977, 982, 987, 992, 997,1001,1006,1011,
1016,1021,1026,1030,1035,1040,1045,1050,1054,1059,1064,1069,1074,1079,1083,1088,
1093,1098,1103,1107,1112,1117,1122,1127,1131,1136,1141,1146,1150,1155,1160,1165,
1170,1174,1179,1184,1189,1193,1198,1203,1208,1212,1217,1222,1227,1231,1236,1241,
1246,1250,1255,1260,1264,1269,1274,1279,1283,1288,1293,1298,1302,1307,1312,1316,
1321,1326,1331,1335,1340,1345,1350,1355,1360,1365,1370,1375,1380,1385,1390,1395,
1400,1406,1411,1416,1421,1427,1432,1438,1443,1449,1454,1460,1465,1471,1477,1482,
1488,1494,1500,1506,1512,1518,1523,1530,1536,1542,1548,1554,1560,1566,1573,1579,
1585,1592,1598,1605,1611,1618,1624,1631,1638,1644,1651,1658,1665,1672,1679,1685,
1692,1700,1707,1714,1721,1728,1735,1743,1750,1757,1765,1772,1780,1787,1795,1803,
1810,1818,1826,1834,1842,1849,1857,1865,1874,1882,1890,1898,1906,1915,1923,1931,
1940,1948,1957,1965,1974,1983,1991,2000,2009,2018,2027,2036,2045,2054,2063,2072,
2081,2091,2100,2110,2119,2128,2138,2148,2157,2167,2177,2187,2196,2206,2216,2226,
2237,2247,2257,2267,2277,2288,2298,2309,2319,2330,2341,2351,2362,2373,2384,2395,
2406,2417,2428,2439,2451,2462,2473,2485,2496,2508,2519,2531,2543,2555,2567,2579,
2591,2603,2615,2627,2639,2652,2664,2677,2689,2702,2714,2727,2740,2753,2766,2779,
2792,2805,2818,2831,2845,2858,2872,2885,2899,2913,2927,2940,2954,2968,2982,2997,
3011,3025,3040,3054,3069,3083,3098,3113,3128,3142,3157,3173,3188,3203,3218,3234,
3249,3265,3280,3296,3312,3328,3344,3360,3376,3392,3409,3425,3441,3458,3475,3491,
3508,3525,3542,3559,3576,3594,3611,3628,3646,3664,3681,3699,3717,3735,3753,3771,
3790,3808,3826,3845,3864,3882,3901,3920,3939,3958,3978,3997,4016,4036,4055,4075,
4095};
static MS_U16 Linear_index[512] = {
   0,   8,  16,  24,  32,  40,  48,  56,  64,  72,  80,  88,  96, 104, 112,
 120, 128, 136, 144, 152, 160, 168, 176, 184, 192, 200, 208, 216, 224, 232, 240,
 248, 256, 264, 272, 280, 288, 297, 305, 313, 321, 329, 337, 345, 353, 361, 369,
 377, 385, 393, 401, 409, 417, 425, 433, 441, 449, 457, 465, 473, 481, 489, 497,
 505, 513, 521, 529, 537, 545, 553, 561, 569, 577, 585, 593, 601, 609, 617, 625,
 633, 641, 649, 657, 665, 673, 681, 689, 697, 705, 713, 721, 729, 737, 745, 753,
 761, 769, 777, 785, 793, 801, 809, 817, 825, 833, 841, 849, 857, 865, 873, 882,
 890, 898, 906, 914, 922, 930, 938, 946, 954, 962, 970, 978, 986, 994,1002,1010,
1018,1026,1034,1042,1050,1058,1066,1074,1082,1090,1098,1106,1114,1122,1130,1138,
1146,1154,1162,1170,1178,1186,1194,1202,1210,1218,1226,1234,1242,1250,1258,1266,
1274,1282,1290,1298,1306,1314,1322,1330,1338,1346,1354,1362,1370,1378,1386,1394,
1402,1410,1418,1426,1434,1442,1450,1458,1467,1475,1483,1491,1499,1507,1515,1523,
1531,1539,1547,1555,1563,1571,1579,1587,1595,1603,1611,1619,1627,1635,1643,1651,
1659,1667,1675,1683,1691,1699,1707,1715,1723,1731,1739,1747,1755,1763,1771,1779,
1787,1795,1803,1811,1819,1827,1835,1843,1851,1859,1867,1875,1883,1891,1899,1907,
1915,1923,1931,1939,1947,1955,1963,1971,1979,1987,1995,2003,2011,2019,2027,2035,
2043,2052,2060,2068,2076,2084,2092,2100,2108,2116,2124,2132,2140,2148,2156,2164,
2172,2180,2188,2196,2204,2212,2220,2228,2236,2244,2252,2260,2268,2276,2284,2292,
2300,2308,2316,2324,2332,2340,2348,2356,2364,2372,2380,2388,2396,2404,2412,2420,
2428,2436,2444,2452,2460,2468,2476,2484,2492,2500,2508,2516,2524,2532,2540,2548,
2556,2564,2572,2580,2588,2596,2604,2612,2620,2628,2637,2645,2653,2661,2669,2677,
2685,2693,2701,2709,2717,2725,2733,2741,2749,2757,2765,2773,2781,2789,2797,2805,
2813,2821,2829,2837,2845,2853,2861,2869,2877,2885,2893,2901,2909,2917,2925,2933,
2941,2949,2957,2965,2973,2981,2989,2997,3005,3013,3021,3029,3037,3045,3053,3061,
3069,3077,3085,3093,3101,3109,3117,3125,3133,3141,3149,3157,3165,3173,3181,3189,
3197,3205,3213,3222,3230,3238,3246,3254,3262,3270,3278,3286,3294,3302,3310,3318,
3326,3334,3342,3350,3358,3366,3374,3382,3390,3398,3406,3414,3422,3430,3438,3446,
3454,3462,3470,3478,3486,3494,3502,3510,3518,3526,3534,3542,3550,3558,3566,3574,
3582,3590,3598,3606,3614,3622,3630,3638,3646,3654,3662,3670,3678,3686,3694,3702,
3710,3718,3726,3734,3742,3750,3758,3766,3774,3782,3790,3798,3807,3815,3823,3831,
3839,3847,3855,3863,3871,3879,3887,3895,3903,3911,3919,3927,3935,3943,3951,3959,
3967,3975,3983,3991,3999,4007,4015,4023,4031,4039,4047,4055,4063,4071,4079,4087,
4095};
static MS_U16 Index_offset[512] = {
   0,   0,   0,   1,   1,   1,   1,   1,   2,   2,   2,   2,   3,   3,   3,
   4,   4,   4,   5,   5,   5,   6,   6,   7,   7,   7,   8,   8,   9,   9,   9,
  10,  10,  11,  11,  12,  12,  13,  13,  14,  14,  15,  15,  16,  16,  17,  17,
  18,  18,  19,  19,  20,  20,  21,  21,  22,  22,  23,  23,  24,  24,  25,  25,
  26,  26,  27,  28,  28,  29,  29,  30,  30,  31,  31,  32,  33,  33,  34,  34,
  35,  35,  36,  37,  37,  38,  38,  39,  40,  40,  41,  41,  42,  43,  43,  44,
  44,  45,  46,  46,  47,  47,  48,  49,  49,  50,  51,  51,  52,  52,  53,  54,
  55,  55,  56,  56,  57,  58,  58,  59,  60,  60,  61,  62,  62,  63,  64,  64,
  65,  65,  66,  67,  67,  68,  69,  69,  70,  71,  71,  72,  73,  73,  74,  75,
  75,  76,  77,  77,  78,  79,  80,  80,  81,  82,  82,  83,  84,  84,  85,  86,
  86,  87,  88,  88,  89,  90,  91,  91,  92,  93,  93,  94,  94,  95,  96,  96,
  97,  97,  98,  98,  99,  99, 100, 100, 101, 101, 102, 102, 102, 103, 103, 103,
 104, 104, 104, 105, 105, 105, 105, 106, 106, 106, 106, 107, 107, 107, 107, 107,
 108, 108, 108, 108, 108, 108, 108, 109, 109, 109, 109, 109, 109, 109, 109, 109,
 109, 109, 109, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110, 110,
 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 109, 108, 108,
 108, 108, 108, 108, 108, 108, 108, 107, 107, 107, 107, 107, 107, 106, 106, 106,
 106, 106, 106, 105, 105, 105, 105, 105, 104, 104, 104, 104, 104, 103, 103, 103,
 103, 102, 102, 102, 102, 101, 101, 101, 101, 100, 100, 100, 100,  99,  99,  99,
  98,  98,  98,  98,  97,  97,  97,  96,  96,  96,  95,  95,  95,  95,  94,  94,
  94,  93,  93,  93,  92,  92,  92,  91,  91,  91,  90,  90,  90,  89,  89,  88,
  88,  88,  87,  87,  87,  86,  86,  86,  85,  85,  84,  84,  84,  83,  83,  82,
  82,  82,  81,  81,  80,  80,  80,  79,  79,  78,  78,  78,  77,  77,  76,  76,
  76,  75,  75,  74,  74,  73,  73,  72,  72,  72,  71,  71,  70,  70,  69,  69,
  68,  68,  68,  67,  67,  66,  66,  65,  65,  64,  64,  63,  63,  62,  62,  62,
  61,  61,  60,  60,  59,  59,  58,  58,  57,  57,  56,  56,  55,  55,  54,  54,
  53,  53,  52,  52,  51,  51,  50,  50,  49,  49,  48,  48,  47,  47,  46,  46,
  45,  45,  44,  44,  43,  43,  42,  42,  41,  41,  40,  39,  39,  38,  38,  37,
  37,  36,  36,  35,  35,  34,  34,  33,  32,  32,  31,  31,  30,  30,  29,  29,
  28,  28,  27,  26,  26,  25,  25,  24,  24,  23,  22,  22,  21,  21,  20,  20,
  19,  19,  18,  17,  17,  16,  16,  15,  15,  14,  13,  13,  12,  12,  11,  11,
  10,   9,   9,   8,   8,   7,   6,   6,   5,   5,   4,   3,   3,   2,   2,   1,
   0};

static MS_U32 u32TmoGamma22[513] = {
0x0, 0x3, 0x4, 0x5, 0x5, 0x6, 0x6, 0x7, 0x7, 0x7, 0x8, 0x8, 0x8, 0x9, 0x9, 0x9,
0xa, 0xa, 0xa, 0xa, 0xb, 0xb, 0xb, 0xb, 0xc, 0xc, 0xc, 0xc, 0xc, 0xd, 0xd, 0xd,
0xd, 0xe, 0xe, 0xe, 0xf, 0xf, 0xf, 0x10, 0x10, 0x10, 0x11, 0x11, 0x11, 0x11, 0x12, 0x12,
0x12, 0x13, 0x13, 0x14, 0x14, 0x15, 0x15, 0x15, 0x16, 0x16, 0x17, 0x17, 0x17, 0x18, 0x18, 0x19,
0x19, 0x1a, 0x1a, 0x1b, 0x1c, 0x1c, 0x1d, 0x1d, 0x1e, 0x1e, 0x1f, 0x20, 0x20, 0x21, 0x21, 0x22,
0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2b, 0x2c, 0x2d, 0x2d, 0x2e,
0x2f, 0x30, 0x31, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
0x40, 0x42, 0x44, 0x45, 0x47, 0x48, 0x4a, 0x4c, 0x4d, 0x4e, 0x50, 0x51, 0x53, 0x54, 0x55, 0x56,
0x58, 0x5a, 0x5d, 0x5f, 0x61, 0x63, 0x65, 0x67, 0x69, 0x6b, 0x6d, 0x6f, 0x71, 0x73, 0x75, 0x77,
0x78, 0x7c, 0x7f, 0x82, 0x85, 0x88, 0x8b, 0x8e, 0x91, 0x93, 0x96, 0x99, 0x9b, 0x9e, 0xa0, 0xa2,
0xa5, 0xa9, 0xae, 0xb2, 0xb6, 0xba, 0xbe, 0xc2, 0xc6, 0xca, 0xcd, 0xd1, 0xd4, 0xd8, 0xdb, 0xdf,
0xe2, 0xe8, 0xee, 0xf4, 0xfa, 0xff, 0x105, 0x10a, 0x10f, 0x115, 0x11a, 0x11e, 0x123, 0x128, 0x12c, 0x131,
0x135, 0x13e, 0x146, 0x14f, 0x156, 0x15e, 0x166, 0x16d, 0x174, 0x17b, 0x182, 0x188, 0x18f, 0x195, 0x19c, 0x1a2,
0x1a8, 0x1b4, 0x1bf, 0x1ca, 0x1d5, 0x1e0, 0x1ea, 0x1f4, 0x1fe, 0x207, 0x211, 0x21a, 0x223, 0x22c, 0x234, 0x23d,
0x245, 0x255, 0x265, 0x274, 0x283, 0x291, 0x2a0, 0x2ad, 0x2bb, 0x2c8, 0x2d4, 0x2e1, 0x2ed, 0x2f9, 0x305, 0x311,
0x31c, 0x332, 0x348, 0x35d, 0x371, 0x385, 0x398, 0x3ab, 0x3bd, 0x3cf, 0x3e1, 0x3f2, 0x403, 0x413, 0x424, 0x433,
0x443, 0x462, 0x47f, 0x49c, 0x4b8, 0x4d3, 0x4ed, 0x507, 0x520, 0x538, 0x551, 0x568, 0x57f, 0x596, 0x5ac, 0x5c2,
0x5d7, 0x601, 0x629, 0x651, 0x677, 0x69c, 0x6c0, 0x6e3, 0x706, 0x727, 0x748, 0x769, 0x788, 0x7a7, 0x7c6, 0x7e4,
0x801, 0x83a, 0x872, 0x8a7, 0x8dc, 0x90e, 0x940, 0x970, 0x9a0, 0x9ce, 0x9fb, 0xa27, 0xa52, 0xa7d, 0xaa7, 0xacf,
0xaf8, 0xb46, 0xb92, 0xbdc, 0xc23, 0xc69, 0xcad, 0xcef, 0xd30, 0xd6f, 0xdad, 0xdea, 0xe25, 0xe5f, 0xe98, 0xed0,
0xf08, 0xf73, 0xfdb, 0x1040, 0x10a2, 0x1102, 0x115f, 0x11ba, 0x1212, 0x1269, 0x12be, 0x1311, 0x1362, 0x13b2, 0x1400, 0x144d,
0x1499, 0x152c, 0x15ba, 0x1645, 0x16cb, 0x174e, 0x17ce, 0x184a, 0x18c4, 0x193a, 0x19ae, 0x1a20, 0x1a90, 0x1afd, 0x1b68, 0x1bd2,
0x1c39, 0x1d03, 0x1dc7, 0x1e84, 0x1f3d, 0x1ff0, 0x209f, 0x2149, 0x21ef, 0x2292, 0x2331, 0x23cd, 0x2466, 0x24fc, 0x258f, 0x261f,
0x26ad, 0x27c2, 0x28ce, 0x29d1, 0x2ace, 0x2bc4, 0x2cb3, 0x2d9d, 0x2e81, 0x2f60, 0x303a, 0x3110, 0x31e1, 0x32ae, 0x3378, 0x343e,
0x3500, 0x367b, 0x37ea, 0x394e, 0x3aa8, 0x3bf9, 0x3d41, 0x3e81, 0x3fba, 0x40eb, 0x4216, 0x433b, 0x445a, 0x4573, 0x4687, 0x4796,
0x48a1, 0x4aa8, 0x4c9f, 0x4e87, 0x5062, 0x522f, 0x53f1, 0x55a7, 0x5754, 0x58f6, 0x5a90, 0x5c21, 0x5daa, 0x5f2c, 0x60a6, 0x621a,
0x6387, 0x64ee, 0x664f, 0x67aa, 0x6900, 0x6a51, 0x6b9d, 0x6ce4, 0x6e27, 0x6f65, 0x709f, 0x71d5, 0x7307, 0x7435, 0x7560, 0x7687,
0x77ab, 0x78cc, 0x79e9, 0x7b03, 0x7c1a, 0x7d2e, 0x7e40, 0x7f4f, 0x805b, 0x8164, 0x826b, 0x836f, 0x8471, 0x8571, 0x866f, 0x876a,
0x8863, 0x8a4f, 0x8c32, 0x8e0e, 0x8fe3, 0x91b1, 0x9377, 0x9538, 0x96f2, 0x98a6, 0x9a55, 0x9bfd, 0x9da1, 0x9f3f, 0xa0d8, 0xa26d,
0xa3fd, 0xa588, 0xa70f, 0xa892, 0xaa10, 0xab8b, 0xad02, 0xae75, 0xafe4, 0xb150, 0xb2b8, 0xb41d, 0xb57e, 0xb6dd, 0xb838, 0xb990,
0xbae5, 0xbd87, 0xc01e, 0xc2ab, 0xc52d, 0xc7a5, 0xca15, 0xcc7b, 0xced9, 0xd12f, 0xd37d, 0xd5c3, 0xd802, 0xda39, 0xdc6a, 0xde95,
0xe0b8, 0xe2d6, 0xe4ee, 0xe700, 0xe90c, 0xeb13, 0xed14, 0xef11, 0xf108, 0xf2fa, 0xf4e8, 0xf6d1, 0xf8b6, 0xfa96, 0xfc72, 0xfe49,
0x1001d, };
//=============================================================================
// Local Function
//=============================================================================
#ifdef TMO_FUNCTION
//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
extern void msWriteByte(DWORD u32Reg, BYTE u8Val );
extern void msWriteByteMask(DWORD u32Reg, BYTE u8Val ,MS_U8 u8Mask);
extern BYTE msReadByte(DWORD u32Reg );
extern void MApi_GFLIP_XC_W2BYTE(DWORD u32Reg, WORD u16Val );
extern WORD MApi_GFLIP_XC_R2BYTE(DWORD u32Reg );
extern void MApi_GFLIP_XC_W2BYTEMSK(DWORD u32Reg, WORD u16Val, WORD u16Mask );
extern WORD MApi_GFLIP_XC_R2BYTEMSK(DWORD u32Reg, WORD u16Mask );
//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
void MHal_TMO_LinearTMO(void);
void MHal_TMO_OldTMO(MS_BOOL bWindow);

void MHal_XC_12pTmo_SetTmoParamVR(StuHDR_12pTmo_Param_Api *stuHdr12pTmoParamApi);

void Mhal_TMO_12pTmo_Top(MS_BOOL bWindow,StuHDR_12pTmo_Param_Api *stuHdr12pTmoParam);
BOOL Mhal_TMO_12pTmo_MemoryAlloc(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTmo_ParamInit(StuHDR_12pTmo_Param *stuHdr12pTmoParam,StuHDR_12pTmo_Param_Api *stuHdr12pTmoParam_input);
void Mhal_TMO_12pTmo_GetTmoParam(StuHDR_12pTmo_Param *stuHdr12pTmoParam, StuHDR_12pTmo_Param_Api *stuHdr12pTmoParamApi);
void Mhal_TMO_12pTmo_GetTmoParam_TmoDefaultSetting(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTmo_GetTmoParam_HlgDefaultSetting(StuHDR_HLG_Param *pHLG_tmoinput);
void Mhal_TMO_12pTmo_Gen12pCurve(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTmo_Linear_Tmo(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTmo_ManualTmoReferMetadata(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTMO_AutoTmoReferMetadata(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTMO_HlgTmo(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
MS_U32 Mhal_TMO_12pTMO_HlgTmo_LinearInterpolation(MS_U16* Lut_nonlinr_index,MS_U16* Lut_linr_index,MS_U16 index,MS_U32* Value_lut,MS_U32* Out_lut);
void Mhal_TMO_12pTmo_GenOotfCurve(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTmo_CurveInterpolation(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTmo_Approach_OOTF(MS_U16 *pSource,MS_U16 *pTarget, MS_U32 u32hlg_Gamma);
void Mhal_TMO_12pTmo_OotfGamma(MS_U32 *u32OOTFcurve512, MS_U32 u32gamma,MS_U8 u8ScalerIndex, MS_U8 u8domain);
void Mhal_TMO_12pTmo_WriteCurve(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTmo_WriteTmoCurveToVR(StuHDR_12pTmo_Param *stuHdr12pTmoParam);
void Mhal_TMO_12pTmo_MemeryFree(StuHDR_12pTmo_Param *stuHdr12pTmoParam);

void MHal_TMO_HDRFixDlcWriteCurve(MS_BOOL bWindow);
MS_U32 MHal_TMO_Pow(MS_U16 data_base, MS_U16 data_exp);
void MHal_TMO_HDR_ToneMappingGen_ByTable_V0(HDR_TMO_GEN_TABLE *pst_TMO_gen_table);
void MHal_TMO_APITMO(void);
MS_U16 MHal_TMO_Exp(MS_U16 data_base, MS_U16 data_exp,MS_U8 u8Mode);
MS_U32 MHal_TMO_Exp2(MS_U16 data_base, MS_U32 data_exp, MS_U8 u8Mode);
void MHal_TMO_HDR_ToneMappingCurveGen_Mod(MS_U16* pCurve18, HDR_ToneMappingData* pData, MS_BOOL bWindow);
void MHal_TMO_NewTMO(MS_BOOL bWindow);
void MHal_TMO_ReferTMO(void);
MS_U32 MHal_TMO_Luminance_To_GammaCode(MS_U16 u16Nits_in, MS_U16 u16maxLuma);
void MHal_TMO_RefernceTMOSetting(MS_U8 u8Mode);
//////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////
MS_BOOL MHal_TMO_TMOinit(void)
{
    printk("u16SrcMinRatio        :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMinRatio         );
    printk("u16SrcMedRatio        :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMedRatio         );
    printk("u16SrcMaxRatio        :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMaxRatio         );
    printk("u8TgtMinFlag          :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TgtMinFlag           );
    printk("u16TgtMin             :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMin              );
    printk("u8TgtMaxFlag          :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TgtMaxFlag           );
    printk("u16TgtMax             :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMax              );
    printk("u16TgtMed             :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMed              );
    printk("u16FrontSlopeMin      :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16FrontSlopeMin       );
    printk("u16FrontSlopeMax      :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16FrontSlopeMax       );
    printk("u16BackSlopeMin       :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16BackSlopeMin        );
    printk("u16BackSlopeMax       :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16BackSlopeMax        );
    printk("u16SceneChangeThrd    :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SceneChangeThrd     );
    printk("u16SceneChangeRatioMax:%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SceneChangeRatioMax );
    printk("u8IIRRatio            :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u8IIRRatio             );
    printk("u8TMO_TargetMode      :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_TargetMode       );
    printk("u8TMO_Algorithm       :%d\n",g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm        );
    #if 0
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMinRatio         = 10;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMedRatio         = 512;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMaxRatio         = 1019;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TgtMinFlag           = 1;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMin              = 500;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TgtMaxFlag           = 0;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMax              = 300;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMed              = 0;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16FrontSlopeMin = 256;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16FrontSlopeMax = 512;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16BackSlopeMin = 128;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16BackSlopeMax = 256;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SceneChangeThrd     = 512;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SceneChangeRatioMax = 1024;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8IIRRatio             = 31;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_TargetMode = 0;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm = 1;
    #endif
    return TRUE;
}

void MHal_TMO_LinearTMO(void)
{
     MS_U16 u16tmo_size = 512;
     MS_U16 u16source_max_luma = 10000;
     MS_U16 u16panel_max_luma = 1000;

     MS_U16 u16idx = 0;
     MS_U16 u16input = 0, u16input_code = 0, u16linear_in_code = 0;
     MS_U16 u16linear_out_code = 0;
     MS_U16 u16in_thrd = 0, u16out_thrd = 0;
     MS_U16 u16output = 0;
     u16panel_max_luma = MApi_GFLIP_XC_R2BYTE(REG_SC_BK4E_27_L);

     u16linear_in_code = (MS_U16)MHal_TMO_Luminance_To_GammaCode(u16panel_max_luma, u16source_max_luma);
     u16linear_out_code = 0xFFFC;
     u16in_thrd = min((u16linear_in_code * 0xFF8) / 0xFFFC, 0xFF8);
     u16out_thrd = min((u16linear_out_code * 0xFF8) / 0xFFFC, 0xFF8);
     for (u16idx = 0; u16idx <u16tmo_size; u16idx++)
     {
         u16input = (u16idx * 8);// 4088(0xFF8) as 1;
         u16input_code = u16input;
         u16code_in[u16idx] = u16input_code;

         u16output = (u16input_code*u16out_thrd) / max(u16in_thrd, 1); //output 0xFF8 as 1
         u16output = min(u16output, 0xFF8);

         u16tmo_lut[u16idx] = min(((u16output * 4095) / 4088), 4095);

     }

    MHal_CFD_SetHdrTmoLut(u16tmo_lut, 0, u16source_max_luma, u16panel_max_luma);

    if(u8PrintfOneTime == TRUE)
    {
        printk("\033[1;31m ###***###[%s][%d]\033[0m\n",__func__,__LINE__);
        u8PrintfOneTime = FALSE;
    }
}

void MHal_TMO_OldTMO(MS_BOOL bWindow)
{

    // set TMO function parameters
    HDRtmp.slope = 1582;
    HDRtmp.rolloff = 12288;

    if((_u16Count%300)==0)
    {
        //printk("\n  Kernel HDRAdjustMode=%x ; smin=%x , smax=%x ,tmax=%x ,tmin=%x ,smed=%x ,tmed=%x    \n",HDRAdjustMode,HDRtmp.smin,HDRtmp.smax,HDRtmp.tmax,HDRtmp.tmin,HDRtmp.smed,HDRtmp.tmed);
    }
    // 2084 format

    MHal_TMO_HDR_ToneMappingCurveGen_Mod(HDR_Curve,&HDRtmp,bWindow);
    if(u8PrintfOneTime == TRUE)
    {
        printk("\033[1;31m ###***###[%s][%d]\033[0m\n",__func__,__LINE__);
        u8PrintfOneTime = FALSE;
    }
}

void Mhal_TMO_12pTmo_Top(MS_BOOL bWindow,StuHDR_12pTmo_Param_Api *stuHdr12pTmoParam_input)
{
    BOOL bErrorCheck=FALSE;
    StuHDR_12pTmo_Param stuHdr12pTmoParam;
    //TMO array memory allocation
    bErrorCheck = Mhal_TMO_12pTmo_MemoryAlloc(&stuHdr12pTmoParam);

    if( FALSE == bErrorCheck )
    {
        printk("[TMO] Allocate memory fail!!\n");
        return;
    }

    //TMO parameters initialization
    Mhal_TMO_12pTmo_ParamInit(&stuHdr12pTmoParam,stuHdr12pTmoParam_input);

    //generate 12pts TMO curve
    Mhal_TMO_12pTmo_Gen12pCurve(&stuHdr12pTmoParam);

    //generate OOTF curve
    Mhal_TMO_12pTmo_GenOotfCurve(&stuHdr12pTmoParam);

    //gamma/tmo curve interpolation into 513/512 entires
    Mhal_TMO_12pTmo_CurveInterpolation(&stuHdr12pTmoParam);

    //write TMO result into hardware
    Mhal_TMO_12pTmo_WriteCurve(&stuHdr12pTmoParam);

    //TMO array memory free
    Mhal_TMO_12pTmo_MemeryFree(&stuHdr12pTmoParam);

    if(u8PrintfOneTime == TRUE)
    {
        printk("\033[1;31m ###***###[%s][%d]\033[0m\n",__func__,__LINE__);
        u8PrintfOneTime = FALSE;
    }
    /*MS_U16 i;
    for (i = 0; i < 12; i++)
    {
        printk("source[%d] = %d, target[%d] = %d\n", i, gStuHdr12pTmoParam.u16SrcNits12ptsOutput[i], i, gStuHdr12pTmoParam.u16TgtNits12ptsOutput[i]);
    }*/
}

#define GAMMALUT_SIZE 513
#define TMOLUT_SIZE 512
#define OOTFLUT_SIZE 513
BOOL Mhal_TMO_12pTmo_MemoryAlloc(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    stuHdr12pTmoParam->u32Gamma513Lut = (MS_U32*)kmalloc(GAMMALUT_SIZE * sizeof(MS_U32), GFP_KERNEL);
    stuHdr12pTmoParam->u16Tmo512Lut = (MS_U16*)kmalloc(TMOLUT_SIZE * sizeof(MS_U32), GFP_KERNEL);
    stuHdr12pTmoParam->stuHdrOotfParam.pu32_OOTFLUT = (MS_U32*)kmalloc(OOTFLUT_SIZE * sizeof(MS_U32), GFP_KERNEL);
    stuHdr12pTmoParam->stuHdrOotfParam.u32_OOTFLUT_size = OOTFLUT_SIZE;

    if((stuHdr12pTmoParam->u32Gamma513Lut == NULL) || (stuHdr12pTmoParam->u16Tmo512Lut == NULL) || (stuHdr12pTmoParam->stuHdrOotfParam.pu32_OOTFLUT == NULL))
    {
        if(NULL != stuHdr12pTmoParam->u32Gamma513Lut)
            kfree(stuHdr12pTmoParam->u32Gamma513Lut);
        if(NULL != stuHdr12pTmoParam->u16Tmo512Lut)
            kfree(stuHdr12pTmoParam->u16Tmo512Lut);
        if(NULL != stuHdr12pTmoParam->stuHdrOotfParam.pu32_OOTFLUT)
            kfree(stuHdr12pTmoParam->stuHdrOotfParam.pu32_OOTFLUT);

        return FALSE;
    }
    return TRUE;
}
#ifdef TMO_DBG_EN
void Mhal_TMO_12pTMO_Parameter_Debug(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MS_U8 u8Idx;
    for (u8Idx = E_12P_TMO_CURVE_ENTRIES_INDEX_START; u8Idx < E_12P_TMO_CURVE_ENTRIES_NUMBER; u8Idx++)
    {
        TMO_DBG(printk("source:%x target:%x\n",stuHdr12pTmoParam->u16SrcNits12ptsInput[u8Idx],stuHdr12pTmoParam->u16TgtNits12ptsInput[u8Idx]));
    }

    TMO_DBG(printk("u16HdrTmoManualEn                  :%x\n",stuHdr12pTmoParam->u16HdrTmoManualEn                 ) );
    TMO_DBG(printk("u16HdrTmoLinearEn                  :%x\n",stuHdr12pTmoParam->u16HdrTmoLinearEn                 ) );
    TMO_DBG(printk("stuHdrHlgParam.u8hlg_ootf_en       :%x\n",stuHdr12pTmoParam->stuHdrHlgParam.u8hlg_ootf_en       ));
    TMO_DBG(printk("stuHdrHlgParam.u8hlg_ootf_en       :%x\n",stuHdr12pTmoParam->stuHdrHlgParam.u8hlg_ootf_en      ) );
    TMO_DBG(printk("stuHdrHlgParam.u32TMO_HLG_POW      :%x\n",stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW     ) );
    TMO_DBG(printk("stuHdrHlgParam.u32hlg_systemGamma  :%x\n",stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma  ));
    TMO_DBG(printk("u8InputHdrMode                     :%x\n",stuHdr12pTmoParam->u8InputHdrMode                     ));
    TMO_DBG(printk("u16Metadata                        :%x\n",stuHdr12pTmoParam->u16Metadata                        ));
    TMO_DBG(printk("u8TmoVersion                       :%x\n",stuHdr12pTmoParam->u8TmoVersion                       ));
}
#endif
//12p TMO parameter initialization
void Mhal_TMO_12pTmo_ParamInit(StuHDR_12pTmo_Param *stuHdr12pTmoParam,StuHDR_12pTmo_Param_Api *stuHdr12pTmoParam_input)
{
#if 0//(1 == RealChip)
    Mhal_TMO_12pTmo_GetTmoParam(stuHdr12pTmoParam,stuHdr12pTmoParam_input);
    #ifdef TMO_DBG_EN
    Mhal_TMO_12pTMO_Parameter_Debug(stuHdr12pTmoParam);
    #endif
#else
    //Mhal_TMO_12pTMO_Parameter_Debug(&gStuHdr12pTmoParamApi);
    Mhal_TMO_12pTmo_GetTmoParam_TmoDefaultSetting(stuHdr12pTmoParam);
#endif
}

void Mhal_TMO_12pTmo_GetTmoParam(StuHDR_12pTmo_Param *stuHdr12pTmoParam, StuHDR_12pTmo_Param_Api *stuHdr12pTmoParamApi)
{
    MS_U8 u8Idx;
    for (u8Idx = E_12P_TMO_CURVE_ENTRIES_INDEX_START; u8Idx < E_12P_TMO_CURVE_ENTRIES_NUMBER; u8Idx++)
    {
        stuHdr12pTmoParam->u16SrcNits12ptsInput[u8Idx] = stuHdr12pTmoParamApi->u16SrcNits12ptsInput[u8Idx];
        stuHdr12pTmoParam->u16TgtNits12ptsInput[u8Idx] = stuHdr12pTmoParamApi->u16TgtNits12ptsInput[u8Idx];
    }
    stuHdr12pTmoParam->u16HdrTmoEn = stuHdr12pTmoParamApi->u16HdrTmoEn;
    stuHdr12pTmoParam->u16HdrTmoManualEn = stuHdr12pTmoParamApi->u16HdrTmoManualEn;
    stuHdr12pTmoParam->u16HdrTmoLinearEn = stuHdr12pTmoParamApi->u16HdrTmoLinearEn;

    stuHdr12pTmoParam->stuHdrHlgParam.u8hlg_ootf_en  = stuHdr12pTmoParamApi->stuHdrHlgParam.u8hlg_ootf_en;
    stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW = stuHdr12pTmoParamApi->stuHdrHlgParam.u32TMO_HLG_POW;
    stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW = ((MS_U64)(stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW))*0xffff/0x7f;
    stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW = (stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW == 0) ? 0xffff : stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW;

    stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma = stuHdr12pTmoParamApi->stuHdrHlgParam.u32hlg_systemGamma;
    stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma = (MS_U64)stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma*0xffff/0x7f;
    stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma = (stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma == 0) ? 0x13332 : stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma;

    stuHdr12pTmoParam->u8InputHdrMode = stuHdr12pTmoParamApi->u8InputHdrMode;
    stuHdr12pTmoParam->u16Metadata = stuHdr12pTmoParamApi->u16Metadata;
    stuHdr12pTmoParam->u8TmoVersion      = E_12P_TMO_VERSION_1;
}

void Mhal_TMO_12pTmo_GetTmoParam_TmoDefaultSetting(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MS_U16 source_nits[12] = {0,6,42,2000,224,285,380,768,1228,2880,4000,10000};
    MS_U16 target_nits[12]=  {0,10,49,3000,252,285,329,456,528,633,676,676};

    //input HDR format of source
    // 2: HDR10, SMPT2084, E_12P_TMO_HDR_MODE_HDR10
    // 3: HLG, E_12P_TMO_HDR_MODE_HLG
    stuHdr12pTmoParam->u8InputHdrMode = E_12P_TMO_HDR_MODE_HDR10;

    //clip of TMO source
    //in nits
    stuHdr12pTmoParam->u16Metadata = 1000;

    //HDR TMO manual/auto mode
    // 0: auto mode, E_12P_TMO_MANUAL_DISABLE
    // 1: manual mode, E_12P_TMO_MANUAL_ENABLE
    stuHdr12pTmoParam->u16HdrTmoManualEn = E_12P_TMO_MANUAL_ENABLE;

    //HDR TMO linear
    // 0: linear, E_12P_TMO_LINEAR_DISABLE
    // 1: linear, E_12P_TMO_LINEAR_ENABLE
    stuHdr12pTmoParam->u16HdrTmoLinearEn = E_12P_TMO_LINEAR_DISABLE;

    //source for TMO
    //in nits
    memcpy(stuHdr12pTmoParam->u16SrcNits12ptsInput,source_nits,12*sizeof(MS_U16));

    //target for TMO
    //in nits
    memcpy(stuHdr12pTmoParam->u16TgtNits12ptsInput,target_nits,12*sizeof(MS_U16));

    //param for HLG TMO
    Mhal_TMO_12pTmo_GetTmoParam_HlgDefaultSetting(&(stuHdr12pTmoParam->stuHdrHlgParam));

    //TMO process domain
    // 0: in Y domain
    // 1: in RGB domain
    stuHdr12pTmoParam->u8TmoVersion = E_12P_TMO_VERSION_0;
}

void Mhal_TMO_12pTmo_GetTmoParam_HlgDefaultSetting(StuHDR_HLG_Param *pHLG_tmoinput)
{
    //param for HLG TMO
    //gamma , 0xffff = 1
    //pHLG_tmoinput->u32TMO_HLG_POW = 0xffff;
    pHLG_tmoinput->u32TMO_HLG_POW = 0x13332;

    //enable for HLG OOTF function
    pHLG_tmoinput->u8hlg_ootf_en = 0;

    //param for HLG OOTF
    //gamma , 0xffff = 1
    //pHLG_tmoinput->u32hlg_systemGamma = 0xffff;
    pHLG_tmoinput->u32hlg_systemGamma = 0x13332;

    //0x1000 = 1
    pHLG_tmoinput->u16hlg_ootf_alpha = 0x1000;

    //0x1000 = 1
    pHLG_tmoinput->u16hlg_ootf_beta = 0x1000;

    pHLG_tmoinput->u32Gamma_afterOOTF = 0x7333;
}

//12pts TMO generate 12pts Source/Target result
void Mhal_TMO_12pTmo_Gen12pCurve(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    if(stuHdr12pTmoParam->u16HdrTmoManualEn == E_12P_TMO_MANUAL_ENABLE)//Manual control by user
    {
        if(stuHdr12pTmoParam->u16HdrTmoLinearEn == E_12P_TMO_LINEAR_ENABLE)
        {
            Mhal_TMO_12pTmo_Linear_Tmo(stuHdr12pTmoParam);
        }
        else
        {
            if(stuHdr12pTmoParam->u8InputHdrMode == E_12P_TMO_HDR_MODE_HDR10)//open HDR
            {
#if REFERENCE_METADATA_ENABLE
                Mhal_TMO_12pTmo_ManualTmoReferMetadata(stuHdr12pTmoParam);
#endif
            }
            else if (stuHdr12pTmoParam->u8InputHdrMode == E_12P_TMO_HDR_MODE_HLG)//HLG
            {
                //do nothing
            }
        }
    }
    else if(stuHdr12pTmoParam->u16HdrTmoManualEn == E_12P_TMO_MANUAL_DISABLE)  //auto gen 12p TMO curve
    {
        if(stuHdr12pTmoParam->u8InputHdrMode == E_12P_TMO_HDR_MODE_HDR10)//open HDR
        {
            Mhal_TMO_12pTMO_AutoTmoReferMetadata(stuHdr12pTmoParam);
        }
        else if(stuHdr12pTmoParam->u8InputHdrMode == E_12P_TMO_HDR_MODE_HLG)//HLG
        {
            Mhal_TMO_12pTMO_HlgTmo(stuHdr12pTmoParam);
        }
    }
}

void Mhal_TMO_12pTmo_Linear_Tmo(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MS_U16 u16LinearLut[12] = {0,100,200,300,500,700, 900,1000,2300,4000,6000,10000};
    MS_U16 *u16SrcNits12ptsOutput = stuHdr12pTmoParam->u16SrcNits12ptsOutput;
    MS_U16 *u16TgtNits12ptsOutput = stuHdr12pTmoParam->u16TgtNits12ptsOutput;
    MS_U16 u16CveIdx;
    for (u16CveIdx = E_12P_TMO_CURVE_ENTRIES_INDEX_START; u16CveIdx < E_12P_TMO_CURVE_ENTRIES_NUMBER; u16CveIdx++)
    {
        u16SrcNits12ptsOutput[u16CveIdx] = u16LinearLut[u16CveIdx];
        u16TgtNits12ptsOutput[u16CveIdx] = u16LinearLut[u16CveIdx];
    }
}

void Mhal_TMO_12pTmo_ManualTmoReferMetadata(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MS_U16 u16CveIdx;
    MS_U16 u16VideoMetadata;
    static MS_U16 u16OriginalMetadata,u16TargetMax;
    MS_U16 *u16SrcNits12ptsInput;
    MS_U16 *u16TgtNits12ptsInput;
    u16SrcNits12ptsInput = stuHdr12pTmoParam->u16SrcNits12ptsInput;
    u16TgtNits12ptsInput = stuHdr12pTmoParam->u16TgtNits12ptsInput;
    MS_U16 u16SrcNits12ptsOutput[12] = {0};
    MS_U16 u16TgtNits12ptsOutput[12] = {0};
    MS_U16 u16ClipIdxEnd = 0;
    MS_U16 u16ClipIdxStart = 0;
    MS_U16 u16TargetPre, u16TargetPost;
    MS_U16 u16Ratio;
    MS_U16 u16RatioStart = E_12P_TMO_MANUAL_MODE_RATIO_START;
    MS_U16 u16RatioEnd   = E_12P_TMO_MANUAL_MODE_RATIO_END;
    MS_U16 u16A0, u16A1, u16A2, u16B0, u16B1, u16B2;
    u16OriginalMetadata = u16SrcNits12ptsInput[10];
    u16TargetMax = u16TgtNits12ptsInput[10];
    u16VideoMetadata = stuHdr12pTmoParam->u16Metadata;
    //u16VideoMetadata = 6000;

    for (u16CveIdx = E_12P_TMO_CURVE_ENTRIES_INDEX_START; u16CveIdx < E_12P_TMO_CURVE_ENTRIES_NUMBER; u16CveIdx++)
    {
        u16SrcNits12ptsOutput[u16CveIdx] = u16SrcNits12ptsInput[u16CveIdx];
        u16TgtNits12ptsOutput[u16CveIdx] = u16TgtNits12ptsInput[u16CveIdx];
    }

    if (u16OriginalMetadata > u16VideoMetadata)
    {
        //find curve index that need to be modified
        for (u16CveIdx = E_12P_TMO_CURVE_ENTRIES_SEARCH_INDEX_START; u16CveIdx > E_12P_TMO_CURVE_ENTRIES_INDEX_START; u16CveIdx--)
        {
            if ((u16SrcNits12ptsInput[u16CveIdx] > u16VideoMetadata))
            {
                u16TgtNits12ptsOutput[u16CveIdx] = u16TargetMax;
                u16ClipIdxEnd = u16CveIdx;
                u16ClipIdxStart = u16CveIdx;
                break;
            }
        }

        for (u16CveIdx = u16ClipIdxEnd; u16CveIdx > E_12P_TMO_CURVE_ENTRIES_INDEX_START; u16CveIdx--)
        {
            if ((u16SrcNits12ptsInput[u16CveIdx] > u16VideoMetadata))
            {
                u16TgtNits12ptsOutput[u16CveIdx] = u16TargetMax;
                u16ClipIdxStart = u16CveIdx;
            }
            else
                break;
        }

        if (u16ClipIdxEnd != E_12P_TMO_CURVE_ENTRIES_SEARCH_INDEX_INIT) //need to be modified
        {
            for (u16CveIdx = u16ClipIdxStart; u16CveIdx <= u16ClipIdxEnd; u16CveIdx++)
            {
                u16TargetPre  = u16TgtNits12ptsOutput[max(u16CveIdx - 1, 0)];
                u16TargetPost = u16TgtNits12ptsOutput[min(u16CveIdx + 1, 11)];

                if (u16ClipIdxStart != u16ClipIdxEnd)
                    u16Ratio = ((u16RatioEnd - u16RatioStart) * (u16CveIdx - u16ClipIdxStart))/(u16ClipIdxEnd - u16ClipIdxStart) + u16RatioStart;
                else
                    u16Ratio = u16RatioEnd;

                u16TgtNits12ptsOutput[u16CveIdx] = (((u16TargetPost - u16TargetPre) * u16Ratio) / 32) + u16TargetPre;
                u16SrcNits12ptsOutput[u16CveIdx] = (u16SrcNits12ptsOutput[max((u16CveIdx-1),0)] + u16SrcNits12ptsOutput[min((u16CveIdx+1),11)])/2;
                u16TgtNits12ptsOutput[u16CveIdx] = min(u16TgtNits12ptsOutput[u16CveIdx], u16SrcNits12ptsOutput[u16CveIdx]);

                if (u16CveIdx > E_12P_TMO_CURVE_ENTRIES_SEARCH_INDEX_END)  //need to check slope
                {
                    // (a2 - a1)/(a1 - a0) = (x - b1)/(b1 - b0)
                    u16A0 = u16SrcNits12ptsOutput[max((u16ClipIdxStart - 2), 0)];
                    u16A1 = u16SrcNits12ptsOutput[max((u16ClipIdxStart - 1), 0)];
                    u16A2 = u16SrcNits12ptsOutput[max((u16ClipIdxEnd   - 0), 0)];
                    u16B0 = u16TgtNits12ptsOutput[max((u16ClipIdxStart - 2), 0)];
                    u16B1 = u16TgtNits12ptsOutput[max((u16ClipIdxStart - 1), 0)];
                    u16B2 = u16TgtNits12ptsOutput[max((u16ClipIdxEnd   - 0), 0)];

                    if ((u16A1 - u16A0) != 0)
                        u16B2 = ((((u16A2 - u16A1) * (u16B1 - u16B0))/(u16A1 - u16A0))) + u16B1;

                    u16TgtNits12ptsOutput[u16CveIdx] = min(u16TgtNits12ptsOutput[u16CveIdx], u16B2);
                }
            }
        }
    }
    else if (u16OriginalMetadata < u16VideoMetadata)
    {
        u16SrcNits12ptsOutput[10] = u16VideoMetadata;
        // (a1 - a0)/(a2 - a0) = (x - b0)/(b2 - b0)
        u16A0 = u16SrcNits12ptsInput[10];
        u16A1 = u16SrcNits12ptsOutput[10];
        u16A2 = u16SrcNits12ptsInput[11];
        u16B0 = u16TgtNits12ptsInput[10];
        u16B1 = u16TgtNits12ptsInput[11];
        u16B2 = u16TgtNits12ptsInput[11];
        if ((u16A2 - u16A0) != 0)
            u16B2 = ((((u16A1 - u16A0) * (u16B2 - u16B0))/(u16A2 - u16A0))) + u16B0;

        u16TgtNits12ptsOutput[10] = min(u16TgtNits12ptsOutput[11], u16B2);

    }

    for (u16CveIdx = E_12P_TMO_CURVE_ENTRIES_INDEX_START; u16CveIdx < E_12P_TMO_CURVE_ENTRIES_NUMBER; u16CveIdx++)
    {
        stuHdr12pTmoParam->u16SrcNits12ptsOutput[u16CveIdx] = u16SrcNits12ptsOutput[u16CveIdx];
        stuHdr12pTmoParam->u16TgtNits12ptsOutput[u16CveIdx] = u16TgtNits12ptsOutput[u16CveIdx];
    }
}

void Mhal_TMO_12pTMO_AutoTmoReferMetadata(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MS_U16 u16CveIdx;
    MS_U16 u16KneePointSource,u16Metadata;
    MS_U16 u16KneePointTarget;
    static MS_U16 u16SourceMax,u16TargetMax;
    MS_U16 u16A0, u16A1, u16A2, u16B0, u16B1, u16B2;

    MS_U16 *u16SrcNits12ptsInput = stuHdr12pTmoParam->u16SrcNits12ptsInput;
    MS_U16 *u16TgtNits12ptsInput = stuHdr12pTmoParam->u16TgtNits12ptsInput;
    MS_U16 *u16SrcNits12ptsOutput = stuHdr12pTmoParam->u16SrcNits12ptsOutput;
    MS_U16 *u16TgtNits12ptsOutput = stuHdr12pTmoParam->u16TgtNits12ptsOutput;
    u16Metadata = stuHdr12pTmoParam->u16Metadata;

    u16SourceMax = u16SrcNits12ptsInput[11];
    u16TargetMax = u16TgtNits12ptsInput[11];
    MS_U16 u16PanelMax = u16TargetMax;
    MS_U8 u8RatioLut[4]= {13,26,64,96},u8Ratio = 128;

    if(u16PanelMax <= E_12P_TMO_PANEL_DISPLAY_LUMINANCE_THRD_1)
    {
        u8Ratio = u8RatioLut[0];
    }
    else if(u16PanelMax <= E_12P_TMO_PANEL_DISPLAY_LUMINANCE_THRD_2)
    {
        u8Ratio = ((u8RatioLut[1] - u8RatioLut[0])*(u16PanelMax - 200))/(500 - 200) + u8RatioLut[0];
    }
    else if(u16PanelMax <= E_12P_TMO_PANEL_DISPLAY_LUMINANCE_THRD_3)
    {
        u8Ratio = ((u8RatioLut[2] - u8RatioLut[1])*(u16PanelMax - 500))/(750 - 500) + u8RatioLut[1];
    }
    else if(u16PanelMax <= E_12P_TMO_PANEL_DISPLAY_LUMINANCE_THRD_4)
    {
        u8Ratio = ((u8RatioLut[3] - u8RatioLut[2])*(u16PanelMax - 750))/(1000 - 750) + u8RatioLut[2];
    }
    else
    {
        u8Ratio = u8RatioLut[3];
    }
    u16KneePointSource = u16TargetMax*u8Ratio>>7;

    u16Metadata = min(u16SourceMax, u16Metadata);
    u16Metadata = min(E_12P_TMO_METADATA_VALID_MAX, u16Metadata);

    u16KneePointTarget = u16KneePointSource;

    u16SrcNits12ptsOutput[0]   = 0;
    u16SrcNits12ptsOutput[1]   =  u16KneePointSource/16;
    u16SrcNits12ptsOutput[2]   =  u16KneePointSource*5/16;
    u16SrcNits12ptsOutput[3]   =  u16KneePointSource*10/16;
    u16SrcNits12ptsOutput[4]   =  u16KneePointSource;
    u16SrcNits12ptsOutput[5]   = ((u16Metadata-u16KneePointSource)*14/256)+u16KneePointSource;
    u16SrcNits12ptsOutput[6]   = ((u16Metadata-u16KneePointSource)*34/256)+u16KneePointSource;
    u16SrcNits12ptsOutput[7]   = ((u16Metadata-u16KneePointSource)*60/256)+u16KneePointSource;
    u16SrcNits12ptsOutput[8]   = ((u16Metadata-u16KneePointSource)*81/256)+u16KneePointSource;
    u16SrcNits12ptsOutput[9]   = ((u16Metadata-u16KneePointSource)*161/256)+u16KneePointSource;
    u16SrcNits12ptsOutput[10]  = u16Metadata;
    u16SrcNits12ptsOutput[11]  = u16SourceMax;

    u16TgtNits12ptsOutput[0]   = 0;
    u16TgtNits12ptsOutput[1]   = u16KneePointTarget/16;
    u16TgtNits12ptsOutput[2]   = u16KneePointTarget*5/16;
    u16TgtNits12ptsOutput[3]   = u16KneePointTarget*10/16;
    u16TgtNits12ptsOutput[4]   = u16KneePointTarget;
    u16TgtNits12ptsOutput[5]   = ((u16TargetMax-u16KneePointTarget)*60/256)+u16KneePointTarget;
    u16TgtNits12ptsOutput[6]   = ((u16TargetMax-u16KneePointTarget)*95/256)+u16KneePointTarget;
    u16TgtNits12ptsOutput[7]   = ((u16TargetMax-u16KneePointTarget)*130/256)+u16KneePointTarget;
    u16TgtNits12ptsOutput[8]   = ((u16TargetMax-u16KneePointTarget)*150/256)+u16KneePointTarget;
    u16TgtNits12ptsOutput[9]   = ((u16TargetMax-u16KneePointTarget)*200/256)+u16KneePointTarget;
    u16TgtNits12ptsOutput[10]  = ((u16TargetMax-u16KneePointTarget)*225/256)+u16KneePointTarget;
    u16TgtNits12ptsOutput[11]  = u16TargetMax;

    if (u16Metadata > E_12P_TMO_METADATA_HIGH_METADATA_THRD)
    {
        // (a1 - a0)/(a2 - a0) = (x - b0)/(b2 - b0)
        u16A0 = u16SrcNits12ptsOutput[9];
        u16A1 = u16Metadata;
        u16A2 = u16SrcNits12ptsOutput[11];
        u16B0 = u16TgtNits12ptsOutput[9];
        u16B1 = u16TgtNits12ptsOutput[10];
        u16B2 = u16TgtNits12ptsOutput[11];

        if ((u16A2 - u16A0) != 0)
            u16B1 = ((((u16A1 - u16A0) * (u16B2 - u16B0))/(u16A2 - u16A0))) + u16B0;

        u16TgtNits12ptsOutput[10] = min(u16SrcNits12ptsOutput[11], u16B1);
    }

    for (u16CveIdx = E_12P_TMO_CURVE_ENTRIES_INDEX_START; u16CveIdx < E_12P_TMO_CURVE_ENTRIES_NUMBER; u16CveIdx++)
    {
        u16TgtNits12ptsOutput[u16CveIdx] = min(u16SrcNits12ptsOutput[u16CveIdx], u16TgtNits12ptsOutput[u16CveIdx]);
    }
}

void Mhal_TMO_12pTMO_HlgTmo(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MS_U32 u32Gamma;
    MS_U16 * u16SrcNits12ptsOutput = stuHdr12pTmoParam->u16SrcNits12ptsOutput;
    MS_U16 * u16TgtNits12ptsOutput = stuHdr12pTmoParam->u16TgtNits12ptsOutput;
    u32Gamma = stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW;

    ////Gamma  parameter start////
    //0x13332 gamma 1.2 0xFFFF = 1
    //0x16665 gamma 1.4
    MS_U32 /*,u32Lut[512],u32PowerLut[512]*/u32Tmp;
    MS_U16 u16LutIdx/*,u32FinalLut[512]*/,u16Temp,u16Source[12],u16Target[12];
    MS_U32 *u32Lut = (MS_U32*)kmalloc(512 * sizeof(MS_U32), GFP_KERNEL);
    MS_U32 *u32PowerLut = (MS_U32*)kmalloc(512 * sizeof(MS_U32), GFP_KERNEL);
    MS_U32 *u32FinalLut = (MS_U32*)kmalloc(512 * sizeof(MS_U32), GFP_KERNEL);
    ////Gamma  parameter End///
    for(u16LutIdx = 0; u16LutIdx<512;u16LutIdx++)
    {
        u32Tmp = ((MS_U64)u16LutIdx)*0xFFFF/511;
        u32PowerLut[u16LutIdx] =(MS_U32)newPow04(u32Tmp , u32Gamma);
    }
    u32Lut[0] = u32PowerLut[0];
    for(u16LutIdx = 1; u16LutIdx<512;u16LutIdx++)
    {
        u32Lut[u16LutIdx] = Mhal_TMO_12pTMO_HlgTmo_LinearInterpolation(DeHLG_Gamma_index,Linear_index,u16LutIdx,u32PowerLut,u32Lut);
        u16Temp =  (MS_U16)((((MS_U64)u32Lut[u16LutIdx])*4095 + (1<<28))>>29);
        u32FinalLut[u16LutIdx] = min(u16Temp,4095);
    }
    u32FinalLut[0] = min((MS_U16)(u32Lut[0] >>17),4095);
    for(u16LutIdx = 0;u16LutIdx <12;u16LutIdx++)
    {
        u16Temp = u16LutIdx*511/11;
        u16Source[u16LutIdx] = (MS_U32)u16Temp*10000/511;
        u16Target[u16LutIdx] = (MS_U32)u32FinalLut[u16Temp]*10000/4095;
    }
    u32Gamma = 0x23331; //degamma 2.2 to 12p TMO parameter
    for(u16LutIdx = 0;u16LutIdx <12;u16LutIdx++)
    {
        u32Tmp = (MS_U64)u16Source[u16LutIdx]*0xFFFF/u16Source[11];
        u16SrcNits12ptsOutput[u16LutIdx] = (((MS_U64)newPow04(u32Tmp , u32Gamma))*10000 +(1<<28))>>29;
        u32Tmp = (MS_U64)u16Target[u16LutIdx]*0xFFFF/u16Target[11];
        u16TgtNits12ptsOutput[u16LutIdx] = (((MS_U64)newPow04(u32Tmp , u32Gamma))*10000 +(1<<28))>>29;

    }
    kfree((void *)u32Lut);
    kfree((void *)u32PowerLut);
    kfree((void *)u32FinalLut);
}

MS_U32 Mhal_TMO_12pTMO_HlgTmo_LinearInterpolation(MS_U16* Lut_nonlinr_index,MS_U16* Lut_linr_index,MS_U16 index,MS_U32* Value_lut,MS_U32* Out_lut)
{
    MS_U32 u32Temp,u32Offset;
    u32Offset = Index_offset[index];
    u32Temp = (((Value_lut[index + u32Offset] - Out_lut[index-1])*100/(Lut_nonlinr_index[index + u32Offset]-Lut_linr_index[index-1]))*(Lut_linr_index[index ] - Lut_linr_index[index - 1])+50)/100 + Out_lut[index-1];
    return u32Temp;
}

void Mhal_TMO_12pTmo_GenOotfCurve(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MS_U32 u32Gamma;
    MS_U32 u32TMO_HLG_POW ;
    MS_U8  u8hlg_ootf_en ;
    MS_U32 u32hlg_systemGamma ;
    MS_U8  u8HDR_mode;
    MS_U16 *u16SrcNits12pts;
    MS_U16 *u16TgtNits12pts;
    MS_U32 *u32OOTFcurve512;

    u8HDR_mode         =  stuHdr12pTmoParam->u8InputHdrMode;
    u32TMO_HLG_POW     =  stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW;
    u8hlg_ootf_en      =  stuHdr12pTmoParam->stuHdrHlgParam.u8hlg_ootf_en;
    u32hlg_systemGamma =  stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma;
    u32OOTFcurve512    =  stuHdr12pTmoParam->stuHdrOotfParam.pu32_OOTFLUT;

    u16SrcNits12pts = stuHdr12pTmoParam->u16SrcNits12ptsOutput;
    u16TgtNits12pts = stuHdr12pTmoParam->u16TgtNits12ptsOutput;

    if(( E_CFIO_MODE_HDR3 == u8HDR_mode) && (u8hlg_ootf_en == 1))
    {
        Mhal_TMO_12pTmo_Approach_OOTF(u16SrcNits12pts, u16TgtNits12pts, u32hlg_systemGamma); // modify tmo value
        u32Gamma = ((MS_U64)u32hlg_systemGamma)*0xFFFF/(u32hlg_systemGamma+0x13332);// modify oetf gamma value
        Mhal_TMO_12pTmo_OotfGamma(u32OOTFcurve512, u32Gamma,1, stuHdr12pTmoParam->u8TmoVersion);

        stuHdr12pTmoParam->stuHdrHlgParam.u32Gamma_afterOOTF = u32Gamma;
    }
}

void Mhal_TMO_12pTmo_Approach_OOTF(MS_U16 *pSource,MS_U16 *pTarget, MS_U32 u32hlg_Gamma)
{
    ////OOTF  parameter start////
    //0x13332 gamma 1.2 0xFFFF = 1
    //0x23331 gamma 2.2

    MS_U32 u32inv_tmoGamma = ((MS_U64)0x23331)*0xFFFF/(0x13332+u32hlg_Gamma);
    ////OOTF  parameter End///

    MS_U8 u8LutIdx;
    MS_U16 u16source, u16final_source;
    MS_U32 u32new_source, u32Tmp;

    // adjust TMO curve by OOTF tmoGamma
    for(u8LutIdx = 1; u8LutIdx<12;u8LutIdx++)
    {

            u16source = pSource[u8LutIdx];
            u32Tmp = ((MS_U64)u16source)*0xFFFF/10000;
            u32new_source =(MS_U32)newPow04(u32Tmp , u32inv_tmoGamma);  // 1 is 1<<29
            u16final_source =  (MS_U16)((((MS_U64)u32new_source)*10000+ (1<<28))>>29);
            pSource[u8LutIdx] = u16final_source;

    }
}

//param
//
//u8domain - 0: YUV domain ; 1: RGB domain
//
void Mhal_TMO_12pTmo_OotfGamma(MS_U32 *u32OOTFcurve512, MS_U32 u32gamma,MS_U8 u8ScalerIndex, MS_U8 u8domain)
{
    MS_U32 u16Adder = 0;

    MS_U16 counter =0;
    //MS_U32 gammaTable[513];
    //MS_U32 *gammaTable = (MS_U32*)kmalloc(513 * sizeof(MS_U32), GFP_KERNEL);

    MS_U16 u16ResPQ[33] =    {2,2,4,8,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16, 32, 32, 32,1};
    MS_U16 u1Mul[33] =      {0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,24,25,26,32} ;
    int i,j;
    for( i =0; i<32 ; i++)
        for( j = 0; j<u16ResPQ[i] ; j++)
        {
            u32OOTFcurve512[counter++] = (newPow32(u16Adder,u32gamma)>>13);
            u16Adder = u16Adder + (1<<u1Mul[i]);
        }
    u32OOTFcurve512[512]=0x1001d;

    if (0 == u8domain)
    {
        gamma(4,0,0,0,1,u32OOTFcurve512,513,E_GAMMA_MASERATI_MD,NULL,2); //use when TMO is not in gamma
    }

    //kfree((void *)gammaTable);
}

void Mhal_TMO_12pTmo_CurveInterpolation(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MS_U32 u32Temp3=0;
    MS_U32 u32Temp4=0;
    MS_U32 u32SrcRatio, u32TgtRatio;
    ////Gamma 2.2 parameter start////
    MS_U32 u32Gamma =0x7333/*0x745D*/;
    MS_U16 u16Alpha = 0x8000,u16Beta = 1,u16LowerBoundGain = 0x0100;
    MS_U8 u8BitDepth=16,sMode = 0;
    ////Gamma 2.2 parameter End///

    MS_U32 u32TMO_HLG_POW ;
    MS_U8  u8hlg_ootf_en ;
    MS_U32 u32hlg_systemGamma ;
    MS_U8  u8integerStart = 3;

    if(stuHdr12pTmoParam->u8TmoVersion == 0)
    {
        u8integerStart = E_12P_TMO_TMO_CURVE_INT_START_VERSION_0;
    }
    else
    {
        u8integerStart = E_12P_TMO_TMO_CURVE_INT_START_VERSION_1;
    }

    MS_U16 *source_nits;
    MS_U16 *target_nits;
    MS_U32 *u32OOTFcurve512;

    u32TMO_HLG_POW = stuHdr12pTmoParam->stuHdrHlgParam.u32TMO_HLG_POW;
    u8hlg_ootf_en  = stuHdr12pTmoParam->stuHdrHlgParam.u8hlg_ootf_en;
    u32hlg_systemGamma =  stuHdr12pTmoParam->stuHdrHlgParam.u32hlg_systemGamma;

    source_nits = stuHdr12pTmoParam->u16SrcNits12ptsOutput;
    target_nits = stuHdr12pTmoParam->u16TgtNits12ptsOutput;
    u32OOTFcurve512 = stuHdr12pTmoParam->stuHdrOotfParam.pu32_OOTFLUT;

    //transfer to gamma code
    MS_U16 source_gamma[12]={0};
    MS_U16 target_gamma[12]={0};
    MS_U16 *u16curve512 = stuHdr12pTmoParam->u16Tmo512Lut;
    MS_U32 *u32curve512 = stuHdr12pTmoParam->u32Gamma513Lut;
    MS_U16 i = 0,q = 0;

    if(( E_CFIO_MODE_HDR3 == stuHdr12pTmoParam->u8InputHdrMode) && (u8hlg_ootf_en == 1))
    {
        u32Gamma = stuHdr12pTmoParam->stuHdrHlgParam.u32Gamma_afterOOTF;
    }

    if(E_CFIO_MODE_HDR3 == stuHdr12pTmoParam->u8InputHdrMode )//HLG
    {
        for( i=0; i<12;i++)
        {
            u32Temp3 =(MS_U32)(min((source_nits[i]*0x10000ul/source_nits[11]), 0x10000ul));
            source_gamma[i] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp3, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
            u32Temp4 =(MS_U32)(min((target_nits[i]*0x10000ul/target_nits[11]), 0x10000ul));
            target_gamma[i] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp4, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
            //printf("source gamma[%d] = %d, target gamma[%d] = %d\n",i,source_gamma[i], i,target_gamma[i]);
        }
    }
    else
    {
        for( i=0; i<12;i++)
        {
            if(i< u8integerStart)
            {
                if (source_nits[i] <= source_nits[11])
                {
                    u32Temp3 =(MS_U32)(min((source_nits[i]*0x10000ul/source_nits[11]), 0x10000ul));
                    source_gamma[i] = ((MS_U32)Mhal_CFD_GammaSampling(u32Temp3, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode))*0x0B6F/0xFFFF;
                }
                else
                {
                    u32SrcRatio = ((MS_U32)source_nits[i]*0x10000ul/source_nits[11]);
                    u32Temp3 = (MS_U32)(min((u32SrcRatio * 131) >> 17, 0x10000ul));
                    source_gamma[i] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp3, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
                }
            }
            else
            {
                u32Temp3 =(MS_U32)(min((source_nits[i]*0x10000ul/source_nits[11]), 0x10000ul));
                source_gamma[i] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp3, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
            }

            if(i < u8integerStart)
            {
                if (target_nits[i] <= target_nits[11])
                {
                    u32Temp4 =(MS_U32)(min((target_nits[i]*0x10000ul/target_nits[11]), 0x10000ul));
                    target_gamma[i] = ((MS_U32)Mhal_CFD_GammaSampling(u32Temp4, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode))*0x0B6F/0xFFFF;
                }
                else
                {
                    u32TgtRatio = ((MS_U32)target_nits[i]*0x10000ul/target_nits[11]);
                    u32Temp4 = (MS_U32)(min((u32TgtRatio * 131) >> 17, 0x10000ul));
                    target_gamma[i] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp4, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
                }
            }
            else
            {
                u32Temp4 =(MS_U32)(min((target_nits[i]*0x10000ul/target_nits[11]), 0x10000ul));
                target_gamma[i] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp4, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
            }
        }

    }

#if 0
    {
        FILE *pt_file;
        pt_file  = fopen("output_TMO_gamma.txt","w");
        fprintf(pt_file,"%s\n",__FUNCTION__);

        MS_U16 u16idx;
        for (u16idx=0; u16idx<12 ; u16idx++)
        {
            fprintf(pt_file,"source_gamma[%03d] = 0x%04x\n",u16idx, source_gamma[u16idx]);
        }
        for (u16idx=0; u16idx<12 ; u16idx++)
        {
            fprintf(pt_file,"target_gamma[%03d] = 0x%04x\n",u16idx, target_gamma[u16idx]);
        }
        fclose(pt_file);
    }
#endif

    //hermite cubic interpolation
    //Get consecutive differences and slopes
    MS_S64 dx[11]={0};
    MS_S64 dy[11]={0};
    MS_S64 ms[11]={0};

    for ( i = 0; i< 11; i++)
    {
        dx[i] = source_gamma[i+1] - source_gamma[i];//s16
        dy[i] = target_gamma[i+1] - target_gamma[i];//s16
        if (dx[i] !=0)
            ms[i] = (( MS_S64)dy[i]<<16)/dx[i]; //delta(i)  //s16<<16->s32b
        else
            ms[i] = 0;
        //printf("ms[%d]= %d\n",i,ms[i]);
    }

    //Get degree-1 coefficients
    int idx = 0;
    MS_S64 c1s[12]={0};
    MS_S64 m=0,m_nxt=0;
    MS_S64 dx_=0,dx_nxt=0;
    MS_S64 common=0;
    MS_S64 dx_nxt_dev=0,dx_dev=0;

    c1s[0] = ms[0];
    idx = idx +1;
    for ( i = 0; i< 10; i++)
    {
        m =ms[i];//s32b
        m_nxt = ms[i+1];//s32b

        if ((MS_S64)m*m_nxt <= 0)
            c1s[idx]=0;//s32b
        else
        {
            dx_ =dx[i];//s16
            dx_nxt =dx[i+1];//s16
            common = dx_+ dx_nxt;//s17b

            dx_nxt_dev = ((MS_S64)(common + dx_nxt)<<16); //s33b
            if( m!=0)
                dx_nxt_dev = (dx_nxt_dev/m); //s33b

            dx_dev = ((MS_S64)(common + dx_)<<16);
            if( m_nxt!=0)
                dx_dev = (dx_dev/m_nxt);

            if( dx_nxt_dev + dx_dev != 0)
                c1s[idx] = (3*(MS_S64)(common<<16))/(dx_nxt_dev + dx_dev);
            else
                c1s[idx] =  (3*(MS_S64)(common<<16));
        }
        //printf("cls[%d]= %d\n",idx,c1s[idx]);
        idx = idx +1;

    }
    c1s[idx] = ms[10];

    //Get degree-2 and -3 coefficients
    MS_S64 c1=0,m_=0;
    MS_S64 invDx=0;
    MS_S64 common_=0,c2s[11]={0},c3s[11]={0},mk[12]={0};
    MS_S64 ak[11]={0},bk[11]={0},tk[11]={0};
    MS_S64 k=0;

    idx = 1;
    for ( i = 0; i< 11; i++)
    {
        c1 = c1s[i];//s32
        m_ = ms[i];//s32

        if(dx[i]!=0)
            invDx = (1<<24)/dx[i];
        else
            invDx = (1<<24);

        common_ = c1 + c1s[i + 1] - m_ - m_;  // d(i)+d(i+1) -2*delta(i)

        c2s[i] = (((m_ - c1 - common_)*invDx)+128)>>8;  // [-2*d(i)-d(i+3*delta(i)]/(h(i))
        c3s[i] = (((common_*invDx +128)>>8)*invDx+128)>>8;        // [d(i)+d(i+1)-2*delta(i)]/(h(i)^2)
        // printf("c2s[%d]= %d\n",i,c1 );
    }

    mk[0] = ms[0];
    for( i = 1; i < (ms[10]>>16); i++)
    {
        mk[i] = (ms[i-1] +  ms[i])/2;
        //printf("mk[%d]= %d\n",i,mk[i]);
        if (ms[i]==0)
            mk[i] =0;
    }
    mk[11] = ms[10];

    MS_S64 sq_ak,sq_bk;
    MS_S64 sq_sum;
    for( i = 0; i< 11; i++)
    {
        if(ms[i] != 0)
        {
            ak[i] = (mk[i]<<16)/ms[i];
            bk[i] = (mk[min(i+1, 11)]<<16)/ms[i];
        }
        else
        {
            ak[i] = mk[i]<<16;
            bk[i] = mk[min(i+1, 11)]<<16;
        }

        sq_ak = (MS_S64)(ak[i])*(ak[i])>>16;
        sq_bk = (MS_S64)(bk[i])*(bk[i])>>16;
        sq_sum = ((sq_ak + sq_bk) * (sq_ak + sq_bk))>>16;

        if (sq_sum == 0)
            tk[i] = 1;
        else
        {
            if(sq_sum != 0)
                k = (3<<16)/sq_sum;
            else
                k=3<<16;

            tk[i] = k;
        }
        //printf("tk[%d]=%d\n",i,tk[i]);
    }

    //MS_U32 tmo_new[513]={0},tmo_out[513]={0};
    MS_U64 in=0,x_cur=0,x_nxt=0;
    MS_U64 p0=0,diffSq =0;
    MS_U64 cur_idx=0,nxt_idx=0;
    MS_U64 max_output_val=32760;  //4095 << 3 = 32760  4080 << 3 = 32640 (0x7F80)

    MS_U64 tmo_new_out =0;

    MS_S64 diff;

    MS_S64 tmp1, tmp2,tmp3,tmp4;
    MS_U64 tmo_out_tmp=0;
    MS_U32 tmo_pre=0;
    idx=0;
    //printk("====start====\n");
    MS_U16 TMO_in_Gamma;

    if(1 == stuHdr12pTmoParam->u8TmoVersion) //RGB
    {
        TMO_in_Gamma = 0x0008;
    }
    else //YUV
    {
        TMO_in_Gamma = 0;
    }

    for( i = 0; i< 512; i++)
    {
        if(TMO_in_Gamma == 0x0008)
        {
            if( ( E_CFIO_MODE_HDR2 == stuHdr12pTmoParam->u8InputHdrMode)  || (u8hlg_ootf_en == 0))
            {
                in = (MS_U64)u32TmoGamma22[i];
            }
            else
            {
                in = (MS_U64)u32OOTFcurve512[i];
            }
            u16curve512[i] = (i*4095)/511;
        }
        else
        {
            in = (i<<16)/511;
        }


        for ( q = 0; q < 11; q++)
        {
            if ((source_gamma[q] < in ) & (source_gamma[q+1] > in))
            {
                cur_idx = q;
                break;
            }
            else if (source_gamma[q] == in )
            {
                cur_idx = q;
                break;
            }
        }

        nxt_idx = min(cur_idx + 1, 11);
        x_cur = source_gamma[cur_idx];
        x_nxt = source_gamma[nxt_idx];
#if 0
        t = ((MS_U64)(in-x_cur)<<16)/(x_nxt - x_cur);

        t3 = (((MS_U64)t*t>>16)*t)>>16;
        t2 = ((MS_U64)t*t)>>16;

        s1 = (((2*t3)) - ((3*t2)) +(1<<16));
        s2 = t3 -2*t2 +t;
        s3 = (MS_S32)(-2*t3) + 3*t2;
        s4 = t3 - t2;
#endif
        p0 = target_gamma[cur_idx];
        diff = (in-x_cur) ;

        diffSq = (diff*diff+(1<<15))>>16;
        tmp1 = (c1s[cur_idx]*diff + (1<<15) )>>16;
        tmp2 = (c2s[cur_idx]*diffSq + (1<<15))>>16;
        tmp3 = (c3s[cur_idx]*diff +(1<<15))>>16;
        tmp4 = (tmp3 * diffSq +(1<<15))>>16;

        tmo_new_out = p0 +((tmp1+ tmp2 + tmp4));

        //tmo_new_out = (p0 +(c1s[cur_idx]*diff )+ (c2s[cur_idx]*diffSq) + (c3s[cur_idx]*diff*diffSq);
        if(TMO_in_Gamma == 0x0008)
        {
            tmo_out_tmp = (((MS_U64)(tmo_new_out * max_output_val) +(((MS_U64)1)<<14))>>15);
        }
        else
        {
            tmo_out_tmp = (((MS_U64)(tmo_new_out * max_output_val) +(1<<18))>>19);
        }


        if((MS_U32)tmo_out_tmp < tmo_pre){      //prevent inverse tone
            tmo_out_tmp = (MS_U64)tmo_pre;
        }

        if(TMO_in_Gamma == 0x0008)
        {
            u32curve512[idx] = min((MS_U32)tmo_out_tmp,65535);
            tmo_pre = min((MS_U32)tmo_out_tmp,65535);
        }
        else
        {
            u16curve512[idx] = min((MS_U16)tmo_out_tmp,4095);
            tmo_pre = u16curve512[idx];
        }

        idx= idx+1;

    } //end of for( i = 0; i< 512; i++)

    u32curve512[512] = (65535 - u32curve512[511])/3 +65535;

    //printk("====End====\n");
}

void Mhal_TMO_12pTmo_WriteCurve(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    if(stuHdr12pTmoParam->u8TmoVersion == E_12P_TMO_VERSION_1)
    {
        MHal_CFD_SetHdrOETFLutOnly(stuHdr12pTmoParam->u32Gamma513Lut);
        MHal_CFD_SetHdrTmoLut(stuHdr12pTmoParam->u16Tmo512Lut, 0, stuHdr12pTmoParam->u16SrcNits12ptsOutput[11], stuHdr12pTmoParam->u16SrcNits12ptsOutput[11]);
    }
    else if(stuHdr12pTmoParam->u8TmoVersion == E_12P_TMO_VERSION_0)
    {
        if(( E_12P_TMO_HDR_MODE_HDR10 == stuHdr12pTmoParam->u8InputHdrMode) || (stuHdr12pTmoParam->stuHdrHlgParam.u8hlg_ootf_en == 0))
        {
            //MHal_CFD_SetHdrTmoLut(u32TmoGamma22, 0,4);// for debug only
        }
        else
        {
            //MHal_CFD_SetHdrTmoLut(u32OOTFcurve512, 0,4);// for debug only
        }
        MHal_CFD_SetHdrTmoLut(stuHdr12pTmoParam->u16Tmo512Lut, 0, stuHdr12pTmoParam->u16SrcNits12ptsOutput[11], stuHdr12pTmoParam->u16TgtNits12ptsOutput[11]);
    }
    Mhal_TMO_12pTmo_WriteTmoCurveToVR(stuHdr12pTmoParam);
}

void Mhal_TMO_12pTmo_WriteTmoCurveToVR(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_01_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[1]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_02_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[2]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_03_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[3]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_04_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[4]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_05_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[5]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_06_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[6]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_07_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[7]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_08_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[8]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_09_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[9]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_0A_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[10]  );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_0B_L,stuHdr12pTmoParam->u16SrcNits12ptsOutput[11]  );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_0F_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[1]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_10_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[2]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_11_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[3]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_12_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[4]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_13_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[5]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_14_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[6]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_15_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[7]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_16_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[8]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_17_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[9]   );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_18_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[10]  );
    MHal_XC_VR_W2BYTE(REG_SC_VR_BK07_19_L,stuHdr12pTmoParam->u16TgtNits12ptsOutput[11]  );
}


void Mhal_TMO_12pTmo_MemeryFree(StuHDR_12pTmo_Param *stuHdr12pTmoParam)
{
    if(NULL != stuHdr12pTmoParam->u32Gamma513Lut)
        kfree(stuHdr12pTmoParam->u32Gamma513Lut);
    if(NULL != stuHdr12pTmoParam->u16Tmo512Lut)
        kfree(stuHdr12pTmoParam->u16Tmo512Lut);
    if(NULL != stuHdr12pTmoParam->stuHdrOotfParam.pu32_OOTFLUT)
        kfree(stuHdr12pTmoParam->stuHdrOotfParam.pu32_OOTFLUT);
}

void MHal_TMO_WriteTmoCurve(MS_BOOL bWindow)
{
    MHal_XC_12pTmo_SetTmoParamVR(&gStuHdr12pTmoParamApi);
    if (g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm == 2)
    {
        //msReferTMO();
        MHal_TMO_ReferTMO();
        g_DlcParameters.u8Dlc_Mode = 1;
    }
    else if (g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm == 1)
    {
        msNewTMO(bWindow);
        //MHal_TMO_NewTMO(bWindow);
        g_DlcParameters.u8Dlc_Mode = 0;
    }
    else if(g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm == 3)
    {
        //msLinearTMO();
        MHal_TMO_LinearTMO();
        g_DlcParameters.u8Dlc_Mode = 1;
    }
    else
    {
        if (0 == g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_UseIniControls)
        {
            if(_stCfdTmo.u16Length == 0)
            {
                //msOldTMO(bWindow);
                MHal_TMO_OldTMO(bWindow);          //in order to compatab, will be removed when TMO 12P CL all submit
            }
            else
            {
                //ms12pTMO(bWindow);
                Mhal_TMO_12pTmo_Top(bWindow,&gStuHdr12pTmoParamApi);
            }
        }
        else
        {
            //msAPITMO();
            MHal_TMO_APITMO();
        }
        g_DlcParameters.u8Dlc_Mode = 1;
    }
    MHal_TMO_RefernceTMOSetting(g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm);
}
void MHal_XC_12pTmo_SetTmoParamVR(StuHDR_12pTmo_Param_Api *stuHdr12pTmoParamApi)
{
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[0] = 0;
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[1] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_01_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[2] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_02_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[3] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_03_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[4] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_04_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[5] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_05_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[6] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_06_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[7] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_07_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[8] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_08_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[9] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_09_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[10] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_0A_L);
    stuHdr12pTmoParamApi->u16SrcNits12ptsInput[11] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_0B_L);

    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[0] = 0;
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[1] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_0F_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[2] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_10_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[3] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_11_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[4] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_12_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[5] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_13_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[6] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_14_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[7] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_15_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[8] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_16_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[9] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_17_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[10] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_18_L);
    stuHdr12pTmoParamApi->u16TgtNits12ptsInput[11] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_19_L);

    if(stuHdr12pTmoParamApi->u8UiHdrOff == UI_HDR_OFF)
    {
        stuHdr12pTmoParamApi->u16HdrTmoLinearEn = E_12P_TMO_LINEAR_ENABLE;
        stuHdr12pTmoParamApi->u16HdrTmoManualEn = E_12P_TMO_MANUAL_ENABLE;
    }
    else
    {
        stuHdr12pTmoParamApi->u16HdrTmoManualEn = MHal_XC_VR_R2BYTEMSK(REG_SC_VR_BK03_20_L,0x0001);
        stuHdr12pTmoParamApi->u16HdrTmoLinearEn = MHal_XC_VR_R2BYTEMSK(REG_SC_VR_BK03_20_L,0x0004) >> 2;
    }

#if OOTF_HW_EXIST
    stuHdr12pTmoParamApi->stuHdrHlgParam.u8hlg_ootf_en = FALSE;
#else
    stuHdr12pTmoParamApi->stuHdrHlgParam.u8hlg_ootf_en  = (MHal_XC_VR_R2BYTEMSK(REG_SC_VR_BK03_20_L, 0x0008)) >> 3;
#endif

    stuHdr12pTmoParamApi->stuHdrHlgParam.u32TMO_HLG_POW = MHal_XC_VR_R2BYTEMSK(REG_SC_VR_BK03_21_L,0x00ff);
    stuHdr12pTmoParamApi->stuHdrHlgParam.u32hlg_systemGamma = (MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_21_L)>>8);
}
MS_BOOL MHal_XC_12pTmoVR_IsTmoChanged(void)
{
    MS_U8 u8index = 0;
    static MS_U32 u32parameter[32]={0};
    MS_BOOL b12pupdate = TRUE,bOptionpupdate = TRUE;
    MS_U8 u8max = 12;
    for(u8index = 0;u8index< u8max;u8index++)
    {
        if((u32parameter[u8index] != MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_00_L+u8index*2))||(u32parameter[u8index+u8max] != MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_0E_L+u8index*2)))
        {
            b12pupdate = TRUE;
            break;
        }
        else
        {
            b12pupdate = FALSE;
        }
    }
    if((u32parameter[u8max*2] != MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_20_L))||(u32parameter[u8max*2+ 1] != MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_21_L)))
    {
        bOptionpupdate = TRUE;
    }
    else
    {
        bOptionpupdate = FALSE;
    }
    if(bOptionpupdate == TRUE || b12pupdate == TRUE)
    {
        for(u8index = 0;u8index< u8max;u8index++)
        {
            u32parameter[u8index] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_00_L+u8index*2);
            u32parameter[u8index+u8max] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_0E_L+u8index*2);
        }
        u32parameter[u8max*2] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_20_L);
        u32parameter[u8max*2 + 1] = MHal_XC_VR_R2BYTE(REG_SC_VR_BK03_21_L);
    }
    return bOptionpupdate|b12pupdate;
}
void MHal_TMO_TmoHandler(MS_BOOL bWindow)
{
    //MS_BOOL bUseDlc = TRUE;
    //u8log = LogPrintfOneTimes();
    static MS_U8 u8PreTMO_Algorithm = 0xFF;

    if(1 == u8log)
    {
        MHal_TMO_TMOinit();
    }
    if(MApi_GFLIP_XC_R2BYTE(REG_SC_BK30_01_L) == 0x001C)
    {
        g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm = msReadByte(REG_SC_BK30_0D_L);
        if (u8PreTMO_Algorithm != g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm)
        {
            u8PrintfOneTime = TRUE;
            u8PreTMO_Algorithm = g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm;
        }
    }
    if (bTmoFireEnable && MHAL_XC_IsCFDInitFinished(bWindow)){
        if(g_DlcParameters.u8Dlc_Mode == 2 || g_DlcParameters.u8Tmo_Mode == 1)
        {
            //msNewTMO(bWindow);
            if (g_HDRinitParameters.DLC_HDRCustomerDlcCurve.bFixHdrCurve == TRUE &&
                g_HDRinitParameters.DLC_HDRCustomerDlcCurve.pucDlcCurve != NULL &&
                g_HDRinitParameters.DLC_HDRCustomerDlcCurve.u16DlcCurveSize == DLC_CURVE_SIZE) //use customer fix DLC curve.
            {
                //bUseDlc = FALSE;
                msHDRFixDlcWriteCurve(bWindow);//HDR Fix DLC
                //MHal_TMO_HDRFixDlcWriteCurve(bWindow);//HDR Fix DLC
            }
            else
            {
                if (MHal_XC_12pTmoVR_IsTmoChanged())
                {
                    MHal_XC_CFD_SetTmoVal();
                    MHal_TMO_WriteTmoCurve(bWindow);
                    if(FALSE == MHal_XC_FireAutoDownload(E_KDRV_XC_AUTODOWNLOAD_CLIENT_HDR))
                    {
                        printk("M+D Gamma AutoDownload FAILED!\n");
                    }
                }
            }
        }
    }
}

void MHal_TMO_HDRFixDlcWriteCurve(MS_BOOL bWindow)//HDR Fix DLC
{
    // Write data to fix luma curve ...
    int ucTmp=0;
    if( MAIN_WINDOW == bWindow )
    {
        for(ucTmp=0; ucTmp<=0x0f; ucTmp++)
        {
            g_ucTable[ucTmp] = g_HDRinitParameters.DLC_HDRCustomerDlcCurve.pucDlcCurve[ucTmp];
            msWriteByte((REG_ADDR_DLC_DATA_START_MAIN+ucTmp), g_ucTable[ucTmp]);
            msDlcWriteCurveLSB(bWindow, ucTmp, g_uwTable[ucTmp]&0x03);
            // set DLC curve index N0 & 16
            if (ucTmp == 0)
            {
                msWriteByte(REG_ADDR_DLC_DATA_EXTEND_N0_MAIN, g_HDRinitParameters.DLC_HDRCustomerDlcCurve.pucDlcCurve[16]);
                msWriteByte(REG_ADDR_DLC_DATA_EXTEND_N0_MAIN + 1, 0x01);

            }
            if (ucTmp == 15)
            {
                msWriteByte(REG_ADDR_DLC_DATA_EXTEND_16_MAIN, g_HDRinitParameters.DLC_HDRCustomerDlcCurve.pucDlcCurve[17]);
                msWriteByte(REG_ADDR_DLC_DATA_EXTEND_16_MAIN + 1, 0x00);
            }
        }
    }

    if(u8PrintfOneTime == TRUE)
    {
        printk("\033[1;31m ###***###[%s][%d]\033[0m\n",__func__,__LINE__);
        u8PrintfOneTime = FALSE;
    }
}

MS_U32 MHal_TMO_Pow(MS_U16 data_base, MS_U16 data_exp)
{
    // returns data_base^data_exp
    // data_base : 0.16 (0xFFFF->1.0)
    // data_exp : 4.12  (0x1000->1.0)
    // data_out : 0.16  (0xFFFF->1.0)
    MS_U16 idx, lsb, shift;
    MS_U32 data_tmp, log_res, xlog_res, final_res;

    // input 0.16, output 4.16
    // data  4.16, [(0:63); (32:63)*2; (32:63)*2^2; (32:63)*2^3; (32:63)*2^4; (32:63)*2^5; (32:63)*2^6; (32:63)*2^7; (32:63)*2^8; (32:63)*2^9; (32:64)*2^10]
    static const MS_U32 log_lut[385] =
    {
        0xC0000,   0xB1721,   0xA65AF,   0x9FDE3,   0x9B43D,   0x97B1D,   0x94C71,   0x924FA,   0x902CB,   0x8E4A4,   0x8C9AB,   0x8B145,   0x89AFF,   0x88681,   0x87388,   0x861DF,
        0x85159,   0x841D4,   0x83332,   0x8255B,   0x81839,   0x80BBC,   0x7FFD3,   0x7F472,   0x7E98D,   0x7DF19,   0x7D50F,   0x7CB66,   0x7C216,   0x7B91A,   0x7B06D,   0x7A808,
        0x79FE7,   0x79806,   0x79062,   0x788F6,   0x781C0,   0x77ABC,   0x773E9,   0x76D42,   0x766C7,   0x76075,   0x75A4A,   0x75443,   0x74E61,   0x748A0,   0x74300,   0x73D7E,
        0x7381A,   0x732D3,   0x72DA7,   0x72895,   0x7239D,   0x71EBC,   0x719F3,   0x71541,   0x710A4,   0x70C1C,   0x707A8,   0x70348,   0x6FEFB,   0x6FABF,   0x6F696,   0x6F27D,
        0x6EE75,   0x6E694,   0x6DEF0,   0x6D784,   0x6D04E,   0x6C94A,   0x6C277,   0x6BBD0,   0x6B555,   0x6AF03,   0x6A8D7,   0x6A2D1,   0x69CEF,   0x6972E,   0x6918E,   0x68C0C,
        0x686A8,   0x68161,   0x67C35,   0x67723,   0x6722B,   0x66D4A,   0x66881,   0x663CF,   0x65F32,   0x65AAA,   0x65636,   0x651D6,   0x64D88,   0x6494D,   0x64524,   0x6410B,
        0x63D03,   0x63522,   0x62D7E,   0x62612,   0x61EDC,   0x617D8,   0x61104,   0x60A5E,   0x603E3,   0x5FD91,   0x5F765,   0x5F15F,   0x5EB7D,   0x5E5BC,   0x5E01B,   0x5DA9A,
        0x5D536,   0x5CFEF,   0x5CAC3,   0x5C5B1,   0x5C0B9,   0x5BBD8,   0x5B70F,   0x5B25D,   0x5ADC0,   0x5A938,   0x5A4C4,   0x5A064,   0x59C16,   0x597DB,   0x593B1,   0x58F99,
        0x58B91,   0x583B0,   0x57C0C,   0x574A0,   0x56D6A,   0x56666,   0x55F92,   0x558EC,   0x55271,   0x54C1F,   0x545F3,   0x53FED,   0x53A0B,   0x5344A,   0x52EA9,   0x52928,
        0x523C4,   0x51E7D,   0x51951,   0x5143F,   0x50F47,   0x50A66,   0x5059D,   0x500EB,   0x4FC4E,   0x4F7C6,   0x4F352,   0x4EEF2,   0x4EAA4,   0x4E669,   0x4E23F,   0x4DE27,
        0x4DA1F,   0x4D23E,   0x4CA9A,   0x4C32E,   0x4BBF8,   0x4B4F4,   0x4AE20,   0x4A77A,   0x4A0FF,   0x49AAC,   0x49481,   0x48E7B,   0x48898,   0x482D8,   0x47D37,   0x477B6,
        0x47252,   0x46D0B,   0x467DF,   0x462CD,   0x45DD4,   0x458F4,   0x4542B,   0x44F79,   0x44ADC,   0x44654,   0x441E0,   0x43D80,   0x43932,   0x434F7,   0x430CD,   0x42CB5,
        0x428AD,   0x420CC,   0x41927,   0x411BC,   0x40A86,   0x40382,   0x3FCAE,   0x3F608,   0x3EF8D,   0x3E93A,   0x3E30F,   0x3DD09,   0x3D726,   0x3D166,   0x3CBC5,   0x3C644,
        0x3C0E0,   0x3BB99,   0x3B66D,   0x3B15B,   0x3AC62,   0x3A782,   0x3A2B9,   0x39E06,   0x3996A,   0x394E2,   0x3906E,   0x38C0E,   0x387C0,   0x38385,   0x37F5B,   0x37B43,
        0x3773A,   0x36F5A,   0x367B5,   0x3604A,   0x35913,   0x35210,   0x34B3C,   0x34496,   0x33E1B,   0x337C8,   0x3319D,   0x32B97,   0x325B4,   0x31FF4,   0x31A53,   0x314D2,
        0x30F6E,   0x30A27,   0x304FB,   0x2FFE9,   0x2FAF0,   0x2F610,   0x2F147,   0x2EC94,   0x2E7F7,   0x2E370,   0x2DEFC,   0x2DA9B,   0x2D64E,   0x2D213,   0x2CDE9,   0x2C9D0,
        0x2C5C8,   0x2BDE8,   0x2B643,   0x2AED8,   0x2A7A1,   0x2A09E,   0x299CA,   0x29324,   0x28CA8,   0x28656,   0x2802B,   0x27A25,   0x27442,   0x26E81,   0x268E1,   0x26360,
        0x25DFC,   0x258B5,   0x25389,   0x24E77,   0x2497E,   0x2449E,   0x23FD5,   0x23B22,   0x23685,   0x231FD,   0x22D8A,   0x22929,   0x224DC,   0x220A1,   0x21C77,   0x2185E,
        0x21456,   0x20C76,   0x204D1,   0x1FD65,   0x1F62F,   0x1EF2C,   0x1E858,   0x1E1B2,   0x1DB36,   0x1D4E4,   0x1CEB9,   0x1C8B3,   0x1C2D0,   0x1BD0F,   0x1B76F,   0x1B1ED,
        0x1AC8A,   0x1A742,   0x1A216,   0x19D05,   0x1980C,   0x1932C,   0x18E63,   0x189B0,   0x18513,   0x1808B,   0x17C18,   0x177B7,   0x1736A,   0x16F2F,   0x16B05,   0x166EC,
        0x162E4,   0x15B04,   0x1535F,   0x14BF3,   0x144BD,   0x13DBA,   0x136E6,   0x1303F,   0x129C4,   0x12372,   0x11D47,   0x11741,   0x1115E,   0x10B9D,   0x105FD,   0x1007B,
        0xFB18,    0xF5D0,    0xF0A4,    0xEB93,    0xE69A,    0xE1BA,    0xDCF1,    0xD83E,    0xD3A1,    0xCF19,    0xCAA5,    0xC645,    0xC1F8,    0xBDBC,    0xB993,    0xB57A,
        0xB172,    0xA991,    0xA1ED,    0x9A81,    0x934B,    0x8C47,    0x8574,    0x7ECD,    0x7852,    0x7200,    0x6BD5,    0x65CF,    0x5FEC,    0x5A2B,    0x548B,    0x4F09,
        0x49A6,    0x445E,    0x3F32,    0x3A20,    0x3528,    0x3048,    0x2B7F,    0x26CC,    0x222F,    0x1DA7,    0x1933,    0x14D3,    0x1086,     0xC4A,     0x821,     0x408,
        0x0
    };
    // input 4.16, output 0.16
    // data  0.16, 2^4*[(0:63); (32:63)*2; (32:63)*2^2; (32:63)*2^3; (32:63)*2^4; (32:63)*2^5; (32:63)*2^6; (32:63)*2^7; (32:63)*2^8; (32:63)*2^9; (32:64)*2^10]
    static const MS_U16 exp_lut[385] =
    {
        0xFFFF,    0xFFEF,    0xFFDF,    0xFFCF,    0xFFBF,    0xFFAF,    0xFF9F,    0xFF8F,    0xFF7F,    0xFF6F,    0xFF5F,    0xFF4F,    0xFF3F,    0xFF2F,    0xFF1F,    0xFF0F,
        0xFEFF,    0xFEF0,    0xFEE0,    0xFED0,    0xFEC0,    0xFEB0,    0xFEA0,    0xFE90,    0xFE80,    0xFE70,    0xFE60,    0xFE50,    0xFE41,    0xFE31,    0xFE21,    0xFE11,
        0xFE01,    0xFDF1,    0xFDE1,    0xFDD1,    0xFDC2,    0xFDB2,    0xFDA2,    0xFD92,    0xFD82,    0xFD72,    0xFD62,    0xFD53,    0xFD43,    0xFD33,    0xFD23,    0xFD13,
        0xFD03,    0xFCF4,    0xFCE4,    0xFCD4,    0xFCC4,    0xFCB4,    0xFCA5,    0xFC95,    0xFC85,    0xFC75,    0xFC66,    0xFC56,    0xFC46,    0xFC36,    0xFC26,    0xFC17,
        0xFC07,    0xFBE7,    0xFBC8,    0xFBA9,    0xFB89,    0xFB6A,    0xFB4A,    0xFB2B,    0xFB0B,    0xFAEC,    0xFACD,    0xFAAD,    0xFA8E,    0xFA6F,    0xFA4F,    0xFA30,
        0xFA11,    0xF9F2,    0xF9D2,    0xF9B3,    0xF994,    0xF975,    0xF956,    0xF936,    0xF917,    0xF8F8,    0xF8D9,    0xF8BA,    0xF89B,    0xF87C,    0xF85D,    0xF83E,
        0xF81F,    0xF7E1,    0xF7A3,    0xF765,    0xF727,    0xF6E9,    0xF6AC,    0xF66E,    0xF630,    0xF5F3,    0xF5B5,    0xF578,    0xF53B,    0xF4FD,    0xF4C0,    0xF483,
        0xF446,    0xF409,    0xF3CC,    0xF38F,    0xF352,    0xF315,    0xF2D9,    0xF29C,    0xF25F,    0xF223,    0xF1E6,    0xF1AA,    0xF16D,    0xF131,    0xF0F5,    0xF0B9,
        0xF07C,    0xF004,    0xEF8C,    0xEF15,    0xEE9D,    0xEE26,    0xEDAF,    0xED38,    0xECC2,    0xEC4C,    0xEBD6,    0xEB60,    0xEAEA,    0xEA75,    0xEA00,    0xE98B,
        0xE916,    0xE8A2,    0xE82E,    0xE7BA,    0xE746,    0xE6D2,    0xE65F,    0xE5EC,    0xE579,    0xE506,    0xE494,    0xE422,    0xE3B0,    0xE33E,    0xE2CD,    0xE25B,
        0xE1EA,    0xE109,    0xE028,    0xDF49,    0xDE6A,    0xDD8C,    0xDCAF,    0xDBD2,    0xDAF7,    0xDA1C,    0xD943,    0xD86A,    0xD792,    0xD6BB,    0xD5E4,    0xD50F,
        0xD43A,    0xD366,    0xD293,    0xD1C1,    0xD0F0,    0xD01F,    0xCF50,    0xCE81,    0xCDB3,    0xCCE5,    0xCC19,    0xCB4D,    0xCA82,    0xC9B8,    0xC8EF,    0xC826,
        0xC75F,    0xC5D1,    0xC447,    0xC2C0,    0xC13C,    0xBFBB,    0xBE3D,    0xBCC2,    0xBB4A,    0xB9D5,    0xB863,    0xB6F4,    0xB587,    0xB41D,    0xB2B7,    0xB153,
        0xAFF1,    0xAE93,    0xAD37,    0xABDE,    0xAA87,    0xA934,    0xA7E3,    0xA694,    0xA548,    0xA3FF,    0xA2B8,    0xA174,    0xA032,    0x9EF3,    0x9DB7,    0x9C7C,
        0x9B45,    0x98DC,    0x967E,    0x9428,    0x91DC,    0x8F99,    0x8D60,    0x8B2E,    0x8906,    0x86E6,    0x84CF,    0x82C0,    0x80B9,    0x7EBA,    0x7CC3,    0x7AD4,
        0x78EC,    0x770C,    0x7534,    0x7363,    0x7199,    0x6FD6,    0x6E1A,    0x6C65,    0x6AB7,    0x690F,    0x676E,    0x65D4,    0x6440,    0x62B2,    0x612A,    0x5FA8,
        0x5E2D,    0x5B47,    0x5878,    0x55BF,    0x531C,    0x508D,    0x4E13,    0x4BAC,    0x4958,    0x4716,    0x44E6,    0x42C7,    0x40B9,    0x3EBC,    0x3CCE,    0x3AEF,
        0x391F,    0x375D,    0x35A9,    0x3402,    0x3268,    0x30DB,    0x2F5A,    0x2DE5,    0x2C7C,    0x2B1E,    0x29CA,    0x2881,    0x2742,    0x260D,    0x24E1,    0x23BE,
        0x22A5,    0x208C,    0x1E93,    0x1CB9,    0x1AFB,    0x1959,    0x17CF,    0x165E,    0x1503,    0x13BD,    0x128B,    0x116C,    0x105D,     0xF60,     0xE71,     0xD91,
        0xCBF,     0xBF9,     0xB3F,     0xA91,     0x9ED,     0x953,     0x8C2,     0x83B,     0x7BB,     0x743,     0x6D2,     0x669,     0x605,     0x5A8,     0x550,     0x4FE,
        0x4B0,     0x423,     0x3A7,     0x339,     0x2D8,     0x282,     0x237,     0x1F4,     0x1BA,     0x186,     0x158,     0x12F,     0x10C,      0xEC,      0xD1,      0xB8,
        0xA2,      0x8F,      0x7F,      0x70,      0x63,      0x57,      0x4D,      0x44,      0x3C,      0x35,      0x2F,      0x29,      0x24,      0x20,      0x1C,      0x19,
        0x16,      0x11,       0xD,       0xA,       0x8,       0x6,       0x5,       0x4,       0x3,       0x2,       0x2,       0x1,       0x1,       0x1,       0x1,       0x1,
        0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,       0x0,
        0x0
    }; // continued data is all zero

    if (data_base == 0)
        return 0;
    if (data_exp == 0)
        return 0xFFFF;

    // log(base) : 0.16 -> 4.16
    data_tmp = data_base;
    for (shift = 0; data_tmp > 63; shift++)
        data_tmp >>= 1;
    idx = (data_base >> shift) + 32*shift;
    lsb = data_base & ((0x1 << shift) - 1);
    if (idx == 383) // to fix!
        lsb = lsb + 1;
    log_res = log_lut[idx] - ((log_lut[idx] - log_lut[idx+1]) * lsb >> shift);

    // x*log(base) : 4.16, x : 4.12
    xlog_res = data_exp * log_res >> 12;

    // exp^(x*log(base)) : 4.16 -> 0.16
    data_tmp = xlog_res;
    for (shift = 0; data_tmp > (63 << 4); shift++)
        data_tmp >>= 1;
    idx = (xlog_res >> (shift+4)) + 32*shift;
    lsb = xlog_res & ((0x1 << (shift+4)) - 1);
    final_res = exp_lut[idx] - ((exp_lut[idx] - exp_lut[idx+1]) * lsb >> (shift+4));

    return final_res;
}

MS_BOOL MHal_TMO_TMOSet(MS_U16 *u16TMOData , MS_U16 u16TMOSize)
{
    MS_U16 i = 0;

    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMinRatio = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMedRatio = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMaxRatio = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMin = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMed = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMax = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16FrontSlopeMin = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16FrontSlopeMax = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16BackSlopeMin = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16BackSlopeMax = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SceneChangeThrd = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SceneChangeRatioMax = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8IIRRatio = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_TargetMode = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SDRPanelGain = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Smin = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Smed = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Smax = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Tmin = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Tmed = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Tmax = u16TMOData[i++];
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm = u16TMOData[i++]==1?2:0;
    g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_UseIniControls = u16TMOData[i++];

    if (_stCfdTmo.u16Length == 0)
    {
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK2E_25_L, 0x0001, 0x0001);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK4E_21_L, g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Smin, 0xFFFF);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK4E_22_L, g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Smed, 0xFFFF);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK4E_23_L, g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Smax, 0xFFFF);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK4E_25_L, g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Tmin, 0xFFFF);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK4E_26_L, g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Tmed, 0xFFFF);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK4E_27_L, g_HDRinitParameters.DLC_HDRNewToneMappingData.u16Tmax, 0xFFFF);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK4E_24_L, g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SDRPanelGain, 0xFFFF);

        printk("[%s %d]use 12P control 3P\n", __FUNCTION__,__LINE__);
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16SrcMinRatio %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMinRatio ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16SrcMedRatio %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMedRatio ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16SrcMaxRatio %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SrcMaxRatio ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16TgtMin %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMin ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16TgtMed %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMed ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16TgtMax %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16TgtMax ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16FrontSlopeMin %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16FrontSlopeMin ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16FrontSlopeMax %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16FrontSlopeMax ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16BackSlopeMin %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16BackSlopeMin ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16BackSlopeMax %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16BackSlopeMax ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16SceneChangeThrd %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SceneChangeThrd ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u16SceneChangeRatioMax %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u16SceneChangeRatioMax ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u8IIRRatio %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u8IIRRatio ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u8TMO_TargetMode %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_TargetMode ));
        HDR_DBG_HAL(DLC_DEBUG("\n  Kernel DLC_HDRNewToneMappingData.u8TMO_Algorithm %x \n", g_HDRinitParameters.DLC_HDRNewToneMappingData.u8TMO_Algorithm ));
    }

    return TRUE ;
}

void MHal_TMO_HDR_ToneMappingGen_ByTable_V0(HDR_TMO_GEN_TABLE *pst_TMO_gen_table)
{

    MS_U32 *u32Input_InNits, *u32Input_nit2Gamma, *u32Output_nit2Gamma;
    MS_U16 u16count;
    MS_U16 u16count2;
    MS_S16 s16count;
    MS_U16 *u16Array;
    MS_U32 *u32Output_InNits;

    MS_U32 u32temp;

    MS_U16 dlc_in;
    //TMO curves
    MS_U16 *u16curve512;

    ////Gamma 2.2 parameter start////
    MS_U32 u32Gamma =0x745D,u32GammaInv = 0x23333;
    MS_U16 u16Alpha = 0x8000,u16Beta = 1,u16LowerBoundGain = 0x0100;
    MS_U8  u8BitDepth=16,sMode = 0;
    ////Gamma 2.2 parameter End///


    //temp

    MS_U32 u32input_lb;
    MS_U32 u32input_hb;
    MS_U32 u32output_lb;
    MS_U32 u32output_hb;
    MS_U32 u32step;
    MS_U64 u64temp;

    if(0 == pst_TMO_gen_table->u16Control_points)
    {
        return;
    }

#if RealChip
    //kmalloc
    u32Input_InNits     = (MS_U32*) kmalloc((pst_TMO_gen_table->u16Control_points)*sizeof(MS_U32),GFP_KERNEL);
    u32Input_nit2Gamma  = (MS_U32*) kmalloc((pst_TMO_gen_table->u16Control_points)*sizeof(MS_U32),GFP_KERNEL);
    u32Output_nit2Gamma = (MS_U32*) kmalloc((pst_TMO_gen_table->u16Control_points)*sizeof(MS_U32),GFP_KERNEL);
    u16curve512 = (MS_U16*)kmalloc(512*sizeof(MS_U16),GFP_KERNEL);

#else
    //malloc
    u32Input_InNits     = (MS_U32*) malloc((pst_TMO_gen_table->u16Control_points)*sizeof(MS_U32));
    u32Input_nit2Gamma  = (MS_U32*) malloc((pst_TMO_gen_table->u16Control_points)*sizeof(MS_U32));
    u32Output_nit2Gamma = (MS_U32*) malloc((pst_TMO_gen_table->u16Control_points)*sizeof(MS_U32));

    u16curve512         = (MS_U16*) malloc(512*sizeof(MS_U16));

#endif

    //transfer PQ codes to nits by table
    u16count = pst_TMO_gen_table->u16Control_points;
    u16Array = pst_TMO_gen_table->pt_input_PQcode;

    for (u16count = 0; u16count<pst_TMO_gen_table->u16Control_points; u16count++)
    {
        u32Input_InNits[u16count] = TMOAPI_PQ2Nits_1024x32bits[u16Array[u16count]];

#if (1 == this_debug)

        printk("u32Input_InNits[%d]=%d\n",u16count,u32Input_InNits[u16count]);

#endif

    }

    u32Output_InNits = pst_TMO_gen_table->pt_output_nits_array;


#if (1 == this_debug)
    for (u16count = 0; u16count<pst_TMO_gen_table->u16Control_points; u16count++)
    {
        printk("u32Output_InNits[%d]=%d\n",u16count,u32Output_InNits[u16count]);
    }
#endif

    //now, we got input_point_innits and output_point_innits
    //transfer nits to gamma , y=x^2.2

    for (u16count = 0; u16count<pst_TMO_gen_table->u16Control_points; u16count++)
    {
        u32temp = (((MS_U64)u32Input_InNits[u16count]) *0x10000ul+u32Input_InNits[pst_TMO_gen_table->u16Control_points-1]/2)/u32Input_InNits[pst_TMO_gen_table->u16Control_points-1];

        u32temp = min(u32temp, 0x10000ul);

#if (1 == this_debug)

        printk("temp[%d]=%d\n",u16count,u32temp);

#endif


        //for this function
        //input 1x = 0x10000
        //output 1x = 0xffff
        if (u16Array[u16count] <= last_less1nit_entry)
        {
            u32Input_nit2Gamma[u16count] = (MS_U32)Mhal_CFD_GammaSampling(u32temp, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode)*0x03E4/0xFFFF;
        }
        else
        {
            u32Input_nit2Gamma[u16count] = (MS_U32)Mhal_CFD_GammaSampling(u32temp, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
        }


        u32temp = (((MS_U64)u32Output_InNits[u16count]) *0x10000ul+u32Output_InNits[pst_TMO_gen_table->u16Control_points-1]/2)/u32Output_InNits[pst_TMO_gen_table->u16Control_points-1];

        u32temp = min(u32temp, 0x10000ul);

        s16count = ((MS_S16) u16count);

        //for this function
        //input 1x = 0x10000
        //output 1x = 0xffff
        if (s16count <= pst_TMO_gen_table->s16_last_less1_index_outputnits)
        {
#if 1
            if (s16count <= pst_TMO_gen_table->s16_last_index_m10000_outputnits)
            {
                //10000^(1/2.2)
                u32Output_nit2Gamma[u16count] = (MS_U32)Mhal_CFD_GammaSampling(u32temp, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode)*0x03E4/0xFFFF;
            }
            else if ((s16count > pst_TMO_gen_table->s16_last_index_m10000_outputnits) && (s16count <= pst_TMO_gen_table->s16_last_index_m100_outputnits))
            {
                //100^(1/2.2)
                u32Output_nit2Gamma[u16count] = (MS_U32)Mhal_CFD_GammaSampling(u32temp, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode)*0x1F8F/0xFFFF;
            }
            else
            {
                u32Output_nit2Gamma[u16count] = (MS_U32)Mhal_CFD_GammaSampling(u32temp, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
            }
#else
            u32Output_nit2Gamma[u16count] = (MS_U32)Mhal_CFD_GammaSampling(u32temp, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode)*0x03E4/0xFFFF;
#endif
        }
        else
        {
            u32Output_nit2Gamma[u16count] = (MS_U32)Mhal_CFD_GammaSampling(u32temp, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain , u8BitDepth,sMode);
        }

#if (1 == this_debug)

        printk("temp2[%d]=%d\n",u16count,u32temp);
        printk("u32Output_nit2Gamma[%d]=%d\n",u16count,u32Output_nit2Gamma[u16count]);
#endif

#if (1 == this_debug)

        printk("u32Input_nit2Gamma[%d]=%d\n",u16count,u32Input_nit2Gamma[u16count]);

#endif


    }

    //for debug point

    //interpolation for TMO[512]
    //starts from u32Input_nit2Gamma[] and u32Output_nit2Gamma[]

    MS_U16 u16start_idx = 0;

    //0: match the 1st point
    //1: match the final point
    //2: match the middle interval
    MS_U8 u8MatchStatus = 0;

    for (u16count = 0; u16count < 512; u16count++)
    {

        //for debug
        if (u16count == 500)
            u16count = u16count;


        dlc_in = u16count * (0xFFFF) / 511;

        for (u16count2 = u16start_idx; u16count2 < (pst_TMO_gen_table->u16Control_points); u16count2++)
        {
            //case1
            if (dlc_in == u32Input_nit2Gamma[0])
            {
                u8MatchStatus = 0;
                break;
            }
            else if (dlc_in == u32Input_nit2Gamma[(pst_TMO_gen_table->u16Control_points-1)])
            {
                u8MatchStatus = 1;
                break;
            }
            else if ((dlc_in >= u32Input_nit2Gamma[u16count2]) && (dlc_in <= u32Input_nit2Gamma[u16count2+1]))
            {
                u8MatchStatus = 2;
                u16start_idx = u16count2;
                break;
            }
        }

        //match
        if (0 == u8MatchStatus)
        {
            u16curve512[u16count] = u32Output_nit2Gamma[u16count2];
        }
        else if (1 == u8MatchStatus)
        {
            u16curve512[u16count] = u32Output_nit2Gamma[(pst_TMO_gen_table->u16Control_points-1)];
        }
        else if (2 == u8MatchStatus) //need interpolation
        {
            u32input_lb = u32Input_nit2Gamma[u16count2];
            u32input_hb = u32Input_nit2Gamma[u16count2+1];
            u32output_lb = u32Output_nit2Gamma[u16count2];
            u32output_hb = u32Output_nit2Gamma[u16count2+1];

            u32step = u32input_hb - u32input_lb;

            u64temp = ((MS_U64)u32output_hb)*(dlc_in-u32input_lb) + ((MS_U64)u32output_lb)*(u32input_hb-dlc_in);
            u64temp = (u64temp+(u32step>>1))/u32step;

            u16curve512[u16count] = u64temp;
        }

        u16curve512[u16count] = u16curve512[u16count]>>4;

    }

    MHal_CFD_SetHdrTmoLut(u16curve512,0,10000,10000);

    if(u32Input_InNits != NULL)
        kfree(u32Input_InNits);
    if(u32Input_nit2Gamma != NULL)
        kfree(u32Input_nit2Gamma);
    if(u32Output_nit2Gamma != NULL)
        kfree(u32Output_nit2Gamma);
    if(u16curve512 != NULL)
        kfree(u16curve512);
}

void MHal_TMO_APITMO(void)
{
    HDR_TMO_GEN_TABLE st_TMO_gen_table;

    st_TMO_gen_table.u16Control_points = g_HDRinitParameters.HDR_tmoapi.u16ControlPoints;
    st_TMO_gen_table.s16_last_less1_index_outputnits = g_HDRinitParameters.HDR_tmoapi.s16LastLess1IndexOutputnits;
    st_TMO_gen_table.pt_input_PQcode = g_HDRinitParameters.HDR_tmoapi.pt_input_points_array;
    st_TMO_gen_table.pt_output_nits_array = g_HDRinitParameters.HDR_tmoapi.pt_output_nits_array;
    st_TMO_gen_table.s16_last_index_m10000_outputnits = g_HDRinitParameters.HDR_tmoapi.s16LastIndexM10000Outputnits;
    st_TMO_gen_table.s16_last_index_m100_outputnits = g_HDRinitParameters.HDR_tmoapi.s16LastIndexM100Outputnits;

    MHal_TMO_HDR_ToneMappingGen_ByTable_V0(&st_TMO_gen_table);
    if(u8PrintfOneTime == TRUE)
    {
        printk("\033[1;31m ###***###[%s][%d]\033[0m\n",__func__,__LINE__);
        u8PrintfOneTime = FALSE;
    }
}

MS_U16 MHal_TMO_Exp(MS_U16 data_base, MS_U16 data_exp,MS_U8 u8Mode)
{
    if(0 == u8Mode)
   {
        return MHal_TMO_Pow(data_base << 6, data_exp);
   }
    else if(2 == u8Mode)
   {
        return MHal_TMO_Pow(data_base , data_exp);
   }
    else
    {
        return MHal_TMO_Pow(data_base << 4, data_exp);
    }
}

MS_U32 MHal_TMO_Exp2(MS_U16 data_base, MS_U32 data_exp, MS_U8 u8Mode)
{

    if (0 == u8Mode)
    {
        return (pow01_16In32Out(data_base << 6, data_exp));
    }
    else if (1 == u8Mode)
    {
        return (pow01_16In32Out(data_base << 4, data_exp));
    }
    else
    {
        return (pow01_16In32Out(data_base, data_exp));
    }

}

void MHal_TMO_HDR_ToneMappingCurveGen_Mod(MS_U16* pCurve18, HDR_ToneMappingData* pData, MS_BOOL bWindow)
{
    MS_U16 smin, smed, smax;
    MS_U16 tmin, tmed, tmax;
    MS_S64 s1, s2, s3;
    MS_S64 t1, t2, t3;
    MS_S64 norm;
    MS_S64 c1, c2, c3;
    MS_S64 term, nume, deno, base;
    MS_U16 dlc_in, dlc_out, i;
    const MS_U16 dlc_pivots[18] = { 0, 0x20, 0x60, 0xA0, 0xE0, 0x120, 0x160, 0x1A0, 0x1E0, 0x220, 0x260, 0x2A0, 0x2E0, 0x320, 0x360, 0x3A0, 0x3E0, 0x3FF };
    MS_U32 u32nits2Gamma[6] = { 0 };
    MS_U32 u32Temp3, data_out, u32nit[6] = { 0 };
    MS_U16 u16SourceUpper, u16SourceLower, u16TargetUpper, u16TargeLower;
    MS_U16 *u16curve512 = (MS_U16*)kmalloc(512 * sizeof(MS_U16), GFP_KERNEL);

    // modify smed/tmed parameter
    MS_U8 u8UpperBound = 80, u8lowerBound = 30;

    ////Gamma 2.2 parameter start////
    MS_U32 u32Gamma = 0x745D, u32GammaInv = 0x23333;
    MS_U16 u16Alpha = 0x8000, u16Beta = 1, u16LowerBoundGain = 0x0100;
    MS_U8 u8BitDepth = 14, sMode = 0;
    ////Gamma 2.2 parameter End///

    u32nit[0] = MApi_GFLIP_XC_R2BYTE(REG_SC_BK4E_21_L) ;//smin
    u32nit[1] = MApi_GFLIP_XC_R2BYTE(REG_SC_BK4E_22_L) ;//smed
    u32nit[2] = MApi_GFLIP_XC_R2BYTE(REG_SC_BK4E_23_L) ;//smax
    u32nit[3] = MApi_GFLIP_XC_R2BYTE(REG_SC_BK4E_25_L) ;//tmin
    u32nit[4] = MApi_GFLIP_XC_R2BYTE(REG_SC_BK4E_26_L) ;//tmed
    u32nit[5] = MApi_GFLIP_XC_R2BYTE(REG_SC_BK4E_27_L) ;//tmax

    // anti-dumbness
    // if the registers are all 0, set linear TMO
    if( (u32nit[0] || u32nit[1] || u32nit[2] || u32nit[3] || u32nit[4]  || u32nit[5]) == 0)
    {
        u32nit[0] = 0x80   ;//smin
        u32nit[1] = 0x400  ;//smed
        u32nit[2] = 0x2710 ;//smax
        u32nit[3] = 0x80   ;//tmin
        u32nit[4] = 0x400  ;//tmed
        u32nit[5] = 0x2710 ;//tmax

        MApi_GFLIP_XC_W2BYTE( REG_SC_BK4E_21_L, u32nit[0]);
        MApi_GFLIP_XC_W2BYTE( REG_SC_BK4E_22_L, u32nit[1]);
        MApi_GFLIP_XC_W2BYTE( REG_SC_BK4E_23_L, u32nit[2]);
        MApi_GFLIP_XC_W2BYTE( REG_SC_BK4E_25_L, u32nit[3]);
        MApi_GFLIP_XC_W2BYTE( REG_SC_BK4E_26_L, u32nit[4]);
        MApi_GFLIP_XC_W2BYTE( REG_SC_BK4E_27_L, u32nit[5]);
    }

    // smin/med/max tmin/med/max in nits (0-10000)
    // make sure smed is between u16SourceUpper and u16SourceLower
    // make sure tmed is between u16TargetUpper and u16TargeLower
    smin = u32nit[0] / 10000;
    smed = u32nit[1];
    smax = u32nit[2];
    tmin = u32nit[3] / 10000;
    tmed = u32nit[4];
    tmax = u32nit[5];



    u16SourceUpper = ((smax - smin)*u8UpperBound) / 100 + smin;
    u16SourceLower = ((smax - smin)*u8lowerBound) / 100 + smin;
    u16TargetUpper = ((tmax - tmin)*u8UpperBound) / 100 + tmin;
    u16TargeLower  = ((tmax - tmin)*u8lowerBound) / 100 + tmin;


    // modify smed by (smax-smin) ratio
    if (smed > u16SourceUpper)
    {
        u32nit[1] = u16SourceUpper;
    }
    else if (smed< u16SourceLower)
    {
        u32nit[1] = u16SourceLower;
    }

    // modify tmed by (tmax-tmin) ratio
    if (tmed > u16TargetUpper)
    {
        u32nit[4] = u16TargetUpper;
    }
    else if (tmed < u16TargeLower)
    {
        u32nit[4] = u16TargeLower;
    }

    // nits to gamma code
    for (i = 0; i<3; i++)
    {
        if (i == 0)
        {
            //min units = 0.00001nits
            u32Temp3 = (MS_U32)(min((u32nit[i] * 0x10000ul / u32nit[2]), 0x10000ul));
            u32nits2Gamma[i] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp3, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain, u8BitDepth, sMode) * 0x03E4 / 0xFFFF;

            u32Temp3 = (MS_U32)(min((u32nit[i + 3] * 0x10000ul / u32nit[5]), 0x10000ul));
            u32nits2Gamma[i + 3] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp3, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain, u8BitDepth, sMode) * 0x03E4 / 0xFFFF;
        }
        else
        {
            u32Temp3 = (MS_U32)(min((u32nit[i] * 0x10000ul / u32nit[2]), 0x10000ul));
            u32nits2Gamma[i] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp3, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain, u8BitDepth, sMode);// * 0xFFFF / 0xFFFC;

            u32Temp3 = (MS_U32)(min((u32nit[i + 3] * 0x10000ul / u32nit[5]), 0x10000ul));
            u32nits2Gamma[i + 3] = (MS_U32)Mhal_CFD_GammaSampling(u32Temp3, u32Gamma, u16Alpha, u16Beta, u16LowerBoundGain, u8BitDepth, sMode);// * 0xFFFF / 0xFFFC;
        }

    }



    s1 = MHal_TMO_Exp2(u32nits2Gamma[0], (pData->slope) << 4, 2);
    s2 = MHal_TMO_Exp2(u32nits2Gamma[1], (pData->slope) << 4, 2);
    s3 = MHal_TMO_Exp2(u32nits2Gamma[2], (pData->slope) << 4, 2);
    t1 = MHal_TMO_Exp2(u32nits2Gamma[3], (0x1000000 / pData->rolloff) << 4, 2);
    t2 = MHal_TMO_Exp2(u32nits2Gamma[4], (0x1000000 / pData->rolloff) << 4, 2);
    t3 = MHal_TMO_Exp2(u32nits2Gamma[5], (0x1000000 / pData->rolloff) << 4, 2);
    s1 = s1 >> 16;  // 32bit >>16 -> 16bit
    s2 = s2 >> 16;
    s3 = s3 >> 16;
    t1 = t1 >> 16;
    t2 = t2 >> 16;
    t3 = t3 >> 16;




    norm = (MS_S64)s3*t3*(s1 - s2) + (MS_S64)s2*t2*(s3 - s1) + (MS_S64)s1*t1*(s2 - s3);         // 48bit
    c1 = (MS_S64)s2*s3*(t2 - t3)*t1 + (MS_S64)s1*s3*(t3 - t1)*t2 + (MS_S64)s1*s2*(t1 - t2)*t3;  // 64bit
    c2 = (MS_S64)t1*(s3*t3 - s2*t2) + (MS_S64)t2*(s1*t1 - s3*t3) + (MS_S64)t3*(s2*t2 - s1*t1);  // 48bit
    c3 = (MS_S64)t1*(s3 - s2) + (MS_S64)t2*(s1 - s3) + (MS_S64)t3*(s2 - s1);                    // 32bit

    norm = norm > 0 ? (norm >> 16) : -(abs(norm) >> 16);  // 48bit >>16 -> 32bit
    c1 = c1 > 0 ? (c1 >> 16) : -(abs(c1) >> 16);          // 64bit >>16 -> 48bit
    c3 = c3 > 0 ? (c3 << 16) : -(abs(c3) << 16);          // 32bit <<16 -> 48bit

    if (0 != norm)
    {
        c1 = c1 / norm; // 16bit
        c2 = c2 / norm; // 16bit
        c3 = c3 / norm; // 16bit
    }


    if (g_DlcParameters.u8Dlc_Mode == 2) //DLC 18point fixed curve
    {
        //printk("===========[%s][%d][DLC TMO]===========\n",__FUNCTION__,__LINE__);
        for (i = 0; i < 18; i++)
        {
            dlc_in = dlc_pivots[i];

            term = MHal_TMO_Exp(dlc_in, pData->slope, 0);
            nume = c1 + (c2*term >> 16); // s4.12 + s4.12*0.16>>16 = s4.12
            deno = 0x1000 + (c3*term >> 16); // s4.12 + s4.12*0.16>>16 = s4.12
            base = (nume << 10) / deno; // (s4.12<<10) / s4.12 = s0.10
            base = (base < 0) ? 0 : (base > 1023) ? 1023 : base;
            dlc_out = MHal_TMO_Exp((MS_U16)base, pData->rolloff, 0);

            pCurve18[i] = dlc_out >> 6;
        }

        pCurve18[0] = (2 * pCurve18[0] > pCurve18[1]) ? 2 * pCurve18[0] - pCurve18[1] : (pCurve18[1] - 2 * pCurve18[0]) | 0x400;
        pCurve18[17] = 2 * pCurve18[17] - pCurve18[16];

        _u16Count++;
        if (_u16Count >= 300)
        {
            HDR_DBG_HAL(DLC_DEBUG("\n  Kernel [0]=%x ,[1]=%x,[2]=%x,[3]=%x,[4]=%x,[5]=%x,[6]=%x,[7]=%x,[8]=%x   \n", pCurve18[0], pCurve18[1], pCurve18[2], pCurve18[3], pCurve18[4], pCurve18[5], pCurve18[6], pCurve18[7], pCurve18[8]));
            HDR_DBG_HAL(DLC_DEBUG("\n  Kernel [9]=%x ,[10]=%x,[11]=%x,[12]=%x,[13]=%x,[14]=%x,[15]=%x,[16]=%x,[17]=%x,[18]=%x   \n", pCurve18[9], pCurve18[10], pCurve18[11], pCurve18[12], pCurve18[13], pCurve18[14], pCurve18[15], pCurve18[16], pCurve18[17], pCurve18[18]));
            _u16Count = 0;
        }

        for (i = 0; i < 18; i++)
        {
            if (i>0 && i<17)
            {
                msWriteByte((REG_ADDR_DLC_DATA_START_MAIN + (i - 1)), pCurve18[i] >> 2);
                msDlcWriteCurveLSB(0, i, pCurve18[i] & 0x03);
            }
            if (MAIN_WINDOW == bWindow)
            {
                // set DLC curve index N0 & 16
                if (i == 0)
                {
                    msWriteByte(REG_ADDR_DLC_DATA_EXTEND_N0_MAIN, pCurve18[0] >> 2);
                    msWriteByte(REG_ADDR_DLC_DATA_EXTEND_N0_MAIN + 1, 0x01);
                    msDlcWriteCurveLSB(MAIN_WINDOW, 0xFF, pCurve18[0] & 0x03);
                }

                if (i == 17)
                {
                    msWriteByte(REG_ADDR_DLC_DATA_EXTEND_16_MAIN, pCurve18[17] >> 2);
                    msWriteByte(REG_ADDR_DLC_DATA_EXTEND_16_MAIN + 1, 0x00);
                    msDlcWriteCurveLSB(MAIN_WINDOW, 16, pCurve18[17] & 0x03);
                }
            }
        }

    }
    else
    {
        //printk("c1:%d,c2:%d,c3:%d norm:%d\n",c1,c2,c3,norm);
        MS_U16 u16target_max_lum_nits = tmax;
        for (i = 0; i < 512; i++)
        {
            dlc_in = i * (0xFFFF) / 511;
            term = MHal_TMO_Exp2(dlc_in, (pData->slope) << 4, 2);
            nume = c1 + (c2*term >> 32); // s4.16 + s4.16*0.32>>32 = s4.16
            deno = 0x10000 + (c3*term >> 32); // s4.16 + s4.16*0.32>>32 = s4.16
            base = (nume << 16) / deno; // (s4.16<<16) / s4.16 = s0.16
            base = (base < 0) ? 0 : (base > 0xFFFF) ? 0xFFFF : base; // limit to 16bit
            data_out = MHal_TMO_Exp2((MS_U16)base, (pData->rolloff) << 4, 2);
            data_out = min(data_out, 0xFFFFFFFF - 0x80000);  // limit to 32bit before rounding
            u16curve512[i] = (data_out + 0x80000) >> 20; //12bit
        }

        MHal_CFD_SetHdrTmoLut(u16curve512, 0, u32nit[2], u32nit[5]);
    }

    if (u16curve512)
        kfree((void *)u16curve512);
}

void MHal_TMO_NewTMO(MS_BOOL bWindow)
{
    if(u8PrintfOneTime == TRUE)
    {
        printk("\033[1;31m ###***###[%s][%d]\033[0m\n",__func__,__LINE__);
        u8PrintfOneTime = FALSE;
    }
}

void MHal_TMO_ReferTMO(void)
{
    MS_U16 u16tmo_size = 512;
    MS_U16 u16source_max_luma = 10000;
    MS_U16 u16panel_max_luma = 1000;
    MS_U16 u16linear_luma = 800;
    MS_U16 u16idx = 0;
    MS_U16 u16input = 0,u16input_code = 0,u16linear_in_code = 0;
    MS_U16 u16linear_out_code = 0;
    MS_U16 u16in_thrd = 0,u16out_thrd = 0,u16upper_bound;
    MS_U16 u16output = 0;
    MS_U32 u32norm_in = 0,u32gamma_out = 0;
    MS_U32 u32In = 0;
    MS_U32 u32Gamma = 0x7FFF;
    MS_U16 u16A = 0x8000;
    MS_U16 u16B = 0;
    MS_U16 u16S = 0x0100;
    MS_U8 u8BitDepth = 14;
    MS_U8 sMode = 0;
    u16panel_max_luma = MApi_GFLIP_XC_R2BYTE(REG_SC_BK4E_27_L);
    u16panel_max_luma = max(u16panel_max_luma,u16linear_luma);
    u16linear_in_code = (MS_U16)MHal_TMO_Luminance_To_GammaCode(u16linear_luma, u16source_max_luma);
    u16linear_out_code = (MS_U16)MHal_TMO_Luminance_To_GammaCode(u16linear_luma, u16panel_max_luma);
    u16in_thrd = min((u16linear_in_code*0xFF8)/0xFFFC,0xFF8);
    u16out_thrd =min((u16linear_out_code*0xFF8)/0xFFFC,0xFF8);
    for( u16idx= 0;u16idx <u16tmo_size;u16idx++)
    {
        u16input = (u16idx*8);// 4088(0xFF8) as 1;
        u16input_code = u16input;
        u16code_in[u16idx] = u16input_code;


        u16upper_bound = 0xFF8 - u16out_thrd;

        if (u16input_code <= u16in_thrd )
        {
            u16output = (u16input_code*u16out_thrd)/max(u16in_thrd,1); //output 0xFF8 as 1
        }
        else
        {
            u32norm_in = ((u16input_code- u16in_thrd)*0x10000ul)/max((0xFF8-u16in_thrd),1);
            u32In = min(u32norm_in,0x10000ul);
            //Gammasampling function input 0x10000 as 1 ,output 0xFFFC as 1
            u32gamma_out =Mhal_CFD_GammaSampling( u32In,  u32Gamma,  u16A,  u16B,  u16S,  u8BitDepth,  sMode);//Gamma^(1/2)
            u16output = (u32gamma_out*u16upper_bound)/0xFFFC + u16out_thrd;
        }


        u16tmo_lut[u16idx] = min(((u16output*4095)/4088),4095);

    }
    MHal_CFD_SetHdrTmoLut(u16tmo_lut, 0,u16source_max_luma,u16panel_max_luma);
    if(u8PrintfOneTime == TRUE)
    {
        printk("\033[1;31m ###***###[%s][%d]\033[0m\n",__func__,__LINE__);
        u8PrintfOneTime = FALSE;
    }
}

MS_U32 MHal_TMO_Luminance_To_GammaCode(MS_U16 u16Nits_in, MS_U16 u16maxLuma)
{
    MS_U32 u32Gamma_code;
    MS_U32 u32In = 0;
    MS_U32 u32Gamma = 0x745D;
    MS_U16 u16A = 0x8000;
    MS_U16 u16B = 0;
    MS_U16 u16S = 0x0100;
    MS_U8 u8BitDepth = 14;
    MS_U8 sMode = 0;
    /* Gamma 2.2
    sMode = 0;
    //stDeGammaData->sData[0] = 0x8CCD>>2; // 2.2
    //stDeGammaData->sData[1] = 0;
    u32Gamma = 0x745D;
    u32GammaInv = 0x23333;
    u16Beta = 0;
    u16Alpha = 0x8000;
    u16LowerBoundGain  = 0x0100;;
    */
    u32In = min((u16Nits_in*0x10000ul/max(u16maxLuma,1)),0x10000ul);
    //Gammasampling function input 0x10000 as 1 ,output 0xFFFC as 1
    u32Gamma_code = Mhal_CFD_GammaSampling( u32In,  u32Gamma,  u16A,  u16B,  u16S,  u8BitDepth,  sMode);
    return u32Gamma_code;
}

void MHal_TMO_RefernceTMOSetting(MS_U8 u8Mode)
{
    static MS_U8 u8RestoreFlag = 0;
    static MS_U16 u16FSC_Color_Engine = 0,u16TMO_user_alpha = 0;
    if(2 != u8Mode &&0 ==u8RestoreFlag)
    {
        u16FSC_Color_Engine = MApi_GFLIP_XC_R2BYTEMSK(REG_SC_BK18_06_L,0x0400);
        u16TMO_user_alpha = MApi_GFLIP_XC_R2BYTEMSK(REG_SC_BK7A_3A_L,0x001F);
    }

    if(2 == u8Mode && 0 ==u8RestoreFlag)//Refernce TMO setting
    {
        //printk("ReferTMO\n");
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK18_06_L, 0x0400, 0x0400);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK7A_3A_L, 0x0000, 0x001F);
        u8RestoreFlag = 1;
    }
    else if(2 != u8Mode && 1 == u8RestoreFlag ) //Restore to original setting
    {
        //printk("DefaultTMO\n");
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK18_06_L, u16FSC_Color_Engine, 0x0400);
        MApi_GFLIP_XC_W2BYTEMSK(REG_SC_BK7A_3A_L, u16TMO_user_alpha, 0x001F);
        printk("u16FSC_Color_Engine:%x,u16TMO_user_alpha:%x\n",u16FSC_Color_Engine,u16TMO_user_alpha);
        u8RestoreFlag = 0;
    }
    else
    {
        //donothing
    }

}
#endif
//_HAL_TMO_C