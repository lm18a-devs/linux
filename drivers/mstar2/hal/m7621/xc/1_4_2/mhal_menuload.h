//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (��MStar Confidential Information��) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
#ifndef MHAL_MENULOAD_H
#define MHAL_MENULOAD_H

//Select the source to trigger menuload
#define TRIG_SRC_FE_VFDE        0
#define TRIG_SRC_RE_VSYNC       1
#define TRIG_SRC_FE_VSYNC       2
#define TRIG_SRC_DELAY_LINE     3

#define OPM_LOCK_INIT_STATE     0
#define OPM_LOCK_FE_VSYNC       1
#define OPM_LOCK_TRAIN_TRIG_P   2
#define OPM_LOCK_DS             3

#define  MS_MLOAD_CMD_ALIGN     4
#define  MS_MLOAD_REG_LEN       0x04 // how many data in one MIU request
#define  MS_MLOAD_CMD_LEN       MS_MLOAD_MEM_BASE_UNIT
#define  MS_MLOAD_MEM_BASE_UNIT BYTE_PER_WORD
#define  MS_MLOAD_BUS_WIDTH     (32)
#define  MS_MLOAD_XC_MAX_CMD_CNT   192
#define  MS_MLOAD_HDR_MAX_CMD_CNT  1024 // viable count that RD suggest.
#define  MLOAD_ASYNCHRONOUS       0
#if (MLOAD_ASYNCHRONOUS==1)
#define  MS_MLOAD_MAX_CMD_CNT(enClient) MS_MLOAD_HDR_MAX_CMD_CNT
#else
#define  MS_MLOAD_MAX_CMD_CNT(enClient) (((enClient % CLIENT_NUM) == 0) ? MS_MLOAD_XC_MAX_CMD_CNT : MS_MLOAD_HDR_MAX_CMD_CNT)
#endif
#define  MS_MLOAD_NULL_CMD      0xFF010000
#define  MS_MLOAD_DUMMY_CMD_CNT(x)  ((x+0x0003)&~0x0003)
#define  MS_MLOAD_END_CMD(enClient) (0x1F020000 | (MS_MLOAD_REG_LEN) | (_XC_Device_Offset[enClient/CLIENT_NUM] << 24))
#define  MS_MLOAD_MIU_BUS_SEL   0x00

#define  MS_MLOAD_CMD_LEN_64BITS 8 //8 bytes
#define  MS_MLOAD_NULL_CMD_SPREAD_MODE    (((MS_U64)0x13FF<<23)|((MS_U64)0x01<<16)|((MS_U64)0x0000))  //0x00000009FF810000
#define  MS_MLOAD_END_CMD_SPREAD_MODE(x)   ( ((MS_U64)0x0000<<48) | (((MS_U64)0x131F<<23)|((MS_U64)0x02<<16)|((MS_U64)0x0000)) | (MS_MLOAD_REG_LEN) ) //(0x000000098F820000 | (MS_MLOAD_REG_LEN)) = 0x000000098F820004
#define  MS_MLOAD_END_CMD_DEV1_SPREAD_MODE(x)   ( ((MS_U64)0x0000<<48) | (((MS_U64)0x139F<<23)|((MS_U64)0x02<<16)|((MS_U64)0x0000)) | (MS_MLOAD_REG_LEN) ) //(0x000000098F820000 | (MS_MLOAD_REG_LEN)) = 0x000000098F820004
#if ENABLE_NMLOAD
#define  MS_MLOAD2_END_CMD_SPREAD_MODE(x)   ( ((MS_U64)0x0000<<48) | (((MS_U64)0x131F<<23)|((MS_U64)0x22<<16)|((MS_U64)0x0000)) | (MS_MLOAD_REG_LEN) ) //(0x000000098F820000 | (MS_MLOAD_REG_LEN)) = 0x000000098FA20004
#define  MS_MLOAD2_END_CMD_DEV1_SPREAD_MODE(x)   ( ((MS_U64)0x0000<<48) | (((MS_U64)0x139F<<23)|((MS_U64)0x22<<16)|((MS_U64)0x0000)) | (MS_MLOAD_REG_LEN) ) //(0x000000098F820000 | (MS_MLOAD_REG_LEN)) = 0x00000009CFA20004
#endif

#define  MS_MLG_REG_LEN         0x40 // how many data in one MIU request
#define  MS_MLG_CMD_LEN         BYTE_PER_WORD
#define  MS_MLG_MEM_BASE_UNIT   BYTE_PER_WORD
#if ENABLE_NMLOAD
#define  MS_MLOAD2_CFD_MEM_SIZE      0x2000
#endif

#define CLIENT_NUM 2

#define XC_ADDR_L(BANK, ADDR) (0x130000 | ((MS_U16)(BANK) << 8) | (MS_U16)((ADDR)*2))
#define XC_ADDR_H(BANK, ADDR) (0x130000 | ((MS_U16)(BANK) << 8) | (MS_U16)((ADDR)*2+1))

#define SC_W2BYTE(DEVICE_ID,ADDR, VAL) \
    MHal_XC_W2BYTE((ADDR|(_XC_Device_Offset[DEVICE_ID/CLIENT_NUM]<<8)),VAL)
#define SC_W2BYTEMSK(DEVICE_ID,ADDR, VAL ,MSK) \
    MHal_XC_W2BYTEMSK((ADDR|(_XC_Device_Offset[DEVICE_ID/CLIENT_NUM]<<8)),VAL,MSK)
#define SC_R2BYTE(DEVICE_ID,ADDR) \
    MHal_XC_R2BYTE((ADDR|(_XC_Device_Offset[DEVICE_ID/CLIENT_NUM]<<8)))
#define SC_R2BYTEMSK(DEVICE_ID,ADDR,MSK) \
    MHal_XC_R2BYTEMSK((ADDR|(_XC_Device_Offset[DEVICE_ID/CLIENT_NUM]<<8)), MSK)
#define MDrv_WriteByteMask(ADDR, VAL, MSK) \
   MHal_XC_WriteByteMask(ADDR, VAL, MSK)
#define MDrv_WriteByte(ADDR, VAL) \
   MHal_XC_WriteByte(ADDR, VAL)

#define _PK_L_(bank, addr)   (((MS_U16)(bank) << 8) | (MS_U16)((addr)*2))
#define _PK_H_(bank, addr)   (((MS_U16)(bank) << 8) | (MS_U16)((addr)*2+1))
#define REG_SC_BK0F_06_L      _PK_L_(0x0F, 0x06)
#define REG_SC_BK0F_06_H      _PK_H_(0x0F, 0x06)
#define REG_SC_BK0F_07_L      _PK_L_(0x0F, 0x07)
#define REG_SC_BK0F_07_H      _PK_H_(0x0F, 0x07)
#define REG_SC_BK0F_08_L      _PK_L_(0x0F, 0x08)
#define REG_SC_BK0F_08_H      _PK_H_(0x0F, 0x08)
#define REG_SC_BK0F_09_L      _PK_L_(0x0F, 0x09)
#define REG_SC_BK0F_09_H      _PK_H_(0x0F, 0x09)
#define REG_SC_BK0F_0A_L      _PK_L_(0x0F, 0x0A)
#define REG_SC_BK0F_0A_H      _PK_H_(0x0F, 0x0A)
#ifndef BIT
#define BIT(_bit_) (1 << (_bit_))
#endif
typedef struct
{
    union
    {
        struct
        {
        	MS_U16 u16Data;
        	MS_U8 u8Addr;
        	MS_U8 u8Bank;
        };
        MS_U32 u32Cmd;
    };
}MS_MLoad_Data;

typedef struct
{
    union
    {
        struct
        {
        	MS_U16 u16Data;
        	MS_U8  u8Addr; //addr 0 ~ 0x7F
        	MS_U8  u8Bank; //subbank 0 ~ 0xFF
        	MS_U32 u32NoUse;
        };
        MS_U64 u64Cmd;
    };
}MS_MLoad_Data_64Bits_SubBank;

#ifdef MLG_1024 // Gamma_1024
typedef struct
{
    MS_U64 BData0 : 12;     //0
    MS_U64 BData1 : 12;
    MS_U64 BData2 : 12;
    MS_U64 GData0 : 12;
    MS_U64 GData1 : 12;
    MS_U64 GData2_L : 4;      //63

    MS_U64 GData2_H : 8;      //64
    MS_U64 RData0 : 12;
    MS_U64 RData1 : 12;
    MS_U64 RData2 : 12;
    MS_U64 Dummy0 :20;      //127

    MS_U64 Dummy1 : 16 ;       //128
    MS_U64 BEnable : 1 ;
    MS_U64 GEnable : 1 ;
    MS_U64 REnable : 1 ;
    MS_U64 Dummy2 :45;      //191

    MS_U64 Dummy3;      //192~255
} MS_SC_MLG_TBL;
#else // Gamma_256 or not support MLG case
typedef struct
{
    MS_U16 u16B;
    MS_U16 u16G;
    MS_U16 u16R;
    MS_U16 u16Enable;
    MS_U16 u16Dummy[4];
} MS_SC_MLG_TBL;
#endif

typedef enum
{
    MLOAD_TRIGGER_BY_OP_SYNC=0,
    MLOAD_TRIGGER_BY_IP_MAIN_SYNC=1,
    MLOAD_TRIGGER_BY_IP_SUB_SYNC=2,
    MLOAD_TRIGGER_BY_SW=3,
    MLOAD_TRIGGER_MAX,
}MLoad_Trigger_Sync;

typedef enum
{
    MLoad_WD_Timer_Reset_DMA = 0,
    MLoad_WD_Timer_Reset_MIU = 1,
    MLoad_WD_Timer_Reset_ALL = 3,
    MLoad_WD_Timer_Reset_MAX,
}MLoad_WD_Timer_Reset_Type;
// Define store value
typedef enum
{
    E_K_STORE_VALUE_AUTO_TUNE_AREA_TRIG = 0, //Generate TRAIN_TRIG_P from delayed line of Vsync(Setting the delay line for Auto tune area)
    E_K_STORE_VALUE_DISP_AREA_TRIG,  //Generate DISP_TRIG_P from delayed line of Vsync(Setting the delay line for Display area)
    E_K_STORE_VALUE_IP_AUTO_TUNE_AREA_TRIG, //  DI change to ip and add auto tune area trig for ip
    E_K_STORE_VALUE_IP_DISP_AREA_TRIG,  // DI change to ip and add display area trig for ip
    E_K_STORE_VALUE_INIT_CMD_NUM, //BK20_90 init cmd number
    E_K_STORE_VALUE_MAX
}EN_K_STORE_VALUE_TYPE;
typedef struct
{
    MS_U16 u16OldValue[E_K_STORE_VALUE_MAX];
    MS_U8 u8XCMLoadMIUSel;
    MS_PHY g_u32MLoadPhyAddr_Suspend;
    MS_U32 g_u32MLoadBufByteLen_Suspend;
}MLOAD_PRIVATE;

typedef enum
{
    E_MLOAD = 0,
    E_MLOAD2 = 1,
    E_MLOAD_MAX,
} EN_MLOAD_TYPE;

MS_U16 KHal_XC_MLoad_get_status(EN_MLOAD_CLIENT_TYPE enClient);
void KHal_XC_MLoad_set_on_off(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn);
void KHal_XC_MLoad_set_len(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16Len);
void KHal_XC_MLoad_set_depth(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16depth);
void KHal_XC_MLoad_set_miusel(EN_MLOAD_CLIENT_TYPE enClient,MS_U8 u8MIUSel);
void KHal_XC_MLoad_set_base_addr(EN_MLOAD_CLIENT_TYPE enClient,MS_PHY u32addr);
void KHal_XC_MLoad_set_trigger_timing(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16sel);
void KHal_XC_MLoad_set_opm_lock(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16sel);
void KHal_XC_MLoad_set_trigger_delay(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16delay);
void KHal_XC_MLoad_set_trig_p(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16train, MS_U16 u16disp);
MS_BOOL KHal_XC_MLoad_get_trig_p(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 *pu16Train, MS_U16 *pu16Disp);
void KHal_XC_MLoad_Set_riu(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn);
MS_BOOL KHal_XC_MLoad_GetCaps(void);     //
void KHal_XC_MLoad_set_riu_cs(MS_BOOL bEn);      //
void KHal_XC_MLoad_set_sw_dynamic_idx_en(MS_BOOL ben);       //
void KHal_XC_MLoad_set_opm_arbiter_bypass(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL ben);
void KHal_XC_MLoad_set_miu_bus_sel(EN_MLOAD_CLIENT_TYPE enClient,MS_U8 u8BitMode);
void KHal_XC_MLoad_enable_watch_dog(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn);
void KHal_XC_MLoad_set_watch_dog_time_delay(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 value);
void KHal_XC_MLoad_enable_watch_dog_reset(EN_MLOAD_CLIENT_TYPE enClient,MLoad_WD_Timer_Reset_Type enMLWDResetType);

void KHal_XC_MLoad_Enable_64BITS_COMMAND(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn);
void KHal_XC_MLoad_Enable_64BITS_SPREAD_MODE(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEn);
void KHal_XC_MLoad_Command_Format_initial(EN_MLOAD_CLIENT_TYPE enClient);
void KHal_XC_MLoad_Set_64Bits_MIU_Bus_Sel(EN_MLOAD_CLIENT_TYPE enClient);
MS_U8 KHal_XC_MLoad_Get_64Bits_MIU_Bus_Sel(EN_MLOAD_CLIENT_TYPE enClient);
MS_U64 KHal_XC_MLoad_Gen_64bits_spreadMode(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask);      //
MS_BOOL KHal_XC_MLoad_parsing_64bits_spreadMode_NonXC(MS_U64 u64Cmd, MS_U32 *u32Addr, MS_U16 *u16Data);      //
MS_U64 KHal_XC_MLoad_Gen_64bits_spreadMode_NonXC(MS_U32 u32Bank,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask);     //
MS_U64 KHal_XC_MLoad_Gen_64bits_subBankMode(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask);     //
MS_U32 KHal_XC_MLoad_Gen_32bits_subBankMode(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask);     //
MS_BOOL KHal_XC_MLoad_parsing_32bits_subBankMode(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32MloadData, MS_U32 *pu32Addr, MS_U16 *pu16Data);       //
MS_U16 KHal_XC_MLoad_Get_Depth(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16CmdCnt);        //

void KHal_XC_MLoad_set_trigger_sync(EN_MLOAD_CLIENT_TYPE enClient,EN_MLOAD_TRIG_SYNC eTriggerSync);
void KHal_XC_MLoad_set_BitMask(EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL enable);
#if ENABLE_NMLOAD
//MLoad_EX
MS_BOOL KHal_XC_NMLoad_GetCaps(EN_MLOAD_TYPE enMloadType);
MS_BOOL KHal_XC_NMLoad_set_trigger_sync(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enclient);
MS_BOOL KHal_XC_MLoad_EX_get_status(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enclient);
void KHal_XC_NMLoad_set_base_addr(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY u32Addr);
void KHal_XC_NMLoad_set_depth(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U16 u16Depth);
void KHal_XC_NMLoad_set_on_off(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_BOOL bEn);
#endif
#endif
