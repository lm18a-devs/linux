//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
/// @file device_demodulator.h
/// @brief\b demodulator API interface enumeration and struct definitaiotn.
/// @author MStar Semiconductor Inc.
///
/// device_demodulator base class for various demodulator.
///
/// Features:
/// - Provide the enum and struct definiation for demodulator.
/// - The base class for various demodulator(DVBT,DVBC....)
///////////////////////////////////////////////////////////////////////////////////////////////////



#ifndef device_demodulator_I2b28dd03m121c8cf959bmm7254_H
#define device_demodulator_I2b28dd03m121c8cf959bmm7254_H

#define STR_ENABLE 0
#define STB_ENABLE 0
#define MIX_EDINBURGH_WINDERMERE_ENABLE 0
#define ATSC_SYSTEM_ENABLE 0
#define DVBS_SYSTEM_ENABLE 1
#define DVBT_SYSTEM_ENABLE 1
#define DVBC_SYSTEM_ENABLE 1
#define MSPI_ENABLE 1
#define PRELOAD_DSP_CODE_FROM_MAIN_CHIP_I2C_ENABLE 0
#define PRELOAD_DSP_CODE_FROM_MAIN_CHIP_I2C_ONLY_LOAD_T2_ENABLE 0
#define LOAD_DSP_CODE_FROM_MAIN_CHIP_I2C_ENABLE 1

//#include "mapi_base.h"
#include "mapi_types.h"
//#include "mapi_demodulator.h"
//#include "mapi_dish.h"
// #include "device_demodulator_msb1233c.h"

// ------------------------------------------------------------
// Structure Define
// ------------------------------------------------------------


/// Define converlution code rate for DVB-T and DVB-S
typedef enum
{
    DEMOD_CONV_CODE_RATE_1_2_MSB1240,                                                 ///< Code rate = 1/2
    DEMOD_CONV_CODE_RATE_1_3_MSB1240,                                                 ///< Code rate = 1/3
    DEMOD_CONV_CODE_RATE_2_3_MSB1240,                                                 ///< Code rate = 2/3
    DEMOD_CONV_CODE_RATE_1_4_MSB1240,                                                 ///< Code rate = 1/4
    DEMOD_CONV_CODE_RATE_3_4_MSB1240,                                                 ///< Code rate = 3/4
    DEMOD_CONV_CODE_RATE_2_5_MSB1240,                                                 ///< Code rate = 2/5    
    DEMOD_CONV_CODE_RATE_3_5_MSB1240,                                                 ///< Code rate = 3/5
    DEMOD_CONV_CODE_RATE_4_5_MSB1240,                                                 ///< Code rate = 4/5          
    DEMOD_CONV_CODE_RATE_5_6_MSB1240,                                                 ///< Code rate = 5/6
    DEMOD_CONV_CODE_RATE_7_8_MSB1240,                                                 ///< Code rate = 7/8
    DEMOD_CONV_CODE_RATE_8_9_MSB1240,                                                 ///< Code rate = 8/9    
    DEMOD_CONV_CODE_RATE_9_10_MSB1240                                                 ///< Code rate = 9/10 
    
} DEMOD_EN_CONV_CODE_RATE_TYPE_MSB1240;

/// Define terrestrial band width
typedef enum
{
    DEMOD_BW_MODE_6MHZ_MSB1240 = 0,                                                   ///< 6 MHz
    DEMOD_BW_MODE_7MHZ_MSB1240,                                                       ///< 7 MHz
    DEMOD_BW_MODE_8MHZ_MSB1240                                                        ///< 8 MHz
} DEMOD_EN_TER_BW_MODE_MSB1240;


/// Define terrestrial constellation type
typedef enum
{
    DEMOD_TER_QPSK_MSB1240,                                                           ///< QPSK type
    DEMOD_TER_QAM16_MSB1240,                                                          ///< QAM 16 type
    DEMOD_TER_QAM64_MSB1240                                                           ///< QAM 64 type
} DEMOD_EN_TER_CONSTEL_TYPE_MSB1240;

/// Define terrestrial hierarchy information
typedef enum
{
    DEMOD_TER_HIE_NONE_MSB1240,                                                       ///< Non-hierarchy
    DEMOD_TER_HIE_ALPHA_1_MSB1240,                                                    ///< Hierarchy alpha = 1
    DEMOD_TER_HIE_ALPHA_2_MSB1240,                                                    ///< Hierarchy alpha = 2
    DEMOD_TER_HIE_ALPHA_4_MSB1240                                                     ///< Hierarchy alpha = 4
} DEMOD_EN_TER_HIE_TYPE_MSB1240;

/// Define terrestrial guard interval
typedef enum
{
    DEMOD_TER_GI_1_32_MSB1240,                                                        ///< Guard interval value = 1/32
    DEMOD_TER_GI_1_16_MSB1240,                                                        ///< Guard interval value = 1/16
    DEMOD_TER_GI_1_8_MSB1240,                                                         ///< Guard interval value = 1/8
    DEMOD_TER_GI_1_4_MSB1240                                                          ///< Guard interval value = 1/4
} DEMOD_EN_TER_GI_TYPE_MSB1240;

/// Define terrestrial transmission mode
typedef enum
{
    DEMOD_TER_FFT_2K_MSB1240,                                                         ///< 2k FFT mode
    DEMOD_TER_FFT_8K_MSB1240                                                          ///< 8k FFT mode
} DEMOD_EN_TER_FFT_MODE_MSB1240;

/// Define terrestrial transmission mode
typedef enum
{
    DEMOD_TER_HP_SEL_MSB1240,                                                         ///< High priority level selection
    DEMOD_TER_LP_SEL_MSB1240                                                          ///< Low priority level selection
} DEMOD_EN_TER_LEVEL_SEL_MSB1240;

/// Define DVB-C modulation scheme
typedef enum
{
    DEMOD_CAB_QAM16_MSB1240,                                                          ///< QAM 16
    DEMOD_CAB_QAM32_MSB1240,                                                          ///< QAM 32
    DEMOD_CAB_QAM64_MSB1240,                                                          ///< QAM 64
    DEMOD_CAB_QAM128_MSB1240,                                                         ///< QAM 128
    DEMOD_CAB_QAM256_MSB1240
} DEMOD_EN_CAB_CONSTEL_TYPE_MSB1240;


/// Define DVB-S IQ tuning mode
typedef enum
{
    DEMOD_CAB_IQ_NORMAL_MSB1240,                                                      ///< Normal
    DEMOD_CAB_IQ_INVERT_MSB1240                                                       ///< Inverse
} DEMOD_EN_CAB_IQ_MODE_MSB1240;


/// Define DVB-S modulatiopn scheme
typedef enum
{
    DEMOD_SAT_DVBS2_MSB1240,                                                          ///< DVBS2
    DEMOD_SAT_DVBS_MSB1240                                                            ///< DVBS
} DEMOD_EN_SAT_MOD_TYPE_MSB1240;

typedef enum
{
    DEMOD_SAT_QPSK_MSB1240,                                                           ///< QPSK
    DEMOD_SAT_8PSK_MSB1240,                                                           ///< 8PSK
    DEMOD_SAT_QAM16_MSB1240                                                           ///< QAM16
} DEMOD_EN_SAT_CONSTEL_TYPE_MSB1240;

/// Define DVB-S Roll-Off factor
typedef enum
{
    DEMOD_SAT_RO_35_MSB1240,                                                          ///< roll-off factor = 0.35
    DEMOD_SAT_RO_25_MSB1240,                                                          ///< roll-off factor = 0.25
    DEMOD_SAT_RO_20_MSB1240                                                           ///< roll-off factor = 0.20
} DEMOD_EN_SAT_ROLL_OFF_TYPE_MSB1240;

/// Define DVB-S IQ tuning mode
typedef enum
{
    DEMOD_SAT_IQ_NORMAL_MSB1240,                                                      ///< Normal
    DEMOD_SAT_IQ_INVERSE_MSB1240                                                      ///< Inverse
} DEMOD_EN_SAT_IQ_MODE_MSB1240;

/// Define Bit Error Rate range measure from signal
typedef enum
{
    DEMOD_BIT_ERR_RATIO_LOW_MSB1240 ,                                                 ///< Low BER
    DEMOD_BIT_ERR_RATIO_MEDIUM_MSB1240 ,                                              ///< Medium BER
    DEMOD_BIT_ERR_RATIO_HIGH_MSB1240                                                  ///< High BER
} DEMOD_EN_BIT_ERR_RATIO_MSB1240;

/// Define lock status of front end
typedef enum
{
    DEMOD_FE_UNLOCKED_MSB1240 = 0,                                                    ///< Frontend is unlocked
    DEMOD_FE_LOCKED_MSB1240                                                           ///< Frontend is locked
} DEMOD_EN_FE_LOCK_STATUS_MSB1240;


/// Define tuning mode
/// NOTE: When this typedef is modified, the apiChScan should be rebuild.
typedef enum
{
    DEMOD_FE_TUNE_MANUAL_MSB1240,                                                     ///< Manual tuning to carrier
    DEMOD_FE_TUNE_AUTO_MSB1240,                                                       ///< Auto tuning to carrier
} DEMOD_EN_FE_TUNE_MODE_MSB1240;

/// Define output mode
/// NOTE: When this typedef is modified, the apiChScan should be rebuild.
typedef enum
{
    DEMOD_INTERFACE_SERIAL_MSB1240 = 0,                                                   ///< Serial interface
    DEMOD_INTERFACE_PARALLEL_MSB1240                                                  ///< Parallel interface
} DEMOD_INTERFACE_MODE_MSB1240;

/// Define tuning paramter of DVB-T front-end
typedef struct
{
    DEMOD_EN_TER_BW_MODE_MSB1240                  eBandWidth;                         ///< Band width
    DEMOD_EN_TER_CONSTEL_TYPE_MSB1240             eConstellation;                     ///< Constellation type
    DEMOD_EN_TER_HIE_TYPE_MSB1240                 eHierarchy;                         ///< Hierarchy
    DEMOD_EN_TER_GI_TYPE_MSB1240                  eGuardInterval;                     ///< Guard interval
    DEMOD_EN_TER_FFT_MODE_MSB1240                 eFFT_Mode;                          ///< Transmission mode
    DEMOD_EN_CONV_CODE_RATE_TYPE_MSB1240          eHPCodeRate;                        ///< HP code rate
    DEMOD_EN_CONV_CODE_RATE_TYPE_MSB1240          eLPCodeRate;                        ///< LP code rate
    DEMOD_EN_TER_LEVEL_SEL_MSB1240                eLevelSel;                          ///< Select HP or LP level
} DEMOD_MS_TER_CARRIER_PARAM_MSB1240;

/// Define tuning paramter of DVB-C front-end
typedef struct
{
    DEMOD_EN_CAB_CONSTEL_TYPE_MSB1240           eConstellation;                     ///< Constellation type
    MAPI_U16                          u16SymbolRate;                      ///< Symbol rate (Ksym/sec)

    DEMOD_EN_CAB_IQ_MODE_MSB1240                eIQMode;                            ///< IQ Mode
    MAPI_U8                           u8TapAssign;                        ///< Tap assign
    MAPI_U32                          u32FreqOffset;                      ///< Carrier frequency offset
    MAPI_U8                           u8TuneFreqOffset;                       ///< Requeset tuner freq offset
} DEMOD_MS_CAB_CARRIER_PARAM_MSB1240;

/// Define tuning paramter of DVB-S front-end
typedef struct
{
    DEMOD_EN_SAT_MOD_TYPE_MSB1240                   eDemod_Type;                        ///< Mode type
    DEMOD_EN_SAT_CONSTEL_TYPE_MSB1240               eConstellation;                     ///< Constellation type
    DEMOD_EN_SAT_ROLL_OFF_TYPE_MSB1240              eRollOff;                           ///< Roll-Off factor
    DEMOD_EN_SAT_IQ_MODE_MSB1240                    eIQ_Mode;                           ///< IQ mode
    DEMOD_EN_CONV_CODE_RATE_TYPE_MSB1240            eCodeRate;                          ///< Converlution code rate
    MAPI_U32                                u32SymbolRate;
    MAPI_U8                                 u8Polarity;                         // 0: Horizon; > 0(default 1): Vertical;
    MAPI_S16                                s16FreqOffset;                        

} DEMOD_MS_SAT_CARRIER_PARAM_MSB1240;

/// Define carrier paramter of digital tuner
/// NOTE: When this typedef is modified, the apiChScan should be rebuild.
typedef struct
{
    MAPI_U32                          u32Frequency;
    union
    {
        DEMOD_MS_TER_CARRIER_PARAM_MSB1240        TerParam;                           ///< Paramters for DVB-T front-end
        DEMOD_MS_CAB_CARRIER_PARAM_MSB1240        CabParam;                           ///< Paramters for DVB-C front-end
        DEMOD_MS_SAT_CARRIER_PARAM_MSB1240        SatParam;                           ///< Paramters for DVB-S front-end
    };
} DEMOD_MS_FE_CARRIER_PARAM_MSB1240;

/// Define the quality report
typedef struct
{
    DEMOD_EN_FE_LOCK_STATUS_MSB1240                 eLock;                              ///< Lock
    DEMOD_EN_BIT_ERR_RATIO_MSB1240                  eBER;                               ///< Bit error rate
    float                                 	fSNR;                              ///< SNR
    float                                   fSignalLevel;                       ///< Signal Level=1~100
    MAPI_U16                                u16SignalStrength;                  ///< Signal Strength[dBm],mick
    MAPI_U8                                 u8SignalQuality;                    ///< Signal Quality,mick
    float                                   fPreBER;                            ///< xxE-xx,mick
    float                                   fPostBerTSBER;
    MAPI_U32                                u32LockTime;                        ///< LockTime
    MAPI_U16                                u16TSpacketError;                   ///< TS Packet Error
} DEMOD_MS_FE_CARRIER_STATUS_MSB1240;

typedef struct
{
        MAPI_BOOL                           bLNBPowerOn;                                              ///< Power On/Off
        MAPI_BOOL                           b22kOn;                                                           ///< LNB 22k On/Off
        MAPI_BOOL                           bLNBOutLow;                                                   ///< LNB 13/18V
                
} DEMOD_MS_FE_CARRIER_DISEQC_MSB1240;

/// Define the carrier information
typedef struct
{
    DEMOD_MS_FE_CARRIER_PARAM_MSB1240             Param;                              ///< Carrier parameter
    DEMOD_MS_FE_CARRIER_STATUS_MSB1240            Status;                             ///< Quality report
    DEMOD_MS_FE_CARRIER_DISEQC_MSB1240            DiSEqCp;                                                      ///< DiSEqC
} DEMOD_MS_FE_CARRIER_INFO_MSB1240;

typedef struct
{
    // Demodulator option
    MAPI_BOOL                         bX4CFE_en;                          ///< Carrier frequency estimation
    MAPI_BOOL                         bPPD_en;                            ///< Tap assign estimation
    MAPI_BOOL                         bIQAutoSwap_en;                     ///< IQ mode auto swap
    MAPI_BOOL                         bQAMScan_en;                        ///< QAM type auto scan
    MAPI_BOOL                         bFHO_en;                            ///< FHO
    MAPI_BOOL                         (*fptTunerSet)(MAPI_U32);             ///< Tuner set freq function pointer
} Demod_Mode_MSB1240;

typedef struct
{
    MAPI_U8        cmd_code;
    MAPI_U8        param[64];
} device_demodulator_extend_S_CMDPKTREG;

typedef struct BIN_FORMAT
{
    MAPI_U16 B_ID;                 //!< Unique ID
    MAPI_U32 B_FAddr;              //!< Start address
    MAPI_U32 B_Len;                //!< Length in bytes
    MAPI_U8  B_IsComp;             //!< Is bin compressed
} BINFORMAT;

typedef enum
{
	device_demodulator_extend_COFDM_FEC_LOCK_DVBT,
	device_demodulator_extend_COFDM_FEC_LOCK_DVBC,
	device_demodulator_extend_COFDM_TR_LOCK_DVBC,
	device_demodulator_extend_COFDM_PSYNC_LOCK,
	device_demodulator_extend_COFDM_TPS_LOCK,
	device_demodulator_extend_COFDM_TPS_LOCK_HISTORY,
	device_demodulator_extend_COFDM_DCR_LOCK,
	device_demodulator_extend_COFDM_AGC_LOCK,
	device_demodulator_extend_COFDM_MODE_DET,
	device_demodulator_extend_COFDM_LOCK_STABLE_DVBT,
	device_demodulator_extend_COFDM_SYNC_LOCK_DVBT,
	device_demodulator_extend_COFDM_FAST_LOCK_DVBT,//add 0920
	//addy update 0805
	device_demodulator_extend_COFDM_P1_LOCK,
	device_demodulator_extend_COFDM_P1_LOCK_HISTORY,
	device_demodulator_extend_COFDM_L1_CRC_LOCK,
	device_demodulator_extend_COFDM_FEC_LOCK_T2

} device_demodulator_extend_COFDM_LOCK_STATUS;

typedef enum
{
	device_demodulator_extend_E_DEVICE_DEMOD_UNKOWN = -1,
	device_demodulator_extend_E_DEVICE_DEMOD_DVB_T2,
	device_demodulator_extend_E_DEVICE_DEMOD_DVB_T,
	device_demodulator_extend_E_DEVICE_DEMOD_DVB_C,
	device_demodulator_extend_E_DEVICE_DEMOD_DVB_S,
	device_demodulator_extend_E_DEVICE_DEMOD_NUM
} device_demodulator_extend_E_SYSTEM;

typedef enum
{
    device_demodulator_extend_CMD_SYSTEM_INIT = 0,
    device_demodulator_extend_CMD_DAC_CALI,
    device_demodulator_extend_CMD_DVBT_CONFIG,
    device_demodulator_extend_CMD_DVBC_CONFIG,
    device_demodulator_extend_CMD_VIF_CTRL,
    device_demodulator_extend_CMD_FSM_CTRL,
    device_demodulator_extend_CMD_INDIR_RREG,
    device_demodulator_extend_CMD_INDIR_WREG,
    device_demodulator_extend_CMD_GET_INFO,
    device_demodulator_extend_CMD_TS_CTRL,
    device_demodulator_extend_CMD_TUNED_VALUE,
    device_demodulator_extend_CMD_MAX_NUM
} device_demodulator_extend_E_CMD_CODE;

typedef enum
{
    device_demodulator_extend_TS_PARALLEL = 0,
    device_demodulator_extend_TS_SERIAL = 1,
    device_demodulator_extend_TS_MODE_MAX_NUM
} device_demodulator_extend_E_TS_MODE;

typedef enum
{
    device_demodulator_extend_COFDM_CCI_FLAG = 0,
    device_demodulator_extend_CHECK_FLAG_MAX_NUM
} device_demodulator_extend_COFDM_CHECK_FLAG;

typedef enum
{
    TS_MODUL_MODE,
    TS_FFT_VALUE,
    TS_GUARD_INTERVAL,
    TS_CODE_RATE,
    TS_HIERARCHY,

    TS_PARAM_MAX_NUM
}E_SIGNAL_TYPE;

typedef enum
{
    T2_MODUL_MODE,
    T2_FFT_VALUE,
    T2_GUARD_INTERVAL,
    T2_CODE_RATE,
    T2_PREAMBLE,
    T2_S1_SIGNALLING,
    T2_PILOT_PATTERN,
    T2_BW_EXT,
    T2_PAPR_REDUCTION,
    T2_OFDM_SYMBOLS_PER_FRAME,
    T2_PARAM_MAX_NUM,
    T2_PLP_ROTATION,
    T2_PLP_FEC_TYPE,
    T2_NUM_PLP,
} E_T2_SIGNAL_INFO;

typedef enum
{
    TS_VIF_TOP,
    TS_VIF_VGA_MAXIMUM,
    TS_VIF_GAIN_DISTRIBUTION_THR,
    TS_VIF_AGC_VGA_BASE,
    TS_VIF_CHINA_DESCRAMBLER_BOX,
    TS_VIF_CR_KP1,
    TS_VIF_CR_KI1,
    TS_VIF_CR_KP2,
    TS_VIF_CR_KI2,
    TS_VIF_CR_KP,
    TS_VIF_CR_KI,
    TS_VIF_CR_LOCK_THR,
    TS_VIF_CR_THR,
    TS_VIF_CR_KP_KI_ADJUST,
    TS_VIF_DELAY_REDUCE,
    TS_VIF_OVER_MODULATION,
    TS_VIF_CLAMPGAIN_CLAMP_OV_NEGATIVE,
    TS_VIF_CLAMPGAIN_GAIN_OV_NEGATIVE,
    TS_VIF_ACI_AGC_REF,
    TS_VIF_ASIA_SIGNAL_OPTION,
    TS_VIF_CR_KP_ASIA,
    TS_VIF_CR_KI_ASIA,
    TS_VIF_CR_KP_KI_ADJUST_ASIA,

    TS_VIF_PARAM_MAX_NUM
}E_VIF_PARAMETER;

typedef struct S_T2_MOD_INFO
{
    MAPI_U8 s1_siso_miso;  // 0: SISO, 1:MISO
    MAPI_U8 plp_number; //8bit: min value='1'
    MAPI_U8 plp_id; //8bit
    MAPI_U8 constellation_l1; // 0:BPSK; 1:QPSK; 2:16QAM; 3:64QAM; 4~8:reserved or future use
    MAPI_U8 coderate_l1; // 0:1/2; 1~3:reserved or future use
    MAPI_U8 plp_constellation; // 0:QPSK; 1:16QAM; 2:64QAM; 3:256QAM; 4~7:reserved or future use
    MAPI_U8 plp_rotation_constellation; // 1: rotation is used. ): rotation is not used.
    MAPI_U8 plp_coderate; // 0:1/2; 1:3/5; 2:2/3; 3:3/4; 4:4/5; 5:5/6; 6~7:reserved or future use
    MAPI_U8 plp_fec; // 0: 16K LDPC; 1:64K LDPC; 2~3:reserved or future use
    MAPI_U8 plp_frame_il_length; // ref DVB-T2 spec.
    MAPI_U8 plp_type_time_il; // ref DVB-T2 spec.
    MAPI_U8 plp_length_time_il; // ref DVB-T2 spec.
    MAPI_U8 pilot_pattern; // 0:PP1; 1:PP2; 2:PP3; 3:PP4; 4:PP5; 5:PP6; 6:PP7; 7:PP8; 8~15:reserved or future use
    MAPI_U8 mode_carrier; // ref DVB-T2 spec.
    MAPI_U8 fef_type; // 0~15: reserved or future use
    MAPI_U32 fef_length; // 22bit
    MAPI_U32 fef_interval; // 8bit
    MAPI_U8 papr; // 0:No PAPR reduction is used; 1:ACE-PAPR only is used
                  // 2:TR-PAPR only is used; 3:Both ACE and TR are used
                  // 4~8: Reserved for future use
    MAPI_U8 guard_interval; // 0:1/32; 1:1/16; 2:1/8; 3:1/4; 4:1/128; 5:19/128; 6:19/256; 7:reserved or future use
}t2_mod_info;

#if 0
class device_demodulator : public mapi_base
{
public:

    // ------------------------------------------------------------
    // public Enum Define
    // ------------------------------------------------------------

    static MAPI_BOOL DTV_DVBT_DSPReg_Init();

    static MAPI_U8 u8DeviceBusy;

    // ------------------------------------------------------------
    // public operations
    // ------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------
    /// Connect function point to functions.
    /// @param  None
    /// @return                 \b TRUE: Connect functions success, FALSE: Connect functions failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DeviceDemodCreate(void);

    //-------------------------------------------------------------------------------------------------
    /// Connect to demod & set demod type.
    /// @param  enDemodType \b IN: Demod type.
    /// @return                 \b TRUE: Connecting and setting success, FALSE: Connecting failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Connect(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE enDemodType);

    //-------------------------------------------------------------------------------------------------
    /// Disconnect from demod.
    /// @param  None
    /// @return                 \b TRUE: Disconnect success, FALSE: Disconnect failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Disconnect(void);

    //-------------------------------------------------------------------------------------------------
    /// Reset demod status and state.
    /// @param  None
    /// @return  None
    //-------------------------------------------------------------------------------------------------
    static void Reset();

    //-------------------------------------------------------------------------------------------------
    /// Set demod to bypass iic ,so controller can communcate with tuner.
    /// @param  enable       \b IN: bypass or not.
    /// @return                 \b TRUE: Bypass setting success, FALSE: Bypass setting done failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL IIC_Bypass_Mode(MAPI_BOOL enable);

#if (STR_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// init demod. of STR resume from suspend
    /// @param  None
    /// @return                 \b TRUE: Init success, FALSE: Init failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL ResumeInit(void);
#endif

    //-------------------------------------------------------------------------------------------------
    /// Init demod.
    /// @param  None
    /// @return                 \b TRUE: Init success, FALSE: Init failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Power_On_Initialization(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod power on.
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Set_PowerOn(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod power off.
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Set_PowerOff(void);

    //-------------------------------------------------------------------------------------------------
    /// Trigger demod to active or not.
    /// @param  bEnable     \b IN: Enbale or not.
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------

    static MAPI_BOOL  DTV_DVBT2_DSPReg_Init(void);
    //-------------------------------------------------------------------------------------------------
    /// Init Demod DSP reg
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------

    static MAPI_BOOL Active(MAPI_BOOL bEnable);

    //-------------------------------------------------------------------------------------------------
    /// Get current demodulator type.
    /// @param  None
    /// @return  EN_DEVICE_DEMOD_TYPE     \b Current demodulator type.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE GetCurrentDemodulatorType(void);

    //-------------------------------------------------------------------------------------------------
    /// Get current demodulator type.
    /// @param  enDemodType \b IN: demodulator type
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL SetCurrentDemodulatorType(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE enDemodType);

    //-------------------------------------------------------------------------------------------------
    /// For user defined command.
    /// @param  SubCmd     \b IN: sub command.
    /// @param  u32Param1     \b IN: command parameter1.
    /// @param  u32Param2     \b IN: command parameter2.
    /// @param  pvParam3     \b IN: command parameter3 point.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL ExtendCmd(MAPI_U8 SubCmd, MAPI_U32 u32Param1, MAPI_U32 u32Param2, void *pvParam3);
    static void* PreLoadDSPcode(void *arg);

//Public:ATV
    static mapi_demodulator_datatype::AFC  ATV_GetAFC_Distance(void);

    static MAPI_BOOL ATV_SetVIF_SoundSystem(mapi_demodulator_datatype::DEMOD_AUDIOSTANDARD_TYPE_ eIF_Freq);

//@@++
    static mapi_demodulator_datatype::IF_FREQ ATV_GetIF(void);
    static MAPI_BOOL ATV_SetIF(mapi_demodulator_datatype::IF_FREQ eIF_Freq);
    static MAPI_U32 ATV_GetIFAGC(void);
    static MAPI_U16 ATV_GetVifIfFreq(void);
    static MAPI_BOOL ATV_Initialized(void);
    static MAPI_U8 ATV_ScanningStatus(MAPI_BOOL status);
    static MAPI_BOOL ATV_SetPeakingParameters(RFBAND eBand);
    static MAPI_BOOL ATV_SetAudioNotch(void);
    static MAPI_BOOL ATV_SetAGCParameters(mapi_demodulator_datatype::EN_EMC_IF_AGC_SETTINGS par,MAPI_U32 val);
//@@++

    static MAPI_BOOL ATV_SetAudioSawFilter(MAPI_U8 u8SawFilterMode);

    static MAPI_BOOL ATV_SetVIF_IfFreq(MAPI_U16 u16IfFreq);

    static MAPI_BOOL ATV_VIF_Init(void);

    static MAPI_BOOL ATV_VIF_Handler(MAPI_BOOL);

    static MAPI_BOOL ATV_GetVIF_InitailValue(mapi_vif_datatype::stVIFInitialIn *VIFInitialIn_inst);
    //Public:DTV


    //-------------------------------------------------------------------------------------------------
    /// Get signal SNR.
    /// @param  None
    /// @return      \b  Signal condition.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_FRONTEND_SIGNAL_CONDITION DTV_GetSNR(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Packet error of DVBT and DVBC.
    /// @param  SNR
    /// @return      \b  SNR.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetSNR_fo(float *f_snr);

    //-------------------------------------------------------------------------------------------------
    /// Get signal BER.
    /// @param  None
    /// @return      \b  Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U32 DTV_GetBER(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Pre Viterbi BER of DVBT.
    /// @param  None
    /// @return      \b  Pre Viterbi Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetPreBER(float *p_preBer);

    //-------------------------------------------------------------------------------------------------
    /// Get Post Viterbi BER of DVBT and DVBC.
    /// @param  None
    /// @return      \b  Post Viterbi Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetPostBER(float *p_postBer);

    //-------------------------------------------------------------------------------------------------
    /// Get Packet error of DVBT and DVBC.
    /// @param  None
    /// @return      \b  Packet error.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetPacketErr(void);

     //-------------------------------------------------------------------------------------------------
    /// Get Packet error of DVBT and DVBC.
    /// @param  None
    /// @return      \b  Packet error.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetPacketErr(MAPI_U16 *pu16BitErr);

     //-------------------------------------------------------------------------------------------------
    /// Get signal quality.
    /// @param  None
    /// @return      \b  Signal quality.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetSignalQuality(void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal strength.
    /// @param  None
    /// @return      \b  Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetSignalStrength(void);

    //-------------------------------------------------------------------------------------------------
    /// Get cell ID.
    /// @return  MAPI_U16   \b OUT:Cell ID.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetCellID(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod to output serial or parallel.
    /// @param  bEnable \b IN: True : serial . False : parallel.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_Serial_Control( MAPI_BOOL bEnable);

    //-------------------------------------------------------------------------------------------------
    /// check if Hierarchy On.
    /// @return      \b TRUE: Hierarchy On.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_IsHierarchyOn( void);


#if (DVBT_SYSTEM_ENABLE == 1)
    //Public:DTV-DVB-T

    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBT) (DVBT2)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  eBandWidth     \b IN: eBandWidth.
    /// @param  bLPsel     \b IN: LP select.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth, MAPI_BOOL bPalBG, MAPI_BOOL bLPsel = MAPI_FALSE);
    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBT) (DVBT2)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_T_GetLockStatus(void);
    //Public:DTV-DVB-T2

    static MAPI_BOOL DTV_GetPlpBitMap(MAPI_U8* u8PlpBitMap);
    static MAPI_BOOL DTV_GetPlpGroupID(MAPI_U8 u8PlpID, MAPI_U8* u8GroupID);
    static MAPI_BOOL DTV_SetPlpGroupID(MAPI_U8 u8PlpID, MAPI_U8 u8GroupID);
#endif
#if (ISDB_SYSTEM_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.ISDB
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_ISDB_GetLockStatus(void);
#endif

    static MAPI_BOOL WriteDspReg(MAPI_U16 u16Addr, MAPI_U8 u8Data);
    static MAPI_BOOL ReadDspReg(MAPI_U16 u16Addr, MAPI_U8* pData);
#if (DVBC_SYSTEM_ENABLE == 1)
//Public:DTV-DVB-C

    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBC)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  eBandWidth     \b IN: BandWidth.
    /// @param  uSymRate     \b IN: Symbol rate.
    /// @param  eQAM     \b IN: QAM type.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_C_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth,MAPI_U32 uSymRate,mapi_demodulator_datatype::EN_CAB_CONSTEL_TYPE eQAM);

    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBC)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_C_GetLockStatus(void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal modulation mode.
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_CAB_CONSTEL_TYPE DTV_DVB_C_GetSignalModulation (void);


    //-------------------------------------------------------------------------------------------------
    /// Get current symbol rate.(DVBC)
    /// @param  None
    /// @return      \b Symbol rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_DVB_C_GetCurrentSymbolRate(void);
#endif
#if (DVBS_SYSTEM_ENABLE == 1)
    //Public:DTV-DVB-S
    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBS)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  uSymRate     \b IN: Symbol rate.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetFrequency(MAPI_U32 u32Frequency, MAPI_U32 u32SymbolBitrate);

    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBS)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_S_GetLockStatus(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Roll Off factor.(DVBS)
    /// @param  None
    /// @return      \b Roll off  factor.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_SAT_ROLL_OFF_TYPE DTV_DVB_S_GetRollOff(void);

    //-------------------------------------------------------------------------------------------------
    /// Set  Tone.(DVBS)
    /// @param  eTone  \b eTone1 eTone0
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetToneBurst(mapi_dish_datatype::EN_SAT_TONEBUREST_TYPE eTone);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bLow \b IN true:13v false:18v
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetLNBPower(mapi_dish_datatype::EN_SAT_LNBPOWER_TYPE eTone);

     //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bOn \b IN TRUE:22k on  FALSE:22k off
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_Set22KOnOff(MAPI_BOOL bOn);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bOn \b OUT TRUE:22k on  FALSE:22k off
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_Get22KStatus(MAPI_BOOL* bOn);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  pCmd    \ IN cmd
    /// @param  u8CmdSize   \IN cmd size
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SendDiSEqCCmd(MAPI_U8* pCmd,MAPI_U8 u8CmdSize);


    //-------------------------------------------------------------------------------------------------
    /// To init blind scan:switch to blind scan mode and set frequency range
    /// @param u16StartFreq         \b IN: start frequency
    /// @param u16EndFreq         \b IN: end frequency
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_Init(MAPI_U16 u16StartFreq,MAPI_U16 u16EndFreq);
    //-------------------------------------------------------------------------------------------------
    /// blind scan next frequencys
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_ScanNextFreq(void);
    //-------------------------------------------------------------------------------------------------
    /// get transponders found
    /// @param u8No         \b IN: transponders no
    /// @param u16Freq         \b OUT: TP frequency
    /// @param u16SymbolRate         \b OUT: TP symbol rate
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetFoundTP(MAPI_U8 u8No, MAPI_U16 &u16Freq,MAPI_U16 &u16SymbolRate);
    //-------------------------------------------------------------------------------------------------
    /// cancel current frequency scan
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_Cancel(void);
    //-------------------------------------------------------------------------------------------------
    /// end blind scan
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_End(void);
   //-------------------------------------------------------------------------------------------------
    /// get status
    /// @param eStatus         \b OUT:status
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetStatus(mapi_demodulator_datatype::EN_BLINDSCAN_STATUS &eStatus);
    ///-------------------------------------------------------------------------------------------------
    /// get current frequency  scanned currently
    /// @param u16Freq         \b OUT: requency
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetScanFreq(MAPI_U16 &u16Freq);
#endif
#if (DTMB_SYSTEM_ENABLE == 1)
    //Public:DTV-DTMB
    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DTMB)
    /// @param  u32Freq                  \b IN: Tuner frequency
    /// @param  eBand                   \b IN: Channel bandwidth
    /// @param  eSubCarriers         \b IN: sub-carriers
    /// @param  ePN                      \b IN: PN padding
    /// @param  eMAPPING                \b IN: mapping type
    /// @param  eFEC                     \b IN: FEC code rate
    /// @param  s8RouteID               \b IN: RouteID
    /// @return                          \b TRUE: Set  success, FALSE: Set  failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DTMB_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth, mapi_demodulator_datatype::EN_DTMB_SUBCARRIERS eSubCarriers,
                    mapi_demodulator_datatype::EN_DTMB_PN_PADDING ePN, mapi_demodulator_datatype::EN_DTMB_MAPPING eMAPPING, mapi_demodulator_datatype::EN_DTMB_FEC_CODERATE eFEC );

    static MAPI_BOOL DTV_DTMB_GetProperity(MAPI_U32* u32Frequency, RF_CHANNEL_BANDWIDTH* eBandWidth, mapi_demodulator_datatype::EN_DTMB_SUBCARRIERS* eSubCarriers,
                        mapi_demodulator_datatype::EN_DTMB_PN_PADDING* ePN, mapi_demodulator_datatype::EN_DTMB_MAPPING* eMAPPING, mapi_demodulator_datatype::EN_DTMB_FEC_CODERATE* eFEC );
    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DTMB)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DTMB_GetLockStatus(void);
#endif

#if (ATSC_SYSTEM_ENABLE==1)
//Public:DTV-ATSC
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_ATSC_GetLockStatus(void);

    static MAPI_BOOL DTV_ATSC_ClkEnable(MAPI_BOOL bEnable);

    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE DTV_ATSC_GetModulationMode(void);

    static MAPI_BOOL DTV_ATSC_ChangeModulationMode(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE eMode);
#endif
#if (DVBT_SYSTEM_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// Get TPS Parameters
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_T_Get_TPS_Parameter( MAPI_U16 * pu16TPS_parameter, E_SIGNAL_TYPE eSignalInfo);
    //-------------------------------------------------------------------------------------------------
    /// Get signal modulation mode.
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_CONSTEL_TYPE DTV_DVB_T_GetSignalModulation (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal guard Interval
    /// @param  None
    /// @return      \b  guard Interval
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_GUARD_INTERVAL DTV_DVB_T_GetSignalGuardInterval (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal FFT Value
    /// @param  None
    /// @return      \b  FFT Value
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_FFT_VAL DTV_DVB_T_GetSignalFFTValue (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal Code Rate
    /// @param  None
    /// @return      \b  Code Rate
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_CODE_RATE DTV_DVB_T_GetSignalCodeRate (void);
#endif
private:

    // ------------------------------------------------------------
    // Variable Define
    // ------------------------------------------------------------
    static MAPI_BOOL m_bSerialOut;
    static MAPI_U8 gVifTop;// = VIF_TOP;
    static MAPI_U8 gVifSoundSystem;// = VIF_SOUND_DK2;
    static MAPI_U8 gVifIfFreq;// = IF_FREQ_3890;
//@@++
    static mapi_demodulator_datatype::IF_FREQ m_eIF_Freq ;//= mapi_demodulator_datatype::IF_FREQ_INVALID;

    static MAPI_U32 Tuner_TOP_Setting ;//= 0xFF;
    static MAPI_U32 Tuner_Top_Setting_LPrime ;//=0x05;
    static MAPI_U32 Tuner_Top_Setting_SECAM_VHF ;//=0x04;
    static MAPI_U32 Tuner_Top_Setting_SECAM_UHF ;//F=0x03;
    static MAPI_U32 Tuner_Top_Setting_PAL_VHF ;//=0x02;
    static MAPI_U32 Tuner_Top_Setting_PAL_UHF ;//=0x01;

    static MAPI_U16 IfFreq ;//= 0;
    static MAPI_BOOL IFDM_Initialized ;// = FALSE;
    static MAPI_U8 Scanning_Active ;//=0;
//@@++
    static MAPI_U8 u8MsbData[6];
    static MAPI_U8 gu8ChipRevId;
    static MAPI_U8 gCalIdacCh0, gCalIdacCh1;
    static S_CMDPKTREG gsCmdPacket;
    static MAPI_U8 gu8DemoDynamicI2cAddress;//=0x32;//Default
    static MAPI_BOOL     FECLock;
    static MAPI_BOOL     gbTVAutoScanEn;//=FALSE;//init value=FALSE, follow with auto/manual scan
    static MAPI_BOOL     m_bDSPLoad;
    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE m_enCurrentDemodulator_Type;

    // ------------------------------------------------------------
    // private operations
    // ------------------------------------------------------------

    static MAPI_BOOL I2C_CH_Reset(MAPI_U8 ch_num);

    static MAPI_BOOL WriteReg(MAPI_U16 u16Addr, MAPI_U8 u8Data);

    static MAPI_BOOL ReadReg(MAPI_U16 u16Addr, MAPI_U8 *pu8Data);

    static MAPI_BOOL WriteRegs(MAPI_U16 u16Addr, MAPI_U8* u8pData, MAPI_U16 data_size);

    static MAPI_BOOL WriteReg2bytes(MAPI_U16 u16Addr, MAPI_U16 u16Data);

    static MAPI_BOOL Load2Sdram(MAPI_U8 *u8_ptr, MAPI_U16 data_length, MAPI_U16 sdram_win_offset_base);

    static MAPI_BOOL LoadSdram2Sram(MAPI_U8 CodeNum);

    static MAPI_BOOL MSB123x_MEM_switch(MAPI_U8 mem_type);

    static MAPI_BOOL LoadDspCodeToSDRAM_Boot(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbt2(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbt(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbc(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbs(void);

    static MAPI_BOOL LoadDspCodeToSDRAM(MAPI_U8 code_n);

    static MAPI_U8 DTV_DVBT_DSPReg_CRC(void);

    static void DTV_DVBT_DSPReg_ReadBack(void);

    static MAPI_BOOL Cmd_Packet_Send(S_CMDPKTREG* pCmdPacket, MAPI_U8 param_cnt);

    static MAPI_BOOL Cmd_Packet_Exe_Check(MAPI_BOOL* cmd_done);

    static MAPI_BOOL LoadDSPCode(void);

    static MAPI_BOOL I2C_Address_Polling(void);

    static MAPI_BOOL ATV_VIF_SetHandler(MAPI_BOOL bAutoScan, MAPI_U16 GainDistributionThr);

    static MAPI_BOOL ATV_VIF_IfInitial(MAPI_U16 VifVgaMaximum);

    static MAPI_BOOL ATV_VIF_TopAdjust(MAPI_U8 ucVifTop);

    static MAPI_BOOL ATV_VIF_SoftReset();

    static MAPI_BOOL DTV_Config(RF_CHANNEL_BANDWIDTH BW, MAPI_BOOL bSerialTS, MAPI_BOOL bPalBG);

    static void Driving_Control(MAPI_BOOL bEnable);

    static mapi_demodulator_datatype::E_VIF_TYPE ATV_GetVIF_Type();

    static MAPI_BOOL MSB123x_HW_init(void);

    static MAPI_U16 MSB1231_Lock(E_SYSTEM system, COFDM_LOCK_STATUS eStatus );

};
#endif
/*@ </Include> @*/

#if 0
class device_demodulator_extend : public mapi_base
{
public:

    // ------------------------------------------------------------
    // public Enum Define
    // ------------------------------------------------------------

    static MAPI_BOOL DTV_DVBT_DSPReg_Init();

    static MAPI_U8 u8DeviceBusy;

    // ------------------------------------------------------------
    // public operations
    // ------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------
    /// Connect function point to functions.
    /// @param  None
    /// @return                 \b TRUE: Connect functions success, FALSE: Connect functions failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DeviceDemodCreate(void);

    //-------------------------------------------------------------------------------------------------
    /// Connect to demod & set demod type.
    /// @param  enDemodType \b IN: Demod type.
    /// @return                 \b TRUE: Connecting and setting success, FALSE: Connecting failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Connect(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE enDemodType);

    //-------------------------------------------------------------------------------------------------
    /// Disconnect from demod.
    /// @param  None
    /// @return                 \b TRUE: Disconnect success, FALSE: Disconnect failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Disconnect(void);

    //-------------------------------------------------------------------------------------------------
    /// Reset demod status and state.
    /// @param  None
    /// @return  None
    //-------------------------------------------------------------------------------------------------
    static void Reset();

    //-------------------------------------------------------------------------------------------------
    /// Set demod to bypass iic ,so controller can communcate with tuner.
    /// @param  enable       \b IN: bypass or not.
    /// @return                 \b TRUE: Bypass setting success, FALSE: Bypass setting done failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL IIC_Bypass_Mode(MAPI_BOOL enable);

#if (STR_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// init demod. of STR resume from suspend
    /// @param  None
    /// @return                 \b TRUE: Init success, FALSE: Init failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL ResumeInit(void);
#endif

    //-------------------------------------------------------------------------------------------------
    /// Init demod.
    /// @param  None
    /// @return                 \b TRUE: Init success, FALSE: Init failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Power_On_Initialization(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod power on.
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Set_PowerOn(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod power off.
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Set_PowerOff(void);

    //-------------------------------------------------------------------------------------------------
    /// Trigger demod to active or not.
    /// @param  bEnable     \b IN: Enbale or not.
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------

    static MAPI_BOOL  DTV_DVBT2_DSPReg_Init(void);
    //-------------------------------------------------------------------------------------------------
    /// Init Demod DSP reg
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------

    static MAPI_BOOL Active(MAPI_BOOL bEnable);

    //-------------------------------------------------------------------------------------------------
    /// Get current demodulator type.
    /// @param  None
    /// @return  EN_DEVICE_DEMOD_TYPE     \b Current demodulator type.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE GetCurrentDemodulatorType(void);

    //-------------------------------------------------------------------------------------------------
    /// Get current demodulator type.
    /// @param  enDemodType \b IN: demodulator type
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL SetCurrentDemodulatorType(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE enDemodType);

    //-------------------------------------------------------------------------------------------------
    /// For user defined command.
    /// @param  SubCmd     \b IN: sub command.
    /// @param  u32Param1     \b IN: command parameter1.
    /// @param  u32Param2     \b IN: command parameter2.
    /// @param  pvParam3     \b IN: command parameter3 point.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL ExtendCmd(MAPI_U8 SubCmd, MAPI_U32 u32Param1, MAPI_U32 u32Param2, void *pvParam3);
    static void* PreLoadDSPcode(void *arg);

//Public:ATV
    static mapi_demodulator_datatype::AFC  ATV_GetAFC_Distance(void);

    static MAPI_BOOL ATV_SetVIF_SoundSystem(mapi_demodulator_datatype::DEMOD_AUDIOSTANDARD_TYPE_ eIF_Freq);

//@@++
    static mapi_demodulator_datatype::IF_FREQ ATV_GetIF(void);
    static MAPI_BOOL ATV_SetIF(mapi_demodulator_datatype::IF_FREQ eIF_Freq);
    static MAPI_U32 ATV_GetIFAGC(void);
    static MAPI_U16 ATV_GetVifIfFreq(void);
    static MAPI_BOOL ATV_Initialized(void);
    static MAPI_U8 ATV_ScanningStatus(MAPI_BOOL status);
    static MAPI_BOOL ATV_SetPeakingParameters(RFBAND eBand);
    static MAPI_BOOL ATV_SetAGCParameters(mapi_demodulator_datatype::EN_EMC_IF_AGC_SETTINGS par,MAPI_U32 val);
//@@++

    static MAPI_BOOL ATV_SetAudioSawFilter(MAPI_U8 u8SawFilterMode);

    static MAPI_BOOL ATV_SetVIF_IfFreq(MAPI_U16 u16IfFreq);

    static MAPI_BOOL ATV_VIF_Init(void);

    static MAPI_BOOL ATV_VIF_Handler(MAPI_BOOL);

    //Public:DTV


    //-------------------------------------------------------------------------------------------------
    /// Get signal SNR.
    /// @param  None
    /// @return      \b  Signal condition.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_FRONTEND_SIGNAL_CONDITION DTV_GetSNR(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Packet error of DVBT and DVBC.
    /// @param  SNR
    /// @return      \b  SNR.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetSNR_fo(float *f_snr);

    //-------------------------------------------------------------------------------------------------
    /// Get signal BER.
    /// @param  None
    /// @return      \b  Bit error rate.
    //-------------------------------------------------------------------------------------------------
      static MAPI_U32 DTV_GetBER(void);
    //-------------------------------------------------------------------------------------------------
    /// Get Pre Viterbi BER of DVBT.
    /// @param  None
    /// @return      \b  Pre Viterbi Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetPreBER(float *p_preBer);

    //-------------------------------------------------------------------------------------------------
    /// Get Post Viterbi BER of DVBT and DVBC.
    /// @param  None
    /// @return      \b  Post Viterbi Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetPostBER(float *p_postBer);

    //-------------------------------------------------------------------------------------------------
    /// Get IF AGC Gain of DVBT/T2 and DVBC.
    /// @param  None
    /// @return      \b  IF AGC.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetIFAGCGain(MAPI_U16 *pu16ifagc);

    //-------------------------------------------------------------------------------------------------
    /// Get Packet error of DVBT and DVBC.
    /// @param  None
    /// @return      \b  Packet error.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetPacketErr(MAPI_U16  *pu16BitErr);

     //-------------------------------------------------------------------------------------------------
    /// Get signal quality.
    /// @param  None
    /// @return      \b  Signal quality.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetSignalQuality(void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal strength.
    /// @param  None
    /// @return      \b  Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetSignalStrength(void);

    //-------------------------------------------------------------------------------------------------
    /// Get cell ID.
    /// @return  MAPI_U16   \b OUT:Cell ID.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetCellID(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod to output serial or parallel.
    /// @param  bEnable \b IN: True : serial . False : parallel.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_Serial_Control( MAPI_BOOL bEnable);

    //-------------------------------------------------------------------------------------------------
    /// check if Hierarchy On.
    /// @return      \b TRUE: Hierarchy On.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_IsHierarchyOn( void);

#if (DVBT_SYSTEM_ENABLE == 1)
    //Public:DTV-DVB-T

    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBT) (DVBT2)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  eBandWidth     \b IN: eBandWidth.
    /// @param  bLPsel     \b IN: LP select.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth, MAPI_BOOL bPalBG, MAPI_BOOL bLPsel = MAPI_FALSE);

    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBT) (DVBT2)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_T_GetLockStatus(void);
    //Public:DTV-DVB-T2

    static MAPI_BOOL DTV_GetPlpBitMap(MAPI_U8* u8PlpBitMap);
    static MAPI_BOOL DTV_GetPlpGroupID(MAPI_U8 u8PlpID, MAPI_U8* u8GroupID);
    static MAPI_BOOL DTV_SetPlpGroupID(MAPI_U8 u8PlpID, MAPI_U8 u8GroupID);
#endif
#if (ISDB_SYSTEM_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.ISDB
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_ISDB_GetLockStatus(void);
#endif

    static MAPI_BOOL WriteDspReg(MAPI_U16 u16Addr, MAPI_U8 u8Data);
    static MAPI_BOOL ReadDspReg(MAPI_U16 u16Addr, MAPI_U8* pData);
#if (DVBC_SYSTEM_ENABLE == 1)
//Public:DTV-DVB-C

    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBC)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  eBandWidth     \b IN: BandWidth.
    /// @param  uSymRate     \b IN: Symbol rate.
    /// @param  eQAM     \b IN: QAM type.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_C_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth,MAPI_U32 uSymRate,mapi_demodulator_datatype::EN_CAB_CONSTEL_TYPE eQAM);

    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBC)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_C_GetLockStatus(void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal modulation mode.
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_CAB_CONSTEL_TYPE DTV_DVB_C_GetSignalModulation (void);


    //-------------------------------------------------------------------------------------------------
    /// Get current symbol rate.(DVBC)
    /// @param  None
    /// @return      \b Symbol rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_DVB_C_GetCurrentSymbolRate(void);

    //mick
    static MAPI_BOOL DTV_DVBC_DSPReg_Init();
    static MAPI_U16 DTV_DVB_C_Set_Config_dvbc_auto(MAPI_U8 bAutoDetect);
    static MAPI_U16 CCI_Check( COFDM_CHECK_FLAG eFlag );
    static MAPI_U16 SetDvbcParam(MAPI_U8 constel);
    static MAPI_U16 GetDvbcInfo( MAPI_U32 * DVBC_parameter);
    static MAPI_U16 MSB1231_Lock(E_SYSTEM system, COFDM_LOCK_STATUS eStatus );
    static MAPI_U16 Set_Config_dvbc_atv_detector(MAPI_U8 bEnable);
    static MAPI_U8 MSB1231_Config_DVBC(MAPI_U16 SymbolRate, MAPI_U32 u32IFFreq, MAPI_U16 bSpecInv);
#endif
#if (DVBS_SYSTEM_ENABLE == 1)
//________________________________________________________________________________________________________________________
    //Public:DTV-DVB-S
    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBS)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  uSymRate     \b IN: Symbol rate.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetFrequency(MAPI_U32 u32Frequency, MAPI_U32 u32SymbolBitrate);

    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBS)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_S_GetLockStatus(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Roll Off factor.(DVBS)
    /// @param  None
    /// @return      \b Roll off  factor.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_SAT_ROLL_OFF_TYPE DTV_DVB_S_GetRollOff(void);

    //-------------------------------------------------------------------------------------------------
    /// Set  Tone.(DVBS)
    /// @param  eTone  \b eTone1 eTone0
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetToneBurst(mapi_dish_datatype::EN_SAT_TONEBUREST_TYPE eTone);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bLow \b IN true:13v false:18v
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetLNBPower(mapi_dish_datatype::EN_SAT_LNBPOWER_TYPE eTone);

     //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bOn \b IN TRUE:22k on  FALSE:22k off
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_Set22KOnOff(MAPI_BOOL bOn);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bOn \b OUT TRUE:22k on  FALSE:22k off
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_Get22KStatus(MAPI_BOOL* bOn);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  pCmd    \ IN cmd
    /// @param  u8CmdSize   \IN cmd size
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SendDiSEqCCmd(MAPI_U8* pCmd,MAPI_U8 u8CmdSize);


    //-------------------------------------------------------------------------------------------------
    /// To init blind scan:switch to blind scan mode and set frequency range
    /// @param u16StartFreq         \b IN: start frequency
    /// @param u16EndFreq         \b IN: end frequency
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_Init(MAPI_U16 u16StartFreq,MAPI_U16 u16EndFreq);
    //-------------------------------------------------------------------------------------------------
    /// blind scan next frequencys
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_ScanNextFreq(void);
    //-------------------------------------------------------------------------------------------------
    /// get transponders found
    /// @param u8No         \b IN: transponders no
    /// @param u16Freq         \b OUT: TP frequency
    /// @param u16SymbolRate         \b OUT: TP symbol rate
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetFoundTP(MAPI_U8 u8No, MAPI_U16 &u16Freq,MAPI_U16 &u16SymbolRate);
    //-------------------------------------------------------------------------------------------------
    /// cancel current frequency scan
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_Cancel(void);
    //-------------------------------------------------------------------------------------------------
    /// end blind scan
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_End(void);
   //-------------------------------------------------------------------------------------------------
    /// get status
    /// @param eStatus         \b OUT:status
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetStatus(mapi_demodulator_datatype::EN_BLINDSCAN_STATUS &eStatus);
    ///-------------------------------------------------------------------------------------------------
    /// get current frequency  scanned currently
    /// @param u16Freq         \b OUT: requency
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetScanFreq(MAPI_U16 &u16Freq);
    static MAPI_BOOL MSB1240_DVBS_SetFrequency(MAPI_U16 u16CenterFreq_MHz, MAPI_U32 u32SymbolRate_Ks);
    static MAPI_BOOL MSB1240_DVBS_GetLock(void);
    static MAPI_BOOL MSB1240_DVBS_Demod_Restart(DEMOD_MS_FE_CARRIER_PARAM_MSB1240* pParam);
    static MAPI_BOOL MSB1240_DVBS_DiSEqC_SetTone(MAPI_BOOL bTone1);
    static MAPI_BOOL MSB1240_DVBS_DiSEqC_SendCmd(MAPI_U8* pCmd,MAPI_U8 u8CmdSize);
    static MAPI_BOOL MSB1240_DVBS_DiSEqC_Set22kOnOff(MAPI_BOOL b22kOn);
    static MAPI_BOOL MSB1240_DVBS_DiSEqC_Get22kOnOff(MAPI_BOOL* b22kOn);
#endif
#if (DTMB_SYSTEM_ENABLE == 1)
    //Public:DTV-DTMB
    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DTMB)
    /// @param  u32Freq                  \b IN: Tuner frequency
    /// @param  eBand                   \b IN: Channel bandwidth
    /// @param  eSubCarriers         \b IN: sub-carriers
    /// @param  ePN                      \b IN: PN padding
    /// @param  eMAPPING                \b IN: mapping type
    /// @param  eFEC                     \b IN: FEC code rate
    /// @param  s8RouteID               \b IN: RouteID
    /// @return                          \b TRUE: Set  success, FALSE: Set  failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DTMB_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth, mapi_demodulator_datatype::EN_DTMB_SUBCARRIERS eSubCarriers,
                    mapi_demodulator_datatype::EN_DTMB_PN_PADDING ePN, mapi_demodulator_datatype::EN_DTMB_MAPPING eMAPPING, mapi_demodulator_datatype::EN_DTMB_FEC_CODERATE eFEC );

    static MAPI_BOOL DTV_DTMB_GetProperity(MAPI_U32* u32Frequency, RF_CHANNEL_BANDWIDTH* eBandWidth, mapi_demodulator_datatype::EN_DTMB_SUBCARRIERS* eSubCarriers,
                        mapi_demodulator_datatype::EN_DTMB_PN_PADDING* ePN, mapi_demodulator_datatype::EN_DTMB_MAPPING* eMAPPING, mapi_demodulator_datatype::EN_DTMB_FEC_CODERATE* eFEC );



    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DTMB)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DTMB_GetLockStatus(void);
#endif
#if (ATSC_SYSTEM_ENABLE==1)
//Public:DTV-ATSC
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_ATSC_GetLockStatus(void);

    static MAPI_BOOL DTV_ATSC_ClkEnable(MAPI_BOOL bEnable);

    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE DTV_ATSC_GetModulationMode(void);

    static MAPI_BOOL DTV_ATSC_ChangeModulationMode(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE eMode);
#endif

#if (DVBT_SYSTEM_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// DTV_DVB_T2_Get_L1_Parameter
    /// @param  eSignalInfo
    /// @return      \b  pu16L1_parameter
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_T2_Get_L1_Parameter(MAPI_U16 * pu16L1_parameter, E_T2_SIGNAL_INFO eSignalInfo);

    //-------------------------------------------------------------------------------------------------
    /// Get TPS Parameters
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_T_Get_TPS_Parameter( MAPI_U16 * pu16TPS_parameter, E_SIGNAL_TYPE eSignalType);
    //-------------------------------------------------------------------------------------------------
    /// Get signal modulation mode.
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_CONSTEL_TYPE DTV_DVB_T_GetSignalModulation (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal guard Interval
    /// @param  None
    /// @return      \b  guard Interval
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_GUARD_INTERVAL DTV_DVB_T_GetSignalGuardInterval (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal FFT Value
    /// @param  None
    /// @return      \b  FFT Value
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_FFT_VAL DTV_DVB_T_GetSignalFFTValue (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal Code Rate
    /// @param  None
    /// @return      \b  Code Rate
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_CODE_RATE DTV_DVB_T_GetSignalCodeRate (void);
#endif

    static MAPI_BOOL msb1240_flash_check(void);
  
private:

    // ------------------------------------------------------------
    // Variable Define
    // ------------------------------------------------------------
    static MAPI_BOOL m_bSerialOut;
    static MAPI_U8 gVifTop;// = VIF_TOP;
    static MAPI_U8 gVifSoundSystem;// = VIF_SOUND_DK2;
    static MAPI_U8 gVifIfFreq;// = IF_FREQ_3890;
    static MAPI_U8 use_twin_demod;
//@@++
	static mapi_demodulator_datatype::IF_FREQ m_eIF_Freq ;//= mapi_demodulator_datatype::IF_FREQ_INVALID;

	static MAPI_U32 Tuner_TOP_Setting ;//= 0xFF;
	static MAPI_U32 Tuner_Top_Setting_LPrime ;//=0x05;
	static MAPI_U32 Tuner_Top_Setting_SECAM_VHF ;//=0x04;
	static MAPI_U32 Tuner_Top_Setting_SECAM_UHF ;//=0x03;
	static MAPI_U32 Tuner_Top_Setting_PAL_VHF ;//=0x02;
	static MAPI_U32 Tuner_Top_Setting_PAL_UHF ;//=0x01;

	static MAPI_U16 IfFreq ;//= 0;
	static MAPI_BOOL IFDM_Initialized ;// = FALSE;
	static MAPI_U8 Scanning_Active ;//=0;
//@@++
    static MAPI_U8 u8MsbData[6];
    static MAPI_U8 gu8ChipRevId;
    static MAPI_U8 gCalIdacCh0, gCalIdacCh1;
    static S_CMDPKTREG gsCmdPacket;
    static t2_mod_info st2_mod_info;                //@@++-- 20120405 Arki
    //static DEMOD_MS_FE_CARRIER_INFO demod_info;     //@@++-- 20120412 Arki
    static MAPI_U8 gu8DemoDynamicI2cAddress;//=0x32;//Default
    static MAPI_BOOL     FECLock;
    static MAPI_BOOL     gbTVAutoScanEn;//=FALSE;//init value=FALSE, follow with auto/manual scan
    static MAPI_BOOL     m_bDSPLoad;
    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE m_enCurrentDemodulator_Type;

    // ------------------------------------------------------------
    // private operations
    // ------------------------------------------------------------

    static MAPI_BOOL I2C_CH_Reset(MAPI_U8 ch_num);

    static MAPI_BOOL WriteReg(MAPI_U16 u16Addr, MAPI_U8 u8Data);

    static MAPI_BOOL ReadReg(MAPI_U16 u16Addr, MAPI_U8 *pu8Data);

    static MAPI_BOOL WriteRegs(MAPI_U16 u16Addr, MAPI_U8* u8pData, MAPI_U16 data_size);

    static MAPI_BOOL WriteReg2bytes(MAPI_U16 u16Addr, MAPI_U16 u16Data);

    static MAPI_BOOL Load2Sdram(MAPI_U8 *u8_ptr, MAPI_U16 data_length, MAPI_U16 sdram_win_offset_base);

    static MAPI_BOOL LoadSdram2Sram(MAPI_U8 CodeNum);
    static MAPI_BOOL MSB1233C_MEM_switch(MAPI_U8 mem_type);

    static MAPI_BOOL MSB123x_MEM_switch(MAPI_U8 mem_type);

    static MAPI_BOOL LoadDspCodeToSDRAM_Boot(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbt2(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbt(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbc(void);
    
    static MAPI_BOOL LoadDspCodeToSDRAM_dvbs(void);

    static MAPI_BOOL LoadDspCodeToSDRAM(MAPI_U8 code_n);

    static MAPI_U8 DTV_DVBT_DSPReg_CRC(void);

    static void DTV_DVBT_DSPReg_ReadBack(void);

    static MAPI_BOOL Cmd_Packet_Send(S_CMDPKTREG* pCmdPacket, MAPI_U8 param_cnt);

    static MAPI_BOOL Cmd_Packet_Exe_Check(MAPI_BOOL* cmd_done);

    static MAPI_BOOL LoadDSPCode(void);

    static MAPI_BOOL I2C_Address_Polling(void);

    static MAPI_BOOL ATV_VIF_SetHandler(MAPI_BOOL bAutoScan, MAPI_U16 GainDistributionThr);

    static MAPI_BOOL ATV_VIF_IfInitial(MAPI_U16 VifVgaMaximum);

    static MAPI_BOOL ATV_VIF_TopAdjust(MAPI_U8 ucVifTop);

    static MAPI_BOOL ATV_VIF_SoftReset();

    static MAPI_BOOL DTV_Config(RF_CHANNEL_BANDWIDTH BW, MAPI_BOOL bSerialTS, MAPI_BOOL bPalBG);

    static void Driving_Control(MAPI_BOOL bEnable);

    static mapi_demodulator_datatype::E_VIF_TYPE ATV_GetVIF_Type();

    static MAPI_BOOL MSB123x_HW_init(void);

    static MAPI_BOOL MSB1233C_HW_init(void);

    static MAPI_U16 MSB1233C_Lock(E_SYSTEM system, COFDM_LOCK_STATUS eStatus );

    static MAPI_U8 MSB1233C_Config_DVBC(MAPI_U16 SymbolRate, MAPI_U32 u32IFFreq, MAPI_U16 bSpecInv);

    static MAPI_BOOL MSB123xC_Set_bonding_option(MAPI_U16 u16ChipID);
    
    static MAPI_BOOL MSB123xC_HW_init(void);

    static MAPI_U16 MSB123xC_Lock(E_SYSTEM system, COFDM_LOCK_STATUS eStatus );

    static MAPI_U8 MSB123xC_Config_DVBC(MAPI_U16 SymbolRate, MAPI_U32 u32IFFreq, MAPI_U16 bSpecInv);

    static MAPI_BOOL MSB123xC_MEM_switch(MAPI_U8 mem_type);
    
    static MAPI_BOOL Turn_Off_ALL_Pad_In(MAPI_BOOL b_en);
        
    static MAPI_BOOL DTV_DVB_HW_init(void);
     
    static MAPI_BOOL MSB1240_MEM_switch(MAPI_U8 mem_type);

    static MAPI_BOOL WriteReg_ns(MAPI_U16 u16Addr, MAPI_U8 u8Data);

    static MAPI_BOOL ReadReg_ns(MAPI_U16 u16Addr, MAPI_U8 *pu8Data);

    static MAPI_BOOL I2C_CH_Reset_ns(MAPI_U8 ch_num);		

    static MAPI_BOOL MSPI_PAD_Enable(MAPI_U8 u8TSIndex, MAPI_BOOL bOnOff);

#if (DVBS_SYSTEM_ENABLE == 1)
    static MAPI_BOOL MSB1240_Demod_BlindScan_Start(MAPI_U16 u16StartFreq,MAPI_U16 u16EndFreq);

    static MAPI_BOOL MSB1240_Demod_BlindScan_NextFreq(MAPI_BOOL* bBlindScanEnd);

    static MAPI_BOOL MSB1240_Demod_BlindScan_WaitCurFreqFinished(MAPI_U8* u8Progress,MAPI_U8 *u8FindNum);

    static MAPI_BOOL MSB1240_Demod_BlindScan_Cancel(void);

    static MAPI_BOOL MSB1240_Demod_BlindScan_End(void);

    static MAPI_BOOL MSB1240_Demod_BlindScan_GetCurrentFreq(MAPI_U16 *u16CurrentFeq);
#endif
};
#endif


/*@ </Include> @*/

#if 0
class device_demodulator_extend2 : public mapi_base
{
public:

    // ------------------------------------------------------------
    // public Enum Define
    // ------------------------------------------------------------

    static MAPI_BOOL DTV_DVBT_DSPReg_Init();

    static MAPI_U8 u8DeviceBusy;

    // ------------------------------------------------------------
    // public operations
    // ------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------
    /// Connect function point to functions.
    /// @param  None
    /// @return                 \b TRUE: Connect functions success, FALSE: Connect functions failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DeviceDemodCreate(void);

    //-------------------------------------------------------------------------------------------------
    /// Connect to demod & set demod type.
    /// @param  enDemodType \b IN: Demod type.
    /// @return                 \b TRUE: Connecting and setting success, FALSE: Connecting failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Connect(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE enDemodType);

    //-------------------------------------------------------------------------------------------------
    /// Disconnect from demod.
    /// @param  None
    /// @return                 \b TRUE: Disconnect success, FALSE: Disconnect failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Disconnect(void);

    //-------------------------------------------------------------------------------------------------
    /// Reset demod status and state.
    /// @param  None
    /// @return  None
    //-------------------------------------------------------------------------------------------------
    static void Reset();

    //-------------------------------------------------------------------------------------------------
    /// Set demod to bypass iic ,so controller can communcate with tuner.
    /// @param  enable       \b IN: bypass or not.
    /// @return                 \b TRUE: Bypass setting success, FALSE: Bypass setting done failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL IIC_Bypass_Mode(MAPI_BOOL enable);

#if (STR_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// init demod. of STR resume from suspend
    /// @param  None
    /// @return                 \b TRUE: Init success, FALSE: Init failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL ResumeInit(void);
#endif
    //-------------------------------------------------------------------------------------------------
    /// Init demod.
    /// @param  None
    /// @return                 \b TRUE: Init success, FALSE: Init failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Power_On_Initialization(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod power on.
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Set_PowerOn(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod power off.
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL Set_PowerOff(void);

    //-------------------------------------------------------------------------------------------------
    /// Trigger demod to active or not.
    /// @param  bEnable     \b IN: Enbale or not.
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------

    static MAPI_BOOL  DTV_DVBT2_DSPReg_Init(void);
    //-------------------------------------------------------------------------------------------------
    /// Init Demod DSP reg
    /// @param  None
    /// @return                 \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------

    static MAPI_BOOL Active(MAPI_BOOL bEnable);

    //-------------------------------------------------------------------------------------------------
    /// Get current demodulator type.
    /// @param  None
    /// @return  EN_DEVICE_DEMOD_TYPE     \b Current demodulator type.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE GetCurrentDemodulatorType(void);

    //-------------------------------------------------------------------------------------------------
    /// Get current demodulator type.
    /// @param  enDemodType \b IN: demodulator type
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL SetCurrentDemodulatorType(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE enDemodType);

    //-------------------------------------------------------------------------------------------------
    /// For user defined command.
    /// @param  SubCmd     \b IN: sub command.
    /// @param  u32Param1     \b IN: command parameter1.
    /// @param  u32Param2     \b IN: command parameter2.
    /// @param  pvParam3     \b IN: command parameter3 point.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL ExtendCmd(MAPI_U8 SubCmd, MAPI_U32 u32Param1, MAPI_U32 u32Param2, void *pvParam3);
    static void* PreLoadDSPcode(void *arg);

//Public:ATV
    static mapi_demodulator_datatype::AFC  ATV_GetAFC_Distance(void);

    static MAPI_BOOL ATV_SetVIF_SoundSystem(mapi_demodulator_datatype::DEMOD_AUDIOSTANDARD_TYPE_ eIF_Freq);

    static MAPI_BOOL ATV_SetAudioSawFilter(MAPI_U8 u8SawFilterMode);

    static MAPI_BOOL ATV_SetVIF_IfFreq(MAPI_U16 u16IfFreq);

    static MAPI_BOOL ATV_VIF_Init(void);

    static MAPI_BOOL ATV_VIF_Handler(MAPI_BOOL);

    //Public:DTV


    //-------------------------------------------------------------------------------------------------
    /// Get signal SNR.
    /// @param  None
    /// @return      \b  Signal condition.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_FRONTEND_SIGNAL_CONDITION DTV_GetSNR(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Packet error of DVBT and DVBC.
    /// @param  SNR
    /// @return      \b  SNR.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_GetSNR_fo(float *f_snr);

    //-------------------------------------------------------------------------------------------------
    /// Get signal BER.
    /// @param  None
    /// @return      \b  Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U32 DTV_GetBER(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Pre Viterbi BER of DVBT.
    /// @param  None
    /// @return      \b  Pre Viterbi Bit error rate.
    //-------------------------------------------------------------------------------------------------
		static MAPI_BOOL DTV_GetPreBER(float *p_preBer);

    //-------------------------------------------------------------------------------------------------
    /// Get Post Viterbi BER of DVBT and DVBC.
    /// @param  None
    /// @return      \b  Post Viterbi Bit error rate.
    //-------------------------------------------------------------------------------------------------
		static MAPI_BOOL DTV_GetPostBER(float *p_postBer);

    //-------------------------------------------------------------------------------------------------
    /// Get Packet error of DVBT and DVBC.
    /// @param  None
    /// @return      \b  Packet error.
    //-------------------------------------------------------------------------------------------------
		static MAPI_BOOL DTV_GetPacketErr(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Packet error of DVBT and DVBC.
    /// @param  None
    /// @return      \b  Packet error.
    //-------------------------------------------------------------------------------------------------
		static MAPI_BOOL DTV_GetPacketErr(MAPI_U16  *pu16BitErr);

     //-------------------------------------------------------------------------------------------------
    /// Get signal quality.
    /// @param  None
    /// @return      \b  Signal quality.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetSignalQuality(void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal strength.
    /// @param  None
    /// @return      \b  Bit error rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetSignalStrength(void);

    //-------------------------------------------------------------------------------------------------
    /// Get cell ID.
    /// @return  MAPI_U16   \b OUT:Cell ID.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_GetCellID(void);

    //-------------------------------------------------------------------------------------------------
    /// Set demod to output serial or parallel.
    /// @param  bEnable \b IN: True : serial . False : parallel.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_Serial_Control( MAPI_BOOL bEnable);

    //-------------------------------------------------------------------------------------------------
    /// check if Hierarchy On.
    /// @return      \b TRUE: Hierarchy On.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_IsHierarchyOn( void);

#if (DVBT_SYSTEM_ENABLE == 1)
    //Public:DTV-DVB-T

    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBT) (DVBT2)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  eBandWidth     \b IN: eBandWidth.
    /// @param  bLPsel     \b IN: LP select.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth, MAPI_BOOL bPalBG, MAPI_BOOL bLPsel = MAPI_FALSE);

    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBT) (DVBT2)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_T_GetLockStatus(void);
    //Public:DTV-DVB-T2

    static MAPI_BOOL DTV_GetPlpBitMap(MAPI_U8* u8PlpBitMap);
    static MAPI_BOOL DTV_GetPlpGroupID(MAPI_U8 u8PlpID, MAPI_U8* u8GroupID);
    static MAPI_BOOL DTV_SetPlpGroupID(MAPI_U8 u8PlpID, MAPI_U8 u8GroupID);
#endif
#if (ISDB_SYSTEM_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.ISDB
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_ISDB_GetLockStatus(void);
#endif

    static MAPI_BOOL WriteDspReg(MAPI_U16 u16Addr, MAPI_U8 u8Data);
    static MAPI_BOOL ReadDspReg(MAPI_U16 u16Addr, MAPI_U8* pData);
#if (DVBC_SYSTEM_ENABLE == 1)
//Public:DTV-DVB-C

    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBC)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  eBandWidth     \b IN: BandWidth.
    /// @param  uSymRate     \b IN: Symbol rate.
    /// @param  eQAM     \b IN: QAM type.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_C_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth,MAPI_U32 uSymRate,mapi_demodulator_datatype::EN_CAB_CONSTEL_TYPE eQAM);

    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBC)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_C_GetLockStatus(void);

    //-------------------------------------------------------------------------------------------------
    /// Get current symbol rate.(DVBC)
    /// @param  None
    /// @return      \b Symbol rate.
    //-------------------------------------------------------------------------------------------------
    static MAPI_U16 DTV_DVB_C_GetCurrentSymbolRate(void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal modulation mode.
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_CAB_CONSTEL_TYPE DTV_DVB_C_GetSignalModulation (void);
#endif

#if (DVBS_SYSTEM_ENABLE == 1)
    //Public:DTV-DVB-S
    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DVBS)
    /// @param  u32Frequency     \b IN: Frequency.
    /// @param  uSymRate     \b IN: Symbol rate.
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetFrequency(MAPI_U32 u32Frequency, MAPI_U32 u32SymbolBitrate);

    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DVBS)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DVB_S_GetLockStatus(void);

    //-------------------------------------------------------------------------------------------------
    /// Get Roll Off factor.(DVBS)
    /// @param  None
    /// @return      \b Roll off  factor.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_SAT_ROLL_OFF_TYPE DTV_DVB_S_GetRollOff(void);

    //-------------------------------------------------------------------------------------------------
    /// Set  Tone.(DVBS)
    /// @param  eTone  \b eTone1 eTone0
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetToneBurst(mapi_dish_datatype::EN_SAT_TONEBUREST_TYPE eTone);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bLow \b IN true:13v false:18v
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SetLNBPower(mapi_dish_datatype::EN_SAT_LNBPOWER_TYPE eTone);

     //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bOn \b IN TRUE:22k on  FALSE:22k off
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_Set22KOnOff(MAPI_BOOL bOn);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  bOn \b OUT TRUE:22k on  FALSE:22k off
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_Get22KStatus(MAPI_BOOL* bOn);

    //-------------------------------------------------------------------------------------------------
    /// Set  LNB Power.(DVBS)
    /// @param  pCmd    \ IN cmd
    /// @param  u8CmdSize   \IN cmd size
    /// @return      \b TRUE: Set success, FALSE: Set failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_SendDiSEqCCmd(MAPI_U8* pCmd,MAPI_U8 u8CmdSize);


    //-------------------------------------------------------------------------------------------------
    /// To init blind scan:switch to blind scan mode and set frequency range
    /// @param u16StartFreq         \b IN: start frequency
    /// @param u16EndFreq         \b IN: end frequency
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_Init(MAPI_U16 u16StartFreq,MAPI_U16 u16EndFreq);
    //-------------------------------------------------------------------------------------------------
    /// blind scan next frequencys
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_ScanNextFreq(void);
    //-------------------------------------------------------------------------------------------------
    /// get transponders found
    /// @param u8No         \b IN: transponders no
    /// @param u16Freq         \b OUT: TP frequency
    /// @param u16SymbolRate         \b OUT: TP symbol rate
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetFoundTP(MAPI_U8 u8No, MAPI_U16 &u16Freq,MAPI_U16 &u16SymbolRate);
    //-------------------------------------------------------------------------------------------------
    /// cancel current frequency scan
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_Cancel(void);
    //-------------------------------------------------------------------------------------------------
    /// end blind scan
    /// @param none         \b IN
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_End(void);
   //-------------------------------------------------------------------------------------------------
    /// get status
    /// @param eStatus         \b OUT:status
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetStatus(mapi_demodulator_datatype::EN_BLINDSCAN_STATUS &eStatus);
    ///-------------------------------------------------------------------------------------------------
    /// get current frequency  scanned currently
    /// @param u16Freq         \b OUT: requency
    /// @return             \b OUT: BOOL
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_S_BlindScan_GetScanFreq(MAPI_U16 &u16Freq);
#endif
#if (DTMB_SYSTEM_ENABLE == 1)
    //Public:DTV-DTMB
    //-------------------------------------------------------------------------------------------------
    /// Config demod.(DTMB)
    /// @param  u32Freq                  \b IN: Tuner frequency
    /// @param  eBand                   \b IN: Channel bandwidth
    /// @param  eSubCarriers         \b IN: sub-carriers
    /// @param  ePN                      \b IN: PN padding
    /// @param  eMAPPING                \b IN: mapping type
    /// @param  eFEC                     \b IN: FEC code rate
    /// @param  s8RouteID               \b IN: RouteID
    /// @return                          \b TRUE: Set  success, FALSE: Set  failure.
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DTMB_SetFrequency(MAPI_U32 u32Frequency, RF_CHANNEL_BANDWIDTH eBandWidth, mapi_demodulator_datatype::EN_DTMB_SUBCARRIERS eSubCarriers,
                    mapi_demodulator_datatype::EN_DTMB_PN_PADDING ePN, mapi_demodulator_datatype::EN_DTMB_MAPPING eMAPPING, mapi_demodulator_datatype::EN_DTMB_FEC_CODERATE eFEC );

    static MAPI_BOOL DTV_DTMB_GetProperity(MAPI_U32* u32Frequency, RF_CHANNEL_BANDWIDTH* eBandWidth, mapi_demodulator_datatype::EN_DTMB_SUBCARRIERS* eSubCarriers,
                        mapi_demodulator_datatype::EN_DTMB_PN_PADDING* ePN, mapi_demodulator_datatype::EN_DTMB_MAPPING* eMAPPING, mapi_demodulator_datatype::EN_DTMB_FEC_CODERATE* eFEC );



    //-------------------------------------------------------------------------------------------------
    /// Get Lock Status.(DTMB)
    /// @param  None
    /// @return      \b Lock Status.
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_DTMB_GetLockStatus(void);
#endif
#if (ATSC_SYSTEM_ENABLE==1)
//Public:DTV-ATSC
    static mapi_demodulator_datatype::EN_LOCK_STATUS DTV_ATSC_GetLockStatus(void);

    static MAPI_BOOL DTV_ATSC_ClkEnable(MAPI_BOOL bEnable);

    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE DTV_ATSC_GetModulationMode(void);

    static MAPI_BOOL DTV_ATSC_ChangeModulationMode(mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE eMode);
#endif
#if (DVBT_SYSTEM_ENABLE == 1)
    //-------------------------------------------------------------------------------------------------
    /// Get TPS Parameters
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static MAPI_BOOL DTV_DVB_T_Get_TPS_Parameter( MAPI_U16 * pu16TPS_parameter, E_SIGNAL_TYPE eSignalType);
    //-------------------------------------------------------------------------------------------------
    /// Get signal modulation mode.
    /// @param  None
    /// @return      \b  Modulation Mode
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_CONSTEL_TYPE DTV_DVB_T_GetSignalModulation (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal guard Interval
    /// @param  None
    /// @return      \b  guard Interval
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_GUARD_INTERVAL DTV_DVB_T_GetSignalGuardInterval (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal FFT Value
    /// @param  None
    /// @return      \b  FFT Value
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_FFT_VAL DTV_DVB_T_GetSignalFFTValue (void);

    //-------------------------------------------------------------------------------------------------
    /// Get signal Code Rate
    /// @param  None
    /// @return      \b  Code Rate
    //-------------------------------------------------------------------------------------------------
    static mapi_demodulator_datatype::EN_DVBT_CODE_RATE DTV_DVB_T_GetSignalCodeRate (void);
#endif
private:

    // ------------------------------------------------------------
    // Variable Define
    // ------------------------------------------------------------
    static MAPI_BOOL m_bSerialOut;
    static MAPI_U8 gVifTop;// = VIF_TOP;
    static MAPI_U8 gVifSoundSystem;// = VIF_SOUND_DK2;
    static MAPI_U8 gVifIfFreq;// = IF_FREQ_3890;
    static MAPI_U8 u8MsbData[6];
    static MAPI_U8 gu8ChipRevId;
    static MAPI_U8 gCalIdacCh0, gCalIdacCh1;
    static S_CMDPKTREG gsCmdPacket;
    static MAPI_U8 gu8DemoDynamicI2cAddress;//=0x32;//Default
    static MAPI_BOOL     FECLock;
    static MAPI_BOOL     gbTVAutoScanEn;//=FALSE;//init value=FALSE, follow with auto/manual scan
    static MAPI_BOOL     m_bDSPLoad;
    static mapi_demodulator_datatype::EN_DEVICE_DEMOD_TYPE m_enCurrentDemodulator_Type;

    // ------------------------------------------------------------
    // private operations
    // ------------------------------------------------------------

    static MAPI_BOOL I2C_CH_Reset(MAPI_U8 ch_num);

    static MAPI_BOOL WriteReg(MAPI_U16 u16Addr, MAPI_U8 u8Data);

    static MAPI_BOOL ReadReg(MAPI_U16 u16Addr, MAPI_U8 *pu8Data);

    static MAPI_BOOL WriteRegs(MAPI_U16 u16Addr, MAPI_U8* u8pData, MAPI_U16 data_size);

    static MAPI_BOOL WriteReg2bytes(MAPI_U16 u16Addr, MAPI_U16 u16Data);

    static MAPI_BOOL Load2Sdram(MAPI_U8 *u8_ptr, MAPI_U16 data_length, MAPI_U16 sdram_win_offset_base);

    static MAPI_BOOL LoadSdram2Sram(MAPI_U8 CodeNum);

    static MAPI_BOOL MSB123x_MEM_switch(MAPI_U8 mem_type);

    static MAPI_BOOL LoadDspCodeToSDRAM_Boot(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbt2(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbt(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbc(void);

    static MAPI_BOOL LoadDspCodeToSDRAM_dvbs(void);

    static MAPI_BOOL LoadDspCodeToSDRAM(MAPI_U8 code_n);

    static MAPI_U8 DTV_DVBT_DSPReg_CRC(void);

    static void DTV_DVBT_DSPReg_ReadBack(void);

    static MAPI_BOOL Cmd_Packet_Send(S_CMDPKTREG* pCmdPacket, MAPI_U8 param_cnt);

    static MAPI_BOOL Cmd_Packet_Exe_Check(MAPI_BOOL* cmd_done);

    static MAPI_BOOL LoadDSPCode(void);

    static MAPI_BOOL I2C_Address_Polling(void);

    static MAPI_BOOL ATV_VIF_SetHandler(MAPI_BOOL bAutoScan, MAPI_U16 GainDistributionThr);

    static MAPI_BOOL ATV_VIF_IfInitial(MAPI_U16 VifVgaMaximum);

    static MAPI_BOOL ATV_VIF_TopAdjust(MAPI_U8 ucVifTop);

    static MAPI_BOOL ATV_VIF_SoftReset();

    static MAPI_BOOL DTV_Config(RF_CHANNEL_BANDWIDTH BW, MAPI_BOOL bSerialTS, MAPI_BOOL bPalBG);

    static void Driving_Control(MAPI_BOOL bEnable);

    static mapi_demodulator_datatype::E_VIF_TYPE ATV_GetVIF_Type();

    static MAPI_BOOL MSB123x_HW_init(void);

};
#endif
/*@ </Definitions> @*/


/*@ <IncludeGuardEnd> @*/
#endif // mapi_demodulator_msb1210_I2b28dd03m121c8cf959bmm7254_H
/*@ </IncludeGuardEnd> @*/
