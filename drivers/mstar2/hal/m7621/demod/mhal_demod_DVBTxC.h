////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2008 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (!��MStar Confidential Information!�L) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _MHAL_DEMOD_DVBTXC_H_
#define _MHAL_DEMOD_DVBTXC_H_

#ifdef _MHAL_DEMOD_DVBTXC_C_
#define EXTSEL
#else
#define EXTSEL extern
#endif


//--------------------------------------------------------------------
typedef enum
{
    E_SYS_UNKOWN = -1,
    E_SYS_DVBT,
    E_SYS_DVBC,
    E_SYS_ATSC,
    E_SYS_VIF,
    E_SYS_DVBT2,
    E_SYS_DVBS,
    E_SYS_DVBT_T2,
    E_SYS_NUM
}E_SYSTEM;

typedef enum
{
    // fw version, check sum
    T2_CHECK_SUM_L      = 0x00,
    T2_CHECK_SUM_H,
    T2_FW_VER_0,
    T2_FW_VER_1,
    T2_FW_VER_2,
    T2_FPGA_CONFIG,  //0x05
    T2_DRAM_ADDR_0, //0x06
    T2_DRAM_ADDR_1,
    T2_DRAM_ADDR_2,
    T2_DRAM_ADDR_3,

    // operation mode
    T2_ZIF_EN           = 0x20,
    T2_RF_AGC_EN,
    T2_HUM_DET_EN,
    T2_DCR_EN,
    T2_IQB_EN,
    T2_IIS_EN, 
    T2_CCI_EN,
    T2_LOW_PWR_DET_EN,
    T2_ACI_DET_EN,
    E_T2_Windowing_EN,
    E_T2_FRC_Short_T2_EN,
    E_T2_FDSA_one_shot_EN,	
    E_T2_FDSA_tracking_EN,
    E_T2_Excess_no_image_EN,
    E_T2_FRC_GI_EN,	

    // channel tuning param
    T2_BW               = 0x40,
    T2_FC_L             = 0x41,
    T2_FC_H             = 0x42,
    T2_FS_L,
    T2_FS_H,
    T2_ZIF,
    T2_GI,
    T2_ACI_DET_TYPE,
    T2_AGC_REF,			//0x48
    T2_RSSI_REF,
    T2_SNR_TIME_L,
    T2_SNR_TIME_H,
    T2_BER_CMP_TIME_L,
    T2_BER_CMP_TIME_H,
    T2_SFO_CFO_NUM,
    T2_CCI,
    T2_ACI_DET_TH_L,	//0x50
    T2_ACI_DET_TH_H,
    T2_TS_SERIAL = 0x52,
    T2_TS_CLK_RATE = 0x53,
    T2_TS_OUT_INV = 0x54,
    T2_TS_DATA_SWAP = 0x55,
    T2_TDP_CCI_KP,
    T2_CCI_FSWEEP,          //0x57
    T2_TS_ERR_POL,		//0x58
    T2_IF_AGC_INV_PWM_EN, // 0x59
    T2_CCI_TYPE,		       //0x5A
    T2_LITE,                       //0x5B
    
    // AGC	
    E_T2_AGC_K = 0x60,
    E_T2_AGC_RSSI_K,
    E_T2_AGC_LOCK_TH,				//0x62
    E_T2_AGC_LOCK_NUM,
    E_T2_AGC_CNT_CYCLE,
    E_T2_FE_AGC_LOCK,

    // Mixer
    E_T2_FE_MIX_FC_FS_0,				//0x66
    E_T2_FE_MIX_FC_FS_1,
    E_T2_FE_MIX_FC_FS_2,
    E_T2_FE_MIX_FC_FS_3,

    // IQSWAP	
    E_T2_FE_IQB_SET,					//0x6A

    // CCI
    E_T2_Flag_CI,					//0x6B
    E_T2_FEEXT_CCI_LOCK_DET,
    E_T2_FEEXT_CCI_FREQN_0,
    E_T2_FEEXT_CCI_FREQN_1,
    E_T2_FEEXT_CCI_FREQN_2,      //0x6F

    // ACI
    E_T2_ACI_ANM1_DET_TH_L,	
    E_T2_ACI_ANM1_DET_TH_H,
    E_T2_ACI_ANP1_DET_TH_L,
    E_T2_ACI_ANP1_DET_TH_H,

    // P1
    E_T2TDP_TYPE1_TH_1,			//0x74
    E_T2TDP_TYPE1_TH_2,
    E_T2TDP_TYPE1_TH_3,
    E_T2TDP_TYPE1_TH_4,
    E_T2TDP_TYPE2_TH_1,
    E_T2TDP_TYPE2_TH_2,
    E_T2TDP_TYPE2_TH_3,
    E_T2TDP_TYPE2_TH_4,
    E_T2TDP_TDP_R_0,                    //0x7C
    E_T2TDP_TDP_R_1,

    // pilot pattern	
    E_T2FDP_PPX_EXT,				//0x7E

    // Interpolator
    E_T2TDP_SEQ_FOE_NUM,           //0x7F
    E_FE_INTP_RATEM1_0,
    E_FE_INTP_RATEM1_1,
    E_FE_INTP_RATEM1_2,
    E_FE_INTP_RATEM1_3,

    // ICFO
    E_T2FDP_ICFO_R_VAL,         //0x84
    E_T2_TOTAL_CFO_0,
    E_T2_TOTAL_CFO_1,
    E_T2_TOTAL_CFO_2,
    E_T2_TOTAL_CFO_3, 

    // EQ of SDRAM arrangement
    E_T2EQ_START_ADDR_0 = 0x90,
    E_T2EQ_START_ADDR_1,
    E_T2EQ_START_ADDR_2,
    E_T2EQ_START_ADDR_3,

    // TDI of SDRAM arrangement
    E_T2TDI_START_ADDR_0,
    E_T2TDI_START_ADDR_1,
    E_T2TDI_START_ADDR_2,
    E_T2TDI_START_ADDR_3,

    // DJB of SDRAM arrangement
    E_T2DJB_START_ADDR_0,
    E_T2DJB_START_ADDR_1,
    E_T2DJB_START_ADDR_2,
    E_T2DJB_START_ADDR_3,

    // DJB of SDRAM arrangement
    E_T2FW_START_ADDR_0,
    E_T2FW_START_ADDR_1,
    E_T2FW_START_ADDR_2,
    E_T2FW_START_ADDR_3,

    // dvbt2 lock history
    T2_DVBT2_LOCK_HIS   = 0xF0,
    T2_FEF_DET_IND,
    T2_MPLP_NO_COMMON_IND,
    T2_SNR_L,             // 0xf3
    T2_SNR_H,             // 0xf4
    T2_DOPPLER_DET_FLAG,  // 0xf5
    T2_DOPPLER_DET_TH_L,  // 0xf6
    T2_DOPPLER_DET_TH_H,  // 0xf7

    // splp, mplp releted
    T2_PLP_ID_ARR       = 0x100,
    T2_L1_FLAG          = 0x120,
    T2_PLP_ID,
    T2_GROUP_ID,
    T2_Channel_Switch,
    T_T2_UNLOCK_FLAG_0,
    T_T2_UNLOCK_FLAG_1,
    T_T2_UNLOCK_FLAG_2,

    T2_TS_DATA_RATE_0       = 0x130,
    T2_TS_DATA_RATE_1       = 0x131,
    T2_TS_DATA_RATE_2       = 0x132,
    T2_TS_DATA_RATE_3       = 0x133,
    T2_TS_DATA_RATE_CHANGE_IND       = 0x134,

    T2_SAVE_LOCKING_TIME_SNR_TH_FLAG = 0x200,	//0x200
	T2_SAVE_LOCKING_TIME_SNR_H,		//0x201
	T2_SAVE_LOCKING_TIME_SNR_L,		//0x202
	T2_PLP_MATCH_FLAG,				//0x203
//    T2_AWGN_case,					//0x204
    
    T2_PKT_ACCU_0    = 0x210,
    T2_PKT_ACCU_1,   //0x211
    T2_PKT_ACCU_2,   //0x212
    T2_PKT_ACCU_3,   //0x213
    T2_PKT_READ_CTRL,  //0x214

    T2_S9_DEBUG_INFO = 0x280,		//0x280
    T2_S9_RESET_DJB_INFO,			//0x281
    T2_S9_RESET_FDI_INFO_L,			//0x282
	T2_S9_RESET_FDI_INFO_H, 		//0x283
	T2_S9_RESET_TO_S0_INFO,			//0x284

    T2_ONESHOT_max1_echo_h_L = 0x2A0,   //0x2A0    
    T2_ONESHOT_max1_echo_h_H,    
    T2_ONESHOT_max2_echo_h_L,   
    T2_ONESHOT_max2_echo_h_H,        
    T2_ONESHOT_max3_echo_h_L,
    T2_ONESHOT_max3_echo_h_H,    
    T2_ONESHOT_max1_echo_idx_L,  
    T2_ONESHOT_max1_echo_idx_H,       
    T2_ONESHOT_max2_echo_idx_L,    
    T2_ONESHOT_max2_echo_idx_H,       
    T2_ONESHOT_max3_echo_idx_L,
    T2_ONESHOT_max3_echo_idx_H,
    T2_ONESHOT_L1stIndex_L, //0x2AC    
    T2_ONESHOT_L1stIndex_H,   
    T2_ONESHOT_L2ndIndex_L,
    T2_ONESHOT_L2ndIndex_H,
    T2_ONESHOT_R1stIndex_L,
    T2_ONESHOT_R1stIndex_H,
    T2_ONESHOT_R2ndIndex_L,  
    T2_ONESHOT_R2ndIndex_H,  
    T2_ONESHOT_T2FDP_FDSA_DATA_START_L, //0x2B4
    T2_ONESHOT_T2FDP_FDSA_DATA_START_H,
    T2_ONESHOT_T2FDP_FDSA_DATA_END_L,
    T2_ONESHOT_T2FDP_FDSA_DATA_END_H,
    T2_ONESHOT_ReadEchoNum_L,
    T2_ONESHOT_ReadEchoNum_H,
    T2_ONESHOT_EchoNum,   
    T2_ONESHOT_EchoLen_L,
    T2_ONESHOT_EchoLen_H,    
    T2_ONESHOT_EchoLenCircular_L,
    T2_ONESHOT_EchoLenCircular_H,
    T2_ONESHOT_TestEchoCircular,
    T2_ONESHOT_EchoCircular,
    T2_ONESHOT_Pre_Post_echo,
    T2_ONESHOT_En_echo_cancel,
    T2_ONESHOT_EchoProfileSel,
    T2_ONESHOT_Flag_7M_486p1us, //0x2C4
    T2_ONESHOT_Flag_7M_486p1us_Check,
    T2_ONESHOT_GI_LENGTH_L,
    T2_ONESHOT_GI_LENGTH_H,        
    T2_ONESHOT_ForceCShift_L,
    T2_ONESHOT_ForceCShift_H,    
    T2_ONESHOT_ForceBShift_L,
    T2_ONESHOT_ForceBShift_H,    
    T2_ONESHOT_ForceChOffset_L,
    T2_ONESHOT_ForceChOffset_H,
    T2_ONESHOT_Force_Indicator, //0x2CE
    T2_ONESHOT_Flag_8M_200p1us, 
    T2_ONESHOT_Flag_8M_240p1us, 

    T2_MainCenter_0	= 0x301,
    T2_MainCenter_1, 	            //0x302
    T2_MainCenter_2,	            //0x303
    T2_MainCenter_3,	            //0x304
    T2_MainLeft1st_0,	            //0x305
    T2_MainLeft1st_1,	            //0x306
    T2_MainLeft1st_2,	            //0x307
    T2_MainLeft1st_3,	            //0x308
    T2_MainLeft2nd_0,              //0x309
    T2_MainLeft2nd_1,   		//0x30A
    T2_MainLeft2nd_2,   		//0x30B
    T2_MainLeft2nd_3,   		//0x30C
    T2_MainRight1st_0,      		//0x30D
    T2_MainRight1st_1,        	//0x30E
    T2_MainRight1st_2,         	//0x30F
    T2_MainRight1st_3,        	//0x310
    T2_MainRight2nd_0,          	//0x311
    T2_MainRight2nd_1,		//0x312
    T2_MainRight2nd_2,    		//0x313
    T2_MainRight2nd_3,    		//0x314
    T2_MainCenterMin_0,		//0x315
    T2_MainCenterMin_1,		//0x316
    T2_MainCenterMin_2,     	//0x317
    T2_MainCenterMin_3,		//0x318
    T2_CompareSNR_history,	//0x319 
    T2_MinCase,             		//0x31A
    T2_CompareSNR_case,		//0x31B
    T2_MinCPValue_H,    		//0x31C
    T2_MinCPValue_L,    		//0x31D
    T2_MainBPF_0,       		//0x31E
    T2_MainBPF_1,       		//0x31F
    T2_MainBPF_2,       		//0x320
    T2_MainBPF_3,               	//0x321
    T2_CompareSNR_debug,    //0x322
    
    T2_DAGC0_Locking_Time_H = 0x323, 	//0x323
    T2_DAGC0_Locking_Time_L,	//0x324
    T2_DCR_Locking_Time_H,		//0x325
    T2_DCR_Locking_Time_L,		//0x326
    T2_DAGC1_Locking_Time_H,	//0x327
    T2_DAGC1_Locking_Time_L,	//0x328
    T2_DAGC2_Locking_Time_H,	//0x329
    T2_DAGC2_Locking_Time_L,	//0x32A
    T2_P1_First_Locking_Time_H,	//0x32B
    T2_P1_First_Locking_Time_L,	//0x32C
    T2_P1_Locking_Time_H,		//0x32D
    T2_P1_Locking_Time_L,		//0x32E
    T2_STATE6_Locking_Time_H,	//0x32F
    T2_STATE6_Locking_Time_L,	//0x330
    T2_STATE7_Locking_Time_H,	//0x331
    T2_STATE7_Locking_Time_L,	//0x332
    T2_GI_Locking_Time_H,		//0x333
    T2_GI_Locking_Time_L,		//0x334
    T2_SYMALI_Locking_Time_H,	//0x335
    T2_SYMALI_Locking_Time_L,	//0x336
    T2_TDCFOSFO_Locking_Time_H,	//0x337
    T2_TDCFOSFO_Locking_Time_L,	//0x338
    T2_ICFO_Locking_Time_H,		//0x339
    T2_ICFO_Locking_Time_L, 	//0x33A
    T2_OneShot_Locking_Time_H,	//0x33B
    T2_OneShot_Locking_Time_L,  //0x33C
    T2_STATE8_Locking_Time_H,	//0x33D
    T2_STATE8_Locking_Time_L,	//0x33E
    T2_Total_Locking_Time_H,	//0x33F
    T2_Total_Locking_Time_L, 	//0x340
    T2_start_Locking_Time_H,		//0x341
    T2_start_Locking_Time_L,		//0x342
    T2_end_Locking_Time_H,		//0x343
    T2_end_Locking_Time_L,		//0x344
    T2_CompareSNR_Locking_Time_H, //0x345
    T2_CompareSNR_Locking_Time_L, //0x346
    T2_STABLE_Locking_Time_H,	//0x347
    T2_STABLE_Locking_Time_L,	//0x348
    T2_STABLE_Timeout_Locking_Time_H,	//0x349
    T2_STABLE_Timeout_Locking_Time_L,	//0x34A       

    // Debug
    T2_LITE_FEF_FORCEGAIN_FLAG = 0x380, //0x380
	JOINT_DETECTION_FLAG = 0x381,
    
} E_DVBT2_PARAM;

typedef enum
{
    E_DMD_DVBT_N_PARAM_VERSION = 0x00,    //0x00
    E_DMD_DVBT_N_OP_RFAGC_EN,
    E_DMD_DVBT_N_OP_HUMDET_EN,
    E_DMD_DVBT_N_OP_DCR_EN,
    E_DMD_DVBT_N_OP_IIS_EN,
    E_DMD_DVBT_N_OP_CCI_EN,
    E_DMD_DVBT_N_OP_ACI_EN,
    E_DMD_DVBT_N_OP_IQB_EN,
    E_DMD_DVBT_N_OP_AUTO_IQ_SWAP_EN,      //0x08
    E_DMD_DVBT_N_OP_AUTO_RF_MAX_EN,
    E_DMD_DVBT_N_OP_FORCE_ACI_EN,
    E_DMD_DVBT_N_OP_FIX_MODE_CP_EN,
    E_DMD_DVBT_N_OP_FIX_TPS_EN,
    E_DMD_DVBT_N_OP_AUTO_SCAN_MODE_EN,
    E_DMD_DVBT_N_CFG_RSSI,
    E_DMD_DVBT_N_CFG_ZIF,                 //0x0F
    E_DMD_DVBT_N_CFG_NIF,                 //0x10
    E_DMD_DVBT_N_CFG_LIF,
    E_DMD_DVBT_N_CFG_SAWLESS,
    E_DMD_DVBT_N_CFG_FS_L,
    E_DMD_DVBT_N_CFG_FS_H,
    E_DMD_DVBT_N_CFG_FIF_L,
    E_DMD_DVBT_N_CFG_FIF_H,
    E_DMD_DVBT_N_CFG_FC_L,
    E_DMD_DVBT_N_CFG_FC_H,                //0x18
    E_DMD_DVBT_N_CFG_BW,
    E_DMD_DVBT_N_CFG_MODE,
    E_DMD_DVBT_N_CFG_CP,
    E_DMD_DVBT_N_CFG_LP_SEL,
    E_DMD_DVBT_N_CFG_CSTL,
    E_DMD_DVBT_N_CFG_HIER,
    E_DMD_DVBT_N_CFG_HPCR,                //0x1F
    E_DMD_DVBT_N_CFG_LPCR,                //0x20
    E_DMD_DVBT_N_CFG_IQ_SWAP,
    E_DMD_DVBT_N_CFG_RFMAX,
    E_DMD_DVBT_N_CFG_CCI,
    E_DMD_DVBT_N_CFG_ICFO_RANGE,
    E_DMD_DVBT_N_CFG_RFAGC_REF,
    E_DMD_DVBT_N_CFG_IFAGC_REF_2K,
    E_DMD_DVBT_N_CFG_IFAGC_REF_8K,      // 0x27
    E_DMD_DVBT_N_CFG_IFAGC_REF_ACI,       //0x28
    E_DMD_DVBT_N_CFG_IFAGC_REF_IIS,
    E_DMD_DVBT_N_CFG_IFAGC_REF_2K_H,
    E_DMD_DVBT_N_CFG_IFAGC_REF_8K_H,    // 0x2B
    E_DMD_DVBT_N_CFG_IFAGC_REF_ACI_H,
    E_DMD_DVBT_N_CFG_IFAGC_REF_IIS_H,
    E_DMD_DVBT_N_CFG_TS_SERIAL,
    E_DMD_DVBT_N_CFG_TS_CLK_INV,          //0x2F
    E_DMD_DVBT_N_CFG_TS_DATA_SWAP,        //0x30
    T_State0_AGC_L,               // 0x31
    T_State0_AGC_H,
    T_State1_DCR_L,
    T_State1_DCR_H,
    T_State2_DAGC2_L,
    T_State2_DAGC2_H,
    T_State3_DAGC1_L,
    T_State3_DAGC1_H,
    T_State4_MODECP_L,
    T_State4_MODECP_H,            // 0x3A
    T_State5_L,
    T_State5_H,
    E_DMD_DVBT_N_IFAGC_REF_READ,
    E_DMD_DVBT_N_IFAGC_K,
    E_DMD_DVBT_N_AGC_IF_GAIN_MIN,         //0x3F
    E_DMD_DVBT_N_AGC_IF_GAIN_MAX,         //0x40
    E_DMD_DVBT_N_AGC_LOCK_TH,
    E_DMD_DVBT_N_AGC_LOCK_NUM,
    E_DMD_DVBT_N_ADC_PGA_GAIN_I,
    E_DMD_DVBT_N_ADC_PGA_GAIN_Q,
    E_DMD_DVBT_N_PWDN_ADCI,
    E_DMD_DVBT_N_PWDN_ADCQ,
    E_DMD_DVBT_N_MPLL_ADC_DIV_SEL,
    E_DMD_DVBT_N_DCR_LOCK,                //0x48
    E_DMD_DVBT_N_MIXER_IQ_SWAP_MODE,
    E_DMD_DVBT_N_CCI_BYPASS,
    E_DMD_DVBT_N_CCI_LOCK_DET,
    E_DMD_DVBT_N_CCI_FSWEEP_L,
    E_DMD_DVBT_N_CCI_FSWEEP_H,
    E_DMD_DVBT_N_CCI_KPKI,
    E_DMD_DVBT_N_INTP_RATEM1_0,           //0x4F
    E_DMD_DVBT_N_INTP_RATEM1_1,           //0x50
    E_DMD_DVBT_N_INTP_RATEM1_2,
    E_DMD_DVBT_N_INTP_RATEM1_3,
    E_DMD_DVBT_N_8K_MC_MODE,
    E_DMD_DVBT_N_8K_MC_CP,
    E_DMD_DVBT_N_8K_MC_CPOBS_NUM,
    E_DMD_DVBT_N_8K_MODECP_DET,
    E_DMD_DVBT_N_2K_MC_MODE,
    E_DMD_DVBT_N_2K_MC_CP,                //0x58
    E_DMD_DVBT_N_2K_MC_CPOBS_NUM,
    E_DMD_DVBT_N_2K_MODECP_DET,
    E_DMD_DVBT_N_ICFO_SCAN_WINDOW_L,
    E_DMD_DVBT_N_ICFO_SCAN_WINDOW_H,
    E_DMD_DVBT_N_ICFO_MAX_OFFSET_L,
    E_DMD_DVBT_N_ICFO_MAX_OFFSET_H,
    E_DMD_DVBT_N_ICFO_DONE,               //0x5F
    E_DMD_DVBT_N_TPS_SYNC_LOCK,           //0x60
    E_DMD_DVBT_N_CONSTELLATION,
    E_DMD_DVBT_N_HIERARCHY,
    E_DMD_DVBT_N_HP_CODE_RATE,
    E_DMD_DVBT_N_LP_CODE_RATE,
    E_DMD_DVBT_N_GUARD_INTERVAL,
    E_DMD_DVBT_N_TRANSMISSION_MODE,
    E_DMD_DVBT_N_OFDM_SYMBOL_NUMBER,
    E_DMD_DVBT_N_LENGTH_INDICATOR,        //0x68
    E_DMD_DVBT_N_FRAME_NUMBER,
    E_DMD_DVBT_N_CELL_IDENTIFIER,
    E_DMD_DVBT_N_DVBH_SIGNALLING,
    E_DMD_DVBT_N_SNR_2K_ALPHA,
    E_DMD_DVBT_N_SNR_8K_ALPHA,
    E_DMD_DVBT_N_TS_EN,
    E_DMD_DVBT_N_2K_DAGC1_REF,            //0x6F
    E_DMD_DVBT_N_8K_DAGC1_REF,            //0x70
    E_DMD_DVBT_N_2K_8K_DAGC2_REF,
    E_DMD_DVBT_N_IF_INV_PWM_OUT_EN,
    T_State6_TPS_L,
    T_State6_TPS_H,
    T_State7_L,
    T_State7_H,
    T_State8_L,
    T_State8_H,
    T_State9_L,		         // 0x79
    T_State9_H,
    T_State10_PSYNC_L,
    T_State10_PSYNC_H,	         // 0x7C
    T_DVBT2_NOCHAN_Flag = 0xF1,
    T_DVBT_NOCHAN_Flag = 0xF2,
    T_DETECT_DONE_FLAG = 0xF3,	
}E_DVBT_N_PARAM;

typedef enum
{
	E_TUNER_SI2158=0x00,
	E_TUNER_SI2178,
	E_TUNER_SI2176,
	E_TUNER_UNKOWN = 0xFF
}E_TUNER_TYPE;

typedef enum
{
	TS_PARALLEL = 0,
	TS_SERIAL = 1,

	TS_MODE_MAX_NUM
}E_TS_MODE;

typedef enum
{
    E_DMD_T2_RF_BAND_5MHz = 0x01,
    E_DMD_T2_RF_BAND_1p7MHz = 0x00,
    E_DMD_T2_RF_BAND_6MHz = 0x02,
    E_DMD_T2_RF_BAND_7MHz = 0x03,
    E_DMD_T2_RF_BAND_8MHz = 0x04,
    E_DMD_T2_RF_BAND_10MHz = 0x05,
    E_DMD_T2_RF_BAND_INVALID
} DMD_DVBT2_RF_CHANNEL_BANDWIDTH;

typedef enum
{
	COFDM_FEC_LOCK_DVBT,
	COFDM_FEC_LOCK_DVBC,
	COFDM_TR_LOCK_DVBC,
	COFDM_PSYNC_LOCK,
	COFDM_TPS_LOCK,
	COFDM_TPS_LOCK_HISTORY,
	COFDM_DCR_LOCK,
	COFDM_AGC_LOCK,
	COFDM_MODE_DET,
	COFDM_LOCK_STABLE_DVBT,
    COFDM_MODE_CP_NO_CH_DETECT,
    COFDM_T2_AUTO_DETECT_NO_CH,
    COFDM_T2_AUTO_DETECT_FOUND_T2,
    COFDM_P1_LOCK,
    COFDM_P1_LOCK_HISTORY,
    COFDM_L1_CRC_LOCK,
    COFDM_FEC_LOCK_T2,
    COFDM_LOCK_STATUS_UNKNOWN
} COFDM_LOCK_STATUS;

typedef enum
{
    MHal_DVBTxC_T2_MODUL_MODE,
    MHal_DVBTxC_T2_FFT_VALUE,
    MHal_DVBTxC_T2_GUARD_INTERVAL,
    MHal_DVBTxC_T2_CODE_RATE,
    MHal_DVBTxC_T2_PREAMBLE,
    MHal_DVBTxC_T2_S1_SIGNALLING,
    MHal_DVBTxC_T2_PILOT_PATTERN,
    MHal_DVBTxC_T2_BW_EXT,
    MHal_DVBTxC_T2_PAPR_REDUCTION,
    MHal_DVBTxC_T2_OFDM_SYMBOLS_PER_FRAME,
    MHal_DVBTxC_T2_PLP_ROTATION,
    MHal_DVBTxC_T2_PLP_FEC_TYPE,
    MHal_DVBTxC_T2_NUM_PLP,
	MHal_DVBTxC_T2_PLP_TYPE,
	MHal_DVBTxC_T2_PLP_TIME_IL_TYPE,
	MHal_DVBTxC_T2_PLP_TIME_IL_LENGTH,
	MHal_DVBTxC_T2_DAT_ISSY,
	MHal_DVBTxC_T2_PLP_MODE,
	MHal_DVBTxC_T2_L1_MODULATION,
	MHal_DVBTxC_T2_NUM_T2_FRAMES,
	MHal_DVBTxC_T2_PLP_NUM_BLOCKS_MAX,
	MHal_DVBTxC_T2_FEF_ENABLE,
    MHal_DVBTxC_T2_PARAM_MAX_NUM,
} MHal_DVBTxC_DVBT2_SIGNAL_INFO;

typedef enum
{
	_T_FFT_2K		= 0x0,
	_T_FFT_8K		= 0x1,
	_T_FFT_4K		= 0x2,
}E_T_FFT;

typedef enum
{
	_T_GI_1Y32		= 0x0,
	_T_GI_1Y16		= 0x1,
	_T_GI_1Y8		= 0x2,
	_T_GI_1Y4		= 0x3,
}E_T_GI;

typedef enum
{
	_CR1Y2		= 0x0,
	_CR2Y3		= 0x1,
	_CR3Y4		= 0x2,
	_CR5Y6		= 0x3,
	_CR7Y8		= 0x4,
}E_CODERATE;

typedef enum 	/* 3 bit */
{
	_T_HIERA_NONE 	= 0x0,
	_T_HIERA_1		= 0x1,
	_T_HIERA_2		= 0x2,
	_T_HIERA_4		= 0x3,
}E_HIERARCHY_T;

typedef enum
{
	_QPSK		= 0x0,
	_16QAM		= 0x1,
	_64QAM		= 0x2,
}E_CONSTEL;

typedef enum
{
	_C_16QAM		= 0x0,
	_C_32QAM		= 0x1,
	_C_64QAM		= 0x2,
	_C_128QAM		= 0x3,
	_C_256QAM		= 0x4,
}E_C_CONSTEL;

//--------------------------------------------------------------------
EXTSEL void MHal_DVBTxC_InitClkgen(E_SYSTEM system, E_TS_MODE ts_mode);
EXTSEL B16 MHal_DVBTxC_Download(E_SYSTEM eSystems);
EXTSEL B16 MHal_DVBTxC_Reset(void);
EXTSEL B16 MHal_DVBTxC_Get_Version(U16 *ver_bcd);
EXTSEL B16 MHal_DVBTxC_System_Init(E_SYSTEM system,E_TUNER_TYPE tuner_type);
EXTSEL B16 MHal_DVBTxC_Config_DVBT(U16 u16ChBw, B16 bSetLp, B16 bForce, B16 bAutoScan, U16 u16FFTMode, U16 u16CP);
EXTSEL B16 MHal_DVBTxC_Active(B16 bEnable);
#ifdef DEMOD_DVB_T2_MERGE_T
EXTSEL U8 MHal_DVBTxC_Config_DVBT2(DMD_DVBT2_RF_CHANNEL_BANDWIDTH BW, U32 u32IFFreq, U8 u8PlpID, U8 Channel_Switch, U8 bSetLp);
#else
EXTSEL U8 MHal_DVBTxC_Config_DVBT2(DMD_DVBT2_RF_CHANNEL_BANDWIDTH BW, U32 u32IFFreq, U8 u8PlpID, U8 Channel_Switch);
#endif
EXTSEL S32 MHal_DVBTxC_SetTSClkRate(S32 ts_rate);
EXTSEL U16 MHal_DVBTxC_Set_Config_dvbc_auto(U8 bAutoDetect);
EXTSEL U16 MHal_DVBTxC_Set_Config_dvbc_atv_detector (U8 bEnable);
EXTSEL U16 MHal_DVBTxC_SetDvbcParam (U8 constel);
EXTSEL U16 MHal_DVBTxC_Config_dvbc(U16 SymbolRate, U32 u32IFFreq, U16 bSpecInv);
EXTSEL U16 MHal_DVBTxC_Lock(E_SYSTEM system, COFDM_LOCK_STATUS eStatus );
EXTSEL U16 MHal_DVBTxC_DVBT2_GetSNR (S32 *snr_e2);
EXTSEL BOOL MHal_DVBTxC_DVBT2_GetPostLdpcBer(S32 *ber_e7);
EXTSEL BOOL MHal_DVBTxC_DVBT2_GetSignalQuality(U16 *quality);
EXTSEL BOOL MHal_DVBTxC_DVBT2_Get_L1_Parameter(U16 * pu16L1_parameter, MHal_DVBTxC_DVBT2_SIGNAL_INFO eSignalType);
EXTSEL B16 MHal_DVBTxC_DVBT2_GetPacketErr(U16 *u16PktErr);
EXTSEL B16 MHal_DVBTxC_GetIFAGCGain(U16 *pu16ifagc);
EXTSEL U16 MHal_DVBTxC_DVBTGetSNR (S32 *snr_e2);
EXTSEL U16 MHal_DVBTxC_GetPostViterbiBer(S32 *ber_e7);
EXTSEL U16 MHal_DVBTxC_GetSignalQuality(E_SYSTEM system,U16 *quality);
EXTSEL B16 MHal_DVBTxC_Get_Packet_Error(U16 *u16_data);
EXTSEL U16 MHal_DVBTxC_DVBCGetSNR(S32 *snr_e2);
EXTSEL U16 MHal_DVBTxC_GetTpsInfo(U16 *TPS_parameter);
EXTSEL BOOL MHal_DVBTxC_DVBT2_GetPlpBitMap(U8* u8PlpBitMap);
EXTSEL B16 MHal_DVBTxC_GetDvbcInfo(U32 * DVBC_parameter);
EXTSEL U16 MHal_DVBTxC_Get_FreqOffset(E_SYSTEM eSystems, S32 *pFreqOff, U8 u8BW);
EXTSEL B16 MHal_DVBTxC_GetCellId(E_SYSTEM eSystems, U16 *cell_id);
EXTSEL BOOL MHal_DVBTxC_DVBT2_GetPlpGroupID(U8 u8PlpID, U8* u8GroupID);
EXTSEL BOOL MHal_DVBTxC_DVBT2_SetPlpGroupID(U8 u8PlpID, U8 u8GroupID);
EXTSEL U16 MHal_DVBTxC_Get_Version(U16 *ver_bcd);
EXTSEL BOOL MHal_DVBTxC_GetSpectrumInv(E_SYSTEM system);
EXTSEL BOOL MHal_DVBTxC_GetPacketErrAccu(U32 *pu32PktErr, BOOL bClear_Accu, E_SYSTEM system);
EXTSEL BOOL MHal_DVBTxC_GetUnlockStatus(E_SYSTEM system, U8 *CurrState, U32 *UnlockFlag);

#endif
