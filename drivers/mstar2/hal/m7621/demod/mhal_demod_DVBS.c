#define _DVBS_C_


#include <linux/kernel.h>
#include <linux/jiffies.h>
#include <linux/delay.h>
#include <asm/setup.h>
#include "mdrv_types.h"
#include "mdrv_system.h"
#include "mhal_demod.h"
#include "mhal_demod_DVBS.h"
#include "MsFixpLib.h"

extern struct tag_custom customTAG;

#define MBRegBase   0x112600
#define VDMcuBase   0x103400
#define DMDMcuBase 0x103480
// -- DDI layer
/*
  ================should define at DDI layer====================

typedef enum
{
    DMD_DVBS_QPSK = 0,
    DMD_DVBS_8PSK = 1,
} DMD_DVBS_MODULATION_TYPE;




*/



// -------------------- utpoia ------------------------------
// 1. static variable
static BOOL bDMD_DVBS_NoChannelDetectedWithRFPower = FALSE;
static U32 u32DMD_DVBS_NoChannelTimeAccWithRFPower = 0;
static  BOOL    _bSerialTS=FALSE;
static U8        g_dvbs_lock = 0;
static  BOOL     _bDemodType=FALSE;
static  BOOL    _bTSDataSwap=FALSE;
//static  MS_U64       _f_DVBS_CurrentSNR=0;
static  U8       _u8_DVBS2_CurrentCodeRate;
U8 u8DemodLockFlag;
static  U32        _u32CurrentSR=0;
//static U32       u32ChkScanTimeStartDVBS = 0;
//static  U32       _fPostBer=0;
//Airtel Only 
#define  SNR_UNDER6_MAX_CNT  3
BOOL SNR_UNDER6 = FALSE;
S8   SNR_UNDER6_CNT = SNR_UNDER6_MAX_CNT;
U32  SNR_UPDATE_TS = 0;
/*BEGIN: auto iq swap detection (7/7)*/
static BOOL iq_swap_checking_flag=FALSE;
static BOOL iq_swap_done_flag=FALSE;
static  U16      cf_iq_swap_check=0;
static  U16      bw_iq_swap_check=0;
/*END: auto iq swap detection (7/7)*/
//U32 SNR_OLD=0;
/*
static MS_U16             _u16SignalLevel[185][2]=
{//AV2028 SR=22M, 2/3 CN=5.9
    {32100,    920},{32200,    915},{32350,    910},{32390,    905},{32480,    900},{32550,    895},{32620,    890},{32680,    885},{32750,    880},{32830,    875},
    {32930,    870},{33010,    865},{33100,    860},{33200,    855},{33310,    850},{33410,    845},{33520,    840},{33640,    835},{33770,    830},{33900,    825},
    {34030,    820},{34150,    815},{34290,    810},{34390,    805},{34490,    800},{34580,    795},{34700,    790},{34800,    785},{34880,    780},{34940,    775},
    {35030,    770},{35130,    765},{35180,    760},{35260,    755},{35310,    750},{35340,    745},{35380,    740},{35400,    735},{35450,    730},{35550,    725},
    {35620,    720},{35700,    715},{35800,    710},{35890,    705},{36000,    700},{36120,    695},{36180,    690},{36280,    685},{36400,    680},{36570,    675},
    {36730,    670},{36910,    665},{37060,    660},{37100,    655},{37260,    650},{37340,    645},{37410,    640},{37580,    635},{37670,    630},{37700,    625},
    {37750,    620},{37800,    615},{37860,    610},{37980,    605},{38050,    600},{38170,    595},{38370,    590},{38540,    585},{38710,    580},{38870,    575},
    {39020,    570},{39070,    565},{39100,    560},{39180,    555},{39280,    550},{39460,    545},{39510,    540},{39600,    535},{39620,    530},{39680,    525},
    {39720,    520},{39830,    515},{39880,    510},{39930,    505},{39960,    500},{40000,    495},{40200,    490},{40360,    485},{40540,    480},{40730,    475},
    {40880,    470},{41020,    465},{41150,    460},{41280,    455},{41410,    450},{41520,    445},{41620,    440},{41730,    435},{41840,    430},{41930,    425},
    {42010,    420},{42100,    415},{42180,    410},{42260,    405},{42350,    400},{42440,    395},{42520,    390},{42580,    385},{42660,    380},{42730,    375},
    {42800,    370},{42870,    365},{42940,    360},{43000,    355},{43060,    350},{43130,    345},{43180,    340},{43250,    335},{43310,    330},{43370,    325},
    {43420,    320},{43460,    315},{43520,    310},{43570,    305},{43620,    300},{43660,    295},{43710,    290},{43750,    285},{43810,    280},{43860,    275},
    {43910,    270},{43940,    265},{43990,    260},{44020,    255},{44060,    250},{44110,    245},{44140,    240},{44190,    235},{44230,    230},{44270,    225},
    {44320,    220},{44370,    215},{44400,    210},{44450,    205},{44490,    200},{44530,    195},{44590,    190},{44630,    185},{44660,    180},{44720,    175},
    {44750,    170},{44790,    165},{44830,    160},{44880,    155},{44910,    150},{44960,    145},{45000,    140},{45030,    135},{45070,    130},{45100,    125},
    {45130,    120},{45160,    115},{45200,    110},{45240,    105},{45270,    100},{45300,     95},{45330,     90},{45360,     85},{45400,     80},{45430,     75},
    {45460,     70},{45490,     65},{45530,     60},{45560,     55},{45590,     50},{45630,     45},{45670,     40},{45690,     35},{45740,     30},{45760,     25},
    {45800,     20},{45830,     15},{45860,     10},{45880,      5},{45920,      0}
};
*/
static U16             _u16SignalLevel[14][2]=
{//RDA5815
    //{255,    950},
    {255,    900},
    {257,    850},
    {29225,    800},
    {35070,    750},
    {39499,    700},
    {40910,    650},
    {42040,    600},
    {43210,    550},
    {43884,    500},
    {44980,    450},
    {45567,    400},
    {46712,    350},
    {47630,    300},
    {49091,    200}
};
U8       modulation_order;


typedef struct
{
    U16 u16_x_10cn;
    U8 u8_y_sqi;
}S_SQI_TABLE;
// 2. add dbg message
#define DBG_INTERN_DVBS(x)        //x
#define DBG_INTERN_DVBS_LOCK(x)   //x
#define DBG_INTERN_DVBS_TIME(x)   //x
#define DBG_GET_SIGNAL_DVBS(x)    //x
#define DEBUG_AUTO_IQ_SWAP_DETECTION 0 //debug for iq swap auto detection

// 3. bank define
#define DVBS2FEC_REG_BASE               0x3300    //DVBT2FEC
#define DVBS2OPPRO_REG_BASE           0x3E00    //DVBS2OPPRO
#define TOP_REG_BASE                         0x2000    //DMDTOP
#define DVBSFEC_REG_BASE                 0x3F00    //DVBSFEC
#define DVBS2_REG_BASE                     0x3A00
#define DVBS2_INNER_REG_BASE         0x3B00
#define DVBS2_INNER_EXT_REG_BASE 0x3C00
#define DVBSTFEC_REG_BASE               0x2300    //DVBTFEC
//#define DVBS2_FEC_REG_BASE
#define REG_BACKEND                           0x1F00
#define FRONTEND_REG_BASE              0x2800
#define FRONTENDEXT_REG_BASE        0x2900
#define FRONTENDEXT2_REG_BASE      0x2A00
#define DMDANA_REG_BASE                 0x2E00    //DMDDTOP
#define DVBTM_REG_BASE                    0x3400
// 4. code flow define
#define INTERN_DVBS_TS_SERIAL_INVERSION       0
#define INTERN_DVBS_TS_PARALLEL_INVERSION   1
// 5. dsp reg define
//For Parameter Init Setting
#define     A_S2_ZIF_EN                     0x01                //[0]
#define     A_S2_RF_AGC_EN                  0x00                //[0]
#define     A_S2_DCR_EN                     0x00                //[0]       0=Auto :1=Force
#define     A_S2_IQB_EN                     0x01                //[2]
#define     A_S2_IIS_EN                     0x00                //[0]
#define     A_S2_CCI_EN                     0x00                //[0]       0:1=Enable
#define     A_S2_FORCE_ACI_SELECT           0xFF                //[3:0]     0xFF=OFF(internal default)
#define     A_S2_IQ_SWAP                    0x01                //[0]
#define     A_S2_AGC_REF_EXT_0              0x00                //[7:0]  //0x00 0x90
#define     A_S2_AGC_REF_EXT_1              0x02                //[11:8] //0x02 0x07
#define     A_S2_AGC_K                      0x07                //[15:12]
#define     A_S2_ADCI_GAIN                  0x0F                //[4:0]
#define     A_S2_ADCQ_GAIN                  0x0F                //[12:8]
#define     A_S2_SRD_SIG_SRCH_RNG           0x6A                //[7:0]
#define     A_S2_SRD_DC_EXC_RNG             0x16                //[7:0]
//FRONTENDEXT_SRD_FRC_CFO
#define     A_S2_FORCE_CFO_0                0x00                //[7:0]
#define     A_S2_FORCE_CFO_1                0x00                //[11:8]
#define     A_S2_DECIMATION_NUM             0x00                //[3:0]     00=(Internal Default)
#define     A_S2_PSD_SMTH_TAP               0x29                //[6:0]     Bit7 no define.
//CCI Parameter
//Set_Tuner_BW=(((U16)REG_BASE[DIG_SWUSE1FH]<<8)|REG_BASE[DIG_SWUSE1FL]);
#define     A_S2_CCI_FREQN_0_L              0x00                //[7:0]
#define     A_S2_CCI_FREQN_0_H              0x00                //[11:8]
#define     A_S2_CCI_FREQN_1_L              0x00                //[7:0]
#define     A_S2_CCI_FREQN_1_H              0x00                //[11:8]
#define     A_S2_CCI_FREQN_2_L              0x00                //[7:0]
#define     A_S2_CCI_FREQN_2_H              0x00                //[11:8]
//Inner TR Parameter
#define     A_S2_TR_LOPF_KP                 0x00                //[4:0]     00=(Internal Default)
#define     A_S2_TR_LOPF_KI                 0x00                //[4:0]     00=(Internal Default)
//Inner FineFE Parameter
#define     A_S2_FINEFE_KI_SWITCH_0         0x00                //[15:12]   00=(Internal Default)
#define     A_S2_FINEFE_KI_SWITCH_1         0x00                //[3:0]     00=(Internal Default)
#define     A_S2_FINEFE_KI_SWITCH_2         0x00                //[7:4]     00=(Internal Default)
#define     A_S2_FINEFE_KI_SWITCH_3         0x00                //[11:8]    00=(Internal Default)
#define     A_S2_FINEFE_KI_SWITCH_4         0x00                //[15:12]   00=(Internal Default)
//Inner PR KP Parameter
#define     A_S2_PR_KP_SWITCH_0             0x00                //[11:8]    00=(Internal Default)
#define     A_S2_PR_KP_SWITCH_1             0x00                //[15:12]   00=(Internal Default)
#define     A_S2_PR_KP_SWITCH_2             0x00                //[3:0]     00=(Internal Default)
#define     A_S2_PR_KP_SWITCH_3             0x00                //[7:4]     00=(Internal Default)
#define     A_S2_PR_KP_SWITCH_4             0x00                //[11:8]    00=(Internal Default)
//Inner FS Parameter
#define     A_S2_FS_GAMMA                   0x10                //[7:0]
#define     A_S2_FS_ALPHA0                  0x10                //[7:0]
#define     A_S2_FS_ALPHA1                  0x10                //[7:0]
#define     A_S2_FS_ALPHA2                  0x10                //[7:0]
#define     A_S2_FS_ALPHA3                  0x10                //[7:0]

#define     A_S2_FS_H_MODE_SEL              0x01                //[0]
#define     A_S2_FS_OBSWIN                  0x08                //[12:8]
#define     A_S2_FS_PEAK_DET_TH_L           0x00                //[7:0]
#define     A_S2_FS_PEAK_DET_TH_H           0x01                //[15:8]
#define     A_S2_FS_CONFIRM_NUM             0x01                //[3:0]
//Inner EQ Parameter
#define     A_S2_EQ_MU_FFE_DA               0x00                //[3:0]     00=(Internal Default)
#define     A_S2_EQ_MU_FFE_DD               0x00                //[7:4]     00=(Internal Default)
#define     A_S2_EQ_ALPHA_SNR_DA            0x00                //[7:4]     00=(Internal Default)
#define     A_S2_EQ_ALPHA_SNR_DD            0x00                //[11:8]    00=(Internal Default)
//Outer FEC Parameter
#define     A_S2_FEC_ALFA                   0x00                //[12:8]
#define     A_S2_FEC_BETA                   0x01                //[7:4]
#define     A_S2_FEC_SCALING_LLR            0x00                //[7:0]     00=(Internal Default)
//TS Parameter
#if INTERN_DVBS_TS_SERIAL_INVERSION
#define     A_S2_TS_SERIAL                  0x01                //[0]
#else
#define     A_S2_TS_SERIAL                  0x00                //[0]
#endif
#define     A_S2_TS_CLK_RATE                0x00
#define     A_S2_TS_OUT_INV                 0x00                //[5]
#define     A_S2_TS_DATA_SWAP               0x00                //[5]
//Rev Parameter

#define     A_S2_FW_VERSION_L               0x00                //From FW
#define     A_S2_FW_VERSION_H               0x00                //From FW
#define     A_S2_CHIP_VERSION               0x01
#define     A_S2_FS_L                       0x00
#define     A_S2_FS_H                       0x00
#define     A_S2_MANUAL_TUNE_SYMBOLRATE_L   0x20
#define     A_S2_MANUAL_TUNE_SYMBOLRATE_H   0x4E
#define DMD_MBX_TIMEOUT 200
U8 INTERN_DVBS_DSPREG[] =
{
    A_S2_ZIF_EN,            A_S2_RF_AGC_EN,         A_S2_DCR_EN,             A_S2_IQB_EN,               A_S2_IIS_EN,              A_S2_CCI_EN,              A_S2_FORCE_ACI_SELECT,          A_S2_IQ_SWAP,                   // 00H ~ 07H
    A_S2_AGC_REF_EXT_0,     A_S2_AGC_REF_EXT_1,     A_S2_AGC_K,              A_S2_ADCI_GAIN,            A_S2_ADCQ_GAIN,           A_S2_SRD_SIG_SRCH_RNG,    A_S2_SRD_DC_EXC_RNG,            A_S2_FORCE_CFO_0,               // 08H ~ 0FH
    A_S2_FORCE_CFO_1,       A_S2_DECIMATION_NUM,    A_S2_PSD_SMTH_TAP,       A_S2_CCI_FREQN_0_L,        A_S2_CCI_FREQN_0_H,       A_S2_CCI_FREQN_1_L,       A_S2_CCI_FREQN_1_H,             A_S2_CCI_FREQN_2_L,             // 10H ~ 17H
    A_S2_CCI_FREQN_2_H,     A_S2_TR_LOPF_KP,        A_S2_TR_LOPF_KI,         A_S2_FINEFE_KI_SWITCH_0,   A_S2_FINEFE_KI_SWITCH_1,  A_S2_FINEFE_KI_SWITCH_2,  A_S2_FINEFE_KI_SWITCH_3,        A_S2_FINEFE_KI_SWITCH_4,        // 18H ~ 1FH
    A_S2_PR_KP_SWITCH_0,    A_S2_PR_KP_SWITCH_1,    A_S2_PR_KP_SWITCH_2,     A_S2_PR_KP_SWITCH_3,       A_S2_PR_KP_SWITCH_4,      A_S2_FS_GAMMA,            A_S2_FS_ALPHA0,                 A_S2_FS_ALPHA1,                 // 20H ~ 27H
    A_S2_FS_ALPHA2,         A_S2_FS_ALPHA3,         A_S2_FS_H_MODE_SEL,      A_S2_FS_OBSWIN,            A_S2_FS_PEAK_DET_TH_L,    A_S2_FS_PEAK_DET_TH_H,    A_S2_FS_CONFIRM_NUM,            A_S2_EQ_MU_FFE_DA,              // 28h ~ 2FH
    A_S2_EQ_MU_FFE_DD,      A_S2_EQ_ALPHA_SNR_DA,   A_S2_EQ_ALPHA_SNR_DD,    A_S2_FEC_ALFA,             A_S2_FEC_BETA,            A_S2_FEC_SCALING_LLR,     A_S2_TS_SERIAL,                 A_S2_TS_CLK_RATE,               // 30H ~ 37H
    A_S2_TS_OUT_INV,        A_S2_TS_DATA_SWAP,      A_S2_FW_VERSION_L,       A_S2_FW_VERSION_H,         A_S2_CHIP_VERSION,        A_S2_FS_L,                A_S2_FS_H,                      A_S2_MANUAL_TUNE_SYMBOLRATE_L,  // 38H ~ 3CH
    A_S2_MANUAL_TUNE_SYMBOLRATE_H,
};


static  U8       _u8ToneBurstFlag=0;

static  U16      _u16BlindScanStartFreq=0;
static  U16      _u16BlindScanEndFreq=0;
static  U16      _u16TunerCenterFreq=0;
static  U16      _u16ChannelInfoIndex=0;

//Debug Only+
static  U16      _u16NextCenterFreq=0;
static  U16      _u16LockedSymbolRate=0;
static  U16      _u16LockedCenterFreq=0;
static  U16      _u16PreLockedHB=0;
static  U16      _u16PreLockedLB=0;
static  U16      _u16CurrentSymbolRate=0;
static  S16      _s16CurrentCFO=0;
static  U16      _u16CurrentStepSize=0;
mapi_demodulator_datatype_EN_BLINDSCAN_STATUS eBlindScanStatue_inter = mapi_demodulator_datatype_E_BLINDSCAN_NOTREADY;
//Debug Only-
static  U16      _u16ChannelInfoArray[2][1000];

//S_CMDPKTREG gsCmdPacketDVBS;
extern U32  u32DMD_DVBS2_DJB_START_ADDR;
#define INTERN_DVBS_DEMOD_WAIT_TIMEOUT      (6000)
#define INTERN_DVBS_TUNER_WAIT_TIMEOUT      (50)

//Debug Menu
static BOOL Debug_TS_CLK_on=0;
static BOOL Debug_CFO_on=0;
static BOOL Unicable_System_Flag=FALSE;
static BOOL Initial_TS_CLK_config=FALSE;
#define FFT_CAPTURE_EN_debug 0
#define SHIFT_MULTIPLE  1000
static U32 k_bch=0;
static U32 s2_fec_frame=0;
static U8 pre_TS_CLK_DIV=0;
static U8 pre_TS_CLK_source=0;
static U8 cur_TS_CLK_DIV=0;
static U8 cur_TS_CLK_source=0;
#define MMAP_INDEX_ISDBT_TDI    62
#define MMAP_INDEX_MEM_ADDR     1
#define MMAP_INDEX_MEM_LEN      3
#define INTERNAL_DVBT2_EQ_LEN        0x96000   // 600K
#define INTERNAL_DVBT2_TDI_LEN       0x320000   //3200KB
#define INTERNAL_DVBT2_DJB_LEN       0x96000   //600K
#define INTERNAL_DVBT2_DRAM_OFFSET   0x5000
#define INTERNAL_DVBT2_FW_LEN        (0x10000 - INTERNAL_DVBT2_DRAM_OFFSET)
// ------------------------------- end utpoia------------------------------------------------------------------
BOOL DVBS_ReadReg2bytes(U16 u16Addr, U16 *u16Data)
{
    //DBG_DEMOD_FLOW(printk("%s(),%d\n",__func__,__LINE__));

   BOOL ret = true;
   U8 u8data=0;
   U16 u16data_temp=0;
   
   ret &= MHal_Demod_MB_ReadReg(u16Addr, &u8data);
   u16data_temp=u8data;
   ret &= MHal_Demod_MB_ReadReg(u16Addr + 0x0001, &u8data);
   u16data_temp=u16data_temp<<8|u8data;
   *u16Data=u16data_temp;
    return ret;
}

// reset mcu
B16 DVBS_Reset ( void )
{
    U32 u32StartTime=(U32)jiffies_to_msecs(jiffies);//MsOS_GetSystemTime();
    U8 u8data=0;
    
    DBG_INTERN_DVBS(printk(" @INTERN_DVBS_reset  u32StartTime=%d\n",u32StartTime));
    DBG_INTERN_DVBS(printk(" @INTERN_DVBS_reset\n"));

    //MHal_Demod_WriteReg(DMDMcuBase + 0x00, 0x02);     // reset RIU remapping reset
    MHal_Demod_WriteReg(DMDMcuBase + 0x00, 0x01);     // reset DMD_MCU
    /*
if(MHal_Demod_WriteReg(DMDMcuBase + 0x00, 0x01) !=0)//addy update 0602// reset VD_MCU
{
  DBG_INTERN_DVBS(printk(" @DVBS_reset fail\n"));
  return FALSE;
}
     */
    mdelay(5);//udelay(5*1000);
    MHal_Demod_WriteReg(MBRegBase + 0x00 , 0x00);
    MHal_Demod_WriteReg(DMDMcuBase + 0x00, 0x00);
    MHal_Demod_ReadReg(DMDMcuBase + 0x00, &u8data);

    //if(MHal_Demod_WriteReg(DMDMcuBase + 0x00, 0x00)!=0)//addy update 0602
    if(u8data!=0x00)//addy update 0602
    {
         DBG_INTERN_DVBS(printk(" @DVBS_reset fail\n"));
         return FALSE;
    }
    mdelay(5);//udelay(5*1000);


    MHal_Demod_ReadReg(MBRegBase + 0x00,&u8data);
    while(u8data != 0xFF)           // wait MB_CNTL set done
    {
        //if (MsOS_Timer_DiffTimeFromNow(u32StartTime)>DMD_MBX_TIMEOUT)
        if (((U32)jiffies_to_msecs(jiffies)-u32StartTime)>DMD_MBX_TIMEOUT)
        {
            printk("HAL_SYS_DMD_VD_MBX_DVB_WaitHandShake Timeout\n");
            break;
        }
    }
    
    MHal_Demod_WriteReg(MBRegBase + 0x00 , 0x00);
    g_dvbs_lock = 0;

return TRUE;

}



/****************************************************************************
Subject:    MSB1228 Check Firmware Version
Function:   MSB1228_Get_Version
Parmeter:   U16 ver_bcd: version code in BCD
ex: 0x0107 => R01.07
Return:     E_RESULT_SUCCESS
E_RESULT_FAILURE
Remark:
 *****************************************************************************/
B16 DVBS_Get_Version(U16 *ver_bcd)
{
    U8 status = true;
    U8 tmp = 0;
    U16 u16_INTERN_DVBS_Version;

    DBG_INTERN_DVBS(printk(" @DVBS_Get_Version \n"));

    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_FW_VERSION_H, &tmp);
    u16_INTERN_DVBS_Version = tmp;

    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_FW_VERSION_L, &tmp);
    u16_INTERN_DVBS_Version = u16_INTERN_DVBS_Version<<8|tmp;

    DBG_INTERN_DVBS(printk("FW version: 0x%x \n",u16_INTERN_DVBS_Version));
    *ver_bcd = u16_INTERN_DVBS_Version;

    if(status)
        return TRUE;
    else
        return FALSE;

}


/***********************************************************************************
Subject:    DVB-S CLKGEN initialized function
Function:   INTERN_DVBS_InitClkgen
Parmeter:
Return:     BOOLEAN
Remark:
 ************************************************************************************/
void DVBS_InitClkgen(void)
{
    U8 u8Temp;

    MHal_Demod_WriteReg(0x101e39,0x00);
    
    MHal_Demod_WriteReg(0x1128d0,0x01);

    MHal_Demod_WriteReg(0x10331f,0x00);
    MHal_Demod_WriteReg(0x10331e,0x10);

    if(Initial_TS_CLK_config==FALSE)
    {
        MHal_Demod_WriteReg(0x103301,0x11);
        MHal_Demod_WriteReg(0x103300,0x14);
        Initial_TS_CLK_config=TRUE;
    }

    MHal_Demod_WriteReg(0x103309,0x00);
    MHal_Demod_WriteReg(0x103308,0x00);

    MHal_Demod_WriteReg(0x103321,0x00);
    MHal_Demod_WriteReg(0x103320,0x00);

    MHal_Demod_WriteReg(0x111f0b,0x00);
    MHal_Demod_WriteReg(0x111f0a,0x00);

    MHal_Demod_WriteReg(0x103315,0x00);
    MHal_Demod_WriteReg(0x103314,0x00);

    MHal_Demod_WriteReg(0x1128d1,0x00);
    MHal_Demod_WriteReg(0x1128d0,0x00);

    MHal_Demod_WriteReg(0x111f19,0x00);
    MHal_Demod_WriteReg(0x111f18,0x00);

    MHal_Demod_WriteReg(0x111f1b,0x10);
    MHal_Demod_WriteReg(0x111f1a,0x00);

    MHal_Demod_WriteReg(0x111f21,0x11);
    MHal_Demod_WriteReg(0x111f20,0x10);

    MHal_Demod_WriteReg(0x111f23,0x0c);
    MHal_Demod_WriteReg(0x111f22,0x11);

    MHal_Demod_WriteReg(0x111f25,0x04);

    MHal_Demod_WriteReg(0x111f29,0x00);
    MHal_Demod_WriteReg(0x111f28,0x00);

    MHal_Demod_WriteReg(0x111f2d,0x00);
    MHal_Demod_WriteReg(0x111f2c,0x01);

    MHal_Demod_WriteReg(0x111f2f,0x00);
    MHal_Demod_WriteReg(0x111f2e,0x00);

    MHal_Demod_WriteReg(0x111f31,0x00);
    MHal_Demod_WriteReg(0x111f30,0x01);

    MHal_Demod_WriteReg(0x111f33,0x3c);
    MHal_Demod_WriteReg(0x111f32,0x00);

    MHal_Demod_WriteReg(0x111f35,0x00);
    MHal_Demod_WriteReg(0x111f34,0x00);

    MHal_Demod_WriteReg(0x111f37,0x00);
    MHal_Demod_WriteReg(0x111f36,0x11);

    MHal_Demod_WriteReg(0x111f3b,0x00);
    MHal_Demod_WriteReg(0x111f3a,0x00);

    MHal_Demod_WriteReg(0x111f3d,0x00);
    MHal_Demod_WriteReg(0x111f3c,0x00);

    MHal_Demod_WriteReg(0x111f45,0x01);
    MHal_Demod_WriteReg(0x111f44,0x11);

    MHal_Demod_WriteReg(0x111f75,0x01);
    MHal_Demod_WriteReg(0x111f74,0x10);

    MHal_Demod_WriteReg(0x111f83,0x11);
    MHal_Demod_WriteReg(0x111f82,0x10);

    MHal_Demod_WriteReg(0x111f77,0xc0);
    MHal_Demod_WriteReg(0x111f76,0xc0);

    MHal_Demod_WriteReg(0x15298f,0xcc);
    MHal_Demod_WriteReg(0x15298e,0xcc);

    MHal_Demod_WriteReg(0x111f7b,0x81);
    MHal_Demod_WriteReg(0x111f7a,0x81);

    MHal_Demod_WriteReg(0x111f79,0x01);
    MHal_Demod_WriteReg(0x111f78,0x1c);

    MHal_Demod_WriteReg(0x111f7d,0x11);
    MHal_Demod_WriteReg(0x111f7c,0x11);

    MHal_Demod_WriteReg(0x111f7f,0x80);
    MHal_Demod_WriteReg(0x111f7e,0x41);

    MHal_Demod_WriteReg(0x111fe1,0x00);
    MHal_Demod_WriteReg(0x111fe0,0x08);

    MHal_Demod_WriteReg(0x111fe3,0x08);
    MHal_Demod_WriteReg(0x111fe2,0x10);

    MHal_Demod_WriteReg(0x111fe5,0x00);
    MHal_Demod_WriteReg(0x111fe4,0x08);

    MHal_Demod_WriteReg(0x111fe7,0x00);
    MHal_Demod_WriteReg(0x111fe6,0x08);

    MHal_Demod_WriteReg(0x111fe9,0x08);
    MHal_Demod_WriteReg(0x111fe8,0x10);

    MHal_Demod_WriteReg(0x111feb,0x00);
    MHal_Demod_WriteReg(0x111fea,0x00);

    MHal_Demod_WriteReg(0x111fed,0x00);
    MHal_Demod_WriteReg(0x111fec,0x00);

    MHal_Demod_WriteReg(0x111fef,0x00);
    MHal_Demod_WriteReg(0x111fee,0x88);

    MHal_Demod_WriteReg(0x111ff0,0x08);

    MHal_Demod_WriteReg(0x152991,0x00);
    MHal_Demod_WriteReg(0x152990,0x0c);

    MHal_Demod_WriteReg(0x111ff4,0x01);

    MHal_Demod_WriteReg(0x152997,0x00);
    MHal_Demod_WriteReg(0x152996,0x08);

    MHal_Demod_ReadReg(0x101ED0, &u8Temp);
    u8Temp|=0x10;
    MHal_Demod_WriteReg(0x101ED0, u8Temp);

    MHal_Demod_WriteReg(0x103c0e,0x01);
    // SRAM allocation 64K  avoid change souce from T2 failed.
    MHal_Demod_WriteReg(0x111701,0x00);
    MHal_Demod_WriteReg(0x111700,0x00);

    MHal_Demod_WriteReg(0x111705,0x00);
    MHal_Demod_WriteReg(0x111704,0x00);

    MHal_Demod_WriteReg(0x111703,0xff);
    MHal_Demod_WriteReg(0x111702,0xff);

    MHal_Demod_WriteReg(0x111707,0xff);
    MHal_Demod_WriteReg(0x111706,0xff);

    //Diff from TV tool
    MHal_Demod_WriteReg(0x111708,0x01);
    MHal_Demod_WriteReg(0x111709,0x00);

    MHal_Demod_WriteReg(0x11170a,0x0f);
    MHal_Demod_WriteReg(0x11170b,0x00);

    MHal_Demod_WriteReg(0x111718,0x02);
    MHal_Demod_WriteReg(0x111719,0x00);

    MHal_Demod_WriteReg(0x11171a,0x00);
    MHal_Demod_WriteReg(0x11171b,0x00);

    MHal_Demod_WriteReg(0x1117e0,0x14);
    MHal_Demod_WriteReg(0x1117e1,0x14);

    MHal_Demod_WriteReg(0x1117e4,0x00);
    MHal_Demod_WriteReg(0x1117e5,0x00);

    MHal_Demod_WriteReg(0x1117e6,0x00);
    MHal_Demod_WriteReg(0x1117e7,0x00);
    
    // SRAM End Address
    MHal_Demod_WriteReg(0x111707,0xff);
    MHal_Demod_WriteReg(0x111706,0xff);

    // DRAM Disable
    MHal_Demod_ReadReg(0x111718, &u8Temp);
    MHal_Demod_WriteReg(0x111718, u8Temp&(~0x04));

    MHal_Demod_WriteReg(0x103c0e,0x01);
    
    MHal_Demod_WriteReg(0x101e39,0x03);

    DBG_INTERN_DVBS(printk("INTERN_DVBS_InitClkgen\n"));
}

B16 DVBS_Download(void)
{
    DEMOD_DSP_PARAM_t stParam;
    U32 phyaddr=0, blksize=0, u32tmp;

    stParam.eDSP_Type = DEMOD_STANDARD_DVBS;

    DBG_INTERN_DVBS(printk("@DVBS_DOWNLOAD\n"));

    ///for DJB operation
    //MAdp_MPool_GetBlockPa(E_SYS_MMAP_ISDBT_TDI, &phyaddr, &blksize);
    
    phyaddr = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_ADDR];
    blksize = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_LEN];    
    // u32FwStartAddr must end with 0x8000 >>>>>
    u32tmp = (phyaddr & 0xFFFF);
    if (u32tmp > INTERNAL_DVBT2_DRAM_OFFSET)
        stParam.sDVBT2InitData.u32FwStartAddr = phyaddr - u32tmp + 0x10000 + INTERNAL_DVBT2_DRAM_OFFSET;
    else
        stParam.sDVBT2InitData.u32FwStartAddr = phyaddr - u32tmp + INTERNAL_DVBT2_DRAM_OFFSET;
    // <<<<< u32FwStartAddr must end with 0x8000

    stParam.sDVBT2InitData.u32TdiStartAddr = stParam.sDVBT2InitData.u32FwStartAddr + INTERNAL_DVBT2_FW_LEN;
    stParam.sDVBT2InitData.u32EqStartAddr = stParam.sDVBT2InitData.u32TdiStartAddr + INTERNAL_DVBT2_TDI_LEN;
    stParam.sDVBT2InitData.u32DjbStartAddr = stParam.sDVBT2InitData.u32EqStartAddr + INTERNAL_DVBT2_EQ_LEN;

    if ((stParam.sDVBT2InitData.u32DjbStartAddr + INTERNAL_DVBT2_DJB_LEN) > (phyaddr + blksize))
    {
        DBG_INTERN_DVBS(printk("DVBS_Download : memory use out of range !!\n"));
        return FALSE;
    }
    ///for DJB operation

    if (MHal_Demod_LoadDSPCode(stParam) == TRUE)
    {
        return TRUE;
    }
    else
    {
        DBG_INTERN_DVBS(printk(" @DVBS_DOWNLOAD FAILED\n"));
    }

    return FALSE;
}



B16 DVBS_DSPReg_Init(void)
{
    U8 idx = 0;

    for (idx = 0; idx<sizeof( INTERN_DVBS_DSPREG); idx++)
    {
        if ((idx == E_DMD_S2_FW_VERSION_H) || (idx == E_DMD_S2_FW_VERSION_L)) // version
            continue;
        if( MHal_Demod_MB_WriteDspReg(idx, INTERN_DVBS_DSPREG[idx])!=TRUE)//Driver update 2009/11/20
        {
            DBG_INTERN_DVBS(printk("dsp reg init NG \n"));
            return FALSE;
        }
    }
    DBG_INTERN_DVBS(printk("dsp reg init ok\n"));

    return TRUE;
}

B16 DVBS_System_Init(void)
{
    U8    status = TRUE;
    U8    u8Data = 0;
    U16   version = 0xffff;

    //// DVBS system variable init //////////
    pre_TS_CLK_DIV=0;
    pre_TS_CLK_source=0;
    
    //// MCU Reset //////////
    DBG_INTERN_DVBS(printk("DVBS_System_Init\n"));
    status &= DVBS_Reset();
    mdelay(50);//udelay(50 * 1000);

    //// Check Firmware Version //////////
    status &= DVBS_Get_Version(&version);
    DBG_INTERN_DVBS(printk("FW Version: 0x%4x\n", version));

    status &= DVBS_DSPReg_Init();

    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xD7, &u8Data);
    u8Data=(u8Data|0x80);
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xD7, u8Data);

    //IQ swap
    status &= MHal_Demod_MB_ReadReg(DMDANA_REG_BASE+0xC0, &u8Data);
    u8Data|=(0x02);
    status &= MHal_Demod_MB_WriteReg(DMDANA_REG_BASE+0xC0, u8Data);
    DBG_INTERN_DVBS(printk("DVBS_System_Init->IQ swap\n"));
 
    if(status)
    {
        return TRUE;
    }
    else
    {
        printk("DVBS System_Init NG \n");
        return FALSE;
    }
}
/****************************************************************************
  Subject:    To get the current DemodType at the DVB-S Demod
  Function:   INTERN_DVBS_GetCurrentDemodType
  Parmeter:   pointer for return DVBS/DVBS2 type

  Return:     TRUE
              FALSE
  Remark:
*****************************************************************************/
BOOL INTERN_DVBS_GetCurrentDemodType(DMD_DVBS_DEMOD_TYPE *pDemodType)
{
    U8 u8Data=0;
    BOOL     status = true;

    DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentDemodType\n"));

    //MDrv_SYS_DMD_VD_MBX_ReadReg(DVBS2_INNER_REG_BASE + 0x80, &u8Data);//status &= MDrv_SYS_DMD_VD_MBX_ReadReg(DVBS2_INNER_REG_BASE + 0x80, &u8Data);
    //printk(">>> INTERN_DVBS_GetCurrentDemodType INNER 0x40 = 0x%x <<<\n", u8Data);
    //if ((u8Data & 0x01) == 0)
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
    if(!u8Data)                                                       //S2
    {
        *pDemodType = DMD_SAT_DVBS2;
        DBG_INTERN_DVBS(printk("[dvbs]DemodType=DVBS2\n"));
    }
    else                                                                            //S
    {
        *pDemodType = DMD_SAT_DVBS;
        DBG_INTERN_DVBS(printk("[dvbs]DemodType=DVBS\n"));
    }
    return status;
}

/************************************************************************************************
Subject:    Clk Inversion control
Function:   DVBS_Clk_Inversion_Control
Parmeter:   bInversionEnable : TRUE For Inversion Action
Return:      void
Remark:
 *************************************************************************************************/
void DVBS_Clk_Inversion_Control(B16 bInversionEnable)
{
    U8   u8Temp=0;

    MHal_Demod_ReadReg(0x103301, &u8Temp);


    if (bInversionEnable)
    {
        u8Temp = u8Temp | 0x08; //bit 11: clk inv
    }
    else
    {
        u8Temp = u8Temp & (~0x02);
    }

    DBG_INTERN_DVBS(printk("---> Inversion(Bit11) = 0x%x \n",u8Temp));

    MHal_Demod_WriteReg(0x103301, u8Temp);

}

/************************************************************************************************
  Subject:    channel change config
  Function:   DVBS_Config (DVBS)
  Parmeter:   BW: bandwidth
  Return:     B16 :
  Remark:
*************************************************************************************************/
B16 DVBS_Config(DMD_DVBS_Info_T *pInDVBSInfo, U16 *pu16_symbol_rate_list, U8 u8_symbol_rate_list_num)
{
    BOOL    status= true;
    //U16   u16CenterFreq;
    //MS_U16  u16Fc = 0;
    U8    temp_val=0;
    U8    u8Data;
    U8    u8counter = 0;
    //MS_U32  u32SR;

    U32 u32SymbolRate = pInDVBSInfo->u32SymbolRate;
    BOOL bSpecInv = pInDVBSInfo->bSpecInv;
    BOOL bSerialTS = pInDVBSInfo->bSerialTS;
    //U8 u8TSClk = pInDVBSInfo->u8TSClk;

    //u16CenterFreq  =u32IFFreq;
    //DBG_INTERN_DVBS(printk(" @INTERN_DVBS_config+, SR=%ld, QAM=%d, u32IFFreq=%ld, bSpecInv=%d, bSerialTS=%d, u8TSClk=%d\n", u32SymbolRate, eQamMode, u32IFFreq, bSpecInv, bSerialTS, u8TSClk));
    //DBG_INTERN_DVBS(printk("INTERN_DVBS_Config, t = %ld\n",MsOS_GetSystemTime()));

    //u8TSClk=0x14;//if (u8TSClk == 0xFF) u8TSClk=0x13;
    status &= DVBS_Reset();

    DBG_INTERN_DVBS(printk("u32SymbolRate %d\n", u32SymbolRate));
    DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_Config u32SymbolRate %d<<<\n", u32SymbolRate));

    u32SymbolRate=u32SymbolRate/1000;
    // Symbol Rate
    u8Data=(u32SymbolRate&0xFF);
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_L, u8Data);
    u8Data=((u32SymbolRate>>8)&0xFF);
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_H, u8Data);
    u8Data=((u32SymbolRate>>16)&0xFF);
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DMDTOP_DBG_5, u8Data);

     
    //IQ swap
    status &= MHal_Demod_MB_ReadReg(DMDANA_REG_BASE+0xC0, &u8Data);
    u8Data|=0x02;
    status &= MHal_Demod_MB_WriteReg(DMDANA_REG_BASE+0xC0, u8Data);
    DBG_INTERN_DVBS(printk("dvbs_config IQ setting\n"));
        
    // IQ Swap
    //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_IQ_SWAP, bSpecInv? 0x01:0x00);
    if(bSpecInv)
    {
        status &= MHal_Demod_MB_ReadReg(DMDANA_REG_BASE+0xC0, &u8Data);
        u8Data|=0x02;
        status &= MHal_Demod_MB_WriteReg(DMDANA_REG_BASE+0xC0, u8Data);
    }

    u8DemodLockFlag=0;

    // TS mode
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_TS_SERIAL, bSerialTS? 0x01:0x00);
    DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_Config TS Serial %d<<<\n", bSerialTS));
    _bSerialTS = bSerialTS;

    if (bSerialTS)
    {
        // serial
        MHal_Demod_WriteReg(0x103308, 0x01);   
        MHal_Demod_WriteReg(0x103309, 0x04);   

        MHal_Demod_WriteReg(0x103300, 0x00);   
#if(INTERN_DVBS_TS_SERIAL_INVERSION == 0)
        MHal_Demod_ReadReg(0x103301, &temp_val);
    temp_val|=0x04;
        MHal_Demod_WriteReg(0x103301,temp_val);
#else
        MHal_Demod_ReadReg(0x103301, &temp_val);
    temp_val|=0x07;
        MHal_Demod_WriteReg(0x103301,temp_val);
#endif
    }
    else
    {
        //parallel
        MHal_Demod_WriteReg(0x103308, 0x01);   
        MHal_Demod_WriteReg(0x103309, 0x00);   

        //MHal_Demod_WriteReg(0x103300, 0x11);   
        //MHal_Demod_WriteReg(0x103300, u8TSClk);  
#if(INTERN_DVBS_TS_PARALLEL_INVERSION == 0)
        MHal_Demod_ReadReg(0x103301, &temp_val);
    temp_val|=0x05;
        MHal_Demod_WriteReg(0x103301,temp_val);
#else
        //MHal_Demod_ReadReg(0x103301, &temp_val);
    //temp_val|=0x07;
        //MHal_Demod_WriteReg(0x103301,temp_val);
#endif
    }

    //-----------------------------------------------------------
/*    
    //From INTERN_DVBS_Demod_Restart function.
    DVBS_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);//DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
    //status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_TOP_WR_DBG_90, &u8Data);
    u8Data&=0xF0;
    DVBS_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);//DVBS_WriteReg(TOP_REG_BASE + 0x5B*2, u8Data);
    //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_90, u8Data);
     udelay(30*1000);//MsOS_DelayTask(30);
*/    

    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);//DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
    u8Data&=0xF0;
    u8Data|=0x01;
    MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);//DVBS_WriteReg(TOP_REG_BASE + 0x5B*2, u8Data);

    u8counter = 20;
    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);//DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data); DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_config FW Restart=%d<<<\n", u8Data));
    while( ((u8Data&0x01) == 0x00) && (u8counter != 0) )
    {
        mdelay(10);//udelay(10*1000);//MsOS_DelayTask(10);
        //printk("TOP_WR_DBG_90=0x%x, status=%d, u8counter=%d\n", u8Data, status, u8counter);
        u8Data|=0x01;
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);//DVBS_WriteReg(TOP_REG_BASE + 0x5B*2, u8Data);
        MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);//DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
        //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_90, u8Data);
        //status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_TOP_WR_DBG_90, &u8Data);
        u8counter--;
    }

    if((u8Data & 0x01)==0x00)
    {
        status = FALSE;
    }

    DBG_INTERN_DVBS(printk("INTERN_DVBS_config-\n"));
    return status;
}

// let demod run
B16 DVBS_Active(B16 bEnable)
{
    U8   status = TRUE;
    //MS_U8 u8Data;

    DBG_INTERN_DVBS(printk(" @INTERN_DVBS_Active\n"));

    //MHal_Demod_WriteReg(MBRegBase + (0x0e)*2, 0x01);
    //MHal_Demod_WriteReg(0x112600 + (0x0e)*2, 0x01);   // FSM_EN


    bDMD_DVBS_NoChannelDetectedWithRFPower = FALSE;
    u32DMD_DVBS_NoChannelTimeAccWithRFPower = 0;
    return status;
}



/****************************************************************************
  Subject:    To get the current symbol rate at the DVB-S Demod
  Function:   INTERN_DVBS_GetCurrentSymbolRate
  Parmeter:   pointer pData for return Symbolrate

  Return:     TRUE
              FALSE
  Remark:
*****************************************************************************/
BOOL INTERN_DVBS_GetCurrentSymbolRate(U32 *u32SymbolRate)
{
    U8  tmp = 0;
    U16 u16SymbolRateTmp = 0;

    MHal_Demod_MB_ReadDspReg(E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_H, &tmp);
    u16SymbolRateTmp = tmp;
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_L, &tmp);
    u16SymbolRateTmp = (u16SymbolRateTmp<<8)|tmp;

    MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_DBG_5, &tmp);
    *u32SymbolRate = (tmp<<16)|u16SymbolRateTmp;

    DBG_INTERN_DVBS_LOCK(printk("[dvbs]Symbol Rate=%d\n",*u32SymbolRate));//

    return TRUE;
}


/****************************************************************************
  Subject:    To get the current modulation type at the DVB-S Demod
  Function:   INTERN_DVBS_GetCurrentModulationType
  Parmeter:   pointer for return QAM type

  Return:     TRUE
              FALSE
  Remark:
*****************************************************************************/
BOOL DVBS_GetCurrentModulationType(DMD_DVBS_MODULATION_TYPE *pQAMMode)
{
    U8  u8Data=0;
    BOOL    status = true;
    //DMD_DVBS_DEMOD_TYPE pDemodType = 0;

    DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType\n"));

    MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
    if(u8Data)
    {
        *pQAMMode = DMD_DVBS_QPSK;
        DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType [dvbs]ModulationType=DVBS_QPSK\n"));//DBG_INTERN_DVBS_LOCK(printk("[dvbs]ModulationType=DVBS_QPSK\n"));
        //return TRUE;
    }
    else                                        //S2
    {
        MHal_Demod_MB_ReadReg(DVBS2OPPRO_REG_BASE + 0x45*2 + 1, &u8Data);
        u8Data = (u8Data>>4) & 0x07;
        if(u8Data==0x00)
        {
            *pQAMMode = DMD_DVBS_QPSK; modulation_order=2;
            DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType [dvbs2]ModulationType=DVBS2_QPSK\n"));
        }
        else if(u8Data==0x01) 
        {
            *pQAMMode = DMD_DVBS_8PSK; modulation_order=3;
            DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType [dvbs2]ModulationType=DVBS2_8PSK\n"));
        }
        else if(u8Data==0x02)
        {
            *pQAMMode = DMD_DVBS_16APSK; modulation_order=4;
            DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType [dvbs2]ModulationType=DVBS2_16APSK\n"));
        }
        else if(u8Data==0x03)
        {
            *pQAMMode = DMD_DVBS_32APSK; modulation_order=5;
            DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType [dvbs2]ModulationType=DVBS2_32APSK\n"));
        }
        else if(u8Data==0x04)
        {
            *pQAMMode = DMD_DVBS_8APSK; modulation_order=3;
            DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType [dvbs2]ModulationType=DVBS2_8APSK\n"));
        }
        else if(u8Data==0x05)
        {
            *pQAMMode = DMD_DVBS_8_8APSK; modulation_order=4;
            DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType [dvbs2]ModulationType=DVBS2_8_8APSK\n"));
        }
        else if(u8Data==0x06)
        {
            *pQAMMode = DMD_DVBS_4_8_4_16APSK; modulation_order=5;
            DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentModulationType [dvbs2]ModulationType=DVBS2_4_8_4_16APSK\n"));
        }
    }
    return status;
/*#else
    *pQAMMode = DMD_DVBS_QPSK;
    printk("[dvbs]ModulationType=DVBS_QPSK\n");//DBG_INTERN_DVBS_LOCK(printk("[dvbs]ModulationType=DVBS_QPSK\n"));
    //return true;
#endif*/
}

BOOL INTERN_DVBS_GetCurrentDemodCodeRate(DMD_DVBS_CODE_RATE_TYPE *pCodeRate)
{
    U8    u8Data = 0;//, u8_gCodeRate = 0;
    BOOL  status = true;
    U8    S2_FEC_TYPE=0;

    DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentCodeRate\n"));
    //DMD_DVBS_DEMOD_TYPE pDemodType = 0;
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
    if(!u8Data)//S2 or S2X
    {
        MHal_Demod_MB_ReadReg(DVBS2OPPRO_REG_BASE + 0x45*2 + 1, &S2_FEC_TYPE);
        S2_FEC_TYPE &=0x01;

        if(S2_FEC_TYPE==0)
            s2_fec_frame=64800;
        else
            s2_fec_frame=16200;
        
        MHal_Demod_MB_ReadReg(DVBS2OPPRO_REG_BASE + 0x40*2 + 1, &u8Data);
        u8Data &=0x3F;
        switch (u8Data)
        {
            case 0x00:
                *pCodeRate = DMD_CONV_CODE_RATE_1_4;
                if(S2_FEC_TYPE==0)
                    k_bch=16008;
                else
                    k_bch=3072;
                //_u8_DVBS2_CurrentCodeRate = 8;//3
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=1_4\n"));
                break;
            case 0x01:
                *pCodeRate = DMD_CONV_CODE_RATE_1_3;
                if(S2_FEC_TYPE==0)
                    k_bch=21408;
                else
                    k_bch=5232;                
                //_u8_DVBS2_CurrentCodeRate = 6;//1
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=1_3\n"));
                break;
            case 0x02:
                *pCodeRate = DMD_CONV_CODE_RATE_2_5;
                if(S2_FEC_TYPE==0)
                    k_bch=25728;
                else
                    k_bch=6312;                   
                //_u8_DVBS2_CurrentCodeRate = 10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=2_5\n"));
                break;
            case 0x03:
                *pCodeRate = DMD_CONV_CODE_RATE_1_2;
                if(S2_FEC_TYPE==0)
                    k_bch=32208;
                else
                    k_bch=7032;    
                //_u8_DVBS2_CurrentCodeRate = 5;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=1_2\n"));
                break;
            case 0x04:
                *pCodeRate = DMD_CONV_CODE_RATE_3_5;
                if(S2_FEC_TYPE==0)
                    k_bch=38688;
                else
                    k_bch=9552;    
                //_u8_DVBS2_CurrentCodeRate = 11;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=3_5\n"));
                break;
            case 0x05:
                *pCodeRate = DMD_CONV_CODE_RATE_2_3;
                if(S2_FEC_TYPE==0)
                    k_bch=43040;
                else
                    k_bch=10632;                   
                //_u8_DVBS2_CurrentCodeRate = 7;//2;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=2_3\n"));
                break;
            case 0x06:
                *pCodeRate = DMD_CONV_CODE_RATE_3_4;
                if(S2_FEC_TYPE==0)
                    k_bch=48408;
                else
                    k_bch=11712;                   
                //_u8_DVBS2_CurrentCodeRate = 9;//4
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=3_4\n"));
                break;
            case 0x07:
                *pCodeRate = DMD_CONV_CODE_RATE_4_5;
                if(S2_FEC_TYPE==0)
                    k_bch=51648;
                else
                    k_bch=12432;                  
                //_u8_DVBS2_CurrentCodeRate = 12;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=4_5\n"));
                break;
            case 0x08:
                *pCodeRate = DMD_CONV_CODE_RATE_5_6;
                if(S2_FEC_TYPE==0)
                    k_bch=53840;
                else
                    k_bch=13152;                    
                //_u8_DVBS2_CurrentCodeRate = 13;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=5_6\n"));
                break;
            case 0x09:
                *pCodeRate = DMD_CONV_CODE_RATE_8_9;
                if(S2_FEC_TYPE==0)
                    k_bch=57472;
                else
                    k_bch=14232;                     
                //_u8_DVBS2_CurrentCodeRate = 14;//9;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=8_9\n"));
                break;
            case 0x0A:
                *pCodeRate = DMD_CONV_CODE_RATE_9_10;
                k_bch=58192;
                //_u8_DVBS2_CurrentCodeRate = 15;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=9_10\n"));
                break;
            case 0x0B:
                *pCodeRate = DMD_CONV_CODE_RATE_2_9;
                k_bch=14208;
                //_u8_DVBS2_CurrentCodeRate = 16;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=2_9\n"));
                break;
            case 0x0C:
                *pCodeRate = DMD_CONV_CODE_RATE_13_45;
                k_bch=18528;
                //_u8_DVBS2_CurrentCodeRate = 17;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=13_45\n"));
                break;
            case 0x0D:
                *pCodeRate = DMD_CONV_CODE_RATE_9_20;
                k_bch=28968;
                //_u8_DVBS2_CurrentCodeRate = 18;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=9_20\n"));
                break;
            case 0x0E:
                *pCodeRate = DMD_CONV_CODE_RATE_90_180;
                k_bch=32208;
                //_u8_DVBS2_CurrentCodeRate = 19;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=90_180\n"));
                break;
            case 0x0F:
                *pCodeRate = DMD_CONV_CODE_RATE_96_180;
                k_bch=34368;
                //_u8_DVBS2_CurrentCodeRate =20;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=96_180\n"));
                break;
            case 0x10:
                *pCodeRate = DMD_CONV_CODE_RATE_11_20;
                k_bch=35448;
                //_u8_DVBS2_CurrentCodeRate = 21;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=11_20\n"));
                break;
            case 0x11:
                *pCodeRate = DMD_CONV_CODE_RATE_100_180;
                k_bch=35808;
                //_u8_DVBS2_CurrentCodeRate = 22;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=100_180\n"));
                break;
            case 0x12:
                *pCodeRate = DMD_CONV_CODE_RATE_104_180;
                k_bch=37248;
                //_u8_DVBS2_CurrentCodeRate = 23;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=104_180\n"));
                break;
            case 0x13:
                *pCodeRate = DMD_CONV_CODE_RATE_26_45_L;
                k_bch=37248;
                //_u8_DVBS2_CurrentCodeRate = 24;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=26_45_L\n"));
                break;
            case 0x14:
                *pCodeRate = DMD_CONV_CODE_RATE_18_30;
                k_bch=38688;
                //_u8_DVBS2_CurrentCodeRate = 25;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=18_30\n"));
                break;
            case 0x15:
                *pCodeRate = DMD_CONV_CODE_RATE_28_45;
                k_bch=40128;
                //_u8_DVBS2_CurrentCodeRate = 26;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=28_45\n"));
                break;
            case 0x16:
                *pCodeRate = DMD_CONV_CODE_RATE_23_36;
                k_bch=41208;
                //_u8_DVBS2_CurrentCodeRate = 27;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=23_36\n"));
                break;
            case 0x17:
                *pCodeRate = DMD_CONV_CODE_RATE_116_180;
                k_bch=41568;
                //_u8_DVBS2_CurrentCodeRate = 28;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=116_180\n"));
                break;
            case 0x18:
                *pCodeRate = DMD_CONV_CODE_RATE_20_30;
                k_bch=43008;
                //_u8_DVBS2_CurrentCodeRate = 29;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=20_30\n"));
                break;
            case 0x19:
                *pCodeRate = DMD_CONV_CODE_RATE_124_180;
                k_bch=44448;
                //_u8_DVBS2_CurrentCodeRate = 30;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=124_180\n"));
                break;
            case 0x1A:
                *pCodeRate = DMD_CONV_CODE_RATE_25_36;
                k_bch=44808;
                //_u8_DVBS2_CurrentCodeRate = 31;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=25_36\n"));
                break;
            case 0x1B:
                *pCodeRate = DMD_CONV_CODE_RATE_128_180;
                k_bch=45888;
                //_u8_DVBS2_CurrentCodeRate = 32;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=128_180\n"));
                break;
            case 0x1C:
                *pCodeRate = DMD_CONV_CODE_RATE_13_18;
                k_bch=46608;
                //_u8_DVBS2_CurrentCodeRate = 33;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=13_18\n"));
                break;
            case 0x1D:
                *pCodeRate = DMD_CONV_CODE_RATE_132_180;
                k_bch=47328;
                //_u8_DVBS2_CurrentCodeRate = 34;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=132_180\n"));
                break;
            case 0x1E:
                *pCodeRate = DMD_CONV_CODE_RATE_22_30;
                k_bch=47328;
                //_u8_DVBS2_CurrentCodeRate = 35;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=22_30\n"));
                break;
            case 0x1F:
                *pCodeRate = DMD_CONV_CODE_RATE_135_180;
                k_bch=48408;
                //_u8_DVBS2_CurrentCodeRate = 36;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=135_180\n"));
                break;
            case 0x20:
                *pCodeRate = DMD_CONV_CODE_RATE_140_180;
                k_bch=50208;
                //_u8_DVBS2_CurrentCodeRate = 37;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=140_180\n"));
                break;
            case 0x21:
                *pCodeRate = DMD_CONV_CODE_RATE_7_9;
                k_bch=50208;
                //_u8_DVBS2_CurrentCodeRate = 38;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=7_9\n"));
                break;
            case 0x22:
                *pCodeRate = DMD_CONV_CODE_RATE_154_180;
                k_bch=55248;
                //_u8_DVBS2_CurrentCodeRate = 39;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=154_180\n"));
                break;
            case 0x23:
                *pCodeRate = DMD_CONV_CODE_RATE_11_45;
                k_bch=3792;
                //_u8_DVBS2_CurrentCodeRate = 40;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=11_45\n"));
                break;
            case 0x24:
                *pCodeRate = DMD_CONV_CODE_RATE_4_15;
                k_bch=4152;
                //_u8_DVBS2_CurrentCodeRate = 41;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=4_15\n"));
                break;
            case 0x25:
                *pCodeRate = DMD_CONV_CODE_RATE_14_45;
                k_bch=4872;
                //_u8_DVBS2_CurrentCodeRate = 42;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=14_45\n"));
                break;
            case 0x26:
                *pCodeRate = DMD_CONV_CODE_RATE_7_15;
                k_bch=7392;
                //_u8_DVBS2_CurrentCodeRate = 43;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=7_15\n"));
                break;
            case 0x27:
                *pCodeRate = DMD_CONV_CODE_RATE_8_15;
                k_bch=8472;
                //_u8_DVBS2_CurrentCodeRate = 44;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=8_15\n"));
                break;
            case 0x28:
                *pCodeRate = DMD_CONV_CODE_RATE_26_45_S;
                k_bch=9192;
                //_u8_DVBS2_CurrentCodeRate = 45;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=26_45_S\n"));
                break;
            case 0x29:
                *pCodeRate = DMD_CONV_CODE_RATE_32_45;
                k_bch=11352;
                //_u8_DVBS2_CurrentCodeRate = 46;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=32_45\n"));
                break;                
            default:
                *pCodeRate = DMD_CONV_CODE_RATE_9_10;
                k_bch=58192;
                //_u8_DVBS2_CurrentCodeRate = 15;//10;
                DBG_INTERN_DVBS(printk("INTERN_DVBS2_GetCurrentCodeRate=DVBS2_Default\n"));
        }
    }
    else                                            //S
    {
        MHal_Demod_MB_ReadDspReg(E_DMD_S2_CODE_RATE, &u8Data);
        switch (u8Data)
        {
            case 0x00:
                *pCodeRate = DMD_CONV_CODE_RATE_1_2;
                //_u8_DVBS2_CurrentCodeRate = 0;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentCodeRate=1_2\n"));
                break;
            case 0x01:
                *pCodeRate = DMD_CONV_CODE_RATE_2_3;
                //_u8_DVBS2_CurrentCodeRate = 1;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentCodeRate=2_3\n"));
                break;
            case 0x02:
                *pCodeRate = DMD_CONV_CODE_RATE_3_4;
                //_u8_DVBS2_CurrentCodeRate = 2;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentCodeRate=3_4\n"));
                break;
            case 0x03:
                *pCodeRate = DMD_CONV_CODE_RATE_5_6;
                //_u8_DVBS2_CurrentCodeRate = 3;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentCodeRate=5_6\n"));
                break;
            case 0x04:
                *pCodeRate = DMD_CONV_CODE_RATE_7_8;
                //_u8_DVBS2_CurrentCodeRate = 4;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentCodeRate=7_8\n"));
                break;
            default:
                *pCodeRate = DMD_CONV_CODE_RATE_7_8;
                //_u8_DVBS2_CurrentCodeRate = 4;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentCodeRate=DVBS_Default\n"));
        }
    }
    return status;
}

BOOL INTERN_DVBS_Get_FreqOffset(S32 *pFreqOff, U8 u8BW)
{
    U8    u8Data=0;
    U16   u16Data;
    S16   s16CFO;
    //float       FreqOffset;
    //MS_U32      u32FreqOffset = 0;
    //MS_U8       reg = 0;
    BOOL    status = TRUE;
    MS_S64    dividend64;
    MS_S32    divisor32;
    MS_S32    remainder;  

    DBG_INTERN_DVBS(printk(">>> INTERN_DVBS_Get_FreqOffset DVBS_Estimated_CFO <<<\n"));
    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_INFO_04, &u8Data);
    u16Data=u8Data;
    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_INFO_03, &u8Data);
    u16Data=(u16Data<<8)|u8Data;//Center_Freq_Offset
    if (u16Data >= 0x8000)
    {
        u16Data=0x10000- u16Data;
        s16CFO=-1*u16Data;
    }
    else
    {
        s16CFO=u16Data;
    }
    DBG_INTERN_DVBS(printk(">>> INTERN_DVBS_Get_FreqOffset CFO = %d[KHz] <<<\n", s16CFO));
    dividend64 = s16CFO;
    divisor32 = 1000;
    div_s64_rem(dividend64, divisor32, &remainder);
    DBG_INTERN_DVBS(printk(">>> INTERN_DVBS_Get_FreqOffset CFO  dividend64 = %lld[KHz]   divisor64 =%d  remainder =%d<<<\n", dividend64, divisor32, remainder));
    if((remainder >= 500)||(remainder <= -500))
    {
        if(s16CFO < 0)
            *pFreqOff=(s16CFO/1000)-1;
        else
            *pFreqOff=(s16CFO/1000)+1;
    }
    else
        *pFreqOff = s16CFO/1000;
    DBG_INTERN_DVBS(printk(">>> INTERN_DVBS_Get_FreqOffset *pFreqOff = %d[MHz] <<<\n", (MS_S16)*pFreqOff));
    // no use.
    u8BW = u8BW;
    return status;
}


BOOL INTERN_DVBS_GetTsDivNum(U32* u8TSDivNum,BOOL*tssouce)
{
    U8       u8Data = 0;
    BOOL     status = true;
    U32      u32SymbolRate=0;
    //float       fSymbolRate;
    MS_U64    TS_Data_Rate=0;
    MS_U64    TS_Data_Rate_add_percent=0;
    //float    high_percent=1.085;
    //float    low_percent=1.01;
    U8    percent_setting=101;
    //float    TS_constant_div=0;
    U32    TS_constant_div_288=0;
    U32    TS_constant_div_432=0;
    U32    Percent_288=0;
    U32    Percent_432=0;
    //float     TS_DATA_RATE_MARGIN_RATIO = 1.01;
    MS_U64     dividend64;
    MS_U64     divisor64;
    MS_U64     u64Data;
    U8    pilot_flag;
    U32     current_time=0;
    U8       VCM_OPT=0;

    DMD_DVBS_MODULATION_TYPE pQAMMode;
    DMD_DVBS_CODE_RATE_TYPE   code_rate;

    INTERN_DVBS_GetCurrentSymbolRate(&u32SymbolRate);
    DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum u32SymbolRate=%d\n", u32SymbolRate));//

    MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
    DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum GetCurrentDemodType E_DMD_S2_SYSTEM_TYPE=%d\n", u8Data));//
    if(!u8Data)
    {
        if( DVBS2_VCM_CHECK() ) // VCM signal
        {
            // wait current IS ID which we want to get
            current_time = (U32)jiffies_to_msecs(jiffies);
            do
            {
                status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_VCM_OPT, &VCM_OPT);
            }
            while( VCM_OPT != 0x02 && ((U32)jiffies_to_msecs(jiffies) - current_time) < 500);
        }
        INTERN_DVBS_GetCurrentDemodCodeRate(&code_rate);
        DVBS_GetCurrentModulationType(&pQAMMode);
        //printk("INTERN_DVBS_GetTsDivNum Mod_order=%d\n", modulation_order);
        DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum Mod_order=%d\n",modulation_order));

        MHal_Demod_MB_ReadDspReg(E_DMD_S2_PILOT_FLAG, &pilot_flag);
        if(pilot_flag) // Pilot ON
            DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetTsDivNum Pilot ON<<<\n"));
        else //Pilot off
            DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetTsDivNum Pilot OFF<<<\n"));


        dividend64 = u32SymbolRate*1000;
        dividend64 = dividend64*(k_bch-80)*modulation_order;
        divisor64 = s2_fec_frame+90*modulation_order+(s2_fec_frame*36)/1440*pilot_flag;
        DBG_INTERN_DVBS(printk("pilot off dividend64 =%lld\n", dividend64));
        DBG_INTERN_DVBS(printk("pilot off divisor64 =%lld\n", divisor64));
        TS_Data_Rate = div64_u64(dividend64, divisor64);        

    }
    else                                            //S
    {
        MHal_Demod_MB_ReadDspReg(E_DMD_S2_CODE_RATE, &u8Data);
        DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum DVBS E_DMD_S2_CODERATE=0x%x\n", u8Data));
        switch (u8Data)
        {
            case 0x00: //CR 1/2
                _u8_DVBS2_CurrentCodeRate = 0;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum INTERN_DVBS_GetCurrentCodeRate=1_2\n"));
                 //TS_Data_Rate =(1*188*2*u32SymbolRate*1000)/(204*2);
                dividend64 = u32SymbolRate*1000;
                dividend64 = 1*188*2*dividend64;
                divisor64 = 204*2;
                TS_Data_Rate = div64_u64(dividend64, divisor64);                
            break;
            case 0x01: //CR 2/3
                _u8_DVBS2_CurrentCodeRate = 1;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum INTERN_DVBS_GetCurrentCodeRate=2_3\n"));
                //TS_Data_Rate =(2*188*2*u32SymbolRate*1000)/(204*3);
                dividend64 = u32SymbolRate*1000;
                dividend64 = 2*188*2*dividend64;
                divisor64 = 204*3;
                TS_Data_Rate = div64_u64(dividend64, divisor64);                  
            break;
            case 0x02: //CR 3/4
                _u8_DVBS2_CurrentCodeRate = 2;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum INTERN_DVBS_GetCurrentCodeRate=3_4\n"));
                //TS_Data_Rate =(3*188*2*u32SymbolRate*1000)/(204*4);
                dividend64 = u32SymbolRate*1000;
                dividend64 = 3*188*2*dividend64;
                divisor64 = 204*4;
                TS_Data_Rate = div64_u64(dividend64, divisor64);                  
            break;
            case 0x03: //CR 5/6
                _u8_DVBS2_CurrentCodeRate = 3;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum INTERN_DVBS_GetCurrentCodeRate=5_6\n"));
                //TS_Data_Rate =(5*188*2*u32SymbolRate*1000)/(204*6);
                dividend64 = u32SymbolRate*1000;
                dividend64 = 5*188*2*dividend64;
                divisor64 = 204*6;
                TS_Data_Rate = div64_u64(dividend64, divisor64);                  
            break;
            case 0x04: //CR 7/8
                _u8_DVBS2_CurrentCodeRate = 4;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum INTERN_DVBS_GetCurrentCodeRate=7_8\n"));
                //TS_Data_Rate =(7*188*2*u32SymbolRate*1000)/(204*8);
                dividend64 = u32SymbolRate*1000;
                dividend64 = 7*188*2*dividend64;
                divisor64 = 204*8;
                TS_Data_Rate = div64_u64(dividend64, divisor64);                  
            break;
            default:
                _u8_DVBS2_CurrentCodeRate = 4;
                DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum INTERN_DVBS_GetCurrentCodeRate= default 7_8\n"));
                //TS_Data_Rate =(7*188*2*u32SymbolRate*1000)/(204*8);
                dividend64 = u32SymbolRate*1000;
                dividend64 = 7*188*2*dividend64;
                divisor64 = 204*8;
                TS_Data_Rate = div64_u64(dividend64, divisor64);                  
            break;
        }
    } //printk("INTERN_DVBS_GetTsDivNum u8TSClk = 0x%x\n", *u8TSDivNum);
     
    DBG_INTERN_DVBS(printk("TS_Data_Rate =%lld\n", TS_Data_Rate));
    TS_Data_Rate_add_percent=TS_Data_Rate*percent_setting;
    DBG_INTERN_DVBS(printk("TS_Data_Rate_add_percent =%lld\n", TS_Data_Rate_add_percent));

    u64Data = 54000000UL;
    u64Data = u64Data*percent_setting; 
    if(TS_Data_Rate_add_percent<u64Data)
    {
        //TS_constant_div = 144000*1000/((float)TS_Data_Rate*TS_DATA_RATE_MARGIN_RATIO/8)-1.0;
        //TS_constant_div_288 = (288000000*16*100*SHIFT_MULTIPLE)/(TS_Data_Rate_add_percent*SHIFT_MULTIPLE)-1;
        dividend64=144000000;
        dividend64=dividend64*8*100;
        TS_constant_div_288 = div64_u64(dividend64, TS_Data_Rate_add_percent)-1;
        *u8TSDivNum=TS_constant_div_288;
        DBG_INTERN_DVBS(printk("*u8TSDivNum=%d\n", *u8TSDivNum));
        *tssouce=0;
        return status;
    }

    //TS_constant_div_288= (288000000*16*100*SHIFT_MULTIPLE)/(TS_Data_Rate_add_percent*SHIFT_MULTIPLE)-1;
    //TS_constant_div_432= (432000000*16*100*SHIFT_MULTIPLE)/(TS_Data_Rate_add_percent*SHIFT_MULTIPLE)-1;
    dividend64 = 144000000;
    dividend64 = dividend64*8*100;
    divisor64 = TS_Data_Rate_add_percent;
    DBG_INTERN_DVBS(printk("TS_constant_div_288 dividend64 =%lld\n", dividend64));
    DBG_INTERN_DVBS(printk("TS_constant_div_288 divisor64 =%lld\n", divisor64));
    TS_constant_div_288 = div64_u64(dividend64, divisor64)-1;
    dividend64 = 216000000;
    dividend64 = dividend64*8*100;
    divisor64 = TS_Data_Rate_add_percent;
    DBG_INTERN_DVBS(printk("TS_constant_div_432 dividend64 =%lld\n", dividend64));
    DBG_INTERN_DVBS(printk("TS_constant_div_432 divisor64 =%lld\n", divisor64));
    TS_constant_div_432 = div64_u64(dividend64, TS_Data_Rate_add_percent)-1;
    
    DBG_INTERN_DVBS(printk("TS_constant_div_288 =%d, TS_constant_div_432=%d\n", TS_constant_div_288,TS_constant_div_432));

    //Percent_288=(144000000*8*100*SHIFT_MULTIPLE)*1000/(TS_Data_Rate_add_percent*(TS_constant_div_288+1)*SHIFT_MULTIPLE);
    //Percent_432=(216000000*8*100*SHIFT_MULTIPLE)*1000/(TS_Data_Rate_add_percent*(TS_constant_div_432+1)*SHIFT_MULTIPLE);
    dividend64=144000000;
    dividend64=dividend64*8*100*1000;//gain 1000
    divisor64=TS_Data_Rate_add_percent*(TS_constant_div_288+1);
    Percent_288=div64_u64(dividend64, divisor64);
    dividend64=216000000;
    dividend64=dividend64*8*100*1000;//gain 1000
    divisor64=TS_Data_Rate_add_percent*(TS_constant_div_432+1);
    Percent_432=div64_u64(dividend64, divisor64);

    DBG_INTERN_DVBS(printk("Percent_288 =%d, Percent_432=%d\n", Percent_288,Percent_432));
       
       
    if(Percent_288>Percent_432)
    {
        *u8TSDivNum=TS_constant_div_432;
        *tssouce=1;
        DBG_INTERN_DVBS(printk("Percent_288>Percent_432\n"));
    }
    else
    {
        *u8TSDivNum=TS_constant_div_288;
        *tssouce=0;
        DBG_INTERN_DVBS(printk("Percent_288<Percent_432\n"));
    }
    return status;
}


BOOL DVBS_GetLock(DMD_DVBS_GETLOCK_TYPE eType,  U32 u32TimeInterval)
{
    //float Est_CFO; //test CFO
    U8 u8Data =0; //MS_U8 u8Data2 =0;
    U32 u32Data = 0;
    BOOL bRet = TRUE;
    U8 cData = 0;
    //U8 cBitMask = 0;
    U16 FW_locking_time=0;
    BOOL tssource=0;
    U32    u32SymbolRate=0;
    S16 CFO_value=0;
    U8 phase_turning_num=0;
    U8  u8counter = 0;

    switch( eType )
    {
        case DMD_DVBS_GETLOCK:
            bRet &= MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data); DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetLock manual tune=%d<<<\n", u8Data));
            if ((u8Data&0x02)==0x00)
            {
                bRet &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_SUBSTATE_FLAG, &u8Data);  DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetLock MailBox substate=%d<<<\n", u8Data));
                bRet &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data); DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetLock MailBox state=%d<<<\n", u8Data));
                printk(KERN_DEBUG "DVBS for DEBUG state=0x%d\n",u8Data);
                INTERN_DVBS_GetCurrentSymbolRate(&u32SymbolRate);
                CFO_value=CFO_Show(0);
                if((u8Data >= 15) && (u8Data != 0xcd) &&(((u32SymbolRate<8000)&&(CFO_value>-5200)&&(CFO_value<5200))||((u32SymbolRate>=8000)&&(CFO_value>-10100)&&(CFO_value<10100))))
                {
                    if (u8Data==15)
                    {
                        _bDemodType=FALSE;   //S
                        DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_Demod DVBS Lock<<<\n"));//
                    }
                    else
                    {
                        _bDemodType=TRUE;    //S2
                        DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_Demod DVBS2 Lock<<<\n"));//
                    }
                    if(g_dvbs_lock == 0)
                    {
                        g_dvbs_lock = 1;
                    }

                    MHal_Demod_MB_ReadReg(0x2800+0x7C*2 + 1,&u8Data);
                    FW_locking_time = u8Data;
                    MHal_Demod_MB_ReadReg(0x2800+0x7C*2,&u8Data);
                    FW_locking_time = (FW_locking_time<<8)|u8Data;
                    DBG_INTERN_DVBS(printk("FW_locking_time = %d\n", FW_locking_time));
                
                
                    INTERN_DVBS_GetTsDivNum(&u32Data,&tssource);  //ts_div_num
                    u8Data = u32Data;
                    if (u32Data > 0x1F) u8Data=0x1F;
                    if (u32Data < 0x01) u8Data=0x01;
                    cur_TS_CLK_DIV=u8Data; //save current TS clock div

                    if(tssource==0)
                    {
                        MHal_Demod_ReadReg(0x103301,&u8Data);
                        u8Data&=0xF8;
                        u8Data|=0x01;
                        MHal_Demod_WriteReg(0x103301, u8Data);//select ts clk source 288MHz
                    }
                    else
                    {
                        MHal_Demod_ReadReg(0x103301,&u8Data);
                        u8Data&=0xF8;
                        u8Data|=0x02;//select ts clk source 432MHz
                        MHal_Demod_WriteReg(0x103301, u8Data);//select ts clk source 288MHz
                    }
                    
                    MHal_Demod_ReadReg(0x103301, &u8Data); // read current TS clock source setting
                    cur_TS_CLK_source=u8Data;//save current TS clock source setting
                    //DBG_INTERN_DVBS(printk("cur_TS_CLK_DIV=0x%x\n", cur_TS_CLK_DIV));// print current TS clock div
                    //DBG_INTERN_DVBS(printk("cur_TS_CLK_source=0x%x\n", cur_TS_CLK_source));// print current TS clock source setting
                    //DBG_INTERN_DVBS(printk("pre_TS_CLK_DIV=0x%x\n", pre_TS_CLK_DIV));// print previous TS clock div
                    //DBG_INTERN_DVBS(printk("pre_TS_CLK_source=0x%x\n", pre_TS_CLK_source));// print previous TS clock source setting

                    if((pre_TS_CLK_DIV !=cur_TS_CLK_DIV)||(pre_TS_CLK_source !=cur_TS_CLK_source))
                    {
                        MHal_Demod_ReadReg(0x103302, &u8Data);
                        MHal_Demod_WriteReg(0x103302, u8Data | 0x01);//disbale TS module
                        
                        if (Debug_TS_CLK_on==0)
                        {
                            MHal_Demod_WriteReg(0x103301, cur_TS_CLK_source);
                            //phase_turning_num=cur_TS_CLK_DIV>>1;//calculate TS phase tuning number for 90 and 270 degree
                            phase_turning_num = 3;
                            MHal_Demod_WriteReg(0x10330b, phase_turning_num); // set phase tuning number = 3
                            
                            MHal_Demod_ReadReg(0x103301,&u8Data);
                            u8Data|=0x40;//enable phase tuning
                            u8Data&=~(0x08);//disable polarity
                            MHal_Demod_WriteReg(0x103301, u8Data);
                            
                            MHal_Demod_WriteReg(0x103300, cur_TS_CLK_DIV);//set TS div
                        }
                        MHal_Demod_ReadReg(0x103300, &u8Data); // read current TS clock div
                        pre_TS_CLK_DIV=u8Data;//save current TS clock div
                        MHal_Demod_ReadReg(0x103301, &u8Data); // read current TS clock source setting
                        pre_TS_CLK_source=u8Data;//save current TS clock source setting
                        //DBG_INTERN_DVBS(printk("pre_TS_CLK_DIV=0x%x\n", pre_TS_CLK_DIV));// print current TS clock div
                        //DBG_INTERN_DVBS(printk("pre_TS_CLK_source=0x%x\n", pre_TS_CLK_source));// print current TS clock source setting
                        //DBG_INTERN_DVBS(printk("cur_TS_CLK_DIV=0x%x\n", cur_TS_CLK_DIV));// print previous TS clock div
                        //DBG_INTERN_DVBS(printk("cur_TS_CLK_source=0x%x\n", cur_TS_CLK_source));// print previous TS clock source setting

                        MHal_Demod_ReadReg(0x103302, &u8Data);
                        MHal_Demod_WriteReg(0x103302, u8Data & ~0x01);//enable TS module
                    }
                    
                    MHal_Demod_ReadReg(0x103300, &u8Data); // read current TS clock div
                    DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetLock TsClkDivNum = 0x%x<<<\n", u8Data));//
                    if(!u8DemodLockFlag)
                    {
                        u8DemodLockFlag=1;
                        //Ts Output Enable
                        //MHal_Demod_WriteReg(0x101eaa,0x10); 
                    } //INTERN_DVBS_Get_FreqOffset(&Est_CFO, u8Data);
                    //For Auto Test
                    //Ts Output Enable
                    bRet&=MHal_Demod_MB_ReadReg(DVBTM_REG_BASE+(0x20)*2, &u8Data);
                    u8Data|=0x04;
                    bRet&=MHal_Demod_MB_WriteReg(DVBTM_REG_BASE+(0x20)*2, u8Data);
                    DBG_INTERN_DVBS(printk("@INTERN_DVBS_Demod Lock+++\n"));
                    bRet = TRUE;
                } 
                else
                {
                    if(g_dvbs_lock == 1)
                    {
                        g_dvbs_lock = 0; u8DemodLockFlag=0;
                    }
                    //Ts Output disable
                    bRet&=MHal_Demod_MB_ReadReg(DVBTM_REG_BASE+(0x20)*2, &u8Data);
                    u8Data&=0xFB;
                    bRet&=MHal_Demod_MB_WriteReg(DVBTM_REG_BASE+(0x20)*2, u8Data);
                    DBG_INTERN_DVBS(printk(">>>DVBS/S2 CFO range check<<<\n"));
                    bRet &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data); DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetLock MailBox state=%d<<<\n", u8Data));
                    if((u8Data >= 15)
                    &&(((u32SymbolRate<8000)&&((CFO_value<-5200)||(CFO_value>5200)))||((u32SymbolRate>=8000)&&((CFO_value<-10100)||(CFO_value>10100)))))                    
                    {                    
                        DBG_INTERN_DVBS(printk(">>>DVBS/S2 CFO out of range<<<\n"));
                        MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);//DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
                        u8Data&=0xFE;
                        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);//DVBS_WriteReg(TOP_REG_BASE + 0x5B*2, u8Data);

                        bRet &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data); DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetLock MailBox state=%d<<<\n", u8Data));
                        u8counter = 20;
                        while( (u8Data != 0x01) && (u8counter != 0) )
                        {
                            mdelay(10);//udelay(10*1000);//MsOS_DelayTask(10);
                            bRet &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data); DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_GetLock MailBox state=%d<<<\n", u8Data));
                            u8counter--;
                        }
                        bRet &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data); DBG_INTERN_DVBS(printk(">>>line 1749 INTERN_DVBS_GetLock MailBox state=%d<<<\n", u8Data));

                        MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);//DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
                        DBG_INTERN_DVBS(printk(">>>line 1752 DVBS/S2 state machine enable=%d<<<\n", u8Data));

                        u8Data|=0x01;
                        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);//DVBS_WriteReg(TOP_REG_BASE + 0x5B*2, u8Data);
                        
                        u8counter = 20;
                        MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);//DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data); DBG_INTERN_DVBS(printk(">>>INTERN_DVBS_config FW Restart=%d<<<\n", u8Data));
                        DBG_INTERN_DVBS(printk(">>>line 1759 DVBS/S2 state machine enable=%d<<<\n", u8Data));
                        while( ((u8Data&0x01) == 0x00) && (u8counter != 0) )
                        {
                            mdelay(10);//udelay(10*1000);//MsOS_DelayTask(10);
                            u8Data|=0x01;
                            MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);//DVBS_WriteReg(TOP_REG_BASE + 0x5B*2, u8Data);
                            MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);//DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
                            DBG_INTERN_DVBS(printk(">>>DVBS/S2 state machine enable=%d<<<\n", u8Data));
                            u8counter--;
                        }                                    
                    }
                    DBG_INTERN_DVBS(printk("@INTERN_DVBS_Demod UnLock---\n"));
                    bRet = FALSE;
                }

                if(_bSerialTS==1)
                {
                    if (bRet==FALSE)
                    {
                        _bTSDataSwap=FALSE;
                    }
                    else
                    {
                        if (_bTSDataSwap==FALSE)
                        {
                            _bTSDataSwap=TRUE;
                            MHal_Demod_MB_ReadReg(DVBTM_REG_BASE + 0x20*2, &u8Data);
                            u8Data^=0x20;
                            MHal_Demod_MB_WriteReg(DVBTM_REG_BASE + 0x20*2, u8Data);
                        }
                    }
                }
            }
            else
            {
                bRet = TRUE;
            }
        break;

        case DMD_DVBS_GETLOCK_TR_EVER_LOCK:
            //cBitMask = BIT(7);
            if (MHal_Demod_MB_ReadReg(DVBS2_INNER_REG_BASE+0x3B*2+1, &cData) == FALSE)
            //if (MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_TOP_WR_DBG_90, &cData) == FALSE)
            return FALSE;

            if ((cData & 0x01) != 0)
            //if ((cData & cBitMask) != 0)
            {
                bRet = TRUE;
            }
            else
            {
                bRet = FALSE;
            }
        break;

        default:
            bRet = FALSE;
    }
    return bRet;
}

BOOL DVBS_GetCurrentDemodType(DMD_DVBS_DEMOD_TYPE *pDemodType)
{
    U8    u8Data=0;
    BOOL  status = true;

    DBG_INTERN_DVBS(printk("INTERN_DVBS_GetCurrentDemodType\n"));

    MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
    if(!u8Data)                                                       //S2
    {
        *pDemodType = DMD_SAT_DVBS2;
        DBG_INTERN_DVBS(printk("[dvbs]DemodType=DVBS2\n"));
    }
    else                                                                            //S
    {
        *pDemodType = DMD_SAT_DVBS;
        DBG_INTERN_DVBS(printk("[dvbs]DemodType=DVBS\n"));
    }
    return status;
}

BOOL DVBS_GetPacketErr(U16 *pktErr)
{
    BOOL    status = true;
    U8      tmp = 0;
    U16     u16_INTERN_Packer_err;
#if 0
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
    if(!u8Data) //DVB-S2
    {
        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x02*2, &u8Data);
        status &= MHal_Demod_MB_WriteReg(DVBS2FEC_REG_BASE+0x02*2, u8Data|0x01);

        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x26*2+1, &u8Data);
        u16PktErr = u8Data;
        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x26*2, &u8Data);
        u16PktErr = (u16PktErr << 8)|u8Data;

        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x02*2, &u8Data);
        status &= MHal_Demod_MB_WriteReg(DVBS2FEC_REG_BASE+0x02*2, u8Data&(~0x01));
    }
    else
    {   //DVB-S
        status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x33*2+1, &u8Data);//status &= DVBS_ReadReg(DVBSTFEC_REG_BASE+0x1F*2+1, &u8Data);
        u16PktErr = u8Data;
        status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x33*2, &u8Data);//status &= DVBS_ReadReg(DVBSTFEC_REG_BASE+0x1F*2, &u8Data);
        u16PktErr = (u16PktErr << 8)|u8Data;
    }

    *pktErr = u16PktErr;
    DBG_INTERN_DVBS(printk("INTERN_DVBS PktErr = %d \n", (int)u16PktErr));//
#else

    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_PACKET_ERROR_PER_SEC_H, &tmp);
    u16_INTERN_Packer_err = tmp;

    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_PACKET_ERROR_PER_SEC_L, &tmp);
    u16_INTERN_Packer_err = u16_INTERN_Packer_err<<8|tmp;
  
    *pktErr = u16_INTERN_Packer_err;
    //printk("kevin packet error\n");
    //printk("INTERN_DVBS PktErr = %d \n", (int)u16_INTERN_Packer_err);
    DBG_INTERN_DVBS(printk("INTERN_DVBS PktErr = %d \n", (int)u16_INTERN_Packer_err));//
#endif
    return status;
}


BOOL DVBS_GetPostViterbiBer(U32 *postber_e10)//POST BER  enlarge 10^10
{
    BOOL    status = true;
    U8      reg = 0, reg_frz = 0;
    U16     BitErrPeriod;
    U32     BitErr;
    U8      u8Data = 0;
    U32     current_time = 0;
    MS_U64     dividend64;
    MS_U64     divisor64;

    MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
    if(!u8Data) //S2
    {
            // wait buffer is full
        current_time = (U32)jiffies_to_msecs(jiffies);//MsOS_GetSystemTime();
       
        do
        {
            status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x33*2+1, &reg);        
        }while(reg == 0xFF && (((U32)jiffies_to_msecs(jiffies) - current_time) < 500));

        // freeze outer
        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x02*2, &reg);
        reg |= 0x01;
        status &= MHal_Demod_MB_WriteReg(DVBS2FEC_REG_BASE+0x02*2, reg);

        // Get LDPC error window

        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x12*2+1, &reg);
        BitErrPeriod = reg;
        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x12*2, &reg);
        BitErrPeriod = (BitErrPeriod << 8) | reg;        

        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x33*2+1, &reg);
        BitErr = reg;
        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x33*2, &reg);
        BitErr = (BitErr << 8) | reg;
        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x32*2+1, &reg);
        BitErr = (BitErr << 8) | reg;
        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x32*2, &reg);
        BitErr = (BitErr << 8) | reg;

        // unfreeze outer
        status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x02*2, &reg);
        reg &= ~(0x01);
        status &= MHal_Demod_MB_WriteReg(DVBS2FEC_REG_BASE+0x02*2, reg);
        if (BitErr <= 0 )
        {
            //*postber = 0.5f / (BitErrPeriod*64800.0);
            dividend64 = 5;
            dividend64 = dividend64*1000000000;
            divisor64 = BitErrPeriod*64800;
            *postber_e10 = div64_u64(dividend64, divisor64);
            
        }
        else
        {
            //*postber = (float)BitErr/(BitErrPeriod*64800.0*100.0);
            dividend64 = BitErr;
            dividend64 = dividend64*10000000000;
            divisor64 = BitErrPeriod*64800*100;
            *postber_e10 = div64_u64(dividend64, divisor64);
        }
        DBG_GET_SIGNAL_DVBS(printk("INTERN_DVBS BER = %d \n", *postber_e10));  
    }
    else
    {
        /////////// Post-Viterbi BER /////////////After Viterbi

        //freeze ,DVBSRS_BACKEND_BIT_ERR_NUM_FREEZE
        status &= MHal_Demod_MB_ReadReg(DVBSFEC_REG_BASE+0x03, &reg_frz);
        status &= MHal_Demod_MB_WriteReg(DVBSFEC_REG_BASE+0x03, reg_frz|0x01);

        status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x23*2+1, &reg);
        BitErrPeriod = reg;

        status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x23*2, &reg);
        BitErrPeriod = (BitErrPeriod << 8)|reg;

        status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x36*2+1, &reg);
        BitErr = reg;
        status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x36*2, &reg);
        BitErr = (BitErr << 8)|reg;
        status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x35*2+1, &reg);
        BitErr = (BitErr << 8)|reg;
        status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x35*2, &reg);
        BitErr = (BitErr << 8)|reg;

        reg_frz=reg_frz&(~0x01);
        status &= MHal_Demod_MB_WriteReg(DVBSFEC_REG_BASE+0x03, reg_frz);

        if (BitErrPeriod == 0 )    //PRD
            BitErrPeriod = 1;

        if (BitErr <= 0 )
        {
            //*postber = 0.5f / ((float)BitErrPeriod*128*188*8);
            dividend64 = 5;
            dividend64 =  dividend64*1000000000;
            divisor64 = BitErrPeriod*128*188*8;
            *postber_e10 = div64_u64(dividend64, divisor64);
        }
        else
        {
            //*postber = (float)BitErr / ((float)BitErrPeriod*128*188*8);
            dividend64 = BitErr;
            dividend64 =  dividend64*10000000000;
            divisor64 = BitErrPeriod*128*188*8;
            *postber_e10 = div64_u64(dividend64, divisor64);
        }

        //if (*postber <= 0.0f)
        //    *postber = 1.0e-10f;
        if (*postber_e10 <= 0)
            *postber_e10 = 1;
        DBG_INTERN_DVBS(printk("INTERN_DVBS PostVitBER = %d \n", *postber_e10));
    }
    return status;
}

S32 DVBS_GetTunrSignalLevel_PWR(void)
{
    BOOL status=TRUE;
    U16 u16Data =0;
    U8  u8Data =0;
    U8  u8Index =0;
    S32  fCableLess = 0;//float  fCableLess = 0.0;

    if (FALSE == DVBS_GetLock(DMD_DVBS_GETLOCK, 0) )//Demod unlock
    {
        fCableLess = 0;
    }

    status &= MHal_Demod_MB_ReadReg(FRONTEND_REG_BASE+0x11*2, &u8Data);
    u8Data=(u8Data&0xF0)|0x03;
    status &= MHal_Demod_MB_WriteReg(FRONTEND_REG_BASE+0x11*2, u8Data);

    status &= MHal_Demod_MB_ReadReg(FRONTEND_REG_BASE+0x02, &u8Data);
    u8Data|=0x80;
    status &= MHal_Demod_MB_WriteReg(FRONTEND_REG_BASE+0x02, u8Data);

    status &= MHal_Demod_MB_ReadReg(FRONTEND_REG_BASE+0x12*2+1, &u8Data);
    u16Data=u8Data;
    status &= MHal_Demod_MB_ReadReg(FRONTEND_REG_BASE+0x12*2, &u8Data);
    u16Data=(u16Data<<8)|u8Data;
    DBG_GET_SIGNAL_DVBS(printk("===========================Tuner 65535-u16Data = %d\n", (65535-u16Data)));
    //printk("===========================Tuner 65535-u16Data = %d\n", (65535-u16Data));
    //MsOS_DelayTask(400);

    status &= MHal_Demod_MB_ReadReg(FRONTEND_REG_BASE+0x02, &u8Data);
    u8Data&=~(0x80);
    status &= MHal_Demod_MB_WriteReg(FRONTEND_REG_BASE+0x02, u8Data);

    if(status==FALSE)
    {
        DBG_GET_SIGNAL_DVBS(printk("INTERN_DVBS GetSignalStrength fail!!! \n "));
        fCableLess = 0;
    }

    //DBG_GET_SIGNAL_DVBS(printk("#### INTERN_DVBS_GetTunrSignalLevel_PWR u16Data = %d\n", (int)u16Data));
    for(u8Index=0; u8Index < (sizeof(_u16SignalLevel)/sizeof(_u16SignalLevel[0])); u8Index++)
    {
        if((65535 - u16Data) <= _u16SignalLevel[u8Index][0])
        {
            if(u8Index >=1)
            {
                //fCableLess = (float)(_u16SignalLevel[u8Index][1])+((float)(_u16SignalLevel[u8Index][0] - (65535 - u16Data)) / (float)(_u16SignalLevel[u8Index][0] - _u16SignalLevel[u8Index-1][0]))*(float)(_u16SignalLevel[u8Index-1][1] - _u16SignalLevel[u8Index][1]);
                //fCableLess = (float)(_u16SignalLevel[u8Index][1])+((float)(_u16SignalLevel[u8Index][0] - (65535 - u16Data)) / (float)(_u16SignalLevel[u8Index][0] - _u16SignalLevel[u8Index-1][0]))*(float)(_u16SignalLevel[u8Index-1][1] - _u16SignalLevel[u8Index][1]);   
                //fCableLess =(_u16SignalLevel[u8Index][1])+(((_u16SignalLevel[u8Index][0] - (65535 - u16Data))*(_u16SignalLevel[u8Index-1][1] - _u16SignalLevel[u8Index][1])) / (_u16SignalLevel[u8Index][0] - _u16SignalLevel[u8Index-1][0]));   
                fCableLess = (_u16SignalLevel[u8Index][1])+((_u16SignalLevel[u8Index][0] - (65535 - u16Data)) / (_u16SignalLevel[u8Index][0] - _u16SignalLevel[u8Index-1][0]))*(_u16SignalLevel[u8Index-1][1] - _u16SignalLevel[u8Index][1]);   
            }         
            else
            {
                fCableLess = _u16SignalLevel[u8Index][1];
            }
        break;
        }
    }
//---------------------------------------------------
    if (fCableLess >= 350)
        fCableLess = fCableLess - 35;
    else if ((fCableLess < 350) && (fCableLess >= 250))
        fCableLess = fCableLess - 25;
    else
        fCableLess = fCableLess - 5;

    if (fCableLess < 0)
        fCableLess = 0;
    if (fCableLess > 920)
        fCableLess = 920;

    fCableLess = -1*(fCableLess/10);

    //printk("===========================fCableLess2 = %.2f\n",fCableLess);

    DBG_GET_SIGNAL_DVBS(printk("INTERN_DVBS GetSignalStrength %d\n", fCableLess));

    return fCableLess;
}

/****************************************************************************
Subject:    To get the DVB-S Signal Strength
Function:   MSB1228_GetSignalStrength
Parmeter:   strength
Return:     E_RESULT_SUCCESS
E_RESULT_FAILURE
Remark:
 *****************************************************************************/


/*****************************************************************************


* * * * *DiSEqC part* * * * *


******************************************************************************/

BOOL DVBS_DiSEqC_Init(void)
{
    BOOL status = true;
    U8 u8Data=0;

    //Clear status
    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xCD, &u8Data);
    u8Data=(u8Data|0x3E)&(~0x3E);
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xCD, u8Data);

    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC0, 0x00);
    //Tone En
    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xC0, &u8Data);
    u8Data=(u8Data&(~0x06))|(0x06);
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC0, u8Data);

    DBG_INTERN_DVBS(printk("INTERN_DVBS_DiSEqC_Init\n"));

    return status;
}

BOOL DVBS_DiSEqC_SetLNBOut(BOOL bLow)
{
    BOOL status=TRUE;
    U8 u8Data=0;

    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xC2, &u8Data);
    if(bLow==TRUE)
    {
        u8Data=(u8Data|0x40);//13V
    }
    else
    {
        u8Data=(u8Data&(~0x40));//18V
    }
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC2, u8Data);

    return status;
}

BOOL DVBS_DiSEqC_GetLNBOut(BOOL* bLNBOutLow)
{
    BOOL status=TRUE;
    U8 u8Data=0;

    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xC2, &u8Data);
    if( (u8Data&0x40)==0x40)
    {
        * bLNBOutLow=TRUE;
    }
    else
    {
        * bLNBOutLow=FALSE;
    }

    return status;
}



BOOL DVBS_DiSEqC_Set22kOnOff(BOOL b22kOn)
{
    BOOL status=TRUE;
    U8   u8Data=0;

    //Set DiSeqC 22K

    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xC2, &u8Data);

    if (b22kOn==TRUE)
    {
        u8Data=(u8Data&0xc7);
        u8Data=(u8Data|0x08);
    }
    else
    {
        u8Data=(u8Data&0xc7);
    }
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC2, u8Data);

    DBG_INTERN_DVBS(printk("INTERN_DVBS_DiSEqC_Set22kOnOff:%d\n", b22kOn));
    return status;
}


BOOL DVBS_DiSEqC_Get22kOnOff(BOOL* b22kOn)
{
    BOOL status=TRUE;
    U8   u8Data=0;

    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xC2, &u8Data);
    if ((u8Data&0x38)==0x08)
    {
        *b22kOn=TRUE;
    }
    else
    {
        *b22kOn=FALSE;
    }

    return status;
}

BOOL DVBS_DiSEqC_SendCmd(U8* pCmd,U8 u8CmdSize)
{
    BOOL status=TRUE;
    U8   u8Data;
    U8   u8Index;
    U16  u16WaitCount;

    if(u8CmdSize==5)
        Unicable_System_Flag=TRUE;
    else
        Unicable_System_Flag=FALSE;
    //u16Address=0x0BC4;
    for (u8Index=0; u8Index < u8CmdSize; u8Index++)
    {
        u8Data=*(pCmd+u8Index);
        status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC4 + u8Index, u8Data);
        DBG_INTERN_DVBS(printk("=============INTERN_DVBS_DiSEqC_SendCmd(Demod1) = 0x%X\n",u8Data));
    }

    u8Data=((u8CmdSize-1)&0x07)|0x40;
#if 0
    if(((*pCmd)==0xE0)&&((*(pCmd + 1))==0x10)&&((*(pCmd + 2))==0x38)&&((((*(pCmd + 3))&0x0C)==0x0C)||(((*(pCmd + 3))&0x04)==0x04)))
    {
        u8Data|=0x80;   //u8Data|=0x20;Tone Burst1
    }
    else if(((*pCmd)==0xE0)&&((*(pCmd + 1))==0x10)&&((*(pCmd + 2))==0x38))
    {
        u8Data|=0x20;   //u8Data|=0x80;ToneBurst0
    }
#endif
    if(_u8ToneBurstFlag==1)
    {
        u8Data|=0x80;//0x20;
    }
    else if (_u8ToneBurstFlag==2)
    {
        u8Data|=0x20;//0x80;
    }

    _u8ToneBurstFlag=0;
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DMDTOP_DBG_8, u8Data);
    mdelay(25);//udelay(25*1000);//MsOS_DelayTask(25);
    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data|0x10); 

    u16WaitCount=0;
    do
    {
        MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
        mdelay(1);//udelay(1*1000);//MsOS_DelayTask(1);
        u16WaitCount++;
    }while(((u8Data&0x10)==0x10)&&(u16WaitCount < INTERN_DVBS_DEMOD_WAIT_TIMEOUT)) ;
    
    if(u16WaitCount >= INTERN_DVBS_DEMOD_WAIT_TIMEOUT)
    {
        printk("INTERN_DVBS DiSEqC Send Command Busy!!!\n");
        return FALSE;
    }
    else
    {
        printk("INTERN_DVBS DiSEqC Send Command Success!!!\n");
        return TRUE;
    }
    return status;
}


BOOL DVBS_DiSEqC_SetTxToneMode(BOOL bTxTone22kOff)
{
    BOOL status=TRUE;
    U8   u8Data=0;

    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xD7, &u8Data);
    if (bTxTone22kOff==TRUE)
    {
        u8Data=(u8Data|0x80);                   //1: without 22K.
    }
    else
    {
        u8Data=(u8Data&(~0x80));                //0: with 22K.
    }
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xD7, u8Data);

    return status;
}

BOOL DVBS_DiSEqC_SetTone(BOOL bTone1)
{
    BOOL status=TRUE;
    U8 u8Data=0;
    U8 u8ReSet22k;

    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC4, 0x01);
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC0, 0x4E);
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xCC, 0x88);

     //Tone-burst-new mode              
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xD8+1, 0x88);
    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xD8, &u8Data);
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xD8, u8Data|0x80);

    
    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xC2, &u8Data);
    u8ReSet22k=u8Data;

    if (bTone1==TRUE)
    {
        //Tone burst 1
        status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC2, 0x19);
        _u8ToneBurstFlag=1;
    }
    else
    {
        //Tone burst 0
        status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC2, 0x11);
        _u8ToneBurstFlag=2;
    }
    //DIG_DISEQC_TX_EN

    //MsOS_DelayTask(1);
    mdelay(1);//udelay(1*1000);
    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xCD, &u8Data);
    u8Data=u8Data|0x3E; //Status clear
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xCD, u8Data);
    //MsOS_DelayTask(10);
    mdelay(10);//udelay(10*1000);
    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xCD, &u8Data);
    u8Data=u8Data&~(0x3E);
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xCD, u8Data);
    //MsOS_DelayTask(1);
    mdelay(1);//udelay(1000);

    status &= MHal_Demod_MB_ReadReg(DVBS2_REG_BASE + 0xCD, &u8Data);
    u8Data=u8Data|0x01;  //Tx Enable
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xCD, u8Data);

    //MsOS_DelayTask(30);//(100)
    mdelay(30);//udelay(30*1000);
    //For ToneBurst 22k issue.
    u8Data=u8ReSet22k;
    status &= MHal_Demod_MB_WriteReg(DVBS2_REG_BASE + 0xC2, u8Data);

    DBG_INTERN_DVBS(printk("INTERN_DVBS_DiSEqC_SetTone:%d\n", bTone1));
    //MsOS_DelayTask(100);
    return status;
}


BOOL DVBS_UnicableAGCCheckPower(BOOL pbAGCCheckPower)
{
    //MS_BOOL status = TRUE;
    U8 u8Data=0;

    //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_91, 0x00);

    //DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    //status &= MDrv_SYS_DMD_VD_MBX_ReadDSPReg(E_DMD_S2_MB_TOP_WR_DBG_90, &u8Data);
    u8Data &= 0xFE;//clean bit0
    //DVBS_WriteReg(TOP_REG_BASE + 0x5B*2, u8Data);
    MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
    //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_90, u8Data);

    if(pbAGCCheckPower == FALSE)//0
    {
        //status &= MDrv_SYS_DMD_VD_MBX_ReadDSPReg(E_DMD_S2_MB_TOP_WR_DBG_91, &u8Data);
        u8Data &= 0xFE;//clean bit0
        //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_91, u8Data);
        //printk("CMD=MS_FALSE==============================\n");
    }
    else
    {
        //status &= MDrv_SYS_DMD_VD_MBX_ReadDSPReg(E_DMD_S2_MB_TOP_WR_DBG_91, &u8Data);
        u8Data |= 0x01;           //bit1=1
        //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_91, u8Data);
        //printk("CMD=MS_TRUE==============================\n");
    }

    //DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    //status &= MDrv_SYS_DMD_VD_MBX_ReadDSPReg(E_DMD_S2_MB_TOP_WR_DBG_90, &u8Data);
    u8Data &= 0xF0;
    u8Data |= 0x01;
    //DVBS_ReadReg(TOP_REG_BASE + 0x5B*2, &u8Data);
    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_90, u8Data);
    mdelay(500);//udelay(500*1000);

    //status &= MDrv_SYS_DMD_VD_MBX_ReadDSPReg(E_DMD_S2_MB_TOP_WR_DBG_91, &u8Data);
    u8Data &= 0x80;             //Read bit7
    if (u8Data == 0x80)
    {
        u8Data = 0x00;
        //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_90, u8Data);
        u8Data = 0x00;
        //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_91, u8Data);
        return TRUE;
    }
    else
    {
        u8Data = 0x00;
        //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_90, u8Data);
        u8Data = 0x00;
        //status &= MDrv_SYS_DMD_VD_MBX_WriteDSPReg(E_DMD_S2_MB_TOP_WR_DBG_91, u8Data);
        return FALSE;
    }
}
BOOL DTV_DVB_S_BlindScan_Init(U32 u32StartFreq,U32 u32EndFreq)
{
    BOOL bRet;
    _u32CurrentSR = 0;
    eBlindScanStatue_inter = mapi_demodulator_datatype_E_BLINDSCAN_NOTREADY;
    bRet=DVBS_BlindScan_Start((U16)u32StartFreq,(U16)u32EndFreq);
    if(bRet)
        eBlindScanStatue_inter= mapi_demodulator_datatype_E_BLINDSCAN_INIT_OK;
    else
        eBlindScanStatue_inter = mapi_demodulator_datatype_E_BLINDSCAN_INIT_FAILED;

    return bRet;
}
BOOL DVBS_BlindScan_Start(U16 u16StartFreq,U16 u16EndFreq)
{
    BOOL status=TRUE;
    U8 u8Data=0;

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_Start+\n"));
/*BEGIN: auto iq swap detection (1/7)*/
    iq_swap_checking_flag=FALSE;
    iq_swap_done_flag=FALSE;
    MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DUMMY_REG_6, 0); //disable force coarse cfo mechenism    
/*END: auto iq swap detection (1/7)*/
    _u16BlindScanStartFreq=u16StartFreq;
    _u16BlindScanEndFreq=u16EndFreq;
    _u16TunerCenterFreq=0;
    _u16ChannelInfoIndex=0;
    Unicable_System_Flag=FALSE;
    _u16LockedCenterFreq=0;
    //IQ swap
    status &= MHal_Demod_MB_ReadReg(DMDANA_REG_BASE+0xC0, &u8Data);
    u8Data|=(0x02);
    status &= MHal_Demod_MB_WriteReg(DMDANA_REG_BASE+0xC0, u8Data);
    DBG_INTERN_DVBS(printk("DVBS_System_Init->IQ swap\n"));

    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    u8Data&=0xD0;
    MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);

    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_TOP_WR_DBG_92, (U8)_u16BlindScanStartFreq&0x00ff);
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_TOP_WR_DBG_93, (U8)(_u16BlindScanStartFreq>>8)&0x00ff);

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_Start- u16StartFreq %d u16EndFreq %d\n", u16StartFreq, u16EndFreq));//

    return status;
}
BOOL DTV_DVB_S_BlindScan_ScanNextFreq(BOOL *bBlindScanEnd)
{
    BOOL bScanEnd;
    BOOL bRet;

    bRet = DVBS_BlindScan_NextFreq(&bScanEnd);
    if(bRet)
    {
        if(bScanEnd)
            eBlindScanStatue_inter= mapi_demodulator_datatype_E_BLINDSCAN_ALLFREQ_COMPLETE;
        else
            eBlindScanStatue_inter= mapi_demodulator_datatype_E_BLINDSCAN_SCANFREQ_SCANNING;
        *bBlindScanEnd=bScanEnd;
    }
    else
    {
        eBlindScanStatue_inter= mapi_demodulator_datatype_E_BLINDSCAN_SCANFREQ_START_FAILED;
        *bBlindScanEnd=TRUE;
    }
    DBG_INTERN_DVBS(printk("DTV_DVB_S_BlindScan_ScanNextFreq , %d, %d %d \n", bRet,eBlindScanStatue_inter, bScanEnd ));

    return bRet;
}
BOOL DVBS_BlindScan_NextFreq(BOOL* bBlindScanEnd)
{
    BOOL status=TRUE;
    U8   u8Data=0;

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_NextFreq+\n"));

    * bBlindScanEnd=FALSE;
    //DBG_INTERN_DVBS(printk("DVBS_bBlindtypekevin=%d\n",*bBlindtype));
    /*
    if (*bBlindtype==2)
    {
        DBG_INTERN_DVBS(printk("unicable case M1ap for IQ swap\n"));
        DVBS_ReadReg(TOP_REG_BASE + 0x30*2, &u8Data);
        u8Data|=0x02;
        DVBS_WriteReg(TOP_REG_BASE + 0x30*2, u8Data);
    }
    else
    {
        DBG_INTERN_DVBS(printk("nonunicable case M1ap IQ non swap\n"));
        DVBS_ReadReg(TOP_REG_BASE + 0x30*2, &u8Data);
        u8Data&=(0xFF-0x02);
        DVBS_WriteReg(TOP_REG_BASE + 0x30*2, u8Data);
    }
*/
/*BEGIN: auto iq swap detection (2/7)*/
    if((iq_swap_checking_flag==TRUE)&&(iq_swap_done_flag==FALSE))
    {   
        MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
        u8Data&=~(0x02);
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
        u8Data&=~(0x28);
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
        return status;
    } 
/*END: auto iq swap detection (2/7)*/ 
    if(_u16TunerCenterFreq >=_u16BlindScanEndFreq)
    {
        DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_NextFreq . _u16TunerCenterFreq %d _u16BlindScanEndFreq%d\n", _u16TunerCenterFreq, _u16BlindScanEndFreq));
        * bBlindScanEnd=TRUE;

        return status;
    }
    //Set Tuner Frequency
    mdelay(10);//udelay(10*1000);//MsOS_DelayTask(10); 

    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    if ((u8Data&0x02)==0x00)//Manual Tune
    {
        u8Data&=~(0x28);
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
        u8Data|=0x02;
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
        u8Data|=0x01;
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
    }
    else
    {
        u8Data&=~(0x28);
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
    }

    return status;
}

BOOL DTV_DVB_S_BlindScan_End(void)
{
    DBG_INTERN_DVBS(printk("DTV_DVB_S_BlindScan_End+\n"));    
    if((eBlindScanStatue_inter== mapi_demodulator_datatype_E_BLINDSCAN_SCANFREQ_SCANNING))
    {
        if(DVBS_BlindScan_Cancel() != TRUE)
        {
            return FALSE;
        }
    }

    if(DVBS_BlindScan_End() != TRUE)
    {
        return FALSE;
    }

    eBlindScanStatue_inter= mapi_demodulator_datatype_E_BLINDSCAN_NOTREADY;

    return TRUE;
}
BOOL DVBS_BlindScan_Cancel(void)
{
    BOOL status=TRUE;
    U8   u8Data=0;
    U16  u16Data;

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_Cancel+\n"));

    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    u8Data&=0xF0;
    MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);

    u16Data = 0x0000;
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_TOP_WR_DBG_92, (U8)u16Data&0x00ff);
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_TOP_WR_DBG_93, (U8)(u16Data>>8)&0x00ff);

    _u16TunerCenterFreq=0;
    _u16ChannelInfoIndex=0;

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_Cancel-\n"));

    return status;
}

BOOL DVBS_BlindScan_End(void)
{
    BOOL status=TRUE;
    U8   u8Data=0;
    U16  u16Data;

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_End+\n"));//

    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    u8Data&=0xF0;
    MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);

    u16Data = 0x0000;
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_TOP_WR_DBG_92, (U8)u16Data&0x00ff);
    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_TOP_WR_DBG_93, (U8)(u16Data>>8)&0x00ff);

    _u16TunerCenterFreq=0;
    _u16ChannelInfoIndex=0;

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_End-\n"));//

    return status;
}

BOOL DVBS_BlindScan_GetChannel(U16* u16TPNum, HAL_DEMOD_MS_FE_CARRIER_PARAM *pTable)
{
    BOOL status=TRUE;
    U16  u16TableIndex;

    //*u16TPNum=_u16ChannelInfoIndex-u16ReadStart;
    *u16TPNum=1;
    for(u16TableIndex = 0; u16TableIndex < (*u16TPNum); u16TableIndex++)
    {
        pTable[u16TableIndex].u32Frequency = _u16ChannelInfoArray[0][_u16ChannelInfoIndex-1];
        pTable[u16TableIndex].SatParam.u32SymbolRate = _u16ChannelInfoArray[1][_u16ChannelInfoIndex-1];
        DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_GetChannel Freq: %d SymbolRate: %d\n", pTable[u16TableIndex].u32Frequency, pTable[u16TableIndex].SatParam.u32SymbolRate));
    }
    DBG_INTERN_DVBS(printk("INTERN_DVBS_u16TPNum:%d\n", *u16TPNum));

    return status;
}

BOOL DVBS_BlindScan_GetCurrentFreq(U32 *u32CurrentFeq)
{
    BOOL status=TRUE;
    DBG_INTERN_DVBS(printk("INTERN_DVBS_BlindScan_GetCurrentFreq+\n"));

    *u32CurrentFeq=_u16TunerCenterFreq;
    DBG_INTERN_DVBS(printk("INTERN_DVBS_BlindScan_GetCurrentFreq-: %d\n", _u16TunerCenterFreq));
    return status;
}

BOOL DTV_DVB_S_BlindScan_GetStatus(mapi_demodulator_datatype_EN_BLINDSCAN_STATUS *eStatus, BOOL *bBlindScanLock)
{
    U8 u8Progress,u8ChannelNum;
    BOOL bRet = TRUE;
    BOOL bFound = FALSE;

    DBG_INTERN_DVBS(printk("M2R DTV_DVB_S_BlindScan_GetStatus\n"));
    bFound = FALSE;
    if(eBlindScanStatue_inter == mapi_demodulator_datatype_E_BLINDSCAN_SCANFREQ_SCANNING)
    {
        if(DVBS_BlindScan_WaitCurFreqFinished(&u8Progress, &u8ChannelNum, &bFound) != TRUE)
        {
            bRet = FALSE;
        }
        else
        {
            if(u8Progress == 100)
            {
                eBlindScanStatue_inter = mapi_demodulator_datatype_E_BLINDSCAN_SCANFREQ_COMPLETE;
            }
        }
    }
    *eStatus = eBlindScanStatue_inter;
    *bBlindScanLock=bFound;

    return bRet;
}
BOOL DVBS_BlindScan_WaitCurFreqFinished(U8* u8Progress,U8 *u8FindNum, U8* blindscanlock)
{
    BOOL status=TRUE;
    U32  u32Data;
    U16  u16Data;
    U8   u8Data=0, u8Data2=0,bw=0,state=0;
    U16  u16WaitCount;
    static S16 cfo_check_iq_swap=0;
    S32 cfo=0;
    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_WaitCurFreqFinished+\n"));
    DBG_INTERN_DVBS(printk("iq_swap_checking_flag [%d] iq_swap_done_flag [%d]\n",iq_swap_checking_flag,iq_swap_done_flag));

    u16WaitCount=0;
    *u8FindNum=0;
    *u8Progress=0;
/*BEGIN: auto iq swap detection (3/7)*/
    if((iq_swap_checking_flag==TRUE)&&(iq_swap_done_flag==FALSE))
    {
        iq_swap_checking_flag = FALSE;
        iq_swap_done_flag = TRUE;
        do
        {
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &state);//State=BlindScan
            u16WaitCount++;
            DBG_INTERN_DVBS(printk("state[%d] u16WaitCount[%d]\n",state,u16WaitCount));
            mdelay(5);//udelay(1*1000);//MsOS_DelayTask(1);
        }while((state!=15)&&(state!=16)&&(u16WaitCount<600));//E_DMD_S2_STATE_FLAG
#if DEBUG_AUTO_IQ_SWAP_DETECTION
        MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DUMMY_REG_6, &u8Data);
        DBG_INTERN_DVBS(printk("MB_DUMMY_REG_6: %d\n",u8Data));
        MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_SWUSE00L, &u8Data);
        DBG_INTERN_DVBS(printk("bit4: 0x%x\n",u8Data));
        MHal_Demod_MB_ReadReg(FRONTENDEXT2_REG_BASE+0x12*2+1, &u8Data);
        u16Data = u8Data;
        MHal_Demod_MB_ReadReg(FRONTENDEXT2_REG_BASE+0x12*2, &u8Data);
        u16Data=((u16Data<<8)|u8Data);
        DBG_INTERN_DVBS(printk("CFO: 0x%x\n",u16Data));
#endif
        MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DUMMY_REG_6, 0); //disable force coarse cfo mechenism
        if(state>=15)
            INTERN_DVBS_Get_FreqOffset(&cfo,bw);
        if(u16WaitCount<600
            && state>=15
            &&(((_s16CurrentCFO!=0)&&(abs(cfo*1000)<=abs(cfo_check_iq_swap)))||((_s16CurrentCFO==0)&&((cfo*1000)>=0))))  //No need to do IQ SWAP
        {
            MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
            u8Data|=0x20;
            MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
            return status;
        }
        else   //Need to do IQ SWAP
        {
            //IQ swap
            status &= MHal_Demod_MB_ReadReg(DMDANA_REG_BASE+0xC0, &u8Data);
            if((u8Data&0x02)==0x02)
                u8Data&=~(0x02);
            else
                u8Data|=(0x02);
            status &= MHal_Demod_MB_WriteReg(DMDANA_REG_BASE+0xC0, u8Data);
            DBG_INTERN_DVBS(printk("Blind scan reset IQ swap setting= 0x%x\n",u8Data));

            MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
            u8Data&=0xF0;
            MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
            mdelay(1);//udelay(1*1000);//MsOS_DelayTask(1);

            status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_TOP_WR_DBG_92, (U8)_u16BlindScanStartFreq&0x00ff);
            status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_TOP_WR_DBG_93, (U8)(_u16BlindScanStartFreq>>8)&0x00ff);
            _u16ChannelInfoIndex = 0;
            *u8Progress=100;
            printk("change IQ swap setting, restart from start freq=%d\n",_u16BlindScanStartFreq);
            return status;
        }
    }
/*END: auto iq swap detection (3/7)*/
    do
    {
        status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data);//State=BlindScan
        status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_BLINDSCAN_CHECK, &u8Data2);//SubState=BlindScan
        u16WaitCount++;

        DBG_INTERN_DVBS(printk("INTERN_DVBS_BlindScan_WaitCurFreqFinished_State: 0x%x SubState: 0x%x u16WaitCount:%d\n", u8Data, u8Data2, u16WaitCount));//
        mdelay(5);//udelay(1*1000);//MsOS_DelayTask(1);
    }while(((u8Data!=17)||(u8Data2!=0xff))&&(u16WaitCount<INTERN_DVBS_DEMOD_WAIT_TIMEOUT));//E_DMD_S2_STATE_FLAG

    DBG_INTERN_DVBS(printk("INTERN_DVBS_BlindScan_WaitCurFreqFinished_u16WaitCount: %d\n", u16WaitCount));//

    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DUMMY_REG_2, &u8Data);
    u16Data = u8Data;

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_WaitCurFreqFinished OuterCheckStatus:0x%x\n", u16Data));//

    if (u16WaitCount>=INTERN_DVBS_DEMOD_WAIT_TIMEOUT)
    {
        status=FALSE;
    }
    else
    {
        status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_SUBSTATE_FLAG, &u8Data);//SubState
        if (u8Data==0)
        {
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE13L, &u8Data);
            u32Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE12H, &u8Data);
            u32Data=(u32Data<<8)|u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE12L, &u8Data);
            u32Data=(u32Data<<8)|u8Data;
            _u16ChannelInfoArray[0][_u16ChannelInfoIndex]=((u32Data+500)/1000);
            _u16LockedCenterFreq=((u32Data+500)/1000);//Center Freq

            if (_u16LockedCenterFreq<_u16BlindScanStartFreq) //M2 soultion
                _u16LockedCenterFreq= _u16BlindScanStartFreq;
            else if (_u16LockedCenterFreq>_u16BlindScanEndFreq)
                _u16LockedCenterFreq= _u16BlindScanEndFreq;

            _u16ChannelInfoArray[0][_u16ChannelInfoIndex]=_u16LockedCenterFreq;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE14H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE14L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;
            _u16ChannelInfoArray[1][_u16ChannelInfoIndex]=(u16Data);//Symbol Rate
            _u16LockedSymbolRate=u16Data;
            _u16ChannelInfoIndex++;
            *u8FindNum=_u16ChannelInfoIndex;

            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE15H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE15L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;//Center_Freq_Offset_Locked
            if (u16Data*1000 >= 0x8000)
            {
                u16Data=0x10000- u16Data*1000;
                _s16CurrentCFO=-1*u16Data/1000;
            }
            else
            {
                _s16CurrentCFO=u16Data;
            }
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE16H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE16L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;
            _u16CurrentStepSize=u16Data;//Tuner_Frequency_Step

            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE18H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE18L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;
            _u16PreLockedHB=u16Data;//Pre_Scanned_HB

            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE19H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE19L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;//Pre_Scanned_LB

            _u16PreLockedLB=u16Data;
            *blindscanlock=1;


            DBG_INTERN_DVBS(printk("Current Locked CF:%d BW:%d BWH:%d BWL:%d CFO:%d Step:%d blindscanlock=%d \n", _u16LockedCenterFreq, _u16LockedSymbolRate,_u16PreLockedHB, _u16PreLockedLB, _s16CurrentCFO, _u16CurrentStepSize, *blindscanlock));//
/*START: auto iq swap detection (4/7)*/
            //Unicable_System_Flag=TRUE;  //if all kind of blindscan need to do automatic IQ SWAP detection, do this line.
            if((_u16ChannelInfoIndex==1)&&(Unicable_System_Flag==TRUE)&&(iq_swap_checking_flag==FALSE)&&(iq_swap_done_flag==FALSE))
            {
                Unicable_System_Flag=FALSE;
                iq_swap_checking_flag=TRUE;
                if(_s16CurrentCFO==0)
                {

                    if(_u16LockedSymbolRate>=15000)
                    {
                        cfo_check_iq_swap = -2000; // stong recommend minus value
                    }
                    else
                    {
                        cfo_check_iq_swap = -1000;
                    }
                    MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DUMMY_REG_6, 1); //enable force coarse cfo mechenism
                    //configure force cfo value
                    MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DUMMY_REG_7, (U16)((-1)*cfo_check_iq_swap)&0xFF);
                    MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DUMMY_REG_8, (U16)(((-1)*cfo_check_iq_swap)>>8)&0xFF);

                    cf_iq_swap_check=(_u16LockedCenterFreq*1000+cfo_check_iq_swap)/1000;
                }
                else
                {
                    MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DUMMY_REG_6, 1);
                    //configure force cfo value
                    MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DUMMY_REG_7, 0);
                    MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DUMMY_REG_8, 0);

                    cfo_check_iq_swap = _s16CurrentCFO*1000;
                    cf_iq_swap_check = _u16LockedCenterFreq;
                }
                bw_iq_swap_check = _u16LockedSymbolRate;
                u8Data=(bw_iq_swap_check&0xFF);
                status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_L, u8Data);
                u8Data=((bw_iq_swap_check>>8)&0xFF);
                status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_H, u8Data);
                status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_MB_DMDTOP_DBG_5, 0);

                *u8Progress = 100;
                DBG_INTERN_DVBS(printk("DVBS_BlindScan_WaitCurFreqFinished check IQ start  cf_iq_swap_check [%d] cfo_check_iq_swap [%d] \n",cf_iq_swap_check,cfo_check_iq_swap));
                return status;
            }
/*END: auto iq swap detection (4/7)*/
        }
        else if (u8Data==1)
        {
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_TOP_WR_DBG_93, &u8Data);
            //printk("MDrv_Demod_BlindScan_SetTunerFreq E_DMD_S2_MB_TOP_WR_DBG_93=%d\n", u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_TOP_WR_DBG_92, &u8Data);
            //printk("MDrv_Demod_BlindScan_SetTunerFreq E_DMD_S2_MB_TOP_WR_DBG_92=%d\n", u8Data);
            u16Data=(u16Data<<8)|u8Data;
            _u16NextCenterFreq=u16Data;

            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE12H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE12L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;
            _u16PreLockedHB=u16Data;//Pre_Scanned_HB


            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE13H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE13L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;
            _u16PreLockedLB=u16Data;//Pre_Scanned_LB

            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE14H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE14L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;
            _u16CurrentSymbolRate=u16Data;//Fine_Symbol_Rate

            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE15H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE15L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;//Center_Freq_Offset

            if (u16Data*1000 >= 0x8000)
            {
                u16Data=0x1000- u16Data*1000;
                _s16CurrentCFO=-1*u16Data/1000;
            }
            else
            {
                _s16CurrentCFO=u16Data;
            }
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE16H, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_SWUSE16L, &u8Data);
            u16Data=(u16Data<<8)|u8Data;
            _u16CurrentStepSize=u16Data;//Tuner_Frequency_Step

            *blindscanlock=0;

            DBG_INTERN_DVBS(printk("Pre Locked CF:%d BW:%d HBW:%d LBW:%d Current CF:%d BW:%d CFO:%d Step:%d blindscanlock=%d \n", _u16LockedCenterFreq, _u16LockedSymbolRate,_u16PreLockedHB, _u16PreLockedLB,  _u16NextCenterFreq-_u16CurrentStepSize, _u16CurrentSymbolRate, _s16CurrentCFO, _u16CurrentStepSize, *blindscanlock));//
        }
    }
    *u8Progress=100;
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_SWUSE00L, &u8Data);
    DBG_INTERN_DVBS(printk("E_DMD_S2_MB_DMDTOP_SWUSE00L: %d \n", u8Data));

    status &= MHal_Demod_MB_ReadReg(DMDANA_REG_BASE+0xC0, &u8Data);
    DBG_INTERN_DVBS(printk("NOW IQ SWAP setting: %d \n", u8Data));
    //printk("MDrv_Demod_BlindScan_WaitCurFreqFinished- u8Progress: %d u8FindNum %d\n", *u8Progress, *u8FindNum);
    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_WaitCurFreqFinished- u8Progress: %d \n", *u8Progress));

    return status;
}

BOOL DVBS_BlindScan_GetTunerFreq(U32 *u16TunerCenterFreq, U32 *u16TunerCutOffFreq)
{
    BOOL status=TRUE;
    U8   u8Data=0;
    U16  u16WaitCount;
    U16  u16TunerCutOff;

    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_GetTunerFreq+\n"));

    MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
    if ((u8Data&0x02)==0x02)
    {
        u8Data|=0x08;
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
        u16WaitCount=0;
        do
        {
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data);//SubState
            u16WaitCount++;
            //DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_NextFreq u8Data:0x%x u16WaitCount:%d\n", u8Data, u16WaitCount));
            mdelay(1);//udelay(1*1000);//MsOS_DelayTask(1);
        }while((u8Data!=0x01)&&(u16WaitCount<INTERN_DVBS_DEMOD_WAIT_TIMEOUT));
    }
    else if ((u8Data&0x01)==0x01)
    {
        u8Data|=0x20;
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
        u16WaitCount=0;
        do
        {
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data);//SubState
            u16WaitCount++;
            //DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_NextFreq u8Data:0x%x u16WaitCount:%d\n", u8Data, u16WaitCount));
            mdelay(1);//udelay(1*1000);//MsOS_DelayTask(1);
        }while((u8Data!=0x01)&&(u16WaitCount<INTERN_DVBS_DEMOD_WAIT_TIMEOUT));
        MHal_Demod_MB_ReadReg(TOP_REG_BASE + 0x60*2, &u8Data);
        u8Data|=0x02;
        MHal_Demod_MB_WriteReg(TOP_REG_BASE + 0x60*2, u8Data);
    }
    u16WaitCount=0;

    _u16TunerCenterFreq=0;
/*START: auto iq swap detection (5/7)*/
    if((iq_swap_checking_flag==TRUE)&&(iq_swap_done_flag==FALSE))
    {
        _u16TunerCenterFreq = cf_iq_swap_check;
    }
    else
/*END: auto iq swap detection (5/7)*/
    {
    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_TOP_WR_DBG_93, &u8Data);
    //printk("INTERN_DVBS_BlindScan_GetTunerFreq E_DMD_S2_MB_TOP_WR_DBG_93=%d\n", u8Data);
    _u16TunerCenterFreq=u8Data;
    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_TOP_WR_DBG_92, &u8Data);
    //printk("INTERN_DVBS_BlindScan_GetTunerFreq E_DMD_S2_MB_TOP_WR_DBG_92=%d\n", u8Data);
    _u16TunerCenterFreq=(_u16TunerCenterFreq<<8)|u8Data;
    }
    *u16TunerCenterFreq = _u16TunerCenterFreq;

/*START: auto iq swap detection (6/7)*/
    if((iq_swap_checking_flag==TRUE)&&(iq_swap_done_flag==FALSE))
    {
        u16TunerCutOff = bw_iq_swap_check;
    }
    else
/*END: auto iq swap detection (6/7)*/
    {
    //claire test
    u16TunerCutOff=44000;
    if(_u16TunerCenterFreq<=990)//980
    {
        status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_BALANCE_TRACK, &u8Data);
       if(u8Data==0x01)
       {
          if(_u16TunerCenterFreq<970)//970
          {
            u16TunerCutOff=10000;
          }
          else
          {
            u16TunerCutOff=20000;
          }
          u8Data=0x02;
            status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_BALANCE_TRACK, u8Data);
       }
       else if(u8Data==0x02)
       {
          u8Data=0x00;
            status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_BALANCE_TRACK, u8Data);
       }
    }
    }
    if(u16TunerCutOffFreq != NULL)
    {
        *u16TunerCutOffFreq = u16TunerCutOff;
    }

    //end claire test
    DBG_INTERN_DVBS(printk("MDrv_Demod_BlindScan_GetTunerFreq- _u16TunerCenterFreq:%d u16TunerCutOff:%d\n", _u16TunerCenterFreq,u16TunerCutOff ));
    //Set Tuner Frequency
    //MsOS_DelayTask(10);

    return status;
}

BOOL DVBS_BlindScan_GetFoundTP(U32* u32Freq, U32* u32SymbolRate)
{
    U16 u16Num = 1;
    HAL_DEMOD_MS_FE_CARRIER_PARAM stChannel;
    BOOL bRet = TRUE;

    memset(&stChannel, 0, sizeof(HAL_DEMOD_MS_FE_CARRIER_PARAM));


    if(DVBS_BlindScan_GetChannel(&u16Num,&stChannel) != TRUE)
    {
        *u32Freq = 0;
        *u32SymbolRate = 0;
        bRet = FALSE;
    }
    else
    {
        if(u16Num == 0)
        {
            *u32Freq = 0;
            *u32SymbolRate = 0;
            bRet = FALSE;
        }
        else
        {
            *u32Freq = stChannel.u32Frequency;//1000;
            *u32SymbolRate = stChannel.SatParam.u32SymbolRate;// /1000;
            bRet = TRUE;
        }
    }

    return bRet;
}

/****************************************************************************
Subject:    To get the Post viterbi BER
Function:   MSB1228_GetPostViterbiBer
Parmeter:  Quility
Return:       E_RESULT_SUCCESS
E_RESULT_FAILURE =>Read I2C fail, MSB1228_VIT_STATUS_NG
Remark:     For the Performance issue, here we just return the Post Value.(Not BER)
We will not read the Period, and have the "/256/8"
 *****************************************************************************/
#if  0//!defined(MSOS_TYPE_LINUX)
static float LogApproxTableX[80] =
{ 1.00, 1.30, 1.69, 2.20, 2.86, 3.71, 4.83, 6.27, 8.16, 10.60, 13.79,
  17.92, 23.30, 30.29, 39.37, 51.19, 66.54, 86.50, 112.46, 146.19,
  190.05, 247.06, 321.18, 417.54, 542.80, 705.64, 917.33, 1192.53,
  1550.29, 2015.38, 2620.00, 3405.99, 4427.79, 5756.13, 7482.97,
  9727.86, 12646.22, 16440.08, 21372.11, 27783.74, 36118.86,
  46954.52, 61040.88, 79353.15, 103159.09, 134106.82, 174338.86,
  226640.52, 294632.68, 383022.48, 497929.22, 647307.99, 841500.39, 1093950.50,
  1422135.65, 1848776.35, 2403409.25, 3124432.03, 4061761.64, 5280290.13,
  6864377.17, 8923690.32, 11600797.42, 15081036.65, 19605347.64, 25486951.94,
  33133037.52, 43072948.77, 55994833.40, 72793283.42, 94631268.45,
  123020648.99, 159926843.68, 207904896.79, 270276365.82, 351359275.57,
  456767058.24, 593797175.72, 771936328.43, 1003517226.96
};

static float LogApproxTableY[80] =
{   0.00, 0.11, 0.23, 0.34, 0.46, 0.57, 0.68, 0.80, 0.91, 1.03, 1.14, 1.25, 1.37,
    1.48, 1.60, 1.71, 1.82, 1.94, 2.05, 2.16, 2.28, 2.39, 2.51, 2.62, 2.73, 2.85,
    2.96, 3.08, 3.19, 3.30, 3.42, 3.53, 3.65, 3.76, 3.87, 3.99, 4.10, 4.22, 4.33,
    4.44, 4.56, 4.67, 4.79, 4.90, 5.01, 5.13, 5.24, 5.36, 5.47, 5.58, 5.70, 5.81,
    5.93, 6.04, 6.15, 6.27, 6.38, 6.49, 6.61, 6.72, 6.84, 6.95, 7.06, 7.18, 7.29,
    7.41, 7.52, 7.63, 7.75, 7.86, 7.98, 8.09, 8.20, 8.32, 8.43, 8.55, 8.66, 8.77,
    8.89, 9.00
};
/****************************************************************************
Subject:    Function providing approx. result of Log10(X)
Function:   Log10Approx
Parmeter:   Operand X in float
Return:     Approx. value of Log10(X) in float
Remark:     Ouput range from 0.0, 0.3 to 9.6 (input 1 to 2^32)

 *****************************************************************************/

static float Log10Approx(float flt_x)
{
    U8    indx = 0;

    do{
          if (flt_x < LogApproxTableX[indx])
          break;
    }while (++indx < 79);  //stop at indx = 80

    return LogApproxTableY[indx];
}

#endif

/****************************************************************************
Subject:    Read the signal to noise ratio (SNR)
Function:   MSB1228_DVBTGetSNR
Parmeter:   None
Return:     -1 mean I2C fail, otherwise I2C success then return SNR value
Remark:
 *****************************************************************************/
 MS_U64 unsigned_ipow(MS_U32 num, MS_U32 power)
{
    MS_U64 t = (MS_U64)num;
    if(!power)
        return 1;
    while(--power)
    {
        t *= num;
    }
    return t;
}
int _SNR_Divide_Rounding_Off(int num, int round)
{
    int _num = num/round;
    if(num%round >= (round/2)) //rounding off
    {
        _num++;
    }
    return _num;
}

//S=>1.5~16
//snr_poly = 0.0002357*pow(SNR, 5)-0.01403*pow(SNR, 4)+0.3309*pow(SNR, 3)-3.882*pow(SNR, 2)+23.84*pow(SNR, 1)-54.93;
//_SNR = (real cnr) * 1000
int _SNR_Fit_Poly_DVBS(int _SNR)
{
    int Result = 0;
    //(0.0002357*pow(SNR, 5)-0.01403*pow(SNR, 4)+0.3309*pow(SNR, 3)-3.882*pow(SNR, 2)+23.84*pow(SNR, 1)-54.93)*10000000
    MS_S32 SNR_POLY3_A = ((2357 * _SNR / 1000) - 140300) * _SNR / 1000 + 3309000;
    MS_U64 SNR_POLY3_B = unsigned_ipow(_SNR, 3);
    MS_U64 SNR_POLY2 = div64_u64(38820000 * unsigned_ipow(_SNR, 2), unsigned_ipow(1000, 2));//38820000 * unsigned_ipow(_SNR, 2) / unsigned_ipow(1000, 2);
    MS_U64 SNR_POLY1 = div64_u64(238400000 * unsigned_ipow(_SNR, 1), unsigned_ipow(1000, 1));//238400000 * unsigned_ipow(_SNR, 1) / unsigned_ipow(1000, 1);
    MS_U64 SNR_POLY0 = 549300000;// * unsigned_ipow(_SNR, 0) / unsigned_ipow(1000, 0);
    Result = div64_s64((SNR_POLY3_A * SNR_POLY3_B) , unsigned_ipow(1000, 3)) - SNR_POLY2 + SNR_POLY1 - SNR_POLY0;//(SNR_POLY3_A * SNR_POLY3_B) / unsigned_ipow(1000, 3) - SNR_POLY2 + SNR_POLY1 - SNR_POLY0;//need check
    Result = _SNR_Divide_Rounding_Off(Result, 10000);
    return Result;
}

//8PSK=>6~16
//snr_poly = 0.001305*pow(SNR, 3)-0.05452*pow(SNR, 2)+1.823*pow(SNR, 1)-4.642;
//_SNR = (real cnr) * 1000
int _SNR_Fit_Poly_DVBS2_8PSK(int _SNR)
{
    int Result = 0;
    //(0.001305*pow(SNR, 3)-0.05452*pow(SNR, 2)+1.823*pow(SNR, 1)-4.642)*1000000
    MS_U64 SNR_POLY3 = div64_u64(1305 * unsigned_ipow(_SNR, 3), unsigned_ipow(1000, 3));//1305 * unsigned_ipow(_SNR, 3) / unsigned_ipow(1000, 3);
    MS_U64 SNR_POLY2 = div64_u64(54520 * unsigned_ipow(_SNR, 2), unsigned_ipow(1000, 2));//54520 * unsigned_ipow(_SNR, 2) / unsigned_ipow(1000, 2);
    MS_U64 SNR_POLY1 = div64_u64(1823000 * unsigned_ipow(_SNR, 1), unsigned_ipow(1000, 1));//1823000 * unsigned_ipow(_SNR, 1) / unsigned_ipow(1000, 1);
    MS_U64 SNR_POLY0 = 4642000;// * unsigned_ipow(_SNR, 0) / unsigned_ipow(1000, 0);
    Result = SNR_POLY3 - SNR_POLY2 + SNR_POLY1 - SNR_POLY0;
    Result = _SNR_Divide_Rounding_Off(Result, 1000);
    return Result;
}

//QPSK=>1~16
//snr_poly = -0.0002291*pow(SNR, 4)+0.01153*pow(SNR, 3)-0.2197*pow(SNR, 2)+2.963*pow(SNR, 1)-7.456;
int _SNR_Fit_Poly_DVBS2_QPSK(int _SNR)
{
    int Result = 0;
    //(-0.0002291*pow(SNR, 4)+0.01153*pow(SNR, 3)-0.2197*pow(SNR, 2)+2.963*pow(SNR, 1)-7.456)*10000000
    MS_S32 SNR_POLY3_A = ((-2291 * _SNR) / 1000) + 115300;
    MS_U64 SNR_POLY3_B = unsigned_ipow(_SNR, 3);
    MS_U64 SNR_POLY2 = div64_u64(2197000 * unsigned_ipow(_SNR, 2), unsigned_ipow(1000, 2));//2197000 * unsigned_ipow(_SNR, 2) / unsigned_ipow(1000, 2);
    MS_U64 SNR_POLY1 = div64_u64(29630000 * unsigned_ipow(_SNR, 1), unsigned_ipow(1000, 1));//29630000 * unsigned_ipow(_SNR, 1) / unsigned_ipow(1000, 1);
    MS_U64 SNR_POLY0 = 74560000;// * unsigned_ipow(_SNR, 0) / unsigned_ipow(1000, 0);
    Result = div64_s64((SNR_POLY3_A * SNR_POLY3_B) , unsigned_ipow(1000, 3)) - SNR_POLY2 + SNR_POLY1 - SNR_POLY0;//(SNR_POLY3_A * SNR_POLY3_B) / unsigned_ipow(1000, 3) - SNR_POLY2 + SNR_POLY1 - SNR_POLY0;//need check
    Result = _SNR_Divide_Rounding_Off(Result, 10000);
    return Result;
}

int _SNR_RawData_Converter(int num, int mode)
{
    int SNR;
    int LOG_SNR = num;

    if(LOG_SNR<3000 || (LOG_SNR>=16300)) //LOG_SNR<2.0 or LOG_SNR>=16.3
    {
        SNR = _SNR_Divide_Rounding_Off(LOG_SNR, 100);//LOG_SNR/100;
    }
    else
    {
        int FitSNR = 0;
        switch(mode){
            case 0: //DVBS2 QPSK
                FitSNR = _SNR_Fit_Poly_DVBS2_QPSK(LOG_SNR);
                break;
            case 1: //DVBS2 8PSK
                FitSNR = _SNR_Fit_Poly_DVBS2_8PSK(LOG_SNR);
                break;
            default: //DVBS
                FitSNR = _SNR_Fit_Poly_DVBS(LOG_SNR);
                if(FitSNR>13500)
                    FitSNR += 100;
                break;
        }

        if(FitSNR > 20000 || FitSNR <= 0)
        {
            SNR = _SNR_Divide_Rounding_Off(LOG_SNR, 100);
        }
        else
        {
            if(FitSNR > 9900)
            {
                SNR = _SNR_Divide_Rounding_Off(FitSNR, 100);
                if(SNR >= 101 && SNR < 159)
                {
                    --SNR;
                }
            }
            else
            {
                SNR = FitSNR / 100;
            }
        }
    }

    return SNR;
}


#define SNR_FIFO_SIZE 1 ///2
MS_U32 SNR_FIFO_ARRAY[SNR_FIFO_SIZE]={};
#define MIN_CNR 61
#define MAX_CNR 161
int SNR_FIFO_IDX = 0;

MS_U32 _SNR_Moving_AVG(MS_U32 snr)
{
    int val=0, i=0;
    int nonzero_cnt = 0;

    if(SNR_FIFO_IDX >= SNR_FIFO_SIZE)
    {
        SNR_FIFO_IDX = 0;
    }
    SNR_FIFO_ARRAY[SNR_FIFO_IDX % SNR_FIFO_SIZE] = snr;
    SNR_FIFO_IDX++;

    for(i=0; i<SNR_FIFO_SIZE; i++)
    {
        val += SNR_FIFO_ARRAY[i];
        if(SNR_FIFO_ARRAY[i] != 0)
        {
            nonzero_cnt++;
        }
        //printk("val=%d nonzero_cnt=%d\n",val,nonzero_cnt);
    }

    if(nonzero_cnt)
    {
        val /= nonzero_cnt;
    }

    return val;
}

B16 DVBS_GetSNRForAirtel (U32 *f_snr) 
{
    MS_BOOL status=TRUE,S2_flag=0;
    MS_U16 u16Data,u16Address;
    MS_U8  u8Data,S2_code_rate,S_code_rate;
    MS_U32 SNR,SNR_temp;
    int mode = -1; //-1:DVBS, 0:DVBS2-QPSK, 1:DVBS2-8PSK
    DMD_DVBS_MODULATION_TYPE pQAMMode;

    u16Address=0x0990;
    status&=MHal_Demod_MB_ReadReg(u16Address, &u8Data);
    u8Data|=0x04;
    status&=MHal_Demod_MB_WriteReg(u16Address, u8Data);
    
    //set alpha
    u16Address=0x3C66;
    u8Data=0xfa;
    status&=MHal_Demod_MB_WriteReg(u16Address, u8Data);
    u16Address=0x3C90;
    u8Data=0x01;
    status&=MHal_Demod_MB_WriteReg(u16Address, u8Data);

    //latch protect
    u16Address=0x28FE;//0x3efe;
    status&=MHal_Demod_MB_ReadReg(u16Address, &u8Data);
    u8Data=u8Data|0x01;
    status&=MHal_Demod_MB_WriteReg(u16Address, u8Data);

    //read SNR
    status&=MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_INFO_02,&u8Data);
    u16Data=u8Data;
    status&=MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_INFO_01,&u8Data);
    u16Data = (u16Data<<8)|u8Data;
    SNR=(MS_U32)u16Data;

    if(SNR>=32768)
       SNR=SNR-65536;
    SNR=_SNR_Moving_AVG(SNR);
    SNR=(SNR*1000)>>8; //LOG10

    //latch protect
    u16Address=0x28FE;//0x3efe;
    status&=MHal_Demod_MB_ReadReg(u16Address, &u8Data);
    u8Data=u8Data&0xFE;
    status&=MHal_Demod_MB_WriteReg(u16Address, u8Data);



    //Get DVBS2 mode
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
    if(u8Data == FALSE)
    {
        u16Address=0x3B80;
        status&=MHal_Demod_MB_ReadReg(u16Address, &u8Data);
        mode = ((u8Data & 0x30) >> 4);
        MHal_Demod_MB_ReadDspReg(E_DMD_S2_CODE_RATE, &S2_code_rate);
        S2_flag=1;
    }
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_CODE_RATE, &S_code_rate);

    SNR = _SNR_RawData_Converter(SNR, mode);

    if(SNR <= 60)
    {
        if(--SNR_UNDER6_CNT < 0)
        {
            SNR_UNDER6 = TRUE;
            SNR_UNDER6_CNT = 0;
        }
        else
        {
            SNR_UNDER6 = FALSE;
        }
    }
    else
    {
        SNR_UNDER6 = FALSE;
        SNR_UNDER6_CNT = SNR_UNDER6_MAX_CNT;
    }

    SNR_UPDATE_TS = (U32)jiffies_to_msecs(jiffies);
    DVBS_GetCurrentModulationType(&pQAMMode);
    //if ((S2_flag==1)&&(pQAMMode == DMD_DVBS_8PSK)&&(S2_code_rate==0x08))
    //    SNR_temp=SNR+3;
    if (pQAMMode == DMD_DVBS_8PSK)
    {
        if (SNR<99)    
            SNR_temp=SNR+2;
        else
            SNR_temp=SNR+3;
    }
    else if ((S2_flag==1)&&(pQAMMode == DMD_DVBS_QPSK) && (S_code_rate==0x01))
    {
        SNR_temp=SNR+4;
    }
    else
    {
        if ((S2_flag==1)&&(SNR<103))
            SNR_temp=SNR+2;
        else    
            SNR_temp=SNR+3;
    }
    DBG_GET_SIGNAL_DVBS(printk("SNR_temp=%d,S2_code_rate=%d,S_code_rate=%d,S2_flag=%d\n",SNR_temp,S2_code_rate,S_code_rate,S2_flag));

     //else if ((pQAMMode == DMD_DVBS_8PSK)&&(S2_code_rate==0x06))
     //SNR_temp=SNR+5;
    SNR_temp=SNR_temp-1;// kevin fit
    if(SNR_temp > MAX_CNR)
    {
        SNR_temp = MAX_CNR;
    }
    else if(SNR_temp < MIN_CNR)
    {
        SNR_temp = 0;
        return FALSE;
    }
    
    
    u16Address=0x0990;
    status&=MHal_Demod_MB_ReadReg(u16Address, &u8Data);
    u8Data=u8Data&0xFB;;
    status&=MHal_Demod_MB_WriteReg(u16Address, u8Data);

    *f_snr=SNR_temp;//*f_snr=SNR_temp/10;//remove divide 10, remove DVBS_GetSignalQualityForAirtel mutiple 10=> result should be the same
    //SNR_OLD=*f_snr;
    DBG_GET_SIGNAL_DVBS(printk("SNR_india=%d\n",*f_snr));
    return status;

}


B16 DVBS_GetSNR (U32 *f_snr)
{
    BOOL status= TRUE;
    U8   u8Data =0;
    U16  u16Data =0;
    //MS_U64  snr_poly =0.0;
    //MS_U64  Fixed_SNR=0;
    //MS_U64  f_snr_val = 0;
    //MS_U64  dividend64=0;
    //MS_U64  divisor64=0;

    if(DVBS_GetLock(DMD_DVBS_GETLOCK,  0)== FALSE)
    {
        return 0;
    }

    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_DBG_7, &u8Data);
    u16Data=u8Data;
    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_DBG_6, &u8Data);
    u16Data = (u16Data<<8)|u8Data;
    #if 0
    f_snr_val=(float)u16Data/256.0;
    DBG_GET_SIGNAL_DVBS(printk("INTERN_DVBS GetSNR = %d \n ", (int)f_snr_val));
    snr_poly = 0.005261367463671*pow(f_snr_val, 3)-0.116517828301214*pow(f_snr_val, 2)+0.744836970505452*pow(f_snr_val, 1)-0.86727609780167;
    Fixed_SNR = f_snr_val + snr_poly;
    _f_DVBS_CurrentSNR=Fixed_SNR;

    if ((Fixed_SNR < 20.0) && (Fixed_SNR >= 17.0))
        Fixed_SNR = Fixed_SNR - 0.8;
    else if ((Fixed_SNR < 22.5) && (Fixed_SNR >= 20.0))
        Fixed_SNR = Fixed_SNR - 2.0;
    else if ((Fixed_SNR < 27.0) && (Fixed_SNR >= 22.5))
        Fixed_SNR = Fixed_SNR - 3.0;
    else if ((Fixed_SNR < 29.0) && (Fixed_SNR >= 27.0))
        Fixed_SNR = Fixed_SNR - 3.5;
    else if (Fixed_SNR >= 29.0)
        Fixed_SNR = Fixed_SNR - 3.0;


    if(Fixed_SNR < 1.0)
        Fixed_SNR = 1.0;
    if(Fixed_SNR > 30.0)
        Fixed_SNR = 30.0;
    *f_snr = Fixed_SNR;
    //printk("[DVBS]: NDA_SNR=============================: %.1f\n", NDA_SNR);
    ///////////////////////////////////////////////for calc without float case/////////////////////////////////////////////////////////////////////////////////////
    dividend64=u16Data;
    divisor64=256;
    f_snr_val=div64_u64(dividend64*100 , divisor64);//
    DBG_GET_SIGNAL_DVBS(printk("INTERN_DVBS GetSNR = %lld \n ", f_snr_val));
    //snr_poly = 0.005261367463671*pow(f_snr_val, 3)-0.116517828301214*pow(f_snr_val, 2)+0.744836970505452*pow(f_snr_val, 1)-0.86727609780167;
    snr_poly = 5261367463671*f_snr_val*f_snr_val*f_snr_val-116517828301214*f_snr_val*f_snr_val+744836970505452*f_snr_val-867276097801670;//gain 10^15
    Fixed_SNR = f_snr_val + div64_u64(snr_poly , 10E15);
    _f_DVBS_CurrentSNR=Fixed_SNR;

    if ((Fixed_SNR < 2000) && (Fixed_SNR >= 1700))
        Fixed_SNR = Fixed_SNR - 80;
    else if ((Fixed_SNR < 2250) && (Fixed_SNR >= 2000))
        Fixed_SNR = Fixed_SNR - 200;
    else if ((Fixed_SNR < 2700) && (Fixed_SNR >= 2250))
        Fixed_SNR = Fixed_SNR - 300;
    else if ((Fixed_SNR < 2900) && (Fixed_SNR >= 2700))
        Fixed_SNR = Fixed_SNR - 350;
    else if (Fixed_SNR >= 2900)
        Fixed_SNR = Fixed_SNR - 300;


    if(Fixed_SNR < 100)
        Fixed_SNR = 100;
    if(Fixed_SNR > 3000)
        Fixed_SNR = 3000;
    *f_snr = Fixed_SNR;
    printk("[DVBS]: NDA_SNR=============================: %d\n", *f_snr);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    #endif
    *f_snr = u16Data/256;
    printk("NDA_SNR=============================: %d\n", *f_snr);
    return status;
}




/****************************************************************************
Subject:    To get the DVB-S Signal Strength
Function:   DVBS_GetSignalStrength
Parmeter:   strength
Return:     E_RESULT_SUCCESS
E_RESULT_FAILURE
Remark:     Implement SSI refering to Teracom min. spec 2.0 4.1.1.6
 *****************************************************************************/
B16 DVBS_GetSignalStrength(U16 *pu16SignalBar)
{

    //-1.2~-92.2 dBm
    BOOL status = true;
    U8   u8Data =0;
    U8   _u8_DVBS2_CurrentCodeRateLocal = 0;
    //float   ch_power_db=0.0f, ch_power_db_rel=0.0f;
    MS_S64   ch_power_db=0, ch_power_db_rel=0;
    MS_S64  dividend64=0;
    MS_S64  divisor64=0;
    U8   u8Data2 = 0;
    U8   _u8_DVBS2_CurrentConstellationLocal = 0;
    DMD_DVBS_DEMOD_TYPE pDemodType = 0;
    DBG_INTERN_DVBS_TIME(printk("INTERN_DVBS_GetSignalStrength, start\n"));

    // if (INTERN_DVBC_Lock(COFDM_TPS_LOCK))
    // if (INTERN_DVBC_Lock(COFDM_AGC_LOCK))
    // Actually, it's more reasonable, that signal level depended on cable input power level
    // thougth the signal isn't dvb-t signal.
    //
    // use pointer of IFAGC table to identify
    // case 1: RFAGC from SAR, IFAGC controlled by demod
    // case 2: RFAGC from tuner, ,IFAGC controlled by demod
    /*status=HAL_DMD_GetRFLevel(&ch_power_db, fRFPowerDbm, u8SarValue,
                              sDMD_DVBS_InitData->pTuner_RfagcSsi, sDMD_DVBS_InitData->u16Tuner_RfagcSsi_Size,
                              sDMD_DVBS_InitData->pTuner_IfagcSsi_HiRef, sDMD_DVBS_InitData->u16Tuner_IfagcSsi_HiRef_Size,
                              sDMD_DVBS_InitData->pTuner_IfagcSsi_LoRef, sDMD_DVBS_InitData->u16Tuner_IfagcSsi_LoRef_Size,
                              sDMD_DVBS_InitData->pTuner_IfagcErr_HiRef, sDMD_DVBS_InitData->u16Tuner_IfagcErr_HiRef_Size,
                              sDMD_DVBS_InitData->pTuner_IfagcErr_LoRef, sDMD_DVBS_InitData->u16Tuner_IfagcErr_LoRef_Size);
     */
    ch_power_db = DVBS_GetTunrSignalLevel_PWR();
    //printk("@@@@@@@@@ ch_power_db = %lld \n", ch_power_db);

    status &= INTERN_DVBS_GetCurrentDemodType(&pDemodType);

    if((U8)pDemodType == (U8)DMD_SAT_DVBS)//S
    {
        MS_S64 fDVBS_SSI_Pref[]=
        {
            //0,       1,       2,       3,       4
            -78900,   -77150,  -76140,  -75190,  -74570,//QPSK//gain SHIFT_MULTIPLE
        };
        status &= MHal_Demod_MB_ReadReg(DVBSFEC_REG_BASE + 0x84, &u8Data);
        _u8_DVBS2_CurrentCodeRateLocal = (u8Data & 0x07);
        ch_power_db_rel = ch_power_db*SHIFT_MULTIPLE- fDVBS_SSI_Pref[_u8_DVBS2_CurrentCodeRateLocal];//gain SHIFT_MULTIPLE
    }
    else
    {
        MS_S64 fDVBS2_SSI_Pref[][11]=
        {
            //  0,    1,       2,       3,       4,       5,       6,       7,       8,        9,       10
            //1/4,    1/3,     2/5,     1/2,     3/5,     2/3,     3/4,     4/5,     5/6,      8/9,     9/10
            {-85170, -84080,  -83150,  -81860,  -80630,  -79770,  -78840,  -78190,  -77690,   -76680,  -76460}, //QPSK//gain SHIFT_MULTIPLE
            {   0,    0,     0,     0,  -77360,  -76240,  -74950,     0,  -73520,   -72180,  -71840}  //8PSK//gain SHIFT_MULTIPLE
        };

        status &= MHal_Demod_MB_ReadReg(DVBS2_INNER_REG_BASE + 0xD7, &u8Data);
        _u8_DVBS2_CurrentCodeRateLocal = (u8Data & 0x3C)>>2;

        status &= MHal_Demod_MB_ReadReg(DVBS2_INNER_REG_BASE + 0xD7, &u8Data);
        status &= MHal_Demod_MB_ReadReg(DVBS2_INNER_REG_BASE + 0xD6, &u8Data2);

        if(((u8Data & 0x03)==0x01) && ((u8Data2 & 0x80)==0x00))
        {
            _u8_DVBS2_CurrentConstellationLocal = DMD_DVBS_QPSK;
        }
        else if (((u8Data & 0x03)==0x01) && ((u8Data2 & 0x80)==0x80))
        {
            _u8_DVBS2_CurrentConstellationLocal = DMD_DVBS_8PSK;//8PSK
        }
        ch_power_db_rel = ch_power_db*SHIFT_MULTIPLE - fDVBS2_SSI_Pref[_u8_DVBS2_CurrentConstellationLocal][_u8_DVBS2_CurrentCodeRateLocal];//gain SHIFT_MULTIPLE
    }
/*
    if(ch_power_db_rel <= -15.0f)
    {
        *pu16SignalBar = 0;
    }
    else if (ch_power_db_rel <= 0.0f)
    {
        *pu16SignalBar = (MS_U16)(2.0f/3 * (ch_power_db_rel+15.0f));
    }
    else if (ch_power_db_rel <= 20.0f)
    {
        *pu16SignalBar = (MS_U16)(4.0f * ch_power_db_rel + 10.0f);
    }
    else if (ch_power_db_rel <= 35.0f)
    {
        *pu16SignalBar = (MS_U16)(2.0f/3 * (ch_power_db_rel-20.0f) + 90.0);
    }
*/
    if(ch_power_db_rel <= -0)//gain SHIFT_MULTIPLE
    {
        *pu16SignalBar = 0;
    }
    else if (ch_power_db_rel <= 5000)
    {
        dividend64 = ch_power_db_rel*2;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2)/(3*SHIFT_MULTIPLE)  + 2);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+2;
    }
    else if (ch_power_db_rel <= 10500)
    {
        dividend64 = ch_power_db_rel*2-10;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-10)/(3*SHIFT_MULTIPLE)  + 11);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+11;
    }
    else if (ch_power_db_rel <= 15500)
    {
        dividend64 = ch_power_db_rel*2-10;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-10)/(3*SHIFT_MULTIPLE)  + 17);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+17;
    }
    else if (ch_power_db_rel <= 20500)
    {
        dividend64 = ch_power_db_rel*2-20;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-20)/(3*SHIFT_MULTIPLE)  + 21);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+21;
    }
    else if (ch_power_db_rel <= 25500)
    {
        dividend64 = ch_power_db_rel*2-20;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-20)/(3*SHIFT_MULTIPLE)  + 22);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+22;
    }
    else if (ch_power_db_rel <= 30500)
    {
        dividend64 = ch_power_db_rel*2-20;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-20)/(3*SHIFT_MULTIPLE)  + 26);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+26;
    }
    else if (ch_power_db_rel <= 33000)
    {
        dividend64 = ch_power_db_rel*2-20;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-20)/(3*SHIFT_MULTIPLE)  + 37);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+37;
    }
    else if (ch_power_db_rel <= 41000)
    {
        dividend64 = ch_power_db_rel*2-40;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-40)/(3*SHIFT_MULTIPLE)  + 43);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+43;
    }
    else if (ch_power_db_rel <= 44000)
    {
        dividend64 = ch_power_db_rel*2-40;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-40)/(3*SHIFT_MULTIPLE)  + 44);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+44;
    }
    else if (ch_power_db_rel <= 50000)
    {
        dividend64 = ch_power_db_rel*2-40;
        divisor64 = 3*SHIFT_MULTIPLE;
        //*pu16SignalBar = (U16)((ch_power_db_rel*2-40)/(3*SHIFT_MULTIPLE)  + 48);
        *pu16SignalBar = (U16)div64_s64(dividend64,divisor64)+48;
    }
    else
    {
        *pu16SignalBar = 100;
    }

    //printk("SSI_CH_PWR(dB) = %f \n", ch_power_db_rel);
    DBG_INTERN_DVBS(printk(">>>>>Signal Strength(SSI) = %d\n", (int)*pu16SignalBar));
    return status;
}

BOOL DVBS_GetSignalQualityForAirtel(U16 *quality)
{

    U32    f_snr_temp = 0;
    U16    f_10snr = 0;
    S_SQI_TABLE *s2_sqi_ptr=NULL;
    U8    s2_sqi_table_len = 0;
    U8 u8_i = 0;
    
    S_SQI_TABLE DVBS_SQI[] =
    {
        { 60, 0},
        { 70, 10},
        { 80, 20},
        { 90, 30},
        { 100, 40},
        { 110, 50},
        { 120, 60},
        { 130, 70},
        { 140, 80},
        { 150, 90},
        { 160, 100},
    };
    

     DVBS_GetSNRForAirtel(&f_snr_temp);
     f_10snr=f_snr_temp;//f_10snr=f_snr_temp*10; remove mutiple for DVBS_GetSNRForAirtel() ouptut format

     s2_sqi_ptr=DVBS_SQI;
     s2_sqi_table_len = sizeof(DVBS_SQI)/sizeof(S_SQI_TABLE);

     if ((s2_sqi_table_len == 0) || (s2_sqi_ptr  == NULL))
     {
         *quality = 0;
     }
     else
     {
         while(s2_sqi_ptr[u8_i].u16_x_10cn < f_10snr)
         {
             u8_i++;
             if(u8_i == s2_sqi_table_len) break;
          }

          if (u8_i == s2_sqi_table_len)
          {
              //*quality = (U16)((float)(s2_sqi_ptr[u8_i-1].u8_y_sqi-s2_sqi_ptr[u8_i-2].u8_y_sqi)/(float)(s2_sqi_ptr[u8_i-1].u16_x_10cn-s2_sqi_ptr[u8_i-2].u16_x_10cn)*(float)(f_10snr-s2_sqi_ptr[u8_i-2].u16_x_10cn)+s2_sqi_ptr[u8_i-2].u8_y_sqi);
              //*quality = (U16)((s2_sqi_ptr[u8_i-1].u8_y_sqi-s2_sqi_ptr[u8_i-2].u8_y_sqi)*(f_10snr-s2_sqi_ptr[u8_i-2].u16_x_10cn)/(s2_sqi_ptr[u8_i-1].u16_x_10cn-s2_sqi_ptr[u8_i-2].u16_x_10cn)+s2_sqi_ptr[u8_i-2].u8_y_sqi);
              *quality = (U16)((s2_sqi_ptr[u8_i-1].u8_y_sqi-s2_sqi_ptr[u8_i-2].u8_y_sqi)/(s2_sqi_ptr[u8_i-1].u16_x_10cn-s2_sqi_ptr[u8_i-2].u16_x_10cn)*(f_10snr-s2_sqi_ptr[u8_i-2].u16_x_10cn)+s2_sqi_ptr[u8_i-2].u8_y_sqi);
          }
          else if (u8_i == 0)
          {
              //*quality = (U16)((float)(s2_sqi_ptr[1].u8_y_sqi-s2_sqi_ptr[0].u8_y_sqi)/(float)(s2_sqi_ptr[1].u16_x_10cn-s2_sqi_ptr[0].u16_x_10cn)*(float)(f_10snr-s2_sqi_ptr[0].u16_x_10cn)+s2_sqi_ptr[0].u8_y_sqi);
              //*quality = (U16)((s2_sqi_ptr[1].u8_y_sqi-s2_sqi_ptr[0].u8_y_sqi)*(f_10snr-s2_sqi_ptr[0].u16_x_10cn)/(s2_sqi_ptr[1].u16_x_10cn-s2_sqi_ptr[0].u16_x_10cn)+s2_sqi_ptr[0].u8_y_sqi);
              *quality = (U16)((s2_sqi_ptr[1].u8_y_sqi-s2_sqi_ptr[0].u8_y_sqi)/(s2_sqi_ptr[1].u16_x_10cn-s2_sqi_ptr[0].u16_x_10cn)*(f_10snr-s2_sqi_ptr[0].u16_x_10cn)+s2_sqi_ptr[0].u8_y_sqi);
             if (*quality & 0x8000) *quality = 0;
           }
           else
           {
              //*quality = (U16)((float)(s2_sqi_ptr[u8_i].u8_y_sqi-s2_sqi_ptr[u8_i-1].u8_y_sqi)/(float)(s2_sqi_ptr[u8_i].u16_x_10cn-s2_sqi_ptr[u8_i-1].u16_x_10cn)*(float)(f_10snr-s2_sqi_ptr[u8_i-1].u16_x_10cn)+s2_sqi_ptr[u8_i-1].u8_y_sqi);
              //*quality = (U16)((s2_sqi_ptr[u8_i].u8_y_sqi-s2_sqi_ptr[u8_i-1].u8_y_sqi)*(f_10snr-s2_sqi_ptr[u8_i-1].u16_x_10cn)/(s2_sqi_ptr[u8_i].u16_x_10cn-s2_sqi_ptr[u8_i-1].u16_x_10cn)+s2_sqi_ptr[u8_i-1].u8_y_sqi);
              *quality = (U16)((s2_sqi_ptr[u8_i].u8_y_sqi-s2_sqi_ptr[u8_i-1].u8_y_sqi)/(s2_sqi_ptr[u8_i].u16_x_10cn-s2_sqi_ptr[u8_i-1].u16_x_10cn)*(f_10snr-s2_sqi_ptr[u8_i-1].u16_x_10cn)+s2_sqi_ptr[u8_i-1].u8_y_sqi);
            }

            if (*quality > 100) *quality = 100;
     }
     printk(">>>>>DVBS_GetSignalQualityForAirtel = %d\n", *quality);
     return TRUE;
}

BOOL DVBS_GetSignalQuality(U16 *quality)
{
    DVBS_GetSignalQualityForAirtel(quality);
    printk(">>>>>DVBS_GetSignalQuality = %d\n", *quality);
#if 0
    //float    fber = 0.0;
    MS_U64    fber = 0;
    //float  log_ber;
    BOOL     status = TRUE;
    //float    f_snr = 0.0, ber_sqi = 0.0, cn_rel = 0.0;
    U32     f_snr = 0;
    MS_U64 ber_sqi=0;
    U32     cn_rel = 0;
    U8       u8Data =0,i=0;
    U16      u16Data =0;
    U8       _u8_DVBS2_CurrentCodeRate_sqi = 0;
    U8       _u8QAM_val = 0;
    U16     bchpkt_error,BCH_Eflag2_Window;//
    //fRFPowerDbm = fRFPowerDbm;
    float snr_poly =0.0;
    //float Fixed_SNR =0.0,Fixed_SNR_temp=0.0;
    U32 Fixed_SNR =0,Fixed_SNR_temp=0;
    
    //double eFlag_PER=0.0;
    U32 eFlag_PER=0;
    MS_U64  dividend64=0;
    MS_U64  divisor64=0;

    if (TRUE == DVBS_GetLock(DMD_DVBS_GETLOCK, 0))
    {
        if(_bDemodType)  //S2
        {
            for (i=0;i<250;i++)
            {
            //INTERN_DVBS_GetSNR(&f_snr);
                status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_DBG_7, &u8Data);
            u16Data=u8Data;
                status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_DBG_6, &u8Data);
            u16Data = (u16Data<<8)|u8Data;
                f_snr=u16Data*100/256;
                //snr_poly = 0.005261367463671*pow(f_snr, 3)-0.116517828301214*pow(f_snr, 2)+0.744836970505452*pow(f_snr, 1)-0.86727609780167;
                snr_poly = 5261367463671*f_snr*f_snr*f_snr-116517828301214*f_snr*f_snr+744836970505452*f_snr-867276097801670;//gain 10^15

                Fixed_SNR = f_snr + snr_poly/1000000000000000;

        
                if (Fixed_SNR < 1700)
              Fixed_SNR = Fixed_SNR;
                else if ((Fixed_SNR < 2000) && (Fixed_SNR >= 1700))
                Fixed_SNR = Fixed_SNR ;
                else if ((Fixed_SNR < 2200) && (Fixed_SNR >= 2000))
                    Fixed_SNR = Fixed_SNR - 200;
                else if ((Fixed_SNR < 2700) && (Fixed_SNR >= 2200))
                    Fixed_SNR = Fixed_SNR - 300;
                else if ((Fixed_SNR < 2900) && (Fixed_SNR >= 2700))
                    Fixed_SNR = Fixed_SNR - 350;
                else if (Fixed_SNR >= 2900)
                    Fixed_SNR = Fixed_SNR - 300;

                if (Fixed_SNR>1000)
                    Fixed_SNR=Fixed_SNR+100;
            

                if (Fixed_SNR < 100)
                    Fixed_SNR = 100;
                if (Fixed_SNR > 3000)
                    Fixed_SNR = 3000;

            Fixed_SNR_temp=Fixed_SNR_temp+Fixed_SNR;
            }

            Fixed_SNR=Fixed_SNR_temp/250;
 
            //BCH EFLAG2_Window,  window size 0x2000
            //BCH EFLAG2_Window,  window size 0x2000
            BCH_Eflag2_Window=0x2000;
            MHal_Demod_MB_WriteReg(DVBS2FEC_REG_BASE + 0x25*2 + 1, (BCH_Eflag2_Window>>8));
            MHal_Demod_MB_WriteReg(DVBS2FEC_REG_BASE + 0x25*2 , (BCH_Eflag2_Window&0xff));
            DVBS_GetPacketErr(&bchpkt_error);
            //eFlag_PER = (float)(bchpkt_error)/(float)(BCH_Eflag2_Window);
            eFlag_PER = (bchpkt_error*SHIFT_MULTIPLE)/BCH_Eflag2_Window;
            //fber = 0.089267531133002*pow(eFlag_PER, 2) + 0.019640560289510*eFlag_PER + 0.0000000001;
            dividend64 = 89267531133002*bchpkt_error*bchpkt_error;
            divisor64 = BCH_Eflag2_Window*BCH_Eflag2_Window;
            fber = div64_u64(dividend64,divisor64);
            
            dividend64 = 19640560289510*bchpkt_error;
            divisor64 = BCH_Eflag2_Window;
            fber = fber + div64_u64(dividend64,divisor64);

            fber = fber + 100000;


#if 0
#ifdef MSOS_TYPE_LINUX
            //log_ber = ( - 1) *log10f(1 / fber);
            if (fber > 1.0E-1)
                ber_sqi = (log10f(1.0f/fber))*20.0f + 8.0f;
            else if (fber > 1.0E-3)
                ber_sqi = (log10f(1.0f/fber))*20.0f + 30.0f;
            else if(fber > 8.5E-7)
                ber_sqi = (log10f(1.0f/fber))*20.0f - 30.0f;
            else
                ber_sqi = 100.0;
#else
             log_ber = ( - 1) *log10f(1 / fber);
             if (fber > 1.0E-3)
                ber_sqi =(log10f(1.0f/fber))*20.0f -35.0f;//0.0
            else if(fber > 8.5E-7)
                ber_sqi = (log10f(1.0f/fber))*20.0f - 40.0f;
            else
                ber_sqi = 100.0;

#endif
#else
            //log_ber = ( - 1) *log10f(1 / fber);
            fber=(U32)fber;
            if (fber > 100000000000000)//10^14
                ber_sqi = int_log(1/fber, 10)*20+8;//ber_sqi = (log10f(1.0f/fber))*20.0f + 8.0f;
            else if (fber > 1000000000000)//10^12
                ber_sqi = int_log(1/fber, 10)*20+30;//ber_sqi = (log10f(1.0f/fber))*20.0f + 30.0f;
            else if(fber > 850000000)
                ber_sqi = int_log(1/fber, 10)*20-30;//ber_sqi = (log10f(1.0f/fber))*20.0f - 30.0f;
            else
                ber_sqi = 100;
#endif
#if 0
           float fDVBS2_SQI_CNref[][11]=//gain 10^2
           {   //0,   1,    2,    3,    4,    5,    6,    7,    8,    9,    10
                //old 1/2, 1/3,  2/3,  1/4,  3/4,  2/5,  3/5,  4/5,  5/6,   8/9,  9/10
                //  1/4, 1/3,  2/5,  1/2,  3/5,  2/3,  3/4,  4/5,  5/6,   8/9,  9/10
                    {-1.6,-0.5,  3.1,  1.8,  3.0,  3.8,  4.8,  5.4,  5.9,   6.9,  7.2}, //QPSK
                    {8.7,  8.7,  8.7,  8.7,  8.7,  8.7,  8.7,  8.7, 9.6,  12.3, 12.3}, //8PSK{0.0,  0.0,  0.0,  0.0,  6.3,  7.4,  8.7,  0.0, 10.1,  10.7, 11.1}
           };
#else
           S32 fDVBS2_SQI_CNref[][11]=//gain 10^2
           {   //0,   1,    2,    3,    4,    5,    6,    7,    8,    9,    10
                {-160,-50,  310,  180,  300,  380,  480,  540,  590,   690,  720}, //QPSK
                {870,  870,  870,  870,  870,  870,  870,  870, 960,  1230, 1230}, //8PSK{0.0,  0.0,  0.0,  0.0,  6.3,  7.4,  8.7,  0.0, 10.1,  10.7, 11.1}
           };
#endif
           MHal_Demod_MB_ReadDspReg(E_DMD_S2_CODE_RATE, &u8Data);

           DBG_INTERN_DVBS(printk("INTERN_DVBS_GetTsDivNum DVBS2 E_DMD_S2_CODERATE=0x%x\n", u8Data));
          
           switch (u8Data)
           {
           case 0x03: //CR 1/2
           _u8_DVBS2_CurrentCodeRate_sqi = 3;
           break;
           case 0x01: //CR 1/3
           _u8_DVBS2_CurrentCodeRate_sqi = 1;
           break;
           case 0x05: //CR 2/3
           _u8_DVBS2_CurrentCodeRate_sqi = 5;
           break;
           case 0x00: //CR 1/4
           _u8_DVBS2_CurrentCodeRate_sqi = 0;
           break;
           case 0x06: //CR 3/4
           _u8_DVBS2_CurrentCodeRate_sqi = 6;
           break;
           case 0x02: //CR 2/5
           _u8_DVBS2_CurrentCodeRate_sqi = 2;
           break;
           case 0x04: //CR 3/5
           _u8_DVBS2_CurrentCodeRate_sqi = 4;
           break;
           case 0x07: //CR 4/5
           _u8_DVBS2_CurrentCodeRate_sqi = 7;
           break;
           case 0x08: //CR 5/6
           _u8_DVBS2_CurrentCodeRate_sqi = 8;
           break;
           case 0x09: //CR 8/9
           _u8_DVBS2_CurrentCodeRate_sqi = 9;
           break;
           case 0x0A: //CR 9/10
           _u8_DVBS2_CurrentCodeRate_sqi = 10;
           break;
           default:
           _u8_DVBS2_CurrentCodeRate_sqi = 10;
           break;
           }  

 
          MHal_Demod_MB_ReadDspReg(E_DMD_S2_MOD_TYPE, &u8Data);
          if(u8Data==2)
          {
                _u8QAM_val=0;
          }
          else if ((u8Data & 0x0F)==0x03)  //8PSK
          {
                _u8QAM_val=1;  
          }
          else
          {
                //Calculate SQI by 8PSK if system get an un-know modulation value.
                _u8QAM_val=1;
                printk("Get an un-know modulation value\n");
          }
          
          if ((_u8_DVBS2_CurrentCodeRate_sqi==10)&&(_u8QAM_val==1))
                Fixed_SNR=Fixed_SNR-0.25;
                
          f_snr = Fixed_SNR;
          cn_rel = f_snr - fDVBS2_SQI_CNref[_u8QAM_val][_u8_DVBS2_CurrentCodeRate_sqi];

          ber_sqi=100;
          if (cn_rel < -700)
          {
                *quality = 0;
           }
          else if (cn_rel < -30)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 909)/10 + 1));
                
          }
          else if (cn_rel < -20)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 893)/10 + 1));
                
          }
          else if (cn_rel < -10)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 849)/10 + 1));
                
          }
          else if (cn_rel < 0)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 853)/10 + 1));
                
          }
          else if (cn_rel < 15)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 810)/10 + 1));
                
          }
          else if (cn_rel < 24)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 780)/10 + 1));
                
          }
          else if (cn_rel < 32)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 751)/10 + 1));
                
          }
          else if (cn_rel < 38)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 703)/10 + 1));                
          }
          else if (cn_rel < 46)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 672)/10 + 1));
          }
          else if (cn_rel < 52)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 651)/10 + 1));
          }
          else if (cn_rel < 60)
          {
                 *quality= (U16)(ber_sqi*((cn_rel - 637)/10 + 1));
          }
          else if (cn_rel < 90)
          {
                *quality= (U16)(ber_sqi*((cn_rel - 613)/10 + 1));
          }
          else if (cn_rel < 100)
          {        
                    *quality= (U16)(ber_sqi*((cn_rel - 597)/10 + 1));
          }
          else if (cn_rel < 200)
           {
                if ((_u8_DVBS2_CurrentCodeRate_sqi==9)&&(_u8QAM_val==1))
                    *quality= (U16)(ber_sqi*((cn_rel - 640)/10 + 1));               
                else if ((_u8_DVBS2_CurrentCodeRate_sqi==10)&&(_u8QAM_val==1))
                    *quality= (U16)(ber_sqi*((cn_rel - 590)/10 + 1));
                else
                    *quality= (U16)(ber_sqi*((cn_rel - 560)/10 + 1));
          }
          else if (cn_rel < 600)
          {           
               if ((_u8_DVBS2_CurrentCodeRate_sqi==10)&&(_u8QAM_val==1))
                    *quality= (U16)(ber_sqi*((cn_rel - 586)/10 + 1));
               else     
                    *quality= (U16)(ber_sqi*((cn_rel - 660)/10 + 1));
          }
          else if (cn_rel < 672)
          {              
               if ((_u8_DVBS2_CurrentCodeRate_sqi==10)&&(_u8QAM_val==1))
                    *quality= (U16)(ber_sqi*((cn_rel - 580)/10 + 1));
               else
                    *quality= (U16)(ber_sqi*((cn_rel - 671)/10 + 1));
           }
           else
           {
                *quality = (U16)ber_sqi;
           }
/*
            printk(" eFlag_PER [%f]\n fber [%8.3e]\n ber_sqi [%f]\n",eFlag_PER,fber,ber_sqi);
            printk(" Fixed_SNR %f,_u8_DVBS2_CurrentCodeRate_sqi=%d,_u8QAM_val=%d\n",Fixed_SNR,_u8_DVBS2_CurrentCodeRate_sqi,_u8QAM_val);
            printk("cn_rel=%f\n",cn_rel);
*/            

            //*quality = Fixed_SNR/30*ber_sqi;
            DBG_INTERN_DVBS(printk(" Fixed_SNR %d\n",Fixed_SNR));
            DBG_INTERN_DVBS(printk(" BCH_Eflag2_Window %d\n",BCH_Eflag2_Window));
            DBG_INTERN_DVBS(printk(" eFlag_PER [%d]\n fber [%lld]\n ber_sqi [%lld]\n",eFlag_PER,fber,ber_sqi));
        }
        else //dvbs
        {
            if(DVBS_GetPostViterbiBer(&fber) == FALSE)//ViterbiBer
            {
                DBG_INTERN_DVBS(printk("\nGetPostViterbiBer Fail!"));
                return FALSE;
            }
            _fPostBer=fber;

            
            if (status==FALSE)
            {
                DBG_INTERN_DVBS(printk("MSB131X_DTV_GetSignalQuality GetPostViterbiBer Fail!\n"));
                return 0;
            }

            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_DBG_7, &u8Data);
            u16Data=u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_DBG_6, &u8Data);
            u16Data = (u16Data<<8)|u8Data;
            f_snr=u16Data*100/256.0;
            
            DBG_GET_SIGNAL_DVBS(printk("INTERN_DVBS GetSNR = %d \n ", (int)f_snr));
            snr_poly = 5261367463671*f_snr*f_snr*f_snr-116517828301214*f_snr*f_snr+744836970505452*f_snr-867276097801670;//gain 10^15
            Fixed_SNR = f_snr + snr_poly/1000000000000000;

            if (Fixed_SNR < 1700)
               Fixed_SNR = Fixed_SNR;
            else if ((Fixed_SNR < 2000) && (Fixed_SNR >= 1700))
               Fixed_SNR = Fixed_SNR - 80;
            else if ((Fixed_SNR < 2250) && (Fixed_SNR >= 2000))
               Fixed_SNR = Fixed_SNR - 200;
            else if ((Fixed_SNR < 2700) && (Fixed_SNR >= 2250))
               Fixed_SNR = Fixed_SNR - 300;
            else if ((Fixed_SNR < 2900) && (Fixed_SNR >= 2700))
               Fixed_SNR = Fixed_SNR - 350;
            else if (Fixed_SNR >= 2900)
               Fixed_SNR = Fixed_SNR - 300;


            if (Fixed_SNR < 100)
               Fixed_SNR = 100;
            if (Fixed_SNR > 3000)
               Fixed_SNR = 3000;
            
            U32 fDVBS_SQI_CNref[]=
            {   //0,    1,    2,    3,    4
                    //4.2,   5.9,  7.0,  7.9,  8.5,//QPSK
                    570,   740,  850,  940,  1000,//QPSK
            };

           MHal_Demod_MB_ReadDspReg(E_DMD_S2_CODE_RATE, &u8Data);
        
           switch (u8Data)
           {
            case 0x00: //CR 1/2
                _u8_DVBS2_CurrentCodeRate_sqi = 0;
            break;
            case 0x01: //CR 2/3
                _u8_DVBS2_CurrentCodeRate_sqi = 1;
            break;
            case 0x02: //CR 3/4
                _u8_DVBS2_CurrentCodeRate_sqi = 2;
            break;
            case 0x03: //CR 5/6
                _u8_DVBS2_CurrentCodeRate_sqi = 3;
            break;
            case 0x04: //CR 7/8
                _u8_DVBS2_CurrentCodeRate_sqi = 4;
            break;
            default:
                _u8_DVBS2_CurrentCodeRate_sqi = 4;
            break;
           }

#if 0
            if (fber > 1.0E-3)
                ber_sqi = (log10f(1.0f/fber))*20.0f -35.0f;//ber_sqi =0.0 ;
            else if(fber > 2.9E-6)
#ifdef MSOS_TYPE_LINUX
                ber_sqi = (log10f(1.0f/fber))*20.0f - 40.0f;
#else
                ber_sqi = (Log10Approx(1.0f/fber))*20.0f - 40.0f;
#endif
            else
                ber_sqi = 100.0;
#else
            if (fber > 1.0E7)//enlarge 10^10
                ber_sqi = int_log(1/fber, 10)*20 -35^10E10;//ber_sqi = (log10f(1.0f/fber))*20.0f -35.0f;//ber_sqi =0.0 ;
            else if(fber > 2.9E-6)
                ber_sqi = int_log(1/fber, 10)*20 -35^10E10;//ber_sqi = (log10f(1.0f/fber))*20.0f - 40.0f;
            else
                ber_sqi = 100;
#endif
            f_snr = Fixed_SNR;
            cn_rel = f_snr - fDVBS_SQI_CNref[_u8_DVBS2_CurrentCodeRate_sqi];

            printk("(JAMIE) fber = %lld\n",fber);
            printk("(JAMIE) f_snr = %d\n",f_snr);
            printk("(JAMIE) cn_nordig_s1 = %d\n",fDVBS_SQI_CNref[_u8_DVBS2_CurrentCodeRate_sqi]);
            printk("(JAMIE) cn_rel = %d\n",cn_rel);
            printk("(JAMIE) ber_sqi = %lld\n",ber_sqi);
#if 0
            {
                if (f_snr < 5.1f)
                {
                    *quality = 0;
                }
                else if (f_snr < 5.5f)
                {
                    *quality = (MS_U16)((f_snr*3.5)+2);//5.5
                }
                else if (f_snr < 6.0f)
                {
                    *quality = (MS_U16)((f_snr*5)+1);//6
                }
                else if (f_snr < 6.5f)
                {
                    *quality = (MS_U16)((f_snr*7)+0);//6.5
                }
                else if (f_snr < 7.0f)
                {
                    *quality = (MS_U16)((f_snr*8.5)+3);//7
                }
                else if (f_snr < 7.5f)
                {
                    *quality = (MS_U16)((f_snr*11)+0);//7.5
                }
                else
                {
                    *quality = 100;
                }
            }
#endif
            if (cn_rel < -700)
            {
                *quality = 0;
            }
            else if (cn_rel < 300)
            {
                 if (ber_sqi<70&&ber_sqi>60)
                    *quality = (U16)(ber_sqi*((cn_rel +20)/10 + 1));
                 else if (ber_sqi<=60&&ber_sqi>50)
                    *quality = (U16)(ber_sqi*((cn_rel +120)/10 + 1));
                 else if (ber_sqi<=50&&ber_sqi>40)
                    *quality = (U16)(ber_sqi*((cn_rel +240)/10 + 1));
                 else if (ber_sqi<40)
                    *quality = (U16)(ber_sqi*((cn_rel + 300)/10 + 1));
                 else
                 *quality = (U16)(ber_sqi*((cn_rel - 300)/10 + 1));
            }
            else
            {
                 *quality = (U16)ber_sqi;
            }

            //printk(" cn_rel %f,_u8_DVBS2_CurrentCodeRate_sqi=%d,ber_sqi=%f,Fixed_SNR %f\n",cn_rel,_u8_DVBS2_CurrentCodeRate_sqi,ber_sqi,Fixed_SNR);
            DBG_INTERN_DVBS(printk(">>>>>Signal Quility(SQI) = %d\n", *quality));
            return TRUE;
            
        }
    }
    else
    {
        *quality = 0;
    }
#endif
    return TRUE;
}

/****************************************************************************
Subject:    To get the DVT Signal quality
Function:   MSB1228_GetSignalQuality
Parmeter:   Quality
Return:     E_RESULT_SUCCESS
E_RESULT_FAILURE
Remark:     Implement SQI refering to Teracom min. spec 2.0 4.1.1.7
 *****************************************************************************/

BOOL DVBS_TR_indicator(void)
{
    U8 TR0 = 0;
    U8 TR1 = 0;
    U8 TR2 = 0;

    if(MHal_Demod_MB_ReadReg(0x3b00+(0x0e)*2+1, &TR0) == FALSE)
    {
        return FALSE;
    }
    if(MHal_Demod_MB_ReadReg(0x3D00+(0x19)*2+1, &TR1) == FALSE)
    {
        return FALSE;
    }
    if(MHal_Demod_MB_ReadReg(0x3D00+(0x29)*2+1, &TR2) == FALSE)
    {
        return FALSE;
    }

    if(((TR0|TR1|TR2)&0x01) == 0x01)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

BOOL DVBS_IF_AGC(U16 *ifagc)
{
    U8 tmp, status=TRUE;
    U16 if_agc_gain = 0;

    // select IF gain to read, reg_agc_debug_sel=3
    status &= MHal_Demod_MB_ReadReg(0x2800+0x11*2, &tmp);
    status &= MHal_Demod_MB_WriteReg(0x2800+0x11*2, (tmp&0xF0)|0x03);

    //freeze, reg_frontend_lat=1
    status &= MHal_Demod_MB_ReadReg(0x2800+0x02*2+1, &tmp);
    status &= MHal_Demod_MB_WriteReg(0x2800+0x02*2+1, tmp|0x80);
    
    //read value, reg_agc_debug_out_r[23:0]
    status &= MHal_Demod_MB_ReadReg(0x2800+0x12*2+1, &tmp);
    if_agc_gain = tmp;
    status &= MHal_Demod_MB_ReadReg(0x2800+0x12*2, &tmp);
    if_agc_gain = (if_agc_gain<<8)|tmp;

    //unfreeze, reg_frontend_lat=0
    status &= MHal_Demod_MB_ReadReg(0x2800+0x02*2+1, &tmp);
    status &= MHal_Demod_MB_WriteReg(0x2800+0x02*2+1, tmp&0x7F);

    *ifagc = if_agc_gain;
    
    return status;
}

#if 0
BOOL Packet_error_time_setting(U32 Packet_Error_Check_time,U32 *packet_error_val)
{
    BOOL    status= true;
    U8      tmp = 0;
    U16     u16_INTERN_Packer_err;

    status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_PACKET_ERROR_PER_Time, Packet_Error_Check_time*1000);
    printk("Packet_Error_time(ms) =%d\n",Packet_Error_Check_time*1000);
    
    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_PACKET_ERROR_PER_SEC_H, &tmp);
    u16_INTERN_Packer_err = tmp;

    status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_PACKET_ERROR_PER_SEC_L, &tmp);
    u16_INTERN_Packer_err = u16_INTERN_Packer_err<<8|tmp;

    printk("Packet_Error_number: 0x%x in time(ms)=%d\n",u16_INTERN_Packer_err,Packet_Error_Check_time*1000);
    *packet_error_val = u16_INTERN_Packer_err;

    return status;
}
#endif

BOOL DVBS_GetPacketErrAccu (U32 *pu32PktErr, BOOL bClear_Accu)
{
    BOOL         status = true;
    U8             u8Data = 0;
    U16           u16PktErr_temp = 0;
    static  U32       u32PktErr=0;
    
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &u8Data); 
    if (u8Data>=15)
    {
        MHal_Demod_MB_ReadDspReg(E_DMD_S2_SYSTEM_TYPE, &u8Data);
        if(!u8Data) //DVB-S2
        {
            status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x02*2, &u8Data); //freeze
            status &= MHal_Demod_MB_WriteReg(DVBS2FEC_REG_BASE+0x02*2, u8Data|0x01);

            status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x26*2+1, &u8Data);
            u16PktErr_temp = u8Data;
            status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x26*2, &u8Data);
            u16PktErr_temp = (u16PktErr_temp << 8)|u8Data;

            status &= MHal_Demod_MB_ReadReg(DVBS2FEC_REG_BASE+0x02*2, &u8Data); //freeze
            status &= MHal_Demod_MB_WriteReg(DVBS2FEC_REG_BASE+0x02*2, u8Data&(~0x01));
        }
        else
        { //DVB-S

            status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x33*2+1, &u8Data);
            u16PktErr_temp = u8Data;
            status &= MHal_Demod_MB_ReadReg(REG_BACKEND+0x33*2, &u8Data);
            u16PktErr_temp = (u16PktErr_temp << 8)|u8Data;

        }
        u32PktErr=u32PktErr+u16PktErr_temp;
        *pu32PktErr = u32PktErr;

        //printk("INTERN_DVBS PktErr = %d \n", (int)u16PktErr);//
    }
    if(bClear_Accu)
    {
          //clear the packet error accu number
          u32PktErr=0;
    }
    return true;
}

BOOL Ts_Clock_Setting_Get (U32 *TS_CLK_Val_ori,BOOL *ts_source)
{

    BOOL tssource=0;
    U8 u8Data =0;
    U32 u32Data = 0;
    INTERN_DVBS_GetTsDivNum(&u32Data,&tssource);  //ts_div_num
    u8Data = u32Data;
    if (u32Data > 0x1F) u8Data=0x1F;
    if (u32Data < 0x05) u8Data=0x05;
    *ts_source=tssource;
    if (tssource==1)
        *TS_CLK_Val_ori=(432000*1000)/(2*(u8Data+1));
    else
        *TS_CLK_Val_ori=(288000*1000)/(2*(u8Data+1));
    return true;
}

U32 Ts_Clock_Setting_Set (BOOL Debug_on, U32 TS_CLK_Val_new)
{
    U8 div432, div288, u8tmp;
    //float rate432, rate288, ftmp;
    MS_U64  dividend64=0;
    MS_U64  divisor64=0;
    MS_U64  ftmp=0;
    U32  rate432=0 , rate288=0;
    Debug_TS_CLK_on=Debug_on;
    if (Debug_TS_CLK_on==1)
    {
        //ftmp = 864000000.0/2/(2*TS_CLK_Val_new)-1;
        dividend64 = 864000000;
        divisor64 = 2*2*TS_CLK_Val_new;
        ftmp = div64_u64(dividend64, divisor64)-1;
            
        if ((MS_S64)ftmp < 0)
            ftmp = 0;
        //div432 = (U8)floor(ftmp);
        div432 = (U8)ftmp;
        if (div432 > 0x1f)
            div432 = 0x1f;
        
        //ftmp = 864000000.0/3/(2*TS_CLK_Val_new)-1;
        dividend64 = 864000000;
        divisor64 = 3*2*TS_CLK_Val_new;
        ftmp = div64_u64(dividend64, divisor64)-1;
        
        if ((MS_S64)ftmp < 0)
            ftmp = 0;
        //div288 = (U8)floor(ftmp);
        div288 = (U8)ftmp;
        if (div288 > 0x1f)
            div288 = 0x1f;

        rate432 = 864000000 / 2 / 2 / (div432 + 1);
        
        rate288 = 864000000 / 3 / 2 / (div288 + 1);

        if ((rate432 - TS_CLK_Val_new) < (rate288 - TS_CLK_Val_new))
        {
            MHal_Demod_ReadReg(0x103301,&u8tmp);
            u8tmp&=0xF8;
            u8tmp|=0x02;//select ts clk source 432MHz
            MHal_Demod_WriteReg(0x103301, u8tmp);//clk 432M
            
            MHal_Demod_ReadReg(0x103300, &u8tmp);
            u8tmp &= 0xe0;
            MHal_Demod_WriteReg(0x103300, (u8tmp | div432));
            return rate432;
        }

        MHal_Demod_ReadReg(0x103301,&u8tmp);
        u8tmp&=0xF8;
        u8tmp|=0x01;//select ts clk source 288MHz
        MHal_Demod_WriteReg(0x103301, u8tmp);
        MHal_Demod_ReadReg(0x103300, &u8tmp);
        u8tmp &= 0xe0;
        MHal_Demod_WriteReg(0x103300, (u8tmp | div288));
        return rate288;
    }
    return true;    
}
      
BOOL Power_Saving (void)
{
    U8         u8Data =0;

    DBG_INTERN_DVBS(printk("DVBS/S2 Power_Saving\n"));
    //REG_BASE[0x3509]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x9, &u8Data);
    
    MHal_Demod_MB_WriteReg(0x3500+0x9, u8Data|0x11);
    //REG_BASE[0x350a]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xa, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xa, u8Data|0x11);
    //REG_BASE[0x350c]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xc, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xc, u8Data|0x11);
    //REG_BASE[0x350e]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xe, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe, u8Data|0x11);
    //REG_BASE[0x3512]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x12, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x12, u8Data|0x11);
    //REG_BASE[0x3519]  = 0x11;
    //REG_BASE[0x3518]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x19, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x19, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x18, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x18, u8Data|0x11); 
    //REG_BASE[0x351b]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x1b, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x1b, u8Data|0x11);
    //REG_BASE[0x351d]  = 0x11;
    //REG_BASE[0x351c]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x1d, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x1d, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x1c, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x1c, u8Data|0x11);
    //REG_BASE[0x351e]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x1e, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x1e, u8Data|0x11);
    //REG_BASE[0x3521]  = 0x11;
    //REG_BASE[0x3520]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x21, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x21, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x20, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x20, u8Data|0x11);
    //REG_BASE[0x3523]  = 0x11;
    //REG_BASE[0x3522]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x23, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x23, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x22, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x22, u8Data|0x11);
    //REG_BASE[0x3525]  = 0x11;
    //REG_BASE[0x3524]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x25, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x25, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x24, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x24, u8Data|0x11);
    //REG_BASE[0x3529]  = 0x11;
    //REG_BASE[0x3528]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x29, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x29, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x28, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x28, u8Data|0x11);
    //REG_BASE[0x352b]  = 0x11;
    //REG_BASE[0x352a]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x2b, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x2b, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x2a, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x2a, u8Data|0x11);
    //REG_BASE[0x352c]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x2c, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x2c, u8Data|0x11);
    //REG_BASE[0x352f]  = 0x11;
    //REG_BASE[0x352e]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x2f, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x2f, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x2e, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x2e, u8Data|0x11);
    //REG_BASE[0x3531]  = 0x11;
    //REG/_BASE[0x3530]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x31, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x31, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x30, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x30, u8Data|0x11);
    //REG_BASE[0x3532]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x32, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x32, u8Data|0x11);
    //REG_BASE[0x3535]  = 0x11;
    //REG_BASE[0x3534]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x35, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x35, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x34, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x34, u8Data|0x11);
    //REG_BASE[0x353b]  = 0x11;
    //REG_BASE[0x353a]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x3b, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x3b, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x3a, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x3a, u8Data|0x11);
    //REG_BASE[0x353d]  = 0x11;
    //REG_BASE[0x353c]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x3d, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x3d, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x3c, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x3c, u8Data|0x11);
    //REG_BASE[0x3543]  = 0x11;
    //REG_BASE[0x3542]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x43, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x43, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x42, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x42, u8Data|0x11);
    //REG_BASE[0x3545]  = 0x11;
    //REG_BASE[0x3544]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x45, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x45, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x44, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x44, u8Data|0x11);
    //REG_BASE[0x3546]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x46, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x46, u8Data|0x11);
    //REG_BASE[0x3563]  = 0x11;
    //REG_BASE[0x3562]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x63, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x63, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x62, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x62, u8Data|0x11);
    //REG_BASE[0x3565]  = 0x11;
    //REG_BASE[0x3564]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x65, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x65, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x64, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x64, u8Data|0x11);
    //REG_BASE[0x3569]  = 0x11;
    //REG_BASE[0x3568]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x69, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x69, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x68, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x68, u8Data|0x11);
    //REG_BASE[0x356b]  = 0x11;
    //REG_BASE[0x356a]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x6b, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x6b, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x6a, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x6a, u8Data|0x11);
    //REG_BASE[0x356d]  = 0x11;
    //REG_BASE[0x356c]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x6d, &u8Data);
       
    MHal_Demod_MB_WriteReg(0x3500+0x6d, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x6c, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x6c, u8Data|0x11);
    //REG_BASE[0x356e]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x6e, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x6e, u8Data|0x11);
    //REG_BASE[0x3571]  = 0x11;
    //REG_BASE[0x3570]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x71, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x71, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x70, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x70, u8Data|0x11);    
    //REG_BASE[0x3572]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x72, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x72, u8Data|0x11);
    //REG_BASE[0x3575]  = 0x11;
    //REG_BASE[0x3574]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x75, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x75, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x74, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x74, u8Data|0x11); 
    //REG_BASE[0x3577]  = 0x11;
    //REG_BASE[0x3576]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x77, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x77, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x76, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x76, u8Data|0x11); 
    //REG_BASE[0x3579]  = 0x11;
    //REG_BASE[0x3578]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x79, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x79, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x78, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x78, u8Data|0x11); 
    //REG_BASE[0x357b]  = 0x11;
    //REG_BASE[0x357a]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x7b, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x7b, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x7a, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x7a, u8Data|0x11); 
    //REG_BASE[0x357d]  = 0x11;
    //REG_BASE[0x357c]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x7d, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x7d, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x7c, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x7c, u8Data|0x11); 
    //REG_BASE[0x357f]  = 0x11;
    //REG_BASE[0x357e]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0x7f, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x7f, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0x7e, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0x7e, u8Data|0x11); 
    //REG_BASE[0x35e1]  = 0x11;
    //REG_BASE[0x35e0]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xe1, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe1, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0xe0, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe0, u8Data|0x11);
    //REG_BASE[0x35e3]  = 0x11;
    //REG_BASE[0x35e2]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xe3, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe3, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0xe2, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe2, u8Data|0x11);
    //REG_BASE[0x35e5]  = 0x11;
    //REG_BASE[0x35e4]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xe5, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe5, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0xe4, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe4, u8Data|0x11);
    //REG_BASE[0x35e6]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xe6, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe6, u8Data|0x11);
    //REG_BASE[0x35e9]  = 0x11;
    //REG_BASE[0x35e8]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xe9, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe9, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0xe8, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xe8, u8Data|0x11);
    //REG_BASE[0x35eb]  = 0x11;
    //REG_BASE[0x35ea]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xeb, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xeb, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0x3500+0xea, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xea, u8Data|0x11);
    //REG_BASE[0x35ec]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xec, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xec, u8Data|0x11);
    //REG_BASE[0x35ee]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xee, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xee, u8Data|0x11);
    //REG_BASE[0x35f0]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3500+0xf0, &u8Data);
    MHal_Demod_MB_WriteReg(0x3500+0xf0, u8Data|0x11);
    
    //-------P2    
    //P2=1;
    //REG_BASE[0x2203]  = 0x11;
    //REG_BASE[0x2202]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x03, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x03, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x02, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x02, u8Data|0x11);
    //REG_BASE[0x2205]  = 0x11;
    //REG_BASE[0x2204]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x05, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x05, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x04, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x04, u8Data|0x11);
    //REG_BASE[0x2207]  = 0x11;
    //REG_BASE[0x2206]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x07, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x07, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x06, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x06, u8Data|0x11);
    //REG_BASE[0x2223]  = 0x11;
    //REG_BASE[0x2222]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x23, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x23, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x22, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x22, u8Data|0x11);
    //REG_BASE[0x2251]  = 0x11;
    //REG_BASE[0x2250]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x51, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x51, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x50, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x50, u8Data|0x11);
    //REG_BASE[0x2261]  = 0x11;
    //REG_BASE[0x2260]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x61, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x61, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x60, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x60, u8Data|0x11);
    //REG_BASE[0x2263]  = 0x11;
    //REG_BASE[0x2262]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x63, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x63, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x62, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x62, u8Data|0x11);
    //REG_BASE[0x2265]  = 0x11;
    //REG_BASE[0x2264]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x65, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x65, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x64, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x64, u8Data|0x11);
    //REG_BASE[0x2268]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x68, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x68, u8Data|0x11);
    //REG_BASE[0x226b]  = 0x11;
    //REG_BASE[0x226a]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x6b, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x6b, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x6a, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x6a, u8Data|0x11);
    //REG_BASE[0x226c]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x6c, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x6c, u8Data|0x11);
    //REG_BASE[0x2271]  = 0x11;
    //REG_BASE[0x2270]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x71, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x71, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x70, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x70, u8Data|0x11);
    //REG_BASE[0x2272]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x72, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x72, u8Data|0x11);
    ///REG_BASE[0x2274]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x74, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x74, u8Data|0x11);
    //REG_BASE[0x2277]  = 0x11;
    //REG_BASE[0x2276]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x77, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x77, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x76, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x76, u8Data|0x11);
    //REG_BASE[0x2279]  = 0x11;
    //REG_BASE[0x2278]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x79, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x79, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x78, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x78, u8Data|0x11);
    //REG_BASE[0x227a]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x7a, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x7a, u8Data|0x11);
    //REG_BASE[0x2281]  = 0x11;
    //REG_BASE[0x2280]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x81, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x81, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x80, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x80, u8Data|0x11);
    //REG_BASE[0x2283]  = 0x11;
    //REG_BASE[0x2282]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x83, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x83, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x82, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x82, u8Data|0x11);
    //REG_BASE[0x2285]  = 0x11;
    //REG_BASE[0x2284]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x85, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x85, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x84, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x84, u8Data|0x11);
    //REG_BASE[0x2287]  = 0x11;
    //REG_BASE[0x2286]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x87, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x87, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x86, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x86, u8Data|0x11);
    //REG_BASE[0x2289]  = 0x11;
    //REG_BASE[0x2288]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x89, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x89, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x88, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x88, u8Data|0x11);
    //REG_BASE[0x228a]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x8a, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x8a, u8Data|0x11);
    //REG_BASE[0x228d]  = 0x11;
    //REG_BASE[0x228c]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x8d, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x8d, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x8c, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x8c, u8Data|0x11);
    //REG_BASE[0x228f]  = 0x11;
    //REG_BASE[0x228e]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x8f, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x8f, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x8e, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x8e, u8Data|0x11);
    //REG_BASE[0x2291]  = 0x11;
    //REG_BASE[0x2290]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x91, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x91, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0x90, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x90, u8Data|0x11);
    //REG_BASE[0x2292]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x92, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x92, u8Data|0x11);
    //REG_BASE[0x2294]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0x94, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0x94, u8Data|0x11);
    //---P2=0;
    //REG_BASE[0x3635]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3600+0x35, &u8Data);
    MHal_Demod_MB_WriteReg(0x3600+0x35, u8Data|0x11);
    //REG_BASE[0x3663]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3600+0x63, &u8Data);
    MHal_Demod_MB_WriteReg(0x3600+0x63, u8Data|0x11);
    //REG_BASE[0x3679]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3600+0x79, &u8Data);
    MHal_Demod_MB_WriteReg(0x3600+0x79, u8Data|0x11);
    //REG_BASE[0x36e2]  = 0x11;
    MHal_Demod_MB_ReadReg(0x3600+0xe2, &u8Data);
    MHal_Demod_MB_WriteReg(0x3600+0xe2, u8Data|0x11);
    //REG_BASE[0x36e5]  = 0x11;
    //REG_BASE[0x36e4]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0xe5, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe5, u8Data|0x11);
    MHal_Demod_MB_ReadReg(0xA200+0xe4, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe4, u8Data|0x11);
    //REG_BASE[0x36e6]  = 0x11;
    MHal_Demod_MB_ReadReg(0xA200+0xe6, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe6, u8Data|0x11);
    //            REG_BASE[DMDTOP_FORCE_ALLSRAM_ON]=0xFC;
    //           REG_BASE[DMDTOP_DVBS2_SRAM_SD_EN]=0xFF;   
    MHal_Demod_MB_ReadReg(0x2000+0x48, &u8Data);
    MHal_Demod_MB_WriteReg(0x2000+0x48, u8Data|0xFC);
    //DVBS_ReadReg(0x2000+0x49, &u8Data);
    MHal_Demod_MB_WriteReg(0x2000+0x49, 0xFF);
    //--P2=1;
    //REG_BASE[0x21e1]  = 0x00;
    //REG_BASE[0x21e0]  = 0x00;
    MHal_Demod_MB_ReadReg(0xA200+0xe1, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe1, u8Data|0x00);
    MHal_Demod_MB_ReadReg(0xA200+0xe0, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe0, u8Data|0x00);
    //REG_BASE[0x21e3]  = 0x00;
    //REG_BASE[0x21e2]  = 0x00;
    MHal_Demod_MB_ReadReg(0xA200+0xe3, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe3, u8Data|0x00);
    MHal_Demod_MB_ReadReg(0xA200+0xe2, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe2, u8Data|0x00);
    //REG_BASE[0x21e5]  = 0x00;
    //REG_BASE[0x21e4]  = 0x00;
    MHal_Demod_MB_ReadReg(0xA200+0xe5, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe5, u8Data|0x00);
    MHal_Demod_MB_ReadReg(0xA200+0xe4, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe4, u8Data|0x00);
    //REG_BASE[0x21e7]  = 0x00;
    //REG_BASE[0x21e6]  = 0x00;
    MHal_Demod_MB_ReadReg(0xA200+0xe7, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe7, u8Data|0x00);
    MHal_Demod_MB_ReadReg(0xA200+0xe6, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe6, u8Data|0x00);
    //REG_BASE[0x21e9]  = 0x00;
    //REG_BASE[0x21e8]  = 0x00;
    MHal_Demod_MB_ReadReg(0xA200+0xe9, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe9, u8Data|0x00);
    MHal_Demod_MB_ReadReg(0xA200+0xe8, &u8Data);
    MHal_Demod_MB_WriteReg(0xA200+0xe8, u8Data|0x00);
    //----P2=0;
    //REG_BASE[0x2e60]  = 0xfe;   
    MHal_Demod_MB_ReadReg(0x2e00+0x60, &u8Data);
    MHal_Demod_MB_WriteReg(0x2e00+0x60, u8Data|0xfe);
    //REG_BASE[0x2e6b]  = 0x1e;
    //REG_BASE[0x2e6a]  = 0x83;
    MHal_Demod_MB_ReadReg(0x2e00+0x6b, &u8Data);
    MHal_Demod_MB_WriteReg(0x2e00+0x6b, u8Data|0x1e);
    MHal_Demod_MB_ReadReg(0x2e00+0x6a, &u8Data);
    MHal_Demod_MB_WriteReg(0x2e00+0x6a, u8Data|0x83);
    //REG_BASE[0x2003] = 0x01; 
    MHal_Demod_MB_ReadReg(0x2000+0x06, &u8Data);
    MHal_Demod_WriteReg(0x2000+0x06, u8Data&0xEF);
    
    return TRUE;
}

S16 CFO_Show (BOOL Debug_on)
{
   U8 u8Data =0;
   U16 CFO_val_temp=0;
   S16 CFO_val=0;    

   Debug_CFO_on=Debug_on;
   MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_INFO_04, &u8Data);
   CFO_val_temp = u8Data;
   MHal_Demod_MB_ReadDspReg(E_DMD_S2_MB_DMDTOP_INFO_03, &u8Data);
   CFO_val_temp = (CFO_val_temp<<8)|u8Data;
   if (CFO_val_temp&0x8000)
       CFO_val =CFO_val_temp-0x10000;
   else
       CFO_val=CFO_val_temp;
   if (Debug_CFO_on==1)
   {
        printk("DVBS CFO value = %dKHz\n",CFO_val);
        Debug_CFO_on=0;
   }
   else
   {
        DBG_INTERN_DVBS(printk("DVBS CFO value = %dKHz\n",CFO_val)); 
   }
   return CFO_val;
}

BOOL DVBS2_VCM_CHECK(void)
{
    U8 VCM_Check = 0;
    BOOL   status = TRUE;

    status &= MHal_Demod_MB_ReadReg(0x3E00+0x43*2, &VCM_Check);       
    
    if( (VCM_Check & 0x04) == 0x00) // VCM signal
        status = TRUE;
    else // CCM signal
        status = FALSE;
    
    return status;
}

BOOL DVBS2_Get_IS_ID_INFO(U8 *u8IS_ID, U8 *u8IS_ID_table)
{
    BOOL   status = TRUE;
    U8 iter;

    // get IS-ID
    //status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_IS_ID, u8IS_ID);
    status &= MHal_Demod_MB_ReadDspReg(0x2000+0x71*2, u8IS_ID);
    // get IS-ID table
    for(iter = 0; iter <= 0x0F;++iter)
    {
        status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_IS_ID_TABLE + 2*iter, &u8IS_ID_table[2*iter]);   
        status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_IS_ID_TABLE + 2*iter + 1, &u8IS_ID_table[2*iter + 1]);   
    }
    
    return status;    
}

BOOL DVBS2_Set_IS_ID(U8 u8IS_ID)
{
    U8 VCM_OPT = 0;
    U32 current_time = 0;
    BOOL   status = TRUE;

    // assign IS-ID
    //status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_IS_ID, u8IS_ID);
    status &= MHal_Demod_MB_WriteDspReg(0x2000+0x71*2, u8IS_ID);
    // wait for VCM_OPT == 1 or time out
    current_time = (U32)jiffies_to_msecs(jiffies);
    do
    {
        status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_VCM_OPT, &VCM_OPT);    
    }
    while(VCM_OPT != 0x01 && ((U32)jiffies_to_msecs(jiffies) - current_time) < 100);
    
    return status;
}


BOOL DVBS2_Set_Default_IS_ID(U8 *u8IS_ID, U8 *u8IS_ID_table)
{
    U8 VCM_OPT = 0;
    U32 current_time = 0;
    BOOL status = TRUE;

    U8 Default_IS_ID = 0;
    U8 iter;
    U8 temp, convert_counter;

    // Find the smallest IS_ID in the table

    for(iter = 0; iter < 0x0F;++iter)
    {
        // low byte
        temp = u8IS_ID_table[2*iter];
        
        if( temp != 0)
        {
            for(convert_counter = 0; convert_counter < 8;++convert_counter)
            {
                if( temp > ( (temp >> 1) * 2) ) 
                    break;                    
                else
                    temp = temp >> 1;
            }
            
            Default_IS_ID = iter*16 + convert_counter;
        }

        // high byte
        temp = u8IS_ID_table[2*iter + 1];
        
        if( temp != 0)
        {
            for(convert_counter = 0; convert_counter < 8;++convert_counter)
            {
                if( temp > ( (temp >> 1) * 2) ) 
                    break;                    
                else
                    temp = temp >> 1;
            }
            
            Default_IS_ID = iter*16 + 8 + convert_counter;
        }        
    }

    // assign IS-ID
    //status &= MHal_Demod_MB_WriteDspReg(E_DMD_S2_IS_ID, Default_IS_ID);
    status &= MHal_Demod_MB_WriteDspReg(0x2000+0x71*2, Default_IS_ID);

    *u8IS_ID = Default_IS_ID;

    // wait for VCM_OPT == 1 or time out
    current_time = (U32)jiffies_to_msecs(jiffies);
    do
    {
        status &= MHal_Demod_MB_ReadDspReg(E_DMD_S2_VCM_OPT, &VCM_OPT);    
    }
    while(VCM_OPT != 1 && ((U32)jiffies_to_msecs(jiffies) - current_time) < 100);
    
    return status;
}

U32 SAMPLING_RATE= 144000;

void MSD_ACI_coefficient_read(void)
{
    U8 reg = 0;
    U16 coef_0 = 0;
    U16 coef_1 = 0;
    U16 coef_2 = 0;
    U16 coef_3 = 0;

    MHal_Demod_MB_WriteReg(0x2800 + 0x40*2+1,0x00);
    MHal_Demod_MB_ReadReg(0x2800 + 0x42*2+1,&reg);
    coef_0 = reg;
    MHal_Demod_MB_ReadReg(0x2800 + 0x42*2,&reg);
    coef_0 = (coef_0<<8)|reg;

    MHal_Demod_MB_WriteReg(0x2800 + 0x40*2+1,0x01);
    MHal_Demod_MB_ReadReg(0x2800 + 0x42*2+1,&reg);
    coef_1 = reg;
    MHal_Demod_MB_ReadReg(0x2800 + 0x42*2,&reg);
    coef_1 = (coef_1<<8)|reg;

    MHal_Demod_MB_WriteReg(0x2800 + 0x40*2+1,0x02);
    MHal_Demod_MB_ReadReg(0x2800 + 0x42*2+1,&reg);
    coef_2 = reg;
    MHal_Demod_MB_ReadReg(0x2800 + 0x42*2,&reg);
    coef_2 = (coef_2<<8)|reg;

    MHal_Demod_MB_WriteReg(0x2800 + 0x40*2+1,0x03);
    MHal_Demod_MB_ReadReg(0x2800 + 0x42*2+1,&reg);
    coef_3 = reg;
    MHal_Demod_MB_ReadReg(0x2800 + 0x42*2,&reg);
    coef_3 = (coef_3<<8)|reg;

    printk("[msd]ACI_0 = 0x%x, ACI_1 = 0x%x, ACI_2 = 0x%x, ACI_3 = 0x%x\n",coef_0,coef_1,coef_2,coef_3);

    return;
}

void MSD_show_version(void)
{

    U8 reg = 0;
    U16 ver0 = 0;
    U8  ver1 = 0;
    U16 wh_monitor = 0;
    U16 DBG_90 = 0;
    //U32 demod_status_monitor = 0;

    MHal_Demod_MB_ReadDspReg(E_DMD_S2_FW_VERSION_H, &reg);
    ver0 = reg;
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_FW_VERSION_L, &reg);
    ver1 = reg;

    MHal_Demod_MB_ReadReg(0x2000 + (0x65<<1) + 1,&reg);
    wh_monitor = reg;
    MHal_Demod_MB_ReadReg(0x2000 + (0x65<<1) + 0,&reg);
    wh_monitor = (wh_monitor<<8)|reg;

#if 0
    ReadReg(0x1D00 + (0x7A<<1) + 3,&reg);
    demod_status_monitor = reg;
    ReadReg(0x1D00 + (0x7A<<1) + 2,&reg);
    demod_status_monitor = (demod_status_monitor<<8)|reg;
    ReadReg(0x1D00 + (0x7A<<1) + 1,&reg);
    demod_status_monitor = (demod_status_monitor<<8)|reg;
    ReadReg(0x1D00 + (0x7A<<1) + 0,&reg);
    demod_status_monitor = (demod_status_monitor<<8)|reg;
#endif

    MHal_Demod_MB_ReadReg(0x2000 + (0x60<<1) + 1,&reg);
    DBG_90 = reg;
    MHal_Demod_MB_ReadReg(0x2000 + (0x60<<1) + 0,&reg);
    DBG_90 = (DBG_90<<8)|reg;

    printk("[msd]ver0 = 0x%x, ver1 = 0x%x\n",ver0,ver1);

    printk("[msd]wh_monitor = 0x%x,\n",wh_monitor);
    //printk("[msd]demod_status_monitor = 0x%x,\n",demod_status_monitor);
    printk("[msd]DBG_90 = 0x%x,\n",DBG_90);

    return;
}

void MSD_outer_info(void)
{

    U8 reg = 0;
    U16 ldpc_err_win = 0;
    U8 ldpc_max_iter = 0;
    U16 ldpc_err_count = 0;
    U16 bch_eflag2_sum = 0;
    U16 ldpc_ber_count_plp_l = 0;
    U16 ldpc_ber_count_plp_h = 0;
    //float pre_ber = 0.0;
    static U16 acc_eflag2_num = 0;

    U8 cr = 0;
    U8 p_sync = 0;
    U16 pkt_err = 0;
    U16 bit_error_period = 0;
    U32 bit_error_num = 0;

    // 0: dvbs2, 1:dvbs
    U8 dvbs_sel = 2;

    U8 u8_llr0 = 0;
    U8 u8_fec_beta = 0;
    U8 u8_fec_alfa = 0;
    U8 u8_llr_clip = 0;
    U8 u8Data = 0;
    //U16 reg_0945h = 0;

    MHal_Demod_MB_ReadReg(0x3b00+0x40*2,&reg);
    dvbs_sel = reg&0x01;

#if 0
    ReadReg(0x0900+0x45*2+1,&reg);
    reg_0945h = reg;
    ReadReg(0x0900+0x45*2,&reg);
    reg_0945h = (reg_0945h<<8)|reg;
#endif

    if(dvbs_sel == 0)
    {
        MHal_Demod_MB_ReadReg(0x3300+0x12*2 + 1,&reg);
        ldpc_err_win = reg;
        MHal_Demod_MB_ReadReg(0x3300+0x12*2,&reg);
        ldpc_err_win = (ldpc_err_win<<8)|reg;

        MHal_Demod_MB_ReadReg(0x3300+0x18*2,&reg);
        ldpc_max_iter = reg;

        MHal_Demod_MB_ReadReg(0x3300+0x19*2 + 1,&reg);
        ldpc_err_count = reg;
        MHal_Demod_MB_ReadReg(0x3300+0x19*2,&reg);
        ldpc_err_count = (ldpc_err_count<<8)|reg;

        MHal_Demod_MB_ReadReg(0x3300+0x26*2 + 1,&reg);
        bch_eflag2_sum = reg;
        MHal_Demod_MB_ReadReg(0x3300+0x26*2,&reg);
        bch_eflag2_sum = (bch_eflag2_sum<<8)|reg;

        MHal_Demod_MB_ReadReg(0x3300+0x75*2,&reg);
        u8_llr0 = reg;
        MHal_Demod_MB_ReadReg(0x3300+0x10*2,&reg);
        u8_fec_beta = reg;
        MHal_Demod_MB_ReadReg(0x3300+0x16*2+1,&reg);
        u8_fec_alfa = reg;

        MHal_Demod_MB_ReadReg(0x3300+0x74*2,&reg);
        u8_llr_clip = reg;

        //ReadReg(0x2600+0x32*2 + 1,&reg);
        //ldpc_ber_count_plp_l = reg;
        //ReadReg(0x2600+0x32*2,&reg);
        //ldpc_ber_count_plp_l = (ldpc_ber_count_plp_l<<8)|reg;

        MHal_Demod_MB_ReadReg(0x3E00+(0x40)*2,&u8Data);//OPPRO
        printk ("[msd][outer][s2][0] 0x3E00+(0x40)*2 : 0x%x\n",u8Data);

        MHal_Demod_MB_ReadReg(0x3300+(0x03)*2,&u8Data);//DVBT2FEC
        printk ("[msd][outer][s2][0] 0x3300+(0x03)*2 : 0x%x\n",u8Data);

        MHal_Demod_MB_ReadReg(0x3400+(0x26)*2,&u8Data);//DVBSTM
        printk ("[msd][outer][s2][0] 0x3400+(0x26)*2 : 0x%x\n",u8Data);
        MHal_Demod_MB_ReadReg(0x3400+(0x26)*2+1,&u8Data);//DVBSTM
        printk ("[msd][outer][s2][0] 0x3400+(0x26)*2+1 : 0x%x\n",u8Data);

        MHal_Demod_MB_ReadReg(0x3E00+(0x4E)*2,&u8Data);//DVBS2OPPRO_OPPRO_FIQ_INT
        printk ("[msd][outer][s2][0] 0x3E00+(0x4E)*2 : 0x%x\n",u8Data);

        acc_eflag2_num += bch_eflag2_sum;

        printk("[msd][outer][s2][1]ldpc_err_win=%d, ldpc_max_iter=%d, ldpc_err_count=%d\n",ldpc_err_win,ldpc_max_iter,ldpc_err_count);
        printk("[msd][outer][s2][2]bch_eflag2_sum=%d, ldpc_ber_count_plp_l=0x%x, ldpc_ber_count_plp_h=0x%x\n",bch_eflag2_sum,ldpc_ber_count_plp_l,ldpc_ber_count_plp_h);
        printk("[msd][outer][s2][3]u8_llr0=0x%x, u8_fec_beta=0x%x, u8_fec_alfa=0x%x, u8_llr_clip=0x%x\n",u8_llr0,u8_fec_beta,u8_fec_alfa,u8_llr_clip);

        //printk("------------->[msd][3]ber=%f\n",pre_ber);
        printk("-------------| %d/%d |-----------\n",bch_eflag2_sum,acc_eflag2_num);


        if(bch_eflag2_sum != 0)
        {
            printk("0000000000\n");
            printk("0000011100\n");
            printk("------1000\n");
            printk("000000----\n");
            printk("------1111\n");
        }
    }
    else
    {
        MHal_Demod_MB_ReadReg(0x3F00+0x42*2,&reg);
        cr = reg&0x07;

        MHal_Demod_MB_ReadReg(0x3F00+0x16*2,&reg);
        p_sync = reg&0x1f;

        MHal_Demod_MB_ReadReg(0x3F00+0x1f*2 + 1,&reg);
        pkt_err = reg;
        MHal_Demod_MB_ReadReg(0x3F00+0x1f*2 + 0,&reg);
        pkt_err = (pkt_err<<8)|reg;

        MHal_Demod_MB_ReadReg(0x3F00+0x18*2 + 1,&reg);
        bit_error_period = reg;
        MHal_Demod_MB_ReadReg(0x3F00+0x18*2 + 0,&reg);
        bit_error_period = (bit_error_period<<8)|reg;

        MHal_Demod_MB_ReadReg(0x3F00+0x1d*2 + 3,&reg);
        bit_error_num = reg;
        MHal_Demod_MB_ReadReg(0x3F00+0x1d*2 + 2,&reg);
        bit_error_num = (bit_error_num<<8)|reg;
        MHal_Demod_MB_ReadReg(0x3F00+0x1d*2 + 1,&reg);
        bit_error_num = (bit_error_num<<8)|reg;
        MHal_Demod_MB_ReadReg(0x3F00+0x1d*2 + 0,&reg);
        bit_error_num = (bit_error_num<<8)|reg;

        printk("[msd][outer][s][1]cr=%d, p_sync=0x%x, pkt_err=%d\n",cr,p_sync,pkt_err);
        printk("[msd][outer][s][2]bit_error_period=%d, bit_error_num=%d, ber_e6=%d\n",bit_error_period,bit_error_num,(bit_error_num*1000000)/(bit_error_period*192512));//gain 10^6
    }

    return;
}

void MSD_inner_info(void)
{
    U8  reg = 0;
    U8  dvbs_sel = 0;
    U8  force_dd_or_costas = 0;
    U8  force_wfbl_or_not = 0;
    U8  force_pls_encode_done = 0;
    U8  force_mod_type = 0;
    U8  pr_lock_num = 0;
    U8  pr_lock_status = 0;
    U8  pr_kp = 0;
    U8  pr_ki = 0;
    U8  pr_kp_frac = 0;
    U8  tr_kp = 0;
    U8  tr_ki = 0;
    U8  fine_kp = 0;
    U8  fine_ki = 0;
    U32 dd_snr = 0;
    U32 da_snr = 0;
    U32 linear_snr = 0;
    U32 pr_error = 0;
    U32 pr_cfo = 0;
    U32 pr_cfo2 = 0;
    U32 finecfo_pe_ff = 0;
    U32 finecfo_ki = 0;
    U32 finecfo_ki_ff = 0;
    U8  cr = 0, rolloff = 0;
    U32 nda_snr_a=0, nda_snr_a2b=0;
    U16 fs_cfo=0;
    //double d_dd_snr= 0.0,d_da_snr=0.0, d_nda_snr=0.0, d_linear_snr=0.0;
    S32 d_dd_snr= 0,d_da_snr=0, d_nda_snr=0, d_linear_snr=0;
    //double f_tmp0=0.0,f_tmp1=0.0;
    S32  f_tmp0=0,f_tmp1=0;
    //double fs_cfo_khz = 0.0;
    S32 fs_cfo_khz = 0;
    U16 symbolrate = 0;
    U32 pr_kiff_forward = 0;
    U32 pr_kiff_backward = 0;
    //double pr_kiff_forward_khz = 0;
    //double pr_kiff_backward_khz = 0;
    S32 pr_kiff_forward_khz = 0;
    S32 pr_kiff_backward_khz = 0;
    U32 finefe_ki_ff = 0;
    //double finefe_ki_ff_khz = 0;
    S32 finefe_ki_ff_khz=0;
    //U8  dbg_sel_tmp = 0;
    U8 mf_sel = 0;
    U8 u8_eq_dd = 0;
    U8 u8_eq_da = 0;
    U8 u8_eq_ffe_da = 0;
    U8 u8_eq_ffe_dd = 0;


    MHal_Demod_MB_ReadReg(0x3b00+0x40*2,&reg);
    dvbs_sel = reg&0x01;
    force_dd_or_costas = (reg>>1)&0x01;
    force_wfbl_or_not = (reg>>2)&0x01;
    force_pls_encode_done = (reg>>3)&0x01;
    force_mod_type = (reg>>4)&0x03;

    MHal_Demod_MB_ReadReg(0x3b00+0x40*2 + 1,&reg);
    pr_lock_num = (reg>>0)&0x07;
    pr_lock_status = (reg>>4)&0x01;

    MHal_Demod_MB_ReadReg(0x3C00+0x6B*2,&reg);
    pr_kp = (reg>>0)&0x0f;
    pr_ki = (reg>>4)&0x0f;

    MHal_Demod_MB_ReadReg(0x3B00+0x0a*2 + 1,&reg);
    tr_ki = reg;
    MHal_Demod_MB_ReadReg(0x3B00+0x0a*2 + 0,&reg);
    tr_kp = reg;

    MHal_Demod_MB_ReadReg(0x3B00+0x41*2+1,&reg);
    pr_kp_frac = (reg>>0)&0x0f;


    MHal_Demod_MB_ReadReg(0x3C00+0x6b*2+1,&reg);
    fine_kp = (reg>>4)&0x0f;
    MHal_Demod_MB_ReadReg(0x3C00+0x6c*2,&reg);
    fine_ki = (reg>>0)&0x0f;


    MHal_Demod_MB_ReadReg(0x3C00+0x36*2 + 3,&reg);
    dd_snr = reg;
    MHal_Demod_MB_ReadReg(0x3C00+0x36*2 + 2,&reg);
    dd_snr = (dd_snr<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3C00+0x36*2 + 1,&reg);
    dd_snr = (dd_snr<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3C00+0x36*2 + 0,&reg);
    dd_snr = (dd_snr<<8)|reg;

    MHal_Demod_MB_ReadReg(0x3C00+0x34*2 + 3,&reg);
    da_snr = reg;
    MHal_Demod_MB_ReadReg(0x3C00+0x34*2 + 2,&reg);
    da_snr = (da_snr<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3C00+0x34*2 + 1,&reg);
    da_snr = (da_snr<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3C00+0x34*2 + 0,&reg);
    da_snr = (da_snr<<8)|reg;


    MHal_Demod_MB_ReadReg(0x3b00+0x5d*2 + 3,&reg);
    linear_snr = reg&0x03;
    MHal_Demod_MB_ReadReg(0x3b00+0x5d*2 + 2,&reg);
    linear_snr = (linear_snr<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3b00+0x5d*2 + 1,&reg);
    linear_snr = (linear_snr<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3b00+0x5d*2 + 0,&reg);
    linear_snr = (linear_snr<<8)|reg;

#if 0
    // PR error[19:0]
    DVBS_WriteReg(0x1b00+0x04*2 + 1,0x01);
    MHal_Demod_MB_ReadReg(0x1b00+0x43*2 + 2,&reg);
    pr_error = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x1b00+0x43*2 + 1,&reg);
    pr_error = (pr_error<<8)|reg;
    MHal_Demod_MB_ReadReg(0x1b00+0x43*2 + 0,&reg);
    pr_error = (pr_error<<8)|reg;
    //PR cfo1[12:0]
    DVBS_WriteReg(0x1b00+0x04*2 + 1,0x06);
    MHal_Demod_MB_ReadReg(0x1b00+0x43*2 + 1,&reg);
    pr_cfo = reg&0x1f;
    MHal_Demod_MB_ReadReg(0x1b00+0x43*2 + 0,&reg);
    pr_cfo = (pr_cfo<<8)|reg;
    //PR cfo2[12:0]
    DVBS_WriteReg(0x1b00+0x04*2 + 1,0x07);
    MHal_Demod_MB_ReadReg(0x1b00+0x43*2 + 1,&reg);
    pr_cfo2 = reg&0x1f;
    MHal_Demod_MB_ReadReg(0x1b00+0x43*2 + 0,&reg);
    pr_cfo2 = (pr_cfo2<<8)|reg;
    //finecfo pe_ff
    DVBS_WriteReg(0x1b00+0x04*2 + 1,0x01);
    MHal_Demod_MB_ReadReg(0x2100+0x28*2 + 2,&reg);
    finecfo_pe_ff = reg&0x1f;
    MHal_Demod_MB_ReadReg(0x2100+0x28*2 + 1,&reg);
    finecfo_pe_ff = (finecfo_pe_ff<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2100+0x28*2 + 0,&reg);
    finecfo_pe_ff = (finecfo_pe_ff<<8)|reg;
#endif
#if 0
    // finecfo ki
    DVBS_WriteReg(0x2400+0x04*2 + 1,0x04);
    MHal_Demod_MB_ReadReg(0x2500+0x28*2 + 2,&reg);
    finecfo_ki = reg;
    MHal_Demod_MB_ReadReg(0x2500+0x28*2 + 1,&reg);
    finecfo_ki = (finecfo_ki<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2500+0x28*2 + 0,&reg);
    finecfo_ki = (finecfo_ki<<8)|reg;
#endif
#if 0
    //finecfo_ki_ff [39:0], read[39:8] only
    MHal_Demod_MB_ReadReg(0x2100+0x2b*2 + 4,&reg);
    finecfo_ki_ff = reg;
    MHal_Demod_MB_ReadReg(0x2100+0x2b*2 + 3,&reg);
    finecfo_ki_ff = (finecfo_ki_ff<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2100+0x2b*2 + 2,&reg);
    finecfo_ki_ff = (finecfo_ki_ff<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2100+0x2b*2 + 1,&reg);
    finecfo_ki_ff = (finecfo_ki_ff<<8)|reg;

    //code rate
    DVBS_WriteReg(0x1b00+0x04*2 + 1,0x00);
    MHal_Demod_MB_ReadReg(0x1b00+0x6b*2 + 1,&reg);
    cr = reg&0x3c;
#endif
    //roll off
    MHal_Demod_MB_ReadReg(0x3b00+0x0f*2,&reg);
    rolloff = reg&0x3;

    //nda_snr_a[17:0]
    MHal_Demod_MB_ReadReg(0x3b00+0x46*2 + 2,&reg);
    nda_snr_a = reg&0x03;
    MHal_Demod_MB_ReadReg(0x3b00+0x46*2 + 1,&reg);
    nda_snr_a = (nda_snr_a<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3b00+0x46*2 + 0,&reg);
    nda_snr_a = (nda_snr_a<<8)|reg;

    //nda_snr_a2b[25:0]
    MHal_Demod_MB_ReadReg(0x3b00+0x48*2 + 3,&reg);
    nda_snr_a2b = reg&0x3f;
    MHal_Demod_MB_ReadReg(0x3b00+0x48*2 + 2,&reg);
    nda_snr_a2b = (nda_snr_a2b<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3b00+0x48*2 + 1,&reg);
    nda_snr_a2b = (nda_snr_a2b<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3b00+0x48*2 + 0,&reg);
    nda_snr_a2b = (nda_snr_a2b<<8)|reg;

    //pr_ki_ff_forward (25,27)
    MHal_Demod_MB_ReadReg(0x3b00+0x78*2 + 3,&reg);
    pr_kiff_forward = reg&0x01;
    MHal_Demod_MB_ReadReg(0x3b00+0x78*2 + 2,&reg);
    pr_kiff_forward = (pr_kiff_forward<<8)|reg;
    MHal_Demod_MB_ReadReg(0x300+0x78*2 + 1,&reg);
    pr_kiff_forward = (pr_kiff_forward<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3b00+0x78*2 + 0,&reg);
    pr_kiff_forward = (pr_kiff_forward<<8)|reg;

    //pr_ki_ff_backward (25,27)
    MHal_Demod_MB_ReadReg(0x3b00+0x7b*2 + 3,&reg);
    pr_kiff_backward = reg&0x01;
    MHal_Demod_MB_ReadReg(0x3b00+0x7b*2 + 2,&reg);
    pr_kiff_backward = (pr_kiff_backward<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3b00+0x7b*2 + 1,&reg);
    pr_kiff_backward = (pr_kiff_backward<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3b00+0x7b*2 + 0,&reg);
    pr_kiff_backward = (pr_kiff_backward<<8)|reg;

    //finefe_ki_ff (24,20)
    MHal_Demod_MB_ReadReg(0x3C00+0x2c*2 + 2,&reg);
    finefe_ki_ff = reg;
    MHal_Demod_MB_ReadReg(0x3C00+0x2c*2 + 1,&reg);
    finefe_ki_ff = (finefe_ki_ff<<8)|reg;
    MHal_Demod_MB_ReadReg(0x3C00+0x2c*2 + 0,&reg);
    finefe_ki_ff = (finefe_ki_ff<<8)|reg;

    //fs cfo one shot [13:0]
    MHal_Demod_MB_ReadReg(0x3b00+0x31*2 + 1,&reg);
    fs_cfo = reg&0x3f;
    MHal_Demod_MB_ReadReg(0x3b00+0x31*2 + 0,&reg);
    fs_cfo = (fs_cfo<<8)|reg;

    //symbolrate
    //ReadReg(0x0b00+0x29*2 + 1,&reg);
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_H,&reg);
    symbolrate = reg;
    //ReadReg(0x0b00+0x29*2 + 0,&reg);
    MHal_Demod_MB_ReadDspReg(E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_L,&reg);
    symbolrate = (symbolrate<<8)|reg;
  
    MHal_Demod_MB_ReadReg(0x3b00+0x7d*2 + 0,&reg);
    mf_sel = reg&0x03;

    MHal_Demod_MB_ReadReg(0x1b00+0x4b*2 + 0,&reg);
    u8_eq_da = (reg>>4)&0x0f;

    MHal_Demod_MB_ReadReg(0x3b00+0x4b*2 + 1,&reg);
    u8_eq_dd = (reg)&0x0f;

    MHal_Demod_MB_ReadReg(0x3D00+0x50*2 + 0,&reg);
    u8_eq_ffe_da = reg;

    MHal_Demod_MB_ReadReg(0x3D00+0x55*2 + 1,&reg);
    u8_eq_ffe_dd =reg;

    #if 0
    // fs_cfo
    if (fs_cfo & 0x2000)
        f_tmp0 = (double)fs_cfo-0x4000;
    else
        f_tmp0 = (double)fs_cfo;

    fs_cfo_khz = (double)symbolrate*f_tmp0/16384.0;

    //pr
    if (pr_kiff_forward & 0x1000000)
        f_tmp0 = (double)pr_kiff_forward-0x2000000;
    else
        f_tmp0 = (double)pr_kiff_forward;

    pr_kiff_forward_khz = (double)symbolrate*f_tmp0/134217728.0;

    if (pr_kiff_backward & 0x1000000)
        f_tmp0 = (double)pr_kiff_backward-0x2000000;
    else
        f_tmp0 = (double)pr_kiff_backward;

    pr_kiff_backward_khz = (double)symbolrate*f_tmp0/134217728.0;

 //finefe
    if(finefe_ki_ff & 0x800000)
        f_tmp0 = (double)finefe_ki_ff - 0x1000000;
    else
        f_tmp0 = (double)finefe_ki_ff;

    finefe_ki_ff_khz = (double)symbolrate*f_tmp0/1048576.0;


    // nda_snr
    if (nda_snr_a == 0 || nda_snr_a2b == 0)
    {
        d_nda_snr = -1.0;
    }
    else
    {
        f_tmp0 = (double)nda_snr_a/65536.0;
        f_tmp1 = (double)nda_snr_a2b/4194304.0;
        f_tmp1 = sqrt(f_tmp1);

        f_tmp0 = f_tmp0/f_tmp1;

        f_tmp0-=1.0;

        if ( f_tmp0 <= 0)
            d_nda_snr = -1;
        else
            d_nda_snr = 10.0*log10(1/f_tmp0);
    }

    //da_snr
    if (da_snr != 0)
    {
        // pow(2,31) = 2147483648
        f_tmp0 = da_snr/2147483648.0;
        d_da_snr = 10.0*log10(1.0/f_tmp0);
    }
    else
    {
        d_da_snr = -1;
    }

    //dd_snr
    if (dd_snr != 0)
    {
        f_tmp0 = dd_snr/2147483648.0;
        d_dd_snr = 10.0*log10(1.0/f_tmp0);
    }
    else
    {
        d_dd_snr = -1;
    }
    // linear_snr
    if (linear_snr != 0)
    {
        f_tmp0 = linear_snr/64.0;
        d_linear_snr = 10.0*log10(f_tmp0);
    }
    else
    {
        d_linear_snr = -1;
    }
    #else
    // fs_cfo
    if (fs_cfo & 0x2000)
        f_tmp0 = fs_cfo-0x4000;
    else
        f_tmp0 = fs_cfo;

    fs_cfo_khz = symbolrate*f_tmp0*SHIFT_MULTIPLE/16384;

    //pr
    if (pr_kiff_forward & 0x1000000)
        f_tmp0 = pr_kiff_forward-0x2000000;
    else
        f_tmp0 = pr_kiff_forward;

    pr_kiff_forward_khz = symbolrate*f_tmp0*SHIFT_MULTIPLE/134217728;

    if (pr_kiff_backward & 0x1000000)
        f_tmp0 = pr_kiff_backward-0x2000000;
    else
        f_tmp0 = pr_kiff_backward;

    pr_kiff_backward_khz = symbolrate*f_tmp0*SHIFT_MULTIPLE/134217728;

 //finefe
    if(finefe_ki_ff & 0x800000)
        f_tmp0 = finefe_ki_ff - 0x1000000;
    else
        f_tmp0 = finefe_ki_ff;

    finefe_ki_ff_khz = symbolrate*f_tmp0*SHIFT_MULTIPLE/1048576;

    #if 1
    // nda_snr
    if (nda_snr_a == 0 || nda_snr_a2b == 0)
    {
        d_nda_snr = -1;
    }
    else
    {
        f_tmp0 = nda_snr_a*SHIFT_MULTIPLE/65536;
        f_tmp1 = nda_snr_a2b*SHIFT_MULTIPLE/4194304;
        f_tmp1 = int_sqrt(f_tmp1);

        f_tmp0 = f_tmp0*SHIFT_MULTIPLE/f_tmp1;

        f_tmp0-=1;

        //if ( f_tmp0 <= 0)
        //    d_nda_snr = -1;
        //else
        //    d_nda_snr = 10*log10(1/f_tmp0);
        d_nda_snr = f_tmp0;
    }

    //da_snr
    if (da_snr != 0)
    {
        // pow(2,31) = 2147483648
        f_tmp0 = da_snr*SHIFT_MULTIPLE/2147483648UL;
        //d_da_snr = 10*log10(1/f_tmp0);
        d_da_snr = f_tmp0;
    }
    else
    {
        d_da_snr = -1;
    }

    //dd_snr
    if (dd_snr != 0)
    {
        f_tmp0 = dd_snr*SHIFT_MULTIPLE/2147483648UL;
        //d_dd_snr = 10*log10(1/f_tmp0);
        d_dd_snr = f_tmp0;
    }
    else
    {
        d_dd_snr = -1;
    }
    // linear_snr
    if (linear_snr != 0)
    {
        f_tmp0 = linear_snr*SHIFT_MULTIPLE/64;
        //d_linear_snr = 10*log10(f_tmp0);
        d_linear_snr = f_tmp0;
    }
    else
    {
        d_linear_snr = -1;
    }
    #endif
    #endif

    printk("[inner][1]dvbs_sel=%d, force_dd_or_costas=%d, force_wfbl_or_not=%d\n",dvbs_sel,force_dd_or_costas,force_wfbl_or_not);
    printk("[inner][2]force_pls_encode_done=%d, force_mod_type=%d, pr_lock_num=%d, pr_lock_status=%d\n",force_pls_encode_done,force_mod_type,pr_lock_num,pr_lock_status);
    printk("[inner][3]pr_kp=0x%x, pr_ki=0x%x, pr_kp_frac=0x%x, tr_kp=0x%x, tr_ki=0x%x\n",pr_kp,pr_ki,pr_kp_frac,tr_kp,tr_ki);
    printk("[inner][4]dd_snr=0x%x, da_snr=0x%x,linear_snr=0x%x\n",dd_snr,da_snr,linear_snr);
    printk("[inner][5]pr_error=0x%x,pr_cfo=0x%x,pr_cfo2=0x%x\n",pr_error,pr_cfo,pr_cfo2);
    printk("[inner][6]finecfo_pe_ff=0x%x,finecfo_ki=0x%x\n",finecfo_pe_ff,finecfo_ki);
    printk("[inner][7]fine_kp=0x%x,fine_ki=0x%x, finecfo_ki_ff=0x%x\n",fine_kp,fine_ki,finecfo_ki_ff);
    printk("[inner][8]cr=0x%x,rolloff=%d\n",cr,rolloff);
    printk("[inner][9]nda_snr_a=0x%x,nda_snr_a2b=0x%x,fs_cfo=0x%x\n",nda_snr_a,nda_snr_a2b,fs_cfo);
    //printk("[inner][10]d_nda_snr=%.2f,d_da_snr=%.2f,d_dd_snr=%.2f,d_linear_snr=%.2f\n",d_nda_snr,d_da_snr,d_dd_snr,d_linear_snr);
    //printk("[inner][11]fs_cfo=%.2f, pr_forward=%.2f, pr_backward=%.2f,finefe=%.2f [khz]\n",fs_cfo_khz,pr_kiff_forward_khz,pr_kiff_backward_khz,finefe_ki_ff_khz);
    printk("[inner][10]d_nda_snr=%d,d_da_snr=%d,d_dd_snr=%d,d_linear_snr=%d\n",d_nda_snr,d_da_snr,d_dd_snr,d_linear_snr);
    printk("[inner][11]fs_cfo*e3=%d, pr_forward*e3=%d, pr_backward*e3=%d,finefe*e3=%d [khz]\n",fs_cfo_khz,pr_kiff_forward_khz,pr_kiff_backward_khz,finefe_ki_ff_khz);
    printk("[inner]mf_sel=%d, eq_da=0x%x, eq_dd=0x%x, eq_ffe_da=0x%x, eq_ffe_dd=0x%x\n",mf_sel,u8_eq_da,u8_eq_dd,u8_eq_ffe_da,u8_eq_ffe_dd);
    return;
}

void MSD_sw_indicator(void)
{
    U8 reg,reg1;
    U8 zif,bw,low_pwr_det,aci_det,cci_det;
    U8 aci_det_fd,fdsa_align,large_the_flag,fd_function;

    MHal_Demod_MB_ReadReg(0x0900+0x4a*2,&reg);
    MHal_Demod_MB_ReadReg(0x0900+0x4a*2 + 1,&reg1);

    zif = reg&0x01;
    bw = (reg>>1)&0x07;
    low_pwr_det = (reg>>4)&0x01;
    aci_det = (reg>>5)&0x01;
    cci_det = (reg>>6)&0x03;
    cci_det = cci_det|((reg1&0x01)<<2);

    aci_det_fd = (reg1>>1)&0x03;
    fdsa_align = (reg1>>3)&0x01;
    large_the_flag = (reg1>>4)&0x01;
    fd_function = (reg1>>5)&0x03;

    printk("[msb1240][sw][1]zif=%d, bw=%d, low_pwr_det=%d\n",zif,bw,low_pwr_det);
    printk("[msb1240][sw][2]aci_det=%d, cci_det=%d, aci_det_fd=%d\n",aci_det,cci_det,aci_det_fd);
    printk("[msb1240][sw][3]fdsa_align=%d, large_the_flag=%d, fd_function=%d\n",fdsa_align,large_the_flag,fd_function);
    return;
}


void MSD_top_indicator(void)
{
    U8 reg;
    U16 ts_pad;

    MHal_Demod_MB_ReadReg(0x0900+0x2d*2 + 1,&reg);
    ts_pad = reg;
    MHal_Demod_MB_ReadReg(0x0900+0x2d*2,&reg);
    ts_pad = (ts_pad<<8)|reg;

    printk("[top]ts_pad=0x%x\n",ts_pad);

    return;

}

// 0: ADC output
// 1: DAGC0 output
// 2: DAGC1 output
// 3: Mixer output
// 4: DAGC2 output
// 5: Interpolator output
//To be implement
void MSD_fft_capture(U8 u8_port, U16 u16_s1, U16 u16_s2, U8 smooth_en)
{
    U8 reg;
    U8 u8_timeout = 0;
    U16 indx = 0;
    U32 u32_val = 0;
    U32 t1 = (U32)jiffies_to_msecs(jiffies);
    U8 u8_smooth_factor = 0;

    if (u16_s1 > 4095) u16_s1 = 4095;
    if (u16_s2 > 4095) u16_s2 = 4095;
    if (u16_s1 > u16_s2) u16_s1 = u16_s2;

    // mcu rst
    MHal_Demod_MB_WriteReg(0x0b00+0x19*2,0x01);

    MHal_Demod_MB_WriteReg(0x0900+0x15*2,0x04);
    MHal_Demod_MB_WriteReg(0x2600+0x9*2,0x00);
    MHal_Demod_MB_WriteReg(0x2600+0x1*2,0x3d);
    MHal_Demod_MB_WriteReg(0x1300+0x20*2,0x01);

    MHal_Demod_MB_WriteReg(0x1300+0x28*2 + 1,u8_port);
    MHal_Demod_MB_WriteReg(0x3e00+0x00*2 + 1,0xa0);
    udelay(100);
    MHal_Demod_MB_WriteReg(0x3e00+0x00*2 + 1,0x80);

    u8_timeout = 0;
    MHal_Demod_MB_ReadReg(0x1300+0x30*2,&reg);
    while((reg&0x01) != 0x01)
    {
        MHal_Demod_MB_ReadReg(0x1300+0x30*2,&reg);
        u8_timeout++;
        udelay(10);
        if (u8_timeout > 200)
        {
            printk("fft capture error.....timeout=%d\n",u8_timeout);
            return;
        }
    }
    // sram freeze
    MHal_Demod_MB_WriteReg(0x1300+0x2a*2,0x01);

    MHal_Demod_MB_ReadReg(0x1300+0x25*2+1,&reg);
    u8_smooth_factor = reg&0x7f;

    for (indx=u16_s1;indx<=u16_s2;indx++)
    {
        MHal_Demod_MB_WriteReg(0x1300+0x29*2,(U8)indx);
        MHal_Demod_MB_WriteReg(0x1300+0x29*2 + 1,(U8)(indx>>8));
        udelay(1);
        if (smooth_en == 0)
        {
            MHal_Demod_MB_ReadReg(0x1300+0x31*2+2,&reg);
            u32_val = reg&0x03;
            MHal_Demod_MB_ReadReg(0x1300+0x31*2+1,&reg);
            u32_val = (u32_val<<8)|reg;
            MHal_Demod_MB_ReadReg(0x1300+0x31*2,&reg);
            u32_val = (u32_val<<8)|reg;
            printk("[fft][%d][%d][%4d]%d\n",u8_port,smooth_en,indx,u32_val*SHIFT_MULTIPLE/1024);
        }
        else
        {
            MHal_Demod_MB_ReadReg(0x1300+0x35*2+2,&reg);
            u32_val = reg&0x7f;
            MHal_Demod_MB_ReadReg(0x1300+0x35*2+1,&reg);
            u32_val = (u32_val<<8)|reg;
            MHal_Demod_MB_ReadReg(0x1300+0x35*2,&reg);
            u32_val = (u32_val<<8)|reg;
            printk("[fft][%d][%d][%4d]%d\n",u8_port,smooth_en,indx, (u32_val*SHIFT_MULTIPLE)/(256*u8_smooth_factor));
        }
    }

    printk("[fft]total time=%d\n",(U32)jiffies_to_msecs(jiffies)-t1);
    return;
}

void MSD_front_info(void)
{
    U8 reg;
    U8 agc_k, agc_lock;
    U16 agc_ref;
    U8 d0_k, d0_ref, d0_lock;
    U8 d1_k, d1_ref, d1_lock;
    U8 d2_k, d2_ref, d2_lock;
    U8 d3_k, d3_ref, d3_lock;
    U16 agc_gain, d0_gain,d1_gain,d2_gain,d3_gain,agc_err;
    U8 iis_reg;
    U32 fb_fs, fc_fs, nco_ff, carr_phase,rate1;

    U16 srd_cfo=0;
    U8  srd_frc_cfo = 0;
    U16  srd_hw_cfo = 0;
    U8  srd_dec_val = 0;
    U32 mix_cfo_combine = 0;

    U16 srd1_cfo=0;
    U8  srd1_frc_cfo = 0;
    U16  srd1_hw_cfo = 0;
    U8  srd1_dec_val = 0;

    U16 srd2_cfo=0;
    U8  srd2_frc_cfo = 0;
    U16  srd2_hw_cfo = 0;
    U8  srd2_dec_val = 0;

    U16 srd_selecet=0;
    U16  d_3db_cfo = 0;
    U16  d_3db_diff = 0;
    U16  d_6db_diff = 0;
    U16  d_9db_diff = 0;
    U16  LBtm= 0;
    U16  RBtm= 0;
    U8 csr_check_index=0;
    U8 cfo_ratio=0;
    U16 srd0_cutoff_3dB_left,srd0_cutoff_3dB_right,srd0_cutoff_6dB_left,srd0_cutoff_6dB_right,srd0_cutoff_9dB_left,srd0_cutoff_9dB_right=0;
    U16 srd1_cutoff_3dB_left,srd1_cutoff_3dB_right,srd1_cutoff_6dB_left,srd1_cutoff_6dB_right,srd1_cutoff_9dB_left,srd1_cutoff_9dB_right=0;
    U16 srd2_cutoff_3dB_left,srd2_cutoff_3dB_right,srd2_cutoff_6dB_left,srd2_cutoff_6dB_right,srd2_cutoff_9dB_left,srd2_cutoff_9dB_right=0;
    U16 Left_TrackCount5,Right_TrackCount5,Left_TrackCount10,Right_TrackCount10,Left_TrackCount20,Right_TrackCount20,PSD_Smth_Tap,Left_HW,Right_HW=0;
  
    U16 p4_cfo=0;
    U16 p4_cfo1=0;
    U16 p4_cfo2=0;
    U16 p4_org =0;
    U16 p4_select =0;
    U8 p4_trace_1 =0;
    U8 p4_trace_2 =0;
    U8 p4_retry =0;

    U32 d_agc_gain = 0;
    U32 d_d0_gain = 0;
    U32 d_d1_gain = 0;
    U32 d_d2_gain = 0;
    U32 d_d3_gain = 0;
    S32 combine_cfo_khz = 0;
    U32 d_srd_cfo = 0;
    U32 d_srd1_cfo = 0;
    U32 d_srd2_cfo = 0;

    S32 d_srd_hw_cfo = 0;
    S32 d_srd1_hw_cfo = 0;
    S32 d_srd2_hw_cfo = 0;

    S32 d_p4_cfo = 0;
    S32 d_p4_cfo1 = 0;
    S32 d_p4_cfo2 = 0;
    S32 d_p4_org =0;
    S32 d_p4_select =0;
    //double rf_level_dbm = 0.0;

    U8 reg_agc_inv = 0;
    //U8 dspreg_agc_inv = 0;

    MHal_Demod_MB_ReadReg(0x2800+0x08*2 + 1,&reg);
    agc_k = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x09*2 + 1,&reg);
    agc_ref = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2800+0x09*2,&reg);
    agc_ref = (agc_ref<<4)|((reg&0xf0)>>4);
  
    MHal_Demod_MB_ReadReg(0x2800+0x14*2 + 1,&reg);
    agc_lock = reg&0x01;

    MHal_Demod_MB_ReadReg(0x2800+0x38*2,&reg);
    d0_k = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x38*2 + 1,&reg);
    d0_ref = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x3b*2,&reg);
    d0_lock = reg&0x01;

    MHal_Demod_MB_ReadReg(0x2800+0x58*2,&reg);
    d1_k = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x58*2 + 1,&reg);
    d1_ref = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x5b*2,&reg);
    d1_lock = reg&0x01;

    // acidagc2
    MHal_Demod_MB_ReadReg(0x2800+0x6a*2,&reg);
    d2_k = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x6a*2 + 1,&reg);
    d2_ref = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x6d*2,&reg);
    d2_lock = reg&0x01;

    MHal_Demod_MB_ReadReg(0x2900+0x6a*2,&reg);
    d3_k = reg;
    MHal_Demod_MB_ReadReg(0x2900+0x6a*2 + 1,&reg);
    d3_ref = reg;
    MHal_Demod_MB_ReadReg(0x2900+0x6e*2,&reg);
    d3_lock = reg&0x01;

    // read gain value, sel = 0x03

    MHal_Demod_MB_WriteReg(0x2800+0x11*2,0x03);

    MHal_Demod_MB_ReadReg(0x2800+0x12*2 + 1,&reg);
    agc_gain = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x12*2,&reg);
    agc_gain = (agc_gain<<8)|reg;

    MHal_Demod_MB_ReadReg(0x2800+0x3c*2 + 1,&reg);
    d0_gain = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2800+0x3c*2,&reg);
    d0_gain = (d0_gain<<8)|reg;


    MHal_Demod_MB_ReadReg(0x2800+0x5c*2 + 1,&reg);
    d1_gain = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2800+0x5c*2,&reg);
    d1_gain = (d1_gain<<8)|reg;

    // acidagc2
    MHal_Demod_MB_ReadReg(0x2800+0x6e*2 + 1,&reg);
    d2_gain = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2800+0x6e*2,&reg);
    d2_gain = (d2_gain<<8)|reg;

    // d3 gain read
    MHal_Demod_MB_WriteReg(0x2900+0x6d*2,0x03);
    MHal_Demod_MB_ReadReg(0x2900+0x6e*2 + 1,&reg);
    d3_gain = reg;
    MHal_Demod_MB_ReadReg(0x2900+0x6e*2,&reg);
    d3_gain = (d3_gain<<8)|reg;
    d3_gain = (d3_gain>>4);

    // read agc err value, sel = 0x05
    MHal_Demod_MB_WriteReg(0x2800+0x11*2,0x05);
    MHal_Demod_MB_ReadReg(0x2800+0x12*2 + 1,&reg);
    agc_err = reg;
    MHal_Demod_MB_ReadReg(0x2800+0x12*2,&reg);
    agc_err = (agc_err<<8)|reg;

    MHal_Demod_MB_ReadReg(0x2800+0x50*2,&reg);
    iis_reg = reg;


    // fb_fs
    MHal_Demod_MB_ReadReg(0x2800+0x28*2 + 2,&reg);
    fb_fs = reg&0x07;
    MHal_Demod_MB_ReadReg(0x2800+0x28*2 + 1,&reg);
    fb_fs = (fb_fs<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2800+0x28*2 + 0,&reg);
    fb_fs = (fb_fs<<8)|reg;
    // fc_fs
    MHal_Demod_MB_ReadReg(0x2800+0x2a*2 + 2,&reg);
    fc_fs = reg&0x07;
    MHal_Demod_MB_ReadReg(0x2800+0x2a*2 + 1,&reg);
    fc_fs = (fc_fs<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2800+0x2a*2 + 0,&reg);
    fc_fs = (fc_fs<<8)|reg;
    // nco_ff
    MHal_Demod_MB_ReadReg(0x2800+0x2c*2 + 2,&reg);
    nco_ff = reg&0x07;
    MHal_Demod_MB_ReadReg(0x2800+0x2c*2 + 1,&reg);
    nco_ff = (nco_ff<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2800+0x2c*2 + 0,&reg);
    nco_ff = (nco_ff<<8)|reg;
    // carr_phase
    MHal_Demod_MB_ReadReg(0x2800+0x2e*2 + 1,&reg);
    carr_phase = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2800+0x2e*2 + 0,&reg);
    carr_phase = (carr_phase<<8)|reg;
    // rate1
    MHal_Demod_MB_ReadReg(0x2800+0x68*2 + 3,&reg);
    rate1 = reg&0x1f;
    MHal_Demod_MB_ReadReg(0x2800+0x68*2 + 2,&reg);
    rate1 = (rate1<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2800+0x68*2 + 1,&reg);
    rate1 = (rate1<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2800+0x68*2 + 0,&reg);
    rate1 = (rate1<<8)|reg;

    // sr0
    // srd_frc_cfo
    MHal_Demod_MB_ReadReg(0x2A00+0x10*2 + 1,&reg);
    srd_frc_cfo = (reg>>1)&0x01;

    // srd_cfo, srd_dec_val
    MHal_Demod_MB_ReadReg(0x2A00+0x12*2 + 1,&reg);
    srd_cfo = reg&0xf;
    srd_dec_val = (reg&0x70)>>4;
    MHal_Demod_MB_ReadReg(0x2A00+0x12*2 + 0,&reg);
    srd_cfo = (srd_cfo<<8)|reg;

    // srd_hw_cfo
    MHal_Demod_MB_WriteReg(0x2A00+0x1c*2 + 1,0x08);
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 1,&reg);
    srd_hw_cfo = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 0,&reg);
    srd_hw_cfo = (srd_hw_cfo<<8)|reg;

    // sr1
    // srd_frc_cfo
    MHal_Demod_MB_ReadReg(0x2A00+0x30*2 + 1,&reg);
    srd1_frc_cfo = (reg>>1)&0x01;

    // srd_cfo, srd_dec_val
    MHal_Demod_MB_ReadReg(0x2A00+0x32*2 + 1,&reg);
    srd1_cfo = reg&0xf;
    srd1_dec_val = (reg&0x70)>>4;
    MHal_Demod_MB_ReadReg(0x2A00+0x32*2 + 0,&reg);
    srd1_cfo = (srd1_cfo<<8)|reg;

    // srd1_hw_cfo
    MHal_Demod_MB_WriteReg(0x2A00+0x3c*2 + 1,0x08);
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2 + 1,&reg);
    srd1_hw_cfo = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2 + 0,&reg);
    srd1_hw_cfo = (srd1_hw_cfo<<8)|reg;

    // sr2
    // srd_frc_cfo
    MHal_Demod_MB_ReadReg(0x2A00+0x50*2 + 1,&reg);
    srd2_frc_cfo = (reg>>1)&0x01;

    // srd_cfo, srd_dec_val
    MHal_Demod_MB_ReadReg(0x2A00+0x52*2 + 1,&reg);
    srd2_cfo = reg&0xf;
    srd2_dec_val = (reg&0x70)>>4;
    MHal_Demod_MB_ReadReg(0x2A00+0x52*2 + 0,&reg);
    srd2_cfo = (srd2_cfo<<8)|reg;

    // srd2_hw_cfo
    MHal_Demod_MB_WriteReg(0x2A00+0x5c*2 + 1,0x08);
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2 + 1,&reg);
    srd2_hw_cfo = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2 + 0,&reg);
    srd2_hw_cfo = (srd2_hw_cfo<<8)|reg;

    // mixer_cfo_combine [26:0] (27,27)
    MHal_Demod_MB_ReadReg(0x2800+0x33*2 + 3,&reg);
    mix_cfo_combine = reg&0x07;
    MHal_Demod_MB_ReadReg(0x2800+0x33*2 + 2,&reg);
    mix_cfo_combine = (mix_cfo_combine<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2800+0x33*2 + 1,&reg);
    mix_cfo_combine = (mix_cfo_combine<<8)|reg;
    MHal_Demod_MB_ReadReg(0x2800+0x33*2 + 0,&reg);
    mix_cfo_combine = (mix_cfo_combine<<8)|reg;


    MHal_Demod_MB_ReadReg(0x2900+0x2b*2 + 1,&reg);
    p4_cfo = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2900+0x2b*2 + 0,&reg);
    p4_cfo = (p4_cfo<<8)|reg;

    MHal_Demod_MB_ReadReg(0x2900+0x3b*2 + 1,&reg);
    p4_cfo1 = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2900+0x3b*2 + 0,&reg);
    p4_cfo1 = (p4_cfo1<<8)|reg;

    MHal_Demod_MB_ReadReg(0x2900+0x4b*2 + 1,&reg);
    p4_cfo2 = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2900+0x4b*2 + 0,&reg);
    p4_cfo2 = (p4_cfo2<<8)|reg;

    d_agc_gain = agc_gain*SHIFT_MULTIPLE/32768;
    d_d0_gain = d0_gain*SHIFT_MULTIPLE/512;
    d_d1_gain = d1_gain*SHIFT_MULTIPLE/512;
    d_d2_gain = d2_gain*SHIFT_MULTIPLE/512;
    d_d3_gain = d3_gain*SHIFT_MULTIPLE/512;


    if (mix_cfo_combine & 0x4000000)
        combine_cfo_khz = mix_cfo_combine - 0x8000000;
    else
        combine_cfo_khz = mix_cfo_combine;

    combine_cfo_khz = combine_cfo_khz*SAMPLING_RATE/134217728;

    if (srd_cfo & 0x800)
        d_srd_cfo = (srd_cfo-0x1000)/4096*SAMPLING_RATE;
    else
        d_srd_cfo = srd_cfo/4096*SAMPLING_RATE;

    if (srd1_cfo & 0x800)
        d_srd1_cfo = (srd1_cfo-0x1000)/4096*SAMPLING_RATE;
    else
        d_srd1_cfo = srd1_cfo/4096*SAMPLING_RATE;

    if (srd2_cfo & 0x800)
        d_srd2_cfo = (srd2_cfo-0x1000)/4096*SAMPLING_RATE;
    else
        d_srd2_cfo = srd2_cfo/4096*SAMPLING_RATE;

    if (srd_hw_cfo & 0x800)
        d_srd_hw_cfo = (srd_hw_cfo-0x1000)/4096*SAMPLING_RATE;
    else
        d_srd_hw_cfo = srd_hw_cfo/4096*SAMPLING_RATE;

    if (srd1_hw_cfo & 0x800)
        d_srd1_hw_cfo = (srd1_hw_cfo-0x1000)/4096*SAMPLING_RATE;
    else
        d_srd1_hw_cfo = srd1_hw_cfo/4096*SAMPLING_RATE;

    if (srd2_hw_cfo & 0x800)
        d_srd2_hw_cfo = (srd2_hw_cfo-0x1000)/4096*SAMPLING_RATE;
    else
        d_srd2_hw_cfo = srd2_hw_cfo/4096*SAMPLING_RATE;


    if (p4_cfo & 0x800)
        d_p4_cfo = (p4_cfo-0x1000)/4096*SAMPLING_RATE/4;
    else
        d_p4_cfo = p4_cfo/4096*SAMPLING_RATE/4;

    if (p4_cfo1 & 0x800)
        d_p4_cfo1 = (p4_cfo1-0x1000)/4096*SAMPLING_RATE/4;
    else
        d_p4_cfo1 = p4_cfo1/4096*SAMPLING_RATE/4;
  
    if (p4_cfo2 & 0x800)
        d_p4_cfo2 = (p4_cfo2-0x1000)/4096*SAMPLING_RATE/4;
    else
        d_p4_cfo2 = p4_cfo2/4096*SAMPLING_RATE/4;

    //IIC_Bypass_Mode(true);
    //rf_level_dbm = MDrv_DVBS_Tuner_Get_RSSI(agc_gain,1);
    //IIC_Bypass_Mode(false);

    MHal_Demod_MB_ReadReg(0x2800+0x18*2,&reg);
    reg_agc_inv = reg;

    //MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_IFAGC_POLARITY, &reg);
    //dspreg_agc_inv = reg;

    //CSRD
    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE00L, &reg);
    srd_selecet=reg;

    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE00H, &reg);
    d_3db_cfo = reg;

    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE01L, &reg);
    d_3db_diff = reg;

    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE01H, &reg);
    d_6db_diff = reg;

    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE02L, &reg);
    d_9db_diff = reg;

    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE03H, &reg);
    LBtm= reg;
    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE03L, &reg);
    LBtm= (LBtm<<8)|reg;

    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE04H, &reg);
    RBtm= reg;
    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE04L, &reg);
    RBtm= (RBtm<<8)|reg;

    //CSR check index
    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_DBG_0, &reg);
    csr_check_index= reg;

    //cfo ratio
    MHal_Demod_MB_ReadDspReg((U16)E_DMD_S2_MB_DMDTOP_SWUSE05L, &reg);
    cfo_ratio= reg;
  
    //SRD 1 cutoff
    MHal_Demod_MB_WriteReg(0x2A00+0x3c*2 + 1,0x06);
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2 + 1,&reg);
    srd1_cutoff_3dB_left = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2 + 0,&reg);
    srd1_cutoff_3dB_left = (srd1_cutoff_3dB_left<<8)|reg;

    MHal_Demod_MB_WriteReg(0x2A00+0x3c*2 + 1,0x05);
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2 + 1,&reg);
    srd1_cutoff_3dB_right = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2 + 0,&reg);
    srd1_cutoff_3dB_right = (srd1_cutoff_3dB_right<<8)|reg;
  
    MHal_Demod_MB_WriteReg(0x2A00+0x3c*2 + 1,0x0E);
    MHal_Demod_MB_ReadReg(0x2A00+0x43*2,&reg);
    srd1_cutoff_6dB_left = reg&0xff;
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2 + 1,&reg);
    srd1_cutoff_6dB_left = (srd1_cutoff_6dB_left<<4)|(reg&0xf0>>4);

    MHal_Demod_MB_ReadReg(0x2A00+0x42*2+1,&reg);
    srd1_cutoff_6dB_right = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2,&reg);
    srd1_cutoff_6dB_right = (srd1_cutoff_6dB_right<<8)|(reg);
    
    MHal_Demod_MB_WriteReg(0x2A00+0x3c*2 + 1,0x0F);
    MHal_Demod_MB_ReadReg(0x2A00+0x43*2,&reg);
    srd1_cutoff_9dB_left = reg&0xff;
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2 + 1,&reg);
    srd1_cutoff_9dB_left = (srd1_cutoff_9dB_left<<4)|(reg&0xf0>>4);

    MHal_Demod_MB_ReadReg(0x2A00+0x42*2+1,&reg);
    srd1_cutoff_9dB_right = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x42*2,&reg);
    srd1_cutoff_9dB_right = (srd1_cutoff_9dB_right<<8)|(reg);

    //SRD 2 cutoff
    MHal_Demod_MB_WriteReg(0x2A00+0x5c*2 + 1,0x06);
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2 + 1,&reg);
    srd2_cutoff_3dB_left = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2 + 0,&reg);
    srd2_cutoff_3dB_left = (srd2_cutoff_3dB_left<<8)|reg;

    MHal_Demod_MB_WriteReg(0x2A00+0x5c*2 + 1,0x05);
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2 + 1,&reg);
    srd2_cutoff_3dB_right = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2 + 0,&reg);
    srd2_cutoff_3dB_right = (srd2_cutoff_3dB_right<<8)|reg;
  
    MHal_Demod_MB_WriteReg(0x2A00+0x5c*2 + 1,0x0E);
    MHal_Demod_MB_ReadReg(0x2A00+0x63*2,&reg);
    srd2_cutoff_6dB_left = reg&0xff;
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2 + 1,&reg);
    srd2_cutoff_6dB_left = (srd2_cutoff_6dB_left<<4)|(reg&0xf0>>4);
  
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2+1,&reg);
    srd2_cutoff_6dB_right = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2,&reg);
    srd2_cutoff_6dB_right = (srd2_cutoff_6dB_right<<8)|(reg);
  
    MHal_Demod_MB_WriteReg(0x2A00+0x5c*2 + 1,0x0F);
    MHal_Demod_MB_ReadReg(0x2A00+0x63*2,&reg);
    srd2_cutoff_9dB_left = reg&0xff;
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2 + 1,&reg);
    srd2_cutoff_9dB_left = (srd2_cutoff_9dB_left<<4)|(reg&0xf0>>4);

    MHal_Demod_MB_ReadReg(0x2A00+0x62*2+1,&reg);
    srd2_cutoff_9dB_right = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x62*2,&reg);
    srd2_cutoff_9dB_right = (srd2_cutoff_9dB_right<<8)|(reg);

    // SRD0 cutoff
    MHal_Demod_MB_WriteReg(0x2A00+0x1c*2 + 1,0x06);
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 1,&reg);
    srd0_cutoff_3dB_left = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 0,&reg);
    srd0_cutoff_3dB_left = (srd0_cutoff_3dB_left<<8)|reg;

    MHal_Demod_MB_WriteReg(0x2A00+0x1c*2 + 1,0x05);
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 1,&reg);
    srd0_cutoff_3dB_right = reg&0xf;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 0,&reg);
    srd0_cutoff_3dB_right = (srd0_cutoff_3dB_right<<8)|reg;
  
    MHal_Demod_MB_WriteReg(0x2A00+0x1c*2 + 1,0x0E);
    MHal_Demod_MB_ReadReg(0x2A00+0x23*2,&reg);
    srd0_cutoff_6dB_left = reg&0xff;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 1,&reg);
    srd0_cutoff_6dB_left = (srd0_cutoff_6dB_left<<4)|(reg&0xf0>>4);

    MHal_Demod_MB_ReadReg(0x2A00+0x22*2+1,&reg);
    srd0_cutoff_6dB_right = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2,&reg);
    srd0_cutoff_6dB_right = (srd0_cutoff_6dB_right<<8)|(reg);
  
    MHal_Demod_MB_WriteReg(0x2A00+0x1c*2 + 1,0x0F);
    MHal_Demod_MB_ReadReg(0x2A00+0x23*2,&reg);
    srd0_cutoff_9dB_left = reg&0xff;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 1,&reg);
    srd0_cutoff_9dB_left = (srd0_cutoff_9dB_left<<4)|(reg&0xf0>>4);

    MHal_Demod_MB_ReadReg(0x2A00+0x22*2+1,&reg);
    srd0_cutoff_9dB_right = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2,&reg);
    srd0_cutoff_9dB_right = (srd0_cutoff_9dB_right<<8)|(reg);
  
    //TR lock 
    MHal_Demod_MB_ReadReg(0x3b00+(0x0e)*2+1, &reg);
    if (reg&0x01)
    {
        printk("TR 0 lock\n");
    }

    MHal_Demod_MB_ReadReg(0x3D00+(0x19)*2+1, &reg);
    if (reg&0x01)
    {
        printk("TR 1 lock\n");
    }

    MHal_Demod_MB_ReadReg(0x3D00+(0x29)*2+1, &reg);
    if (reg&0x01)
    {
        printk("TR 2 lock\n");
    }
  
    //HW spectrum tracker
    MHal_Demod_MB_ReadReg(0x2A00+0x18*2+1,&reg);
    PSD_Smth_Tap = reg&0x7f;
  
    MHal_Demod_MB_ReadReg(0x2A00+0x76*2+1,&reg);
    Left_TrackCount5 = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x76*2,&reg); 
    Left_TrackCount5 = (Left_TrackCount5<<8)|(reg);
    MHal_Demod_MB_ReadReg(0x2A00+0x73*2+1,&reg);
    Right_TrackCount5 = reg;
    MHal_Demod_MB_ReadReg(0x2A00+0x73*2,&reg); 
    Right_TrackCount5 = (Right_TrackCount5<<4)|((reg&0xf0)>>4);
  
    MHal_Demod_MB_ReadReg(0x2A00+0x77*2+1,&reg);
    Left_TrackCount10 = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x77*2,&reg); 
    Left_TrackCount10 = (Left_TrackCount10<<8)|(reg);
    MHal_Demod_MB_ReadReg(0x2A00+0x74*2+1,&reg);
    Right_TrackCount10 = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x74*2,&reg); 
    Right_TrackCount10 = (Right_TrackCount10<<8)|(reg);

    MHal_Demod_MB_ReadReg(0x2A00+0x78*2+1,&reg);
    Left_TrackCount20 = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x78*2,&reg); 
    Left_TrackCount20 = (Left_TrackCount20<<8)|(reg);
    MHal_Demod_MB_ReadReg(0x2A00+0x75*2+1,&reg);
    Right_TrackCount20 = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x75*2,&reg); 
    Right_TrackCount20 = (Right_TrackCount20<<8)|(reg);

    //HW 
    MHal_Demod_MB_WriteReg(0x2A00+0x1c*2 + 1,0x11);
    MHal_Demod_MB_ReadReg(0x2A00+0x23*2,&reg);
    Left_HW = reg;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 +1 ,&reg);
    Left_HW = (Left_HW <<4)|((reg&0xf0)>>4);
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2 + 1,&reg);
    Right_HW = reg&0x0f;
    MHal_Demod_MB_ReadReg(0x2A00+0x22*2,&reg);
    Right_HW = (Right_HW<<8)|(reg);

    //P4 monitor
    MHal_Demod_MB_ReadReg(0x2000+0x66*2,&p4_trace_1);
    MHal_Demod_MB_ReadReg(0x2000+0x66*2+1,&p4_trace_2);
    MHal_Demod_MB_ReadReg(0x2000+0x7D*2,&p4_retry);
  
    MHal_Demod_MB_ReadReg(0x2000+0x7A*2+1,&reg);
    p4_org = reg;
    MHal_Demod_MB_ReadReg(0x2000+0x7A*2,&reg);
    p4_org = (p4_org<<8)|reg;
    if (p4_org & 0x8000)
        d_p4_org = (-1)*(0x10000 -p4_org)/4096*SAMPLING_RATE/4;
    else
        d_p4_org = p4_org/4096*SAMPLING_RATE/4;
    
    MHal_Demod_MB_ReadReg(0x2000+0x7B*2+1,&reg);
    p4_select = reg;
    MHal_Demod_MB_ReadReg(0x2000+0x7B*2,&reg);
    p4_select = (p4_select<<8)|reg;
    if (p4_select & 0x8000)
        d_p4_select =  (-1)*(0x10000 -p4_select)/4096*SAMPLING_RATE;
    else
        d_p4_select = p4_select/4096*SAMPLING_RATE;
    
    printk("[ft][1]agc_k=0x%x,agc_ref=0x%x,agc_lock=0x%x,agc_gain=0x%x, d_agc_gain*e3=%d\n",agc_k,agc_ref, agc_lock,agc_gain,d_agc_gain);
    printk("[ft][2]d0_k=0x%x,d0_ref=0x%x,d0_lock=0x%x,d0_gain=0x%x,d_d0_gain*e3=%d\n",d0_k,d0_ref, d0_lock,d0_gain,d_d0_gain);
    printk("[ft][3]d1_k=0x%x,d1_ref=0x%x,d1_lock=0x%x,d1_gain=0x%x,d_d1_gain*e3=%d\n",d1_k,d1_ref, d1_lock,d1_gain,d_d1_gain);
    printk("[ft][4]d2_k=0x%x,d2_ref=0x%x,d2_lock=0x%x,d2_gain=0x%x,d_d2_gain*e3=%d\n",d2_k,d2_ref, d2_lock,d2_gain,d_d2_gain);
    printk("[ft][5]d3_k=0x%x,d3_ref=0x%x,d3_lock=0x%x,d3_gain=0x%x,d_d3_gain*e3=%d\n",d3_k,d3_ref, d3_lock,d3_gain,d_d3_gain);
    printk("[ft][6]iis=0x%x\n",iis_reg);
    printk("[ft][7]fb_fs=0x%x,fc_fs=0x%x,nco_ff=0x%x,carr_phase=0x%x\n",fb_fs,fc_fs,nco_ff,carr_phase);
    printk("[ft][8]rate1=0x%x\n",rate1);
    printk("[ft][9]agc_err=0x%x\n",agc_err);


    printk("[ft][10]srd_frc_cfo=%d, srd_cfo=0x%x, srd_dec_val=%d,mix_cfo_combine=0x%x\n",srd_frc_cfo,srd_cfo,srd_dec_val,mix_cfo_combine);
    printk("[ft][11]srd1_frc_cfo=%d, srd1_cfo=0x%x, srd1_dec_val=%d,mix_cfo_combine=0x%x\n",srd1_frc_cfo,srd1_cfo,srd1_dec_val,mix_cfo_combine);
    printk("[ft][12]srd2_frc_cfo=%d, srd2_cfo=0x%x, srd2_dec_val=%d,mix_cfo_combine=0x%x\n",srd2_frc_cfo,srd2_cfo,srd2_dec_val,mix_cfo_combine);
    printk("[ft][13]mix_cfo_combine_khz=%d KHz\n",combine_cfo_khz);
    printk("[ft][14]srd_cfo=%d,srd1_cfo=%d,srd2_cfo=%d KHz\n",d_srd_cfo,d_srd1_cfo,d_srd2_cfo);
    printk("[ft][15]srd_hw_cfo=%d,srd1_hw_cfo=%d,srd2_hw_cfo=%d KHz\n",d_srd_hw_cfo,d_srd1_hw_cfo,d_srd2_hw_cfo);
    printk("[ft][16]d_p4_cfo=%d,d_p4_cfo1=%d,d_p4_cfo2=%d KHz (no including deci)\n",d_p4_cfo,d_p4_cfo1,d_p4_cfo2);
    //printk("[ft][17]rf_level=%.2f dBm\n",rf_level_dbm);
    printk("[ft][17]reg_agc_inv=0x%x,cfo_ratio=x%x\n",reg_agc_inv,cfo_ratio);
    printk("[ft][18]srd_selecet=0x%x,d_3db_cfo=0x%x, d_3db_diff=0x%x, d_6db_diff=0x%x, d_9db_diff=0x%x, LBtm=0x%x, RBtm=0x%x, \n", srd_selecet,d_3db_cfo, d_3db_diff, d_6db_diff, d_9db_diff, LBtm, RBtm);
    printk("[ft][19]srd0_cutoff_3dB_left=0x%x, srd0_cutoff_3dB_right=0x%x, srd0_cutoff_6dB_left=0x%x, srd0_cutoff_6dB_right=0x%x, srd0_cutoff_9dB_left=0x%x, srd0_cutoff_9dB_right=0x%x, \n", srd0_cutoff_3dB_left, srd0_cutoff_3dB_right, srd0_cutoff_6dB_left, srd0_cutoff_6dB_right, srd0_cutoff_9dB_left, srd0_cutoff_9dB_right);
    printk("[ft][20]srd1_cutoff_3dB_left=0x%x, srd1_cutoff_3dB_right=0x%x, srd1_cutoff_6dB_left=0x%x, srd1_cutoff_6dB_right=0x%x, srd1_cutoff_9dB_left=0x%x, srd1_cutoff_9dB_right=0x%x, \n", srd1_cutoff_3dB_left, srd1_cutoff_3dB_right, srd1_cutoff_6dB_left, srd1_cutoff_6dB_right, srd1_cutoff_9dB_left, srd1_cutoff_9dB_right);
    printk("[ft][21]srd2_cutoff_3dB_left=0x%x, srd2_cutoff_3dB_right=0x%x, srd2_cutoff_6dB_left=0x%x, srd2_cutoff_6dB_right=0x%x, srd2_cutoff_9dB_left=0x%x, srd2_cutoff_9dB_right=0x%x, \n", srd2_cutoff_3dB_left, srd2_cutoff_3dB_right, srd2_cutoff_6dB_left, srd2_cutoff_6dB_right, srd2_cutoff_9dB_left, srd2_cutoff_9dB_right);
    printk("[ft][22]csr_check_index=0x%x, PSD_Smth_Tap =0x%x, Left_TrackCount5 =0x%x, Right_TrackCount5 =0x%x,Left_TrackCount10 =0x%x,Right_TrackCount10 =0x%x,Left_TrackCount20 =0x%x,Right_TrackCount20 =0x%x,Left_HW =0x%x,Right_HW =0x%x\n", csr_check_index,PSD_Smth_Tap,Left_TrackCount5,Right_TrackCount5,Left_TrackCount10,Right_TrackCount10,Left_TrackCount20,Right_TrackCount20,Left_HW, Right_HW);
    printk("[ft][23]p4_trace_1=0x%x, p4_trace_2 =0x%x, p4_retry =0x%x, d_p4_org =%d KHz, d_p4_select =%d KHz dec_factor =%d\n", p4_trace_1, p4_trace_2, p4_retry, d_p4_org, d_p4_select, (srd_dec_val<<1));
    return;
}


void MSD_sw_info(BOOL FFT_CAPTURE_EN)
{

  U8 reg = 0;
  U8 fsm = 0;
  U8 fsm_sub = 0;
  //U8 hit  = 0;
  //U16 reg_0946h = 0;
  static U8 counter = 0;
  static U8 fft_capture  = 0;
  // U16 reg_0945h = 0;


  MHal_Demod_MB_ReadDspReg(E_DMD_S2_STATE_FLAG, &reg);
  fsm = reg;
  MHal_Demod_MB_ReadDspReg(E_DMD_S2_SUBSTATE_FLAG, &reg); 
  fsm_sub = reg;

#if 0 // cci
  MHal_Demod_MB_ReadReg(0x0b00+0x47*2,&reg);
  hit = reg;

  MHal_Demod_MB_ReadReg(0x0900+0x46*2+1,&reg);
  reg_0946h = reg;
  MHal_Demod_MB_ReadReg(0x0900+0x46*2,&reg);
  reg_0946h = (reg_0946h<<8)|reg;
#endif
 

  printk("[MSD]fsm = %d,fsm_sub=%d,[%d]\n",fsm,fsm_sub, (U32)jiffies_to_msecs(jiffies));

  if(fsm >= 15)
  {
      MSD_show_version();
      MSD_front_info();
      // msb1240_sw_indicator();
      //MSD_top_indicator();
      MSD_inner_info();
      MSD_outer_info();
      MSD_ACI_coefficient_read();
  }
  else
  {
      MSD_show_version();
      MSD_front_info();
      // msb1240_sw_indicator();
      // msb1240_top_indicator();
      MSD_inner_info();
      // msb1240_outer_info();
  }

  if (fsm >= 15) fft_capture++;
  else fft_capture = 0;

  if (FFT_CAPTURE_EN == 1)
  {
      if ( (fsm >= 14) && (fft_capture == 15) )
      {
          MSD_fft_capture(0,0,4095,1);
          MSD_fft_capture(4,0,4095,1);
#if (FFT_CAPTURE_EN_debug==1)          
          while(1)
          {
              mdelay(500);//udelay(500*1000);
              printk("DVBS/S2 FFT Capture ........ Done\n");
              mdelay(500);//udelay(500*1000);
          }
#endif
    } 
  }
  counter++;

  return;
}

