////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2008 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (!��MStar Confidential Information!�L) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//
/// @file mhal_demod_DVBTxC.c
/// @brief SOC demod DVBT, DVBT2, DVBC Controller Interface
/// @author MStar Semiconductor, Inc.
//
////////////////////////////////////////////////////////////////////////////////
#define _MHAL_DEMOD_DVBTXC_C_
#include <linux/kernel.h>
#include <linux/jiffies.h>
#include <linux/delay.h>
#include <asm/setup.h>
#include "mdrv_types.h"
#include "mdrv_system.h"
#include "mhal_demod.h"
#include "mhal_demod_DVBTxC.h"
#include "MsFixpLib.h"

#define DBG_MSB(x)

#define MBRegBase   0x112600
#define DMDMcuBase  0x103480
#define DMD_CLK_GEN 0x103300
#define T2FDP_REG_BASE  0x3100
#define T2L1_REG_BASE  0x2b00
#define T2TDP_REG_BASE  0x3000
#define DVBTM_REG_BASE  0x3400
#define T2DJB_REG_BASE  0x2d00
#define T2FEC_REG_BASE  0x3300
#define EQE_REG_BASE  0x9a00UL			// P2 = 1,  0x11a00 -> 0x1a00
#define EQE2_REG_BASE 0x9c00UL
#define FE_REG_BASE 0x2800
#define BACKEND_REG_BASE  0x1f00

#define INTERNAL_DVBT2_EQ_LEN        0x96000   // 600K
#define INTERNAL_DVBT2_TDI_LEN       0x320000   //3200KB
#define INTERNAL_DVBT2_DJB_LEN       0x96000   //600K
#define INTERNAL_DVBT2_DRAM_OFFSET   0x5000

#ifdef DEMOD_DVB_T2_MERGE_T
#define INTERNAL_DVBT2_FW_LEN        (0x20000 - INTERNAL_DVBT2_DRAM_OFFSET)
#else
#define INTERNAL_DVBT2_FW_LEN        (0x10000 - INTERNAL_DVBT2_DRAM_OFFSET)
#endif
#define INTERNAL_DVBT2_PS2XDATA_LEN  15

#define TS_SER_C        0x00    //0: parallel 1:serial
#define TS_INV_C        TS_CLK_INV

#define TS_CLK_INV  1
#define TS_SER      0x00    //0: parallel 1:serial
#define TS_INV      TS_CLK_INV

#define LOW_IF      1
// for Silab tuner
#define Fif_H_SI    0x13    // 0x1388 Khz (5000 Khz)
#define Fif_L_SI    0x88
// for LG10 tuner
#define Fif_H_LG10  0x11    // 0x11D0 Khz (4560 Khz)
#define Fif_L_LG10  0xD0

//for Silab tuner
#define FC_H_SI     0xA7 
#define FC_L_SI      0xF8  
//for LG10 tuner
#define FC_H_LG10   0x4B 
#define FC_L_LG10   0xF0

#define FS_H        0xBB
#define FS_L        0x80

#define IFAGC_REF_8K_8M 0x00
#define IFAGC_REF_8K_8M_H   0x05
#define IFAGC_REF_8K_6M 0x10
#define IFAGC_REF_8K_6M_H   0x06
#define IFAGC_REF_2K        0x00
#define IFAGC_REF_2K_H      0x02
#define IFAGC_REF_ACI       0x00
#define IFAGC_REF_ACI_H     0x02
#define IFAGC_REF_IIS       0x2F
#define IFAGC_REF_IIS_H     0x02

#define FORCE_MC        0x00    //0: auto 1: Force mode-cp
#define FORCE_TPS       0x00    //0: auto 1: Force TPS
#define AUTO_SCAN       0x00    // Auto Scan - 0:channel change, 1:auto-scan
#define CSTL            0x02    //0:QPSK 1:16 2: 64
#define HIER            0x00
#define HPCR            0x01    // HP_CR 0:1/2, 1:2/3, 2: 3/4, 3: 5/6, 4:7/8
#define LPCR            0x01    // LP_CR 0:1/2, 1:2/3, 2: 3/4, 3: 5/6, 4:7/8
#define FFT_MODE        0x01    // FFT mode - 0:2K, 1:8K
#define CP              0x00    // CP - 0:1/32, 1/16, 1/8, 1/4
#define LP_SEL          0x00    // LP select
#define IQ_SWAP         0x00 
#define PAL_I           0x00    // PAL_I: 0: Non-Pal-I CCI, 1: Pal-I CCI (for UK)
#define CFO_RANGE       0x01    //0: 500KHz 1: 900KHz
#define CFO_RANGE_TW    0x00    //0: 500KHz 1: 900KHz
#define TPS_MAJORITY_MODE_TW 0x01  // TW use 1, AU EU use 0
#define TPS_MAJORITY_MODE_EU 0x00 // TW use 1, AU EU use 0

#define AUTO_SCAN_C 0x00    // Auto Scan - 0:channel change, 1:auto-scan
#define IQ_SWAP_C       0x00
#define PAL_I_C         0x00    // PAL_I: 0: Non-Pal-I CCI, 1: Pal-I CCI (for UK)
#define SR0_H           0x1A    // Symbol Rate: 6875 = 0x1ADB
#define SR0_L           0xDB
#define SR1_H           0x1A    // Symbol Rate: 6900 = 0x1AF4
#define SR1_L           0xF4
#define SR2_H           0x14    // Symbol Rate: 5200 = 0x1450
#define SR2_L           0x50
#define SR3_H           0x10    // Symbol Rate: 4200 = 0x1068
#define SR3_L           0x68
#define SR4_H                   0x1B      // Symbol Rate: 6950 = 0x1B26
#define SR4_L                    0x26
#define SR5_H                   0x13       // Symbol Rate: 5000 = 0x1388
#define SR5_L                   0x88
#define SR6_H                   0x1C        // Symbol Rate: 7200 = 0x1C20
#define SR6_L                   0x20
#define SR7_H                0x1C           // Symbol Rate: 7250 = 0x1C52
#define SR7_L                   0x52
#define SR8_H                   0x0B        // Symbol Rate: 3000 = 0x0BB8
#define SR8_L                   0xB8
#define SR9_H                   0x03        // Symbol Rate: 1000 = 0x03e8
#define SR9_L                   0xE8
#define SR10_H               0x07           // Symbol Rate: 2000 = 0x07D0
#define SR10_L               0xD0
#define SR11_H               0x00
#define SR11_L               0x00
#define QAM             0x04    // QAM: 0:16, 1:32, 2:64, 3:128, 4:256
#define CCI_CONT        0x00
#define AUTO_QAM        0x01
#define AUTO_SR         0x01

#define T2_BW_VAL               0x04
// FC: FC = FS = 5000 = 0x1388     (5.0MHz IF)
#define T2_FC_L_VAL            0x88    // 5.0M
#define T2_FC_H_VAL            0x13
#define T2_TS_SERIAL_VAL        0x00
#define T2_TS_CLK_RATE_VAL      0x06
#define T2_TS_OUT_INV_VAL       0x00
#define T2_TS_DATA_SWAP_VAL     0x00
#define T2_IF_AGC_INV_PWM_EN_VAL 0x00
#define T2_LITE_VAL 0x00
#define T2_AGC_REF_VAL 0x40

#define SUPPORT_ADAPTIVE_TS_CLK
#define TS_DATA_RATE_RATIO 101
#define DBG_AUTO_TS_DATA_RATE(x) x

#define MBX_REG_BASE  0x2F00UL
#define TOP_REG_BASE   0x2000

#define MMAP_INDEX_ISDBT_TDI    62
#define MMAP_INDEX_MEM_ADDR     1
#define MMAP_INDEX_MEM_LEN      3

#define E_DMD_DVBC_UNLOCK_FLAG_0 0x8d
#define E_DMD_DVBC_UNLOCK_FLAG_1 0x8e
#define E_DMD_DVBC_UNLOCK_FLAG_2 0x8f

// ------------------------------------------------------------------------------------------------------------------------------------------------------
typedef struct
{
    DMD_T2_CONSTEL  constel;
    DMD_T2_CODERATE code_rate;
    S32             cn_ref_e2;
} MHal_DVBTxC_T2_SQI_CN_NORDIGP1;

typedef struct
{
	U8		constel;
	U8		code_rate;
	S32	    cn_ref_e2;
} MHal_DVBTxC_SQI_CN_NORDIGP1;

// ------------------------------------------------------------------------------------------------------------------------------------------------------
extern struct tag_custom customTAG;

// ------------------------------------------------------------------------------------------------------------------------------------------------------
#ifdef SUPPORT_ADAPTIVE_TS_CLK
static U8 g_dvb_lock = 0;
static BOOL INTERN_DVB_Locked_Task(E_SYSTEM system);
static BOOL  INTERN_DVB_Adaptive_TS_CLK(E_SYSTEM system);
static BOOL INTERN_DVBT2_GetTsDivNum(MS_U8* u8TSDivNum);
#endif
static void MHal_DVBTxC_Change_TS_Clock(U8 clk_sel, U8 div_num);

static U8 MHal_DVBTxC_DVBT_DSPREG_SI2176_8M[] =    // EU 8M
{
    0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x00, 0x00, FORCE_MC, FORCE_TPS, AUTO_SCAN, 0x00, 0x00, //00-0F
    0x00, LOW_IF, 0x00, FS_L, FS_H, Fif_L_SI, Fif_H_SI, FC_L_SI, FC_H_SI, 0x03, FFT_MODE, CP, LP_SEL, CSTL, HIER, HPCR, //10-1F
    LPCR, IQ_SWAP, 0x00, PAL_I, CFO_RANGE, 0x00, 0x00, IFAGC_REF_8K_8M, 0x00, 0x00, 0x00, IFAGC_REF_8K_8M_H, 0x00, 0x00, TS_SER, TS_INV, // 20 - 2F
    0x00, 0xf0, 0x0a, 0xc4, 0x09, 0xc4, 0x09, 0xf0, 0x0a, 0xc4, 0x09, 0xc4, 0x09, 0x00, 0xd0, 0x80, //30-3F
    0x7f, 0xa0, 0x23, 0x05, 0x05, 0x40, 0x34, 0x06, 0x00, 0x00, 0x00, 0x00, 0x45, 0x00, 0x65, 0x00, //40-4F
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //50-5F
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0x01, 0x03, //60-6F
    0x03, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, TPS_MAJORITY_MODE_EU, 0x00,       //70-7E
};

static U8 MHal_DVBTxC_DVBT_DSPREG_SI2176_6M[] =    // TW 6M
{
    0x00, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x00, 0x00, FORCE_MC, FORCE_TPS, AUTO_SCAN, 0x00, 0x00, //00-0F
    0x00, LOW_IF, 0x00, FS_L, FS_H, Fif_L_SI, Fif_H_SI, FC_L_SI, FC_H_SI, 0x03, FFT_MODE, CP, LP_SEL, CSTL, HIER, HPCR, //10-1F
    LPCR, IQ_SWAP, 0x00, PAL_I, CFO_RANGE, 0x00, 0x00, IFAGC_REF_8K_6M, 0x00, 0x00, 0x00, IFAGC_REF_8K_6M_H, 0x00, 0x00, TS_SER, TS_INV, // 20 - 2F
    0x00, 0xf0, 0x0a, 0xc4, 0x09, 0xc4, 0x09, 0xf0, 0x0a, 0xc4, 0x09, 0xc4, 0x09, 0x00, 0xd0, 0x80, //30-3F
    0x7f, 0xa0, 0x23, 0x05, 0x05, 0x40, 0x34, 0x06, 0x00, 0x00, 0x00, 0x00, 0x45, 0x00, 0x65, 0x00, //40-4F
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //50-5F
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0x01, 0x03, //60-6F
    0x03, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, TPS_MAJORITY_MODE_TW, 0x00,       //70-7E
};

static U8 MHal_DVBTxC_DVBC_DSPREG_SI2176[] =
{
 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, AUTO_SCAN_C, AUTO_SR, AUTO_QAM, CCI_CONT, 0x00, 0x00, 0x01, 0x00, //00-0F
 0x00, 0x00, 0x00, FS_L, FS_H, Fif_L_SI, Fif_H_SI, FC_L_SI, FC_H_SI, SR0_L, SR0_H, SR1_L, SR1_H, SR2_L, SR2_H, SR3_L,       //10-1F
 SR3_H, SR4_L, SR4_H, SR5_L, SR5_H, SR6_L, SR6_H, SR7_L, SR7_H, SR8_L, SR8_H, SR9_L, SR9_H, SR10_L, SR10_H, SR11_L,                     //20-2F
 SR11_H, 0x00, QAM, IQ_SWAP_C, PAL_I_C, TS_SER_C, 0x00, TS_INV_C, 0x00, 0x00, 0xF0, 0x02, 0x90, 0xa0, 0x03, 0x05,                       //30-3F
 0x05, 0x40, 0x04, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x7F, 0x00, 0xFF, 0xFF, 0xFF,    //40-4F
 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x30, 0x73, 0x73, 0x73, 0x73, 0x73, 0x83, 0x83, 0x73,                            //50-5F
 0x62, 0x62, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,                                                          //60-6C
};

static MHal_DVBTxC_T2_SQI_CN_NORDIGP1 DVBT2_SqiCnNordigP1[] =
{
    {_T2_QPSK, _T2_CR1Y2, 350},
    {_T2_QPSK, _T2_CR3Y5, 470},
    {_T2_QPSK, _T2_CR2Y3, 560},
    {_T2_QPSK, _T2_CR3Y4, 660},
    {_T2_QPSK, _T2_CR4Y5, 720},
    {_T2_QPSK, _T2_CR5Y6, 770},

    {_T2_16QAM, _T2_CR1Y2, 870},
    {_T2_16QAM, _T2_CR3Y5, 1010},
    {_T2_16QAM, _T2_CR2Y3, 1140},
    {_T2_16QAM, _T2_CR3Y4, 1250},
    {_T2_16QAM, _T2_CR4Y5, 1330},
    {_T2_16QAM, _T2_CR5Y6, 1380},

    {_T2_64QAM, _T2_CR1Y2, 1300},
    {_T2_64QAM, _T2_CR3Y5, 1480},
    {_T2_64QAM, _T2_CR2Y3, 1620},
    {_T2_64QAM, _T2_CR3Y4, 1770},
    {_T2_64QAM, _T2_CR4Y5, 1870},
    {_T2_64QAM, _T2_CR5Y6, 1940},

    {_T2_256QAM, _T2_CR1Y2, 1700},
    {_T2_256QAM, _T2_CR3Y5, 1940},
    {_T2_256QAM, _T2_CR2Y3, 2080},
    {_T2_256QAM, _T2_CR3Y4, 2290},
    {_T2_256QAM, _T2_CR4Y5, 2430},
    {_T2_256QAM, _T2_CR5Y6, 2510},
};

static MHal_DVBTxC_SQI_CN_NORDIGP1 SqiCnNordigP1[] =
{
    {_QPSK,  _CR1Y2, 510 },
    {_QPSK,  _CR2Y3, 690 },
    {_QPSK,  _CR3Y4, 790 },
    {_QPSK,  _CR5Y6, 890 },
    {_QPSK,  _CR7Y8, 970 },
    {_16QAM, _CR1Y2, 1080},
    {_16QAM, _CR2Y3, 1310},
    {_16QAM, _CR3Y4, 1460},
    {_16QAM, _CR5Y6, 1560},
    {_16QAM, _CR7Y8, 1600},
    {_64QAM, _CR1Y2, 1650},
    {_64QAM, _CR2Y3, 1870},
    {_64QAM, _CR3Y4, 2020},
    {_64QAM, _CR5Y6, 2160},
    {_64QAM, _CR7Y8, 2250},
};

static MS_S64 fViterbiBerFilteredDVBT_E7 = -1;
static U32 u32FecFirstLockTimeDVBT = 0;
static U8 gQamVal;
static U8 sg_bAutoSR = TRUE, sg_bAutoQAM = TRUE;

/***********************************************************************************
Subject:
Function:
Parmeter:
Return:
Remark:
 ************************************************************************************/
void MHal_DVBTxC_InitClkgen(E_SYSTEM system, E_TS_MODE ts_mode)
{
    U8 udatatemp = 0x00;
    U32 phyaddr=0, blksize=0, u32tmp;
    U32 u32MIUInterval;

    if (system == E_SYS_DVBT)
    {
        DBG_MSB(printk("@MHal_DVBTxC_InitClkgen DVBT\n"));

        MHal_Demod_WriteReg(0x103c0e,0x00);
        MHal_Demod_WriteReg(0x101e39,0x00);

        MHal_Demod_WriteReg(0x1128d0,0x01);

        MHal_Demod_ReadReg(0x112003, &udatatemp);
        MHal_Demod_WriteReg(0x112003, udatatemp&~BIT5);

        MHal_Demod_WriteReg(0x10331f,0x00);
        MHal_Demod_WriteReg(0x10331e,0x10);

        MHal_Demod_WriteReg(0x103301,0x01);
        MHal_Demod_WriteReg(0x103300,0x11);

        MHal_Demod_WriteReg(0x103309,0x00);
        MHal_Demod_WriteReg(0x103308,0x00);

        MHal_Demod_WriteReg(0x103302,0x01);
        MHal_Demod_WriteReg(0x103302,0x00);


        MHal_Demod_WriteReg(0x111f0b,0x00);
        MHal_Demod_WriteReg(0x111f0a,0x00);

        MHal_Demod_WriteReg(0x103315,0x00);
        MHal_Demod_WriteReg(0x103314,0x00);

        MHal_Demod_WriteReg(0x1128d1,0x00);
        MHal_Demod_WriteReg(0x1128d0,0x00);

        MHal_Demod_WriteReg(0x111f13,0x00);
        MHal_Demod_WriteReg(0x111f12,0x00);

        MHal_Demod_WriteReg(0x111f21,0x00);
        MHal_Demod_WriteReg(0x111f20,0x00);

        MHal_Demod_WriteReg(0x111f23,0x0c);
        MHal_Demod_WriteReg(0x111f22,0x00);

        MHal_Demod_WriteReg(0x111f25,0x04);

        MHal_Demod_WriteReg(0x111f2d,0x00);
        MHal_Demod_WriteReg(0x111f2c,0x01);

        MHal_Demod_WriteReg(0x111f2f,0x08);
        MHal_Demod_WriteReg(0x111f2e,0x00);

        MHal_Demod_WriteReg(0x111f31,0x00);
        MHal_Demod_WriteReg(0x111f30,0x00);

        MHal_Demod_WriteReg(0x111f33,0x3c);
        MHal_Demod_WriteReg(0x111f32,0x00);


        MHal_Demod_WriteReg(0x111f35,0x10);
        MHal_Demod_WriteReg(0x111f34,0x10);

        MHal_Demod_WriteReg(0x111f37,0x00);
        MHal_Demod_WriteReg(0x111f36,0x11);

        MHal_Demod_WriteReg(0x111f3b,0x00);
        MHal_Demod_WriteReg(0x111f3a,0x00);

        MHal_Demod_WriteReg(0x111f3d,0x08);
        MHal_Demod_WriteReg(0x111f3c,0x00);

        MHal_Demod_WriteReg(0x111f45,0x00);
        MHal_Demod_WriteReg(0x111f44,0x00);

        MHal_Demod_WriteReg(0x111f69,0x00);
        MHal_Demod_WriteReg(0x111f68,0x00);

        MHal_Demod_WriteReg(0x111f6b,0x00);
        MHal_Demod_WriteReg(0x111f6a,0x00);

        MHal_Demod_WriteReg(0x111f6d,0x00);
        MHal_Demod_WriteReg(0x111f6c,0x10);

        MHal_Demod_WriteReg(0x111f6f,0x0c);
        MHal_Demod_WriteReg(0x111f6e,0x40);

        MHal_Demod_WriteReg(0x111f71,0x00);
        MHal_Demod_WriteReg(0x111f70,0x00);

        MHal_Demod_WriteReg(0x111f73,0x00);
        MHal_Demod_WriteReg(0x111f72,0x00);

        MHal_Demod_WriteReg(0x111f75,0x00);
        MHal_Demod_WriteReg(0x111f74,0x00);

        MHal_Demod_WriteReg(0x111f77,0x00);
        MHal_Demod_WriteReg(0x111f76,0x00);

        MHal_Demod_WriteReg(0x111f79,0x40);
        MHal_Demod_WriteReg(0x111f78,0x00);

        MHal_Demod_WriteReg(0x111f7b,0x00);
        MHal_Demod_WriteReg(0x111f7a,0x04);

        MHal_Demod_WriteReg(0x111f7d,0x00);
        MHal_Demod_WriteReg(0x111f7c,0x00);

        MHal_Demod_WriteReg(0x111f7f,0x40);
        MHal_Demod_WriteReg(0x111f7e,0x40);

        MHal_Demod_WriteReg(0x111f83,0x00);
        MHal_Demod_WriteReg(0x111f82,0x00); 
    
        MHal_Demod_WriteReg(0x111fe1,0x00);
        MHal_Demod_WriteReg(0x111fe0,0x00);

        MHal_Demod_WriteReg(0x111ff0,0x00);

        MHal_Demod_WriteReg(0x111fe3,0x00);    
        MHal_Demod_WriteReg(0x111fe2,0x08);  	

        MHal_Demod_WriteReg(0x111fe5,0x00);
        MHal_Demod_WriteReg(0x111fe4,0x00);

        MHal_Demod_WriteReg(0x111fe7,0x00);
        MHal_Demod_WriteReg(0x111fe6,0x00);

        MHal_Demod_WriteReg(0x111fe9,0x00);
        MHal_Demod_WriteReg(0x111fe8,0x08);

        MHal_Demod_WriteReg(0x111feb,0x04);
        MHal_Demod_WriteReg(0x111fea,0x00);

        MHal_Demod_WriteReg(0x111fed,0x00);
        MHal_Demod_WriteReg(0x111fec,0x00);

        MHal_Demod_WriteReg(0x111fef,0x00);
        MHal_Demod_WriteReg(0x111fee,0x88);

        MHal_Demod_WriteReg(0x15298f,0x00);
        MHal_Demod_WriteReg(0x15298e,0x00);

        MHal_Demod_WriteReg(0x152991,0x00);
        MHal_Demod_WriteReg(0x152990,0x00);
        MHal_Demod_WriteReg(0x152992,0x00);

        MHal_Demod_WriteReg(0x1529e5,0x00);
        MHal_Demod_WriteReg(0x1529e4,0x00);


        MHal_Demod_WriteReg(0x152971,0x10);
        MHal_Demod_WriteReg(0x152970,0x01);

        MHal_Demod_WriteReg(0x152997,0x08);
        MHal_Demod_WriteReg(0x152996,0x00);
        MHal_Demod_WriteReg(0x152999,0x10);
        MHal_Demod_WriteReg(0x152998,0x10);   
        MHal_Demod_WriteReg(0x111f43,0x00);
        MHal_Demod_WriteReg(0x111f42,0x00);

        MHal_Demod_WriteReg(0x1117e0,0x23);
        MHal_Demod_WriteReg(0x1117e1,0x21);
        MHal_Demod_WriteReg(0x1117e4,0x01);
        MHal_Demod_WriteReg(0x1117e6,0x11);

        MHal_Demod_WriteReg(0x111701,0x00);
        MHal_Demod_WriteReg(0x111700,0x00);

        MHal_Demod_WriteReg(0x111705,0x00);
        MHal_Demod_WriteReg(0x111704,0x00);

        MHal_Demod_WriteReg(0x111703,0x00);
        MHal_Demod_WriteReg(0x111702,0x00);

        MHal_Demod_WriteReg(0x111707,0x7f);
        MHal_Demod_WriteReg(0x111706,0xff);

        MHal_Demod_WriteReg(0x111718,0x00);

        MHal_Demod_WriteReg(0x101e39,0x03);
        MHal_Demod_WriteReg(0x103c0e,0x01);
    }
    else if (system == E_SYS_DVBC)
    {
        DBG_MSB(printk("@MHal_DVBTxC_InitClkgen DVBC\n"));

        MHal_Demod_WriteReg(0x103c0e, 0x00);
        MHal_Demod_WriteReg(0x101E39, 0x00);

        MHal_Demod_ReadReg(0x112003, &udatatemp);
        MHal_Demod_WriteReg(0x112003, udatatemp&~BIT5); 

        MHal_Demod_WriteReg(0x1128d0, 0x01);

        MHal_Demod_WriteReg(0x10331e,0x10);

        MHal_Demod_WriteReg(0x103301,0x11);
        MHal_Demod_WriteReg(0x103300,0x13);

        MHal_Demod_WriteReg(0x103309,0x00);
        MHal_Demod_WriteReg(0x103308,0x00);


        MHal_Demod_WriteReg(0x103302,0x01);
        MHal_Demod_WriteReg(0x103302,0x00);

        MHal_Demod_WriteReg(0x103321,0x00);
        MHal_Demod_WriteReg(0x103320,0x00);

        MHal_Demod_WriteReg(0x111f0b, 0x00);
        MHal_Demod_WriteReg(0x111f0a, 0x00);

        MHal_Demod_WriteReg(0x103315, 0x00);
        MHal_Demod_WriteReg(0x103314, 0x00);

        MHal_Demod_WriteReg(0x1128d0, 0x00);

        MHal_Demod_WriteReg(0x152929,0x00);
        MHal_Demod_WriteReg(0x152928,0x04);

        MHal_Demod_WriteReg(0x152903,0x04);
        MHal_Demod_WriteReg(0x152902,0x04);

        MHal_Demod_WriteReg(0x152905,0x00);
        MHal_Demod_WriteReg(0x152904,0x00);

        MHal_Demod_WriteReg(0x152907,0x04);
        MHal_Demod_WriteReg(0x152906,0x00);

//        MHal_Demod_WriteReg(0x111f0b,0x00);
//        MHal_Demod_WriteReg(0x111f0a,0x00);

        MHal_Demod_WriteReg(0x111f23,0x08);
        MHal_Demod_WriteReg(0x111f22,0x44);

        MHal_Demod_WriteReg(0x111f3b,0x00);
        MHal_Demod_WriteReg(0x111f3a,0x00);

        MHal_Demod_WriteReg(0x111f7f,0x00);
        MHal_Demod_WriteReg(0x111f7e,0x00);

        MHal_Demod_WriteReg(0x111f71,0x00);
        MHal_Demod_WriteReg(0x111f70,0x00);

        MHal_Demod_WriteReg(0x111f73,0x00);
        MHal_Demod_WriteReg(0x111f72,0x00);

        MHal_Demod_WriteReg(0x111f69,0x88);
        MHal_Demod_WriteReg(0x111f68,0x00);

        MHal_Demod_WriteReg(0x111f4b,0x01);
        MHal_Demod_WriteReg(0x111f4a,0x11);

        MHal_Demod_WriteReg(0x152923,0x00);
        MHal_Demod_WriteReg(0x152922,0x44);

        MHal_Demod_WriteReg(0x111f25,0x04);
        MHal_Demod_WriteReg(0x111f24,0x00);

        MHal_Demod_WriteReg(0x15296d,0x00);
        MHal_Demod_WriteReg(0x15296c,0x81);

        MHal_Demod_WriteReg(0x152971,0x1c);
        MHal_Demod_WriteReg(0x152970,0xc1);

        MHal_Demod_WriteReg(0x152977,0x08);
        MHal_Demod_WriteReg(0x152976,0x08);

        MHal_Demod_WriteReg(0x152981,0x00);
        MHal_Demod_WriteReg(0x152980,0x00);

        MHal_Demod_WriteReg(0x152983,0x00);
        MHal_Demod_WriteReg(0x152982,0x00);

        MHal_Demod_WriteReg(0x152985,0x00);
        MHal_Demod_WriteReg(0x152984,0x00);

        MHal_Demod_WriteReg(0x152987,0x00);
        MHal_Demod_WriteReg(0x152986,0x00);

        MHal_Demod_WriteReg(0x111feb,0x18);
        MHal_Demod_WriteReg(0x111fea,0x14);

        MHal_Demod_WriteReg(0x111f74,0x10);

        MHal_Demod_WriteReg(0x111f77,0x01);

        MHal_Demod_WriteReg(0x111f79,0x41);
        MHal_Demod_WriteReg(0x111f78,0x10);

        MHal_Demod_WriteReg(0x111fe0,0x08);

        MHal_Demod_WriteReg(0x111fe3,0x08);
        MHal_Demod_WriteReg(0x111fe2,0x10);

        MHal_Demod_WriteReg(0x111ff0,0x08);

        MHal_Demod_WriteReg(0x111f31,0x00);

        MHal_Demod_WriteReg(0x1117e6,0x00);

        MHal_Demod_WriteReg(0x111701,0x00);
        MHal_Demod_WriteReg(0x111700,0x00);

        MHal_Demod_WriteReg(0x111705,0x00);
        MHal_Demod_WriteReg(0x111704,0x00);

        MHal_Demod_WriteReg(0x111703,0x00);
        MHal_Demod_WriteReg(0x111702,0x00);

        MHal_Demod_WriteReg(0x111707,0x7f);
        MHal_Demod_WriteReg(0x111706,0xff);

        MHal_Demod_WriteReg(0x111718,0x00);

        MHal_Demod_WriteReg(0x101E39, 0x03);

        MHal_Demod_WriteReg(0x103c0e,0x01);
    }
    else if (system == E_SYS_DVBT2)
    {
        DBG_MSB(printf("@MHal_DVBTxC_InitClkgen DVBT2\n"));

        MHal_Demod_WriteReg(0x101e39,0x00);
        MHal_Demod_WriteReg(0x1128d0, 0x01);
    
        MHal_Demod_WriteReg(0x10331f,0x00);
        //    MHal_Demod_WriteReg(0x10331e,0x1c);
        MHal_Demod_WriteReg(0x10331e,0x10);

        MHal_Demod_WriteReg(0x103301,0x01);
        MHal_Demod_WriteReg(0x103300,0x0e);

        MHal_Demod_WriteReg(0x103309,0x00);
        MHal_Demod_WriteReg(0x103308,0x00);

        MHal_Demod_WriteReg(0x103321,0x00);
        MHal_Demod_WriteReg(0x103320,0x00);

        MHal_Demod_WriteReg(0x111f0b,0x00);
        MHal_Demod_WriteReg(0x111f0a,0x00);

        MHal_Demod_WriteReg(0x103315,0x00);
        MHal_Demod_WriteReg(0x103314,0x00);
    
        MHal_Demod_WriteReg(0x1128d1, 0x00);
        MHal_Demod_WriteReg(0x1128d0, 0x00);
    
        MHal_Demod_WriteReg(0x111f21,0x11);
        MHal_Demod_WriteReg(0x111f20,0x10);

        MHal_Demod_WriteReg(0x111f23,0x01);
        MHal_Demod_WriteReg(0x111f22,0x11);

        MHal_Demod_WriteReg(0x111f25,0x04);

        MHal_Demod_WriteReg(0x111f29,0x00);
        MHal_Demod_WriteReg(0x111f28,0x00);

        MHal_Demod_WriteReg(0x111f2d,0x00);
        MHal_Demod_WriteReg(0x111f2c,0x01);

        MHal_Demod_WriteReg(0x111f2f,0x00);
        MHal_Demod_WriteReg(0x111f2e,0x00);

        MHal_Demod_WriteReg(0x111f31,0x04);
        MHal_Demod_WriteReg(0x111f30,0x00);

        MHal_Demod_WriteReg(0x111f33,0x3c);
        MHal_Demod_WriteReg(0x111f32,0x00);

        MHal_Demod_WriteReg(0x111f35,0x00);
        MHal_Demod_WriteReg(0x111f34,0x00);

        MHal_Demod_WriteReg(0x111f37,0x00);
        MHal_Demod_WriteReg(0x111f36,0x00);

        MHal_Demod_WriteReg(0x111f3b,0x00);
        MHal_Demod_WriteReg(0x111f3a,0x00);

        MHal_Demod_WriteReg(0x111f3d,0x00);
        MHal_Demod_WriteReg(0x111f3c,0x00);

        MHal_Demod_WriteReg(0x111f45,0x00);
        MHal_Demod_WriteReg(0x111f44,0x00);

        MHal_Demod_WriteReg(0x111fe1,0x00);
        MHal_Demod_WriteReg(0x111fe0,0x00);

        MHal_Demod_WriteReg(0x111ff0,0x00);

        MHal_Demod_WriteReg(0x111fe3,0x00);
        MHal_Demod_WriteReg(0x111fe2,0x00);

        MHal_Demod_WriteReg(0x111fe5,0x00);
        MHal_Demod_WriteReg(0x111fe4,0x00);

        MHal_Demod_WriteReg(0x111fe7,0x00);
        MHal_Demod_WriteReg(0x111fe6,0x00);

        MHal_Demod_WriteReg(0x111fe9,0x00);
        MHal_Demod_WriteReg(0x111fe8,0x00);

        MHal_Demod_WriteReg(0x111feb,0xc8);
        MHal_Demod_WriteReg(0x111fea,0x00);

        MHal_Demod_WriteReg(0x111fed,0x00);
        MHal_Demod_WriteReg(0x111fec,0x0c);

        MHal_Demod_WriteReg(0x111fef,0x00);
        MHal_Demod_WriteReg(0x111fee,0x00);

        MHal_Demod_WriteReg(0x111f43,0x00);
        MHal_Demod_WriteReg(0x111f42,0x00);



        MHal_Demod_WriteReg(0x111f75,0x00);
        MHal_Demod_WriteReg(0x111f74,0x00);

        MHal_Demod_WriteReg(0x111f77,0x00);
        MHal_Demod_WriteReg(0x111f76,0x00);

        MHal_Demod_WriteReg(0x111f79,0x00);
        MHal_Demod_WriteReg(0x111f78,0x00);

        MHal_Demod_WriteReg(0x111f7b,0x00);
        MHal_Demod_WriteReg(0x111f7a,0x00);

        MHal_Demod_WriteReg(0x111f7d,0x00);
        MHal_Demod_WriteReg(0x111f7c,0x00);

        MHal_Demod_WriteReg(0x111f7f,0x00);
        MHal_Demod_WriteReg(0x111f7e,0x00);

		MHal_Demod_WriteReg(0x111f83,0x00);
        MHal_Demod_WriteReg(0x111f82,0x00);

        MHal_Demod_WriteReg(0x152971,0x10);
        MHal_Demod_WriteReg(0x152970,0x01);
    
        MHal_Demod_WriteReg(0x111f72,0x01);
    
        MHal_Demod_WriteReg(0x152997, 0x08);
        MHal_Demod_WriteReg(0x152996, 0x00);

        MHal_Demod_WriteReg(0x152999, 0x10);
        MHal_Demod_WriteReg(0x152998, 0x10);

        MHal_Demod_WriteReg(0x1117e0,(0x21+INTERNAL_DVBT2_PS2XDATA_LEN-1));
        MHal_Demod_WriteReg(0x1117e1,0x21);
        MHal_Demod_WriteReg(0x1117e4,(INTERNAL_DVBT2_PS2XDATA_LEN-2));
        MHal_Demod_WriteReg(0x1117e6,0x11);

        MHal_Demod_WriteReg(0x111701,0x00);
        MHal_Demod_WriteReg(0x111700,0x00);

        MHal_Demod_WriteReg(0x111705,0x00);
        MHal_Demod_WriteReg(0x111704,0x00);

        MHal_Demod_WriteReg(0x111703,0x00);
        MHal_Demod_WriteReg(0x111702,0x00);

        MHal_Demod_WriteReg(0x111707,(INTERNAL_DVBT2_DRAM_OFFSET-1)>>8);
        MHal_Demod_WriteReg(0x111706,(INTERNAL_DVBT2_DRAM_OFFSET-1)&0xff);

        u32MIUInterval = MS_MIU_INTERVAL;
        phyaddr = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_ADDR];
        blksize = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_LEN];

        MHal_Demod_ReadReg(0x112000, &udatatemp);
        if (u32MIUInterval & phyaddr)
        {
            MHal_Demod_WriteReg(0x112000,(udatatemp|0x03));
            phyaddr &= ~u32MIUInterval;
        }
        else
        {
            MHal_Demod_WriteReg(0x112000,(udatatemp&~0x03));
        }

        u32tmp = (phyaddr & 0xFFFF);
        if (u32tmp > INTERNAL_DVBT2_DRAM_OFFSET)
            phyaddr = phyaddr - u32tmp + 0x10000 + INTERNAL_DVBT2_DRAM_OFFSET;
        else
            phyaddr = phyaddr - u32tmp + INTERNAL_DVBT2_DRAM_OFFSET;

        MHal_Demod_WriteReg(0x11171b,(U8)(phyaddr>>24));
        MHal_Demod_WriteReg(0x11171a,(U8)(phyaddr>>16));

        MHal_Demod_WriteReg(0x111709,0x00);
        MHal_Demod_WriteReg(0x111708,0x00);

        MHal_Demod_WriteReg(0x11170d,INTERNAL_DVBT2_DRAM_OFFSET>>8);
        MHal_Demod_WriteReg(0x11170c,INTERNAL_DVBT2_DRAM_OFFSET&0xff);

        MHal_Demod_WriteReg(0x11170b,0x00);
        MHal_Demod_WriteReg(0x11170a,0x00);

        MHal_Demod_WriteReg(0x11170f,0xff);
        MHal_Demod_WriteReg(0x11170e,0xff);

        MHal_Demod_WriteReg(0x111718,0x04);

        MHal_Demod_WriteReg(0x11171c,0x01);

        MHal_Demod_WriteReg(0x101e39,0x03);

        MHal_Demod_WriteReg(0x103c0e,0x01);
    }
#ifdef DEMOD_DVB_T2_MERGE_T
    else if (system == E_SYS_DVBT_T2)
    {
        DBG_MSB(printf("@MHal_DVBTxC_InitClkgen DVBT_T2\n"));

        MHal_Demod_WriteReg(0x101e39, 0x00);


        MHal_Demod_WriteReg(0x10331f, 0x00);
        MHal_Demod_WriteReg(0x10331e, 0x10);

        MHal_Demod_WriteReg(0x103301, 0x01);
        MHal_Demod_WriteReg(0x103300, 0x11);

        MHal_Demod_WriteReg(0x103309, 0x00);
        MHal_Demod_WriteReg(0x103308, 0x00);


        MHal_Demod_WriteReg(0x103315, 0x00);
        MHal_Demod_WriteReg(0x103314, 0x00);

        MHal_Demod_WriteReg(0x103321, 0x00);
        MHal_Demod_WriteReg(0x103320, 0x00);


        MHal_Demod_WriteReg(0x111f0b, 0x00);
        MHal_Demod_WriteReg(0x111f0a, 0x00);

        MHal_Demod_WriteReg(0x111f21, 0x11);
        MHal_Demod_WriteReg(0x111f20, 0x10);

        MHal_Demod_WriteReg(0x111f23, 0x01);
        MHal_Demod_WriteReg(0x111f22, 0x11);

        MHal_Demod_WriteReg(0x111f25, 0x04);

        MHal_Demod_WriteReg(0x111f29, 0x00);
        MHal_Demod_WriteReg(0x111f28, 0x00);

        MHal_Demod_WriteReg(0x111f2d, 0x00);
        MHal_Demod_WriteReg(0x111f2c, 0x01);

        MHal_Demod_WriteReg(0x111f2f, 0x00);
        MHal_Demod_WriteReg(0x111f2e, 0x00);

        MHal_Demod_WriteReg(0x111f31, 0x04);
        MHal_Demod_WriteReg(0x111f30, 0x00);

        MHal_Demod_WriteReg(0x111f33, 0x3c);
        MHal_Demod_WriteReg(0x111f32, 0x00);

        MHal_Demod_WriteReg(0x111f35, 0x00);
        MHal_Demod_WriteReg(0x111f34, 0x00);

        MHal_Demod_WriteReg(0x111f3b, 0x00);
        MHal_Demod_WriteReg(0x111f3a, 0x00);

        MHal_Demod_WriteReg(0x111f3d, 0x00);
        MHal_Demod_WriteReg(0x111f3c, 0x00);

        MHal_Demod_WriteReg(0x111f45, 0x00);
        MHal_Demod_WriteReg(0x111f44, 0x00);

        MHal_Demod_WriteReg(0x111fe1, 0x00);
        MHal_Demod_WriteReg(0x111fe0, 0x00);

        MHal_Demod_WriteReg(0x111ff0, 0x00);

        MHal_Demod_WriteReg(0x111fe3, 0x00);
        MHal_Demod_WriteReg(0x111fe2, 0x00);


        MHal_Demod_WriteReg(0x111fe5, 0x00);
        MHal_Demod_WriteReg(0x111fe4, 0x00);

        MHal_Demod_WriteReg(0x111fe7, 0x00);
        MHal_Demod_WriteReg(0x111fe6, 0x00);

        MHal_Demod_WriteReg(0x111fe9, 0x00);
        MHal_Demod_WriteReg(0x111fe8, 0x00);

        MHal_Demod_WriteReg(0x111feb, 0xc8);
        MHal_Demod_WriteReg(0x111fea, 0x00);

        MHal_Demod_WriteReg(0x111fed, 0x00);
        MHal_Demod_WriteReg(0x111fec, 0x0c);

        MHal_Demod_WriteReg(0x111fef, 0x00);
        MHal_Demod_WriteReg(0x111fee, 0x00);

        MHal_Demod_WriteReg(0x111f43, 0x00);
        MHal_Demod_WriteReg(0x111f42, 0x00);

        MHal_Demod_WriteReg(0x111f75, 0x00);
        MHal_Demod_WriteReg(0x111f74, 0x00);

        MHal_Demod_WriteReg(0x111f77, 0x00);
        MHal_Demod_WriteReg(0x111f76, 0x00);

        MHal_Demod_WriteReg(0x111f79, 0x00);
        MHal_Demod_WriteReg(0x111f78, 0x00);

        MHal_Demod_WriteReg(0x111f7b, 0x00);
        MHal_Demod_WriteReg(0x111f7a, 0x00);

        MHal_Demod_WriteReg(0x111f7d, 0x00);
        MHal_Demod_WriteReg(0x111f7c, 0x00);

        MHal_Demod_WriteReg(0x111f7f, 0x00);
        MHal_Demod_WriteReg(0x111f7e, 0x00);

        MHal_Demod_WriteReg(0x111f83, 0x00);
        MHal_Demod_WriteReg(0x111f82, 0x00);

        MHal_Demod_WriteReg(0x152971, 0x10);
        MHal_Demod_WriteReg(0x152970, 0x01);
        MHal_Demod_WriteReg(0x111f72, 0x01);

        MHal_Demod_WriteReg(0x152997, 0x08);
        MHal_Demod_WriteReg(0x152996, 0x00);

        MHal_Demod_WriteReg(0x152999, 0x10);
        MHal_Demod_WriteReg(0x152998, 0x10);

        MHal_Demod_WriteReg(0x152991, 0x00);
        MHal_Demod_WriteReg(0x152990, 0x00);

        MHal_Demod_WriteReg(0x15298f, 0x00);
        MHal_Demod_WriteReg(0x15298e, 0x00);

        MHal_Demod_WriteReg(0x152992, 0x00);

        MHal_Demod_WriteReg(0x1117e0,(0x21+INTERNAL_DVBT2_PS2XDATA_LEN-1));
        MHal_Demod_WriteReg(0x1117e1,0x21);
        MHal_Demod_WriteReg(0x1117e4,(INTERNAL_DVBT2_PS2XDATA_LEN-2));
        MHal_Demod_WriteReg(0x1117e6,0x11);

        MHal_Demod_WriteReg(0x111701,0x00);
        MHal_Demod_WriteReg(0x111700,0x00);

        MHal_Demod_WriteReg(0x111705,0x00);
        MHal_Demod_WriteReg(0x111704,0x00);

        MHal_Demod_WriteReg(0x111703,0x00);
        MHal_Demod_WriteReg(0x111702,0x00);

        MHal_Demod_WriteReg(0x111707,(INTERNAL_DVBT2_DRAM_OFFSET-1)>>8);
        MHal_Demod_WriteReg(0x111706,(INTERNAL_DVBT2_DRAM_OFFSET-1)&0xff);

        u32MIUInterval = MS_MIU_INTERVAL;
        phyaddr = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_ADDR];
        blksize = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_LEN];

        MHal_Demod_ReadReg(0x112000, &udatatemp);
        if (u32MIUInterval & phyaddr)
        {
            MHal_Demod_WriteReg(0x112000,(udatatemp|0x03));
            phyaddr &= ~u32MIUInterval;
        }
        else
        {
            MHal_Demod_WriteReg(0x112000,(udatatemp&~0x03));
        }

        u32tmp = (phyaddr & 0xFFFF);
        if (u32tmp > INTERNAL_DVBT2_DRAM_OFFSET)
            phyaddr = phyaddr - u32tmp + 0x10000 + INTERNAL_DVBT2_DRAM_OFFSET;
        else
            phyaddr = phyaddr - u32tmp + INTERNAL_DVBT2_DRAM_OFFSET;

        MHal_Demod_WriteReg(0x11171b,(U8)(phyaddr>>24));
        MHal_Demod_WriteReg(0x11171a,(U8)(phyaddr>>16));

        MHal_Demod_WriteReg(0x111709,0x00);
        MHal_Demod_WriteReg(0x111708,0x00);

        MHal_Demod_WriteReg(0x11170d,INTERNAL_DVBT2_DRAM_OFFSET>>8);
        MHal_Demod_WriteReg(0x11170c,INTERNAL_DVBT2_DRAM_OFFSET&0xff);

        MHal_Demod_WriteReg(0x11170b,0x00);
        MHal_Demod_WriteReg(0x11170a,0x01);

        MHal_Demod_WriteReg(0x11170f,0xff);
        MHal_Demod_WriteReg(0x11170e,0xff);

        MHal_Demod_WriteReg(0x111718, 0x04);

        MHal_Demod_WriteReg(0x11171c, 0x00);

        MHal_Demod_WriteReg(0x101e39, 0x03);
        MHal_Demod_WriteReg(0x103c0e, 0x01);
    }
#endif // DEMOD_DVB_T2_MERGE_T

}

/***********************************************************************************
Subject:
Function:
Parmeter:
Return:
Remark:
 ************************************************************************************/
B16 MHal_DVBTxC_Download(E_SYSTEM eSystems)
{
    DEMOD_DSP_PARAM_t stParam;
    U32 phyaddr=0, blksize=0, u32tmp;

    if(eSystems == E_SYS_DVBT)
    {
        stParam.eDSP_Type = DEMOD_STANDARD_DVBT;

        DBG_MSB(printk("MHal_DVBTxC_Download : E_SYS_DVBT !! \n"));

        if (MHal_Demod_LoadDSPCode(stParam) == TRUE)
            return TRUE;
    }
    else
    if(eSystems == E_SYS_DVBC)
    {
        stParam.eDSP_Type = DEMOD_STANDARD_DVBC;

        DBG_MSB(printk("MHal_DVBTxC_Download : E_SYS_DVBC !! \n"));

        if (MHal_Demod_LoadDSPCode(stParam) == TRUE)
            return TRUE;
    }
#ifdef DEMOD_DVB_T2_MERGE_T
    else if ((eSystems == E_SYS_DVBT2) || (eSystems == E_SYS_DVBT_T2))
#else
    else if (eSystems == E_SYS_DVBT2)
#endif
    {
        stParam.eDSP_Type = DEMOD_STANDARD_DVBT2;

        DBG_MSB(printk("MHal_DVBTxC_Download : E_SYS_DVBT2 !! \n"));

#if 1 // TODO: need get MMAP in kernel
        phyaddr = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_ADDR];
        blksize = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_LEN];
#else
        MAdp_MPool_GetBlockPa(E_SYS_MMAP_ISDBT_TDI, &phyaddr, &blksize);
#endif

        // u32FwStartAddr must end with 0x8000 >>>>>
        u32tmp = (phyaddr & 0xFFFF);
        if (u32tmp > INTERNAL_DVBT2_DRAM_OFFSET)
            stParam.sDVBT2InitData.u32FwStartAddr = phyaddr - u32tmp + 0x10000 + INTERNAL_DVBT2_DRAM_OFFSET;
        else
            stParam.sDVBT2InitData.u32FwStartAddr = phyaddr - u32tmp + INTERNAL_DVBT2_DRAM_OFFSET;
        // <<<<< u32FwStartAddr must end with 0x8000

        stParam.sDVBT2InitData.u32TdiStartAddr = stParam.sDVBT2InitData.u32FwStartAddr + INTERNAL_DVBT2_FW_LEN;
        stParam.sDVBT2InitData.u32EqStartAddr = stParam.sDVBT2InitData.u32TdiStartAddr + INTERNAL_DVBT2_TDI_LEN;
        stParam.sDVBT2InitData.u32DjbStartAddr = stParam.sDVBT2InitData.u32EqStartAddr + INTERNAL_DVBT2_EQ_LEN;

        if ((stParam.sDVBT2InitData.u32DjbStartAddr + INTERNAL_DVBT2_DJB_LEN) > (phyaddr + blksize))
        {
            DBG_MSB(printk("MHal_DVBTxC_Download : memory use out of range !! \n"));
            return FALSE;
        }

        if (MHal_Demod_LoadDSPCode(stParam) == TRUE)
            return TRUE;
    }
    else
    {
        DBG_MSB(printk("MHal_DVBTxC_Download : No defied system type !! \n"));
    }

    return FALSE;
}

/***********************************************************************************
Subject:
Function:
Parmeter:
Return:
Remark:
 ************************************************************************************/
B16 MHal_DVBTxC_Reset (void)
{
    U8 i, u8tmp;

    DBG_MSB(printk(" @MHal_DVBTxC_Reset\n"));

    MHal_Demod_WriteReg(DMDMcuBase, 0x02); // reset RIU remapping reset
    MHal_Demod_WriteReg(DMDMcuBase, 0x03); // reset DMD_MCU
    MHal_Demod_WriteReg(MBRegBase + 0x00, 0xff); // for handshake

    mdelay(5);

    MHal_Demod_WriteReg(DMDMcuBase, 0x00);

    for (i=0 ; i<50 ; i++)
    {
        MHal_Demod_ReadReg(MBRegBase + 0x00, &u8tmp);
        if (u8tmp != 0xff)
        {
            DBG_MSB(printk("@MHal_DVBTxC_Reset handshake done %d\n", i));
            break;
        }
        udelay(1000);
    }

    if(i == 50)
    {
        printk("@MHal_DVBTxC_Reset handshake timeout\n");
        return FALSE;
    }

#ifdef SUPPORT_ADAPTIVE_TS_CLK
    g_dvb_lock = 0;
#endif
    return TRUE;
}

/***********************************************************************************
Subject:
Function:
Parmeter:
Return:
Remark:
 ************************************************************************************/
B16 MHal_DVBTxC_Get_Version(U16 *ver_bcd)
{
    U8      status = TRUE;
    U8      data1 = 0,data2 = 0;

    status &= MHal_Demod_MB_ReadReg(0x20C1, &data1);
    *ver_bcd = (U16)data1 << 8;

    status &= MHal_Demod_MB_ReadReg(0x20C2, &data2);
    *ver_bcd |= data2;

    DBG_MSB(printk("MHal_DVBTxC FW_VERSION:%x.%x\n",data1,data2));

    if(status)
        return TRUE;
    else
        return FALSE;

}

/***********************************************************************************
Subject:
Function:
Parmeter:
Return:
Remark:
 ************************************************************************************/
static B16 MHal_DVBTxC_DVBT_DSPReg_Init(E_TUNER_TYPE tuner_type)
{
    U8      idx = 0;

    if(tuner_type == E_TUNER_SI2178 || tuner_type == E_TUNER_SI2158)    // EU Model
    {
        for (idx = 0; idx<sizeof(MHal_DVBTxC_DVBT_DSPREG_SI2176_8M); idx++)
        {
            if(MHal_Demod_MB_WriteDspReg(idx, MHal_DVBTxC_DVBT_DSPREG_SI2176_8M[idx]) != TRUE)
            {
                DBG_MSB(printk("dsp reg init NG : E_TUNER_SI2178\n"));
                return FALSE;
            }
        }
        DBG_MSB(printk("dsp reg init ok : E_TUNER_SI2178\n"));
    }
    else if (tuner_type == E_TUNER_SI2176)  // TW Model
    {
        for (idx = 0; idx<sizeof(MHal_DVBTxC_DVBT_DSPREG_SI2176_6M); idx++)
        {
            if( MHal_Demod_MB_WriteDspReg(idx, MHal_DVBTxC_DVBT_DSPREG_SI2176_6M[idx]) != TRUE)
            {
                DBG_MSB(printk("dsp reg init NG : E_TUNER_SI2176\n"));
                return FALSE;
            }
        }
        DBG_MSB(printk("dsp reg init ok : E_TUNER_SI2176\n"));
    }
    else
    {
        DBG_MSB(printk("Error : E_TUNER_UNKOWN\n"));
        return FALSE;
    }

    return TRUE;
}

/***********************************************************************************
Subject:
Function:
Parmeter:
Return:
Remark:
 ************************************************************************************/
static B16 MHal_DVBTxC_DVBC_DSPReg_Init(E_TUNER_TYPE tuner_type)
{
    U8      idx = 0;

    if(tuner_type == E_TUNER_SI2178 || tuner_type == E_TUNER_SI2158 || tuner_type == E_TUNER_SI2176)
    {
        for (idx = 0; idx<sizeof( MHal_DVBTxC_DVBC_DSPREG_SI2176); idx++)
        {
            if( MHal_Demod_MB_WriteDspReg(idx, MHal_DVBTxC_DVBC_DSPREG_SI2176[idx])!=TRUE)
            {
                DBG_MSB(printk("dsp reg init NG : E_TUNER_SI2176\n"));
                return FALSE;
            }
        }
        DBG_MSB(printk("dsp reg init ok : E_TUNER_SI2176\n"));
    }
    else
    {
        DBG_MSB(printk("Error : E_TUNER_UNKOWN\n"));
    }

    return TRUE;
}

/***********************************************************************************
Subject:
Function:
Parmeter:
Return:
Remark:
 ************************************************************************************/
static B16 MHal_DVBTxC_DVBT2_DSPReg_Init(E_TUNER_TYPE tuner_type)
{
    tuner_type = tuner_type;
    DBG_MSB(printk("MHal_DVBTxC_DVBT2_DSPReg_Init\n"));

    if(MHal_Demod_MB_WriteDspReg((U32)T2_BW, T2_BW_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }
    if(MHal_Demod_MB_WriteDspReg((U32)T2_FC_L, T2_FC_L_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }
    if(MHal_Demod_MB_WriteDspReg((U32)T2_FC_H, T2_FC_H_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }
    if(MHal_Demod_MB_WriteDspReg((U32)T2_TS_SERIAL, T2_TS_SERIAL_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }
    if(MHal_Demod_MB_WriteDspReg((U32)T2_TS_OUT_INV, T2_TS_OUT_INV_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }
    if(MHal_Demod_MB_WriteDspReg((U32)T2_TS_DATA_SWAP, T2_TS_DATA_SWAP_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }
    if(MHal_Demod_MB_WriteDspReg((U32)T2_IF_AGC_INV_PWM_EN, T2_IF_AGC_INV_PWM_EN_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }
    if(MHal_Demod_MB_WriteDspReg((U32)T2_LITE, T2_LITE_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }
    if(MHal_Demod_MB_WriteDspReg((U32)T2_AGC_REF, T2_AGC_REF_VAL) != TRUE)
    {
        printk("MHal_DVBTxC_DVBT2_DSPReg_Init NG\n"); return FALSE;
    }

    return TRUE;
}


/***********************************************************************************
Subject:
Function:
Parmeter:
Return:
Remark:
 ************************************************************************************/
B16 MHal_DVBTxC_System_Init(E_SYSTEM system, E_TUNER_TYPE tuner_type)
{
    U8          status = TRUE;

    DBG_MSB(printk("MHal_DVBTxC_System_Init=[%d][%d]\n",system,tuner_type));
    status &= MHal_DVBTxC_Reset();

    if(system == E_SYS_DVBT)
    {
        status &= MHal_DVBTxC_DVBT_DSPReg_Init(tuner_type);
    }
    else if(system == E_SYS_DVBC)
    {
        status &= MHal_DVBTxC_DVBC_DSPReg_Init(tuner_type);
    }
#ifdef DEMOD_DVB_T2_MERGE_T
    else if ((system == E_SYS_DVBT2) || (system == E_SYS_DVBT_T2))
#else
    else if (system == E_SYS_DVBT2)
#endif
    {
        status &= MHal_DVBTxC_DVBT2_DSPReg_Init(tuner_type);
    }

    status &= MHal_Demod_MB_WriteReg(0x20D6, 0x00);

    if(status)
    {
        return TRUE;
    }
    else
    {
        DBG_MSB(printk("MHal_DVBTxC_System_Init NG \n"));
        return FALSE;
    }
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
B16 MHal_DVBTxC_Config_DVBT(U16 u16ChBw, B16 bSetLp, B16 bForce, B16 bAutoScan, U16 u16FFTMode, U16 u16CP)
{
    U8              bandwidth;
    U8              status = TRUE;
    static B16      reg_force = 0;
    static B16      reg_auto_scan = 0;

    DBG_MSB(printk(" @MHal_DVBTxC_config DVBT start = [%d][%d][%d][%d][%d][%d]\n",u16ChBw,bSetLp,bForce,bAutoScan,u16FFTMode,u16CP));

    switch(u16ChBw)
    {
        case 6000:
            bandwidth = 1;
            break;
        case 7000:
            bandwidth = 2;
            break;
        case 8000:
        default:
            bandwidth = 3;
            break;
    }

    status &= MHal_DVBTxC_Reset();


    // Bandwidth: 0:5MHz, 1:6MHz, 2:7MHz, 3:8MHz

    if( MHal_Demod_MB_WriteDspReg(0x19, bandwidth) != TRUE)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Config_DVBT BW seting NG\n"));
        return FALSE;
    }

    // LP select: 0: select HP, 1: setlect LP
    if( MHal_Demod_MB_WriteDspReg(0x1C, bSetLp)!= TRUE)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Config_DVBT LP seting NG\n"));
        return FALSE;
    }

    if (bForce != reg_force)
    {
        reg_force = bForce;

        // mode-cp force: 0: auto, 1: force
        if( MHal_Demod_MB_WriteDspReg(0x0B, (U8)bForce)!= TRUE)
        {
            DBG_MSB(printk(" @MHalDVBTxC_Config_DVBT MODE-CP seting NG\n"));
            return FALSE;
        }
        // FFT mode - 0:2K, 1:8K
        if( MHal_Demod_MB_WriteDspReg(0x1a, (U8)u16FFTMode)!= TRUE)
        {
            DBG_MSB(printk(" @MHal_DVBTxC_Config_DVBT FFT seting NG\n"));
            return FALSE;
        }
        // CP - 0:1/32, 1/16, 1/8, 1/4
        if( MHal_Demod_MB_WriteDspReg(0x1b, (U8)u16CP)!= TRUE)
        {
            DBG_MSB(printk(" @MHal_DVBTxC_Config_DVBT CP seting NG\n"));
            return FALSE;
        }
    }

    if (bAutoScan != reg_auto_scan)
    {
        reg_auto_scan = bAutoScan;

        // Auto Scan - 0:channel change, 1:auto-scan
        if( MHal_Demod_MB_WriteDspReg(0x0D, (U8)bAutoScan)!= TRUE)
        {
            DBG_MSB(printk(" @MHal_DVBTxC_Config_DVBT auto mode seting NG\n"));
            return FALSE;
        }
    }


    if(status)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Config_DVBT OK\n"));
        return  TRUE;
    }
    else
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Config_DVBT NG\n"));
        return FALSE;
    }
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
static void INTERN_DVBT_SignalQualityReset(void)
{
    fViterbiBerFilteredDVBT_E7 = -1;
    u32FecFirstLockTimeDVBT = (U32)jiffies_to_msecs(jiffies);
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
B16 MHal_DVBTxC_Active(B16 bEnable)
{
    U8 Data = 0;

    bEnable = bEnable;
    DBG_MSB(printk("MHal_DVBTxC_Active\n",Data));
    MHal_Demod_WriteReg(0x101e39,0x03);

    if(MHal_Demod_MB_WriteReg(0x2f1c,0x01))
    {
        MHal_Demod_MB_ReadReg(0x2f1c,&Data);

        INTERN_DVBT_SignalQualityReset();
        return TRUE;
    }

    return FALSE;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
#ifdef DEMOD_DVB_T2_MERGE_T
U8 MHal_DVBTxC_Config_DVBT2(DMD_DVBT2_RF_CHANNEL_BANDWIDTH BW, U32 u32IFFreq, U8 u8PlpID, U8 Channel_Switch, U8 bSetLp)
#else
U8 MHal_DVBTxC_Config_DVBT2(DMD_DVBT2_RF_CHANNEL_BANDWIDTH BW, U32 u32IFFreq, U8 u8PlpID, U8 Channel_Switch)
#endif
{
    U8   status = TRUE;

    DBG_MSB(printk(" @MHal_DVBTxC_DVBT2_config BW:%d IFFreq:%d PlpID:%d\n", BW, u32IFFreq, u8PlpID));

    MHal_DVBTxC_Reset();

#ifdef DEMOD_DVB_T2_MERGE_T
    // set default TS clock 8.47MHz
    MHal_DVBTxC_Change_TS_Clock(0x01, 0x10);

    // LP select: 0: select HP, 1: setlect LP
    status &= (MHal_Demod_MB_WriteDspReg(0x1C, bSetLp) == TRUE);
#endif

    // BW mode
    status &= (MHal_Demod_MB_WriteDspReg(T2_BW, BW) == TRUE);

    // FC
    status &= (MHal_Demod_MB_WriteDspReg(T2_FC_L, u32IFFreq&0xff) == TRUE);
    status &= (MHal_Demod_MB_WriteDspReg(T2_FC_H, (u32IFFreq>>8)&0xff) == TRUE);

    // PLP_ID
    status &= (MHal_Demod_MB_WriteDspReg(T2_PLP_ID, u8PlpID) == TRUE);

    // Channel Switch
    status &= (MHal_Demod_MB_WriteDspReg(T2_Channel_Switch, Channel_Switch) == TRUE);

    return status;
}

//===================================================================
//Function:
//Parmeter:
//u8Data:
//Return:
//Remark:
//===================================================================
static void MHal_DVBTxC_Change_TS_Clock(U8 clk_sel, U8 div_num)
{
    U8 reg1, reg2;

    clk_sel &= 0x07;

    MHal_Demod_ReadReg(0x103300, &reg1);
    MHal_Demod_ReadReg(0x103301, &reg2);
    if (((reg2 & 0x40) != 0x40) // phase tuning not enable yet
        || ((reg2 & 0x07) != clk_sel) // clock selection change
        || (reg1 != div_num)) // division number change
    {
        MHal_Demod_ReadReg(0x103302, &reg2);
        MHal_Demod_WriteReg(0x103302, (reg2 | 0x01)); // disbale TS ouptut

        MHal_Demod_WriteReg(0x103300, div_num); //set TS clock div

//        MHal_Demod_WriteReg(0x10330b, (div_num >> 1)); // set phase tuning number for 90 and 270 degree (div_num/2)
        MHal_Demod_WriteReg(0x10330b, 0x3); // set phase tuning number = 3

        MHal_Demod_ReadReg(0x103301, &reg1);
        reg1 &= 0xf0;
        reg1 |= (clk_sel | 0x40); // select clock and enable phase tuning
        MHal_Demod_WriteReg(0x103301, reg1);

        MHal_Demod_WriteReg(0x103302, (reg2 & ~0x01));//enable TS ouptut
    }
}

#ifdef SUPPORT_ADAPTIVE_TS_CLK
/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
static BOOL INTERN_DVB_Adaptive_TS_CLK(E_SYSTEM system)
{
	U8  u8_ts_clk=0x00;
    U8  TS_Clock_Temp;
    U8  CLK_source=0;

	MHal_Demod_ReadReg(0x112615, &u8_ts_clk);
    CLK_source=(u8_ts_clk>>6);
    u8_ts_clk=u8_ts_clk&0x1F;

	MHal_Demod_ReadReg(DMD_CLK_GEN+0x02, &TS_Clock_Temp);
	TS_Clock_Temp=TS_Clock_Temp|0x01;
	MHal_Demod_WriteReg(DMD_CLK_GEN+0x02,TS_Clock_Temp);

    if (system == E_SYS_DVBC)
    {
        if (CLK_source==0)
            MHal_DVBTxC_Change_TS_Clock(0, u8_ts_clk);
        else
            MHal_DVBTxC_Change_TS_Clock(1, u8_ts_clk);
    }
    else
    {
        MHal_DVBTxC_Change_TS_Clock(0, u8_ts_clk);
    }
		
	MHal_Demod_ReadReg(DMD_CLK_GEN+0x02, &TS_Clock_Temp);
	TS_Clock_Temp=(TS_Clock_Temp&0xFE);
	MHal_Demod_WriteReg(DMD_CLK_GEN+0x02,TS_Clock_Temp);

	MHal_Demod_MB_WriteReg(BACKEND_REG_BASE + (0x16*2+1), 0x15);

	MHal_Demod_MB_ReadReg(BACKEND_REG_BASE + (0x10*2), &TS_Clock_Temp);
	TS_Clock_Temp=TS_Clock_Temp|0x01;
	MHal_Demod_MB_WriteReg(BACKEND_REG_BASE + (0x10*2), TS_Clock_Temp);

    //debug: re-check ts clock
	MHal_Demod_ReadReg(DMD_CLK_GEN, &TS_Clock_Temp);
	TS_Clock_Temp=(TS_Clock_Temp&0x1F) ;

    return TRUE;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
static BOOL INTERN_DVB_Locked_Task(E_SYSTEM system)
{
	INTERN_DVB_Adaptive_TS_CLK(system);

	//extension task
	{

	}

	return TRUE;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
static BOOL INTERN_DVBT2_GetTsDivNum(U8* u8TSDivNum)
{
    int TS_DATA_RATE =0;
    U8 u8_tmp =0;
    MS_S64 dividend64, divisor64;

    if (MHal_Demod_MB_ReadDspReg(T2_TS_DATA_RATE_3, &u8_tmp) != TRUE)
    {
        DBG_MSB(printk(">INTERN_DVBT2_GetLock MBX_ReadDspReg fail \n"));
        return FALSE;
    }
    TS_DATA_RATE = u8_tmp;

    if (MHal_Demod_MB_ReadDspReg(T2_TS_DATA_RATE_2, &u8_tmp) != TRUE)
    {
        DBG_MSB(printk(">INTERN_DVBT2_GetLock MBX_ReadDspReg fail \n"));
        return FALSE;
    }
    TS_DATA_RATE = (TS_DATA_RATE<<8) |u8_tmp;

    if (MHal_Demod_MB_ReadDspReg(T2_TS_DATA_RATE_1, &u8_tmp) != TRUE)
    {
        DBG_MSB(printk(">INTERN_DVBT2_GetLock MBX_ReadDspReg fail \n"));
        return FALSE;
    }
    TS_DATA_RATE = (TS_DATA_RATE<<8) |u8_tmp;

    if (MHal_Demod_MB_ReadDspReg(T2_TS_DATA_RATE_0, &u8_tmp) != TRUE)
    {
        DBG_MSB(printk(">INTERN_DVBT2_GetLock MBX_ReadDspReg fail \n"));
        return FALSE;
    }
    TS_DATA_RATE = (TS_DATA_RATE<<8) |u8_tmp;

    DBG_AUTO_TS_DATA_RATE(printk("[dvbt2] TS_DATA_RATE_total = 0x%x   %d \n\n", TS_DATA_RATE, TS_DATA_RATE));


#ifdef DEMOD_DVB_T2_MERGE_T
    dividend64 = (MS_S64)288000000 * 4 * 100;
    divisor64 = (MS_S64)TS_DATA_RATE * TS_DATA_RATE_RATIO;
    *u8TSDivNum = div64_s64(dividend64, divisor64) - 1;
    DBG_AUTO_TS_DATA_RATE(printk(" CLK Source: 288 MHz \n"));
#else
    MHal_Demod_ReadReg(0x103301,&u8_tmp);
    u8_tmp &= 0x07;

    if(u8_tmp == 0x00)
    {
        dividend64 = (MS_S64)172000000 * 4 * 100;
        divisor64 = (MS_S64)TS_DATA_RATE * TS_DATA_RATE_RATIO;
        *u8TSDivNum = div64_s64(dividend64, divisor64) - 1;
        DBG_AUTO_TS_DATA_RATE(printk(" CLK Source: 172 MHz \n"));
    }
    else if(u8_tmp == 0x01)
    {
        dividend64 = (MS_S64)288000000 * 4 * 100;
        divisor64 = (MS_S64)TS_DATA_RATE * TS_DATA_RATE_RATIO;
        *u8TSDivNum = div64_s64(dividend64, divisor64) - 1;
        DBG_AUTO_TS_DATA_RATE(printk(" CLK Source: 288 MHz \n"));
    }
    else
    {
        printk("Error: unknown closk source\n");
    }
#endif

    if (*u8TSDivNum > 0x1f)
        *u8TSDivNum = 0x1f;

    if (*u8TSDivNum < 0x0e)
        *u8TSDivNum = 0x0e;

    DBG_AUTO_TS_DATA_RATE(printk(">>>INTERN_DVBT2_GetTsDivNum = 0x%x<<<\n", *u8TSDivNum));

    return TRUE;
}
#endif
//===================================================================
//Function:
//Parmeter:
//u8Data:
//Return:
//Remark:
//===================================================================
S32 MHal_DVBTxC_SetTSClkRate(S32 ts_rate)
{
    U8 div172, div288;
    MS_S32 rate172, rate288, s32tmp;
    MS_S64 dividend64, divisor64;

    dividend64 = 864000000;
    divisor64 = (MS_S64)ts_rate * 5 * 2;
    s32tmp = div64_s64(dividend64, divisor64) - 1;
    if (s32tmp < 0)
        s32tmp = 0;
    div172 = (U8)s32tmp;
    if (div172 > 0x1f)
        div172 = 0x1f;
        
    dividend64 = 864000000;
    divisor64 = (MS_S64)ts_rate *3 * 2;
    s32tmp = div64_s64(dividend64, divisor64) - 1;
    if (s32tmp < 0)
        s32tmp = 0;
    div288 = (U8)s32tmp;
    if (div288 > 0x1f)
        div288 = 0x1f;

    dividend64 = 864000000;
    divisor64 = (MS_S64)(div172 + 1) * 5 * 2;
    rate172 = div64_s64(dividend64, divisor64);
    divisor64 = (MS_S64)(div288 + 1) * 3 * 2;
    rate288 = div64_s64(dividend64, divisor64);

    if ((rate172 - ts_rate) < (rate288 - ts_rate))
    {
        MHal_DVBTxC_Change_TS_Clock(0x00, div172);
        return rate172;
    }

    MHal_DVBTxC_Change_TS_Clock(0x01, div288);
    return rate288;
}

//===================================================================
//Function:
//Parmeter:
//u8Data:
//Return:
//Remark:
//===================================================================
U16 MHal_DVBTxC_Set_Config_dvbc_auto (U8 bAutoDetect)
{

    DBG_MSB(printk(" @MHal_DVBTxC_Set_Config_dvbc_auto=%d\n",bAutoDetect));

    sg_bAutoSR = bAutoDetect;
    sg_bAutoQAM = bAutoDetect;

    return TRUE;
}

//===================================================================
//Function:
//Parmeter:
//u8Data:
//Return:
//Remark:
//===================================================================
U16 MHal_DVBTxC_Set_Config_dvbc_atv_detector (U8 bEnable)
{
    U8 status = TRUE;

    DBG_MSB(printk(" @MHal_DVBTxC_Set_Config_dvbc_atv_detector=%d\n",bEnable));


    //// Reset Demod ///////////////////
    status &= MHal_DVBTxC_Reset();

    if( MHal_Demod_MB_WriteDspReg(0x0b, bEnable)!=TRUE)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Set_Config_dvbc_atv_detector NG 1\n"));
    }

    //// MHal_DVBTxC system init: DVB-C //////////
    if(status)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Set_Config_dvbc_atv_detector OK\n"));
        return  TRUE;
    }
    else
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Set_Config_dvbc_atv_detector NG 2\n"));
        return FALSE;
    }
}

/************************************************************************************************
Subject:    Set QAM Mode for DVB-C
Function:   
Parmeter:
Return:
Remark:
 *************************************************************************************************/
U16 MHal_DVBTxC_SetDvbcParam (U8 constel)
{
    gQamVal = constel;
    return TRUE;
}

/************************************************************************************************
  Subject:    channel change config
  Function:   MHal_DVBTxC_Config_dvbc
  Parmeter:   BW: bandwidth
  Return:     B16 :
  Remark:
*************************************************************************************************/
U16 MHal_DVBTxC_Config_dvbc (U16 SymbolRate, U32 u32IFFreq, B16 bSpecInv)
{
    U8              reg_qam;
    U8              reg_symrate_l;
    U8              reg_symrate_h;



    DBG_MSB(printk(" @MHal_DVBTxC_Config_dvbc=[%d][%d][%d]\n",SymbolRate,u32IFFreq,bSpecInv));

    reg_symrate_l = (U8) (SymbolRate & 0xff);
    reg_symrate_h = (U8) (SymbolRate >> 8);
    reg_qam = gQamVal;

    //// Reset Demod ///////////////////
    MHal_DVBTxC_Reset();

    if( MHal_Demod_MB_WriteDspReg(0x09, sg_bAutoSR) != TRUE)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Set_Config_dvbc_auto NG 1\n"));
    }

    if( MHal_Demod_MB_WriteDspReg(0x0a, sg_bAutoQAM) != TRUE)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Set_Config_dvbc_auto NG 2\n"));
    }

    // Symbol Rate 4000~7000
    if( MHal_Demod_MB_WriteDspReg(0x19, reg_symrate_l)!= TRUE)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Config_dvbc symbol L ng\n"));
        return FALSE;
    }
    if( MHal_Demod_MB_WriteDspReg(0x1a, reg_symrate_h)!= TRUE)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Config_dvbc symbol H ng\n"));
        return FALSE;
    }

    // QAM mode
    if( MHal_Demod_MB_WriteDspReg(0x32, reg_qam)!= TRUE)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Config_dvbc QAM ng\n"));
        return FALSE;
    }



    //// MHal_DVBTxC system init: DVB-C //////////

    DBG_MSB(printk(" @MHal_DVBTxC_Config_dvbc OK\n"));
    return  TRUE;

}

/************************************************************************************************
  Subject:    
  Function:   
  Parmeter:   
  Return:     
  Remark:
*************************************************************************************************/
static U16 MHal_DVBTxC_AutoDetectDVBT2(COFDM_LOCK_STATUS eStatus)
{
#ifdef DEMOD_DVB_T2_MERGE_T
    U8 flag_merged_joint_detect;
#else
    U8 flag_no_t2=0, flag_no_t=0, flag_detect_done=0;
#endif
    S32 status = TRUE;

#ifdef DEMOD_DVB_T2_MERGE_T
    status &= (MHal_Demod_MB_ReadDspReg(JOINT_DETECTION_FLAG, &flag_merged_joint_detect) == TRUE);
#else
    status &= (MHal_Demod_MB_ReadDspReg(T_DVBT2_NOCHAN_Flag, &flag_no_t2) == TRUE);
    status &= (MHal_Demod_MB_ReadDspReg(T_DVBT_NOCHAN_Flag, &flag_no_t) == TRUE);
    status &= (MHal_Demod_MB_ReadDspReg(T_DETECT_DONE_FLAG, &flag_detect_done) == TRUE);
#endif

    if (!status)
    {
        printk("[%s %d] MHal_Demod_MB_ReadDspReg fail\n", __FUNCTION__, __LINE__);
        return FALSE;
    }

    switch(eStatus)
    {
        case COFDM_T2_AUTO_DETECT_NO_CH:
#ifdef DEMOD_DVB_T2_MERGE_T
            return (flag_merged_joint_detect & BIT7);
#else
            return (flag_detect_done && flag_no_t && flag_no_t2);
#endif

        case COFDM_T2_AUTO_DETECT_FOUND_T2:
#ifdef DEMOD_DVB_T2_MERGE_T
            return (flag_merged_joint_detect & BIT3);
#else
            return (flag_detect_done && flag_no_t && !flag_no_t2);
#endif

        default:
            break;
    }

    return FALSE;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
U16 MHal_DVBTxC_Lock(E_SYSTEM system, COFDM_LOCK_STATUS eStatus )
{
    U16         u16Address = 0;
    U8          cData = 0;
    U8          cBitMask = 0;
    B16 Ret = 0;
    U8 use_dsp_reg = FALSE;
#ifdef SUPPORT_ADAPTIVE_TS_CLK
    MS_U8 unlock_indicator=0;
    MS_U8 u8TSDivNum = 0;
#endif

    system = system;
    switch( eStatus )
    {
    case COFDM_FEC_LOCK_DVBC:
#ifdef SUPPORT_ADAPTIVE_TS_CLK
        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x16, &unlock_indicator);
#endif
        MHal_Demod_MB_ReadReg(0x23E0, &cData);
        if (cData == 0x0C)
        {
#ifdef SUPPORT_ADAPTIVE_TS_CLK
            if(g_dvb_lock == 0  ||	unlock_indicator==0x01)
            {
                g_dvb_lock = 1;
                INTERN_DVB_Locked_Task(E_SYS_DVBC);	
                MHal_Demod_MB_WriteReg(MBX_REG_BASE + 0x16, 0x00);
            }
#endif
            Ret = TRUE;
        }
        else
        {
            Ret = FALSE;
        }
        return Ret;

	case COFDM_TR_LOCK_DVBC:
		u16Address =  (TOP_REG_BASE + 0xc4);
		cBitMask = BIT4;
		break;

    case COFDM_FEC_LOCK_DVBT:
        MHal_Demod_MB_ReadReg(0x23E0, &cData);
        if (cData == 0x0B)
            return TRUE;
        else
        {
            INTERN_DVBT_SignalQualityReset();
            return FALSE;
        }

    case COFDM_PSYNC_LOCK:
        u16Address =  0x232C;
        cBitMask = BIT1;
        break;

    case COFDM_TPS_LOCK:
#ifdef DEMOD_DVB_T2_MERGE_T
        if (MHal_Demod_MB_ReadReg(0x20c4, &cData))
        {
            if (cData < 6) // state < 6
                return FALSE;
        }
#endif
        u16Address =  0x2222;
        cBitMask = BIT1;
        break;

    case COFDM_TPS_LOCK_HISTORY:
        u16Address =  0x20C0;
        cBitMask = BIT3;
        break;

    case COFDM_MODE_CP_NO_CH_DETECT:
        u16Address =  0x20C0;
        cBitMask = BIT7;
        break;

    case COFDM_T2_AUTO_DETECT_NO_CH:
    case COFDM_T2_AUTO_DETECT_FOUND_T2:
        return MHal_DVBTxC_AutoDetectDVBT2(eStatus);

    case COFDM_DCR_LOCK:
        u16Address =  0x2737;
        cBitMask = BIT0;
        break;

    case COFDM_AGC_LOCK:
        u16Address =  0x271D;
        cBitMask = BIT0;
        break;

    case COFDM_MODE_DET:
        u16Address =  0x24CF;
        cBitMask = BIT4;
        break;

    case COFDM_LOCK_STABLE_DVBT:
#ifdef SUPPORT_ADAPTIVE_TS_CLK
        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x16, &unlock_indicator);
#endif
        u16Address =  0x20C0;
        cBitMask = BIT6;
        break;

	case COFDM_P1_LOCK_HISTORY:
        use_dsp_reg = TRUE;
        u16Address =  T2_DVBT2_LOCK_HIS;
        cBitMask = BIT5;
        break;

    case COFDM_FEC_LOCK_T2:
#ifdef SUPPORT_ADAPTIVE_TS_CLK
        if (MHal_Demod_MB_ReadDspReg(T2_TS_DATA_RATE_CHANGE_IND, &unlock_indicator) != TRUE)
        {
            printk("[%s %d] MHal_Demod_MB_ReadDspReg fail\n", __FUNCTION__, __LINE__);
            return FALSE;
        }
#endif
        use_dsp_reg = TRUE;
        u16Address =  T2_DVBT2_LOCK_HIS;
        cBitMask = BIT7;
        break;

    default:
        return FALSE;
    }

    if (use_dsp_reg)
    {
        if (MHal_Demod_MB_ReadDspReg(u16Address, &cData) != TRUE)
            return FALSE;
    }
    else
    {
        if (MHal_Demod_MB_ReadReg(u16Address, &cData) == FALSE)
            return FALSE;
    }

    if ((cData & cBitMask) == cBitMask)
    {
#ifdef SUPPORT_ADAPTIVE_TS_CLK
        if (eStatus == COFDM_LOCK_STABLE_DVBT)
        {
            if(g_dvb_lock == 0 || unlock_indicator==0x01)
            {
                g_dvb_lock = 1;
                INTERN_DVB_Locked_Task(E_SYS_DVBT);	
                MHal_Demod_MB_WriteReg(MBX_REG_BASE + 0x16, 0x00);
                INTERN_DVBT_SignalQualityReset();
            }
        }
        else if (eStatus == COFDM_FEC_LOCK_T2)
        {
            if (unlock_indicator == 1)
            {
                INTERN_DVBT2_GetTsDivNum(&u8TSDivNum);
                DBG_AUTO_TS_DATA_RATE(printk(">>>INTERN_DVBT2_GetLock TsClkDivNum = 0x%x<<<\n", u8TSDivNum));
                DBG_AUTO_TS_DATA_RATE(printk(">>>TS_DATA_RATE_CHANGE Detected: TsClkDivNum = 0x%x<<<\n", u8TSDivNum));

#ifdef DEMOD_DVB_T2_MERGE_T
                MHal_DVBTxC_Change_TS_Clock(0x01, u8TSDivNum);
#else
                MHal_Demod_ReadReg(0x103301, &cData);
                MHal_DVBTxC_Change_TS_Clock((cData & 0x07), u8TSDivNum);
#endif
                if (MHal_Demod_MB_WriteDspReg((U32)T2_TS_DATA_RATE_CHANGE_IND, 0x00) != TRUE)
                {
                    printk("[%s %d] MHal_Demod_MB_WriteDspReg fail\n", __FUNCTION__, __LINE__);
                    return FALSE;
                }
            }
        }
#endif
        return TRUE;
    }

    // not lock
    if (eStatus == COFDM_LOCK_STABLE_DVBT)
    {
        INTERN_DVBT_SignalQualityReset();
    }

    return FALSE;

}

/****************************************************************************
Subject:    
Function:   
Parmeter:   
Return:     
Remark:
 *****************************************************************************/
U16 MHal_DVBTxC_DVBT2_GetSNR (S32 *snr_e2)
{
    U8       status = true;
    U8       reg=0, reg_frz=0;
    U16      u16_snr100 = 0;
    S32      snr_1000=0, snr_offset_1000=0, snr_cali_1000=0;
    U8       u8_win = 0;
    U8       u8_gi = 0;

    // freeze
    status &= MHal_Demod_MB_ReadReg(TOP_REG_BASE+0xef, &reg_frz);
    status &= MHal_Demod_MB_WriteReg(TOP_REG_BASE+0xef, reg_frz|0x80);

    status &= (MHal_Demod_MB_ReadDspReg(T2_SNR_H, &reg) == TRUE);
    u16_snr100 = reg;
    status &= (MHal_Demod_MB_ReadDspReg(T2_SNR_L, &reg) == TRUE);
    u16_snr100 = (u16_snr100<<8)|reg;

    // unfreeze
    status &= MHal_Demod_MB_WriteReg(TOP_REG_BASE+0xef, reg_frz);

    snr_1000 = u16_snr100 * 10;

    // snr cali
    status &= MHal_Demod_MB_ReadReg(T2FDP_REG_BASE+0x01*2, &reg);
    u8_win = (reg>>2)&0x01;

    if (u8_win == 1)
    {
        status &= MHal_Demod_MB_ReadReg(T2L1_REG_BASE+0x31*2, &reg);
        u8_gi = (reg>>1)&0x07;

        if (u8_gi == 0) snr_offset_1000 = 157;
        else if(u8_gi == 1) snr_offset_1000 = 317;
        else if(u8_gi == 2) snr_offset_1000 = 645;
        else if(u8_gi == 3) snr_offset_1000 = 335;
        else if(u8_gi == 4) snr_offset_1000 = 39;
        else if(u8_gi == 5) snr_offset_1000 = 771;
        else if(u8_gi == 6) snr_offset_1000 = 378;

        snr_cali_1000 = snr_1000 - snr_offset_1000;
        if (snr_cali_1000 > 0) snr_1000 = snr_cali_1000;
    }

    if (status == true)
    {
        *snr_e2 = (snr_1000 / 10);
		DBG_MSB(printk(" @MHal_DVBTxC_DVBT2_GetSNR = %d\n", snr_1000));
    }

    return status;
}

/****************************************************************************
Subject:    
Function:   
Parmeter:   
Return:     
Remark:
 *****************************************************************************/
BOOL MHal_DVBTxC_DVBT2_GetPostLdpcBer(S32 *ber_e7)
{
    BOOL          status = true;
    U8            reg=0;
    U16           BitErrPeriod;
    U32           BitErr;
    U16           FecType = 0;
    MS_S64        dividend64, divisor64;

    status &= MHal_Demod_MB_WriteReg(T2FEC_REG_BASE+0x04, 0x01);

    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE+0x25, &reg);
    BitErrPeriod = reg;
    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE+0x24, &reg);
    BitErrPeriod = (BitErrPeriod << 8) | reg;

    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE + (0x34 << 1) + 3, &reg);
    BitErr = reg;
    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE + (0x34 << 1) + 2, &reg);
    BitErr = (BitErr << 8) | reg;
    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE + (0x34 << 1) + 1, &reg);
    BitErr = (BitErr << 8) | reg;
    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE + (0x34 << 1) + 0, &reg);
    BitErr = (BitErr << 8) | reg;

    status &= MHal_Demod_MB_WriteReg(T2FEC_REG_BASE+0x04, 0x00);

    if (BitErrPeriod == 0)
        BitErrPeriod = 1;

    status &= MHal_Demod_MB_ReadReg(T2L1_REG_BASE+0x8f, &reg);
    FecType = reg;
    status &= MHal_Demod_MB_ReadReg(T2L1_REG_BASE+0x8e, &reg);
    FecType = (FecType << 8) | reg;

    if (FecType & 0x0180)
    {
        if (BitErr == 0)
        {
            dividend64 = 5000000; // 0.5 * 10000000
            divisor64 = (MS_S64)BitErrPeriod * 64800;
            *ber_e7 = div64_s64(dividend64, divisor64);
        }
        else
        {
            dividend64 = (MS_S64)BitErr * 10000000;
            divisor64 = (MS_S64)BitErrPeriod * 64800;
            *ber_e7 = div64_s64(dividend64, divisor64);
        }
    }
    else
    {
        if (BitErr == 0)
        {
            dividend64 = 5000000; // 0.5 * 10000000
            divisor64 = (MS_S64)BitErrPeriod * 16200;
            *ber_e7 = div64_s64(dividend64, divisor64);
        }
        else
        {
            dividend64 = (MS_S64)BitErr * 10000000;
            divisor64 = (MS_S64)BitErrPeriod * 16200;
            *ber_e7 = div64_s64(dividend64, divisor64);
        }
    }

    DBG_MSB(printk("MHal_DVBTxC_DVBT2_GetPostLdpcBer = %d\n ", *ber_e7));

    if (status == FALSE)
    {
        printk("MHal_DVBTxC_DVBT2_GetPostLdpcBer Fail!\n");
        return FALSE;
    }

    return status;
}

#define DVBT2_BER_TH_HY_E1 1
/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
BOOL MHal_DVBTxC_DVBT2_GetSignalQuality(U16 *quality)
{
    S32     fber_e7 = 0;
    MS_S64  ber64_e7;
    S32     cn_rec_e2 = 0;
    S32     cn_ref_e2 = 0;
    S32     cn_rel_e2 = 0;

    MS_U8   status = true;
    MS_U16   L1_info_qam = 0, L1_info_cr = 0, i = 0;
    MS_U16   L1_info_gi;
    static MS_S64 fBerFilteredDVBT2_e7 = -1;
    static MS_U8 u8State = 0;
    MS_S64 fBerTH1_e8[] = {1E+4, 1E+3*(10-DVBT2_BER_TH_HY_E1), 1E+3*(10+DVBT2_BER_TH_HY_E1), 1E+4};
    MS_S64 fBerTH2_e8[] = {3E+1, 3E+1, 3*(10-DVBT2_BER_TH_HY_E1), 3*(10+DVBT2_BER_TH_HY_E1)};
    S32 BER_SQI_n, BER_SQI_d;
    S32 SQI_e2 = 0;
    static S32 FilteredSQI_e2 = 0;
    MS_U8  compare_snr_case = 0, min_case = 0;
    MS_S64 fBerTH1_patch_e1 = 10;

    DBG_MSB(printk("MHal_DVBTxC_DVBT2_GetSignalQuality, t=%d\n", jiffies_to_msecs(jiffies)));
 
    // copy from msb1240 >>>>>
    if (MHal_DVBTxC_Lock(E_SYS_DVBT2, COFDM_FEC_LOCK_T2))
    {
        L1_info_qam = 0xff;
        L1_info_cr = 0xff;
        L1_info_gi = 0xff;
        if(MHal_DVBTxC_DVBT2_Get_L1_Parameter(&L1_info_qam, MHal_DVBTxC_T2_MODUL_MODE) == FALSE)
            printk("[dvbt2] QAM parameter retrieve failure\n");

        if(MHal_DVBTxC_DVBT2_Get_L1_Parameter(&L1_info_cr, MHal_DVBTxC_T2_CODE_RATE) == FALSE)
            printk("[dvbt2]code rate parameter retrieve failure\n");

        if(MHal_DVBTxC_DVBT2_Get_L1_Parameter(&L1_info_gi, MHal_DVBTxC_T2_GUARD_INTERVAL) == FALSE)
            printk("[dvbt2] GI parameter retrieve failure\n");

        status &= MHal_DVBTxC_DVBT2_GetSNR(&cn_rec_e2);
        if (cn_rec_e2 < 0)
            return FALSE;

        // patch nordig SQI >>>>>>
        status &= (MHal_Demod_MB_ReadDspReg((MS_U32)T2_MinCase, &min_case) == TRUE);
        status &= (MHal_Demod_MB_ReadDspReg((MS_U32)T2_CompareSNR_case, &compare_snr_case) == TRUE);
        switch(L1_info_cr)
        {
            case _T2_CR3Y5:
                if (cn_rec_e2 < 2700)
                {
                    if ((compare_snr_case == 1) && (min_case != 5))
                        cn_rec_e2 -= 128;
                }
                else
                {
                    if ((compare_snr_case == 1) && (min_case == 5))
                        cn_rec_e2 += 115;
                }
                break;

            case _T2_CR2Y3:
                if (cn_rec_e2 < 2700)
                {
                    if ((compare_snr_case == 1) && (min_case != 5))
                    {
                        cn_rec_e2 -= 128;
                        fBerTH1_patch_e1 = 48;
                    }
                }
                else
                {
                    if ((compare_snr_case == 1) && (min_case == 5))
                        cn_rec_e2 += 137;
                }
                break;

            case _T2_CR3Y4:
                if (cn_rec_e2 < 2700)
                {
                    if ((compare_snr_case == 1) && (min_case != 5))
                        cn_rec_e2 -= 156;
                }
                else
                {
                    if ((compare_snr_case == 1) && (min_case == 5))
                        cn_rec_e2 += 175;
                }
                break;

            default:
                break;
        }
        // <<<<<< patch nordig SQI

        if (MHal_DVBTxC_DVBT2_GetPostLdpcBer(&fber_e7) == FALSE)
        {
            DBG_MSB(printk("GetPostViterbiBer Fail!\n"));
            return FALSE;
        }

        if (fBerFilteredDVBT2_e7 <= 0)
            fBerFilteredDVBT2_e7 = fber_e7;
        else if ((fber_e7 > 0) && (div_s64(fBerFilteredDVBT2_e7, fber_e7) > 30 || div_s64(fBerFilteredDVBT2_e7*100, fber_e7) < 3))
            fBerFilteredDVBT2_e7 = fber_e7;
        else
            fBerFilteredDVBT2_e7 = div_s64(fBerFilteredDVBT2_e7, 2) + (fber_e7 / 2);
        fber_e7 = fBerFilteredDVBT2_e7;

        ber64_e7 = (MS_S64)fber_e7;
        if ((ber64_e7 * 100) > (fBerTH1_e8[u8State] * fBerTH1_patch_e1))
        {
           BER_SQI_n = 0;
           BER_SQI_d = 1;
           u8State = 1;
        }
        else if ((ber64_e7 * 10) >= fBerTH2_e8[u8State])
        {
           BER_SQI_n = 100;
           BER_SQI_d = 15;
           u8State = 2;
        }
        else
        {
            if ((L1_info_cr == _T2_CR3Y4) &&
                (L1_info_qam == _T2_256QAM) &&
                (L1_info_gi == 2) &&
                (cn_rec_e2 < 2010))
            {
                // Patch Noridg SQI,
                // 256QAM, CR3/4, GI1/8, SNR 20.5dB
                BER_SQI_n = 100;
                BER_SQI_d = 15;
            }
            else if ((L1_info_cr == _T2_CR2Y3) &&
                (L1_info_qam == _T2_256QAM) &&
                (L1_info_gi == 1) &&
                (cn_rec_e2 <= 1850))
            {
                // Patch Noridg SQI,
                // 256QAM, CR3/4, GI1/8, SNR 20.5dB
                BER_SQI_n = 100;
                BER_SQI_d = 15;
            }
            else if ((L1_info_cr == _T2_CR3Y5) &&
                (L1_info_qam == _T2_256QAM) &&
                (L1_info_gi == 6) &&
                (cn_rec_e2 <= 1700))
            {
                // Patch Noridg SQI,
                // 256QAM, CR3/4, GI1/8, SNR 20.5dB
                BER_SQI_n = 100;
                BER_SQI_d = 15;
            }
            else
            {
                BER_SQI_n = 100;
                BER_SQI_d = 6;
            }
            u8State = 3;
        }

        ///////// Get Constellation and Code Rate to determine Ref. C/N //////////
        ///////// (refer to Teracom min. spec 2.0 4.1.1.7) /////
        cn_ref_e2 = -100;
        for(i = 0; i < sizeof(DVBT2_SqiCnNordigP1)/sizeof(MHal_DVBTxC_T2_SQI_CN_NORDIGP1); i++)
        {
            if ( (L1_info_qam == (MS_U16)DVBT2_SqiCnNordigP1[i].constel)
            && (L1_info_cr == (MS_U16)DVBT2_SqiCnNordigP1[i].code_rate) )
            {
                cn_ref_e2 = DVBT2_SqiCnNordigP1[i].cn_ref_e2;
                break;
            }
        }

        if (cn_ref_e2 < 0)
            SQI_e2 = 0;
        else
        {
            // 0.7, snr offset
            cn_rel_e2 = cn_rec_e2 - cn_ref_e2 + 70;
            if (cn_rel_e2 > 300)
                SQI_e2 = 10000;
            else if (cn_rel_e2 >= -300)
            {
                SQI_e2 = (cn_rel_e2+300)*BER_SQI_n/BER_SQI_d;
                if (SQI_e2 > 10000) SQI_e2 = 10000;
                else if (SQI_e2 < 0) SQI_e2 = 0;
            }
            else
                SQI_e2 = 0;
        }

        if ((SQI_e2 > (FilteredSQI_e2 * 3)) || (FilteredSQI_e2 > (SQI_e2 * 3)))
        {
            FilteredSQI_e2 = SQI_e2;
        }
        else
        {
            FilteredSQI_e2 += (SQI_e2 - FilteredSQI_e2) / 2;
        }

        if ((FilteredSQI_e2 % 100) < 50)
        {
            *quality = FilteredSQI_e2 / 100;
        }
        else
        {
            *quality = FilteredSQI_e2 / 100 + 1;
        }
    }
    else
    {
        *quality = 0;
    }
    // <<<<< copy from msb1240

    DBG_MSB(printk("SNR = %d, QAM = %d, code Rate = %d\n", cn_rec_e2, L1_info_qam, L1_info_cr));
    DBG_MSB(printk("BER = %d\n", fber_e7));
    DBG_MSB(printk("Signal Quility = %d\n", *quality));
    return status;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
BOOL MHal_DVBTxC_DVBT2_Get_L1_Parameter(U16 * pu16L1_parameter, MHal_DVBTxC_DVBT2_SIGNAL_INFO eSignalType)
{
    MS_U8 u8Data = 0;
    MS_U16    FecType = 0;
	MS_U16	  u16Data = 0;

    if (MHal_DVBTxC_Lock(E_SYS_DVBT2, COFDM_FEC_LOCK_T2))
    {
        if (eSignalType == MHal_DVBTxC_T2_MODUL_MODE)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x47 * 2), &u8Data) == FALSE)
                return FALSE;

            *pu16L1_parameter  = (((MS_U16) u8Data) & (BIT5 | BIT4 | BIT3)) >> 3;
        }
        else  if (eSignalType == MHal_DVBTxC_T2_FFT_VALUE)
        {
            if (MHal_Demod_MB_ReadReg(T2TDP_REG_BASE + (0x40 * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & (BIT2 | BIT1 | BIT0));
        }
        else  if (eSignalType == MHal_DVBTxC_T2_GUARD_INTERVAL)
        {
            if (MHal_Demod_MB_ReadReg(DVBTM_REG_BASE + (0x0b * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & (BIT6 | BIT5 | BIT4)) >> 4;
        }
        else  if (eSignalType == MHal_DVBTxC_T2_CODE_RATE)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x47 * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & (BIT2 | BIT1 | BIT0));
        }
        else if (eSignalType == MHal_DVBTxC_T2_PREAMBLE)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x30 * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & (BIT4)) >> 4;
        }
        else if (eSignalType == MHal_DVBTxC_T2_S1_SIGNALLING)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x30 * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & (BIT3 | BIT2 | BIT1)) >> 1;
        }
        else if (eSignalType == MHal_DVBTxC_T2_PILOT_PATTERN)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x36 * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & 0x0F);
        }
        else if (eSignalType == MHal_DVBTxC_T2_BW_EXT)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x30 * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & (BIT0));
        }
        else if (eSignalType == MHal_DVBTxC_T2_PAPR_REDUCTION)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x31 * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & 0xF0) >> 4;
        }
        else if (eSignalType == MHal_DVBTxC_T2_OFDM_SYMBOLS_PER_FRAME)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x3C * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (MS_U16) u8Data;
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x3C * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter |= (((MS_U16) u8Data) & 0x0F) << 8;
        }
        else if (eSignalType == MHal_DVBTxC_T2_PLP_ROTATION)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x47 * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & BIT6) >> 6;
        }
        else if (eSignalType == MHal_DVBTxC_T2_PLP_FEC_TYPE)
        {
            //FEC Type[8:7]
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + 0x8f, &u8Data) == FALSE) return FALSE;
            FecType = u8Data;
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + 0x8e, &u8Data) == FALSE) return FALSE;
            FecType = (FecType << 8) | u8Data;

            *pu16L1_parameter = (FecType & 0x0180) >> 7;
        }
        else  if (eSignalType == MHal_DVBTxC_T2_NUM_PLP)
        {
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x42 * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (MS_U16)u8Data;
        }
		else if (eSignalType == MHal_DVBTxC_T2_PLP_TYPE)
		{
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x45 * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = ((MS_U16) u8Data) & 0x07;
		}
		else if (eSignalType == MHal_DVBTxC_T2_PLP_TIME_IL_TYPE)
		{
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x48 * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & 0x10) >> 4;
		}
		else if (eSignalType == MHal_DVBTxC_T2_PLP_TIME_IL_LENGTH)
		{
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x49 * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = ((MS_U16) u8Data) & 0xFF;
		}
		else if (eSignalType == MHal_DVBTxC_T2_DAT_ISSY)
		{
            if (MHal_Demod_MB_ReadReg(T2DJB_REG_BASE + (0x61 * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = (((MS_U16) u8Data) & 0x10) >> 4;
		}
		else if (eSignalType == MHal_DVBTxC_T2_PLP_MODE)
		{
		    if (MHal_Demod_MB_WriteReg(T2DJB_REG_BASE + (0x60 * 2) + 1, 0x01) == FALSE)
            {
                return FALSE;
            }
		    if (MHal_Demod_MB_WriteReg(T2DJB_REG_BASE + (0x60 * 2), 0x16) == FALSE)
            {
                return FALSE;
            }
            if (MHal_Demod_MB_ReadReg(T2DJB_REG_BASE + (0x61 * 2), &u8Data) == FALSE)
			{
				return FALSE;
			}
		    if (MHal_Demod_MB_WriteReg(T2DJB_REG_BASE + (0x60 * 2) + 1, 0x00) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = ((MS_U16) u8Data) & 0x03;
		}
		else if (eSignalType == MHal_DVBTxC_T2_L1_MODULATION)
		{
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x31 * 2) + 1, &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = ((MS_U16) u8Data) & 0x0F;
		}
		else if (eSignalType == MHal_DVBTxC_T2_NUM_T2_FRAMES)
		{
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x3b * 2), &u8Data) == FALSE)
            {
                return FALSE;
            }
            *pu16L1_parameter  = ((MS_U16) u8Data) & 0xFF;
		}
		else if (eSignalType == MHal_DVBTxC_T2_PLP_NUM_BLOCKS_MAX)
		{
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x55 * 2) + 1, &u8Data) == FALSE) return FALSE;
            u16Data = u8Data & 0x03;
            if (MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x55 * 2), &u8Data) == FALSE) return FALSE;
            u16Data = (u16Data << 8) | u8Data;

            *pu16L1_parameter = u16Data;
		}
		else if (eSignalType == MHal_DVBTxC_T2_FEF_ENABLE)
		{

			if (MHal_Demod_MB_ReadDspReg(0x00F1, &u8Data) != TRUE)
			{
				DBG_MSB(printk(">MHal_DVBTxC_DVBT2_GetLock MBX_ReadDspReg fail \n"));
				return FALSE;
			}
            *pu16L1_parameter  = ((MS_U16) u8Data) & 0x01;
		}
        else
        {
            return FALSE;
        }

        return TRUE;

    }

    return FALSE;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
B16 MHal_DVBTxC_DVBT2_GetPacketErr(U16 *u16PktErr)
{
    MS_BOOL          status = true;
    MS_U8            reg = 0;
    MS_U16           PktErr;
    U8               reg_frz = 0;

    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE+0x04, &reg_frz);
    status &= MHal_Demod_MB_WriteReg(T2FEC_REG_BASE+0x04, reg_frz|0x01);

    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE+0x5B, &reg);
    PktErr = reg;
    status &= MHal_Demod_MB_ReadReg(T2FEC_REG_BASE+0x5A, &reg);
    PktErr = (PktErr << 8) | reg;

    *u16PktErr = PktErr;

    status &= MHal_Demod_MB_WriteReg(T2FEC_REG_BASE+0x04, reg_frz);

    DBG_MSB(printk("MHal_DVBTxC_DVBT2_GetPacketErr = %d \n ", (int)PktErr));

    *u16PktErr = PktErr;

    return status;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
B16 MHal_DVBTxC_GetIFAGCGain(U16 *pu16ifagc)
{
    U8 tmp, status=TRUE;
    U16 if_agc_gain = 0;

    status &= MHal_Demod_MB_ReadReg(0x2800+0x11*2, &tmp);
    status &= MHal_Demod_MB_WriteReg(0x2800+0x11*2, (tmp&0xF0)|0x03);

    status &= MHal_Demod_MB_ReadReg(0x2800+0x02*2+1, &tmp);
    status &= MHal_Demod_MB_WriteReg(0x2800+0x02*2+1, tmp|0x80);

    status &= MHal_Demod_MB_ReadReg(0x2800+0x12*2+1, &tmp);
    if_agc_gain = tmp;
    status &= MHal_Demod_MB_ReadReg(0x2800+0x12*2, &tmp);
    if_agc_gain = (if_agc_gain<<8)|tmp;

    status &= MHal_Demod_MB_ReadReg(0x2800+0x02*2+1, &tmp);
    status &= MHal_Demod_MB_WriteReg(0x2800+0x02*2+1, tmp&0x7F);

    *pu16ifagc = if_agc_gain;
    return status;
}

/****************************************************************************
  Subject:    Function providing approx. result of Log10(X)
  Function:   Log10Approx
  Parmeter:   Operand X in float
  Return:     Approx. value of Log10(X) in float
  Remark:      Ouput range from 0.0, 0.3 to 9.6 (input 1 to 2^32)
*****************************************************************************/
const MS_S64 _LogApproxTableX_E2[80] =
{
    100UL, 130UL, 169UL, 220UL, 286UL, 371UL, 483UL, 627UL, 816UL, 1060UL, 1379UL,
    1792UL, 2330UL, 3029UL, 3937UL, 5119UL, 6654UL, 8650UL, 11246UL, 14619UL,
    19005UL, 24706UL, 32118UL, 41754UL, 54280UL, 70564UL, 91733UL, 119253UL,
    155029UL, 201538UL, 262000UL, 340599UL, 442779UL, 575613UL, 748297UL,
    972786UL, 1264622UL, 1644008UL, 2137211UL, 2778374UL, 3611886UL,
    4695452UL, 6104088UL, 7935315UL, 10315909UL, 13410682UL, 17433886UL,
    22664052UL, 29463268UL, 38302248UL, 49792922UL, 64730799UL, 84150039UL, 109395050UL,
    142213565UL, 184877635UL, 240340925UL, 312443203UL, 406176164UL, 528029013UL,
    686437717UL, 892369032UL, 1160079742UL, 1508103665UL, 1960534764UL, 2548695194UL,
    3313303752UL, 4307294877UL, 5599483340UL, 7279328342UL, 9463126845UL,
    12302064899UL, 15992684368UL, 20790489679UL, 27027636582UL, 35135927557UL,
    45676705824UL, 59379717572UL, 77193632843UL, 100351722696UL
};

const S32 _LogApproxTableY_E2[80] =
{
      0,  11,  23,  34,  46,  57,  68,  80,  91, 103, 114, 125, 137,
    148, 160, 171, 182, 194, 205, 216, 228, 239, 251, 262, 273, 285,
    296, 308, 319, 330, 342, 353, 365, 376, 387, 399, 410, 422, 433,
    444, 456, 467, 479, 490, 501, 513, 524, 536, 547, 558, 570, 581,
    593, 604, 615, 627, 638, 649, 661, 672, 684, 695, 706, 718, 729,
    741, 752, 763, 775, 786, 798, 809, 820, 832, 843, 855, 866, 877,
    889, 900
};

S32 Log10Approx_E2(MS_S64 s64_x)
{
    MS_U8  indx;
    MS_S64 dividend64, divisor64;

    if (s64_x < _LogApproxTableX_E2[0])
        return 0;

    for (indx=1 ; indx<80 ; indx++)
    {
        if (s64_x < _LogApproxTableX_E2[indx])
        {
            divisor64 = _LogApproxTableX_E2[indx] - _LogApproxTableX_E2[indx - 1];
            dividend64 = (s64_x - _LogApproxTableX_E2[indx - 1]) * (_LogApproxTableY_E2[indx] - _LogApproxTableY_E2[indx - 1]);
            return (_LogApproxTableY_E2[indx - 1] + div64_s64(dividend64, divisor64));
        }
    }

    return _LogApproxTableY_E2[79];
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
B16 MHal_DVBTxC_DVBTGetSNR (S32 *snr_e2)
{
	U8			status = TRUE;
	U8			reg = 0 , reg_frz = 0;
	U32			noise_power = 0;
	MS_S64      noise_power64_e2 = 0;

	status &= MHal_Demod_MB_ReadReg(0x15fe, &reg_frz);
	status &= MHal_Demod_MB_WriteReg(0x15fe, reg_frz|0x01);

	status &= MHal_Demod_MB_WriteReg(0x15ff, 0x01);

	status &= MHal_Demod_MB_ReadReg(0x165b, &reg);
	noise_power = reg & 0x07;

	status &= MHal_Demod_MB_ReadReg(0x165a, &reg);
	noise_power = (noise_power << 8)|reg;

	status &= MHal_Demod_MB_ReadReg(0x1659, &reg);
	noise_power = (noise_power << 8)|reg;

	status &= MHal_Demod_MB_ReadReg(0x1658, &reg);
	noise_power = (noise_power << 8)|reg;


	status &= MHal_Demod_MB_WriteReg(0x15fe, reg_frz);

	status &= MHal_Demod_MB_WriteReg(0x15ff, 0x01);

    noise_power64_e2 = (MS_S64)noise_power * 100;
    noise_power64_e2 = div_s64(noise_power64_e2, 2560);
    if (noise_power64_e2 < 100)
        noise_power64_e2 = 100;

    *snr_e2 = Log10Approx_E2(noise_power64_e2) * 10;

	DBG_MSB(printk("SNR = %d\n", *snr_e2));

	if(status)
	{
		DBG_MSB(printk(" @MHal_DVBTxC_DVBTGetSNR OK\n"));
		return	TRUE;
	}
	else
	{
		DBG_MSB(printk(" @MHal_DVBTxC_DVBTGetSNR NG\n"));
		return FALSE;
	}

}

/****************************************************************************
Subject:    To get the Post viterbi BER
Function:   MHal_DVBTxC_GetPostViterbiBer
Parmeter:  Quility
Return:       E_RESULT_SUCCESS
E_RESULT_FAILURE =>Read I2C fail, MSB1228_VIT_STATUS_NG
Remark:     For the Performance issue, here we just return the Post Value.(Not BER)
We will not read the Period, and have the "/256/8"
 *****************************************************************************/
U16 MHal_DVBTxC_GetPostViterbiBer(S32 *ber_e7)
{
    U8          status = TRUE;
    U8          reg = 0, reg_frz = 0;
    U16         BitErrPeriod = 0;
    U32         BitErr = 0;
    U16         PktErr = 0;
    MS_S64      dividend64, divisor64;


    status &= MHal_Demod_MB_ReadReg(0x1f03, &reg_frz);
    status &= MHal_Demod_MB_WriteReg(0x1f03, reg_frz|0x03);

    status &= MHal_Demod_MB_ReadReg(0x1f47, &reg);
    BitErrPeriod = reg;

    status &= MHal_Demod_MB_ReadReg(0x1f46, &reg);
    BitErrPeriod = (BitErrPeriod << 8)|reg;


    status &= MHal_Demod_MB_ReadReg(0x1f6d, &reg);
    BitErr = reg;

    status &= MHal_Demod_MB_ReadReg(0x1f6c, &reg);
    BitErr = (BitErr << 8)|reg;

    status &= MHal_Demod_MB_ReadReg(0x1f6b, &reg);
    BitErr = (BitErr << 8)|reg;

    status &= MHal_Demod_MB_ReadReg(0x1f6a, &reg);
    BitErr = (BitErr << 8)|reg;

#if 1
    status &= MHal_Demod_MB_ReadReg(0x1f67, &reg);
    PktErr = reg;

    status &= MHal_Demod_MB_ReadReg(0x1f66, &reg);
    PktErr = (PktErr << 8)|reg;
#endif

    reg_frz=reg_frz&(~0x03);
    status &= MHal_Demod_MB_WriteReg(0x1f03, reg_frz);

    if (BitErrPeriod == 0 ) //protect 0
        BitErrPeriod = 1;

    if (BitErr <=0 )
    {
        dividend64 = 5000000; // 0.5 * 10000000
        divisor64 = (MS_S64)BitErrPeriod * 128 * 188 * 8;
        *ber_e7 = div64_s64(dividend64, divisor64);
    }
    else
    {
        dividend64 = (MS_S64)BitErr * 10000000;
        divisor64 = (MS_S64)BitErrPeriod * 128 * 188 * 8;
        *ber_e7 = div64_s64(dividend64, divisor64);
    }

    DBG_MSB(printk("MSB PostVitBER = %d\n ", *ber_e7));
    DBG_MSB(printk("MSB PktErr = %d \n ", (int)PktErr));


    if(status)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_GetPostViterbiBer OK\n"));
        return  TRUE;
    }
    else
    {
        DBG_MSB(printk(" @MHal_DVBTxC_GetPostViterbiBer NG\n"));
        return FALSE;
    }

}

/****************************************************************************
Subject:    To get the DVT Signal quality
Function:   MHal_DVBTxC_GetSignalQuality
Parmeter:   Quality
Return:     E_RESULT_SUCCESS
E_RESULT_FAILURE
Remark:     Implement SQI refering to Teracom min. spec 2.0 4.1.1.7
 *****************************************************************************/
U16 MHal_DVBTxC_GetSignalQuality(E_SYSTEM system,U16 *quality)
{
    MS_S64 tmp64=0;

    if(system == E_SYS_DVBT)
    {
        S32         ber_sqi_e2=0;
        S32         fber_e7=0;
        S32         cn_rec_e2=0;
        S32         cn_nordig_p1_e2=0;
        S32         cn_rel_e2=0;
        S32         snr_e2=0;
        U8          status = TRUE;
        U8          reg_tmp = 0;
        U8          tps_cnstl = 0;
        U8          tps_cr = 0;
        U8          i = 0;
        U32         current_time;

        if (TRUE == MHal_DVBTxC_Lock(E_SYS_DVBT, COFDM_PSYNC_LOCK))
        {
            current_time = (U32)jiffies_to_msecs(jiffies);
            if ((current_time - u32FecFirstLockTimeDVBT) < 150)
            {
                msleep(150 - (current_time - u32FecFirstLockTimeDVBT));
            }

            if(MHal_DVBTxC_GetPostViterbiBer(&fber_e7) == TRUE) // min(fber_e7) is 4, george@170421
            {
                //printk("MHal_DVBTxC_GetSignalQuality(0) fber=%d\n", fber_e7);
                if(fViterbiBerFilteredDVBT_E7 <= 0)
                {
                    fViterbiBerFilteredDVBT_E7 = fber_e7;
                }
                else
                {
                    if ((fber_e7 > 0) && (div_s64(fViterbiBerFilteredDVBT_E7, fber_e7) > 30 || div_s64(fViterbiBerFilteredDVBT_E7*100, fber_e7) < 3))
                    {
                        fViterbiBerFilteredDVBT_E7 = fber_e7;
                    }
                    else
                    {
                        fViterbiBerFilteredDVBT_E7 = div_s64(fViterbiBerFilteredDVBT_E7, 2) + (fber_e7 / 2);
                        fber_e7 = fViterbiBerFilteredDVBT_E7;
                    }
                }
            }
            else
            {
                *quality = 0;
                return FALSE;
            }
            //printk("MHal_DVBTxC_GetSignalQuality(1) filtered fber=%d\n", fber_e7);

            if (fber_e7 > 48000)
                ber_sqi_e2 = 0;
            else if (fber_e7 > 5)//0712 SQI update
            {
                // replace Log10Approx(1/fber) by Log10Approx_E2(fber_e7):
                // log(1/fber) = -log(fber)
                //             = -(log(fber*10^5)-5)
                //             = 5-Log10Approx_E2(fber_e7)/100
                //     ber_sqi = Log10Approx(1/fber)*20.0-23
                //             = 100-Log10Approx_E2(fber_e7)/5-23
                //  ber_sqi_e2 = 10000-Log10Approx_E2(fber_e7)*20-2300
                ber_sqi_e2 = 7700 - 20 * Log10Approx_E2(fber_e7);
            }
            else
                ber_sqi_e2 = 10000;

            if (ber_sqi_e2 > 10000)
                ber_sqi_e2 = 10000;
            //printk("MHal_DVBTxC_GetSignalQuality(2) ber_sqi=%d\n", ber_sqi_e2);

            status &= MHal_DVBTxC_DVBTGetSNR(&snr_e2);
            cn_rec_e2 = snr_e2;
            //printk("MHal_DVBTxC_GetSignalQuality(3) cn_rec=%d\n", cn_rec_e2);

            ///////// Get Constellation and Code Rate to determine Ref. C/N //////////
            ///////// (refer to Teracom min. spec 2.0 4.1.1.7) /////

            status &= MHal_Demod_MB_ReadReg(0x2224, &reg_tmp);
            tps_cnstl = reg_tmp & 0x03;

            status &= MHal_Demod_MB_ReadReg(0x2224+1, &reg_tmp);
            tps_cr = (reg_tmp & 0x70) >> 4;

            for(i = 0; i < sizeof(SqiCnNordigP1)/sizeof(MHal_DVBTxC_SQI_CN_NORDIGP1); i++)
            {
                if ( (tps_cnstl == SqiCnNordigP1[i].constel)
                        && (tps_cr == SqiCnNordigP1[i].code_rate) )
                {
                    cn_nordig_p1_e2 = SqiCnNordigP1[i].cn_ref_e2;
                    break;
                }
            }
            //printk("MHal_DVBTxC_GetSignalQuality(4) cn_nordig_p1=%d\n", cn_nordig_p1_e2);

            cn_rel_e2 = cn_rec_e2 - cn_nordig_p1_e2;

            DBG_MSB(printk("\n>> cn_rel:%d, cn_rec:%d, cn_nordig_p1:%d, ber_sqi:%d \n", cn_rel_e2, cn_rec_e2, cn_nordig_p1_e2, ber_sqi_e2));

            if (cn_rel_e2 < (-700))
            {
                *quality = 0;
                //printk("MHal_DVBTxC_GetSignalQuality(5) quality=0\n");
            }
            else if (cn_rel_e2 < 300)
            {
                tmp64 = (MS_S64)ber_sqi_e2 * (cn_rel_e2 - 300);
                tmp64 = (MS_S64)ber_sqi_e2 * 100 + div_s64(tmp64, 10);
                *quality = div_s64(tmp64, 10000);
                //printk("MHal_DVBTxC_GetSignalQuality(6) quality=%d\n", *quality);
            }
            else
            {
                *quality = ber_sqi_e2 / 100;
                //printk("MHal_DVBTxC_GetSignalQuality(7) quality=%d\n", *quality);
            }
        }
        else
        {
            *quality = 0;
            INTERN_DVBT_SignalQualityReset();
            //printk("MHal_DVBTxC_GetSignalQuality(8) quality=0\n");
        }

        DBG_MSB(printk("SQI = %d\n", (int)*quality));

        return status;
    }
    else // DVB-C
    {
        S32         fber_e7;
        S32         log_ber_e2;

        if (TRUE == MHal_DVBTxC_Lock(E_SYS_DVBC, COFDM_PSYNC_LOCK))
        {
            if (MHal_DVBTxC_GetPostViterbiBer(&fber_e7) == FALSE)
            {
                DBG_MSB(printk("GetPostViterbiBer Fail!\n"));
                return FALSE;
            }

            // replace Log10Approx(1/fber) by Log10Approx_E2(fber_e7):
            // log(1/fber) = -log(fber)
            //             = -(log(fber*10^5)-5)
            //             = 5-Log10Approx_E2(fber_e7)/100
            //  log_ber    = -Log10Approx(1/fber)
            //             = Log10Approx_E2(fber_e7)/100-5
            //  log_ber_e2 = Log10Approx_E2(fber_e7) - 500
            if (fber_e7 < 1)
            {
                log_ber_e2 = -701;
            }
            else
            {
                log_ber_e2 = Log10Approx_E2(fber_e7) - 500;
            }
            DBG_MSB(printk("$$$$$$$$$$$$$$$ DVB-C Log(BER) = %d\n",log_ber_e2));

#if 0
            //SQI issue 2010.0107
            if ( log_ber <= (-7.0) )
            {
             *quality = 100;
            }
            else  if ( log_ber < (-4.5) )
            {
             *quality = 30 + (((-4.5) - log_ber) / ((-4.5)-(-7.0)) * (100-30));
            }
            else  if ( log_ber < (-3.2) )
            {
             *quality = 20 + (((-3.2) - log_ber) / ((-3.2)-(-4.5)) * (30-20));
            }
            else  if ( log_ber < (-2.7) )
            {
             *quality = 10 + (((-2.7) - log_ber) / ((-2.7)-(-3.2)) * (20-10));
            }
            else
            {
             *quality = 10;
            }


#else
            if ( log_ber_e2 <= (-700) )            // PostVit BER < 1e-7
            {
                *quality = 100;
            }
            else  if ( log_ber_e2 < (-370) )       // PostVit BER < 2e-4
            {
                // *quality = 60 + (((-3.7) - log_ber) / ((-3.7)-(-7.0)) * (100-60));
                tmp64 = (-370 - (MS_S64)log_ber_e2) * 40;
                *quality = div_s64(tmp64, 330) + 60;
            }
            else  if ( log_ber_e2 < (-270) )       // PostVit BER < 2e-3
            {
                // *quality = 10 + (((-2.7) - log_ber) / ((-2.7)-(-3.7)) * (60-10));
                tmp64 = (-270 - (MS_S64)log_ber_e2) * 50;
                *quality = div_s64(tmp64, 100) + 10;
            }
            else
            {
                *quality = 10;
            }
#endif

        }
        else
        {
            *quality = 0;
        }

        return TRUE;
    }
}

/****************************************************************************
Subject:
Function:
Parmeter:
Return:
E_RESULT_FAILURE
Remark:
 *****************************************************************************/
B16 MHal_DVBTxC_Get_Packet_Error(U16 *u16_data)
{
    U8          status = TRUE;
    U8          reg = 0, reg_frz = 0;
    U16         PktErr = 0;

    status &= MHal_Demod_MB_ReadReg(0x1f03, &reg_frz);
    status &= MHal_Demod_MB_WriteReg(0x1f03, reg_frz|0x03);

    status &= MHal_Demod_MB_ReadReg(0x1f67, &reg);
    PktErr = reg;

    status &= MHal_Demod_MB_ReadReg(0x1f66, &reg);
    PktErr = (PktErr << 8)|reg;

    *u16_data = PktErr;

    reg_frz=reg_frz&(~0x03);
    status &= MHal_Demod_MB_WriteReg(0x1f03, reg_frz);

    DBG_MSB(printk("===================>MSB PktErr = %d \n ", (int)PktErr));

    if(status)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Get_Packet_Error OK\n"));
        return  TRUE;
    }
    else
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Get_Packet_Error NG\n"));
        return FALSE;
    }

}

/****************************************************************************
Subject:    Read the signal to noise ratio (SNR)
Function:   MHal_DVBTxC_DVBCGetSNR
Parmeter:   None
Return:
Remark:
 *****************************************************************************/
U16 MHal_DVBTxC_DVBCGetSNR (S32 *snr_e2)
{
    MS_BOOL status = true;
    MS_U8 u8Data = 0, reg_frz = 0;
    MS_U16 noisepower = 0;
	MS_S64 tmp64;

    if (TRUE == MHal_DVBTxC_Lock(E_SYS_UNKOWN, COFDM_PSYNC_LOCK))
    {
        status &= MHal_Demod_MB_ReadReg(EQE_REG_BASE + 0x3d, &reg_frz);
        status &= MHal_Demod_MB_WriteReg(EQE_REG_BASE + 0x3d, reg_frz|0x01);

        status &= MHal_Demod_MB_ReadReg(EQE2_REG_BASE + 0x6f, &u8Data);
        noisepower = u8Data;
        status &= MHal_Demod_MB_ReadReg(EQE2_REG_BASE + 0x6e, &u8Data);
        noisepower = (noisepower<<8)|u8Data;

        reg_frz = reg_frz&(~0x01);
        status &= MHal_Demod_MB_WriteReg(EQE_REG_BASE + 0x3d, reg_frz);

        if(noisepower == 0x0000)
        noisepower = 0x0001;
        // snr = 10 * (log(65536) - log(noisepower))
        // snr_e2 = 1000*log(65536) - 10*Log10Approx_E2(noisepower*100)
        tmp64 = (MS_S64)noisepower * 100;
        *snr_e2 = 4816 - (Log10Approx_E2(tmp64) * 10);
    }
    else
    {
        *snr_e2 = 0;
    }

    return status;
}

/****************************************************************************
Subject:    To get the DVBT parameter
Function:   MHal_DVBTxC_GetTpsInfo
Parmeter:   point to return parameter
Constellation (b1 ~ b0)  : 0~2 => QPSK, 16QAM, 64QAM
Hierarchy (b4 ~ b2))     : 0~3 => None, Aplha1, Aplha2, Aplha4
LP Code Rate (b7 ~ b5)   : 0~4 => 1/2, 2/3, 3/4, 5/6, 7/8
HP Code Rate (b10 ~ b8)  : 0~4 => 1/2, 2/3, 3/4, 5/6, 7/8
GI (b12 ~ b11)           : 0~3 => 1/32, 1/16, 1/8, 1/4
FFT (b14 ~ b13)          : 0~2 => 2K, 8K, 4K
Return:     TRUE
FALSE
Remark:   The TPS parameters will be available after TPS lock
 *****************************************************************************/
U16 MHal_DVBTxC_GetTpsInfo(U16 * TPS_parameter)
{
    U8 u8Temp = 0;

    if (MHal_Demod_MB_ReadReg(0x2222, &u8Temp) == FALSE)
        return FALSE;

    if ((u8Temp & 0x02) != 0x02)
    {
        return FALSE; //TPS unlock
    }
    else
    {
        if ( MHal_Demod_MB_ReadReg(0x2224, &u8Temp) == FALSE )
            return FALSE;

        *TPS_parameter = u8Temp & 0x03;                 //Constellation (b1 ~ b0)
        *TPS_parameter |= (u8Temp & 0x70) >> 2;         //Hierarchy (b4 ~ b2)

        if ( MHal_Demod_MB_ReadReg(0x2225, &u8Temp) == FALSE )
            return FALSE;

        *TPS_parameter |= (U16)(u8Temp & 0x07) << 5;    //LP Code Rate (b7 ~ b5)
        *TPS_parameter |= (U16)(u8Temp & 0x70) << 4;    //HP Code Rate (b10 ~ b8)

        if ( MHal_Demod_MB_ReadReg(0x2226, &u8Temp) == FALSE )
            return FALSE;

        *TPS_parameter |= (U16)(u8Temp & 0x03) << 11;   //GI (b12 ~ b11)
        *TPS_parameter |= (U16)(u8Temp & 0x30) << 9;    //FFT (b14 ~ b13)
    }

    return TRUE;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
BOOL MHal_DVBTxC_DVBT2_GetPlpBitMap(U8* u8PlpBitMap)
{
    BOOL   status = TRUE;
    U8     u8Data = 0;
    U8     indx = 0;

    DBG_MSB(printk("MHal_DVBTxC_DVBT2_GetPlpBitMap\n"));

    status &= (MHal_Demod_MB_ReadDspReg(T2_L1_FLAG, &u8Data) == TRUE);
    if (u8Data != 0x30)
    {
        DBG_MSB(printk("\n[MHal_DVBTxC_DVBT2_GetPlpBitMap] Check L1 NOT Ready !! E_DMD_T2_L1_FLAG = 0x%x\n", u8Data));     
        return FALSE;
    }
    while (indx < 32)
    {
        status &= (MHal_Demod_MB_ReadDspReg(T2_PLP_ID_ARR + indx, &u8Data) == TRUE);
        u8PlpBitMap[indx] = u8Data;
        indx++;
    }

    if (status)
    {
        DBG_MSB(printk("\n+++++++++u8PlpBitMap data+++++++++++++++\n"));
        for (indx = 0; indx < 32; indx++)
            DBG_MSB(printk("[%d] ", u8PlpBitMap[indx]));
        DBG_MSB(printk("\n+++++++++u8PlpBitMap end+++++++++++++++\n"));
    }
    return status;
}

/****************************************************************************
Subject:    To get the DVBC parameter
Function:   MHal_DVBTxC_GetDvbcInfo
Parmeter:   point to return parameter
Symbol Rate (b15 ~ b0): 4000~7000
QAM (b23 ~ b16): 0~4 => 16, 32, 64, 128, 256
Return:     TRUE
            FALSE
Remark:   The DVBC parameters will be available after FEC lock
 *****************************************************************************/
B16 MHal_DVBTxC_GetDvbcInfo(U32 * DVBC_parameter)

{
	U8 u8Temp = 0;
	U16 dvb_c_info_temp=0;
    U16 symbol_rate;

	if (MHal_DVBTxC_Lock(E_SYS_DVBC,COFDM_FEC_LOCK_DVBC) == FALSE) // FEC unlock
		return FALSE;

    if (MHal_Demod_MB_ReadReg(EQE2_REG_BASE + 0xc4, &u8Temp) == FALSE)
		return FALSE;
    u8Temp &= 0x7;
	*DVBC_parameter = (U32)u8Temp << 16; // [23:16] = (0...4) -> QAM(16, 32, 64, 128, 256)

    // symbol rate
    if (MHal_Demod_MB_ReadReg(0x20d2, &u8Temp) == FALSE)
        return FALSE;
    symbol_rate = u8Temp;
    if (MHal_Demod_MB_ReadReg(0x20d1, &u8Temp) == FALSE)
        return FALSE;
    symbol_rate = (symbol_rate<<8)|u8Temp;

    if ((symbol_rate < 6902) && (symbol_rate >6898))
    {
        symbol_rate=6900;
    }

    if ((symbol_rate < 6877) && (symbol_rate > 6873))
    {
        symbol_rate=6875;
    }

	*DVBC_parameter |= symbol_rate;

	dvb_c_info_temp = *DVBC_parameter&0xFFFF;
	DBG_MSB(printk("S7 DVBC symbol rate= %d\n",dvb_c_info_temp));
	dvb_c_info_temp = *DVBC_parameter/0x10000;
	switch(dvb_c_info_temp)
	{
		case 0:
			DBG_MSB(printk("S7 DVBC qam mode= 16 QAM\n"));
		break;

		case 1:
			DBG_MSB(printk("S7 DVBC qam mode= 32 QAM\n"));
		break;

		case 2:
			DBG_MSB(printk("S7 DVBC qam mode= 64 QAM\n"));
		break;
		case 3:
			DBG_MSB(printk("S7 DVBC qam mode= 128 QAM\n"));
		break;
		case 4:
			DBG_MSB(printk("S7 DVBC qam mode= 256 QAM\n"));
		break;
		default:
			DBG_MSB(printk("S7 DVBC qam mode= No information\n"));
		break;
	}

	return TRUE;
}

/****************************************************************************
Subject:    To get the DVBT Carrier Freq Offset
Function:   MHal_DVBTxC_Get_FreqOffset
Parmeter:   Frequency offset (in KHz), bandwidth (in MHz)
Return:     E_RESULT_SUCCESS
E_RESULT_FAILURE
Remark:
 *****************************************************************************/
U16 MHal_DVBTxC_Get_FreqOffset(E_SYSTEM eSystems, S32 *pFreqOff, U8 u8BW)
{
    U8  status = TRUE;
    MS_S64 dividend64, divisor64;
#ifdef DEMOD_DVB_T2_MERGE_T
    U8 flag_merged_joint_detect;
#endif

#ifdef DEMOD_DVB_T2_MERGE_T
    if (eSystems == E_SYS_DVBT_T2)
    {
        status &= (MHal_Demod_MB_ReadDspReg(JOINT_DETECTION_FLAG, &flag_merged_joint_detect) == TRUE);
        if (flag_merged_joint_detect & BIT3)
        {
            eSystems = E_SYS_DVBT2;
        }
        else
        {
            eSystems = E_SYS_DVBT;
        }
    }
#endif

    if(eSystems == E_SYS_DVBT)
    {
        MS_S64      FreqCfoTd = 0, FreqCfoFd = 0, FreqIcfo_e3 = 0;
        S32         RegCfoTd = 0, RegCfoFd = 0, RegIcfo = 0;
        U8          reg_frz = 0, reg = 0;

        // td cfo
        status &= MHal_Demod_MB_ReadReg(0x2104+1, &reg_frz);
        status &= MHal_Demod_MB_WriteReg(0x2104+1, reg_frz|0x80);

        status &= MHal_Demod_MB_ReadReg(0x24c6+2, &reg);
        RegCfoTd = reg;

        status &= MHal_Demod_MB_ReadReg(0x24c6+1, &reg);
        RegCfoTd = (RegCfoTd << 8)|reg;

        status &= MHal_Demod_MB_ReadReg(0x24c6, &reg);
        RegCfoTd = (RegCfoTd << 8)|reg;

        if (RegCfoTd & 0x800000)
            RegCfoTd -= 0x1000000;

        // FreqB = (float)u8BW * 8 / 7;
        // FreqCfoTd = RegCfoTd * FreqB * 0.00011642;
        dividend64 = (MS_S64)RegCfoTd * u8BW * 8 * 11642;
        divisor64 = (MS_S64)100000000 * 7;
        FreqCfoTd = div64_s64(dividend64, divisor64);

        status &= MHal_Demod_MB_WriteReg(0x2105, reg_frz&(~0x80));

        // fd cfo
		status &= MHal_Demod_MB_ReadReg(0x15fe, &reg_frz);
		status &= MHal_Demod_MB_WriteReg(0x15fe, reg_frz|0x01); // freeze
		status &= MHal_Demod_MB_WriteReg(0x15ff, 0x01); // load

		status &= MHal_Demod_MB_ReadReg(0x1561, &reg);
		RegCfoFd = reg;

		status &= MHal_Demod_MB_ReadReg(0x1560, &reg);
		RegCfoFd = (RegCfoFd << 8)|reg;

		status &= MHal_Demod_MB_ReadReg(0x155f, &reg);
		RegCfoFd = (RegCfoFd << 8)|reg;

		status &= MHal_Demod_MB_ReadReg(0x155e, &reg);
		RegCfoFd = (RegCfoFd << 8)|reg;

        if (RegCfoFd & 0x1000000)
            RegCfoFd -= 0x2000000;
        // FreqB = (float)u8BW * 8 / 7;
        // FreqCfoFd = (float)((signed int)RegCfoFd<<7)/128 * FreqB * 0.0037253;
        dividend64 = (MS_S64)RegCfoFd * u8BW * 8 * 37253;
        divisor64 = (MS_S64)10000000 * 7;
        FreqCfoFd = div64_s64(dividend64, divisor64);

        // icfo
        MHal_Demod_MB_ReadReg(0x155c, &reg);
        RegIcfo = reg;
        MHal_Demod_MB_ReadReg(0x155c+1, &reg);
        RegIcfo = ((reg<<8)|RegIcfo)>>4;
        if (RegIcfo & 0x400)
            RegIcfo -= 0x800;

        // FreqIcfo = (float)RegIcfo * FreqB / N * 1000;     //unit: kHz
		status &= MHal_Demod_MB_ReadReg(0x2226, &reg);
		reg = reg & 0x30;
		switch (reg)
		{
			case 0x00:	divisor64 = (MS_S64)2048 * 7;  break;
			case 0x20:	divisor64 = (MS_S64)4096 * 7;  break;
			case 0x10:
			default:	divisor64 = (MS_S64)8192 * 7;  break;
		}
        dividend64 = (MS_S64)RegIcfo * u8BW * 8 * 1000000;
        FreqIcfo_e3 = div64_s64(dividend64, divisor64);

		status &= MHal_Demod_MB_WriteReg(0x15fe, reg_frz&~0x01); // freeze
		status &= MHal_Demod_MB_WriteReg(0x15ff, 0x01); // load

        *pFreqOff = div_s64((FreqIcfo_e3 + FreqCfoFd + FreqCfoTd), 1000);

        status &= MHal_Demod_MB_ReadReg(0x155c, &reg);
        if (reg & 0x02)
            *pFreqOff = -*pFreqOff;

        DBG_MSB(printk("$$$$$$$$$$$$$$$$$$$$  DVB-T CFOE = %d\n", *pFreqOff));
    }
    else if (eSystems == E_SYS_DVBT2)
    {
        U8 reg_frz = 0, reg = 0;
        U32 u32_tmp;
        S32 s32_fo_over_fs, s32_mixer_fc_over_fs_current;
        MS_S64 s64_mixer_fc, s64_mixer_fc_current, s64_fo, FreqOff;
        S32 fs = 24000;

        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2f*2), &reg);
        status &= MHal_Demod_MB_WriteReg(FE_REG_BASE+(0x2f*2), (reg&0xcf)|0x30);

        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x02*2)+1, &reg_frz);
        status &= MHal_Demod_MB_WriteReg(FE_REG_BASE+(0x02*2)+1, reg_frz|0x80);

        //Read <26:24>
        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2d*2)+1, &reg);
        u32_tmp = reg & 0x7;
        //Read <23:16>
        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2d*2), &reg);
        u32_tmp = (u32_tmp << 8) | reg;
        //Read <15:8>
        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2c*2)+1, &reg);
        u32_tmp = (u32_tmp << 8) | reg;
        //Read <7:0>
        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2c*2), &reg);
        u32_tmp = (u32_tmp << 8) | reg;

        status &= MHal_Demod_MB_WriteReg(FE_REG_BASE+(0x02*2)+1, reg_frz&0x7f);

        s32_fo_over_fs = (S32)u32_tmp;
        if (u32_tmp & 0x4000000)
            s32_fo_over_fs -= 134217728;
        s64_fo = (MS_S64)s32_fo_over_fs * fs;

        status &= (MHal_Demod_MB_ReadDspReg(T2_FC_H, &reg) == TRUE);
        u32_tmp = reg;
        status &= (MHal_Demod_MB_ReadDspReg(T2_FC_L, &reg) == TRUE);
        u32_tmp = (u32_tmp << 8) | reg;
        s64_mixer_fc = (MS_S64)u32_tmp * 134217728;

        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2b*2)+1, &reg);
        u32_tmp = reg & 0x7;
        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2b*2), &reg);
        u32_tmp = (u32_tmp << 8) | reg;
        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2a*2)+1, &reg);
        u32_tmp = (u32_tmp << 8) | reg;
        status &= MHal_Demod_MB_ReadReg(FE_REG_BASE+(0x2a*2), &reg);
        u32_tmp = (u32_tmp << 8) | reg;
        s32_mixer_fc_over_fs_current = (S32)u32_tmp;
        s64_mixer_fc_current = (MS_S64)s32_mixer_fc_over_fs_current * fs;

        FreqOff = s64_mixer_fc_current + s64_fo - s64_mixer_fc;
        *pFreqOff = div_s64(FreqOff, 134217728);
    }
    else if (eSystems == E_SYS_DVBC)
    {
        U8 reg, reg_frz;
        U16 fif;
        S32 fs = 48000;
        MS_S64 FreqCfo_offset, mixer_rate1, mixer_rate2;
        U32 u32_tmp;
        S32 RegCfo_offset;

        status &= MHal_Demod_MB_ReadReg(EQE_REG_BASE+0x3d, &reg_frz);
        status &= MHal_Demod_MB_WriteReg(EQE_REG_BASE+0x3d, reg_frz|0x01);

        status &= MHal_Demod_MB_ReadReg(EQE2_REG_BASE + 0x75, &reg);
        RegCfo_offset = reg;
        status &= MHal_Demod_MB_ReadReg(EQE2_REG_BASE + 0x74, &reg);
        RegCfo_offset = (RegCfo_offset<<8)|reg;
        status &= MHal_Demod_MB_ReadReg(EQE2_REG_BASE + 0x73, &reg);
        RegCfo_offset = (RegCfo_offset<<8)|reg;
        status &= MHal_Demod_MB_ReadReg(EQE2_REG_BASE + 0x72, &reg);
        RegCfo_offset = (RegCfo_offset<<8)|reg;

        status &= MHal_Demod_MB_WriteReg(EQE_REG_BASE+0x3d, reg_frz&0xfe);
        FreqCfo_offset = (RegCfo_offset<<4)/16;

        status &= MHal_DVBTxC_GetDvbcInfo(&u32_tmp);
        FreqCfo_offset = (-FreqCfo_offset) * (MS_S64)(u32_tmp & 0xffff);

		status &= (MHal_Demod_MB_ReadDspReg(0x16, &reg) == TRUE);
		fif = reg;
		status &= (MHal_Demod_MB_ReadDspReg(0x15, &reg) == TRUE);
		fif = (fif << 8) | reg;

        mixer_rate1 = (MS_S64)fif * (1 << 27);

		status &= MHal_Demod_MB_ReadReg(0x2857, &reg);
		u32_tmp = reg;
		status &= MHal_Demod_MB_ReadReg(0x2856, &reg);
		u32_tmp = (u32_tmp<<8) + reg;
		status &= MHal_Demod_MB_ReadReg(0x2855, &reg);
		u32_tmp = (u32_tmp<<8) + reg;
		status &= MHal_Demod_MB_ReadReg(0x2854, &reg);
		u32_tmp = (u32_tmp<<8) + reg;
        mixer_rate2 = (MS_S64)u32_tmp * fs;

        dividend64 = (mixer_rate2 * 8) - (mixer_rate1 * 8) + FreqCfo_offset;
        divisor64 = (MS_S64)134217728 * 8;
        *pFreqOff = div64_s64(dividend64, divisor64);
    }
    else
    {
        status = FALSE;
    }

    if(status)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Get_FreqOffset OK\n"));
        return  TRUE;
    }
    else
    {
        DBG_MSB(printk(" @MHal_DVBTxC_Get_FreqOffset NG\n"));
        return FALSE;
    }

}

/****************************************************************************
Subject:    To get the Cell Identification
Function:   MHal_DVBTxC_GetCellId
Parmeter:     *cell_id, the CELL_ID value will updated to the content it points to
Return:     TRUE
FALSE
Remark:
 *****************************************************************************/
B16 MHal_DVBTxC_GetCellId(E_SYSTEM eSystems, U16 *cell_id)
{
    U8  id = 0;
    U8  status = TRUE;
#ifdef DEMOD_DVB_T2_MERGE_T
    U8 flag_merged_joint_detect;
#endif

#ifdef DEMOD_DVB_T2_MERGE_T
    if (eSystems == E_SYS_DVBT_T2)
    {
        status &= (MHal_Demod_MB_ReadDspReg(JOINT_DETECTION_FLAG, &flag_merged_joint_detect) == TRUE);
        if (flag_merged_joint_detect & BIT3)
        {
            eSystems = E_SYS_DVBT2;
        }
        else
        {
            eSystems = E_SYS_DVBT;
        }
    }
#endif

    if (eSystems == E_SYS_DVBT2)
    {
        status &= MHal_Demod_MB_ReadReg(T2L1_REG_BASE+(0x38*2)+1, &id);
        *cell_id = (U16)id << 8;
        status &= MHal_Demod_MB_ReadReg(T2L1_REG_BASE+(0x38*2), &id);
        *cell_id |= id;
    }
    else
    {
        status &= MHal_Demod_MB_WriteReg(0x22fe, 0x01);
        status &= MHal_Demod_MB_ReadReg(0x222a, &id);
        *cell_id = (U16)id << 8;
        status &= MHal_Demod_MB_ReadReg(0x222b, &id);
        *cell_id |= id;
        status &= MHal_Demod_MB_WriteReg(0x22fe, 0x00);
    }

    if(status)
    {
        DBG_MSB(printk(" @MHal_DVBTxC_GetCellId OK\n"));
        return  TRUE;
    }
    else
    {
        DBG_MSB(printk(" @MHal_DVBTxC_GetCellId NG\n"));
        return FALSE;
    }
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
BOOL MHal_DVBTxC_DVBT2_GetPlpGroupID(U8 u8PlpID, U8* u8GroupID)
{
    MS_BOOL   status = TRUE;
    MS_U8 u8Data = 0;

    status &= (MHal_Demod_MB_ReadDspReg(T2_L1_FLAG, &u8Data) == TRUE);         // check L1 ready
    if (u8Data != 0x30)
    {
        printk(">>>dvbt2 L1 not ready yet\n");
        return FALSE;
    }

    status &= (MHal_Demod_MB_ReadDspReg(T2_DVBT2_LOCK_HIS, &u8Data) == TRUE);
    if ((u8Data & BIT7) == 0x00)
    {
        printk(">>>dvbt2 is un-lock\n");
        return FALSE;
    }

    // assign PLP-ID value
    status &= MHal_Demod_MB_WriteReg(T2L1_REG_BASE + (0x78) * 2, u8PlpID);
    status &= MHal_Demod_MB_WriteReg(T2L1_REG_BASE + (0x01) * 2 + 1, 0x01); // MEM_EN
    udelay(1000);
    status &= MHal_Demod_MB_ReadReg(T2L1_REG_BASE + (0x79) * 2, u8GroupID);
    status &= MHal_Demod_MB_WriteReg(T2L1_REG_BASE + (0x01) * 2 + 1, 0x00); // ~MEM_EN

    return status;
}

/************************************************************************************************
  Subject:
  Function:
  Parmeter:
  Return:
  Remark:
*************************************************************************************************/
BOOL MHal_DVBTxC_DVBT2_SetPlpGroupID(U8 u8PlpID, U8 u8GroupID)
{
    MS_BOOL   status = TRUE;

    // assign Group-ID and PLP-ID value (must be written in order)
    status &= (MHal_Demod_MB_WriteDspReg(T2_GROUP_ID, u8GroupID) == TRUE);
    status &= (MHal_Demod_MB_WriteDspReg(T2_PLP_ID, u8PlpID) == TRUE);

    return status;
}

//===================================================================
//Function:
//Parmeter:
//u8Data:
//Return:
//Remark:
//===================================================================
BOOL MHal_DVBTxC_GetSpectrumInv(E_SYSTEM system)
{
    MS_U8 u8Data = 0;

    switch(system)
    {
        case E_SYS_DVBT:
            MHal_Demod_MB_WriteReg(0x15fe, 0x01);
            MHal_Demod_MB_WriteReg(0x15ff, 0x01);
            MHal_Demod_MB_ReadReg(0x155c, &u8Data);
            MHal_Demod_MB_WriteReg(0x15fe, 0x00);
            MHal_Demod_MB_WriteReg(0x15ff, 0x01);
            if (u8Data & BIT1)
                return TRUE;
            break;

        case E_SYS_DVBT2:
            MHal_Demod_MB_ReadReg(T2TDP_REG_BASE + (0x41 * 2), &u8Data);
            if (u8Data & BIT2)
                return TRUE;
            break;

        case E_SYS_DVBC:
            MHal_Demod_MB_ReadReg(EQE2_REG_BASE + 0xe0, &u8Data);
            if (u8Data & BIT5)
                return TRUE;
            break;

        default:
            break;
    }
    
    return FALSE;
}

#define T2_PKE_ACCU_FLAG_READ   BIT0
#define T2_PKE_ACCU_FALG_CLEAR  BIT1
//===================================================================
//Function:
//Parmeter:
//u8Data:
//Return:
//Remark:
//===================================================================
BOOL MHal_DVBTxC_GetPacketErrAccu(U32 *pu32PktErr, BOOL bClear_Accu, E_SYSTEM system)
{     
    U16 for_loop_idx;
    U8 reg, stat;
    U32 pkt_err_accu=0;
    S32 status = TRUE;
#ifdef DEMOD_DVB_T2_MERGE_T
    U8 flag_merged_joint_detect;
#endif
    static U32 last_pkt_err_accu=0;

#ifdef DEMOD_DVB_T2_MERGE_T
    if (system == E_SYS_DVBT_T2)
    {
        status &= (MHal_Demod_MB_ReadDspReg(JOINT_DETECTION_FLAG, &flag_merged_joint_detect) == TRUE);
        if (flag_merged_joint_detect & BIT3)
        {
            system = E_SYS_DVBT2;
        }
        else
        {
            system = E_SYS_DVBT;
        }
    }
#endif
		
	*pu32PktErr=0;
    switch(system)
    {
        case E_SYS_DVBT2:
            status &= MHal_Demod_MB_ReadDspReg(T2_DVBT2_LOCK_HIS, &stat);
            if (stat & BIT7)
            {
                //read packet error
                status &= MHal_Demod_MB_WriteDspReg(T2_PKT_READ_CTRL, T2_PKE_ACCU_FLAG_READ);  //inform demod to output the latest accu number

                //short delay
                for(for_loop_idx=0 ; for_loop_idx<=30 ; for_loop_idx++)
                {
                    status &= MHal_Demod_MB_ReadDspReg(T2_PKT_READ_CTRL, &reg);
                    if((reg & T2_PKE_ACCU_FLAG_READ) == 0)  //demod response
                    {
                        status &= MHal_Demod_MB_ReadDspReg(T2_PKT_ACCU_3, &reg);
                        pkt_err_accu = reg;
                        status &= MHal_Demod_MB_ReadDspReg(T2_PKT_ACCU_2, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;
                        status &= MHal_Demod_MB_ReadDspReg(T2_PKT_ACCU_1, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;
                        status &= MHal_Demod_MB_ReadDspReg(T2_PKT_ACCU_0, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;

                        if ((last_pkt_err_accu > 1) && (pkt_err_accu == 0))
                        {
                            pkt_err_accu = last_pkt_err_accu / 2;
                            last_pkt_err_accu = 0;
                        }
                        else
                        {
                            last_pkt_err_accu = pkt_err_accu;
                        }
                        *pu32PktErr = pkt_err_accu;
                        break;
                    }
                    msleep(1);
                }
                if (for_loop_idx > 30)
                {
                    *pu32PktErr = last_pkt_err_accu;
                    printk("DVBT2 GetPacketErrAccu timeout\n");
                }
            } 

            if(bClear_Accu)
            {
                //clear the packet error accu number
                status &= MHal_Demod_MB_WriteDspReg(T2_PKT_READ_CTRL, T2_PKE_ACCU_FALG_CLEAR);  //inform demod to clear the accumulator
            }
            break;

        case E_SYS_DVBT:
            MHal_Demod_MB_ReadReg(0x20C0, &stat);
            if (stat & BIT6)
            {
                //read packet error
                MHal_Demod_MB_WriteReg(MBX_REG_BASE + 0x17, 0x01);  //inform demod to output the latest accu number

                //short delay
                for(for_loop_idx=0 ; for_loop_idx<=20 ; for_loop_idx++)
                {
                    MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x17, &reg);
                    if(reg==0x02)  //demod response
                    {
                        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x18, &reg);
                        pkt_err_accu = reg;
                        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x19, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;
                        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x1e, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;
                        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x1f, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;

                        *pu32PktErr = pkt_err_accu;
                        break;
                    }
                    msleep(1);
                }
                if (for_loop_idx > 20)
                    printk("DVBT GetPacketErrAccu timeout\n");
            } 

            if(bClear_Accu)
            {
                //clear the packet error accu number
                MHal_Demod_MB_WriteReg(MBX_REG_BASE + 0x17, 0x03);  //inform demod to output the latest accu number
            }
            break;

        case E_SYS_DVBC:
            MHal_Demod_MB_ReadReg(0x23E0, &stat);
            if (stat == 0x0C)
            {
                //read packet error
                MHal_Demod_MB_WriteReg(MBX_REG_BASE + 0x08, 0x01);  //inform demod to output the latest accu number

                //short delay
                for(for_loop_idx=0 ; for_loop_idx<=20 ; for_loop_idx++)
                {
                    MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x08, &reg);
                    if(reg==0x02)  //demod response
                    {
                        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x04, &reg);
                        pkt_err_accu = reg;
                        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x05, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;
                        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x06, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;
                        MHal_Demod_MB_ReadReg(MBX_REG_BASE + 0x07, &reg);
                        pkt_err_accu = (pkt_err_accu << 8) | reg;

                        *pu32PktErr = pkt_err_accu;
                        break;
                    }
                    msleep(1);
                }
                if (for_loop_idx > 20)
                    printk("DVBC GetPacketErrAccu timeout\n");
            } 

            if(bClear_Accu)
            {
                //clear the packet error accu number
                MHal_Demod_MB_WriteReg(MBX_REG_BASE + 0x08, 0x03);  //inform demod to output the latest accu number
            }
            break;

        default:
            break;
    }
		
    if (status)
        return TRUE;
    else
        return FALSE;
}

//===================================================================
//Function:
//Parmeter:
//u8Data:
//Return:
//Remark:
//===================================================================
BOOL MHal_DVBTxC_GetUnlockStatus(E_SYSTEM system, U8 *CurrState, U32 *UnlockFlag)
{
    MS_U8 u8Data = 0;
    BOOL status = TRUE;
    U32 flag = 0;

    switch(system)
    {
        case E_SYS_DVBT_T2:
            status &= MHal_Demod_MB_ReadReg(0x20c4, &u8Data);
            *CurrState = u8Data;

            status &= MHal_Demod_MB_ReadDspReg(T_T2_UNLOCK_FLAG_2, &u8Data);
            flag = u8Data;
            status &= MHal_Demod_MB_ReadDspReg(T_T2_UNLOCK_FLAG_1, &u8Data);
            flag = (flag << 8) | u8Data;
            status &= MHal_Demod_MB_ReadDspReg(T_T2_UNLOCK_FLAG_0, &u8Data);
            flag = (flag << 8) | u8Data;
            *UnlockFlag = flag;
            return status;

        case E_SYS_DVBC:
            status &= MHal_Demod_MB_ReadReg(0x23e0, &u8Data);
            *CurrState = u8Data;

            status &= MHal_Demod_MB_ReadDspReg(E_DMD_DVBC_UNLOCK_FLAG_2, &u8Data);
            flag = u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_DVBC_UNLOCK_FLAG_1, &u8Data);
            flag = (flag << 8) | u8Data;
            status &= MHal_Demod_MB_ReadDspReg(E_DMD_DVBC_UNLOCK_FLAG_0, &u8Data);
            flag = (flag << 8) | u8Data;
            *UnlockFlag = flag;
            return status;

        default:
            break;
    }
    
    return FALSE;
}

