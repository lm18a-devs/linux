//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (!��MStar Confidential Information!�L) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------

#include "drvDMD_ISDBT.h"

#ifdef MSOS_TYPE_LINUX_KERNEL
#include <linux/kernel.h>
#include <linux/delay.h>
#else
#include <stdio.h>
#include <math.h>
#endif

#if DMD_ISDBT_3PARTY_EN
#include "mhal_demod_reg.h"
#endif

//-------------------------------------------------------------------------------------------------
//  Driver Compiler Options
//-------------------------------------------------------------------------------------------------

#ifndef BIN_RELEASE
#define BIN_RELEASE                 0
#endif

//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------

#define _RIU_READ_BYTE(addr)        (*(volatile MS_U8*)(RIU_ARM_BASE + BYTE2REAL(addr & 0x000FFFFF))) 
#define _RIU_WRITE_BYTE(addr, val)  (*(volatile MS_U8*)(RIU_ARM_BASE + BYTE2REAL(addr & 0x000FFFFF)) = val)

#define HAL_INTERN_ISDBT_DBINFO(y)   //y

#ifndef MBRegBase
#define MBRegBase               0x112600UL
#endif
#ifndef MBRegBase_DMD1
#define MBRegBase_DMD1          0x112400UL
#endif
#ifndef DMDMcuBase
#define DMDMcuBase              0x103480UL
#endif

#define REG_ISDBT_LOCK_STATUS   0x11F5
#define ISDBT_TDP_REG_BASE      0x1400
#define ISDBT_FDP_REG_BASE      0x1500
#define ISDBT_FDPEXT_REG_BASE   0x1600
#define ISDBT_OUTER_REG_BASE    0x1700 

#define ISDBT_MIU_CLIENTW_ADDR      0x00
#define ISDBT_MIU_CLIENTR_ADDR      0x00
#define ISDBT_MIU_CLIENTW_MASK      0x00
#define ISDBT_MIU_CLIENTR_MASK      0x00
#define ISDBT_MIU_CLIENTW_BIT_MASK  0x00
#define ISDBT_MIU_CLIENTR_BIT_MASK  0x00

#define DMD_ISDBT_TBVA_EN       1
#define TDI_MIU_FREE_BY_DRV     0
#define REMAP_RST_BY_DRV        0
#define POW_SAVE_BY_DRV         0
#define TDI_USE_DRAM_SIZE       0x500000
 
 #define FW_RUN_ON_DRAM_MODE     0 // 0: FWdownload to SRAM, 1: FW download to DRAM

#if (FW_RUN_ON_DRAM_MODE==1)
#define SRAM_OFFSET 0x8000

extern void *memcpy(void *destination, const void *source, size_t num);
#endif

//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------

const MS_U8 INTERN_ISDBT_table[] = {
    #include "LM18A_1_1_1B_ISDBT_demod.dat"
};

#ifndef UTPA2
static const float _LogApproxTableX[80] =
{ 1.00, 1.30, 1.69, 2.20, 2.86, 3.71, 4.83, 6.27, 8.16, 10.60, 
  13.79, 17.92, 23.30, 30.29, 39.37, 51.19, 66.54, 86.50, 112.46, 146.19, 
  190.05, 247.06, 321.18, 417.54, 542.80, 705.64, 917.33, 1192.53, 1550.29, 2015.38, 
  2620.00, 3405.99, 4427.79, 5756.13, 7482.97, 
  9727.86, 12646.22, 16440.08, 21372.11, 27783.74, 
  36118.86, 46954.52, 61040.88, 79353.15, 103159.09, 
  134106.82, 174338.86, 226640.52, 294632.68, 383022.48,
  497929.22, 647307.99, 841500.39, 1093950.50, 1422135.65, 
  1848776.35, 2403409.25, 3124432.03, 4061761.64, 5280290.13, 
  6864377.17, 8923690.32, 11600797.42, 15081036.65, 19605347.64, 
  25486951.94, 33133037.52, 43072948.77, 55994833.40, 72793283.42, 
  94631268.45, 123020648.99, 159926843.68, 207904896.79, 270276365.82, 
  351359275.57,456767058.24, 593797175.72, 771936328.43, 1003517226.96
};

static const float _LogApproxTableY[80] =
{ 0.00, 0.11, 0.23, 0.34, 0.46, 0.57, 0.68, 0.80, 0.91, 1.03, 
  1.14, 1.25, 1.37, 1.48, 1.60, 1.71, 1.82, 1.94, 2.05, 2.16, 
  2.28, 2.39, 2.51, 2.62  2.73, 2.85, 2.96, 3.08, 3.19, 3.30, 
  3.42, 3.53, 3.65, 3.76, 3.87, 3.99, 4.10, 4.22, 4.33, 4.44, 
  4.56, 4.67, 4.79, 4.90, 5.01, 5.13, 5.24, 5.36, 5.47, 5.58, 
  5.70, 5.81, 5.93, 6.04, 6.15, 6.27, 6.38, 6.49, 6.60, 6.72,
  6.83, 6.95, 7.06, 7.17, 7.29, 7.40, 7.52, 7.63, 7.74, 7.86,
  7.97, 8.08, 8.20, 8.31, 8.43, 8.54, 8.65, 8.77, 8.88, 9.00
};
#else
static const MS_U32 _LogApproxTableX[58] =
{ 1, 2, 3, 4, 6, 8, 10, 13, 17, 23, //10
  30, 39, 51, 66, 86, 112, 146, 190, 247, 321, //10
  417, 542, 705, 917, 1192, 1550, 2015, 2620, 3405, 4427, //10
  5756, 7482, 9727, 12646, 16440, 21372, 27783, 36118, 46954, 61040, //10
  79353, 103159, 134106, 174338, 226640, 294632, 383022, 497929, 647307, 841500, //10
  1093950, 1422135, 1848776, 2403409, 3124432, 4061761, 5280290, 6160384 //8
};

static const MS_U16 _LogApproxTableY[58] =
{    0,  30,  47,  60,  77,  90, 100, 111, 123, 136,
   147, 159, 170, 181, 193, 204, 216, 227, 239, 250,
   262, 273, 284, 296, 307, 319, 330, 341, 353, 364,
   376, 387, 398, 410, 421, 432, 444, 455, 467, 478,
   489, 501, 512, 524, 535, 546, 558, 569, 581, 592,
   603, 615, 626, 638, 649, 660, 672, 678
};
#endif

//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------

extern MS_U8 u8DMD_ISDBT_DMD_ID;

extern DMD_ISDBT_ResData *psDMD_ISDBT_ResData;

//-------------------------------------------------------------------------------------------------
//  Local Functions
//-------------------------------------------------------------------------------------------------
#ifndef UTPA2

#ifndef MSOS_TYPE_LINUX
static float Log10Approx(float flt_x)
{
    MS_U8 indx = 0;

    do {
        if (flt_x < _LogApproxTableX[indx])
            break;
        indx++;
    }while (indx < 79);   //stop at indx = 80

    return _LogApproxTableY[indx];
}
#endif

static MS_U16 _CALCULATE_SQI(float fber)
{
    float flog_ber;
    MS_U16 u16SQI;

    #ifdef MSOS_TYPE_LINUX
    flog_ber = (float)log10((double)fber);
    #else
    if (fber != 0.0)
        flog_ber = (float)(-1.0*Log10Approx((double)(1.0 / fber)));
    else
        flog_ber = -8.0;//when fber=0 means u16SQI=100
    #endif

    //PRINT("dan fber = %f\n", fber);
    //PRINT("dan flog_ber = %f\n", flog_ber);
    // Part 2: transfer ber value to u16SQI value.
    if (flog_ber <= ( - 7.0))
    {
        u16SQI = 100;    //*quality = 100;
    }
    else if (flog_ber < -6.0)
    {
        u16SQI = (90+((( - 6.0) - flog_ber) / (( - 6.0) - ( - 7.0))*(100-90)));
    }
    else if (flog_ber < -5.5)
    {
        u16SQI = (80+((( - 5.5) - flog_ber) / (( - 5.5) - ( - 6.0))*(90-80)));
    }
    else if (flog_ber < -5.0)
    {
        u16SQI = (70+((( - 5.0) - flog_ber) / (( - 5.0) - ( - 5.5))*(80-70)));
    }
    else if (flog_ber < -4.5)
    {
        u16SQI = (60+((( - 4.5) - flog_ber) / (( -4.5) - ( - 5.0))*(70-50)));
    }
    else if (flog_ber < -4.0)
    {
        u16SQI = (50+((( - 4.0) - flog_ber) / (( - 4.0) - ( - 4.5))*(60-50)));
    }
    else if (flog_ber < -3.5)
    {
        u16SQI = (40+((( - 3.5) - flog_ber) / (( - 3.5) - ( - 4.0))*(50-40)));
    }
    else if (flog_ber < -3.0)
    {
        u16SQI = (30+((( - 3.0) - flog_ber) / (( - 3.0) - ( - 3.5))*(40-30)));
    }
    else if (flog_ber < -2.5)
    {
        u16SQI = (20+((( - 2.5) - flog_ber) / (( - 2.5) - ( -3.0))*(30-20)));
    }
    else if (flog_ber < -2.0)
    {
        u16SQI = (0+((( - 2.0) - flog_ber) / (( - 2.0) - ( - 2.5))*(20-0)));
    }
    else
    {
        u16SQI = 0;
    }

    return u16SQI;
}

#else // #ifndef UTPA2

static MS_U16 Log10Approx(MS_U32 flt_x)
{
    MS_U8 indx = 0;

    do {
        if (flt_x < _LogApproxTableX[indx])
            break;
        indx++;
    }while (indx < 57);   //stop at indx = 58

    return _LogApproxTableY[indx];
}

static MS_U16 _CALCULATE_SQI(MS_U32 BerValue, MS_U32 BerPeriod)
{
    MS_U16 u16SQI = 0;

    // BerPeriod = 188*8*128*32 = 6160384
    // Log10Approx(6160384) = 678
    
    if ( BerValue == 0)
    {
        u16SQI = 100;    //*quality = 100;
    }
    else if (BerValue * 1000000 < BerPeriod)
    {
        u16SQI = ((9000 + (-600 + 678 - Log10Approx(BerValue))*10)/100);
    }
    else if (BerValue * 316228 < BerPeriod)
    {
        u16SQI = ((8000 + (-550 + 678 - Log10Approx(BerValue))*20)/100);
    }
    else if (BerValue * 100000 < BerPeriod)
    {
        u16SQI = ((7000 + (-500 + 678 - Log10Approx(BerValue))*20)/100);
    }
    else if (BerValue * 31623 < BerPeriod)
    {
        u16SQI = ((6000 + (-450 + 678 - Log10Approx(BerValue))*20)/100);
    }
    else if (BerValue * 10000 < BerPeriod)
    {
         u16SQI = ((5000 + (-400 + 678 - Log10Approx(BerValue))*20)/100);
    }
    else if (BerValue * 3162 < BerPeriod)
    {
         u16SQI = ((4000 + (-350 + 678 - Log10Approx(BerValue))*20)/100);
    }
    else if (BerValue * 1000 < BerPeriod)
    {
         u16SQI = ((3000 + (-300 + 678 - Log10Approx(BerValue))*20)/100);
    }
    else if (BerValue * 316 < BerPeriod)
    {
         u16SQI = ((2000 + (-250 + 678 - Log10Approx(BerValue))*20)/100);
    }
    else if (BerValue * 100 < BerPeriod)
    {
         u16SQI = ((0 + (-200 + 678 - Log10Approx(BerValue))*40)/100);
    }
    else
    {
         u16SQI = 0;
    }

    return u16SQI;
}

#endif // #ifndef UTPA2

static MS_U8 _HAL_DMD_RIU_ReadByte(MS_U32 u32Addr)
{
    #if !DMD_ISDBT_UTOPIA_EN && !DMD_ISDBT_UTOPIA2_EN
    return  _RIU_READ_BYTE(u32Addr);
    #else
    return _RIU_READ_BYTE(((u32Addr) << 1) - ((u32Addr) & 1));
    #endif
}

static void _HAL_DMD_RIU_WriteByte(MS_U32 u32Addr, MS_U8 u8Value)
{
    #if !DMD_ISDBT_UTOPIA_EN && !DMD_ISDBT_UTOPIA2_EN
    _RIU_WRITE_BYTE(u32Addr , u8Value);
    #else
    _RIU_WRITE_BYTE(((u32Addr) << 1) - ((u32Addr) & 1), u8Value);
    #endif
}

static void _HAL_DMD_RIU_WriteByteMask(MS_U32 u32Addr, MS_U8 u8Value, MS_U8 u8Mask)
{
    #if !DMD_ISDBT_UTOPIA_EN && !DMD_ISDBT_UTOPIA2_EN
    _RIU_WRITE_BYTE(u32Addr, (_RIU_READ_BYTE(u32Addr) & ~(u8Mask)) | ((u8Value) & (u8Mask)));
    #else
    _RIU_WRITE_BYTE((((u32Addr) <<1) - ((u32Addr) & 1)), (_RIU_READ_BYTE((((u32Addr) <<1) - ((u32Addr) & 1))) & ~(u8Mask)) | ((u8Value) & (u8Mask)));
    #endif
}

static MS_BOOL _MBX_WriteReg(MS_U16 u16Addr, MS_U8 u8Data)
{
    #if !DMD_ISDBT_UTOPIA_EN && !DMD_ISDBT_UTOPIA2_EN
    DMD_ISDBT_ResData *pRes = psDMD_ISDBT_ResData + u8DMD_ISDBT_DMD_ID;
    #endif
    
    MS_U8 u8CheckCount;
    MS_U8 u8CheckFlag = 0xFF;
    
    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x00, (u16Addr&0xff));
    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x01, (u16Addr>>8));
    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x10, u8Data);
    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x1E, 0x01);

    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)|0x02);    // assert interrupt to VD MCU51
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)&(~0x02)); // de-assert interrupt to VD MCU51

    for (u8CheckCount=0; u8CheckCount < 10; u8CheckCount++)
    {
        u8CheckFlag = _HAL_DMD_RIU_ReadByte(MBRegBase + 0x1E);
        if ((u8CheckFlag&0x01)==0)
            break;
        #if DMD_ISDBT_UTOPIA_EN || DMD_ISDBT_UTOPIA2_EN
        MsOS_DelayTask(1);
        #else
        pRes->sDMD_ISDBT_InitData.DelayMS(1);
        #endif
    }

    if (u8CheckFlag&0x01)
    {
        PRINT("ERROR: ISDBT INTERN DEMOD MBX WRITE TIME OUT!\n");
        return FALSE;
    }

    return TRUE;
}

static MS_BOOL _MBX_ReadReg(MS_U16 u16Addr, MS_U8 *u8Data)
{
    #if !DMD_ISDBT_UTOPIA_EN && !DMD_ISDBT_UTOPIA2_EN
    DMD_ISDBT_ResData *pRes = psDMD_ISDBT_ResData + u8DMD_ISDBT_DMD_ID;
    #endif
    
    MS_U8 u8CheckCount;
    MS_U8 u8CheckFlag = 0xFF;

    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x00, (u16Addr&0xff));
    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x01, (u16Addr>>8));
    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x1E, 0x02);

    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)|0x02);    // assert interrupt to VD MCU51
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)&(~0x02)); // de-assert interrupt to VD MCU51

    for (u8CheckCount=0; u8CheckCount < 10; u8CheckCount++)
    {
        u8CheckFlag = _HAL_DMD_RIU_ReadByte(MBRegBase + 0x1E);
        if ((u8CheckFlag&0x02)==0)
        {
           *u8Data = _HAL_DMD_RIU_ReadByte(MBRegBase + 0x10);
            break;
        }
        #if DMD_ISDBT_UTOPIA_EN || DMD_ISDBT_UTOPIA2_EN
        MsOS_DelayTask(1);
        #else
        pRes->sDMD_ISDBT_InitData.DelayMS(1);
        #endif
    }

    if (u8CheckFlag&0x02)
    {
        PRINT("ERROR: ISDBT INTERN DEMOD MBX READ TIME OUT!\n");
        return FALSE;
    }

    return TRUE;
}

static void _HAL_INTERN_ISDBT_InitClk(void)
{
    #if (FW_RUN_ON_DRAM_MODE == 1)
    DMD_ISDBT_ResData *pRes = psDMD_ISDBT_ResData + u8DMD_ISDBT_DMD_ID;
    MS_U16 u16_temp_val = 0;	
    #endif

    // SRAM End Address
    _HAL_DMD_RIU_WriteByte(0x111707,0xff);
    _HAL_DMD_RIU_WriteByte(0x111706,0xff);

    // DRAM Disable
    _HAL_DMD_RIU_WriteByte(0x111718,_HAL_DMD_RIU_ReadByte(0x111718)&(~0x04));
    
    _HAL_DMD_RIU_WriteByteMask(0x101e39, 0x00, 0x03);

    _HAL_DMD_RIU_WriteByte(0x1128d0, 0x01);
    _HAL_DMD_RIU_WriteByte(0x10331f, 0x00);
    #if (FW_RUN_ON_DRAM_MODE == 1)
    _HAL_DMD_RIU_WriteByte(0x10331e, 0x00);   // 172 Mhz
    #else
    _HAL_DMD_RIU_WriteByte(0x10331e, 0x10);   // 108 Mhz 
    #endif

    _HAL_DMD_RIU_WriteByte(0x103301, 0x00); //ts clock = 7.2M
    _HAL_DMD_RIU_WriteByte(0x103300, 0x0b);
    _HAL_DMD_RIU_WriteByte(0x103309, 0x00);
    _HAL_DMD_RIU_WriteByte(0x103308, 0x00);
    _HAL_DMD_RIU_WriteByte(0x103302, 0x01);
    _HAL_DMD_RIU_WriteByte(0x103302, 0x00);

    _HAL_DMD_RIU_WriteByte(0x103321, 0x00); //Add in MACRUS
    _HAL_DMD_RIU_WriteByte(0x103320, 0x00);
        
    _HAL_DMD_RIU_WriteByte(0x111f0b, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f0a, 0x00);
    _HAL_DMD_RIU_WriteByte(0x103315, 0x00);
    _HAL_DMD_RIU_WriteByte(0x103314, 0x00);
    _HAL_DMD_RIU_WriteByte(0x1128d0, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f13, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f12, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f25, 0x04);
    _HAL_DMD_RIU_WriteByte(0x111f31, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f30, 0x01);
    _HAL_DMD_RIU_WriteByte(0x111f3b, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f3a, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f43, 0x80);
    _HAL_DMD_RIU_WriteByte(0x111f42, 0x08);
    _HAL_DMD_RIU_WriteByte(0x111f45, 0x04);
    _HAL_DMD_RIU_WriteByte(0x111f44, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f63, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f62, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f65, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f64, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f66, 0x01);
    _HAL_DMD_RIU_WriteByte(0x111f69, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f68, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f6b, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f6a, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f6d, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f6c, 0x40);
    _HAL_DMD_RIU_WriteByte(0x111f6f, 0x01);
    _HAL_DMD_RIU_WriteByte(0x111f6e, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f71, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f70, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f73, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111f72, 0x04);
    _HAL_DMD_RIU_WriteByte(0x111f75, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f74, 0x04); //Modify in MACRUS
    _HAL_DMD_RIU_WriteByte(0x111f77, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f76, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f79, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f78, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f7b, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f7a, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f7d, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f7c, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f7f, 0x44);
    _HAL_DMD_RIU_WriteByte(0x111f7e, 0x44);
    
    _HAL_DMD_RIU_WriteByte(0x111f83, 0x00); //Add in MACRUS
    _HAL_DMD_RIU_WriteByte(0x111f82, 0x04);    
    
    _HAL_DMD_RIU_WriteByte(0x111fe1, 0x01);
    _HAL_DMD_RIU_WriteByte(0x111fe0, 0x08);
    _HAL_DMD_RIU_WriteByte(0x111ff0, 0x08);
    _HAL_DMD_RIU_WriteByte(0x111fe3, 0x08);
    _HAL_DMD_RIU_WriteByte(0x111fe2, 0x10);
    _HAL_DMD_RIU_WriteByte(0x111feb, 0x11);
    _HAL_DMD_RIU_WriteByte(0x111fea, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111fef, 0x00);
    _HAL_DMD_RIU_WriteByte(0x111fee, 0x88);
    _HAL_DMD_RIU_WriteByte(0x15298f, 0x44);
    _HAL_DMD_RIU_WriteByte(0x15298e, 0x44);
    _HAL_DMD_RIU_WriteByte(0x152991, 0x44);
    _HAL_DMD_RIU_WriteByte(0x152990, 0x44);
    _HAL_DMD_RIU_WriteByte(0x152992, 0x04);
    _HAL_DMD_RIU_WriteByte(0x1529e5, 0x00);
    _HAL_DMD_RIU_WriteByte(0x1529e4, 0x04);
    _HAL_DMD_RIU_WriteByte(0x152971, 0x10);
    _HAL_DMD_RIU_WriteByte(0x152970, 0x01);
    _HAL_DMD_RIU_WriteByte(0x152997, 0x01);
    _HAL_DMD_RIU_WriteByte(0x152996, 0x08);

    #if (FW_RUN_ON_DRAM_MODE == 1)
    // Bank number of MCU 2 bank[0x1634] may be chaanged with different ASIC
    _HAL_DMD_RIU_WriteByte(0x163400, 0x00); //set start address reg_sram_start_addr_1
    _HAL_DMD_RIU_WriteByte(0x163401, 0x00); //set start address reg_sram_start_addr_1
    _HAL_DMD_RIU_WriteByte(0x163402, 0x00); //set start address reg_sram_end_addr_1
    _HAL_DMD_RIU_WriteByte(0x163403, 0x00); //set start address reg_sram_end_addr_1
    _HAL_DMD_RIU_WriteByte(0x163404, (SRAM_OFFSET & 0xFF)); //set start address reg_sram_start_addr_0
    _HAL_DMD_RIU_WriteByte(0x163405, (SRAM_OFFSET >> 8)); //set start address reg_sram_start_addr_0
    _HAL_DMD_RIU_WriteByte(0x163406, 0x00); //set stop address  reg_sram_end_addr_0
    _HAL_DMD_RIU_WriteByte(0x163407, 0x80); //set stop address  reg_sram_end_addr_0

    _HAL_DMD_RIU_WriteByte(0x163408, 0x00);
    _HAL_DMD_RIU_WriteByte(0x163409, 0x00);
    _HAL_DMD_RIU_WriteByte(0x16340A, 0x00);
    _HAL_DMD_RIU_WriteByte(0x16340B, 0x00);
    _HAL_DMD_RIU_WriteByte(0x16340C, 0x00);
    _HAL_DMD_RIU_WriteByte(0x16340D, 0x00);
    u16_temp_val = SRAM_OFFSET - 1;
    _HAL_DMD_RIU_WriteByte(0x16340E, (u16_temp_val & 0xFF));
    _HAL_DMD_RIU_WriteByte(0x16340F, (u16_temp_val >> 8));

    _HAL_DMD_RIU_WriteByte(0x163418, 0x04);

    // SDRAM address offset
    u16_temp_val = (MS_U16)(((pRes->sDMD_ISDBT_InitData.u32TdiStartAddr * 16) + (4 * 1024 * 1024))>>16);
    _HAL_DMD_RIU_WriteByte(0x16341b,(MS_U8)(u16_temp_val>>8));
    _HAL_DMD_RIU_WriteByte(0x16341a,(MS_U8)u16_temp_val);
    
    _HAL_DMD_RIU_WriteByte(0x16341C, 0x01);
   
    //turn on dmd arbiter
    _HAL_DMD_RIU_WriteByteMask(0x112001, 0x00, 0x10);
    #endif


    _HAL_DMD_RIU_WriteByteMask(0x101e39, 0x03, 0x03);
}

static MS_BOOL _HAL_INTERN_ISDBT_Ready(void)
{
    #if !DMD_ISDBT_UTOPIA_EN && !DMD_ISDBT_UTOPIA2_EN
    DMD_ISDBT_ResData *pRes = psDMD_ISDBT_ResData + u8DMD_ISDBT_DMD_ID;
    #endif
    
    MS_U8 udata = 0x00;

    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x1E, 0x02);

    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)|0x02);    // assert interrupt to VD MCU51
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)&(~0x02)); // de-assert interrupt to VD MCU51

    #if DMD_ISDBT_UTOPIA_EN || DMD_ISDBT_UTOPIA2_EN
    MsOS_DelayTask(1);
    #else
    pRes->sDMD_ISDBT_InitData.DelayMS(1);
    #endif

    udata = _HAL_DMD_RIU_ReadByte(MBRegBase + 0x1E);

    if (udata) return FALSE;

    _MBX_ReadReg(0x20C2, &udata);
    
    if (udata != 0x04) return FALSE;
    
    return TRUE;
}

static MS_BOOL _HAL_INTERN_ISDBT_Download(void)
{
    DMD_ISDBT_ResData *pRes = psDMD_ISDBT_ResData + u8DMD_ISDBT_DMD_ID;
    #if (FW_RUN_ON_DRAM_MODE == 0)
    MS_U8  udata = 0x00;
    MS_U16 i = 0;
    MS_U16 fail_cnt = 0;
    #endif
    MS_U8  u8TmpData;
    MS_U16 u16AddressOffset;
    const MS_U8 *ISDBT_table;
    MS_U16 u16Lib_size;
    #if (FW_RUN_ON_DRAM_MODE == 1)
    MS_U32 u32VA_DramCodeAddr;
    #endif

    pRes->sDMD_ISDBT_PriData.tdi_dram_size = TDI_USE_DRAM_SIZE; 

    if (pRes->sDMD_ISDBT_PriData.bDownloaded)
    {
        if (_HAL_INTERN_ISDBT_Ready())
        {
            #if REMAP_RST_BY_DRV
            _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x02, 0x02); // reset RIU remapping
            #endif
            #if (POW_SAVE_BY_DRV)
            _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x01, 0x01); //enable DMD MCU51 SRAM
            #endif
            _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x01, 0x01); // reset VD_MCU
            _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x00, 0x03);
            #if DMD_ISDBT_UTOPIA_EN || DMD_ISDBT_UTOPIA2_EN
            MsOS_DelayTask(20);
            #else
            pRes->sDMD_ISDBT_InitData.DelayMS(20);
            #endif
            return TRUE;
        }
    }

    ISDBT_table = &INTERN_ISDBT_table[0];
    u16Lib_size = sizeof(INTERN_ISDBT_table);

    #if REMAP_RST_BY_DRV
    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x02, 0x02); // reset RIU remapping
    #endif
    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x01, 0x01); // reset VD_MCU
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x01, 0x00); // disable SRAM

    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x00, 0x01); // release MCU, madison patch

    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x03, 0x50); // enable "vdmcu51_if"
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x03, 0x51); // enable auto-increase

    #if (FW_RUN_ON_DRAM_MODE == 0)
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x04, 0x00); // sram address low byte
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x05, 0x00); // sram address high byte

    ////  Load code thru VDMCU_IF ////
    HAL_INTERN_ISDBT_DBINFO(PRINT(">Load Code...\n"));

    for (i = 0; i < u16Lib_size; i++)
    {
        _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, ISDBT_table[i]); // write data to VD MCU 51 code sram
    }

    ////  Content verification ////
    HAL_INTERN_ISDBT_DBINFO(PRINT(">Verify Code...\n"));

    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x04, 0x00); // sram address low byte
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x05, 0x00); // sram address high byte

    for (i = 0; i < u16Lib_size; i++)
    {
        udata = _HAL_DMD_RIU_ReadByte(DMDMcuBase+0x10); // read sram data

        if (udata != ISDBT_table[i])
        {
            HAL_INTERN_ISDBT_DBINFO(PRINT(">fail add = 0x%x\n", i));
            HAL_INTERN_ISDBT_DBINFO(PRINT(">code = 0x%x\n", INTERN_ISDBT_table[i]));
            HAL_INTERN_ISDBT_DBINFO(PRINT(">data = 0x%x\n", udata));

            if (fail_cnt++ > 10)
            {
                HAL_INTERN_ISDBT_DBINFO(PRINT(">DSP Loadcode fail!"));
                return FALSE;
            }
        }
    }

    u16AddressOffset = (ISDBT_table[0x400] << 8)|ISDBT_table[0x401];

    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x04, (u16AddressOffset&0xFF)); // sram address low byte
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x05, (u16AddressOffset>>8));   // sram address high byte

    u8TmpData = (MS_U8)pRes->sDMD_ISDBT_InitData.u16IF_KHZ;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u16IF_KHZ >> 8);
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)pRes->sDMD_ISDBT_InitData.bIQSwap;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)pRes->sDMD_ISDBT_InitData.u16AgcReferenceValue;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u16AgcReferenceValue >> 8);
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)pRes->sDMD_ISDBT_InitData.u32TdiStartAddr;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u32TdiStartAddr >> 8);
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u32TdiStartAddr >> 16);
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u32TdiStartAddr >> 24);
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    #else  // #if (FW_RUN_ON_DRAM_MODE == 0)
    //download DRAM part
    // VA = MsOS_PA2KSEG1(PA); //NonCache
    HAL_INTERN_ISDBT_DBINFO(PRINT(">>> ISDBT_FW_DRAM_START_ADDR=0x%lx \n", ((pRes->sDMD_ISDBT_InitData.u32TdiStartAddr * 16) + (4 * 1024 * 1024))));
    u32VA_DramCodeAddr = MsOS_PA2KSEG1(((pRes->sDMD_ISDBT_InitData.u32TdiStartAddr * 16) + (4 * 1024 * 1024)));
    memcpy((void*)(MS_VIRT)u32VA_DramCodeAddr, &ISDBT_table[0], u16Lib_size);

    u16AddressOffset = (ISDBT_table[0x400] << 8)|ISDBT_table[0x401];

    u8TmpData = (MS_U8)pRes->sDMD_ISDBT_InitData.u16IF_KHZ;
    HAL_INTERN_ISDBT_DBINFO(PRINT("u16IF_KHZ=%d\n",pRes->sDMD_ISDBT_InitData.u16IF_KHZ));
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset), &u8TmpData, 1);
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u16IF_KHZ >> 8);
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset + 1), &u8TmpData, 1);
    u8TmpData = (MS_U8)pRes->sDMD_ISDBT_InitData.bIQSwap;
    HAL_INTERN_ISDBT_DBINFO(PRINT("bIQSwap=%d\n",pRes->sDMD_ISDBT_InitData.bIQSwap));
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset + 2), &u8TmpData, 1);
    u8TmpData = (MS_U8)pRes->sDMD_ISDBT_InitData.u16AgcReferenceValue;
    HAL_INTERN_ISDBT_DBINFO(PRINT("u16AGC_REFERENCE=%X\n",pRes->sDMD_ISDBT_InitData.u16AgcReferenceValue));
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset + 3), &u8TmpData, 1);
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u16AgcReferenceValue >> 8);
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset + 4), &u8TmpData, 1);
    u8TmpData = (MS_U8)pRes->sDMD_ISDBT_InitData.u32TdiStartAddr;
    HAL_INTERN_ISDBT_DBINFO(PRINT("u32TdiStartAddr=%X\n",pRes->sDMD_ISDBT_InitData.u32TdiStartAddr));
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset + 5), &u8TmpData, 1);
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u32TdiStartAddr >> 8);
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset + 6), &u8TmpData, 1);
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u32TdiStartAddr >> 16);
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset + 7), &u8TmpData, 1);
    u8TmpData = (MS_U8)(pRes->sDMD_ISDBT_InitData.u32TdiStartAddr >> 24);
    memcpy((void*)(MS_VIRT)(u32VA_DramCodeAddr + u16AddressOffset + 8), &u8TmpData, 1);

    HAL_INTERN_ISDBT_DBINFO(PRINT(">Load DRAM code done...\n"));   
    #endif // #if (FW_RUN_ON_DRAM_MODE == 0)
    
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x03, 0x50); // diable auto-increase
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x03, 0x00); // disable "vdmcu51_if"

    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x01, 0x01); // reset MCU, madison patch

    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x01, 0x01); // enable SRAM
    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x00, 0x03); // release VD_MCU

    pRes->sDMD_ISDBT_PriData.bDownloaded = TRUE;

    #if DMD_ISDBT_UTOPIA_EN || DMD_ISDBT_UTOPIA2_EN
    MsOS_DelayTask(20);
    #else
    pRes->sDMD_ISDBT_InitData.DelayMS(20);
    #endif

    HAL_INTERN_ISDBT_DBINFO(PRINT(">DSP Loadcode done."));

    return TRUE;
}

static void _HAL_INTERN_ISDBT_FWVERSION(void)
{
    MS_U8 data1 = 0;
    MS_U8 data2 = 0;
    MS_U8 data3 = 0;

    _MBX_ReadReg(0x20C4, &data1);
    _MBX_ReadReg(0x20C5, &data2);
    _MBX_ReadReg(0x20C6, &data3);

    PRINT("INTERN_ISDBT_FW_VERSION:%x.%x.%x\n", data1, data2, data3);
}

static MS_BOOL _HAL_INTERN_ISDBT_Exit(void)
{
    #if !DMD_ISDBT_UTOPIA_EN && !DMD_ISDBT_UTOPIA2_EN
    DMD_ISDBT_ResData *pRes = psDMD_ISDBT_ResData + u8DMD_ISDBT_DMD_ID;
    #endif
    MS_U8 u8CheckCount = 0;
    #if TDI_MIU_FREE_BY_DRV
    MS_U8 u8RegValTmp = 0;
    #endif

    #if TDI_MIU_FREE_BY_DRV
    u8RegValTmp = _HAL_DMD_RIU_ReadByte(0x101200+ISDBT_MIU_CLIENTW_ADDR);
    if (u8RegValTmp & ISDBT_MIU_CLIENTW_BIT_MASK)
    {
       _HAL_DMD_RIU_WriteByteMask(0x100600+ISDBT_MIU_CLIENTW_MASK, ISDBT_MIU_CLIENTW_BIT_MASK, ISDBT_MIU_CLIENTW_BIT_MASK);
       _HAL_DMD_RIU_WriteByteMask(0x100600+ISDBT_MIU_CLIENTR_MASK, ISDBT_MIU_CLIENTR_BIT_MASK, ISDBT_MIU_CLIENTR_BIT_MASK);
    }
    else
    {
       _HAL_DMD_RIU_WriteByteMask(0x101200+ISDBT_MIU_CLIENTW_MASK, ISDBT_MIU_CLIENTW_BIT_MASK, ISDBT_MIU_CLIENTW_BIT_MASK);
       _HAL_DMD_RIU_WriteByteMask(0x101200+ISDBT_MIU_CLIENTR_MASK, ISDBT_MIU_CLIENTR_BIT_MASK, ISDBT_MIU_CLIENTR_BIT_MASK);
    }
    #endif
    
    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x1C, 0x01);

    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)|0x02);    // assert interrupt to VD MCU51
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)&(~0x02)); // de-assert interrupt to VD MCU51

    while ((_HAL_DMD_RIU_ReadByte(MBRegBase + 0x1C)&0x02) != 0x02)
    {
        #if DMD_ISDBT_UTOPIA_EN || DMD_ISDBT_UTOPIA2_EN
        MsOS_DelayTask(1);
        #else
        pRes->sDMD_ISDBT_InitData.DelayMS(1);
        #endif

        if (u8CheckCount++ == 0xFF)
        {
            PRINT(">> ISDBT Exit Fail!\n");
            return FALSE;
        }
    }
    
    PRINT(">> ISDBT Exit Ok!\n");

    #if POW_SAVE_BY_DRV
    _HAL_DMD_RIU_WriteByte(0x101e83, 0x00);
    _HAL_DMD_RIU_WriteByte(0x101e82, 0x00);
    #if DMD_ISDBT_UTOPIA_EN || DMD_ISDBT_UTOPIA2_EN
    MsOS_DelayTask(1);
    #else
    pRes->sDMD_ISDBT_InitData.DelayMS(1);
    #endif
    _HAL_DMD_RIU_WriteByte(0x101e83, 0x00);
    _HAL_DMD_RIU_WriteByte(0x101e82, 0x11);
    #endif
    
    return TRUE;
}

static MS_BOOL _HAL_INTERN_ISDBT_SoftReset(void)
{
    MS_U8 u8Data = 0;
    #if TDI_MIU_FREE_BY_DRV
    MS_U8 u8RegValTmp = 0;
    #endif
    
    #if TDI_MIU_FREE_BY_DRV
    u8RegValTmp = _HAL_DMD_RIU_ReadByte(0x101200+ISDBT_MIU_CLIENTW_ADDR);
    if (u8RegValTmp & ISDBT_MIU_CLIENTW_BIT_MASK)
    {
       _HAL_DMD_RIU_WriteByteMask(0x100600+ISDBT_MIU_CLIENTW_MASK, ISDBT_MIU_CLIENTW_BIT_MASK, ISDBT_MIU_CLIENTW_BIT_MASK);
       _HAL_DMD_RIU_WriteByteMask(0x100600+ISDBT_MIU_CLIENTR_MASK, ISDBT_MIU_CLIENTR_BIT_MASK, ISDBT_MIU_CLIENTR_BIT_MASK);
    }
    else
    {
       _HAL_DMD_RIU_WriteByteMask(0x101200+ISDBT_MIU_CLIENTW_MASK, ISDBT_MIU_CLIENTW_BIT_MASK, ISDBT_MIU_CLIENTW_BIT_MASK);
       _HAL_DMD_RIU_WriteByteMask(0x101200+ISDBT_MIU_CLIENTR_MASK, ISDBT_MIU_CLIENTR_BIT_MASK, ISDBT_MIU_CLIENTR_BIT_MASK);
    }
    #endif

    //Reset FSM
    if (_MBX_WriteReg(0x20C0, 0x00)==FALSE) return FALSE;

    while (u8Data!=0x02)
    {
        if (_MBX_ReadReg(0x20C1, &u8Data)==FALSE) return FALSE;
    }

    return TRUE;
}

static MS_BOOL _HAL_INTERN_ISDBT_SetACICoef(void)
{
    return TRUE;
}

static MS_BOOL _HAL_INTERN_ISDBT_SetIsdbtMode(void)
{
    #if TDI_MIU_FREE_BY_DRV
    MS_U8 u8RegValTmp = 0;
    #endif

    #if TDI_MIU_FREE_BY_DRV
    u8RegValTmp = _HAL_DMD_RIU_ReadByte(0x101200+ISDBT_MIU_CLIENTW_ADDR);
    if (u8RegValTmp & ISDBT_MIU_CLIENTW_BIT_MASK)
    {
       _HAL_DMD_RIU_WriteByteMask(0x100600+ISDBT_MIU_CLIENTW_MASK, 0, ISDBT_MIU_CLIENTW_BIT_MASK);
       _HAL_DMD_RIU_WriteByteMask(0x100600+ISDBT_MIU_CLIENTR_MASK, 0, ISDBT_MIU_CLIENTR_BIT_MASK);
    }
    else
    {
       _HAL_DMD_RIU_WriteByteMask(0x101200+ISDBT_MIU_CLIENTW_MASK, 0, ISDBT_MIU_CLIENTW_BIT_MASK);
       _HAL_DMD_RIU_WriteByteMask(0x101200+ISDBT_MIU_CLIENTR_MASK, 0, ISDBT_MIU_CLIENTR_BIT_MASK);
    }
    #endif

    if (_MBX_WriteReg(0x20C2, 0x04)==FALSE) return FALSE;
    return _MBX_WriteReg(0x20C0, 0x04);
}

static MS_BOOL _HAL_INTERN_ISDBT_SetModeClean(void)
{
    if (_MBX_WriteReg(0x20C2, 0x07)==FALSE) return FALSE;
    return _MBX_WriteReg(0x20C0, 0x00);
}

static MS_BOOL _HAL_INTERN_ISDBT_Check_FEC_Lock(void)
{
    MS_BOOL bCheckPass = FALSE;
    MS_U8   u8Data = 0;
    _MBX_ReadReg(0x20C3, &u8Data);

    if ((u8Data & 0x04) == 0x04) // Check FEC Lock Flag
    {
        bCheckPass = TRUE;
    }
    return bCheckPass;
}

static MS_BOOL _HAL_INTERN_ISDBT_Check_FSA_TRACK_Lock(void)
{
    MS_BOOL bCheckPass = FALSE;
    MS_U8   u8Data = 0;

    _MBX_ReadReg(REG_ISDBT_LOCK_STATUS, &u8Data);

    if ((u8Data & 0x01) != 0x00) // Check FSA Track Lock Flag
        bCheckPass = TRUE;

    return bCheckPass;
}

static MS_BOOL _HAL_INTERN_ISDBT_Check_PSYNC_Lock(void)
{
    MS_BOOL bCheckPass = FALSE;
    MS_U8   u8Data = 0;

    _MBX_ReadReg(REG_ISDBT_LOCK_STATUS, &u8Data);

    if ((u8Data & 0x04) != 0x00) // Check Psync Lock Flag
        bCheckPass = TRUE;

    return bCheckPass;
}

static MS_BOOL _HAL_INTERN_ISDBT_Check_ICFO_CH_EXIST_Lock(void)
{
    MS_BOOL bCheckPass = FALSE;
    MS_U8   u8Data = 0;

    //_MBX_ReadReg(REG_ISDBT_LOCK_STATUS, &u8Data);

    _MBX_ReadReg(0x20C3, &u8Data);
    if ((u8Data & 0x02) == 0x02) // Check ICFO Lock Flag
        bCheckPass = TRUE;

    return bCheckPass;
}

static MS_BOOL _HAL_INTERN_ISDBT_GetSignalCodeRate(EN_ISDBT_Layer eLayerIndex, EN_ISDBT_CODE_RATE *peIsdbtCodeRate)
{
    MS_BOOL bRet = TRUE;
    MS_U8 u8Data = 0;
    MS_U8 u8CodeRate = 0;

    switch (eLayerIndex)
    {
        case E_ISDBT_Layer_A:
            // [10:8] reg_tmcc_cur_convolution_code_rate_a
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x04*2+1, &u8Data);
            u8CodeRate = u8Data & 0x07;
            break;
        case E_ISDBT_Layer_B:
            // [10:8] reg_tmcc_cur_convolution_code_rate_b
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x05*2+1, &u8Data);
            u8CodeRate = u8Data & 0x07;
            break;
       case E_ISDBT_Layer_C:
            // [10:8] reg_tmcc_cur_convolution_code_rate_c
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x06*2+1, &u8Data);
            u8CodeRate = u8Data & 0x07;
            break;
       default:
            u8CodeRate = 15;
            break;
    }

    switch (u8CodeRate)
    {
        case 0:
            *peIsdbtCodeRate = E_ISDBT_CODERATE_1_2;
            break;
        case 1:
            *peIsdbtCodeRate = E_ISDBT_CODERATE_2_3;
            break;
        case 2:
            *peIsdbtCodeRate = E_ISDBT_CODERATE_3_4;
            break;
        case 3:
            *peIsdbtCodeRate = E_ISDBT_CODERATE_5_6;
            break;
        case 4:
            *peIsdbtCodeRate = E_ISDBT_CODERATE_7_8;
            break;
        default:
            *peIsdbtCodeRate = E_ISDBT_CODERATE_INVALID;
            break;
    }

    return bRet;
}

static MS_BOOL _HAL_INTERN_ISDBT_GetSignalGuardInterval(EN_ISDBT_GUARD_INTERVAL *peIsdbtGI)
{
    MS_BOOL bRet = TRUE;
    MS_U8 u8Data = 0;
    MS_U8 u8CP = 0;

    // [7:6] reg_mcd_out_cp
    // output cp -> 00: 1/4
    //                    01: 1/8
    //                    10: 1/16
    //                    11: 1/32
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE+0x34*2, &u8Data);

    u8CP  = (u8Data >> 6) & 0x03;

    switch (u8CP)
    {
        case 0:
            *peIsdbtGI = E_ISDBT_GUARD_INTERVAL_1_4;
            break;
        case 1:
            *peIsdbtGI = E_ISDBT_GUARD_INTERVAL_1_8;
            break;
        case 2:
            *peIsdbtGI = E_ISDBT_GUARD_INTERVAL_1_16;
            break;
        case 3:
            *peIsdbtGI = E_ISDBT_GUARD_INTERVAL_1_32;
            break;
    }

    return bRet;
}

static MS_BOOL _HAL_INTERN_ISDBT_GetSignalTimeInterleaving(EN_ISDBT_Layer eLayerIndex, EN_ISDBT_TIME_INTERLEAVING *peIsdbtTDI)
{
    MS_BOOL bRet = TRUE;
    MS_U8 u8Data = 0;
    MS_U8 u8Mode = 0;
    MS_U8 u8Tdi = 0;

    // [5:4] reg_mcd_out_mode
    // output mode  -> 00: 2k
    //                         01: 4k
    //                         10: 8k
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE+0x34*2, &u8Data);

    u8Mode  = (u8Data >> 4) & 0x03;

    switch (eLayerIndex)
    {
        case E_ISDBT_Layer_A:
            // [14:12] reg_tmcc_cur_interleaving_length_a
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x04*2+1, &u8Data);
            u8Tdi = (u8Data >> 4) & 0x07;
            break;
        case E_ISDBT_Layer_B:
            // [14:12] reg_tmcc_cur_interleaving_length_b
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x05*2+1, &u8Data);
            u8Tdi = (u8Data >> 4) & 0x07;
            break;
        case E_ISDBT_Layer_C:
            // [14:12] reg_tmcc_cur_interleaving_length_c
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x06*2+1, &u8Data);
            u8Tdi = (u8Data >> 4) & 0x07;
            break;
       default:
            u8Tdi = 15;
            break;
    }

    // u8Tdi+u8Mode*4
    // => 0~3: 2K
    // => 4~7: 4K
    // => 8~11:8K
    switch (u8Tdi+u8Mode*4)
    {
        case 0:
            *peIsdbtTDI = E_ISDBT_2K_TDI_0;
            break;
        case 1:
            *peIsdbtTDI = E_ISDBT_2K_TDI_4;
            break;
        case 2:
            *peIsdbtTDI = E_ISDBT_2K_TDI_8;
            break;
        case 3:
            *peIsdbtTDI = E_ISDBT_2K_TDI_16;
            break;
        case 4:
            *peIsdbtTDI = E_ISDBT_4K_TDI_0;
            break;
        case 5:
            *peIsdbtTDI = E_ISDBT_4K_TDI_2;
            break;
        case 6:
            *peIsdbtTDI = E_ISDBT_4K_TDI_4;
            break;
        case 7:
            *peIsdbtTDI = E_ISDBT_4K_TDI_8;
            break;
        case 8:
            *peIsdbtTDI = E_ISDBT_8K_TDI_0;
            break;
        case 9:
            *peIsdbtTDI = E_ISDBT_8K_TDI_1;
            break;
        case 10:
            *peIsdbtTDI = E_ISDBT_8K_TDI_2;
            break;
        case 11:
            *peIsdbtTDI = E_ISDBT_8K_TDI_4;
            break;
        default:
            *peIsdbtTDI = E_ISDBT_TDI_INVALID;
            break;
    }

    return bRet;
}

static MS_BOOL _HAL_INTERN_ISDBT_GetSignalFFTValue(EN_ISDBT_FFT_VAL *peIsdbtFFT)
{
    MS_BOOL bRet = TRUE;
    MS_U8 u8Data = 0;
    MS_U8 u8Mode = 0;

    // [5:4]  reg_mcd_out_mode
    // output mode  -> 00: 2k
    //                         01: 4k
    //                         10: 8k
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE+0x34*2, &u8Data);

    u8Mode  = (u8Data >> 4) & 0x03;

    switch (u8Mode)
    {
        case 0:
            *peIsdbtFFT = E_ISDBT_FFT_2K;
            break;
        case 1:
            *peIsdbtFFT = E_ISDBT_FFT_4K;
            break;
        case 2:
            *peIsdbtFFT = E_ISDBT_FFT_8K;
            break;
        default:
            *peIsdbtFFT = E_ISDBT_FFT_INVALID;
            break;
    }

    return bRet;
}

static MS_BOOL _HAL_INTERN_ISDBT_GetSignalModulation(EN_ISDBT_Layer eLayerIndex, EN_ISDBT_CONSTEL_TYPE *peIsdbtConstellation)
{
    MS_BOOL bRet = TRUE;
    MS_U8 u8Data = 0;
    MS_U8 u8QAM = 0;

    switch(eLayerIndex)
    {
        case E_ISDBT_Layer_A:
            // [6:4] reg_tmcc_cur_carrier_modulation_a
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x04*2, &u8Data);
            u8QAM = (u8Data >> 4) & 0x07;
            break;
        case E_ISDBT_Layer_B:
            // [6:4] reg_tmcc_cur_carrier_modulation_b
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x05*2, &u8Data);
            u8QAM = (u8Data >> 4) & 0x07;
            break;
        case E_ISDBT_Layer_C:
            // [6:4] reg_tmcc_cur_carrier_modulation_c
            bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE+0x06*2, &u8Data);
            u8QAM = (u8Data >> 4) & 0x07;
            break;
        default:
            u8QAM = 15;
            break;
    }

    switch(u8QAM)
    {
        case 0:
            *peIsdbtConstellation = E_ISDBT_DQPSK;
            break;
        case 1:
            *peIsdbtConstellation = E_ISDBT_QPSK;
            break;
        case 2:
            *peIsdbtConstellation = E_ISDBT_16QAM;
            break;
        case 3:
            *peIsdbtConstellation = E_ISDBT_64QAM;
            break;
        default:
            *peIsdbtConstellation = E_ISDBT_QAM_INVALID;
            break;
    }

    return bRet;
}

static MS_U8 _HAL_INTERN_ISDBT_ReadIFAGC(void)
{
    MS_U8 data = 0;

    _MBX_ReadReg(0x28FD, &data);

    return data;
}

#ifdef UTPA2
static MS_BOOL _HAL_INTERN_ISDBT_GetFreqOffset(MS_U8 *pFFT_Mode, MS_S32 *pTdCfoRegValue, MS_S32 *pFdCfoRegValue, MS_S16 *pIcfoRegValue)
#else
static MS_BOOL _HAL_INTERN_ISDBT_GetFreqOffset(float *pFreqOff)
#endif
{
    MS_BOOL bRet = TRUE;
    MS_U8   u8Data = 0;
    MS_S32  s32TdCfoRegValue = 0;
    MS_S32  s32FdCfoRegValue = 0;
    MS_S16  s16IcfoRegValue = 0;
    #ifndef UTPA2
    float   fTdCfoFreq = 0.0;
    float   fICfoFreq = 0.0;
    float   fFdCfoFreq = 0.0;
    #endif

    //Get TD CFO
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE + 0x04, &u8Data);   //0x02 * 2
    bRet &= _MBX_WriteReg(ISDBT_TDP_REG_BASE + 0x04, (u8Data|0x01));

    //read td_freq_error
    //Read <29,38>
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE + 0x8A, &u8Data);   //0x45 * 2
    s32TdCfoRegValue = u8Data;
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE + 0x8B, &u8Data);   //0x45 * 2 + 1
    s32TdCfoRegValue |= u8Data << 8;
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE + 0x8C, &u8Data);   //0x46 * 2
    s32TdCfoRegValue = u8Data << 16;
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE + 0x8D, &u8Data);   //0x46 * 2 + 1
    s32TdCfoRegValue |= u8Data << 24;

    if (u8Data >= 0x10)
        s32TdCfoRegValue = 0xE0000000 | s32TdCfoRegValue;

    s32TdCfoRegValue >>=4;

    //TD_cfo_Hz = RegCfoTd * fb
    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE + 0x04, &u8Data);   //0x02 * 2
    bRet &= _MBX_WriteReg(ISDBT_TDP_REG_BASE + 0x04, (u8Data&~0x01));

    #ifndef UTPA2
    fTdCfoFreq = ((float)s32TdCfoRegValue) / 17179869184.0; //<25,34>
    fTdCfoFreq = fTdCfoFreq * 8126980.0;
    #endif

    //Get FD CFO
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0xFE, &u8Data);   //0x7f * 2
    bRet &= _MBX_WriteReg(ISDBT_FDP_REG_BASE + 0xFE, (u8Data|0x01));
    //load
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0xFF, &u8Data);   //0x7f * 2 + 1
    bRet &= _MBX_WriteReg(ISDBT_FDP_REG_BASE + 0xFF, (u8Data|0x01));

    //read CFO_KI
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0x5E, &u8Data);   //0x2F * 2
    s32FdCfoRegValue = u8Data;
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0x5F, &u8Data);   //0x2F * 2 + 1
    s32FdCfoRegValue |= u8Data << 8;
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0x60, &u8Data);   //0x30 * 2
    s32FdCfoRegValue |= u8Data << 16;
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0x61, &u8Data);   //0x30 * 2
    s32FdCfoRegValue |= u8Data << 24;

    if(u8Data >= 0x01)
        s32FdCfoRegValue = 0xFE000000 | s32FdCfoRegValue;

    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0xFE, &u8Data);   //0x7f * 2
    bRet &= _MBX_WriteReg(ISDBT_FDP_REG_BASE + 0xFE, (u8Data&~0x01));
    //load
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0xFF, &u8Data);   //0x7f * 2 + 1
    bRet &= _MBX_WriteReg(ISDBT_FDP_REG_BASE + 0xFF, (u8Data|0x01));

    #ifndef UTPA2
    fFdCfoFreq = ((float)s32FdCfoRegValue) / 17179869184.0;
    fFdCfoFreq = fFdCfoFreq * 8126980.0;
    #endif

    //Get ICFO
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0x5C, &u8Data);   //0x2E * 2
    s16IcfoRegValue = u8Data;
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0x5D, &u8Data);   //0x2E * 2 + 1
    s16IcfoRegValue |= u8Data << 8;
    s16IcfoRegValue = (s16IcfoRegValue >> 4) & 0x07FF;

    if(s16IcfoRegValue >= 0x400)
        s16IcfoRegValue = s16IcfoRegValue | 0xFFFFF800;

    bRet &= _MBX_ReadReg(ISDBT_TDP_REG_BASE + 0x68, &u8Data);   //0x34 * 2

    #ifdef UTPA2
    *pFFT_Mode = u8Data;
    *pTdCfoRegValue = s32TdCfoRegValue;
    *pFdCfoRegValue = s32TdCfoRegValue;
    *pIcfoRegValue = s16IcfoRegValue;
    #else
    if((u8Data & 0x30) == 0x0000) // 2k
        fICfoFreq = (float)s16IcfoRegValue*250000.0/63.0;
    else if((u8Data & 0x0030) == 0x0010)	// 4k
        fICfoFreq = (float)s16IcfoRegValue*125000.0/63.0;
    else //if(u16data & 0x0030 == 0x0020) // 8k
        fICfoFreq = (float)s16IcfoRegValue*125000.0/126.0;

    *pFreqOff = fTdCfoFreq + fFdCfoFreq + fICfoFreq;

    HAL_INTERN_ISDBT_DBINFO(PRINT("Total CFO value = %f\n", *pFreqOff));
    #endif

    return bRet;
}

#ifdef UTPA2
static MS_BOOL _HAL_INTERN_ISDBT_GetPreViterbiBer(EN_ISDBT_Layer eLayerIndex, MS_U32 *pBerValue, MS_U16 *pBerPeriod )
#else
static MS_BOOL _HAL_INTERN_ISDBT_GetPreViterbiBer(EN_ISDBT_Layer eLayerIndex, float *pfber)
#endif
{
    MS_BOOL bRet = TRUE;
    MS_U8   u8Data = 0;
    MS_U16  u16BerValue = 0;
    MS_U32  u32BerPeriod = 0;

    // reg_rd_freezeber
    bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x60, &u8Data);
    bRet &= _MBX_WriteReg(ISDBT_OUTER_REG_BASE + 0x60, u8Data|0x08);

    if (eLayerIndex == E_ISDBT_Layer_A)
    {
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x90, &u8Data);  //0x48 * 2
        u16BerValue=u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x91, &u8Data);  //0x48 * 2+1
        u16BerValue |= (u8Data << 8);
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x76, &u8Data); //0x3b * 2
        u32BerPeriod = (u8Data&0x3F);
        u32BerPeriod <<= 16;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x70, &u8Data); //0x38 * 2
        u32BerPeriod |= u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x70, &u8Data); //0x38 * 2 +1
        u32BerPeriod |= (u8Data << 8);
    }
    else if (eLayerIndex == E_ISDBT_Layer_B)
    {
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x92, &u8Data);  //0x49 * 2
        u16BerValue=u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x93, &u8Data);  //0x49 * 2+1
        u16BerValue |= (u8Data << 8);
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x77, &u8Data); //0x3b * 2 + 1
        u32BerPeriod = (u8Data&0x3F);
        u32BerPeriod <<= 16;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x72, &u8Data); //0x39 * 2
        u32BerPeriod |= u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x73, &u8Data); //0x39 * 2 +1
        u32BerPeriod |= (u8Data << 8);
    }
    else if (eLayerIndex == E_ISDBT_Layer_C)
    {
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x94, &u8Data);  //0x4A * 2
        u16BerValue=u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x95, &u8Data);  //0x4A * 2+1
        u16BerValue |= (u8Data << 8);
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x78, &u8Data); //0x3C
        u32BerPeriod = (u8Data&0x003F);
        u32BerPeriod <<= 16;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x74, &u8Data); //0x3A * 2
        u32BerPeriod |= u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x75, &u8Data); //0x3A * 2 +1
        u32BerPeriod |= (u8Data << 8);
    }
    else
    {
        HAL_INTERN_ISDBT_DBINFO(PRINT("Please select correct Layer\n"));
        bRet = FALSE;
    }

    // reg_rd_freezeber
    bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x60, &u8Data);
    bRet &= _MBX_WriteReg(ISDBT_OUTER_REG_BASE + 0x60, (u8Data&~0x08));

    u32BerPeriod <<= 8; // *256

    if(u32BerPeriod == 0) u32BerPeriod = 1;

    #ifdef UTPA2
    *pBerPeriod = u32BerPeriod;
    *pBerValue = u16BerValue;
    #else
    *pfber = (float)u16BerValue/u32BerPeriod;
    HAL_INTERN_ISDBT_DBINFO(PRINT("Layer: 0x%x, Pre-Ber = %e\n", eLayerIndex, *pfber));
    #endif

    return bRet;
}

#ifdef UTPA2
static MS_BOOL _HAL_INTERN_ISDBT_GetPostViterbiBer(EN_ISDBT_Layer eLayerIndex, MS_U32 *pBerValue, MS_U16 *pBerPeriod )
#else
static MS_BOOL _HAL_INTERN_ISDBT_GetPostViterbiBer(EN_ISDBT_Layer eLayerIndex, float *pfber)
#endif
{
    MS_BOOL bRet = TRUE;
    MS_U8   u8Data = 0;
    MS_U8   u8FrzData = 0;
    MS_U32  u32BerValue = 0;
    MS_U16  u16BerPeriod = 0;

    // reg_rd_freezeber
    bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x01*2+1, &u8FrzData);
    u8Data = u8FrzData | 0x01;
    bRet &= _MBX_WriteReg(ISDBT_OUTER_REG_BASE+0x01*2+1, u8Data);

    if (eLayerIndex == E_ISDBT_Layer_A)
    {
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x14, &u8Data);  //0x0A * 2
        u32BerValue = u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x15, &u8Data);  //0x0A * 2+1
        u32BerValue |= u8Data << 8;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x16, &u8Data);  //0x0B * 2
        u32BerValue |= u8Data << 16;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x17, &u8Data);  //0x0B * 2+1
        u32BerValue |= u8Data << 24;

        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x0A, &u8Data);  //0x05 * 2
        u16BerPeriod = u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x0B, &u8Data);  //0x05 * 2+1
        u16BerPeriod |= u8Data << 8;
    }
    else if (eLayerIndex == E_ISDBT_Layer_B)
    {
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x46, &u8Data);  //0x23 * 2
        u32BerValue = u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x47, &u8Data);  //0x23 * 2+1
        u32BerValue |= u8Data << 8;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x48, &u8Data);  //0x24 * 2
        u32BerValue |= u8Data << 16;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x49, &u8Data);  //0x24 * 2+1
        u32BerValue |= u8Data << 24;

        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x3A, &u8Data);  //0x1d * 2
        u16BerPeriod = u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x3B, &u8Data);  //0x1d * 2+1
        u16BerPeriod |= u8Data << 8;
    }
    else if (eLayerIndex == E_ISDBT_Layer_C)
    {
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x88, &u8Data);  //0x44 * 2
        u32BerValue = u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x89, &u8Data);  //0x44 * 2+1
        u32BerValue |= u8Data << 8;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x8A, &u8Data);  //0x45 * 2
        u32BerValue |= u8Data << 16;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x8B, &u8Data);  //0x45 * 2+1
        u32BerValue |= u8Data << 24;

        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x3E, &u8Data);  //0x1f * 2
        u16BerPeriod = u8Data;
        bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE + 0x3F, &u8Data);  //0x1d * 2+1
        u16BerPeriod |= u8Data << 8;
    }
    else
    {
        HAL_INTERN_ISDBT_DBINFO(PRINT("Please select correct Layer\n"));
        bRet = FALSE;
    }

    // reg_rd_freezeber
    bRet &= _MBX_WriteReg(ISDBT_OUTER_REG_BASE+0x01*2+1, u8FrzData);

    if(u16BerPeriod == 0) u16BerPeriod = 1;

    #ifdef UTPA2
    *pBerPeriod = u16BerPeriod;
    *pBerValue = u32BerValue;
    #else
    *pfber = (float)u32BerValue/u16BerPeriod/(128.0*188.0*8.0);
    HAL_INTERN_ISDBT_DBINFO(PRINT("Layer: 0x%x, Post-Ber = %e\n", eLayerIndex, *pfber));
    #endif
    return bRet;
}

static MS_U16 _HAL_INTERN_ISDBT_GetSignalQualityOfLayerA(void)
{
    #ifdef UTPA2
    MS_U32 BerValue  = 0;
    MS_U16 BerPeriod = 0;
    #else
    float fber;
    #endif
    MS_BOOL bRet = TRUE;
    EN_ISDBT_Layer eLayerIndex;
    MS_U16 u16SQI;

    // Tmp solution
    eLayerIndex = E_ISDBT_Layer_A;

    if(_HAL_INTERN_ISDBT_Check_FEC_Lock() == FALSE)
    {
        //PRINT("Dan Demod unlock!!!\n");
        u16SQI = 0;
    }
    else
    {
        // Part 1: get ber value from demod.
        #ifdef UTPA2
        bRet &= _HAL_INTERN_ISDBT_GetPostViterbiBer(eLayerIndex, &BerValue, &BerPeriod);
        
        u16SQI = _CALCULATE_SQI(BerValue, BerPeriod*128*188*8);
        #else
        bRet &= _HAL_INTERN_ISDBT_GetPostViterbiBer(eLayerIndex, &fber);

        u16SQI = _CALCULATE_SQI(fber);
        #endif
    }

    //PRINT("dan SQI = %d\n", SQI);
    return u16SQI;
}

static MS_U16 _HAL_INTERN_ISDBT_GetSignalQualityOfLayerB(void)
{
    #ifdef UTPA2
    MS_U32 BerValue  = 0;
    MS_U16 BerPeriod = 0;
    #else
    float fber;
    #endif
    MS_BOOL bRet = TRUE;
    EN_ISDBT_Layer eLayerIndex;
    MS_U16 u16SQI;

    // Tmp solution
    eLayerIndex = E_ISDBT_Layer_B;

    if(_HAL_INTERN_ISDBT_Check_FEC_Lock() == FALSE)
    {
        //PRINT("Dan Demod unlock!!!\n");
        u16SQI = 0;
    }
    else
    {
        // Part 1: get ber value from demod.
        #ifdef UTPA2
        bRet &= _HAL_INTERN_ISDBT_GetPostViterbiBer(eLayerIndex, &BerValue, &BerPeriod);
        
        u16SQI = _CALCULATE_SQI(BerValue, BerPeriod*128*188*8);
        #else
        bRet &= _HAL_INTERN_ISDBT_GetPostViterbiBer(eLayerIndex, &fber);

        u16SQI = _CALCULATE_SQI(fber);
        #endif
    }

    //PRINT("dan SQI = %d\n", SQI);
    return u16SQI;
}

static MS_U16 _HAL_INTERN_ISDBT_GetSignalQualityOfLayerC(void)
{
    #ifdef UTPA2
    MS_U32 BerValue  = 0;
    MS_U16 BerPeriod = 0;
    #else
    float fber;
    #endif
    MS_BOOL bRet = TRUE;
    EN_ISDBT_Layer eLayerIndex;
    MS_U16 u16SQI;

    // Tmp solution
    eLayerIndex = E_ISDBT_Layer_C;

    if(_HAL_INTERN_ISDBT_Check_FEC_Lock() == FALSE)
    {
        //PRINT("Dan Demod unlock!!!\n");
        u16SQI = 0;
    }
    else
    {
        // Part 1: get ber value from demod.
        #ifdef UTPA2
        bRet &= _HAL_INTERN_ISDBT_GetPostViterbiBer(eLayerIndex, &BerValue, &BerPeriod);
        
        u16SQI = _CALCULATE_SQI(BerValue, BerPeriod*128*188*8);
        #else
        bRet &= _HAL_INTERN_ISDBT_GetPostViterbiBer(eLayerIndex, &fber);

        u16SQI = _CALCULATE_SQI(fber);
        #endif
    }

    //PRINT("dan SQI = %d\n", SQI);
    return u16SQI;
}

static MS_U16 _HAL_INTERN_ISDBT_GetSignalQualityOfLayerCombine(void)
{
    MS_S8  s8LayerAValue = 0, s8LayerBValue = 0, s8LayerCValue = 0;
    MS_U16 u16SQI;
    EN_ISDBT_Layer eLayerIndex;
    EN_ISDBT_CONSTEL_TYPE eIsdbtConstellationA, eIsdbtConstellationB, eIsdbtConstellationC;

    //Get modulation of each layer
    eLayerIndex = E_ISDBT_Layer_A;
    _HAL_INTERN_ISDBT_GetSignalModulation(eLayerIndex, &eIsdbtConstellationA);
    eLayerIndex = E_ISDBT_Layer_B;
    _HAL_INTERN_ISDBT_GetSignalModulation(eLayerIndex, &eIsdbtConstellationB);
    eLayerIndex = E_ISDBT_Layer_C;
    _HAL_INTERN_ISDBT_GetSignalModulation(eLayerIndex, &eIsdbtConstellationC);

    if (eIsdbtConstellationA != E_ISDBT_QAM_INVALID)
        s8LayerAValue = (MS_S8)eIsdbtConstellationA;
    else
        s8LayerAValue = -1;

    if (eIsdbtConstellationB != E_ISDBT_QAM_INVALID)
        s8LayerBValue = (MS_S8)eIsdbtConstellationB;
    else
        s8LayerBValue = -1;

    if (eIsdbtConstellationC != E_ISDBT_QAM_INVALID)
        s8LayerCValue = (MS_S8)eIsdbtConstellationC;
    else
        s8LayerCValue = -1;

    //PRINT("Layer info A:%d, B:%d, C:%d\n", s8LayerAValue, s8LayerBValue, s8LayerCValue);
    if (s8LayerAValue >= s8LayerBValue)
    {
        if (s8LayerCValue >= s8LayerAValue)
        {
            //Get Layer C u16SQI
            u16SQI = _HAL_INTERN_ISDBT_GetSignalQualityOfLayerC();
            //PRINT("dan u16SQI Layer C1: %d\n", u16SQI);
        }
        else  //A>C
        {
            //Get Layer A u16SQI
            u16SQI = _HAL_INTERN_ISDBT_GetSignalQualityOfLayerA();
            //PRINT("dan u16SQI Layer A: %d\n", u16SQI);
        }
    }
    else  // B >= A
    {
        if (s8LayerCValue >= s8LayerBValue)
        {
            //Get Layer C u16SQI
            u16SQI = _HAL_INTERN_ISDBT_GetSignalQualityOfLayerC();
            //PRINT("dan u16SQI Layer C2: %d\n", u16SQI);
        }
        else  //B>C
        {
            //Get Layer B u16SQI
            u16SQI = _HAL_INTERN_ISDBT_GetSignalQualityOfLayerB();
            //PRINT("dan u16SQI Layer B: %d\n", u16SQI);
        }
    }

    return u16SQI;
}

#ifdef UTPA2
static MS_BOOL _HAL_INTERN_ISDBT_GetSNR(MS_U32 *pRegSNR, MS_U16 *pRegSnrObsNum)
#else
static MS_BOOL _HAL_INTERN_ISDBT_GetSNR(float *pf_snr)
#endif
{
    MS_BOOL bRet = TRUE;
    MS_U8   u8Data = 0;
    MS_U32  u32RegSNR = 0;
    MS_U16  u16RegSnrObsNum = 0;
    #ifndef UTPA2
    float   fSNRAvg = 0.0;
    #endif

    //set freeze
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0xFE, &u8Data);   //0x7f * 2
    bRet &= _MBX_WriteReg(ISDBT_FDP_REG_BASE + 0xFE, (u8Data|0x01));
    //load
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0xFF, &u8Data);   //0x7f * 2 + 1
    bRet &= _MBX_WriteReg(ISDBT_FDP_REG_BASE + 0xFF, (u8Data|0x01));

    // ==============Average SNR===============//
    // [26:0] reg_snr_accu
    bRet &= _MBX_ReadReg(ISDBT_FDPEXT_REG_BASE+0x2d*2+1, &u8Data);
    u32RegSNR = u8Data&0x07;
    bRet &= _MBX_ReadReg(ISDBT_FDPEXT_REG_BASE+0x2d*2, &u8Data);
    u32RegSNR = (u32RegSNR<<8) | u8Data;
    bRet &= _MBX_ReadReg(ISDBT_FDPEXT_REG_BASE+0x2c*2+1, &u8Data);
    u32RegSNR = (u32RegSNR<<8) | u8Data;
    bRet &= _MBX_ReadReg(ISDBT_FDPEXT_REG_BASE+0x2c*2, &u8Data);
    u32RegSNR = (u32RegSNR<<8) | u8Data;

    // [12:0] reg_snr_observe_sum_num
    bRet &= _MBX_ReadReg(ISDBT_FDPEXT_REG_BASE+0x2a*2+1, &u8Data);
    u16RegSnrObsNum = u8Data&0x1f;
    bRet &= _MBX_ReadReg(ISDBT_FDPEXT_REG_BASE+0x2a*2, &u8Data);
    u16RegSnrObsNum = (u16RegSnrObsNum<<8) | u8Data;

    //release freeze
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0xFE, &u8Data);   //0x7f * 2
    bRet &= _MBX_WriteReg(ISDBT_FDP_REG_BASE + 0xFE, (u8Data&~0x01));
    //load
    bRet &= _MBX_ReadReg(ISDBT_FDP_REG_BASE + 0xFF, &u8Data);   //0x7f * 2 + 1
    bRet &= _MBX_WriteReg(ISDBT_FDP_REG_BASE + 0xFF, (u8Data|0x01));

    if (u16RegSnrObsNum == 0)
        u16RegSnrObsNum = 1;


    #ifdef UTPA2
     *pRegSNR = u32RegSNR;
     *pRegSnrObsNum = u16RegSnrObsNum;
    #else
     fSNRAvg = (float)u32RegSNR/u16RegSnrObsNum;
     if (fSNRAvg == 0)                 //protect value 0
         fSNRAvg = 0.01;

     #ifdef MSOS_TYPE_LINUX
     *pf_snr = 10.0f*(float)log10f((double)fSNRAvg/2);
     #else
     *pf_snr = 10.0f*(float)Log10Approx((double)fSNRAvg/2);
     #endif
     HAL_INTERN_ISDBT_DBINFO(PRINT("SNR value = %f\n", *pf_snr));
    #endif

    return bRet;
}

static MS_BOOL _HAL_INTERN_ISDBT_Read_PKT_ERR(EN_ISDBT_Layer eLayerIndex, MS_U16 *pu16PacketErr)
{
    MS_BOOL bRet = TRUE;
    MS_U8 u8Data = 0;
    MS_U8 u8FrzData = 0;
    MS_U16 u16PacketErrA = 0xFFFF, u16PacketErrB = 0xFFFF, u16PacketErrC = 0xFFFF;
    #if DMD_ISDBT_TBVA_EN
    MS_U8 bTbvaBypass = 0;
    MS_U8 u8TbvaLayer = 0;
    #endif
    // Read packet errors of three layers
    // OUTER_FUNCTION_ENABLE
    // [8] reg_biterr_num_pcktprd_freeze
    // Freeze Packet error
    bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x01*2+1, &u8FrzData);
    u8Data = u8FrzData | 0x01;
    bRet &= _MBX_WriteReg(ISDBT_OUTER_REG_BASE+0x01*2+1, u8Data);
#if DMD_ISDBT_TBVA_EN
    bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x10*2, &u8Data);
    bTbvaBypass = u8Data & 0x01;
    bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x11*2, &u8Data);
    u8TbvaLayer = u8Data & 0x03;
    switch(eLayerIndex)
    {
        case E_ISDBT_Layer_A:
            // [15:0] OUTER_UNCRT_PKT_NUM_PCKTPRD_A
            if (!bTbvaBypass && u8TbvaLayer == 0)
            {
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x17*2+1, &u8Data);
                u16PacketErrA = u8Data << 8;
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x17*2, &u8Data);
                u16PacketErrA = u16PacketErrA | u8Data;
                *pu16PacketErr = u16PacketErrA;
            }
            else
            {
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x08*2+1, &u8Data);
                u16PacketErrA = u8Data << 8;
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x08*2, &u8Data);
                u16PacketErrA = u16PacketErrA | u8Data;
                *pu16PacketErr = u16PacketErrA;
            }
            break;
        case E_ISDBT_Layer_B:
            // [15:0] OUTER_UNCRT_PKT_NUM_PCKTPRD_B
            if (!bTbvaBypass && u8TbvaLayer == 1)
            {
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x17*2+1, &u8Data);
                u16PacketErrB = u8Data << 8;
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x17*2, &u8Data);
                u16PacketErrB = u16PacketErrB | u8Data;
                *pu16PacketErr = u16PacketErrB;
            }
            else
            {
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x21*2+1, &u8Data);
                u16PacketErrB = u8Data << 8;
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x21*2, &u8Data);
                u16PacketErrB = u16PacketErrB | u8Data;
                *pu16PacketErr = u16PacketErrB;
            }
            break;
        case E_ISDBT_Layer_C:
            // [15:0] OUTER_UNCRT_PKT_NUM_PCKTPRD_C
            if (!bTbvaBypass && u8TbvaLayer == 2)
            {
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x17*2+1, &u8Data);
                u16PacketErrC = u8Data << 8;
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x17*2, &u8Data);
                u16PacketErrC = u16PacketErrC | u8Data;
                *pu16PacketErr = u16PacketErrC;
            }
            else
            {
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x42*2+1, &u8Data);
                u16PacketErrC = u8Data << 8;
                bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x42*2, &u8Data);
                u16PacketErrC = u16PacketErrC | u8Data;
                *pu16PacketErr = u16PacketErrC;
            }
            break;
        default:
            *pu16PacketErr = 0xFFFF;
            break;
    }
#else
    switch(eLayerIndex)
    {
        case E_ISDBT_Layer_A:
            // [15:0] OUTER_UNCRT_PKT_NUM_PCKTPRD_A
            bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x08*2+1, &u8Data);
            u16PacketErrA = u8Data << 8;
            bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x08*2, &u8Data);
            u16PacketErrA = u16PacketErrA | u8Data;
            *pu16PacketErr = u16PacketErrA;
            break;
        case E_ISDBT_Layer_B:
            // [15:0] OUTER_UNCRT_PKT_NUM_PCKTPRD_B
            bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x21*2+1, &u8Data);
            u16PacketErrB = u8Data << 8;
            bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x21*2, &u8Data);
            u16PacketErrB = u16PacketErrB | u8Data;
            *pu16PacketErr = u16PacketErrB;
            break;
        case E_ISDBT_Layer_C:
            // [15:0] OUTER_UNCRT_PKT_NUM_PCKTPRD_C
            bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x42*2+1, &u8Data);
            u16PacketErrC = u8Data << 8;
            bRet &= _MBX_ReadReg(ISDBT_OUTER_REG_BASE+0x42*2, &u8Data);
            u16PacketErrC = u16PacketErrC | u8Data;
            *pu16PacketErr = u16PacketErrC;
            break;
        default:
            *pu16PacketErr = 0xFFFF;
            break;
    }
#endif
    // Unfreeze Packet error
    bRet &= _MBX_WriteReg(ISDBT_OUTER_REG_BASE+0x01*2+1, u8FrzData);

    return bRet;
}

static MS_BOOL _HAL_INTERN_ISDBT_GetReg(MS_U16 u16Addr, MS_U8 *pu8Data)
{
    return _MBX_ReadReg(u16Addr, pu8Data);
}

static MS_BOOL _HAL_INTERN_ISDBT_SetReg(MS_U16 u16Addr, MS_U8 u8Data)
{
    return _MBX_WriteReg(u16Addr, u8Data);
}

//-------------------------------------------------------------------------------------------------
//  Global Functions
//-------------------------------------------------------------------------------------------------
MS_BOOL HAL_INTERN_ISDBT_IOCTL_CMD(DMD_ISDBT_HAL_COMMAND eCmd, void *pArgs)
{
    MS_BOOL bResult = TRUE;

    switch(eCmd)
    {
    case DMD_ISDBT_HAL_CMD_Exit:
        bResult = _HAL_INTERN_ISDBT_Exit();
        break;
    case DMD_ISDBT_HAL_CMD_InitClk:
        _HAL_INTERN_ISDBT_InitClk();
        break;
    case DMD_ISDBT_HAL_CMD_Download:
        bResult = _HAL_INTERN_ISDBT_Download();
        break;
    case DMD_ISDBT_HAL_CMD_FWVERSION:
        _HAL_INTERN_ISDBT_FWVERSION();
        break;
    case DMD_ISDBT_HAL_CMD_SoftReset:
        bResult = _HAL_INTERN_ISDBT_SoftReset();
        break;
    case DMD_ISDBT_HAL_CMD_SetACICoef:
        bResult = _HAL_INTERN_ISDBT_SetACICoef();
        break;
    case DMD_ISDBT_HAL_CMD_SetISDBTMode:
        bResult = _HAL_INTERN_ISDBT_SetIsdbtMode();
        break;
    case DMD_ISDBT_HAL_CMD_SetModeClean:
        bResult = _HAL_INTERN_ISDBT_SetModeClean();
        break;
    case DMD_ISDBT_HAL_CMD_Active:
        break;
    case DMD_ISDBT_HAL_CMD_Check_FEC_Lock:
        bResult = _HAL_INTERN_ISDBT_Check_FEC_Lock();
        break;
    case DMD_ISDBT_HAL_CMD_Check_FSA_TRACK_Lock:
        bResult = _HAL_INTERN_ISDBT_Check_FSA_TRACK_Lock();
        break;
    case DMD_ISDBT_HAL_CMD_Check_PSYNC_Lock:
        bResult = _HAL_INTERN_ISDBT_Check_PSYNC_Lock();
        break;
    case DMD_ISDBT_HAL_CMD_Check_ICFO_CH_EXIST_Lock:
        bResult = _HAL_INTERN_ISDBT_Check_ICFO_CH_EXIST_Lock();
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalCodeRate:
        bResult = _HAL_INTERN_ISDBT_GetSignalCodeRate((*((DMD_ISDBT_GET_CodeRate*)pArgs)).eIsdbtLayer, &((*((DMD_ISDBT_GET_CodeRate*)pArgs)).eCodeRate));
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalGuardInterval:
        bResult = _HAL_INTERN_ISDBT_GetSignalGuardInterval((EN_ISDBT_GUARD_INTERVAL *)pArgs);
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalTimeInterleaving:
        bResult = _HAL_INTERN_ISDBT_GetSignalTimeInterleaving((*((DMD_ISDBT_GET_TimeInterleaving*)pArgs)).eIsdbtLayer, &((*((DMD_ISDBT_GET_TimeInterleaving*)pArgs)).eTimeInterleaving));
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalFFTValue:
        bResult = _HAL_INTERN_ISDBT_GetSignalFFTValue((EN_ISDBT_FFT_VAL *)pArgs);
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalModulation:
        bResult = _HAL_INTERN_ISDBT_GetSignalModulation((*((DMD_ISDBT_GET_MODULATION*)pArgs)).eIsdbtLayer, &((*((DMD_ISDBT_GET_MODULATION*)pArgs)).eConstellation));
        break;
    case DMD_ISDBT_HAL_CMD_ReadIFAGC:
        *((MS_U16 *)pArgs) = _HAL_INTERN_ISDBT_ReadIFAGC();
        break;
    case DMD_ISDBT_HAL_CMD_GetFreqOffset:
        #ifdef UTPA2
        bResult = _HAL_INTERN_ISDBT_GetFreqOffset(&((*((DMD_ISDBT_CFO_DATA*)pArgs)).FFT_Mode), &((*((DMD_ISDBT_CFO_DATA*)pArgs)).TdCfoRegValue), &((*((DMD_ISDBT_CFO_DATA*)pArgs)).FdCfoRegValue), &((*((DMD_ISDBT_CFO_DATA*)pArgs)).IcfoRegValue));
        #else
        bResult = _HAL_INTERN_ISDBT_GetFreqOffset((float *)pArgs);
        #endif
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalQuality:
    case DMD_ISDBT_HAL_CMD_GetSignalQualityOfLayerA:
        *((MS_U16*)pArgs) = _HAL_INTERN_ISDBT_GetSignalQualityOfLayerA();
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalQualityOfLayerB:
        *((MS_U16*)pArgs) = _HAL_INTERN_ISDBT_GetSignalQualityOfLayerB();
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalQualityOfLayerC:
        *((MS_U16*)pArgs) = _HAL_INTERN_ISDBT_GetSignalQualityOfLayerC();
        break;
    case DMD_ISDBT_HAL_CMD_GetSignalQualityCombine:
        *((MS_U16*)pArgs) = _HAL_INTERN_ISDBT_GetSignalQualityOfLayerCombine();
        break;
    case DMD_ISDBT_HAL_CMD_GetSNR:
        #ifdef UTPA2
        bResult = _HAL_INTERN_ISDBT_GetSNR(&((*((DMD_ISDBT_SNR_DATA*)pArgs)).RegSNR), &((*((DMD_ISDBT_SNR_DATA*)pArgs)).RegSnrObsNum));
        #else
        bResult = _HAL_INTERN_ISDBT_GetSNR((float *)pArgs);
        #endif
        break;
    case DMD_ISDBT_HAL_CMD_GetPreViterbiBer:
        #ifdef UTPA2
        bResult = _HAL_INTERN_ISDBT_GetPreViterbiBer((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).eIsdbtLayer, &((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).BerValue), &((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).BerPeriod));
        #else
        bResult = _HAL_INTERN_ISDBT_GetPreViterbiBer((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).eIsdbtLayer, &((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).fBerValue)); 
        #endif
        break;
    case DMD_ISDBT_HAL_CMD_GetPostViterbiBer:
        #ifdef UTPA2
        bResult = _HAL_INTERN_ISDBT_GetPostViterbiBer((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).eIsdbtLayer, &((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).BerValue), &((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).BerPeriod));
        #else
        bResult = _HAL_INTERN_ISDBT_GetPostViterbiBer((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).eIsdbtLayer, &((*((DMD_ISDBT_GET_BER_VALUE*)pArgs)).fBerValue));  
        #endif
        break;
    case DMD_ISDBT_HAL_CMD_Read_PKT_ERR:
        bResult = _HAL_INTERN_ISDBT_Read_PKT_ERR((*((DMD_ISDBT_GET_PKT_ERR*)pArgs)).eIsdbtLayer, &((*((DMD_ISDBT_GET_PKT_ERR*)pArgs)).u16PacketErr));
        break;
    case DMD_ISDBT_HAL_CMD_TS_INTERFACE_CONFIG:
        break;
    case DMD_ISDBT_HAL_CMD_IIC_Bypass_Mode:
        break;
    case DMD_ISDBT_HAL_CMD_SSPI_TO_GPIO:
        break;
    case DMD_ISDBT_HAL_CMD_GPIO_GET_LEVEL:
        break;
    case DMD_ISDBT_HAL_CMD_GPIO_SET_LEVEL:
        break;
    case DMD_ISDBT_HAL_CMD_GPIO_OUT_ENABLE:
        break;
    case DMD_ISDBT_HAL_CMD_GET_REG:
        bResult = _HAL_INTERN_ISDBT_GetReg((*((DMD_ISDBT_REG_DATA *)pArgs)).u16Addr, &((*((DMD_ISDBT_REG_DATA *)pArgs)).u8Data));
        break;
    case DMD_ISDBT_HAL_CMD_SET_REG:
        bResult = _HAL_INTERN_ISDBT_SetReg((*((DMD_ISDBT_REG_DATA *)pArgs)).u16Addr, (*((DMD_ISDBT_REG_DATA *)pArgs)).u8Data);
        break;
    default:
        break;
    }

    return bResult;
}

MS_BOOL MDrv_DMD_ISDBT_Initial_Hal_Interface(void)
{
    return TRUE;
}

