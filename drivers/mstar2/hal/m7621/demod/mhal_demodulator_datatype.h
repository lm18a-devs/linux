//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all 
// or part of MStar Software is expressly prohibited, unless prior written 
// permission has been granted by MStar. 
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.  
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software. 
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s 
//    confidential information in strictest confidence and not disclose to any
//    third party.  
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.  
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or 
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.  
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _MAPI_DEMODULATOR_DATATYPE_H_
#define _MAPI_DEMODULATOR_DATATYPE_H_

//#ifdef _DVBS_C_
//#define EXTSEL
//#else
//#define EXTSEL extern
//#endif

/// demodulator list
typedef enum
{
    /// demodulator: main
    E_DEMOD_MAIN,
    /// demodulator: extend
    E_DEMOD_EXTEND,
    /// demodulator: extend2
    E_DEMOD_EXTEND2,
} EN_DEMOD_LIST;

/// demod version struct
typedef struct //DLL_PUBLIC
{    
    /// the name of demod
    U8 name[4];
    /// the version of demod
    U8 version[2];
    /// the build of demod
    U8 build[2];
    /// the changlist of demod
    U8 changelist[8];
}Demod_Version_t;

//namespace mapi_demodulator_datatype
//{
    // ------------------------------------------------------------
    // Enum Define
    // ------------------------------------------------------------

    /// the VIF type
    typedef enum
    {
        /// internal VIF
        mapi_demodulator_datatype_E_VIF_INTERNAL,
        /// external VIF
        mapi_demodulator_datatype_E_VIF_EXTERNAL
    } mapi_demodulator_datatype_E_VIF_TYPE;

    ///FREQ type define
    typedef enum
    {
        ///FREQ INVALID
        mapi_demodulator_datatype_IF_FREQ_INVALID,
        ///FREQ B
        mapi_demodulator_datatype_IF_FREQ_B,              // 38.90 MHz
        ///FREQ G
        mapi_demodulator_datatype_IF_FREQ_G,              // 38.90 MHz
        ///FREQ I
        mapi_demodulator_datatype_IF_FREQ_I,              // 38.90 MHz
        ///FREQ DK
        mapi_demodulator_datatype_IF_FREQ_DK,             // 38.90 MHz
        ///FREQ L
        mapi_demodulator_datatype_IF_FREQ_L,              // 38.90 MHz
        ///FREQ L PRIME
        mapi_demodulator_datatype_IF_FREQ_L_PRIME,        // 33.90 MHz
        ///FREQ MN
        mapi_demodulator_datatype_IF_FREQ_MN,             // 45.75 MHz
        ///FREQ J
        mapi_demodulator_datatype_IF_FREQ_J,              // 58.75 MHz
        ///FREQ PAL 38
        mapi_demodulator_datatype_IF_FREQ_PAL_38,         // 38.00 MHz
        ///FREQ MAX IF FREQ
        mapi_demodulator_datatype_IF_FREQ_MAX_IF_FREQ,
        ///Digital mode
        mapi_demodulator_datatype_IF_DIGITAL_MODE
    } mapi_demodulator_datatype_IF_FREQ;

    /// Audio SIF Standard Type
    typedef enum
    {
        ///< Audio standard BG
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_BG_,
        ///< Audio standard BG A2
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_BG_A2_,
        ///< Audio standard BG NICAM
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_BG_NICAM_,
        ///< Audio standard I
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_I_,
        ///< Audio standard DK
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_DK_,
        ///< Audio standard DK1 A2
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_DK1_A2_,
        ///< Audio standard DK2 A2
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_DK2_A2_,
        ///< Audio standard DK3 A2
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_DK3_A2_,
        ///< Audio standard DK NICAM
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_DK_NICAM_,
        ///< Audio standard L
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_L_,
        ///< Audio standard L
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_LP_,
        ///< Audio standard M
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_M_,
        ///< Audio standard M BTSC
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_M_BTSC_,
        ///< Audio standard M A2
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_M_A2_,
        ///< Audio standard M EIA J
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_M_EIA_J_,
        ///< Not Audio standard
        mapi_demodulator_datatype_E_DEMOD_AUDIOSTANDARD_NOTSTANDARD_
    } mapi_demodulator_datatype_DEMOD_AUDIOSTANDARD_TYPE_;

    ///AFC type define
    typedef enum
    {
        ///AFC below MINUS 187p5KHz
        mapi_demodulator_datatype_E_AFC_BELOW_MINUS_187p5KHz      = 0x07,
        ///AFC MINUS 162p5KHz
        mapi_demodulator_datatype_E_AFC_MINUS_162p5KHz            = 0x06,
        ///AFC MINUS 137p5KHz
        mapi_demodulator_datatype_E_AFC_MINUS_137p5KHz            = 0x05,
        ///AFC MINUS 112p5KHz
        mapi_demodulator_datatype_E_AFC_MINUS_112p5KHz            = 0x04,
        ///AFC MINUS 87p5KHz
        mapi_demodulator_datatype_E_AFC_MINUS_87p5KHz             = 0x03,
        ///AFC MINUS 62p5KHz
        mapi_demodulator_datatype_E_AFC_MINUS_62p5KHz             = 0x02,
        ///AFC MINUS 37p5KHz
        mapi_demodulator_datatype_E_AFC_MINUS_37p5KHz             = 0x01,
        ///AFC MINUS 12p5KHz
        mapi_demodulator_datatype_E_AFC_MINUS_12p5KHz             = 0x00,
        ///AFC PLUS 12p5KHz
        mapi_demodulator_datatype_E_AFC_PLUS_12p5KHz              = 0x0F,
        ///AFC PLUS 37p5KHz
        mapi_demodulator_datatype_E_AFC_PLUS_37p5KHz              = 0x0E,
        ///AFC PLUS 62p5KHz
        mapi_demodulator_datatype_E_AFC_PLUS_62p5KHz              = 0x0D,
        ///AFC PLUS 87p5KHz
        mapi_demodulator_datatype_E_AFC_PLUS_87p5KHz              = 0x0C,
        ///AFC PLUS 112p5KHz
        mapi_demodulator_datatype_E_AFC_PLUS_112p5KHz             = 0x0B,
        ///AFC PLUS 137p5KHz
        mapi_demodulator_datatype_E_AFC_PLUS_137p5KHz             = 0x0A,
        ///AFC PLUS 162p5KHz
        mapi_demodulator_datatype_E_AFC_PLUS_162p5KHz             = 0x09,
        ///AFC ABOVE PLUS 187p5KHz
        mapi_demodulator_datatype_E_AFC_ABOVE_PLUS_187p5KHz       = 0x08,
        ///AFC out of AFCWIN
        mapi_demodulator_datatype_E_AFC_OUT_OF_AFCWIN             = 0x10,
    } mapi_demodulator_datatype_AFC;

    /// DVBC QAM Type
    typedef enum
    {
        ///< QAM 16
        mapi_demodulator_datatype_E_CAB_QAM16 = 0,
        ///< QAM 32
        mapi_demodulator_datatype_E_CAB_QAM32 = 1,
        ///< QAM 64
        mapi_demodulator_datatype_E_CAB_QAM64 = 2,
        ///< QAM 128
        mapi_demodulator_datatype_E_CAB_QAM128 = 3,
        ///< QAM 256
        mapi_demodulator_datatype_E_CAB_QAM256 = 4,
        ///< Invalid value
        mapi_demodulator_datatype_E_CAB_INVALID
    } mapi_demodulator_datatype_EN_CAB_CONSTEL_TYPE;

    /// DVB-T modulation mode
    typedef enum
    {
        /// QPSK
        mapi_demodulator_datatype_E_DVBT_QPSK     = 0,
        /// 16QAM
        mapi_demodulator_datatype_E_DVBT_16_QAM   = 1,
        /// 64QAM
        mapi_demodulator_datatype_E_DVBT_64_QAM      = 2,
        /// 256QAM
        mapi_demodulator_datatype_E_DVBT_256_QAM     = 3,
        /// invalid indicator
        mapi_demodulator_datatype_E_DVBT_INVALID
    } mapi_demodulator_datatype_EN_DVBT_CONSTEL_TYPE;

    /// DVB-T code rate
    typedef enum
    {
        /// 1/2
        mapi_demodulator_datatype_E_DVBT_CODERATE_1_2 = 0,
        /// 2/3
        mapi_demodulator_datatype_E_DVBT_CODERATE_2_3 = 1,
        /// 3/4
        mapi_demodulator_datatype_E_DVBT_CODERATE_3_4 = 2,
        /// 5/6
        mapi_demodulator_datatype_E_DVBT_CODERATE_5_6 = 3,
        /// 7/8
        mapi_demodulator_datatype_E_DVBT_CODERATE_7_8 = 4,
        /// 4/5
        mapi_demodulator_datatype_E_DVBT_CODERATE_4_5 = 5,
        /// 6/7
        mapi_demodulator_datatype_E_DVBT_CODERATE_6_7 = 6,
        /// 8/9
        mapi_demodulator_datatype_E_DVBT_CODERATE_8_9 = 7,
        /// invalid indicator
        mapi_demodulator_datatype_E_DVBT_CODERATE_INVALID,
    } mapi_demodulator_datatype_EN_DVBT_CODE_RATE;
    
   /// DVB-T TPS hierarchy
    typedef enum
    {
        /// None
        mapi_demodulator_datatype_E_DVBT_HIERA_NONE = 0,
        /// 1
        mapi_demodulator_datatype_E_DVBT_HIERA_1 = 1,
        /// 2
        mapi_demodulator_datatype_E_DVBT_HIERA_2 = 2,
        /// 4
        mapi_demodulator_datatype_E_DVBT_HIERA_4 = 3,
        /// invalid indicator
        mapi_demodulator_datatype_E_DVBT_HIERARCHY_UNKNOWN = 7,
    } mapi_demodulator_datatype_EN_DVBT_HIERARCHY;
    
    /// DVB-S code rate
    typedef enum
    {
        /// 1/2
        mapi_demodulator_datatype_E_DVBS_CODERATE_1_2 = 0x00,
        /// 1/3
        mapi_demodulator_datatype_E_DVBS_CODERATE_1_3 = 0x01,
        /// 1/4
        mapi_demodulator_datatype_E_DVBS_CODERATE_1_4 = 0x02,
        /// 2/3
        mapi_demodulator_datatype_E_DVBS_CODERATE_2_3 = 0x03,
        /// 3/4
        mapi_demodulator_datatype_E_DVBS_CODERATE_3_4 = 0x04,
        /// 2/5
        mapi_demodulator_datatype_E_DVBS_CODERATE_2_5 = 0x05,
        /// 3/5
        mapi_demodulator_datatype_E_DVBS_CODERATE_3_5 = 0x06,
        /// 4/5
        mapi_demodulator_datatype_E_DVBS_CODERATE_4_5 = 0x07,
        /// 5/6
        mapi_demodulator_datatype_E_DVBS_CODERATE_5_6 = 0x08,
        /// 6/7
        mapi_demodulator_datatype_E_DVBS_CODERATE_6_7 = 0x09,
        /// 7/8
        mapi_demodulator_datatype_E_DVBS_CODERATE_7_8 = 0x0A,
        /// 8/9
        mapi_demodulator_datatype_E_DVBS_CODERATE_8_9 = 0x0B,
        /// 9/10
        mapi_demodulator_datatype_E_DVBS_CODERATE_9_10 = 0x0C,
        /// invalid indicator
        mapi_demodulator_datatype_E_DVBS_CODERATE_INVALID,
    } mapi_demodulator_datatype_EN_DVBS_CODE_RATE;

    /// DVB-S modulation mode
    typedef enum
    {
        /// QPSK
        mapi_demodulator_datatype_E_DVBS_QPSK   = 0x00,
        /// 8PSK
        mapi_demodulator_datatype_E_DVBS_8PSK   = 0x01,
        /// invalid indicator
        mapi_demodulator_datatype_E_DVBS_INVALID
    } mapi_demodulator_datatype_EN_DVBS_CONSTEL_TYPE;

    /// ISDBT
    typedef enum
    {
      ///Layer A
        mapi_demodulator_datatype_E_ISDBT_Layer_A = 0x00,
        ///Layer B
        mapi_demodulator_datatype_E_ISDBT_Layer_B = 0x01,
        ///Layer C
        mapi_demodulator_datatype_E_ISDBT_Layer_C = 0x02,
        ///Invalid
        mapi_demodulator_datatype_E_ISDBT_Layer_INVALID,
    }mapi_demodulator_datatype_EN_ISDBT_Layer;

    /// ISDBT FFT enum
    typedef enum
    {
        /// 2K
        mapi_demodulator_datatype_E_ISDBT_FFT_2K = 0x00,
        /// 4k
        mapi_demodulator_datatype_E_ISDBT_FFT_4K = 0x01,
        /// 8k
        mapi_demodulator_datatype_E_ISDBT_FFT_8K = 0x02,
        /// invalid indicator
        mapi_demodulator_datatype_E_ISDBT_FFT_INVALID,
    } mapi_demodulator_datatype_EN_ISDBT_FFT_VAL;

    /// ISDBT modulation mode
    typedef enum
    {
        /// DQPSK
        mapi_demodulator_datatype_E_ISDBT_DQPSK   = 0,
        /// QPSK
        mapi_demodulator_datatype_E_ISDBT_QPSK    = 1,
        /// 16QAM
        mapi_demodulator_datatype_E_ISDBT_16QAM   = 2,
        /// 64QAM
        mapi_demodulator_datatype_E_ISDBT_64QAM   = 3,
        /// invalid indicator
        mapi_demodulator_datatype_E_ISDBT_QAM_INVALID,
    } mapi_demodulator_datatype_EN_ISDBT_CONSTEL_TYPE;

    /// ISDBT code rate
    typedef enum
    {
        /// 1/2
        mapi_demodulator_datatype_E_ISDBT_CODERATE_1_2 = 0,
        /// 2/3
        mapi_demodulator_datatype_E_ISDBT_CODERATE_2_3 = 1,
        /// 3/4
        mapi_demodulator_datatype_E_ISDBT_CODERATE_3_4 = 2,
        /// 5/6
        mapi_demodulator_datatype_E_ISDBT_CODERATE_5_6 = 3,
        /// 7/8
        mapi_demodulator_datatype_E_ISDBT_CODERATE_7_8 = 4,
        /// invalid indicator
        mapi_demodulator_datatype_E_ISDBT_CODERATE_INVALID,
    } mapi_demodulator_datatype_EN_ISDBT_CODE_RATE;

    /// ISDBT guard interval enum
    typedef enum
    {
        /// 1/4
        mapi_demodulator_datatype_E_ISDBT_GUARD_INTERVAL_1_4  = 0,
        /// 1/8
        mapi_demodulator_datatype_E_ISDBT_GUARD_INTERVAL_1_8  = 1,
        /// 1/16
        mapi_demodulator_datatype_E_ISDBT_GUARD_INTERVAL_1_16 = 2,
        /// 1/32
        mapi_demodulator_datatype_E_ISDBT_GUARD_INTERVAL_1_32 = 3,
        /// invalid indicator
        mapi_demodulator_datatype_E_ISDBT_GUARD_INTERVAL_INVALID,
    } mapi_demodulator_datatype_EN_ISDBT_GUARD_INTERVAL;

    /// ISDBT Time Interleaving enum
    typedef enum
    { 
    // 2K mode
        /// Tdi = 0
        mapi_demodulator_datatype_E_ISDBT_2K_TDI_0 = 0,
        /// Tdi = 4
        mapi_demodulator_datatype_E_ISDBT_2K_TDI_4 = 1,
        /// Tdi = 8
        mapi_demodulator_datatype_E_ISDBT_2K_TDI_8 = 2,
        /// Tdi = 16
        mapi_demodulator_datatype_E_ISDBT_2K_TDI_16 = 3,
    // 4K mode
        /// Tdi = 0
        mapi_demodulator_datatype_E_ISDBT_4K_TDI_0 = 4,
        /// Tdi = 2
        mapi_demodulator_datatype_E_ISDBT_4K_TDI_2 = 5,
        /// Tdi = 4
        mapi_demodulator_datatype_E_ISDBT_4K_TDI_4 = 6,
        /// Tdi = 8
        mapi_demodulator_datatype_E_ISDBT_4K_TDI_8 = 7,
    // 8K mode   
        /// Tdi = 0
        mapi_demodulator_datatype_E_ISDBT_8K_TDI_0 = 8,
        /// Tdi = 1
        mapi_demodulator_datatype_E_ISDBT_8K_TDI_1 = 9,
        /// Tdi = 2
        mapi_demodulator_datatype_E_ISDBT_8K_TDI_2 = 10,
        /// Tdi = 4
        mapi_demodulator_datatype_E_ISDBT_8K_TDI_4 = 11,        
        /// invalid indicator
        mapi_demodulator_datatype_E_ISDBT_TDI_INVALID,
    } mapi_demodulator_datatype_EN_ISDBT_TIME_INTERLEAVING;

    /// DVB-T2 code rate
    typedef enum
    {
        /// 1/2
        mapi_demodulator_datatype_E_DVBT2_CODERATE_1_2 = 0,
        /// 3/5
        mapi_demodulator_datatype_E_DVBT2_CODERATE_3_5 = 1,
        /// 2/3
        mapi_demodulator_datatype_E_DVBT2_CODERATE_2_3 = 2,
        /// 3/4
        mapi_demodulator_datatype_E_DVBT2_CODERATE_3_4 = 3,
        /// 4/5
        mapi_demodulator_datatype_E_DVBT2_CODERATE_4_5 = 4,
        /// 5/6
        mapi_demodulator_datatype_E_DVBT2_CODERATE_5_6 = 5,
        /// invalid indicator
        mapi_demodulator_datatype_E_DVBT2_CODERATE_INVALID,
    } mapi_demodulator_datatype_EN_DVBT2_CODE_RATE;

    /// guard interval enum
    typedef enum
    {
        /// 1/32
        mapi_demodulator_datatype_E_DVBT_GUARD_INTERVAL_1_32  = 0,
        /// 1/16
        mapi_demodulator_datatype_E_DVBT_GUARD_INTERVAL_1_16  = 1,
        /// 1/8
        mapi_demodulator_datatype_E_DVBT_GUARD_INTERVAL_1_8   = 2,
        /// 1/4
        mapi_demodulator_datatype_E_DVBT_GUARD_INTERVAL_1_4   = 3,
        /// 1/28
        mapi_demodulator_datatype_E_DVBT_GUARD_INTERVAL_1_128 = 4,
        /// 19/128
        mapi_demodulator_datatype_E_DVBT_GUARD_INTERVAL_19_128 = 5,
        /// 19/256
        mapi_demodulator_datatype_E_DVBT_GUARD_INTERVAL_19_256 = 6,
        /// invalid indicator
        mapi_demodulator_datatype_E_DVBT_GUARD_INTERVAL_INVALID,
    } mapi_demodulator_datatype_EN_DVBT_GUARD_INTERVAL;

    /// FFT Ext-Bitmask
    #define MSB_FLAG_FFT_EXT_DATA   0x08
    /// DVB-T FFT enum
    typedef enum
    {
        /// 2K
        mapi_demodulator_datatype_E_DVBT_FFT_2K = 0x00,
        /// 8k
        mapi_demodulator_datatype_E_DVBT_FFT_8K = 0x01,
        /// 4k
        mapi_demodulator_datatype_E_DVBT_FFT_4K = 0x02,
        /// 1k
        mapi_demodulator_datatype_E_DVBT_FFT_1K = 0x03,
        /// 16k
        mapi_demodulator_datatype_E_DVBT_FFT_16K = 0x04,
        /// 32k
        mapi_demodulator_datatype_E_DVBT_FFT_32K = 0x05,
        /// 8K ext
        //mapi_demodulator_datatype_E_DVBT_FFT_8K_EXT = (mapi_demodulator_datatype_E_DVBT_FFT_8K | MSB_FLAG_FFT_EXT_DATA),
        mapi_demodulator_datatype_E_DVBT_FFT_8K_EXT = 0x06,
        /// 16k ext
        mapi_demodulator_datatype_E_DVBT_FFT_16K_EXT = (mapi_demodulator_datatype_E_DVBT_FFT_16K | MSB_FLAG_FFT_EXT_DATA),
        /// 32k ext
        mapi_demodulator_datatype_E_DVBT_FFT_32K_EXT = 0x07,
        /// invalid indicator
        mapi_demodulator_datatype_E_DVBT_FFT_INVALID,
    } mapi_demodulator_datatype_EN_DVBT_FFT_VAL;

    ///DTMB sub carriers type
    typedef enum
    {
        ///ADTB-T
        mapi_demodulator_datatype_E_DTMB_SUBCARRIERS_SINGLE,
        ///DMB-T
        mapi_demodulator_datatype_E_DTMB_SUBCARRIERS_MULTI,
        /// unsupported type
        mapi_demodulator_datatype_E_DTMB_SUBCARRIERS_UNSUPPORTED
    } mapi_demodulator_datatype_EN_DTMB_SUBCARRIERS;

    ///DTMB pn padding value
    typedef enum
    {
        ///ADTB-T (1/6)
        mapi_demodulator_datatype_E_DTMB_PN_PADDING_595,
        ///DMB-T (1/4)
        mapi_demodulator_datatype_E_DTMB_PN_PADDING_420,
        ///DMB-T (1/4)
        mapi_demodulator_datatype_E_DTMB_PN_PADDING_945,
        /// unsupported value
        mapi_demodulator_datatype_E_DTMB_PN_PADDING_UNSUPPORTED
    } mapi_demodulator_datatype_EN_DTMB_PN_PADDING;

    /// DTMB Mapping QAM
    typedef enum
    {
        ///ADTB-T
        mapi_demodulator_datatype_E_DTMB_MAPPING_4QAM_NR,
        ///ADTB-T  DMB-T
        mapi_demodulator_datatype_E_DTMB_MAPPING_4QAM,
        ///ADTB-T  DMB-T
        mapi_demodulator_datatype_E_DTMB_MAPPING_16QAM,
        ///ADTB-T
        mapi_demodulator_datatype_E_DTMB_MAPPING_32QAM,
        ///DMB-T
        mapi_demodulator_datatype_E_DTMB_MAPPING_64QAM,
        ///unsupported type
        mapi_demodulator_datatype_E_DTMB_MAPPING_UNSUPPORTED
    } mapi_demodulator_datatype_EN_DTMB_MAPPING;


    ///DTMB FEC coderate
    typedef enum
    {
        ///0.4
        mapi_demodulator_datatype_E_DTMB_FEC_0_4,
        ///0.6
        mapi_demodulator_datatype_E_DTMB_FEC_0_6,
        ///0.8
        mapi_demodulator_datatype_E_DTMB_FEC_0_8,
        /// unsupported value
        mapi_demodulator_datatype_E_DTMB_FEC_UNSUPPORTED
    } mapi_demodulator_datatype_EN_DTMB_FEC_CODERATE;

    /// the demodulator type
    typedef enum
    {
        mapi_demodulator_datatype_E_DEVICE_DEMOD_ATV = 0,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_DVB_T,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_DVB_C,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_DVB_S,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_DTMB,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_ATSC,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_ATSC_VSB,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_ATSC_QPSK,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_ATSC_16QAM,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_ATSC_64QAM,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_ATSC_256QAM,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_DVB_T2,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_ISDB,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_MAX,
        mapi_demodulator_datatype_E_DEVICE_DEMOD_NULL = mapi_demodulator_datatype_E_DEVICE_DEMOD_MAX,

    } mapi_demodulator_datatype_EN_DEVICE_DEMOD_TYPE;

    /// the signal strength
    typedef enum
    {
        /// no signal
        mapi_demodulator_datatype_E_FE_SIGNAL_NO = 0,
        /// weak signal
        mapi_demodulator_datatype_E_FE_SIGNAL_WEAK,
        /// moderate signal
        mapi_demodulator_datatype_E_FE_SIGNAL_MODERATE,
        /// strong signal
        mapi_demodulator_datatype_E_FE_SIGNAL_STRONG,
        /// very strong signal
        mapi_demodulator_datatype_E_FE_SIGNAL_VERY_STRONG,
    } mapi_demodulator_datatype_EN_FRONTEND_SIGNAL_CONDITION;

    /// the demod lock status
    typedef enum
    {
        /// lock
        mapi_demodulator_datatype_E_DEMOD_LOCK,
        /// is checking
        mapi_demodulator_datatype_E_DEMOD_CHECKING,
        /// after checking
        mapi_demodulator_datatype_E_DEMOD_CHECKEND,
        /// unlock
        mapi_demodulator_datatype_E_DEMOD_UNLOCK,
        /// Check no channel
        mapi_demodulator_datatype_E_DEMOD_T2_AUTO_DETECT_NO_CH,
        /// Check T2 signal
        mapi_demodulator_datatype_E_DEMOD_T2_AUTO_DETECT_FOUND_T2,
        /// hold lock status
        mapi_demodulator_datatype_E_DEMOD_LOCK_HOLD,
        /// NULL state
        mapi_demodulator_datatype_E_DEMODE_DEMOD_NULL,
    } mapi_demodulator_datatype_EN_LOCK_STATUS;

    /// Define DVB-S Roll-Off factor
    typedef enum
    {
        mapi_demodulator_datatype_E_SAT_RO_35,                                                          ///< roll-off factor = 0.35
        mapi_demodulator_datatype_E_SAT_RO_25,                                                          ///< roll-off factor = 0.25
        mapi_demodulator_datatype_E_SAT_RO_20                                                           ///< roll-off factor = 0.20
    } mapi_demodulator_datatype_EN_SAT_ROLL_OFF_TYPE;

    /// Define blind scan status of DVBS
    typedef enum
    {
        mapi_demodulator_datatype_E_BLINDSCAN_NOTREADY,                                                ///blind scan : not ready
        mapi_demodulator_datatype_E_BLINDSCAN_INIT_FAILED,                                             ///blind scan : init failed
        mapi_demodulator_datatype_E_BLINDSCAN_INIT_OK,                                                 ///blind scan : init ok
        mapi_demodulator_datatype_E_BLINDSCAN_SCANFREQ_START_FAILED,                                   ///blind scan : scan freq start failed
        mapi_demodulator_datatype_E_BLINDSCAN_SCANFREQ_SCANNING,                                       ///blind scan : scan freq scanning
        mapi_demodulator_datatype_E_BLINDSCAN_SCANFREQ_COMPLETE,                                       ///blind scan : scan freq complete
        mapi_demodulator_datatype_E_BLINDSCAN_ALLFREQ_COMPLETE,                                        ///blind scan : scan all freq complete
        mapi_demodulator_datatype_E_BLINDSCAN_UNKNOWN                                                  ///blind scan : unknown
    } mapi_demodulator_datatype_EN_BLINDSCAN_STATUS;

    /// IF AGC Setting Define
    typedef enum
    {
        ///IF AGC LPRIME
        mapi_demodulator_datatype_EMC_IF_AGC_LPRIME_SETTING=0,
        ///IF AGC SECAM VHF
        mapi_demodulator_datatype_EMC_IF_AGC_SECAM_VHF_SETTING,
        ///IF AGC SECAM UHF
        mapi_demodulator_datatype_EMC_IF_AGC_SECAM_UHF_SETTING,
        ///IF AGC PAL VHF
        mapi_demodulator_datatype_EMC_IF_AGC_PAL_VHF_SETTING,
        ///IF AGC PAL UHF
        mapi_demodulator_datatype_EMC_IF_AGC_PAL_UHF_SETTING,
        ///IF AGC SCAN
        mapi_demodulator_datatype_EMC_IF_AGC_SCAN_SETTING
    }mapi_demodulator_datatype_EN_EMC_IF_AGC_SETTINGS;

    /// Define Demod DVBT Information
    typedef struct //DLL_PUBLIC
    {
        U16 u16Version;
        U8 u16DemodState;
        float SfoValue;
        float TotalCfo;
        U16 u16ChannelLength;
        U8 u8Fft;
        U8 u8Constel;
        U8 u8Gi;
        U8 u8HpCr;
        U8 u8LpCr;
        U8 u8Hiearchy;
        U8 u8Fd;
        U8 u8ChLen;
        U8 u8SnrSel;
        U8 u8PertoneNum;
        U8 u8DigAci;
        U8 u8FlagCi;
        U8 u8TdCoef;
    }mapi_demodulator_datatype_ST_DEMOD_DVBT_INFO;

    /// Define Demod DVBC Information
    typedef struct //DLL_PUBLIC
    {
        U16 u16Version;
        U16 u16SymbolRate;
        mapi_demodulator_datatype_EN_CAB_CONSTEL_TYPE eQamMode;
        U32 u32IFFreq;
        BOOL bSpecInv;
        BOOL bSerialTS;
        U8 u8SarValue;
        U32 u32ChkScanTimeStart;
        mapi_demodulator_datatype_EN_LOCK_STATUS eLockStatus;
        U16 u16Strength;
        U16 u16Quality;
        U32 u32Intp;
        U32 u32FcFs;
        U8 u8Qam;
        U16 u16SymbolRateHal;
    } mapi_demodulator_datatype_ST_DEMOD_DVBC_INFO;
//};

#endif
