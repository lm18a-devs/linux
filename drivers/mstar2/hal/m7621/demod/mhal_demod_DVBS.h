#ifndef _DVBS_H_
#define _DVBS_H_

#include "mdrv_types.h"
#include "mhal_demodulator_datatype.h"

//#ifdef _DVBS_C_
//#define EXTSEL
//#else
//#define EXTSEL extern
//#endif

//#define DTV_SYS_DVBT				0
//#define DTV_SYS_DVBC				1
//#define DTV_SYSTEM_SELECT			DTV_SYS_DVBT  // header file 2개로 인하여 네임 변경 dvb =>dvbc로
#define USE_TERACOM_SSI_SQI_SPEC	1
#define TS_CLK_INV 					1

//--------------------------------------------------------------------
#define     DEMOD_SLAVE_ID          0x32
#define     DEMOD_ADDR_H            0x00
#define     DEMOD_ADDR_L            0x01
#define     DEMOD_WRITE_REG         0x02
#define     DEMOD_WRITE_REG_EX      0x03
#define     DEMOD_READ_REG          0x04
#define     DEMOD_RAM_CONTROL       0x05


//New tuner IF frequency
#define		FC_0_85_MHz		0xAE50	
#define		FC_3_25_MHz		0xA4F0	 
#define		FC_3_5_MHz  	0xA3F6	 
#define		FC_4_MHz	    0xA202
#define		FC_5_MHz	    0x9E1A
#define		FC_5_MHz_DVBC	0x5CF8  //0331update
#define		FC_4_56_MHz		0x9FD2	//LG10 tuner update 0405

//#define BIT(_bit_)                  (1 << (_bit_))


#define COFDM_DEM_I2C_ID			DEMOD_SLAVE_ID
#define MSB1228_REG_INVERSION              0x8024

//#define BOOL	BOOLEAN
/*
typedef enum
{
	COFDM_FEC_LOCK_DVBT,
	COFDM_FEC_LOCK_DVBC,
	COFDM_TR_LOCK_DVBC,// andy 2009-9-29 오후 1:50:23
	COFDM_PSYNC_LOCK,
	COFDM_TPS_LOCK,
	COFDM_TPS_LOCK_HISTORY,// andy 2009-9-28 오후 7:20:36
	COFDM_DCR_LOCK,
	COFDM_AGC_LOCK,
	COFDM_MODE_DET,
	COFDM_LOCK_STABLE_DVBT,//0923 update
      COFDM_MODE_CP_NO_CH_DETECT,

} COFDM_LOCK_STATUS;
*/

/*
//--------------------------------------------------------------------
typedef enum
{
	E_SYS_UNKOWN = -1,
	E_SYS_DVBT,
	E_SYS_DVBC,
	E_SYS_ATSC,
	E_SYS_VIF,
	E_SYS_DVBT2,
	E_SYS_DVBS,

	E_SYS_NUM,
}E_SYSTEM;
//andy new driver update 090720 start


typedef enum
{
	E_VERSION_ATSC=0,
	E_VERSION_DVBT,
	E_VERSION_DVBC,
	E_VERSION_NUM
}E_VERSION;


typedef enum
{
	E_TUNER_SI2158=0x00,
	E_TUNER_SI2178,
	E_TUNER_SI2176,
	E_TUNER_UNKOWN = 0xFF
}E_TUNER_TYPE;


typedef enum
{
	CMD_SYSTEM_INIT = 0,
	CMD_DAC_CALI,
	CMD_DVBT_CONFIG,
	CMD_DVBC_CONFIG,
	CMD_VIF_CTRL,
	CMD_FSM_CTRL,
	CMD_INDIR_RREG,
	CMD_INDIR_WREG,
	CMD_GET_INFO,
	CMD_TS_CTRL,

	CMD_MAX_NUM
}E_CMD_CODE;
//andy new driver update 090720 end

typedef enum
{
	pc_op_code = 0,
	pc_if_freq,
	pc_sound_sys,
	pc_vif_vga_maximum_l,
	pc_vif_vga_maximum_h,
	pc_scan_mode,
	pc_vif_top,
	pc_gain_distribution_thr_l,
	pc_gain_distribution_thr_h,

	VIF_PARAM_MAX_NUM
}E_VIF_PARAM;

typedef enum
{
	pc_system = 0,

	SYS_PARAM_MAX_NUM
}E_SYS_PARAM;

typedef enum
{
	SET_IF_FREQ = 0,
	SET_SOUND_SYS,
	VIF_INIT,
	SET_VIF_HANDLER,
	VIF_TOP_ADJUST,

	VIF_CMD_MAX_NUM
}E_VIF_CMD;

typedef enum
{
	TS_PARALLEL = 0,
	TS_SERIAL = 1,

	TS_MODE_MAX_NUM
}E_TS_MODE;

typedef enum
{
	TS_CLK_NO_INV = 0,
	TS_CLK_INVERSE = 1,

	TS_CLK_MODE_MAX_NUM
}E_TS_CLK_MODE;

typedef enum
{
	dac_op_code = 0,
	dac_idac_ch0,
	dac_idac_ch1,

	DAC_PARAM_MAX_NUM
}
E_DAC_PARAM;

typedef enum
{
	DAC_RUN_CALI = 0,
	DAC_IDAC_ASSIGN,

	DAC_CMD_MAX_NUM
}
E_DAC_CMD;

typedef enum
{
    COFDM_CCI_FLAG = 0,

    CHECK_FLAG_MAX_NUM
}
COFDM_CHECK_FLAG;

typedef struct
{
	U8		cmd_code;
	U8		param[64];
} S_CMDPKTREG;

typedef struct
{
	float	power_db;
	U8		sar3_val;
}S_RFAGC_SSI;

typedef struct
{
	float	power_db;
	U8		agc_val;
}S_IFAGC_SSI;

typedef struct
{
	float	attn_db;
	S8		agc_err;
}S_IFAGC_ERR;

typedef struct
{
	U8		constel;
	U8		code_rate;
	float	p_ref;
}S_SSI_PREF;

typedef struct
{
	U8		constel;
	U8		code_rate;
	float	cn_ref;
}S_SQI_CN_NORDIGP1;

// GI (b12 ~ b11)           : 0~3 => 1/32, 1/16, 1/8, 1/4
typedef enum
{
	_T_GI_1Y32		= 0x0,
	_T_GI_1Y16		= 0x1,
	_T_GI_1Y8		= 0x2,
	_T_GI_1Y4		= 0x3,
}E_T_GI;

//FFT (b14 ~ b13)          : 0~2 => 2K, 8K, 4K
typedef enum
{
	_T_FFT_2K		= 0x0,
	_T_FFT_8K		= 0x1,
	_T_FFT_4K		= 0x2,
}E_T_FFT;

typedef enum
{
	_C_16QAM		= 0x0,
	_C_32QAM		= 0x1,
	_C_64QAM		= 0x2,
	_C_128QAM		= 0x3,
	_C_256QAM		= 0x4,
}E_C_CONSTEL;



 * TPS : hierarchy.

typedef enum 	/ 3 bit /
{
	_T_HIERA_NONE 	= 0x0,
	_T_HIERA_1		= 0x1,
	_T_HIERA_2		= 0x2,
	_T_HIERA_4		= 0x3,
}E_HIERARCHY_T;


typedef enum
{
	E_RANGE_UNKOWN = -1,
	E_RANGE_500KHZ,
	E_RANGE_900KHZ,
	E_RANGE_NUM
}E_RANGE_CFO;
*/
typedef enum
{
    DMD_DVBS_DBGLV_NONE,    // disable all the debug message
    DMD_DVBS_DBGLV_INFO,    // information
    DMD_DVBS_DBGLV_NOTICE,  // normal but significant condition
    DMD_DVBS_DBGLV_WARNING, // warning conditions
    DMD_DVBS_DBGLV_ERR,     // error conditions
    DMD_DVBS_DBGLV_CRIT,    // critical conditions
    DMD_DVBS_DBGLV_ALERT,   // action must be taken immediately
    DMD_DVBS_DBGLV_EMERG,   // system is unusable
    DMD_DVBS_DBGLV_DEBUG,   // debug-level messages
} DMD_DVBS_DbgLv;

typedef enum
{
    DMD_DVBS_LOCK,
    DMD_DVBS_CHECKING,
    DMD_DVBS_CHECKEND,
    DMD_DVBS_UNLOCK,
    DMD_DVBS_NULL,
} DMD_DVBS_LOCK_STATUS;

typedef enum
{
    DMD_DVBS_GETLOCK,
    DMD_DVBS_GETLOCK_TR_EVER_LOCK,
    DMD_DVBS_GETLOCK_NO_CHANNEL,
} DMD_DVBS_GETLOCK_TYPE;

typedef enum
{
    DMD_DVBS_QPSK = 0,
    DMD_DVBS_8PSK = 1,
    DMD_DVBS_16APSK = 2,
    DMD_DVBS_32APSK = 3,
    DMD_DVBS_8APSK = 4,
    DMD_DVBS_8_8APSK = 5,
    DMD_DVBS_4_8_4_16APSK = 6,
} DMD_DVBS_MODULATION_TYPE;

typedef enum
{
    DMD_SAT_DVBS  = 0,
    DMD_SAT_DVBS2 = 1,
} DMD_DVBS_DEMOD_TYPE;

typedef enum
{
/*
    E_DMD_S2_ZIF_EN = 0x00,
    E_DMD_S2_RF_AGC_EN,
    E_DMD_S2_DCR_EN,
    E_DMD_S2_IQB_EN,
    E_DMD_S2_IIS_EN,
    E_DMD_S2_CCI_EN,
    E_DMD_S2_FORCE_ACI_SELECT,
    E_DMD_S2_IQ_SWAP,					//For DVBS2
    E_DMD_S2_AGC_REF_EXT_0,
    E_DMD_S2_AGC_REF_EXT_1,
    E_DMD_S2_AGC_K,
    E_DMD_S2_ADCI_GAIN,
    E_DMD_S2_ADCQ_GAIN,
    E_DMD_S2_SRD_SIG_SRCH_RNG,
    E_DMD_S2_SRD_DC_EXC_RNG,
    E_DMD_S2_FORCE_CFO_0,				//0FH
    E_DMD_S2_FORCE_CFO_1,
    E_DMD_S2_DECIMATION_NUM,
    E_DMD_S2_PSD_SMTH_TAP,
    E_DMD_S2_CCI_FREQN_0_L,
    E_DMD_S2_CCI_FREQN_0_H,
    E_DMD_S2_CCI_FREQN_1_L,
    E_DMD_S2_CCI_FREQN_1_H,
    E_DMD_S2_CCI_FREQN_2_L,
    E_DMD_S2_CCI_FREQN_2_H,
    E_DMD_S2_TR_LOPF_KP,
    E_DMD_S2_TR_LOPF_KI,
    E_DMD_S2_FINEFE_KI_SWITCH_0,
    E_DMD_S2_FINEFE_KI_SWITCH_1,
    E_DMD_S2_FINEFE_KI_SWITCH_2,
    E_DMD_S2_FINEFE_KI_SWITCH_3,
    E_DMD_S2_FINEFE_KI_SWITCH_4,		//1FH
    E_DMD_S2_PR_KP_SWITCH_0,
    E_DMD_S2_PR_KP_SWITCH_1,
    E_DMD_S2_PR_KP_SWITCH_2,
    E_DMD_S2_PR_KP_SWITCH_3,
    E_DMD_S2_PR_KP_SWITCH_4,
    E_DMD_S2_FS_GAMMA,
    E_DMD_S2_FS_ALPHA0,
    E_DMD_S2_FS_ALPHA1,
    E_DMD_S2_FS_ALPHA2,
    E_DMD_S2_FS_ALPHA3,
    E_DMD_S2_FS_H_MODE_SEL,
    E_DMD_S2_FS_OBSWIN,
    E_DMD_S2_FS_PEAK_DET_TH_L,
    E_DMD_S2_FS_PEAK_DET_TH_H,
    E_DMD_S2_FS_CONFIRM_NUM,
    E_DMD_S2_EQ_MU_FFE_DA,				//2FH
    E_DMD_S2_EQ_MU_FFE_DD,
    E_DMD_S2_EQ_ALPHA_SNR_DA,
    E_DMD_S2_EQ_ALPHA_SNR_DD,
    E_DMD_S2_FEC_ALFA,					//For DVBS2
    E_DMD_S2_FEC_BETA,					//For DVBS2
    E_DMD_S2_FEC_SCALING_LLR,			//For DVBS2
*/
    E_DMD_S2_TS_SERIAL=0x00,
    E_DMD_S2_TS_CLK_RATE,
    E_DMD_S2_TS_OUT_INV,
    E_DMD_S2_TS_DATA_SWAP,
    //------------------------------------------
    E_DMD_S2_FW_VERSION_L,           	//0x3A
    E_DMD_S2_FW_VERSION_H,           	//0x3B
    E_DMD_S2_CHIP_VERSION,
    E_DMD_S2_FS_L,		 				//Frequency
    E_DMD_S2_FS_H,						//Frequency
    E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_L,	//0x3F
    E_DMD_S2_MANUAL_TUNE_SYMBOLRATE_H,	//
    E_DMD_S2_SYSTEM_TYPE,				//DVBS/S2
    E_DMD_S2_MODULATION_TYPE,			//QPSK/8PSK
    E_DMD_S2_BLINDSCAN_CHECK,
    E_DMD_S2_UNCRT_PKT_NUM_7_0,
    E_DMD_S2_UNCRT_PKT_NUM_8_15,
    E_DMD_S2_STATE_FLAG,
    E_DMD_S2_SUBSTATE_FLAG,
    E_DMD_S2_HUM_DETECT_FLAG,
    E_DMD_S2_CCI_DETECT_FLAG,
    E_DMD_S2_IIS_DETECT_FLAG,
    E_DMD_S2_OPEN_HUM_VLD_IRQ_FLAG,
    E_DMD_S2_SRD_COARSE_DONE_FLAG,
    E_DMD_S2_SRD_FINE_DONE_FLAG,
    E_DMD_S2_FINEFE_DONE_FLAG,
    E_DMD_S2_REV_FRAME_FLAG,			//0x4F
    E_DMD_S2_DUMMY_FRAME_FLAG,
    E_DMD_S2_PLSC_DONE_FLAG,
    E_DMD_S2_GET_INFO_FROM_FRAME_LENGTH_DONE_FLAG,
    E_DMD_S2_IQ_SWAP_DETECT_FLAG,
    E_DMD_S2_FRAME_ACQUISITION_DONE_FLAG,
    E_DMD_S2_OLCFE_DONE_FLAG,
    E_DMD_S2_FSYNC_FOUND_FLAG,
    E_DMD_S2_FSYNC_FAIL_SEARCH_FLAG,
    E_DMD_S2_FALSE_ALARM_FLAG,
    E_DMD_S2_VITERBI_IN_SYNC_FLAG,
    E_DMD_S2_INT_CODE_RATE_SEARCH_FAIL_FLAG,
    E_DMD_S2_VITERBI_INT_PRE_FLAG,
    E_DMD_S2_BER_WINDOW_END_FLAG,
    E_DMD_S2_PASS_WRONG_INT_FLAG,
    E_DMD_S2_CLK_CNT_OVER_FLAG,
    E_DMD_S2_UNCRT_OVER_FLAG,			//0x5F
    E_DMD_S2_DISEQC_RX_LENGTH,
    E_DMD_S2_DISEQC_INTERRUPT_FLAG,
    E_DMD_S2_DISEQC_RX_FLAG,
    E_DMD_S2_DISEQC_INTERRUPT_STATUS,
    E_DMD_S2_DISEQC_STATUS_FLAG,
    E_DMD_S2_ACI_FIR_SELECTED,			//0x65
    //LOCK
    E_DMD_S2_AGC_LOCK_FLAG,					
    E_DMD_S2_DCR_LOCK_FLAG,
    E_DMD_S2_DAGC0_LOCK_FLAG,
    E_DMD_S2_DAGC1_LOCK_FLAG,
    E_DMD_S2_DAGC2_LOCK_FLAG,
    E_DMD_S2_DAGC3_LOCK_FLAG,	
    E_DMD_S2_TR_LOCK_FLAG,
    E_DMD_S2_CLCFE_LOCK_FLAG,
    E_DMD_S2_EQ_LOCK_FLAG,							
    E_DMD_S2_PR_LOCK_FLAG,				//0x6F		
    E_DMD_S2_FSYNC_LOCK_FLAG,			
    E_DMD_S2_FSYNC_FAIL_LOCK_FLAG,	

    E_DMD_S2_MB_SWUSE12L,				//0x72
    E_DMD_S2_MB_SWUSE12H,
    E_DMD_S2_MB_SWUSE13L,
    E_DMD_S2_MB_SWUSE13H,
    E_DMD_S2_MB_SWUSE14L,
    E_DMD_S2_MB_SWUSE14H,
    E_DMD_S2_MB_SWUSE15L,
    E_DMD_S2_MB_SWUSE15H,
    E_DMD_S2_MB_SWUSE16L,
    E_DMD_S2_MB_SWUSE16H,
    E_DMD_S2_MB_SWUSE17L,
    E_DMD_S2_MB_SWUSE17H,
    E_DMD_S2_MB_SWUSE18L,				
    E_DMD_S2_MB_SWUSE18H,				//0x7F
    E_DMD_S2_MB_SWUSE19L,				
    E_DMD_S2_MB_SWUSE19H,
    E_DMD_S2_MB_SWUSE1AL,
    E_DMD_S2_MB_SWUSE1AH,
    E_DMD_S2_MB_SWUSE1BL,
    E_DMD_S2_MB_SWUSE1BH,
    E_DMD_S2_MB_SWUSE1CL,
    E_DMD_S2_MB_SWUSE1CH,
    E_DMD_S2_MB_SWUSE1DL,
    E_DMD_S2_MB_SWUSE1DH,
    E_DMD_S2_MB_SWUSE1EL,
    E_DMD_S2_MB_SWUSE1EH,
    E_DMD_S2_MB_SWUSE1FL,
    E_DMD_S2_MB_SWUSE1FH, 				//0x8D

    E_DMD_S2_MB_DMDTOP_DBG_0,
    E_DMD_S2_MB_DMDTOP_DBG_1,			//0x8F
    E_DMD_S2_MB_DMDTOP_DBG_2,
    E_DMD_S2_MB_DMDTOP_DBG_3,
    E_DMD_S2_MB_DMDTOP_DBG_4,
    E_DMD_S2_MB_DMDTOP_DBG_5,			//0x93 SYMBOLRATE_H_ADC144
    E_DMD_S2_MB_DMDTOP_DBG_6,
    E_DMD_S2_MB_DMDTOP_DBG_7,
    E_DMD_S2_MB_DMDTOP_DBG_8,     //     DiSEqC Tx : Tx_len, OddEnableMode, ToneBurstFlag
    E_DMD_S2_MB_DMDTOP_DBG_9,			//0x96 PILOT_FLAG
    E_DMD_S2_MB_DMDTOP_DBG_A,			//0x97 EMPTY
    E_DMD_S2_MB_DMDTOP_DBG_B,			//0x98 EMPTY
    	 	
    E_DMD_S2_MB_DMDTOP_SWUSE00L,
    E_DMD_S2_MB_DMDTOP_SWUSE00H,
    E_DMD_S2_MB_DMDTOP_SWUSE01L,
    E_DMD_S2_MB_DMDTOP_SWUSE01H,	
    E_DMD_S2_MB_DMDTOP_SWUSE02L,
    E_DMD_S2_MB_DMDTOP_SWUSE02H,		//0x9F
    E_DMD_S2_MB_DMDTOP_SWUSE03L,
    E_DMD_S2_MB_DMDTOP_SWUSE03H,		
    E_DMD_S2_MB_DMDTOP_SWUSE04L,
    E_DMD_S2_MB_DMDTOP_SWUSE04H,
    E_DMD_S2_MB_DMDTOP_SWUSE05L,
    E_DMD_S2_MB_DMDTOP_SWUSE05H,	
    E_DMD_S2_MB_DMDTOP_SWUSE06L,
    E_DMD_S2_MB_DMDTOP_SWUSE06H,
    E_DMD_S2_MB_DMDTOP_SWUSE07L,
    E_DMD_S2_MB_DMDTOP_SWUSE07H,	

    E_DMD_S2_MB_TOP_WR_DBG_90,
    E_DMD_S2_MB_TOP_WR_DBG_91,
    E_DMD_S2_MB_TOP_WR_DBG_92,
    E_DMD_S2_MB_TOP_WR_DBG_93,
    E_DMD_S2_MB_TOP_WR_DBG_94,
    E_DMD_S2_MB_TOP_WR_DBG_95,			//0xAF
    E_DMD_S2_MB_TOP_WR_DBG_96,			//CFO_Estimated
    E_DMD_S2_MB_TOP_WR_DBG_97,      //CFO_Estimated
    E_DMD_S2_MB_TOP_WR_DBG_98,      //DEBUG EMPTY
    E_DMD_S2_MB_TOP_WR_DBG_99,	    //DEBUG EMPTY

    E_DMD_S2_MB_DUMMY_REG_0,
    E_DMD_S2_MB_DUMMY_REG_1,
    E_DMD_S2_MB_DUMMY_REG_2,	
    E_DMD_S2_MB_DUMMY_REG_3,
    E_DMD_S2_MB_DUMMY_REG_4,
    E_DMD_S2_MB_DUMMY_REG_5,
    E_DMD_S2_MB_DUMMY_REG_6,
    E_DMD_S2_MB_DUMMY_REG_7,
    E_DMD_S2_MB_DUMMY_REG_8,	
    E_DMD_S2_MB_DUMMY_REG_9,
    E_DMD_S2_MB_DUMMY_REG_A,
    E_DMD_S2_MB_DUMMY_REG_B,			//0xBF
    E_DMD_S2_MB_DUMMY_REG_C,			
    E_DMD_S2_MB_DUMMY_REG_D,
    E_DMD_S2_MB_DUMMY_REG_E,	
    E_DMD_S2_MB_DUMMY_REG_F,
    E_DMD_S2_MB_DUMMY_REG_10,     //Empty
    E_DMD_S2_MB_DUMMY_REG_11,     //Empty

    E_DMD_S2_MB_DMDTOP_INFO_01,
    E_DMD_S2_MB_DMDTOP_INFO_02,
    E_DMD_S2_MB_DMDTOP_INFO_03,
    E_DMD_S2_MB_DMDTOP_INFO_04,		
    E_DMD_S2_MB_DMDTOP_INFO_05,
    E_DMD_S2_MB_DMDTOP_INFO_06,
    E_DMD_S2_MB_DMDTOP_INFO_07,
    E_DMD_S2_MB_DMDTOP_INFO_08,	

    E_DMD_S2_IDLE_STATE_UPDATED,
    E_DMD_S2_LOG_FLAG,					//0xCF
    E_DMD_S2_LOG_SKIP_INDEX,			
    E_DMD_S2_LOCK_COUNT,
    E_DMD_S2_NARROW_STEP_FLAG,
    E_DMD_S2_UNCORRECT_PKT_COUNT,
    E_DMD_S2_DISEQC_INIT_MODE,
    E_DMD_S2_DECIMATE_FORCED,
    E_DMD_S2_SRD_MAX_SRG_FLAG,
    E_DMD_S2_DVBS_OUTER_RETRY,

    E_DMD_S2_FORCED_DECIMATE_FLAG,
    E_DMD_S2_NO_SIGNAL_FLAG,
    E_DMD_S2_SPECTRUM_TRACK_FLAG,
    E_DMD_S2_SRD_LOCAL_SEARCH_FLAG,
    E_DMD_S2_NO_SIGNAL_RATIO_CHECK_FLAG,
    E_DMD_S2_LOW_SR_ACI_FLAG,
    E_DMD_S2_SPECTRUM_TRACKER_TIMEOUT,
    E_DMD_S2_TR_TIMEOUT,				//0xDF
    E_DMD_S2_BALANCE_TRACK,				
    E_DMD_S2_GAIN_TILT_FLAG,			//0xE1
    E_DMD_S2_SIS_EN,
    E_DMD_S2_ISSY_ACTIVE,
    E_DMD_S2_CODE_RATE,//174
    E_DMD_S2_PILOT_FLAG,
    E_DMD_S2_FEC_TYPE,
    E_DMD_S2_MOD_TYPE,//177
    E_DMD_S2_VCM_OPT,                              //0xB2  
    E_DMD_S2_OPPRO_FLAG ,
    E_DMD_S2_IS_ID,
    E_DMD_S2_CHECK_EVER_UNLOCK, 
    E_DMD_S2_PACKET_ERROR_PER_SEC_L,
    E_DMD_S2_PACKET_ERROR_PER_SEC_H,
    E_DMD_S2_IS_ID_TABLE = 0x100,         // use 32bytes length

    DVBS2_PARAM_LEN = 0x120
} DVBS_Param_2;


/// For demod init

typedef enum
{
    E_DMD_DVBS_FAIL=0,
    E_DMD_DVBS_OK=1
} DMD_DVBS_Result;


typedef struct
{
    U16 	u16Version;
    U32 	u32SymbolRate;
    DMD_DVBS_MODULATION_TYPE eQamMode;
    U32 	u32IFFreq;
    BOOL bSpecInv;
    BOOL bSerialTS;
    U8 	u8SarValue;
    U32 	u32ChkScanTimeStart;
    DMD_DVBS_LOCK_STATUS eLockStatus;
    U16 	u16Strength;
    U16 	u16Quality;
    U32 	u32Intp; //
    U32 	u32FcFs; //
    U8 	u8Qam; //
    U16 	u16SymbolRateHal; //
} DMD_DVBS_Info;

//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
/// Define converlution code rate for DVB-T and DVB-S
typedef enum
{
    HAL_DEMOD_CONV_CODE_RATE_1_2,		///< Code rate = 1/2
    HAL_DEMOD_CONV_CODE_RATE_1_3,		///< Code rate = 1/3
    HAL_DEMOD_CONV_CODE_RATE_2_3,		///< Code rate = 2/3
    HAL_DEMOD_CONV_CODE_RATE_1_4,		///< Code rate = 1/4
    HAL_DEMOD_CONV_CODE_RATE_3_4,		///< Code rate = 3/4
    HAL_DEMOD_CONV_CODE_RATE_2_5,		///< Code rate = 2/5
    HAL_DEMOD_CONV_CODE_RATE_3_5,		///< Code rate = 3/5
    HAL_DEMOD_CONV_CODE_RATE_4_5,		///< Code rate = 4/5
    HAL_DEMOD_CONV_CODE_RATE_5_6,		///< Code rate = 5/6
    HAL_DEMOD_CONV_CODE_RATE_7_8,		///< Code rate = 7/8
    HAL_DEMOD_CONV_CODE_RATE_8_9,		///< Code rate = 8/9
    HAL_DEMOD_CONV_CODE_RATE_9_10		///< Code rate = 9/10

} HAL_DEMOD_EN_CONV_CODE_RATE_TYPE;

/// Define terrestrial band width
typedef enum
{
    HAL_DEMOD_BW_MODE_6MHZ = 0,                                                   ///< 6 MHz
    HAL_DEMOD_BW_MODE_7MHZ,                                                       ///< 7 MHz
    HAL_DEMOD_BW_MODE_8MHZ                                                        ///< 8 MHz
} HAL_DEMOD_EN_TER_BW_MODE;


/// Define terrestrial constellation type
typedef enum
{
    HAL_DEMOD_TER_QPSK,                                                           ///< QPSK type
    HAL_DEMOD_TER_QAM16,                                                          ///< QAM 16 type
    HAL_DEMOD_TER_QAM64                                                           ///< QAM 64 type
} HAL_DEMOD_EN_TER_CONSTEL_TYPE;

/// Define terrestrial hierarchy information
typedef enum
{
    HAL_DEMOD_TER_HIE_NONE,                                                       ///< Non-hierarchy
    HAL_DEMOD_TER_HIE_ALPHA_1,                                                    ///< Hierarchy alpha = 1
    HAL_DEMOD_TER_HIE_ALPHA_2,                                                    ///< Hierarchy alpha = 2
    HAL_DEMOD_TER_HIE_ALPHA_4                                                     ///< Hierarchy alpha = 4
} HAL_DEMOD_EN_TER_HIE_TYPE;

/// Define terrestrial guard interval
typedef enum
{
    HAL_DEMOD_TER_GI_1_32,                                                        ///< Guard interval value = 1/32
    HAL_DEMOD_TER_GI_1_16,                                                        ///< Guard interval value = 1/16
    HAL_DEMOD_TER_GI_1_8,                                                         ///< Guard interval value = 1/8
    HAL_DEMOD_TER_GI_1_4                                                          ///< Guard interval value = 1/4
} HAL_DEMOD_EN_TER_GI_TYPE;

/// Define terrestrial transmission mode
typedef enum
{
    HAL_DEMOD_TER_FFT_2K,                                                         ///< 2k FFT mode
    HAL_DEMOD_TER_FFT_8K                                                          ///< 8k FFT mode
} HAL_DEMOD_EN_TER_FFT_MODE;

/// Define terrestrial transmission mode
typedef enum
{
    HAL_DEMOD_TER_HP_SEL,                                                         ///< High priority level selection
    HAL_DEMOD_TER_LP_SEL                                                          ///< Low priority level selection
} HAL_DEMOD_EN_TER_LEVEL_SEL;

/// Define DVB-C modulation scheme
typedef enum
{
    HAL_DEMOD_CAB_QAM16,                                                          ///< QAM 16
    HAL_DEMOD_CAB_QAM32,                                                          ///< QAM 32
    HAL_DEMOD_CAB_QAM64,                                                          ///< QAM 64
    HAL_DEMOD_CAB_QAM128,                                                         ///< QAM 128
    HAL_DEMOD_CAB_QAM256
} HAL_DEMOD_EN_CAB_CONSTEL_TYPE;


/// Define DVB-S IQ tuning mode
typedef enum
{
    HAL_DEMOD_CAB_IQ_NORMAL,                                                      ///< Normal
    HAL_DEMOD_CAB_IQ_INVERT                                                       ///< Inverse
} HAL_DEMOD_EN_CAB_IQ_MODE;


/// Define DVB-S modulatiopn scheme
typedef enum
{
    HAL_DEMOD_SAT_DVBS2,                                                          ///< DVBS2
    HAL_DEMOD_SAT_DVBS                                                            ///< DVBS
} HAL_DEMOD_EN_SAT_MOD_TYPE;

typedef enum
{
    HAL_DEMOD_SAT_QPSK,                                                           ///< QPSK
    HAL_DEMOD_SAT_8PSK,                                                           ///< 8PSK
    HAL_DEMOD_SAT_QAM16                                                           ///< QAM16
} HAL_DEMOD_EN_SAT_CONSTEL_TYPE;

/// Define DVB-S Roll-Off factor
typedef enum
{
    HAL_DEMOD_SAT_RO_35,                                                          ///< roll-off factor = 0.35
    HAL_DEMOD_SAT_RO_25,                                                          ///< roll-off factor = 0.25
    HAL_DEMOD_SAT_RO_20                                                           ///< roll-off factor = 0.20
} HAL_DEMOD_EN_SAT_ROLL_OFF_TYPE;

/// Define DVB-S IQ tuning mode
typedef enum
{
    HAL_DEMOD_SAT_IQ_NORMAL,                                                      ///< Normal
    HAL_DEMOD_SAT_IQ_INVERSE                                                      ///< Inverse
} HAL_DEMOD_EN_SAT_IQ_MODE;

/// Define Bit Error Rate range measure from signal
typedef enum
{
    HAL_DEMOD_BIT_ERR_RATIO_LOW ,                                                 ///< Low BER
    HAL_DEMOD_BIT_ERR_RATIO_MEDIUM ,                                              ///< Medium BER
    HAL_DEMOD_BIT_ERR_RATIO_HIGH                                                  ///< High BER
} HAL_DEMOD_EN_BIT_ERR_RATIO;

/// Define lock status of front end
typedef enum
{
    HAL_DEMOD_FE_UNLOCKED = 0,                                                    ///< Frontend is unlocked
    HAL_DEMOD_FE_LOCKED                                                           ///< Frontend is locked
} HAL_DEMOD_EN_FE_LOCK_STATUS;


/// Define tuning mode
/// NOTE: When this typedef is modified, the apiChScan should be rebuild.
typedef enum
{
    HAL_DEMOD_FE_TUNE_MANUAL,                                                     ///< Manual tuning to carrier
    HAL_DEMOD_FE_TUNE_AUTO,                                                       ///< Auto tuning to carrier
} HAL_DEMOD_EN_FE_TUNE_MODE;

/// Define output mode
/// NOTE: When this typedef is modified, the apiChScan should be rebuild.
typedef enum
{
    HAL_DEMOD_INTERFACE_SERIAL = 0,                                                   ///< Serial interface
    HAL_DEMOD_INTERFACE_PARALLEL                                                  ///< Parallel interface
} HAL_DEMOD_INTERFACE_MODE;

/// Define tuning paramter of DVB-T front-end
typedef struct
{
    HAL_DEMOD_EN_TER_BW_MODE                  eBandWidth;                         ///< Band width
    HAL_DEMOD_EN_TER_CONSTEL_TYPE             eConstellation;                     ///< Constellation type
    HAL_DEMOD_EN_TER_HIE_TYPE                 eHierarchy;                         ///< Hierarchy
    HAL_DEMOD_EN_TER_GI_TYPE                  eGuardInterval;                     ///< Guard interval
    HAL_DEMOD_EN_TER_FFT_MODE                 eFFT_Mode;                          ///< Transmission mode
    HAL_DEMOD_EN_CONV_CODE_RATE_TYPE          eHPCodeRate;                        ///< HP code rate
    HAL_DEMOD_EN_CONV_CODE_RATE_TYPE          eLPCodeRate;                        ///< LP code rate
    HAL_DEMOD_EN_TER_LEVEL_SEL                eLevelSel;                          ///< Select HP or LP level
} HAL_DEMOD_MS_TER_CARRIER_PARAM;

/// Define tuning paramter of DVB-C front-end
typedef struct
{
    HAL_DEMOD_EN_CAB_CONSTEL_TYPE       eConstellation;                     ///< Constellation type
    U16                          	u16SymbolRate;                      ///< Symbol rate (Ksym/sec)

    HAL_DEMOD_EN_CAB_IQ_MODE            eIQMode;                            ///< IQ Mode
    U8                           	u8TapAssign;                        ///< Tap assign
    U32                          	u32FreqOffset;                      ///< Carrier frequency offset
    U8                           	u8TuneFreqOffset;                       ///< Requeset tuner freq offset
} HAL_DEMOD_MS_CAB_CARRIER_PARAM;

/// Define tuning paramter of DVB-S front-end
typedef struct
{
    //HAL_DEMOD_EN_SAT_MOD_TYPE             eDemod_Type;                        ///< Mode type
    HAL_DEMOD_EN_SAT_CONSTEL_TYPE           eConstellation;                     ///< Constellation type
    HAL_DEMOD_EN_SAT_ROLL_OFF_TYPE          eRollOff;                           ///< Roll-Off factor
    HAL_DEMOD_EN_SAT_IQ_MODE                eIQ_Mode;                           ///< IQ mode
    HAL_DEMOD_EN_CONV_CODE_RATE_TYPE        eCodeRate;                          ///< Converlution code rate
    U32                                	u32SymbolRate;
    //MS_U8                                 u8Polarity;                         // 0: Horizon; > 0(default 1): Vertical;
    //MS_S16                                s16FreqOffset;

} HAL_DEMOD_MS_SAT_CARRIER_PARAM;

/// Define carrier paramter of digital tuner
/// NOTE: When this typedef is modified, the apiChScan should be rebuild.
typedef struct
{
    U32                          	  u32Frequency;
    union
    {
        HAL_DEMOD_MS_TER_CARRIER_PARAM        TerParam;                           ///< Paramters for DVB-T front-end
        HAL_DEMOD_MS_CAB_CARRIER_PARAM        CabParam;                           ///< Paramters for DVB-C front-end
        HAL_DEMOD_MS_SAT_CARRIER_PARAM        SatParam;                           ///< Paramters for DVB-S front-end
    };
} HAL_DEMOD_MS_FE_CARRIER_PARAM;

//--------------------------------------------------------------------
//--------------------------------------------------------------------
/*
typedef struct
{
    MS_U8        cmd_code;
    MS_U8        param[80];
} S_CMDPKTREG;

typedef enum
{
    TS_MODUL_MODE,
    TS_FFX_VALUE,
    TS_GUARD_INTERVAL,
    TS_CODE_RATE,

    TS_PARAM_MAX_NUM
}E_SIGNAL_TYPE;

typedef enum
{
    CMD_SYSTEM_INIT = 0,
    CMD_DAC_CALI,
    CMD_DVBT_CONFIG,
    CMD_DVBC_CONFIG,
    CMD_VIF_CTRL,
    CMD_FSM_CTRL,
    CMD_INDIR_RREG,
    CMD_INDIR_WREG,
    CMD_GET_INFO,
    CMD_TS_CTRL,
    CMD_TUNED_VALUE,

    CMD_MAX_NUM
}E_CMD_CODE;

typedef enum
{
    TS_PARALLEL = 0,
    TS_SERIAL = 1,

    TS_MODE_MAX_NUM
}E_TS_MODE;

typedef enum
{
    E_SYS_UNKOWN = -1,
    E_SYS_DVBT,
    E_SYS_DVBC,
    E_SYS_ATSC,
    E_SYS_VIF,
    E_SYS_DVBS,

    E_SYS_NUM
}E_SYSTEM;
*/
/// Define the quality report
typedef struct
{
    HAL_DEMOD_EN_FE_LOCK_STATUS             eLock;                              ///< Lock
    HAL_DEMOD_EN_BIT_ERR_RATIO              eBER;                               ///< Bit error rate
    float                                 	fSNR;                              	///< SNR
    float                                   fSignalLevel;                       ///< Signal Level=1~100
    U16                                	u16SignalStrength;                  ///< Signal Strength[dBm],mick
    U8                                 	u8SignalQuality;                    ///< Signal Quality,mick
    float                                   fPreBER;                            ///< xxE-xx,mick
    float                                   fPostBerTSBER;
    U32                                	u32LockTime;                        ///< LockTime
    U16                                	u16TSpacketError;                   ///< TS Packet Error
} HAL_DEMOD_MS_FE_CARRIER_STATUS;

typedef struct
{
        BOOL                           bLNBPowerOn;                                              ///< Power On/Off
        BOOL                           b22kOn;                                                           ///< LNB 22k On/Off
        BOOL                           bLNBOutLow;                                                   ///< LNB 13/18V

} HAL_DEMOD_MS_FE_CARRIER_DISEQC;

/// Define the carrier information
typedef struct
{
    HAL_DEMOD_MS_FE_CARRIER_PARAM             Param;                              ///< Carrier parameter
    HAL_DEMOD_MS_FE_CARRIER_STATUS            Status;                             ///< Quality report
    HAL_DEMOD_MS_FE_CARRIER_DISEQC            DiSEqCp;                                                      ///< DiSEqC
} HAL_DEMOD_MS_FE_CARRIER_INFO;

typedef struct
{
    // Demodulator option
    BOOL                         bX4CFE_en;                          ///< Carrier frequency estimation
    BOOL                         bPPD_en;                            ///< Tap assign estimation
    BOOL                         bIQAutoSwap_en;                     ///< IQ mode auto swap
    BOOL                         bQAMScan_en;                        ///< QAM type auto scan
    BOOL                         bFHO_en;                            ///< FHO
    BOOL                         (*fptTunerSet)(U32);             ///< Tuner set freq function pointer
} Hal_Demod_Mode;

typedef enum
{
    DMD_CONV_CODE_RATE_1_2,                                                 
    DMD_CONV_CODE_RATE_1_3,                                                 
    DMD_CONV_CODE_RATE_2_3,                                           
    DMD_CONV_CODE_RATE_1_4,                                                
    DMD_CONV_CODE_RATE_3_4,                                                 
    DMD_CONV_CODE_RATE_2_5,                                                
    DMD_CONV_CODE_RATE_3_5,                                                 
    DMD_CONV_CODE_RATE_4_5,                                                 
    DMD_CONV_CODE_RATE_5_6,                                                 
    DMD_CONV_CODE_RATE_7_8,                                                 
    DMD_CONV_CODE_RATE_8_9,                                                 
    DMD_CONV_CODE_RATE_9_10,                                               
    DMD_CONV_CODE_RATE_2_9,                                                 
    DMD_CONV_CODE_RATE_13_45,
    DMD_CONV_CODE_RATE_9_20,
    DMD_CONV_CODE_RATE_90_180,
    DMD_CONV_CODE_RATE_96_180,
    DMD_CONV_CODE_RATE_11_20,
    DMD_CONV_CODE_RATE_100_180,
    DMD_CONV_CODE_RATE_104_180,
    DMD_CONV_CODE_RATE_26_45_L,
    DMD_CONV_CODE_RATE_18_30,
    DMD_CONV_CODE_RATE_28_45,
    DMD_CONV_CODE_RATE_23_36,
    DMD_CONV_CODE_RATE_116_180,
    DMD_CONV_CODE_RATE_20_30,
    DMD_CONV_CODE_RATE_124_180,
    DMD_CONV_CODE_RATE_25_36,
    DMD_CONV_CODE_RATE_128_180,
    DMD_CONV_CODE_RATE_13_18,
    DMD_CONV_CODE_RATE_132_180,
    DMD_CONV_CODE_RATE_22_30,
    DMD_CONV_CODE_RATE_135_180,
    DMD_CONV_CODE_RATE_140_180,
    DMD_CONV_CODE_RATE_7_9,
    DMD_CONV_CODE_RATE_154_180,
    DMD_CONV_CODE_RATE_11_45,
    DMD_CONV_CODE_RATE_4_15,
    DMD_CONV_CODE_RATE_14_45,
    DMD_CONV_CODE_RATE_7_15,
    DMD_CONV_CODE_RATE_8_15,
    DMD_CONV_CODE_RATE_26_45_S,
    DMD_CONV_CODE_RATE_32_45,

} DMD_DVBS_CODE_RATE_TYPE;
/*
typedef enum 	// 4 bit 
{
	HAL_DEMOD_TPS_CODE_1_2 		= 0x00,
	HAL_DEMOD_TPS_CODE_1_3,
	HAL_DEMOD_TPS_CODE_1_4,
	HAL_DEMOD_TPS_CODE_2_3,
	HAL_DEMOD_TPS_CODE_3_4,
	HAL_DEMOD_TPS_CODE_2_5,
	HAL_DEMOD_TPS_CODE_3_5,
	HAL_DEMOD_TPS_CODE_4_5,
	HAL_DEMOD_TPS_CODE_5_6,
	HAL_DEMOD_TPS_CODE_6_7,
	HAL_DEMOD_TPS_CODE_7_8,
	HAL_DEMOD_TPS_CODE_8_9,
	HAL_DEMOD_TPS_CODE_9_10,

	HAL_DEMOD_TPS_CODE_END,

	HAL_DEMOD_TPS_CODE_UNKNOWN 	= 0x0F
} HAL_DEMOD_TPS_CODERATE_T;
*/
typedef struct
{
    U16 	u16Version;
    U32 	u32SymbolRate;
    DMD_DVBS_MODULATION_TYPE eQamMode;
    U32 	u32IFFreq;
    BOOL bSpecInv;
    BOOL bSerialTS;
    U8 	u8SarValue;
    U8 	u8TSClk;
    U32 	u32ChkScanTimeStart;
    DMD_DVBS_LOCK_STATUS eLockStatus;
    U16 	u16Strength;
    U16 	u16Quality;
    U32 	u32Intp; //
    U32 	u32FcFs; //
    U8 	u8Qam; //
    U16 	u16SymbolRateHal; //
} DMD_DVBS_Info_T;

typedef struct
{
    S32 DATA; // 2^31-1 ~ -2^31
    S8 EXP; // -128~127
}FLOAT_ST;

typedef enum
{
    add = 0,
    minus,
    multiply,
    divide
}OP_type;

#if 0
//T3
#define REG_CMD_CTRL	0x20CC
#define REG_DTA_CTRL	0x20CD
#define REG_CMD_ADDR	0x20CE
#define REG_CMD_DATA	0x20CF
#else
//T8 //addy update 0602
#define REG_CMD_CTRL	0x2F1C//MBRegBase + 0x1C
#define REG_DTA_CTRL	0x2F1D//MBRegBase + 0x1D
#define REG_CMD_ADDR	0x2F1E//MBRegBase + 0x1E
#define REG_CMD_DATA	0x2F1F//MBRegBase + 0x1F
#endif

#define _REG_START		REG_CMD_CTRL
#define _REG_END		REG_CMD_CTRL
#define _REG_DRQ		REG_DTA_CTRL
#define _REG_FSM		REG_CMD_CTRL
#define _REG_ERR		REG_DTA_CTRL

#define _BIT_START		BIT1
#define _BIT_END		BIT0
#define _BIT_DRQ		BIT0
#define _BIT_FSM		BIT3
#define _BIT_ERR		BIT7

//--------------------------------------------------------------------
/*
typedef enum
{
	// OP Mode Settings
	p_opmode_rfagc_en = 0,
	p_opmode_humdet_en,
	p_opmode_dcr_en,
	p_opmode_iqb_en,
	p_opmode_auto_iq_swap,
	p_opmode_auto_fsa_left,
	p_opmode_auto_rfmax,
	p_opmode_mode_forced,
	p_opmode_cp_forced,

	// Config Params
	pc_config_rssi,
	pc_config_zif,
	pc_config_fc_l,
	pc_config_fc_h,
	pc_config_fs_l,
	pc_config_fs_h,
	pc_config_bw,
	pc_config_fsa_left,
	pc_config_rfmax,
	pc_config_lp_sel,
	pc_config_cp,
	pc_config_mode,
	pc_config_iq_swap,
	pc_config_atv_system,
	pc_config_serial_ts,
	pc_config_ts_out_inv,
	pc_config_ts_data_swap,
	pc_config_icfo_range,

	DVBT_PARAM_LEN,
} DVBT_Param;

typedef enum
{
    E_DMD_DVBC_CFG_BW0_L = 0x19,
    E_DMD_DVBC_CFG_BW0_H = 0x1a,
    E_DMD_DVBC_CFG_QAM = 0x32,
} DVBC_Param;

typedef enum
{
    agc_ref_small = 0,
    agc_ref_large,
	agc_ref_aci,
    ripple_switch_th_l,
    ripple_switch_th_h,

    TUNED_PARAM_MAX_NUM
}E_TUNED_PARAM;

*/


//--------------------------------------------------------------------
BOOL INTERN_DVBS_GetCurrentDemodType(DMD_DVBS_DEMOD_TYPE *pDemodType);
#if 0
B16 DVBS_WriteReg(U16 u16Addr, U8 u8Data);
B16 DVBS_ReadReg(U16 u16Addr, U8 *pu8Data);
#endif
B16 DVBS_Reset ( void );
B16 DVBS_Download(void);
B16 DVBS_Active(B16 bEnable);
B16 DVBS_DSPReg_Init(void);
//B16 DVBS_Check_StepLock(E_SYSTEM system);
B16 DVBS_Get_Packet_Error(U16 *u16_data);
B16 DVBS_GetDvbcInfo( U32 * DVBC_parameter);
//B16 DVBS_Init(void);//addy update 0602
//EXTSEL B16 DVBS_Config(MS_U32 u32SymbolRate, DMD_DVBS_MODULATION_TYPE eQamMode, MS_U32 u32IFFreq, MS_BOOL bSpecInv, MS_BOOL bSerialTS, MS_U8 u8TSClk, MS_U16 *pu16_symbol_rate_list,MS_U8 u8_symbol_rate_list_num);
B16 DVBS_Config(DMD_DVBS_Info_T *pInDVBSInfo, U16 *pu16_symbol_rate_list, U8 u8_symbol_rate_list_num);
void DVBS_InitClkgen(void);
B16 DVBS_System_Init(void);
B16 DVBS_Get_Version(U16 *ver_bcd);
B16 DVBS_GetSignalStrength(U16 *pu16SignalBar);
B16 DVBS_GetSNR (U32 *f_snr);
B16 DVBS_GetSNRForAirtel (U32 *f_snr); 
BOOL DVBS_GetSignalQuality(U16 *quality);
BOOL DVBS_GetSignalQualityForAirtel(U16 *quality);
BOOL DVBS_GetPostViterbiBer(U32 *postber);//POST BER
BOOL INTERN_DVBS_GetCurrentSymbolRate(U32 *u32SymbolRate);
BOOL DVBS_GetCurrentModulationType(DMD_DVBS_MODULATION_TYPE *pQAMMode);
BOOL INTERN_DVBS_GetCurrentDemodCodeRate(DMD_DVBS_CODE_RATE_TYPE *pCodeRate);
BOOL INTERN_DVBS_Get_FreqOffset(S32 *pFreqOff, U8 u8BW);
BOOL INTERN_DVBS_GetTsDivNum(U32* u8TSDivNum,BOOL*tssouce);
BOOL DVBS_GetLock(DMD_DVBS_GETLOCK_TYPE eType,  U32 u32TimeInterval);
//float DVBS_GetTunrSignalLevel_PWR(void);
BOOL DVBS_GetPacketErr(U16 *pktErr);
BOOL DVBS_DiSEqC_Init(void);
BOOL DVBS_DiSEqC_SetLNBOut(BOOL bLow);
BOOL DVBS_DiSEqC_GetLNBOut(BOOL* bLNBOutLow);
BOOL DVBS_DiSEqC_Set22kOnOff(BOOL b22kOn);
BOOL DVBS_DiSEqC_Get22kOnOff(BOOL* b22kOn);
BOOL DVBS_DiSEqC_SendCmd(U8* pCmd,U8 u8CmdSize);
BOOL DVBS_DiSEqC_SetTxToneMode(BOOL bTxTone22kOff);
BOOL DVBS_DiSEqC_SetTone(BOOL bTone1);
BOOL DVBS_UnicableAGCCheckPower(BOOL pbAGCCheckPower);
BOOL DVBS_BlindScan_Start(U16 u16StartFreq,U16 u16EndFreq);
BOOL DVBS_BlindScan_NextFreq(BOOL* bBlindScanEnd);
BOOL DVBS_BlindScan_Cancel(void);
BOOL DVBS_BlindScan_End(void);
BOOL DVBS_BlindScan_GetChannel(U16* u16TPNum,HAL_DEMOD_MS_FE_CARRIER_PARAM *pTable);
BOOL DVBS_BlindScan_GetCurrentFreq(U32 *u32CurrentFeq);
BOOL DVBS_BlindScan_WaitCurFreqFinished(U8* u8Progress,U8 *u8FindNum, U8* blindscanlock);
BOOL DVBS_BlindScan_GetTunerFreq(U32 *u16TunerCenterFreq, U32 *u16TunerCutOffFreq);
BOOL DVBS_BlindScan_GetFoundTP(U32* u32Freq, U32* u32SymbolRate);
BOOL DTV_DVB_S_BlindScan_GetStatus(mapi_demodulator_datatype_EN_BLINDSCAN_STATUS *eStatus, BOOL *bBlindScanLock);
BOOL DTV_DVB_S_BlindScan_ScanNextFreq(BOOL *bBlindScanEnd);
BOOL DTV_DVB_S_BlindScan_Init(U32 u32StartFreq,U32 u32EndFreq);
BOOL DTV_DVB_S_BlindScan_End(void);
BOOL DVBS_GetPacketErr(U16 *pktErr);
BOOL DVBS_TR_indicator(void);
BOOL DVBS_IF_AGC(U16 *ifagc);
BOOL Packet_error_time_setting(U32 Packet_Error_Check_time,U32 *packet_error_val);
BOOL DVBS_GetPacketErrAccu (U32 *pu32PktErr, BOOL bClear_Accu);
U32 Ts_Clock_Setting_Set(BOOL Debug_on,U32 TS_CLK_Val_new);
BOOL Ts_Clock_Setting_Get(U32 *TS_CLK_Val_ori,BOOL *ts_source);
BOOL Power_Saving (void);
S16 CFO_Show(BOOL Debug_on);
void MSD_ACI_coefficient_read(void);
void MSD_show_version(void);
void MSD_outer_info(void);
void MSD_inner_info(void);
void MSD_sw_indicator(void);
void MSD_top_indicator(void);
void MSD_front_info(void);
void MSD_fft_capture(U8 u8_port, U16 u16_s1, U16 u16_s2, U8 smooth_en);
void MSD_sw_info(BOOL FFT_CAPTURE_EN);
BOOL DVBS_ReadReg2bytes(U16 u16Addr, U16 *u16Data);
BOOL DVBS2_VCM_CHECK(void);
BOOL DVBS2_Get_IS_ID_INFO(U8 *u8IS_ID, U8 *u8IS_ID_table);
BOOL DVBS2_Set_IS_ID(U8 u8IS_ID);
BOOL DVBS2_Set_Default_IS_ID(U8 *u8IS_ID, U8 *u8IS_ID_table);
//--------------------------------------------------------------------

#endif



