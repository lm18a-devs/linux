typedef unsigned char MS_BOOL;

typedef signed char MS_S8;     
typedef unsigned char MS_U8;               
typedef signed short MS_S16;
typedef unsigned short MS_U16;
typedef signed int MS_S32;
typedef unsigned int MS_U32;
typedef signed long long MS_S64;                           
typedef unsigned long long MS_U64;                        

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define MAX_INT 0x7FFFFFFF
#define Pow2_62 0x4000000000000000

typedef struct
{
  MS_S32 DATA; // 2^31-1 ~ -2^31
  MS_S8 EXP; // -128~127
}MS_FLOAT_ST;

//output = DATA*2^(EXP)

typedef enum
{
  OP_TYPE_ADD = 0,
  OP_TYPE_MINUS,
  OP_TYPE_MULTIPLY,
  OP_TYPE_DIVIDE
}eOP_TYPE;

extern MS_FLOAT_ST MS_FLOAT_OP(MS_FLOAT_ST stRn,MS_FLOAT_ST stRd, eOP_TYPE eOpcode);
