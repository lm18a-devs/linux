#include <linux/math64.h>
#include "MsFixpLib.h"

#define DBG_MSB(x)

static MS_U32 ABS_32(MS_S32 input);
static MS_U64 ABS_64(MS_S64 input);
static MS_U8 Find_MSB(MS_S64 input);
static void Normalize(MS_FLOAT_ST *input);

MS_U32 ABS_32(MS_S32 input)
{
    MS_U32 result;

    if(input < 0)
    {
        result = (-1)*input;
    }
    else
        result = input;

    return result;
}

MS_U64 ABS_64(MS_S64 input)
{
    MS_U64 result;

    if(input < 0)
    {
        result = (-1)*input;
    }
    else
        result = input;

    return result;
}

MS_U8 Find_MSB(MS_S64 input)
{
    MS_S8 iter = -1;
    MS_U64 data = ABS_64(input);

    while(0 != data)
    {
        ++iter;
        data >>= 1;
    }
    
    if(iter >= 0)
    {
        return iter;
    }   

    return 0;
 }

void Normalize(MS_FLOAT_ST *input)
{
    MS_U8 LSB, sign_flag;
    
    MS_S8 EXP;
    MS_U32 data;

    LSB = 0;

    if((*input).DATA == 0)
    {
        (*input).EXP = 0;
    }
    else
    {
        if((*input).DATA < 0) // negative value
            sign_flag = 1;
        else
            sign_flag = 0;

        data = ABS_32((*input).DATA);
        EXP = (*input).EXP;

        if(EXP != 0)
        {
           while( (data & 0x01) == 0x00 )
           {
            ++LSB;
            data >>= 1;
           }

           EXP += LSB;

           (*input).DATA = data;
           (*input).EXP = EXP;

           if(sign_flag == 1)
           {
            (*input).DATA *= (-1);
           }
        }
    }
}

MS_FLOAT_ST MS_FLOAT_OP(MS_FLOAT_ST stRn,MS_FLOAT_ST stRd, eOP_TYPE eOpcode)
{
    MS_FLOAT_ST result;

    MS_S32 data1, data2;
    MS_U32 udata1, udata2;
    MS_S8 EXP1, EXP2;
    MS_S8 iter, MSB, MSB_temp;

    MS_S64 temp, temp2;

    Normalize(&stRn);
    Normalize(&stRd);

    data1 = stRn.DATA;
    data2 = stRd.DATA;

    udata1 = ABS_32(data1);
    udata2 = ABS_32(data2);

    EXP1 = stRn.EXP;
    EXP2 = stRd.EXP;

    switch(eOpcode)
    {
        case OP_TYPE_ADD:
        {
            if(EXP1 == EXP2)
            {
                temp = data1;
                temp += data2;

                if(temp > MAX_INT || temp < (-1)*MAX_INT)
                {
                    temp >>= 1;
                    result.DATA = temp;
                    result.EXP = (EXP1 + 1);
                }
                else
                {
                    result.DATA = (data1 + data2);
                    result.EXP = EXP1;
                }
            }
            else if(EXP1 > EXP2)
            {
                temp = data1;       

                MSB = Find_MSB(temp);  
                
                if( (MSB - EXP2) < 63)              
                {
                    for(iter = EXP1; iter > EXP2;--iter)
                    {
                        temp = (temp << 1);                    
                    }

                    temp += data2;

                    if(temp > MAX_INT || temp < (-1)*MAX_INT)
                    {
                        MSB = Find_MSB(temp);

                        temp >>= (MSB-30);
                        result.DATA = temp;
                        result.EXP = (EXP2 + (MSB-30));
                    }
                    else
                    {
                        result.DATA = temp;
                        result.EXP = EXP2;
                    }
                }
                else
                {
                    result.DATA = data1;
                    result.EXP = data1;
                }
            }
            else
            {
                return MS_FLOAT_OP(stRd, stRn, OP_TYPE_ADD);
            }
        }
        break;

        case OP_TYPE_MINUS:
        {
            stRd.DATA *= (-1);
            return MS_FLOAT_OP(stRn, stRd, OP_TYPE_ADD);
        }
        break;

        case OP_TYPE_MULTIPLY:
        {
            if(data1 == 0 || data2 == 0)
            {
                result.DATA = 0;
                result.EXP = 0;
            }                
            else
            {
                temp = data1;
                temp *= data2;

                if( (temp <= MAX_INT) && (temp >= (-1*MAX_INT) ) )
                {
                    result.DATA = data1 * data2;
                    result.EXP = EXP1 + EXP2;
                }
                else // overflow
                {
                    MSB = Find_MSB(temp);

                    temp = temp >> (MSB-30);

                    result.DATA = (MS_S32)temp;
                    result.EXP = EXP1 + EXP2 + (MSB-30);
                }       
            }
        }
        break;

        case OP_TYPE_DIVIDE:
        {
            if(data1 != 0 && data2 != 0)
            {
                if(udata1 < udata2)
                {
                    temp = Pow2_62;
                    temp = div_s64(temp, data2) * data1;

                    MSB = Find_MSB(temp);

                    if(MSB > 30)
                    {                         
                        temp >>= (MSB-30);
                        result.DATA = temp;
                        result.EXP = EXP1 - EXP2 + (MSB-30) - 62;                        
                    }
                    else
                    {
                        result.DATA = temp;
                        result.EXP = EXP1 - EXP2 - 62;
                    }                   
                }
                else if(udata1 == udata2)
                {
                    result.DATA = data1 / data2;
                    result.EXP = EXP1 - EXP2;
                }
                else // udata1 > udata2
                {
                    MSB = Find_MSB(data1);     

                    DBG_MSB(printk("MSB = %u\n",MSB));

                    MSB_temp = Find_MSB(data2);

                    DBG_MSB(printk("MSB_temp = %u\n",MSB_temp));

                    EXP2 -= ((MSB-MSB_temp) + 1);

                    DBG_MSB(printk("EXP2 = %d\n",EXP2));

                    temp = Pow2_62;
                    temp2 = (MS_S64)data2 << ((MSB-MSB_temp) + 1);
                    temp = div64_s64(temp, temp2) * data1;

                    DBG_MSB(printk("temp = %lld\n",temp));
                    
                    MSB = Find_MSB(temp);       

                    if(MSB > 30)
                    {                         
                        temp >>= (MSB-30);
                        result.DATA = temp;
                        result.EXP = EXP1 - EXP2 + (MSB-30) - 62;                        
                    }
                    else
                    {
                        result.DATA = temp;
                        result.EXP = EXP1 - EXP2 - 62;
                    }                        
                }                
            }
            else
            {
                result.DATA = 0;
                result.EXP = 0;
            }
        }
        break;

        default:
        break;
    }            

    Normalize(&result);

    return result;
}
