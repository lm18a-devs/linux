//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all 
// or part of MStar Software is expressly prohibited, unless prior written 
// permission has been granted by MStar. 
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.  
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software. 
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s 
//    confidential information in strictest confidence and not disclose to any
//    third party.  
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.  
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or 
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.  
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (!��MStar Confidential Information!�L) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////


//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------

#include "drvDMD_ATSC.h"

#ifdef MSOS_TYPE_LINUX_KERNEL
#include <linux/kernel.h>
#include <linux/delay.h>
#else
#include <stdio.h>
#include <math.h>
#endif

#if DMD_ATSC_3PARTY_EN
#include "mhal_demod_reg.h"
#endif

//-------------------------------------------------------------------------------------------------
//  Driver Compiler Options
//-------------------------------------------------------------------------------------------------

#ifndef BIN_RELEASE
#define BIN_RELEASE                 0
#endif

//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------

#define _RIU_READ_BYTE(addr)        (*(volatile MS_U8*)(RIU_ARM_BASE + BYTE2REAL(addr & 0x000FFFFF)))
#define _RIU_WRITE_BYTE(addr, val)  (*(volatile MS_U8*)(RIU_ARM_BASE + BYTE2REAL(addr & 0x000FFFFF)) = val)

#define HAL_INTERN_ATSC_DBINFO(y)   //y

#ifndef MBRegBase
 #define MBRegBase                 0x112600UL
#endif
#ifndef MBRegBase_DMD1
 #define MBRegBase_DMD1            0x112400UL
#endif

#ifndef DMDMcuBase
 #define DMDMcuBase                0x103480UL
#endif

#define INTERN_ATSC_VSB_TRAIN_SNR_LIMIT   0x05//0xBE//14.5dB
#define INTERN_ATSC_FEC_ENABLE            0x1F

#define VSB_ATSC           0x04
#define QAM256_ATSC        0x02

#define QAM16_J83ABC       0x00
#define QAM32_J83ABC       0x01
#define QAM64_J83ABC       0x02
#define QAM128_J83ABC      0x03
#define QAM256_J83ABC      0x04

#define J83ABC_ENABLE           0
#define USE_PSRAM_32KB          1
#define IS_MULTI_INT_DMD        0
#define USE_T2_MERGED_FEND      1
#define REMAP_RST_BY_DRV        0
#define USE_BANK_REMAP          1
#define POW_SAVE_BY_DRV         0
#define IS_UTOF_FW              0
#define FIX_TS_NON_SYNC_ISSUE   0

#if J83ABC_ENABLE || IS_UTOF_FW
 #define INTERN_ATSC_OUTER_STATE          0xF0
#else
 #define INTERN_ATSC_OUTER_STATE          0x80
#endif

//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------

const MS_U8 INTERN_ATSC_table[] = {
    #include "LM18A_11_0_5_ATSC_demod_atsc.dat"
};

static MS_U16 u16Lib_size = sizeof(INTERN_ATSC_table);

#if (!J83ABC_ENABLE)
static MS_U8 Demod_Flow_register[21] = {0x52, 0x72, 0x52, 0x72, 0x5C, 0x5C, 0xA3, 0xEC, 0xEA,
                                        0x05, 0x74, 0x1E, 0x38, 0x3A, 0x00, 0x00, 0x00, 0x00,
                                        0x00, 0x00, 0x00};
#endif // #if (!J83ABC_ENABLE)

#ifndef UTPA2
static const float _LogApproxTableX[80] =
{ 1.00, 1.30, 1.69, 2.20, 2.86, 3.71, 4.83, 6.27, 8.16, 10.60, 
  13.79, 17.92, 23.30, 30.29, 39.37, 51.19, 66.54, 86.50, 112.46, 146.19, 
  190.05, 247.06, 321.18, 417.54, 542.80, 705.64, 917.33, 1192.53, 1550.29, 2015.38, 
  2620.00, 3405.99, 4427.79, 5756.13, 7482.97, 
  9727.86, 12646.22, 16440.08, 21372.11, 27783.74, 
  36118.86, 46954.52, 61040.88, 79353.15, 103159.09, 
  134106.82, 174338.86, 226640.52, 294632.68, 383022.48,
  497929.22, 647307.99, 841500.39, 1093950.50, 1422135.65, 
  1848776.35, 2403409.25, 3124432.03, 4061761.64, 5280290.13, 
  6864377.17, 8923690.32, 11600797.42, 15081036.65, 19605347.64, 
  25486951.94, 33133037.52, 43072948.77, 55994833.40, 72793283.42, 
  94631268.45, 123020648.99, 159926843.68, 207904896.79, 270276365.82, 
  351359275.57,456767058.24, 593797175.72, 771936328.43, 1003517226.96
};

static const float _LogApproxTableY[80] =
{ 0.00, 0.11, 0.23, 0.34, 0.46, 0.57, 0.68, 0.80, 0.91, 1.03, 
  1.14, 1.25, 1.37, 1.48, 1.60, 1.71, 1.82, 1.94, 2.05, 2.16, 
  2.28, 2.39, 2.51, 2.62  2.73, 2.85, 2.96, 3.08, 3.19, 3.30, 
  3.42, 3.53, 3.65, 3.76, 3.87, 3.99, 4.10, 4.22, 4.33, 4.44, 
  4.56, 4.67, 4.79, 4.90, 5.01, 5.13, 5.24, 5.36, 5.47, 5.58, 
  5.70, 5.81, 5.93, 6.04, 6.15, 6.27, 6.38, 6.49, 6.60, 6.72,
  6.83, 6.95, 7.06, 7.17, 7.29, 7.40, 7.52, 7.63, 7.74, 7.86,
  7.97, 8.08, 8.20, 8.31, 8.43, 8.54, 8.65, 8.77, 8.88, 9.00
};
#endif

//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------

extern MS_U8 u8DMD_ATSC_DMD_ID;

extern DMD_ATSC_ResData *psDMD_ATSC_ResData;

//-------------------------------------------------------------------------------------------------
//  Local Functions
//-------------------------------------------------------------------------------------------------
#ifndef UTPA2
#ifndef MSOS_TYPE_LINUX
static float Log10Approx(float flt_x)
{
    MS_U8  indx = 0;

    do {
        if (flt_x < _LogApproxTableX[indx])
            break;
        indx++;
    }while (indx < 79);   //stop at indx = 80

    return _LogApproxTableY[indx];
}
#endif
#endif

static MS_U8 _HAL_DMD_RIU_ReadByte(MS_U32 u32Addr)
{
    #if !DMD_ATSC_UTOPIA_EN && !DMD_ATSC_UTOPIA2_EN
    return _RIU_READ_BYTE(u32Addr);
    #else
    return _RIU_READ_BYTE(((u32Addr) << 1) - ((u32Addr) & 1));
    #endif
}

static void _HAL_DMD_RIU_WriteByte(MS_U32 u32Addr, MS_U8 u8Value)
{
    #if !DMD_ATSC_UTOPIA_EN && !DMD_ATSC_UTOPIA2_EN
    _RIU_WRITE_BYTE(u32Addr , u8Value);
    #else
    _RIU_WRITE_BYTE(((u32Addr) << 1) - ((u32Addr) & 1), u8Value);
    #endif
}

static void _HAL_DMD_RIU_WriteByteMask(MS_U32 u32Addr, MS_U8 u8Value, MS_U8 u8Mask)
{
    #if !DMD_ATSC_UTOPIA_EN && !DMD_ATSC_UTOPIA2_EN
    _RIU_WRITE_BYTE(u32Addr, (_RIU_READ_BYTE(u32Addr) & ~(u8Mask)) | ((u8Value) & (u8Mask)));
    #else
    _RIU_WRITE_BYTE((((u32Addr) <<1) - ((u32Addr) & 1)), (_RIU_READ_BYTE((((u32Addr) <<1) - ((u32Addr) & 1))) & ~(u8Mask)) | ((u8Value) & (u8Mask)));
    #endif
}

static MS_BOOL _MBX_WriteReg(MS_U16 u16Addr, MS_U8 u8Data)
{   
    #if !DMD_ATSC_UTOPIA_EN && !DMD_ATSC_UTOPIA2_EN
    DMD_ATSC_ResData *pRes = psDMD_ATSC_ResData + u8DMD_ATSC_DMD_ID;
    #endif
    
    MS_U8 u8CheckCount;
    MS_U8 u8CheckFlag = 0xFF;
    MS_U32 u32MBRegBase = MBRegBase;
    
    if (u8DMD_ATSC_DMD_ID == 0)
        u32MBRegBase = MBRegBase;
    else if (u8DMD_ATSC_DMD_ID == 1)
        u32MBRegBase = MBRegBase_DMD1;
    
    _HAL_DMD_RIU_WriteByte(u32MBRegBase + 0x00, (u16Addr&0xff));
    _HAL_DMD_RIU_WriteByte(u32MBRegBase + 0x01, (u16Addr>>8));
    _HAL_DMD_RIU_WriteByte(u32MBRegBase + 0x10, u8Data);
    _HAL_DMD_RIU_WriteByte(u32MBRegBase + 0x1E, 0x01);

    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)|0x02);    // assert interrupt to VD MCU51
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)&(~0x02)); // de-assert interrupt to VD MCU51

    for (u8CheckCount=0; u8CheckCount < 10; u8CheckCount++)
    {
        u8CheckFlag = _HAL_DMD_RIU_ReadByte(u32MBRegBase + 0x1E);
        if ((u8CheckFlag&0x01)==0)
            break;
        #if DMD_ATSC_UTOPIA_EN || DMD_ATSC_UTOPIA2_EN
        MsOS_DelayTask(1);
        #else
        pRes->sDMD_ATSC_InitData.DelayMS(1);
        #endif
    }
    
    if (u8CheckFlag&0x01)
    {
        PRINT("ERROR: ATSC INTERN DEMOD MBX WRITE TIME OUT!\n");
        return FALSE;
    }
    
    return TRUE;
}

static MS_BOOL _MBX_ReadReg(MS_U16 u16Addr, MS_U8 *u8Data)
{
    #if !DMD_ATSC_UTOPIA_EN && !DMD_ATSC_UTOPIA2_EN
    DMD_ATSC_ResData *pRes = psDMD_ATSC_ResData + u8DMD_ATSC_DMD_ID;
    #endif
    
    MS_U8 u8CheckCount;
    MS_U8 u8CheckFlag = 0xFF;
    MS_U32 u32MBRegBase = MBRegBase;
    
    if (u8DMD_ATSC_DMD_ID == 0)
        u32MBRegBase = MBRegBase;
    else if (u8DMD_ATSC_DMD_ID == 1)
        u32MBRegBase = MBRegBase_DMD1;
    
    _HAL_DMD_RIU_WriteByte(u32MBRegBase + 0x00, (u16Addr&0xff));
    _HAL_DMD_RIU_WriteByte(u32MBRegBase + 0x01, (u16Addr>>8));
    _HAL_DMD_RIU_WriteByte(u32MBRegBase + 0x1E, 0x02);
    
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)|0x02);    // assert interrupt to VD MCU51
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)&(~0x02)); // de-assert interrupt to VD MCU51

    for (u8CheckCount=0; u8CheckCount < 10; u8CheckCount++)
    {
        u8CheckFlag = _HAL_DMD_RIU_ReadByte(u32MBRegBase + 0x1E);
        if ((u8CheckFlag&0x02)==0)
        {
           *u8Data = _HAL_DMD_RIU_ReadByte(u32MBRegBase + 0x10);
            break;
        }
        #if DMD_ATSC_UTOPIA_EN || DMD_ATSC_UTOPIA2_EN
        MsOS_DelayTask(1);
        #else
        pRes->sDMD_ATSC_InitData.DelayMS(1);
        #endif
    }
    
    if (u8CheckFlag&0x02)
    {
        PRINT("ERROR: ATSC INTERN DEMOD MBX READ TIME OUT!\n");
        return FALSE;
    }
    
    return TRUE;
}

#if IS_MULTI_INT_DMD
static MS_BOOL _SEL_DMD(void)
{
    MS_U8 u8data = 0;
    
    u8data = _HAL_DMD_RIU_ReadByte(0x101e3c);
    
    if (u8DMD_ATSC_DMD_ID == 0) //select DMD0
        u8data &= (~0x10);
    else if (u8DMD_ATSC_DMD_ID == 1) //sel DMD1
        u8data |= 0x10;
    
    _HAL_DMD_RIU_WriteByte(0x101e3c, u8data);
    
    return TRUE;
}
#endif

#if !J83ABC_ENABLE
static void _initTable(void)
{
    DMD_ATSC_ResData *pRes = psDMD_ATSC_ResData + u8DMD_ATSC_DMD_ID;
    
    if (pRes->sDMD_ATSC_InitData.bTunerGainInvert)
        Demod_Flow_register[12]=1;
    else Demod_Flow_register[12]=0;
    
    if (pRes->sDMD_ATSC_InitData.bIQSwap)
        Demod_Flow_register[14] = 1;
    else Demod_Flow_register[14] = 0;
        
    Demod_Flow_register[15] =  pRes->sDMD_ATSC_InitData.u16IF_KHZ&0xFF;
    Demod_Flow_register[16] = (pRes->sDMD_ATSC_InitData.u16IF_KHZ)>>8;
    
    PRINT("\n#### IF_KHz  = [%d]\n", pRes->sDMD_ATSC_InitData.u16IF_KHZ);
    PRINT("\n#### IQ_SWAP = [%d]\n", pRes->sDMD_ATSC_InitData.bIQSwap);
    PRINT("\n#### Tuner Gain Invert = [%d]\n", pRes->sDMD_ATSC_InitData.bTunerGainInvert); 
}
#endif

static void _HAL_INTERN_ATSC_InitClk(MS_BOOL bRFAGCTristateEnable)
{
    MS_U8 u8Val = 0;

    PRINT("--------------DMD_ATSC_CHIP_M7621--------------\n");

    u8Val = _HAL_DMD_RIU_ReadByte(0x101e39);
    _HAL_DMD_RIU_WriteByte(0x101e39, u8Val&(~0x03));
              
    _HAL_DMD_RIU_WriteByte(0x1128d0,0x01);
                           
    _HAL_DMD_RIU_WriteByte(0x10331e,0x10);
                               
    _HAL_DMD_RIU_WriteByte(0x103301,0x01);
    _HAL_DMD_RIU_WriteByte(0x103300,0x11);
                               
    _HAL_DMD_RIU_WriteByte(0x103309,0x00);
    _HAL_DMD_RIU_WriteByte(0x103308,0x00);
                               
                               
                               
    _HAL_DMD_RIU_WriteByte(0x103302,0x01);
    _HAL_DMD_RIU_WriteByte(0x103302,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x103321,0x00);
    _HAL_DMD_RIU_WriteByte(0x103320,0x08);
                               
    _HAL_DMD_RIU_WriteByte(0x111f0b,0x00);
    _HAL_DMD_RIU_WriteByte(0x111f0a,0x08);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x103315,0x00);
    _HAL_DMD_RIU_WriteByte(0x103314,0x08); 
                               
    _HAL_DMD_RIU_WriteByte(0x1128d0,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x152928,0x00);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x152903,0x00);
    _HAL_DMD_RIU_WriteByte(0x152902,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x152905,0x00);
    _HAL_DMD_RIU_WriteByte(0x152904,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x152907,0x00);
    _HAL_DMD_RIU_WriteByte(0x152906,0x00);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x111f21,0x44);
    _HAL_DMD_RIU_WriteByte(0x111f20,0x40);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x111f23,0x10);
    _HAL_DMD_RIU_WriteByte(0x111f22,0x44);
                               
    _HAL_DMD_RIU_WriteByte(0x111f3b,0x08);
    _HAL_DMD_RIU_WriteByte(0x111f3a,0x08);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x111f71,0x00);
    _HAL_DMD_RIU_WriteByte(0x111f70,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x111f73,0x00);
    _HAL_DMD_RIU_WriteByte(0x111f72,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x111f79,0x11);
    _HAL_DMD_RIU_WriteByte(0x111f78,0x18);
                               
    _HAL_DMD_RIU_WriteByte(0x152991,0x88);
    _HAL_DMD_RIU_WriteByte(0x152990,0x88);
                               
    _HAL_DMD_RIU_WriteByte(0x111f69,0x44);
    _HAL_DMD_RIU_WriteByte(0x111f68,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x111f75,0x81);
    _HAL_DMD_RIU_WriteByte(0x111f74,0x11);
                               
    _HAL_DMD_RIU_WriteByte(0x111f77,0x81);
    _HAL_DMD_RIU_WriteByte(0x111f76,0x88);
                               
    _HAL_DMD_RIU_WriteByte(0x15298f,0x11);
    _HAL_DMD_RIU_WriteByte(0x15298e,0x88);
                               
    _HAL_DMD_RIU_WriteByte(0x152923,0x00);
    _HAL_DMD_RIU_WriteByte(0x152922,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x111f25,0x10);
    _HAL_DMD_RIU_WriteByte(0x111f24,0x11);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x152971,0x1c);
    _HAL_DMD_RIU_WriteByte(0x152970,0xc1);
                               
    _HAL_DMD_RIU_WriteByte(0x152977,0x04);
    _HAL_DMD_RIU_WriteByte(0x152976,0x04);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x111f6f,0x11);
    _HAL_DMD_RIU_WriteByte(0x111f6e,0x00);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x111feb,0x18);
                               
    _HAL_DMD_RIU_WriteByte(0x111f7f,0x10);
    _HAL_DMD_RIU_WriteByte(0x111f7e,0x11);
                               
    _HAL_DMD_RIU_WriteByte(0x111f31,0x14);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x152981,0x00);
    _HAL_DMD_RIU_WriteByte(0x152980,0x00);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x152983,0x00);
    _HAL_DMD_RIU_WriteByte(0x152982,0x00);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x152985,0x00);
    _HAL_DMD_RIU_WriteByte(0x152984,0x00);
                               
                               
    _HAL_DMD_RIU_WriteByte(0x152987,0x00);
    _HAL_DMD_RIU_WriteByte(0x152986,0x00);
                               
    _HAL_DMD_RIU_WriteByte(0x152979,0x11);
    _HAL_DMD_RIU_WriteByte(0x152978,0x14);
                               
    _HAL_DMD_RIU_WriteByte(0x15298d,0x81);
    _HAL_DMD_RIU_WriteByte(0x15298c,0x44);
    _HAL_DMD_RIU_WriteByteMask(0x101e39, 0x03, 0x03);
}

static MS_BOOL _HAL_INTERN_ATSC_Ready(void)
{
    #if !DMD_ATSC_UTOPIA_EN && !DMD_ATSC_UTOPIA2_EN
    DMD_ATSC_ResData *pRes  = psDMD_ATSC_ResData + u8DMD_ATSC_DMD_ID;
    #endif
    
    MS_U8 udata = 0x00;
    MS_U32 u32MBRegBase = MBRegBase;
    
    if (u8DMD_ATSC_DMD_ID == 0)
        u32MBRegBase = MBRegBase;
    else if (u8DMD_ATSC_DMD_ID == 1)
        u32MBRegBase = MBRegBase_DMD1;

    _HAL_DMD_RIU_WriteByte(u32MBRegBase + 0x1E, 0x02);

    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)|0x02);    // assert interrupt to VD MCU51
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)&(~0x02)); // de-assert interrupt to VD MCU51

    #if DMD_ATSC_UTOPIA_EN || DMD_ATSC_UTOPIA2_EN
    MsOS_DelayTask(1);
    #else
    pRes->sDMD_ATSC_InitData.DelayMS(1);
    #endif

    udata = _HAL_DMD_RIU_ReadByte(u32MBRegBase + 0x1E);

    if (udata) return FALSE;

    _MBX_ReadReg(0x20C2, &udata);
    
    if (udata == 0x05 || udata == 0x06) return FALSE;
    
    return TRUE;
}

static MS_BOOL _HAL_INTERN_ATSC_Download(void)
{
    DMD_ATSC_ResData *pRes  = psDMD_ATSC_ResData + u8DMD_ATSC_DMD_ID;
    
    MS_U8 udata = 0x00;
    MS_U16 i=0;
    MS_U16 fail_cnt=0;
    #if J83ABC_ENABLE
    MS_U8  u8TmpData;
    MS_U16 u16AddressOffset;
    #endif
     
    if (pRes->sDMD_ATSC_PriData.bDownloaded)
    {  
        if (_HAL_INTERN_ATSC_Ready())
        {
        #if REMAP_RST_BY_DRV
        _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x02, 0x02); // reset RIU remapping
        #endif
        #if (POW_SAVE_BY_DRV)
        _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x01, 0x01); //enable DMD MCU51 SRAM
        #endif
        _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x01, 0x01); // reset VD_MCU
        _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x00, 0x03); 
        
        #if DMD_ATSC_UTOPIA_EN || DMD_ATSC_UTOPIA2_EN
        MsOS_DelayTask(20);
        #else
        pRes->sDMD_ATSC_InitData.DelayMS(20);
        #endif
        return TRUE;
        }
    }
    
    #if REMAP_RST_BY_DRV
    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x02, 0x02); // reset RIU remapping
    #endif
    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x01, 0x01); // reset VD_MCU

    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x01, 0x00); // disable SRAM

    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x00, 0x01); // release MCU, madison patch

    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x03, 0x50); // enable "vdmcu51_if"
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x03, 0x51); // enable auto-increase
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x04, 0x00); // sram address low byte
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x05, 0x00); // sram address high byte

    ////  Load code thru VDMCU_IF ////
    HAL_INTERN_ATSC_DBINFO(PRINT(">Load Code...\n"));

    for (i = 0; i < u16Lib_size; i++)
    {
        _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, INTERN_ATSC_table[i]); // write data to VD MCU 51 code sram
    }

    ////  Content verification ////
    HAL_INTERN_ATSC_DBINFO(PRINT(">Verify Code...\n"));

    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x04, 0x00); // sram address low byte
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x05, 0x00); // sram address high byte

    for (i = 0; i < u16Lib_size; i++)
    {
        udata = _HAL_DMD_RIU_ReadByte(DMDMcuBase+0x10); // read sram data

        if (udata != INTERN_ATSC_table[i])
        {
            HAL_INTERN_ATSC_DBINFO(PRINT(">fail add = 0x%x\n", i));
            HAL_INTERN_ATSC_DBINFO(PRINT(">code = 0x%x\n", INTERN_ATSC_table[i]));
            HAL_INTERN_ATSC_DBINFO(PRINT(">data = 0x%x\n", udata));

            if (fail_cnt++ > 10)
            {
                HAL_INTERN_ATSC_DBINFO(PRINT(">DSP Loadcode fail!"));
                return FALSE;
            }
        }
    }

    #if !J83ABC_ENABLE
    _initTable();
    #if (USE_PSRAM_32KB)// kavana add sram  size
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x04, 0x00); // sram address low byte
    #else
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x04, 0x80); // sram address low byte
    #endif
    #if (USE_PSRAM_32KB) // kavana add sram  size
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x05, 0x70); // sram address high byte
    #else
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x05, 0x6B); // sram address high byte
    #endif
    
    for (i = 0; i < sizeof(Demod_Flow_register); i++)
    {
        _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, Demod_Flow_register[i]);
    }
    #else // #if !J83ABC_ENABLE
    u16AddressOffset = (INTERN_ATSC_table[0x400] << 8)|INTERN_ATSC_table[0x401];
    
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x04, (u16AddressOffset&0xFF)); // sram address low byte
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x05, (u16AddressOffset>>8));   // sram address high byte
     
    u8TmpData = (MS_U8)pRes->sDMD_ATSC_InitData.u16IF_KHZ;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)(pRes->sDMD_ATSC_InitData.u16IF_KHZ >> 8);
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)pRes->sDMD_ATSC_InitData.bIQSwap;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)pRes->sDMD_ATSC_InitData.u16AGC_REFERENCE;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)(pRes->sDMD_ATSC_InitData.u16AGC_REFERENCE >> 8);
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)pRes->sDMD_ATSC_InitData.u8IS_DUAL;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    u8TmpData = (MS_U8)u8DMD_ATSC_DMD_ID;
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x0C, u8TmpData); // write data to VD MCU 51 code sram
    #endif // #if !J83ABC_ENABLE
    
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x03, 0x50); // diable auto-increase
    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x03, 0x00); // disable "vdmcu51_if"
    
    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x01, 0x01); // reset MCU, madison patch

    _HAL_DMD_RIU_WriteByte(DMDMcuBase+0x01, 0x01); // enable SRAM

    _HAL_DMD_RIU_WriteByteMask(DMDMcuBase+0x00, 0x00, 0x03); // release VD_MCU
    
    pRes->sDMD_ATSC_PriData.bDownloaded = TRUE;
    
    #if DMD_ATSC_UTOPIA_EN || DMD_ATSC_UTOPIA2_EN
    MsOS_DelayTask(20);
    #else
    pRes->sDMD_ATSC_InitData.DelayMS(20);
    #endif
    
    HAL_INTERN_ATSC_DBINFO(PRINT(">DSP Loadcode done.\n"));
    
    return TRUE;
}

static void _HAL_INTERN_ATSC_FWVERSION(void)
{
    MS_U8 data1,data2,data3;
    
    #if J83ABC_ENABLE
    _MBX_ReadReg(0x20C4, &data1);
    _MBX_ReadReg(0x20C5, &data2);
    _MBX_ReadReg(0x20C6, &data3);
	#else
    _MBX_ReadReg(0x20C4, &data1);
    _MBX_ReadReg(0x20CF, &data2);
    _MBX_ReadReg(0x20D0, &data3);
	#endif

    PRINT("INTERN_ATSC_FW_VERSION:%x.%x.%x\n", data1, data2, data3);
}

static MS_BOOL _HAL_INTERN_ATSC_Exit(void)
{
    #if !DMD_ATSC_UTOPIA_EN && !DMD_ATSC_UTOPIA2_EN
    DMD_ATSC_ResData *pRes = psDMD_ATSC_ResData + u8DMD_ATSC_DMD_ID;
    #endif
    
    MS_U8 u8CheckCount = 0;
    
    _HAL_DMD_RIU_WriteByte(MBRegBase + 0x1C, 0x01);

    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)|0x02);    // assert interrupt to VD MCU51
    _HAL_DMD_RIU_WriteByte(DMDMcuBase + 0x03, _HAL_DMD_RIU_ReadByte(DMDMcuBase + 0x03)&(~0x02)); // de-assert interrupt to VD MCU51
    
    while ((_HAL_DMD_RIU_ReadByte(MBRegBase + 0x1C)&0x02) != 0x02)
    {
        #if DMD_ATSC_UTOPIA_EN || DMD_ATSC_UTOPIA2_EN
        MsOS_DelayTask(1);
        #else
        pRes->sDMD_ATSC_InitData.DelayMS(1);
        #endif
        
        if (u8CheckCount++ == 0xFF)
        {
            PRINT(">> ATSC Exit Fail!\n");
            return FALSE;
        }
    }
    
    PRINT(">> ATSC Exit Ok!\n");
    
    #if POW_SAVE_BY_DRV
    _HAL_DMD_RIU_WriteByte(0x101e83, 0x00);
    _HAL_DMD_RIU_WriteByte(0x101e82, 0x00);
    #if DMD_ATSC_UTOPIA_EN || DMD_ATSC_UTOPIA2_EN
    MsOS_DelayTask(1);
    #else
    pRes->sDMD_ATSC_InitData.DelayMS(1);
    #endif
    _HAL_DMD_RIU_WriteByte(0x101e83, 0x00);
    _HAL_DMD_RIU_WriteByte(0x101e82, 0x11);
    #endif
    
    return TRUE;
}

static MS_BOOL _HAL_INTERN_ATSC_SoftReset(void)
{
    MS_U8 u8Data = 0xFF;
    
    //Reset FSM
    if (_MBX_WriteReg(0x20C0, 0x00)==FALSE) return FALSE;
    
    #if J83ABC_ENABLE
    while (u8Data != 0x02)
    #else
    while (u8Data != 0x00)
    #endif
    {
        if (_MBX_ReadReg(0x20C1, &u8Data)==FALSE) return FALSE;
    }
    
    #if !J83ABC_ENABLE
    //Execute demod top reset
    _MBX_ReadReg(0x2002, &u8Data);
    _MBX_WriteReg(0x2002, (u8Data|0x10));
    return _MBX_WriteReg(0x2002, (u8Data&(~0x10)));
	#else
	return TRUE;
	#endif
}

static MS_BOOL _HAL_INTERN_ATSC_SetVsbMode(void)
{
    return _MBX_WriteReg(0x20C0, 0x08); 
}

static MS_BOOL _HAL_INTERN_ATSC_Set64QamMode(void)
{
    #if !J83ABC_ENABLE
    if (_MBX_WriteReg(0x20C3, 0x00)==FALSE) return FALSE;
    #endif
    return _MBX_WriteReg(0x20C0, 0x04);
}

static MS_BOOL _HAL_INTERN_ATSC_Set256QamMode(void)
{
    #if !J83ABC_ENABLE
    if (_MBX_WriteReg(0x20C3, 0x01)==FALSE) return FALSE;
    #endif
    return _MBX_WriteReg(0x20C0, 0x04);
}

static MS_BOOL _HAL_INTERN_ATSC_SetModeClean(void)
{
    return _MBX_WriteReg(0x20C0, 0x00);
}

static DMD_ATSC_DEMOD_TYPE _HAL_INTERN_ATSC_Check8VSB64_256QAM(void)
{
    MS_U8 mode = 0;

    #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x2302, &mode); //EQ mode check
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x3902, &mode); //EQ mode check
    #else
    _MBX_ReadReg(0x2A02, &mode); //EQ mode check
    #endif
    
    mode &= 0x07;
    
    if (mode == QAM16_J83ABC) return DMD_ATSC_DEMOD_ATSC_16QAM;
    else if (mode == QAM32_J83ABC) return DMD_ATSC_DEMOD_ATSC_32QAM;
    else if (mode == QAM64_J83ABC) return DMD_ATSC_DEMOD_ATSC_64QAM;
    else if (mode == QAM128_J83ABC) return DMD_ATSC_DEMOD_ATSC_128QAM;
    else if (mode == QAM256_J83ABC) return DMD_ATSC_DEMOD_ATSC_256QAM;
    else return DMD_ATSC_DEMOD_ATSC_256QAM;
    #else // #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x1700, &mode); //mode check
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x0400, &mode); //mode check  (atsc_dmd)
    #else
    _MBX_ReadReg(0x2900, &mode); //mode check
    #endif // #if J83ABC_ENABLE

    if ((mode&VSB_ATSC) == VSB_ATSC) return DMD_ATSC_DEMOD_ATSC_VSB;
    else if ((mode & QAM256_ATSC) == QAM256_ATSC) return DMD_ATSC_DEMOD_ATSC_256QAM;
    else return DMD_ATSC_DEMOD_ATSC_64QAM;
    #endif
}

static MS_BOOL _HAL_INTERN_ATSC_Vsb_QAM_AGCLock(void)
{
    MS_U8 data1 = 0;
    #if !IS_UTOF_FW
    MS_U32 u32MBRegBase = MBRegBase;
    #endif
    
    #if !IS_UTOF_FW
    if (u8DMD_ATSC_DMD_ID == 0)
        u32MBRegBase = MBRegBase;
    else if (u8DMD_ATSC_DMD_ID == 1)
        u32MBRegBase = MBRegBase_DMD1;
    
    data1 = _HAL_DMD_RIU_ReadByte(u32MBRegBase + 0x12);
    if (data1 == 0x03)
        return TRUE;
    else
        return FALSE; 
    #else
    _MBX_ReadReg(0x20C3, &data1);
    
    //if (data1 & 0x01)
    //    return FALSE;
    //else
    //    return TRUE;
    return TRUE;
    #endif
}

static MS_BOOL _HAL_INTERN_ATSC_Vsb_PreLock(void)
{
    MS_U8 data1 = 0;
    MS_U8 data2 = 0;
    MS_U16 checkValue;

    _MBX_ReadReg(0x20C2, &data1); //<0>TR_LOCK, <1>PTK_LOCK
    
    if ((data1&0x02) == 0x02)
    {
        #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
        _MBX_ReadReg(0x18EA, &data1);
        _MBX_ReadReg(0x18EB, &data2);
        #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
        _MBX_ReadReg(0x05EA, &data1); //atsc_dmdext
        _MBX_ReadReg(0x05EB, &data2); //atsc_dmdext
        #else
        _MBX_ReadReg(0x2AEA, &data1);
        _MBX_ReadReg(0x2AEB, &data2);
        #endif
        
        checkValue  = data1 << 8;
        checkValue |= data2;

        HAL_INTERN_ATSC_DBINFO(PRINT("Internal Pre Locking time :[%d]ms\n",checkValue));

        return TRUE;
    }
    else
    {
        HAL_INTERN_ATSC_DBINFO(PRINT("\nPreLock - FALSE"));
        
        return FALSE;
    }
}

static MS_BOOL _HAL_INTERN_ATSC_Vsb_FSync_Lock(void)
{
    MS_U8 data1 = 0;
    MS_U8 data2 = 0;
    MS_U16 checkValue;
 
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x1824, &data1); //<4>1:Field Sync lock = Fsync lock
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x0524, &data1); //<4>1:Field Sync lock = Fsync lock   
    #else
    _MBX_ReadReg(0x2A24, &data1); //<4>1:Field Sync lock = Fsync lock
    #endif

    if ((data1&0x10) == 0x10)
    {
        #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
        _MBX_ReadReg(0x18EE, &data1);
        _MBX_ReadReg(0x18EF, &data2);
        #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
        _MBX_ReadReg(0x05EE, &data1); //atsc_dmdext
        _MBX_ReadReg(0x05EF, &data2); //atsc_dmdext
        #else
        _MBX_ReadReg(0x2AEE, &data1);
        _MBX_ReadReg(0x2AEF, &data2);
        #endif
        
        checkValue  = data1 << 8;
        checkValue |= data2;

        HAL_INTERN_ATSC_DBINFO(PRINT("Internal Fsync Locking time :[%d]ms\n",checkValue));

        return TRUE;
    }
    else
    {
        HAL_INTERN_ATSC_DBINFO(PRINT("\nFsync Lock - FALSE"));
        
        return FALSE;
    }
}

static MS_BOOL _HAL_INTERN_ATSC_Vsb_CE_Lock(void)
{
    return TRUE;
}

static MS_BOOL _HAL_INTERN_ATSC_Vsb_FEC_Lock(void)
{
    MS_U8 data1=0, data2=0, data3=0, data4=0, data5=0;
    MS_U8 data6 =0, data7 = 0;

    _MBX_ReadReg(0x20C1, &data1);
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x1A17, &data2);//AD_NOISE_PWR_TRAIN1
    #else
    _MBX_ReadReg(0x2C17, &data2);//AD_NOISE_PWR_TRAIN1
    #endif
    _MBX_ReadReg(0x20C2, &data3);//<0>TR_LOCK, <1>PTK_LOCK
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x1901, &data4);//FEC_EN_CTL
    _MBX_ReadReg(0x1C67, &data5);//addy
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x0601, &data4);//FEC_EN_CTL (atsc_fec)
    _MBX_ReadReg(0x2D67, &data5);//addy  (atsc_eqext)
    #else
    _MBX_ReadReg(0x2B01, &data4);//FEC_EN_CTL
    _MBX_ReadReg(0x2D67, &data5);//addy
    #endif
    _MBX_ReadReg(0x1F01, &data6);
    _MBX_ReadReg(0x1F40, &data7);

    //Driver update 0426 :suggestion for field claim
    if (data1==INTERN_ATSC_OUTER_STATE &&
        ((data2<=INTERN_ATSC_VSB_TRAIN_SNR_LIMIT) || (data5 <= INTERN_ATSC_VSB_TRAIN_SNR_LIMIT)) &&
        ((data3&0x02)==0x02) &&
        ((data4&INTERN_ATSC_FEC_ENABLE)==INTERN_ATSC_FEC_ENABLE) &&
        ((data6&0x10) == 0x10) && ((data7&0x01) == 0x01))
    {
        HAL_INTERN_ATSC_DBINFO(PRINT("\nFEC Lock"));
        return TRUE;
    }
    else
    {
        HAL_INTERN_ATSC_DBINFO(PRINT("\nFEC unLock"));
        return FALSE;
    }
}

static MS_BOOL _HAL_INTERN_ATSC_QAM_PreLock(void)
{
    MS_U8 data1 = 0;
    
    #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x2250, &data1); //TR_LOCK
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x3850, &data1); //TR_LOCK
    #else
    _MBX_ReadReg(0x2950, &data1); //TR_LOCK
    #endif
    #else // #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x1B15, &data1); //TR_LOCK
    #else
    _MBX_ReadReg(0x2615, &data1); //TR_LOCK
    #endif
    #endif // #if J83ABC_ENABLE
    
    #if J83ABC_ENABLE
    if (data1&0x01)
    {
        HAL_INTERN_ATSC_DBINFO(PRINT("    QAM preLock OK  \n"));
        return TRUE;
    }
    else
    {
        HAL_INTERN_ATSC_DBINFO(PRINT("    QAM preLock NOT OK   \n"));
        return FALSE;
    }
    #else
    if((data1&0x10) == 0x10)
    {
        HAL_INTERN_ATSC_DBINFO(PRINT("    QAM preLock OK  \n"));
        return TRUE;
    }
    else
    {
        HAL_INTERN_ATSC_DBINFO(PRINT("    QAM preLock NOT OK   \n"));
        return FALSE;
    }
    #endif
}

static MS_BOOL _HAL_INTERN_ATSC_QAM_Main_Lock(void)
{
    MS_U8 data1=0, data2=0, data3=0, data4=0, data5=0, data6=0;
    #if FIX_TS_NON_SYNC_ISSUE
    static MS_U8 Last_TSP_Count=0;
    MS_U8 TSP_Count=0;
    MS_U8 check_ts_en;
    #endif
    
    #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x20C1, &data1);
    _MBX_ReadReg(0x2418, &data2); //boundary detected
    _MBX_ReadReg(0x2250, &data3); //TR_LOCK
    _MBX_ReadReg(0x2401, &data4); //FEC_EN_CTL
    _MBX_ReadReg(0x2501, &data5); //RS_backend
    _MBX_ReadReg(0x2540, &data6); //RS_backend
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x20C1, &data1);
    _MBX_ReadReg(0x0618, &data2); //boundary detected
    _MBX_ReadReg(0x3850, &data3); //TR_LOCK
    _MBX_ReadReg(0x0601, &data4); //FEC_EN_CTL
    _MBX_ReadReg(0x1F01, &data5); //RS_backend
    _MBX_ReadReg(0x1F40, &data6); //RS_backend
    #else
    _MBX_ReadReg(0x20C1, &data1);
    _MBX_ReadReg(0x2B18, &data2); //boundary detected
    _MBX_ReadReg(0x2950, &data3); //TR_LOCK
    _MBX_ReadReg(0x2B01, &data4); //FEC_EN_CTL
    _MBX_ReadReg(0x2101, &data5); //RS_backend
    _MBX_ReadReg(0x2140, &data6); //RS_backend
    #endif
       
    if (data1==INTERN_ATSC_OUTER_STATE && (data2&0x01)==0x01 &&
        data4==INTERN_ATSC_FEC_ENABLE && (data3&0x01) ==0x01 &&
        ((data5&0x10) == 0x10) && ((data6&0x01) == 0x01))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
    #else // #if J83ABC_ENABLE
    _MBX_ReadReg(0x20C1, &data1);
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x1918, &data2); //boundary detected
    _MBX_ReadReg(0x1B15, &data3); //TR_LOCK
    _MBX_ReadReg(0x1901, &data4); //FEC_EN_CTL
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x0618, &data2); //boundary detected (atsc_fec)
    _MBX_ReadReg(0x2615, &data3); //TR_LOCK    (dmd_tr)
    _MBX_ReadReg(0x0601, &data4); //FEC_EN_CTL  (atsc_fec)
    #else
    _MBX_ReadReg(0x2B18, &data2); //boundary detected
    _MBX_ReadReg(0x2615, &data3); //TR_LOCK
    _MBX_ReadReg(0x2B01, &data4); //FEC_EN_CTL
    #endif
    _MBX_ReadReg(0x1F01, &data5);
    _MBX_ReadReg(0x1F40, &data6);
        
    if (data1==INTERN_ATSC_OUTER_STATE && (data2&0x01)==0x01 &&
        data4==INTERN_ATSC_FEC_ENABLE && (data3&0x10)==0x10 &&
        ((data5&0x10) == 0x10) && ((data6&0x01) == 0x01))
    {
        #if FIX_TS_NON_SYNC_ISSUE
        TSP_Count = _HAL_DMD_RIU_ReadByte(0x161731);

        if((Last_TSP_Count != TSP_Count) && (Last_TSP_Count != 0))
        {
           _MBX_ReadReg(0x3440,&check_ts_en);
           check_ts_en = check_ts_en & (~0x4);
           _MBX_WriteReg(0x3440,check_ts_en);
           _MBX_ReadReg(0x3440,&check_ts_en);
           check_ts_en = check_ts_en | (0x4);
           _MBX_WriteReg(0x3440,check_ts_en);
        }
        
        Last_TSP_Count = TSP_Count;
        #endif
        
        return TRUE;
    }
    else
    {
        #if FIX_TS_NON_SYNC_ISSUE
        Last_TSP_Count = 0;
        #endif
        
        return FALSE;
    }
    #endif // #if J83ABC_ENABLE
}

static MS_U8 _HAL_INTERN_ATSC_ReadIFAGC(void)
{
    MS_U16 data = 0;
    
    #if J83ABC_ENABLE
    #if USE_T2_MERGED_FEND
    //debug select
    _MBX_WriteReg(0x2822, 0x03);
    //set freeze & dump
    _MBX_WriteReg(0x2805, 0x80);
    //Read High Byte of IF AGC
    _MBX_ReadReg(0x2825,  (MS_U8*)(&data));
    //Unfreeze & dump
    //set freeze & dump
    _MBX_WriteReg(0x2805, 0x00); 
    #else
    //debug select
    _MBX_WriteReg(0x2716, 0x03);
    //set freeze & dump
    _MBX_WriteReg(0x2703, 0x80);
    //Read High Byte of IF AGC
    _MBX_ReadReg(0x2719,  (MS_U8*)(&data));
    //Unfreeze & dump
    //set freeze & dump
    _MBX_WriteReg(0x2703, 0x00); 
    #endif
    #else // #if J83ABC_ENABLE
    #if USE_T2_MERGED_FEND
    //debug select
    _MBX_WriteReg(0x2822, 0x03);
    //set freeze & dump
    _MBX_WriteReg(0x2805, 0x80);
    //Read High Byte of IF AGC
    _MBX_ReadReg(0x2825,  (MS_U8*)(&data));
    //Unfreeze & dump
    //set freeze & dump
    _MBX_WriteReg(0x2805, 0x00); 
    #else
    //debug select
    _MBX_WriteReg(0x2716, 0x03);
    //set freeze & dump
    _MBX_WriteReg(0x2703, 0x80);
    //Read High Byte of IF AGC
    _MBX_ReadReg(0x2719,  (MS_U8*)(&data));
    //Unfreeze & dump
    //set freeze & dump
    _MBX_WriteReg(0x2703, 0x00); 
    #endif
    #endif // #if J83ABC_ENABLE
    
    return data;
}

static void _HAL_INTERN_ATSC_CheckSignalCondition(DMD_ATSC_SIGNAL_CONDITION* pstatus)
{
    DMD_ATSC_DEMOD_TYPE eMode;
    #if J83ABC_ENABLE
    MS_U8 u8NoisePowerH = 0, u8NoisePowerL = 0;
    static MS_U8 u8NoisePowerL_Last = 0xff;
    #else
    MS_U8 u8NoisePowerH=0;
    #endif
    static MS_U8 u8NoisePowerH_Last = 0xff;
    
    eMode = _HAL_INTERN_ATSC_Check8VSB64_256QAM();

    #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x23BE, &u8NoisePowerL); //DVBC_EQ
    _MBX_ReadReg(0x23BF, &u8NoisePowerH);
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x39BE, &u8NoisePowerL); //DVBC_EQ
    _MBX_ReadReg(0x39BF, &u8NoisePowerH);
    #else
    _MBX_ReadReg(0x2ABE, &u8NoisePowerL); //DVBC_EQ
    _MBX_ReadReg(0x2ABF, &u8NoisePowerH);
    #endif
    #else // #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x1A15, &u8NoisePowerH);
    #else
    _MBX_ReadReg(0x2C15, &u8NoisePowerH); //(atsc_eq)
    #endif
    #endif // #if J83ABC_ENABLE
    
    if (eMode == DMD_ATSC_DEMOD_ATSC_VSB) //VSB mode//SNR=10*log10((1344<<10)/noisepower)
    {
        if (!_HAL_INTERN_ATSC_Vsb_FEC_Lock()) u8NoisePowerH=0xFF;
        else if (abs(u8NoisePowerH_Last-u8NoisePowerH) > 5)
            u8NoisePowerH_Last = u8NoisePowerH;
        else u8NoisePowerH = u8NoisePowerH_Last;

        if (u8NoisePowerH > 0xBE) //SNR<14.5
            *pstatus=DMD_ATSC_SIGNAL_NO;
        else if (u8NoisePowerH > 0x4D) //SNR<18.4
            *pstatus=DMD_ATSC_SIGNAL_WEAK;
        else if (u8NoisePowerH > 0x23) //SNR<21.8
            *pstatus=DMD_ATSC_SIGNAL_MODERATE;
        else if (u8NoisePowerH > 0x0A) //SNR<26.9
            *pstatus=DMD_ATSC_SIGNAL_STRONG;
        else
            *pstatus=DMD_ATSC_SIGNAL_VERY_STRONG;
    }
    else //QAM MODE
    {
        #if J83ABC_ENABLE
        if (!_HAL_INTERN_ATSC_QAM_Main_Lock() || u8NoisePowerH) u8NoisePowerL=0xFF;
        else if (abs(u8NoisePowerL_Last-u8NoisePowerL) > 5)
            u8NoisePowerL_Last = u8NoisePowerL;
        else u8NoisePowerL = u8NoisePowerL_Last;
        
        //SNR=10*log10(65536/noisepower)
        if (eMode == DMD_ATSC_DEMOD_ATSC_256QAM)
        {
            if (u8NoisePowerL > 0x71) //SNR<27.6
                *pstatus=DMD_ATSC_SIGNAL_NO;
            else if (u8NoisePowerL > 0x31) //SNR<31.2
                *pstatus=DMD_ATSC_SIGNAL_WEAK;
            else if (u8NoisePowerL > 0x25) //SNR<32.4
                *pstatus=DMD_ATSC_SIGNAL_MODERATE;
            else if (u8NoisePowerL > 0x17) //SNR<34.4
                *pstatus=DMD_ATSC_SIGNAL_STRONG;
            else
                *pstatus=DMD_ATSC_SIGNAL_VERY_STRONG;
        }
        else
        {
            if (u8NoisePowerL > 0x1D) //SNR<21.5
                *pstatus=DMD_ATSC_SIGNAL_NO;
            else if (u8NoisePowerL > 0x14) //SNR<25.4
                *pstatus=DMD_ATSC_SIGNAL_WEAK;
            else if (u8NoisePowerL > 0x0F) //SNR<27.8
                *pstatus=DMD_ATSC_SIGNAL_MODERATE;
            else if (u8NoisePowerL > 0x0B) //SNR<31.4
                *pstatus=DMD_ATSC_SIGNAL_STRONG;
            else
                *pstatus=DMD_ATSC_SIGNAL_VERY_STRONG;
        }
        #else
        if (!_HAL_INTERN_ATSC_QAM_Main_Lock()) u8NoisePowerH=0xFF;
        else if (abs(u8NoisePowerH_Last-u8NoisePowerH) > 5)
            u8NoisePowerH_Last = u8NoisePowerH;
        else u8NoisePowerH = u8NoisePowerH_Last;
        
        if (eMode == DMD_ATSC_DEMOD_ATSC_256QAM) //256QAM//SNR=10*log10((2720<<10)/noisepower)
        {
            if (u8NoisePowerH > 0x13) //SNR<27.5
                *pstatus=DMD_ATSC_SIGNAL_NO;
            else if (u8NoisePowerH > 0x08) //SNR<31.2
                *pstatus=DMD_ATSC_SIGNAL_WEAK;
            else if (u8NoisePowerH > 0x06) //SNR<32.4
                *pstatus=DMD_ATSC_SIGNAL_MODERATE;
            else if (u8NoisePowerH > 0x04) //SNR<34.2
                *pstatus=DMD_ATSC_SIGNAL_STRONG;
            else
                *pstatus=DMD_ATSC_SIGNAL_VERY_STRONG;
        }
        else //64QAM//SNR=10*log10((2688<<10)/noisepower)
        {
            if (u8NoisePowerH > 0x4C) //SNR<21.5
                *pstatus=DMD_ATSC_SIGNAL_NO;
            else if (u8NoisePowerH > 0x1F) //SNR<25.4
                *pstatus=DMD_ATSC_SIGNAL_WEAK;
            else if (u8NoisePowerH > 0x11) //SNR<27.8
                *pstatus=DMD_ATSC_SIGNAL_MODERATE;
            else if (u8NoisePowerH > 0x07) //SNR<31.4
                *pstatus=DMD_ATSC_SIGNAL_STRONG;
            else
                *pstatus=DMD_ATSC_SIGNAL_VERY_STRONG;
        }
        #endif
    }
}

static MS_U8 _HAL_INTERN_ATSC_ReadSNRPercentage(void)
{
    DMD_ATSC_DEMOD_TYPE eMode;
    MS_U8 u8NoisePowerH = 0, u8NoisePowerL = 0;
    MS_U16 u16NoisePower;

    eMode = _HAL_INTERN_ATSC_Check8VSB64_256QAM();
    
    #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP) 
    _MBX_ReadReg(0x23BE, &u8NoisePowerL); //DVBC_EQ
    _MBX_ReadReg(0x23BF, &u8NoisePowerH);
    #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
    _MBX_ReadReg(0x39BE, &u8NoisePowerL); //DVBC_EQ
    _MBX_ReadReg(0x39BF, &u8NoisePowerH);
    #else
    _MBX_ReadReg(0x2ABE, &u8NoisePowerL); //DVBC_EQ
    _MBX_ReadReg(0x2ABF, &u8NoisePowerH);
    #endif
    #else // #if J83ABC_ENABLE
    #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
    _MBX_ReadReg(0x1A14, &u8NoisePowerL);
    _MBX_ReadReg(0x1A15, &u8NoisePowerH);
    #else
    _MBX_ReadReg(0x2C14, &u8NoisePowerL);  //atsc_eq
    _MBX_ReadReg(0x2C15, &u8NoisePowerH);
    #endif
    #endif // #if J83ABC_ENABLE
    
    u16NoisePower = (u8NoisePowerH<<8) | u8NoisePowerL;

    if (eMode == DMD_ATSC_DEMOD_ATSC_VSB) //VSB mode//SNR=10*log10((1344<<10)/noisepower)
    {
        if (!_HAL_INTERN_ATSC_Vsb_FEC_Lock())
            return 0;//SNR=0;
        else if (u16NoisePower<=0x008A)//SNR>=40dB
            return 100;//SNR=MAX_SNR;
        else if (u16NoisePower<=0x0097)//SNR>=39.6dB
            return 99;//
        else if (u16NoisePower<=0x00A5)//SNR>=39.2dB
            return 98;//
        else if (u16NoisePower<=0x00B5)//SNR>=38.8dB
            return 97;//
        else if (u16NoisePower<=0x00C7)//SNR>=38.4dB
            return 96;//
        else if (u16NoisePower<=0x00DA)//SNR>=38.0dB
            return 95;//
        else if (u16NoisePower<=0x00EF)//SNR>=37.6dB
            return 94;//
        else if (u16NoisePower<=0x0106)//SNR>=37.2dB
            return 93;//
        else if (u16NoisePower<=0x0120)//SNR>=36.8dB
            return 92;//
        else if (u16NoisePower<=0x013B)//SNR>=36.4dB
            return 91;//
        else if (u16NoisePower<=0x015A)//SNR>=36.0dB
            return 90;//
        else if (u16NoisePower<=0x017B)//SNR>=35.6dB
            return 89;//
        else if (u16NoisePower<=0x01A0)//SNR>=35.2dB
            return 88;//
        else if (u16NoisePower<=0x01C8)//SNR>=34.8dB
            return 87;//
        else if (u16NoisePower<=0x01F4)//SNR>=34.4dB
            return 86;//
        else if (u16NoisePower<=0x0224)//SNR>=34.0dB
            return 85;//
        else if (u16NoisePower<=0x0259)//SNR>=33.6dB
            return 84;//
        else if (u16NoisePower<=0x0293)//SNR>=33.2dB
            return 83;//
        else if (u16NoisePower<=0x02D2)//SNR>=32.8dB
            return 82;//
        else if (u16NoisePower<=0x0318)//SNR>=32.4dB
            return 81;//
        else if (u16NoisePower<=0x0364)//SNR>=32.0dB
            return 80;//
        else if (u16NoisePower<=0x03B8)//SNR>=31.6dB
            return 79;//
        else if (u16NoisePower<=0x0414)//SNR>=31.2dB
            return 78;//
        else if (u16NoisePower<=0x0479)//SNR>=30.8dB
            return 77;//
        else if (u16NoisePower<=0x04E7)//SNR>=30.4dB
            return 76;//
        else if (u16NoisePower<=0x0560)//SNR>=30.0dB
            return 75;//
        else if (u16NoisePower<=0x05E5)//SNR>=29.6dB
            return 74;//
        else if (u16NoisePower<=0x0677)//SNR>=29.2dB
            return 73;//
        else if (u16NoisePower<=0x0716)//SNR>=28.8dB
            return 72;//
        else if (u16NoisePower<=0x07C5)//SNR>=28.4dB
            return 71;//
        else if (u16NoisePower<=0x0885)//SNR>=28.0dB
            return 70;//
        else if (u16NoisePower<=0x0958)//SNR>=27.6dB
            return 69;//
        else if (u16NoisePower<=0x0A3E)//SNR>=27.2dB
            return 68;//
        else if (u16NoisePower<=0x0B3B)//SNR>=26.8dB
            return 67;//
        else if (u16NoisePower<=0x0C51)//SNR>=26.4dB
            return 66;//
        else if (u16NoisePower<=0x0D81)//SNR>=26.0dB
            return 65;//
        else if (u16NoisePower<=0x0ECF)//SNR>=25.6dB
            return 64;//
        else if (u16NoisePower<=0x103C)//SNR>=25.2dB
            return 63;//
        else if (u16NoisePower<=0x11CD)//SNR>=24.8dB
            return 62;//
        else if (u16NoisePower<=0x1385)//SNR>=24.4dB
            return 61;//
        else if (u16NoisePower<=0x1567)//SNR>=24.0dB
            return 60;//
        else if (u16NoisePower<=0x1778)//SNR>=23.6dB
            return 59;//
        else if (u16NoisePower<=0x19BB)//SNR>=23.2dB
            return 58;//
        else if (u16NoisePower<=0x1C37)//SNR>=22.8dB
            return 57;//
        else if (u16NoisePower<=0x1EF0)//SNR>=22.4dB
            return 56;//
        else if (u16NoisePower<=0x21EC)//SNR>=22.0dB
            return 55;//
        else if (u16NoisePower<=0x2531)//SNR>=21.6dB
            return 54;//
        else if (u16NoisePower<=0x28C8)//SNR>=21.2dB
            return 53;//
        else if (u16NoisePower<=0x2CB7)//SNR>=20.8dB
            return 52;//
        else if (u16NoisePower<=0x3108)//SNR>=20.4dB
            return 51;//
        else if (u16NoisePower<=0x35C3)//SNR>=20.0dB
            return 50;//
        else if (u16NoisePower<=0x3AF2)//SNR>=19.6dB
            return 49;//
        else if (u16NoisePower<=0x40A2)//SNR>=19.2dB
            return 48;//
        else if (u16NoisePower<=0x46DF)//SNR>=18.8dB
            return 47;//
        else if (u16NoisePower<=0x4DB5)//SNR>=18.4dB
            return 46;//
        else if (u16NoisePower<=0x5534)//SNR>=18.0dB
            return 45;//
        else if (u16NoisePower<=0x5D6D)//SNR>=17.6dB
            return 44;//
        else if (u16NoisePower<=0x6670)//SNR>=17.2dB
            return 43;//
        else if (u16NoisePower<=0x7052)//SNR>=16.8dB
            return 42;//
        else if (u16NoisePower<=0x7B28)//SNR>=16.4dB
            return 41;//
        else if (u16NoisePower<=0x870A)//SNR>=16.0dB
            return 40;//
        else if (u16NoisePower<=0x9411)//SNR>=15.6dB
            return 39;//
        else if (u16NoisePower<=0xA25A)//SNR>=15.2dB
            return 38;//
        else if (u16NoisePower<=0xB204)//SNR>=14.8dB
            return 37;//
        else if (u16NoisePower<=0xC331)//SNR>=14.4dB
            return 36;//
        else if (u16NoisePower<=0xD606)//SNR>=14.0dB
            return 35;//
        else if (u16NoisePower<=0xEAAC)//SNR>=13.6dB
            return 34;//
        else// if (u16NoisePower>=0xEAAC)//SNR<13.6dB
            return 33;//
    }
    else //QAM MODE
    {
        if( eMode == DMD_ATSC_DEMOD_ATSC_256QAM ) //256QAM//SNR=10*log10((2720<<10)/noisepower)
        {
            if (!_HAL_INTERN_ATSC_QAM_Main_Lock())
                return 0;//SNR=0;
            else if (u16NoisePower<=0x0117)//SNR>=40dB
                return 100;//
            else if (u16NoisePower<=0x0131)//SNR>=39.6dB
                return 99;//
            else if (u16NoisePower<=0x014F)//SNR>=39.2dB
                return 98;//
            else if (u16NoisePower<=0x016F)//SNR>=38.8dB
                return 97;//
            else if (u16NoisePower<=0x0193)//SNR>=38.4dB
                return 96;//
            else if (u16NoisePower<=0x01B9)//SNR>=38.0dB
                return 95;//
            else if (u16NoisePower<=0x01E4)//SNR>=37.6dB
                return 94;//
            else if (u16NoisePower<=0x0213)//SNR>=37.2dB
                return 93;//
            else if (u16NoisePower<=0x0246)//SNR>=36.8dB
                return 92;//
            else if (u16NoisePower<=0x027E)//SNR>=36.4dB
                return 91;//
            else if (u16NoisePower<=0x02BC)//SNR>=36.0dB
                return 90;//
            else if (u16NoisePower<=0x02FF)//SNR>=35.6dB
                return 89;//
            else if (u16NoisePower<=0x0349)//SNR>=35.2dB
                return 88;//
            else if (u16NoisePower<=0x039A)//SNR>=34.8dB
                return 87;//
            else if (u16NoisePower<=0x03F3)//SNR>=34.4dB
                return 86;//
            else if (u16NoisePower<=0x0455)//SNR>=34.0dB
                return 85;//
            else if (u16NoisePower<=0x04C0)//SNR>=33.6dB
                return 84;//
            else if (u16NoisePower<=0x0535)//SNR>=33.2dB
                return 83;//
            else if (u16NoisePower<=0x05B6)//SNR>=32.8dB
                return 82;//
            else if (u16NoisePower<=0x0643)//SNR>=32.4dB
                return 81;//
            else if (u16NoisePower<=0x06DD)//SNR>=32.0dB
                return 80;//
            else if (u16NoisePower<=0x0787)//SNR>=31.6dB
                return 79;//
            else if (u16NoisePower<=0x0841)//SNR>=31.2dB
                return 78;//
            else if (u16NoisePower<=0x090D)//SNR>=30.8dB
                return 77;//
            else if (u16NoisePower<=0x09EC)//SNR>=30.4dB
                return 76;//
            else if (u16NoisePower<=0x0AE1)//SNR>=30.0dB
                return 75;//
            else if (u16NoisePower<=0x0BEE)//SNR>=29.6dB
                return 74;//
            else if (u16NoisePower<=0x0D15)//SNR>=29.2dB
                return 73;//
            else if (u16NoisePower<=0x0E58)//SNR>=28.8dB
                return 72;//
            else if (u16NoisePower<=0x0FBA)//SNR>=28.4dB
                return 71;//
            else if (u16NoisePower<=0x113E)//SNR>=28.0dB
                return 70;//
            else if (u16NoisePower<=0x12E8)//SNR>=27.6dB
                return 69;//
            else if (u16NoisePower<=0x14BB)//SNR>=27.2dB
                return 68;//
            else if (u16NoisePower<=0x16BB)//SNR>=26.8dB
                return 67;//
            else if (u16NoisePower<=0x18ED)//SNR>=26.4dB
                return 66;//
            else if (u16NoisePower<=0x1B54)//SNR>=26.0dB
                return 65;//
            else if (u16NoisePower<=0x1DF7)//SNR>=25.6dB
                return 64;//
            else if (u16NoisePower<=0x20DB)//SNR>=25.2dB
                return 63;//
            else if (u16NoisePower<=0x2407)//SNR>=24.8dB
                return 62;//
            else if (u16NoisePower<=0x2781)//SNR>=24.4dB
                return 61;//
            else if (u16NoisePower<=0x2B50)//SNR>=24.0dB
                return 60;//
            else if (u16NoisePower<=0x2F7E)//SNR>=23.6dB
                return 59;//
            else if (u16NoisePower<=0x3413)//SNR>=23.2dB
                return 58;//
            else if (u16NoisePower<=0x3919)//SNR>=22.8dB
                return 57;//
            else if (u16NoisePower<=0x3E9C)//SNR>=22.4dB
                return 56;//
            else if (u16NoisePower<=0x44A6)//SNR>=22.0dB
                return 55;//
            else if (u16NoisePower<=0x4B45)//SNR>=21.6dB
                return 54;//
            else if (u16NoisePower<=0x5289)//SNR>=21.2dB
                return 53;//
            else if (u16NoisePower<=0x5A7F)//SNR>=20.8dB
                return 52;//
            else if (u16NoisePower<=0x633A)//SNR>=20.4dB
                return 51;//
            else if (u16NoisePower<=0x6CCD)//SNR>=20.0dB
                return 50;//
            else if (u16NoisePower<=0x774C)//SNR>=19.6dB
                return 49;//
            else if (u16NoisePower<=0x82CE)//SNR>=19.2dB
                return 48;//
            else if (u16NoisePower<=0x8F6D)//SNR>=18.8dB
                return 47;//
            else if (u16NoisePower<=0x9D44)//SNR>=18.4dB
                return 46;//
            else if (u16NoisePower<=0xAC70)//SNR>=18.0dB
                return 45;//
            else if (u16NoisePower<=0xBD13)//SNR>=17.6dB
                return 44;//
            else if (u16NoisePower<=0xCF50)//SNR>=17.2dB
                return 43;//
            else if (u16NoisePower<=0xE351)//SNR>=16.8dB
                return 42;//
            else if (u16NoisePower<=0xF93F)//SNR>=16.4dB
                return 41;//
            else// if (u16NoisePower>=0xF93F)//SNR<16.4dB
                return 40;//
        }
        else //64QAM//SNR=10*log10((2688<<10)/noisepower)
        {
            if (!_HAL_INTERN_ATSC_QAM_Main_Lock())
                return 0;//SNR=0;
            else if (u16NoisePower<=0x0113)//SNR>=40dB
                return 100;//
            else if (u16NoisePower<=0x012E)//SNR>=39.6dB
                return 99;//
            else if (u16NoisePower<=0x014B)//SNR>=39.2dB
                return 98;//
            else if (u16NoisePower<=0x016B)//SNR>=38.8dB
                return 97;//
            else if (u16NoisePower<=0x018E)//SNR>=38.4dB
                return 96;//
            else if (u16NoisePower<=0x01B4)//SNR>=38.0dB
                return 95;//
            else if (u16NoisePower<=0x01DE)//SNR>=37.6dB
                return 94;//
            else if (u16NoisePower<=0x020C)//SNR>=37.2dB
                return 93;//
            else if (u16NoisePower<=0x023F)//SNR>=36.8dB
                return 92;//
            else if (u16NoisePower<=0x0277)//SNR>=36.4dB
                return 91;//
            else if (u16NoisePower<=0x02B3)//SNR>=36.0dB
                return 90;//
            else if (u16NoisePower<=0x02F6)//SNR>=35.6dB
                return 89;//
            else if (u16NoisePower<=0x033F)//SNR>=35.2dB
                return 88;//
            else if (u16NoisePower<=0x038F)//SNR>=34.8dB
                return 87;//
            else if (u16NoisePower<=0x03E7)//SNR>=34.4dB
                return 86;//
            else if (u16NoisePower<=0x0448)//SNR>=34.0dB
                return 85;//
            else if (u16NoisePower<=0x04B2)//SNR>=33.6dB
                return 84;//
            else if (u16NoisePower<=0x0525)//SNR>=33.2dB
                return 83;//
            else if (u16NoisePower<=0x05A5)//SNR>=32.8dB
                return 82;//
            else if (u16NoisePower<=0x0630)//SNR>=32.4dB
                return 81;//
            else if (u16NoisePower<=0x06C9)//SNR>=32.0dB
                return 80;//
            else if (u16NoisePower<=0x0770)//SNR>=31.6dB
                return 79;//
            else if (u16NoisePower<=0x0828)//SNR>=31.2dB
                return 78;//
            else if (u16NoisePower<=0x08F1)//SNR>=30.8dB
                return 77;//
            else if (u16NoisePower<=0x09CE)//SNR>=30.4dB
                return 76;//
            else if (u16NoisePower<=0x0AC1)//SNR>=30.0dB
                return 75;//
            else if (u16NoisePower<=0x0BCA)//SNR>=29.6dB
                return 74;//
            else if (u16NoisePower<=0x0CED)//SNR>=29.2dB
                return 73;//
            else if (u16NoisePower<=0x0E2D)//SNR>=28.8dB
                return 72;//
            else if (u16NoisePower<=0x0F8B)//SNR>=28.4dB
                return 71;//
            else if (u16NoisePower<=0x110A)//SNR>=28.0dB
                return 70;//
            else if (u16NoisePower<=0x12AF)//SNR>=27.6dB
                return 69;//
            else if (u16NoisePower<=0x147D)//SNR>=27.2dB
                return 68;//
            else if (u16NoisePower<=0x1677)//SNR>=26.8dB
                return 67;//
            else if (u16NoisePower<=0x18A2)//SNR>=26.4dB
                return 66;//
            else if (u16NoisePower<=0x1B02)//SNR>=26.0dB
                return 65;//
            else if (u16NoisePower<=0x1D9D)//SNR>=25.6dB
                return 64;//
            else if (u16NoisePower<=0x2078)//SNR>=25.2dB
                return 63;//
            else if (u16NoisePower<=0x239A)//SNR>=24.8dB
                return 62;//
            else if (u16NoisePower<=0x270A)//SNR>=24.4dB
                return 61;//
            else if (u16NoisePower<=0x2ACE)//SNR>=24.0dB
                return 60;//
            else if (u16NoisePower<=0x2EEF)//SNR>=23.6dB
                return 59;//
            else if (u16NoisePower<=0x3376)//SNR>=23.2dB
                return 58;//
            else if (u16NoisePower<=0x386D)//SNR>=22.8dB
                return 57;//
            else if (u16NoisePower<=0x3DDF)//SNR>=22.4dB
                return 56;//
            else if (u16NoisePower<=0x43D7)//SNR>=22.0dB
                return 55;//
            else if (u16NoisePower<=0x4A63)//SNR>=21.6dB
                return 54;//
            else if (u16NoisePower<=0x5190)//SNR>=21.2dB
                return 53;//
            else if (u16NoisePower<=0x596E)//SNR>=20.8dB
                return 52;//
            else if (u16NoisePower<=0x620F)//SNR>=20.4dB
                return 51;//
            else if (u16NoisePower<=0x6B85)//SNR>=20.0dB
                return 50;//
            else if (u16NoisePower<=0x75E5)//SNR>=19.6dB
                return 49;//
            else if (u16NoisePower<=0x8144)//SNR>=19.2dB
                return 48;//
            else if (u16NoisePower<=0x8DBD)//SNR>=18.8dB
                return 47;//
            else if (u16NoisePower<=0x9B6A)//SNR>=18.4dB
                return 46;//
            else if (u16NoisePower<=0xAA68)//SNR>=18.0dB
                return 45;//
            else if (u16NoisePower<=0xBAD9)//SNR>=17.6dB
                return 44;//
            else if (u16NoisePower<=0xCCE0)//SNR>=17.2dB
                return 43;//
            else if (u16NoisePower<=0xE0A4)//SNR>=16.8dB
                return 42;//
            else if (u16NoisePower<=0xF650)//SNR>=16.4dB
                return 41;//
            else// if (u16NoisePower>=0xF650)//SNR<16.4dB
                return 40;//
        }
    }
}

#ifdef UTPA2
static MS_BOOL _HAL_INTERN_ATSC_GET_QAM_SNR(MS_U16 *pNoisepower, MS_U32 *pSym_num)
#else
static MS_BOOL _HAL_INTERN_ATSC_GET_QAM_SNR(float *f_snr)
#endif
{
    MS_U8 u8Data = 0;
    MS_U16 noisepower = 0;
    
    if (_HAL_INTERN_ATSC_QAM_Main_Lock())
    {
        #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
        // latch
        _MBX_WriteReg(0x2205, 0x80);
        // read noise power
        _MBX_ReadReg(0x2345, &u8Data);
        noisepower = u8Data;
        _MBX_ReadReg(0x2344, &u8Data);
        noisepower = (noisepower<<8)|u8Data;
        // unlatch
        _MBX_WriteReg(0x2205, 0x00);
        #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
        // latch
        _MBX_WriteReg(0x3805, 0x80);
        // read noise power
        _MBX_ReadReg(0x3945, &u8Data);
        noisepower = u8Data;
        _MBX_ReadReg(0x3944, &u8Data);
        noisepower = (noisepower<<8)|u8Data;
        // unlatch
        _MBX_WriteReg(0x3805, 0x00);
        #else
        // latch
        _MBX_WriteReg(0x2905, 0x80);
        // read noise power
        _MBX_ReadReg(0x2A45, &u8Data);
        noisepower = u8Data;
        _MBX_ReadReg(0x2A44, &u8Data);
        noisepower = (noisepower<<8)|u8Data;
        // unlatch
        _MBX_WriteReg(0x2905, 0x00);
        #endif

        if (noisepower == 0x0000)
            noisepower = 0x0001;

        #ifdef UTPA2
         *pNoisepower = noisepower;
         *pSym_num = 65536;
        #else
         #ifdef MSOS_TYPE_LINUX
         *f_snr = 10.0f*log10f(65536.0f/(float)noisepower);
         #else
         *f_snr = 10.0f*Log10Approx(65536.0f/(float)noisepower);
         #endif
        #endif
    }
    else
    {
        #ifdef UTPA2
        *pNoisepower = 0;
        *pSym_num = 0;
        #else
        *f_snr = 0.0f;
        #endif
    }
    
    return TRUE;
}

static MS_U16 _HAL_INTERN_ATSC_ReadPKTERR(void)
{
    MS_U16 data = 0;
    MS_U8 reg = 0;
    DMD_ATSC_DEMOD_TYPE eMode;

    eMode = _HAL_INTERN_ATSC_Check8VSB64_256QAM();
    
    if (eMode == DMD_ATSC_DEMOD_ATSC_VSB)
    {
        if (!_HAL_INTERN_ATSC_Vsb_FEC_Lock()) data = 0;
        else
        {
            _MBX_ReadReg(0x1F67, &reg);
            data = reg;
            _MBX_ReadReg(0x1F66, &reg);
            data = (data << 8)|reg;
        }
    }
    else
    {
        if (!_HAL_INTERN_ATSC_QAM_Main_Lock()) data = 0;
        else
        {
            #if J83ABC_ENABLE
            #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
            _MBX_ReadReg(0x2567, &reg);
            data = reg;
            _MBX_ReadReg(0x2566, &reg);
            data = (data << 8)|reg;
            #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
            _MBX_ReadReg(0x1F67, &reg);
            data = reg;
            _MBX_ReadReg(0x1F66, &reg);
            data = (data << 8)|reg;
            #else
            _MBX_ReadReg(0x2167, &reg);
            data = reg;
            _MBX_ReadReg(0x2166, &reg);
            data = (data << 8)|reg;
            #endif
            #else // #if J83ABC_ENABLE
            _MBX_ReadReg(0x1F67, &reg);
            data = reg;
            _MBX_ReadReg(0x1F66, &reg);
            data = (data << 8)|reg;
            #endif // #if J83ABC_ENABLE
        }
    }
    
    return data;
}

#ifdef UTPA2
static MS_BOOL _HAL_INTERN_ATSC_ReadBER(MS_U32 *pBitErr, MS_U16 *pError_window, MS_U32 *pWin_unit)
#else
static MS_BOOL _HAL_INTERN_ATSC_ReadBER(float *pBer)
#endif
{
    MS_BOOL status = TRUE;
    MS_U8 reg = 0, reg_frz = 0;
    MS_U16 BitErrPeriod;
    MS_U32 BitErr;
    DMD_ATSC_DEMOD_TYPE eMode;

    eMode = _HAL_INTERN_ATSC_Check8VSB64_256QAM();
        
    if (eMode == DMD_ATSC_DEMOD_ATSC_VSB)
    {
        if (!_HAL_INTERN_ATSC_Vsb_FEC_Lock())
        {
            #ifdef UTPA2
            *pBitErr = 0;
            *pError_window = 0;
            *pWin_unit = 0;
            #else
            *pBer = 0;
            #endif
        }
        else
        {
            _MBX_ReadReg(0x1F03, &reg_frz);
            _MBX_WriteReg(0x1F03, reg_frz|0x03);
            
            _MBX_ReadReg(0x1F47, &reg);
            BitErrPeriod = reg;
            _MBX_ReadReg(0x1F46, &reg);
            BitErrPeriod = (BitErrPeriod << 8)|reg;
            
            status &= _MBX_ReadReg(0x1F6d, &reg);
            BitErr = reg;
            status &= _MBX_ReadReg(0x1F6c, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x1F6b, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x1F6a, &reg);
            BitErr = (BitErr << 8)|reg;
            
            reg_frz=reg_frz&(~0x03);
            _MBX_WriteReg(0x1F03, reg_frz);
            
            if (BitErrPeriod == 0 )    //protect 0
                BitErrPeriod = 1;
            
            #ifdef UTPA2
            *pBitErr = BitErr;
            *pError_window = BitErrPeriod;
            *pWin_unit = 8*187*128;
            #else
            if (BitErr <=0 )
                *pBer = 0.5f / ((float)BitErrPeriod*8*187*128);
            else
                *pBer = (float)BitErr / ((float)BitErrPeriod*8*187*128);
            #endif
        }
    }
    else
    {
        if (!_HAL_INTERN_ATSC_QAM_Main_Lock())
        {
            #ifdef UTPA2
            *pBitErr = 0;
            *pError_window = 0;
            *pWin_unit = 0;
            #else
            *pBer = 0;
            #endif
        }
        else
        {
            #if J83ABC_ENABLE
            #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
            _MBX_ReadReg(0x2503, &reg_frz);
            _MBX_WriteReg(0x2503, reg_frz|0x03);

            _MBX_ReadReg(0x2547, &reg);
            BitErrPeriod = reg;
            _MBX_ReadReg(0x2546, &reg);
            BitErrPeriod = (BitErrPeriod << 8)|reg;

            status &= _MBX_ReadReg(0x256d, &reg);
            BitErr = reg;
            status &= _MBX_ReadReg(0x256c, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x256b, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x256a, &reg);
            BitErr = (BitErr << 8)|reg;

            reg_frz=reg_frz&(~0x03);
            _MBX_WriteReg(0x2503, reg_frz);
            #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
            _MBX_ReadReg(0x1F03, &reg_frz);
            _MBX_WriteReg(0x1F03, reg_frz|0x03);

            _MBX_ReadReg(0x1F47, &reg);
            BitErrPeriod = reg;
            _MBX_ReadReg(0x1F46, &reg);
            BitErrPeriod = (BitErrPeriod << 8)|reg;

            status &= _MBX_ReadReg(0x1F6d, &reg);
            BitErr = reg;
            status &= _MBX_ReadReg(0x1F6c, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x1F6b, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x1F6a, &reg);
            BitErr = (BitErr << 8)|reg;

            reg_frz=reg_frz&(~0x03);
            _MBX_WriteReg(0x1F03, reg_frz);
            #else
            _MBX_ReadReg(0x2103, &reg_frz);
            _MBX_WriteReg(0x2103, reg_frz|0x03);
            
            _MBX_ReadReg(0x2147, &reg);
            BitErrPeriod = reg;
            _MBX_ReadReg(0x2146, &reg);
            BitErrPeriod = (BitErrPeriod << 8)|reg;
            
            status &= _MBX_ReadReg(0x216d, &reg);
            BitErr = reg;
            status &= _MBX_ReadReg(0x216c, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x216b, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x216a, &reg);
            BitErr = (BitErr << 8)|reg;
            
            reg_frz=reg_frz&(~0x03);
            _MBX_WriteReg(0x2103, reg_frz);
            #endif
            
            if (BitErrPeriod == 0)    //protect 0
                BitErrPeriod = 1;
            
            #ifdef UTPA2
            *pBitErr = BitErr;
            *pError_window = BitErrPeriod;
            *pWin_unit = 8*188*128;
            #else
            if (BitErr <=0)
                *pBer = 0.5f / ((float)BitErrPeriod*8*188*128);
            else
                *pBer = (float)BitErr / ((float)BitErrPeriod*8*188*128);
            #endif
            #else //#if J83ABC_ENABLE
            _MBX_ReadReg(0x1F03, &reg_frz);
            _MBX_WriteReg(0x1F03, reg_frz|0x03);
            
            _MBX_ReadReg(0x1F47, &reg);
            BitErrPeriod = reg;
            _MBX_ReadReg(0x1F46, &reg);
            BitErrPeriod = (BitErrPeriod << 8)|reg;
            
            status &= _MBX_ReadReg(0x1F6d, &reg);
            BitErr = reg;
            status &= _MBX_ReadReg(0x1F6c, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x1F6b, &reg);
            BitErr = (BitErr << 8)|reg;
            status &= _MBX_ReadReg(0x1F6a, &reg);
            BitErr = (BitErr << 8)|reg;
            
            reg_frz=reg_frz&(~0x03);
            _MBX_WriteReg(0x1F03, reg_frz);
            
            if (BitErrPeriod == 0 )    //protect 0
                BitErrPeriod = 1;
            
            #ifdef UTPA2
            *pBitErr = BitErr;
            *pError_window = BitErrPeriod;
            *pWin_unit = 7*122*128;
            #else
            if (BitErr <=0 )
                *pBer = 0.5f / ((float)BitErrPeriod*7*122*128);
            else
                *pBer = (float)BitErr / ((float)BitErrPeriod*7*122*128);
            #endif
            #endif //#if J83ABC_ENABLE
        }
    }
    
    return status;
}

#ifdef UTPA2
static MS_BOOL _HAL_INTERN_ATSC_ReadFrequencyOffset(MS_U8 *pMode, MS_S16 *pFF, MS_S16 *pRate)
#else
static MS_S16 _HAL_INTERN_ATSC_ReadFrequencyOffset(void)
#endif
{
    DMD_ATSC_DEMOD_TYPE eMode;
    MS_U8 u8PTK_LOOP_FF_R3=0, u8PTK_LOOP_FF_R2=0;
    MS_U8 u8PTK_RATE_2=0;
    MS_U8 u8AD_CRL_LOOP_VALUE0=0, u8AD_CRL_LOOP_VALUE1=0;
    MS_U8 u8MIX_RATE_0=0, u8MIX_RATE_1=0, u8MIX_RATE_2=0;
    MS_S16 PTK_LOOP_FF;
    MS_S16 AD_CRL_LOOP_VALUE;
    MS_S16 MIX_RATE;
    #ifndef UTPA2
    MS_S16 FreqOffset = 0; //kHz
    #endif
    
    eMode = _HAL_INTERN_ATSC_Check8VSB64_256QAM();
    
    #ifdef UTPA2
    *pMode = eMode;
    #endif
    
    if (eMode == DMD_ATSC_DEMOD_ATSC_VSB) //VSB mode//
    {
        #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
        _MBX_WriteReg(0x177E, 0x01);
        _MBX_WriteReg(0x17E6, 0xff);
        _MBX_ReadReg(0x177C, &u8PTK_LOOP_FF_R2);
        _MBX_ReadReg(0x177D, &u8PTK_LOOP_FF_R3);
        _MBX_WriteReg(0x177E, 0x00);
        _MBX_WriteReg(0x17E6, 0xff);
        #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
        _MBX_WriteReg(0x047E, 0x01);   //atsc_dmd
        _MBX_WriteReg(0x04E6, 0xff);
        _MBX_ReadReg(0x047C, &u8PTK_LOOP_FF_R2);
        _MBX_ReadReg(0x047D, &u8PTK_LOOP_FF_R3);
        _MBX_WriteReg(0x047E, 0x00);
        _MBX_WriteReg(0x04E6, 0xff);
        #else
        _MBX_WriteReg(0x297E, 0x01);
        _MBX_WriteReg(0x29E6, 0xff);
        _MBX_ReadReg(0x297C, &u8PTK_LOOP_FF_R2);
        _MBX_ReadReg(0x297D, &u8PTK_LOOP_FF_R3);
        _MBX_WriteReg(0x297E, 0x00);
        _MBX_WriteReg(0x29E6, 0xff);
        #endif
        
        PTK_LOOP_FF = (u8PTK_LOOP_FF_R3<<8) | u8PTK_LOOP_FF_R2;
        #ifdef UTPA2
        *pFF = PTK_LOOP_FF;
        #else
        FreqOffset  = (float)(-PTK_LOOP_FF*0.04768);
        #endif
        
        #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
        _MBX_ReadReg(0x1782, &u8PTK_RATE_2);
        #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
        _MBX_ReadReg(0x0482, &u8PTK_RATE_2);   //atsc_dmd
        #else
        _MBX_ReadReg(0x2982, &u8PTK_RATE_2);
        #endif
        
        #ifdef UTPA2
        *pRate = u8PTK_RATE_2;
        #else
        if (u8PTK_RATE_2 == 0x07)
            FreqOffset = FreqOffset-100;
        else if (u8PTK_RATE_2 == 0x08)
            FreqOffset = FreqOffset-500;
        #endif
    }
    else //QAM MODE
    {
        #if J83ABC_ENABLE
        #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
        _MBX_ReadReg(0x2340, &u8AD_CRL_LOOP_VALUE0);
        _MBX_ReadReg(0x2341, &u8AD_CRL_LOOP_VALUE1);
        _MBX_ReadReg(0x2854, &u8MIX_RATE_0);
        _MBX_ReadReg(0x2855, &u8MIX_RATE_1);
        _MBX_ReadReg(0x2856, &u8MIX_RATE_2);
        #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
        _MBX_ReadReg(0x3940, &u8AD_CRL_LOOP_VALUE0);
        _MBX_ReadReg(0x3941, &u8AD_CRL_LOOP_VALUE1);
        _MBX_ReadReg(0x2854, &u8MIX_RATE_0);
        _MBX_ReadReg(0x2855, &u8MIX_RATE_1);
        _MBX_ReadReg(0x2856, &u8MIX_RATE_2);
        #else
        _MBX_ReadReg(0x2A40, &u8AD_CRL_LOOP_VALUE0);
        _MBX_ReadReg(0x2A41, &u8AD_CRL_LOOP_VALUE1);
        _MBX_ReadReg(0x2758, &u8MIX_RATE_0);
        _MBX_ReadReg(0x2759, &u8MIX_RATE_1);
        _MBX_ReadReg(0x275A, &u8MIX_RATE_2);
        #endif
        
        AD_CRL_LOOP_VALUE = (u8AD_CRL_LOOP_VALUE1 << 8) | u8AD_CRL_LOOP_VALUE0;
        MIX_RATE = ((u8MIX_RATE_2 << 16) | (u8MIX_RATE_1 << 8) | u8MIX_RATE_0) >> 4;
        
        #ifdef UTPA2
        *pMode |= 0x80;
        *pFF = AD_CRL_LOOP_VALUE;
        *pRate = MIX_RATE;
        #else
        if (eMode == DMD_ATSC_DEMOD_ATSC_256QAM) //256QAM//
        {
            FreqOffset = (float)(AD_CRL_LOOP_VALUE*0.0000199); //5.360537E6/2^28*1000
        }
        else if (eMode == DMD_ATSC_DEMOD_ATSC_64QAM)//64QAM//
        {
            FreqOffset = (float)(AD_CRL_LOOP_VALUE*0.0000188); //5.056941E6/2^21*1000
        }
        
        FreqOffset = FreqOffset+(float)(MIX_RATE-0x3A07)/330.13018; //(0.001/25.41*2^27/16)???
        #endif
        #else //#if J83ABC_ENABLE
        #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
        _MBX_ReadReg(0x1A04, &u8AD_CRL_LOOP_VALUE0);
        _MBX_ReadReg(0x1A05, &u8AD_CRL_LOOP_VALUE1);
        #else
        _MBX_ReadReg(0x2C04, &u8AD_CRL_LOOP_VALUE0);
        _MBX_ReadReg(0x2C05, &u8AD_CRL_LOOP_VALUE1);
        #endif
        
        AD_CRL_LOOP_VALUE = (u8AD_CRL_LOOP_VALUE1<<8) | u8AD_CRL_LOOP_VALUE0;
        
        #if (USE_T2_MERGED_FEND && !USE_BANK_REMAP)
        _MBX_ReadReg(0x1704, &u8MIX_RATE_0);
        _MBX_ReadReg(0x1705, &u8MIX_RATE_1);
        _MBX_ReadReg(0x1706, &u8MIX_RATE_2);
        #elif (USE_T2_MERGED_FEND && USE_BANK_REMAP)
        _MBX_ReadReg(0x0404, &u8MIX_RATE_0);  //atsc_dmd
        _MBX_ReadReg(0x0405, &u8MIX_RATE_1);
        _MBX_ReadReg(0x0406, &u8MIX_RATE_2);
        #else
        _MBX_ReadReg(0x2904, &u8MIX_RATE_0);
        _MBX_ReadReg(0x2905, &u8MIX_RATE_1);
        _MBX_ReadReg(0x2906, &u8MIX_RATE_2);
        #endif
        
        MIX_RATE = (u8MIX_RATE_2<<12)|(u8MIX_RATE_1<<4)|(u8MIX_RATE_0>>4);
        
        #ifdef UTPA2
        *pFF = AD_CRL_LOOP_VALUE;
        *pRate = MIX_RATE;
        #else
        if (eMode == DMD_ATSC_DEMOD_ATSC_256QAM) //256QAM//
        {
            FreqOffset = (float)(AD_CRL_LOOP_VALUE*0.0025561); //5.360537E6/2^21*1000
        }
        else if (eMode == DMD_ATSC_DEMOD_ATSC_64QAM)//64QAM//
        {
            FreqOffset = (float)(AD_CRL_LOOP_VALUE*0.00241134); //5.056941E6/2^21*1000
        }
        
        FreqOffset = FreqOffset+(float)(MIX_RATE-0x3D70)/2.62144; //(0.001/25*2^20/16)
        #endif
        #endif //#if J83ABC_ENABLE
    }
    
    #ifdef UTPA2
    return TRUE;
    #else
    return FreqOffset;
    #endif
}

static MS_BOOL _HAL_INTERN_ATSC_GetReg(MS_U16 u16Addr, MS_U8 *pu8Data)
{
    return _MBX_ReadReg(u16Addr, pu8Data);
}

static MS_BOOL _HAL_INTERN_ATSC_SetReg(MS_U16 u16Addr, MS_U8 u8Data)
{
    return _MBX_WriteReg(u16Addr, u8Data);
}

//-------------------------------------------------------------------------------------------------
//  Global Functions
//-------------------------------------------------------------------------------------------------
MS_BOOL HAL_INTERN_ATSC_IOCTL_CMD(DMD_ATSC_HAL_COMMAND eCmd, void *pArgs)
{
    MS_BOOL bResult = TRUE;
    
    #if IS_MULTI_INT_DMD
    _SEL_DMD();
    #endif
    
    switch(eCmd)
    {
    case DMD_ATSC_HAL_CMD_Exit:
        bResult = _HAL_INTERN_ATSC_Exit();
        break;
    case DMD_ATSC_HAL_CMD_InitClk:
        _HAL_INTERN_ATSC_InitClk(false);
        break;
    case DMD_ATSC_HAL_CMD_Download:
        bResult = _HAL_INTERN_ATSC_Download();
        break;
    case DMD_ATSC_HAL_CMD_FWVERSION:
        _HAL_INTERN_ATSC_FWVERSION();
        break;
    case DMD_ATSC_HAL_CMD_SoftReset:
        bResult = _HAL_INTERN_ATSC_SoftReset();
        break;
    case DMD_ATSC_HAL_CMD_SetVsbMode:
        bResult = _HAL_INTERN_ATSC_SetVsbMode();
        break;
    case DMD_ATSC_HAL_CMD_Set64QamMode:
        bResult = _HAL_INTERN_ATSC_Set64QamMode();
        break;
    case DMD_ATSC_HAL_CMD_Set256QamMode:
        bResult = _HAL_INTERN_ATSC_Set256QamMode();
        break;
    case DMD_ATSC_HAL_CMD_SetModeClean:
        bResult = _HAL_INTERN_ATSC_SetModeClean();
        break;
    case DMD_ATSC_HAL_CMD_Set_QAM_SR:
        break;
    case DMD_ATSC_HAL_CMD_Active:
        break;
    case DMD_ATSC_HAL_CMD_Check8VSB64_256QAM:
        *((DMD_ATSC_DEMOD_TYPE *)pArgs) = _HAL_INTERN_ATSC_Check8VSB64_256QAM();
        break;
    case DMD_ATSC_HAL_CMD_AGCLock:
        bResult = _HAL_INTERN_ATSC_Vsb_QAM_AGCLock();
        break;
    case DMD_ATSC_HAL_CMD_Vsb_PreLock:
        bResult = _HAL_INTERN_ATSC_Vsb_PreLock();
        break;
    case DMD_ATSC_HAL_CMD_Vsb_FSync_Lock:
        bResult = _HAL_INTERN_ATSC_Vsb_FSync_Lock();
        break;
    case DMD_ATSC_HAL_CMD_Vsb_CE_Lock:
        bResult = _HAL_INTERN_ATSC_Vsb_CE_Lock();
        break;
    case DMD_ATSC_HAL_CMD_Vsb_FEC_Lock:
        bResult = _HAL_INTERN_ATSC_Vsb_FEC_Lock();
        break;
    case DMD_ATSC_HAL_CMD_QAM_PreLock:
        bResult = _HAL_INTERN_ATSC_QAM_PreLock();
        break;
    case DMD_ATSC_HAL_CMD_QAM_Main_Lock:
        bResult = _HAL_INTERN_ATSC_QAM_Main_Lock();
        break;
    case DMD_ATSC_HAL_CMD_ReadIFAGC:
        *((MS_U16 *)pArgs) = _HAL_INTERN_ATSC_ReadIFAGC();
        break;
    case DMD_ATSC_HAL_CMD_CheckSignalCondition:
        _HAL_INTERN_ATSC_CheckSignalCondition((DMD_ATSC_SIGNAL_CONDITION *)pArgs);
        break;
    case DMD_ATSC_HAL_CMD_ReadSNRPercentage:
        *((MS_U8 *)pArgs) = _HAL_INTERN_ATSC_ReadSNRPercentage();
        break;
    case DMD_ATSC_HAL_CMD_GET_QAM_SNR:
        #ifdef UTPA2
        bResult = _HAL_INTERN_ATSC_GET_QAM_SNR(&((*((DMD_ATSC_SNR_DATA *)pArgs)).noisepower), &((*((DMD_ATSC_SNR_DATA *)pArgs)).sym_num));
        #else
        bResult = _HAL_INTERN_ATSC_GET_QAM_SNR((float *)pArgs);
        #endif
        break;
    case DMD_ATSC_HAL_CMD_ReadPKTERR:
        *((MS_U16 *)pArgs) = _HAL_INTERN_ATSC_ReadPKTERR();
        break;
    case DMD_ATSC_HAL_CMD_GetPreViterbiBer:
        break;
    case DMD_ATSC_HAL_CMD_GetPostViterbiBer:
        #ifdef UTPA2
        bResult = _HAL_INTERN_ATSC_ReadBER(&((*((DMD_ATSC_BER_DATA *)pArgs)).BitErr), &((*((DMD_ATSC_BER_DATA *)pArgs)).Error_window), &((*((DMD_ATSC_BER_DATA *)pArgs)).Win_unit));
        #else
        bResult = _HAL_INTERN_ATSC_ReadBER((float *)pArgs);
        #endif
        break;
    case DMD_ATSC_HAL_CMD_ReadFrequencyOffset:
        #ifdef UTPA2
        bResult = _HAL_INTERN_ATSC_ReadFrequencyOffset(&((*((DMD_ATSC_CFO_DATA *)pArgs)).Mode), &((*((DMD_ATSC_CFO_DATA *)pArgs)).FF), &((*((DMD_ATSC_CFO_DATA *)pArgs)).Rate));
        #else
        *((MS_S16 *)pArgs) = _HAL_INTERN_ATSC_ReadFrequencyOffset();
        #endif
        break;
    case DMD_ATSC_HAL_CMD_TS_INTERFACE_CONFIG:
        break;
    case DMD_ATSC_HAL_CMD_IIC_Bypass_Mode:
        break;
    case DMD_ATSC_HAL_CMD_SSPI_TO_GPIO:
        break;
    case DMD_ATSC_HAL_CMD_GPIO_GET_LEVEL:
        break;
    case DMD_ATSC_HAL_CMD_GPIO_SET_LEVEL:
        break;
    case DMD_ATSC_HAL_CMD_GPIO_OUT_ENABLE:
        break;
    case DMD_ATSC_HAL_CMD_GET_REG:
        bResult = _HAL_INTERN_ATSC_GetReg((*((DMD_ATSC_REG_DATA *)pArgs)).u16Addr, &((*((DMD_ATSC_REG_DATA *)pArgs)).u8Data));
        break;
    case DMD_ATSC_HAL_CMD_SET_REG:
        bResult = _HAL_INTERN_ATSC_SetReg((*((DMD_ATSC_REG_DATA *)pArgs)).u16Addr, (*((DMD_ATSC_REG_DATA *)pArgs)).u8Data);
        break;
    default:
        break;
    }
    
    return bResult;
}

MS_BOOL MDrv_DMD_ATSC_Initial_Hal_Interface(void)
{
    return TRUE;
}
