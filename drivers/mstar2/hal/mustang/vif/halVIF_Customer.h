////////////////////////////////////////////////////////////////////////////////
////
//// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
////
// You can redistribute it and/or modify it under the terms of the GNU General Public
// License version 2 as published by the Free Foundation. This program is distributed
// in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
////
//////////////////////////////////////////////////////////////////////////////////

#ifndef _HALVIF_CUSTOMER_H_
#define _HALVIF_CUSTOMER_H_

#define LOPEZ           '1'
#define ERIS            '2'
#define PLUTO           '3'
#define GEMINI          '4'
#define METIS           '5'
#define MARTINA         '6'
#define EUCLID          '7'
#define TITANIA3        '8'
#define TITANIA4        '9'
#define TITANIA7        'A'
#define Janus           'B'
#define TITANIA8        'C'
#define TITANIA9        'D'
#define MARIA10         'E'
#define TITANIA12       'F'
#define TITANIA13        "10"
#define JANUS2           "11" 
#define AMBER1           "12" 
#define NUGGET           "13"
#define NAPOLI           "14"
#define MONACO           "15"
#define MUJI             "16"
#define MUNICH           "17"
#define MUSTANG        "18"
//=======================================
//
//// chip
#define CHIP_TYPE       MUSTANG
#define _2_DIGIT_CHIP_TYPE_
#endif //_HALVIF_CUSTOMER_H_
