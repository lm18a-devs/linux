//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    drvGE.c
/// @brief  GE Driver Interface
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _DRV_GE_C_
#define _DRV_GE_C_


//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------
#include <linux/delay.h>


#include "khalGE.h"
#include "kdrvGE.h"
#include "kdrvGE_private.h"
#include "kregGE.h"
//	#include "osalGE.h"
#include "kregGE.h"

GE_CTX_LOCAL g_drvGECtxLocal;
GE_CTX_SHARED g_drvGECtxShared;

#define HAL_MIU1_BASE               0x80000000UL // 1512MB

//-------------------------------------------------------------------------------------------------
//  Driver Compiler Options
//-------------------------------------------------------------------------------------------------
#define GE_DEBUG_ENABLE             0UL

#define ENABLE_PWR          0UL   // FIXME: for porting
#define MS_DEBUG                    1UL

//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------

#if GE_LOCK_SUPPORT
#define GE_ENTRY(pGECtx)                  do{\
                                                    if(!GE_API_MUTEX)\
                                                    {\
                                                        GE_BeginDraw(pGECtx);\
                                                    }\
                                          }while(0)
#define GE_RETURN(pGECtx, _ret)           do{\
                                                    if(!GE_API_MUTEX)\
                                                        GE_EndDraw(pGECtx);return _ret;\
                                          }while(0)
#else
#define GE_ENTRY(pGECtx)                  do{}while(0);
#define GE_RETURN(pGECtx, _ret)           return _ret;
#endif


#if GE_DEBUG_ENABLE
#define GE_DBG(_fmt, _args...)      printk(_fmt, ##_args)
#else
#define GE_DBG(_fmt, _args...)      { }
#endif


#ifdef MS_DEBUG //---------------------------------------------------------------------------------
#define GE_ERR(_fmt, _args...)      printk(_fmt, ##_args)
#define GE_ASSERT(_cnd, _ret, _fmt, _args...)                   \
                                    do{if (!(_cnd)) {                \
                                        printk(_fmt, ##_args);    \
                                        return _ret;                \
                                        }                                    \
                                    }while(0);
#else //-------------------------------------------------------------------------------------------
#define GE_ERR(_fmt, _args...)      { }
#define GE_ASSERT(_cnd, _ret, _fmt, _args...)                   \
                                    do{if (!(_cnd)) {              \
                                        printk(_fmt, ##_args);  \
                                        return _ret;            \
                                        }                               \
                                    }while(0);  // CAUTION: It should be before GE_ENTRY
#endif //------------------------------------------------------------------------------------------


static MS_BOOL GE_FastClip(GE_Context *pGECtx, MS_U16 x0, MS_U16 y0, MS_U16 x1, MS_U16 y1)
{
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;
    if ( ((x0 < pGECtxLocal->u16ClipL) && (x1 < pGECtxLocal->u16ClipL)) ||
         ((x0 > pGECtxLocal->u16ClipR) && (x1 > pGECtxLocal->u16ClipR)) ||
         ((y0 < pGECtxLocal->u16ClipT) && (y1 < pGECtxLocal->u16ClipT)) ||
         ((y0 > pGECtxLocal->u16ClipB) && (y1 > pGECtxLocal->u16ClipB))    )
    {
        return TRUE; // can't be clipped
    }
    return FALSE;
}

static MS_BOOL GE_RectOverlap(GE_CTX_LOCAL *pGECtxLocal, GE_Rect *rect0, GE_DstBitBltType *rect1)
{
    if ( (pGECtxLocal->PhySrcAddr != pGECtxLocal->PhyDstAddr) ||
         (rect0->x+rect0->width-1  < rect1->dstblk.x)            ||
         (rect0->x > rect1->dstblk.x+rect1->dstblk.width-1)      ||
         (rect0->y+rect0->height-1 < rect1->dstblk.y)            ||
         (rect0->y > rect1->dstblk.y+rect1->dstblk.height-1))
    {
        return FALSE; // no overlap
    }

    return TRUE;
}

//	static GE_Result GE_Restore_PaletteContext(GE_CTX_LOCAL *pGECtxLocal)
//	{
//	    if((TRUE == pGECtxLocal->bSrcPaletteOn)/*|| (TRUE == pGECtxLocal->bDstPaletteOn)*/)
//	    { //Palette on in this KickOff, Need Restore Palette Context if need:
//	        if((pGECtxLocal->u32GESEMID != pGECtxLocal->u16GEPrevSEMID)
//	           || (TRUE == pGECtxLocal->halLocalCtx.bPaletteDirty)
//	           || ((pGECtxLocal->pSharedCtx->u32LstGEPaletteClientId != pGECtxLocal->u32GEClientId) && pGECtxLocal->pSharedCtx->u32LstGEPaletteClientId))
//	        { //Need restore palette:
//	            GE_SetPalette(pGECtxLocal);
//	            pGECtxLocal->pSharedCtx->u32LstGEPaletteClientId = pGECtxLocal->u32GEClientId;
//	            pGECtxLocal->halLocalCtx.bPaletteDirty = FALSE;
//	        }
//	    }
//
//	    return (E_GE_OK);
//	}

static MS_U16 GE_GetFmt(GE_BufFmt fmt)
{
    switch(fmt)
    {
        case E_GE_FMT_I1:
                return GE_FMT_I1;
              break;
        case E_GE_FMT_I2:
                return GE_FMT_I2;
              break;
        case E_GE_FMT_I4:
                return GE_FMT_I4;
              break;
        case E_GE_FMT_I8:
                return GE_FMT_I8;
              break;
        case E_GE_FMT_FaBaFgBg2266:
                return GE_FMT_FaBaFgBg2266;
              break;
        case E_GE_FMT_1ABFgBg12355:
                return GE_FMT_1ABFgBg12355;
              break;
        case E_GE_FMT_RGB565:
                return GE_FMT_RGB565;
              break;
        case E_GE_FMT_ARGB1555:
                return GE_FMT_ARGB1555;
              break;
        case E_GE_FMT_ARGB4444:
                return GE_FMT_ARGB4444;
              break;
        case E_GE_FMT_ARGB1555_DST:
                return GE_FMT_ARGB1555_DST;
              break;
        case E_GE_FMT_YUV422:
                return GE_FMT_YUV422;
              break;
        case E_GE_FMT_ARGB8888:
                return GE_FMT_ARGB8888;
              break;
        case E_GE_FMT_RGBA5551:
                return GE_FMT_RGBA5551;
              break;
        case E_GE_FMT_RGBA4444:
                return GE_FMT_RGBA4444;
              break;
        case E_GE_FMT_ABGR8888:
                return GE_FMT_ABGR8888;
              break;
        case E_GE_FMT_ABGR1555:
                return GE_FMT_ABGR1555;
              break;
        case E_GE_FMT_BGRA5551:
                return GE_FMT_BGRA5551;
              break;
        case E_GE_FMT_ABGR4444:
                return GE_FMT_ABGR4444;
              break;
        case E_GE_FMT_BGRA4444:
                return GE_FMT_BGRA4444;
              break;
        case E_GE_FMT_BGR565:
                return GE_FMT_BGR565;
              break;
        case E_GE_FMT_RGBA8888:
                return GE_FMT_RGBA8888;
              break;
        case E_GE_FMT_BGRA8888:
                return GE_FMT_BGRA8888;
              break;
        default:
                return GE_FMT_GENERIC;
             break;
    }
}


MS_U32 GE_Divide2Fixed(MS_U16 u16x, MS_U16 u16y, MS_U8 nInteger, MS_U8 nFraction)
{
    MS_U8  neg = 0;
    MS_U32 mask;
    MS_U32 u32x;
    MS_U32 u32y;

    if (u16x == 0)
    {
        return 0;
    }

    if (u16x & 0x8000)
    {
        u32x = 0xFFFF0000 | u16x;
        u32x = ~u32x + 1; //convert to positive
        neg++;
    }
    else
    {
        u32x = u16x;
    }

    if (u16y & 0x8000)
    {
        u32y = 0xFFFF0000 | u16y;
        u32y = ~u32y + 1; //convert to positive
        neg++;
    }
    else
    {
        u32y = u16y;
    }

    // start calculation
    u32x = (u32x << nFraction) / u32y;
    if (neg % 2)
    {
        u32x = ~u32x + 1;
    }
    // total bit number is: 1(s) + nInteger + nFraction
    mask = (1 << (1 + nInteger + nFraction)) - 1;
    u32x &= mask;

    return u32x;
}

GE_Result GE_Get_Resource(GE_Context *pGECtx)
{
    //GE_BeginDraw(pGECtx);
    return E_GE_OK;
}

GE_Result GE_Free_Resource(GE_Context *pGECtx)
{
    //GE_EndDraw(pGECtx);
    return E_GE_OK;
}

//-------------------------------------------------------------------------------------------------
/// GE DDI Initialization
/// @param  cfg                     \b IN: @ref GE_Config
/// @param  ppGECtx                     \b IN: GE context
/// @return @ref GE_Result
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_Init(void* pInstance, GE_Config *cfg, GE_Context **ppGECtx)
{
//	    MS_U32 u32regaddr = 0x0, u32regsize = 0x0;
    GE_CTX_LOCAL *pGECtxLocal = NULL;
    GE_CTX_SHARED *pGEShared;
    MS_BOOL bNeedInitShared = FALSE;

    GE_DBG("%s\n", __FUNCTION__);
#if(GFX_UTOPIA20)
        GFX_INSTANT_PRIVATE*    psGFXInstPriTmp;
        UtopiaInstanceGetPrivate(pInstance, (void**)&psGFXInstPriTmp);
        pGECtxLocal = &g_drvGECtxLocal;
        pGECtxLocal->ctxHeader.pInstance =pInstance;
#else
        pGECtxLocal = &g_drvGECtxLocal;
#endif

//	#if defined(MSOS_TYPE_LINUX) || defined(MSOS_TYPE_LINUX_KERNEL)
//	    MS_U32 u32ShmId;
//	    MS_U32 u32Addr;
//	    MS_U32 u32BufSize;
//
//	    if (FALSE == MsOS_SHM_GetId((MS_U8*)"GE driver", sizeof(GE_CTX_SHARED), &u32ShmId, &u32Addr, &u32BufSize, MSOS_SHM_QUERY))  // Richard: Query if there is IPC driver exist in share memory
//	    {
//	        if (FALSE == MsOS_SHM_GetId((MS_U8*)"GE driver", sizeof(GE_CTX_SHARED), &u32ShmId, &u32Addr, &u32BufSize, MSOS_SHM_CREATE)) // Richard: Create IPC driver exist in share memory
//	        {
//	            GE_DBG("SHM allocation failed!\n");
//	            *ppGECtx = NULL;
//	            return E_GE_FAIL;
//	        }
//	        GE_DBG("[%s][%d] This is first initial 0x%08x\n", __FUNCTION__, __LINE__, u32Addr);   // Richard: The process first calling this function should print this out
//	        memset( (MS_U8*)u32Addr, 0, sizeof(GE_CTX_SHARED));
//	        bNeedInitShared = TRUE;
//
//	    }
//	    pGEShared = (GE_CTX_SHARED*)u32Addr;
//	#else
    pGEShared =  &g_drvGECtxShared;
    bNeedInitShared = TRUE;
    memset( pGEShared, 0, sizeof(GE_CTX_SHARED));
//	#endif


//	    #ifdef __AEON__
//	    pGECtxLocal->u32GESEMID = SEM_MWMipsID;
//	    #else
//	    pGECtxLocal->u32GESEMID = SEM_HKAeonID;
//	    #endif
    pGECtxLocal->u16GEPrevSEMID = pGECtxLocal->u32GESEMID;
    pGECtxLocal->bIsComp = cfg->bIsCompt;
    pGECtxLocal->bSrcPaletteOn = FALSE;
    pGECtxLocal->bDstPaletteOn = FALSE;
#if GE_LOCK_SUPPORT
    pGECtxLocal->bGELockStatus = FALSE;
#endif

    // need add lock for protect SHM.

    pGECtxLocal->u32CTXInitMagic = 0x55aabeef;

    pGECtxLocal->pSharedCtx = pGEShared;
    GE_Init_HAL_Context(&pGECtxLocal->halLocalCtx, &pGEShared->halSharedCtx, bNeedInitShared);
    pGECtxLocal->halLocalCtx.bIsComp = pGECtxLocal->bIsComp;

    pGECtxLocal->u32GEClientId = ++pGEShared->u32GEClientAllocator;
    if(0 == pGECtxLocal->u32GEClientId)
         pGECtxLocal->u32GEClientId = ++pGEShared->u32GEClientAllocator;

    pGECtxLocal->ctxHeader.u32GE_DRV_Version =  GE_API_MUTEX;
    pGECtxLocal->ctxHeader.bGEMode4MultiProcessAccess  =  GE_SWTABLE;
    pGECtxLocal->ctxHeader.s32CurrentProcess =  (MS_S32)getpid();
    pGECtxLocal->ctxHeader.s32CurrentThread = 0;

    if (pGECtxLocal->ctxHeader.bGEMode4MultiProcessAccess ==FALSE)
        pGECtxLocal->pSharedCtx->halSharedCtx.bGE_DirectToReg =TRUE;
    else
        pGECtxLocal->pSharedCtx->halSharedCtx.bGE_DirectToReg =FALSE;
    *ppGECtx = (GE_Context *)pGECtxLocal;

//	    if(!OSAL_GE_GetMapBase(&u32regaddr, &u32regsize))
//	    {
//	        GE_DBG("Get GE IOMAP Base faill!\n");
//	        return E_GE_FAIL;
//	    }
//
//	    GE_Set_IOMap_Base(&pGECtxLocal->halLocalCtx, u32regaddr);

//	    if (!OSAL_GE_GetMapBase2(&u32regaddr, &u32regsize))
//	    {
//	        GE_DBG("Get Chip bank IOMAP Base faill!\n");
//	        return E_GE_FAIL;
//	    }
//	    GE_Set_IOMap_Base2(&pGECtxLocal->halLocalCtx, u32regaddr);

    pGECtxLocal->s32GE_Recursive_Lock_Cnt = 0;
#if GE_LOCK_SUPPORT
    pGECtxLocal->s32GELock  = -1;

    pGECtxLocal->s32GEMutex = MsOS_CreateMutex(E_MSOS_FIFO, "GE_Mutex", MSOS_PROCESS_SHARED);
    if (pGECtxLocal->s32GEMutex < 0)
    {
            GE_ERR("%s Fail\n", __FUNCTION__);
            return E_GE_FAIL;
    }

#endif

#if (ENABLE_PWR)
    MDrv_PWR_ClockTurnOn(E_PWR_MODULE_GE);
#endif


    GE_Get_Resource(*ppGECtx);
    // Enable Command Queue
    (*ppGECtx)->pBufInfo.srcfmt = ((GE_BufFmt)E_GE_FMT_ARGB1555);
    (*ppGECtx)->pBufInfo.dstfmt = ((GE_BufFmt)E_GE_FMT_ARGB1555);
    GE_Init(&pGECtxLocal->halLocalCtx, cfg);
    //Init GE Palette from HW:
//	    GE_InitCtxHalPalette(&pGECtxLocal->halLocalCtx);

//	    if (bNeedSetActiveCtrlMiu1)
//	        GE_SetActiveCtrlMiu1(&pGECtxLocal->halLocalCtx);
    // Default setting : To avoid GE issues commands too frequently when VC in enabled
//	    MDrv_GE_SetVCmd_W_Thread(*ppGECtx, 0x4);
//	    MDrv_GE_SetVCmd_R_Thread(*ppGECtx, 0x4);

    pGECtxLocal->pSharedCtx->bNotFirstInit = TRUE;

    GE_Free_Resource(*ppGECtx);
    return E_GE_OK;
}

//-------------------------------------------------------------------------------------------------
/// Set GE dither
/// @param  pGECtx                     \b IN: GE context
/// @param  enable                  \b IN: enable and update setting
/// @return @ref GE_Result
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetDither(GE_Context *pGECtx, MS_BOOL enable)
{
    MS_U16              u16en;
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    u16en = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_EN);
    if (enable)
    {
        u16en |= GE_EN_DITHER;
    }
    else
    {
        u16en &= ~GE_EN_DITHER;
    }
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_EN, u16en);

    GE_RETURN(pGECtx,E_GE_OK);
}

//-------------------------------------------------------------------------------------------------
/// GE Primitive Drawing - BLT
/// @param  pGECtx                     \b IN: GE context
/// @param  src                     \b IN: source RECT of bitblt
/// @param  dst                     \b IN: destination RECT of bitblt
/// @param  flags                   \b IN: RECT drawing flags
/// @param  scaleinfo                     \b IN: Customed scale info
/// @return @ref GE_Result
/// @note\n
/// FMT_I1,I2,I4,I8: should be 64-bit aligned in base and pitch to do stretch bitblt.\n
/// FMT_YUV422: the rectangle should be YUV422 block aligned. (4 bytes for 2 pixels in horizontal)
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_BitBltEX(GE_Context *pGECtx, GE_Rect *src, GE_DstBitBltType *dst, U32 flags, GE_ScaleInfo* scaleinfo)
{
    U16              u16cmd = 0;
    U16              u16cfg = 0;
    U16              u16en = 0;
    U16              temp;
    BOOL             bOverlap = FALSE;
    BOOL             bNonOnePixelMode = FALSE;
    GE_Point            v0, v1, v2;
    U8               u8Rot;
    PatchBitBltInfo     patchBitBltInfo;
    BOOL bDstXInv = FALSE;
    U16              u16tmp,Srcfmt,Dstfmt;
    U16              u16tmp_dy;
    U16               srcMiu, dstMiu;
    U16               u16enGeAlphaBlending;

    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;
#ifdef GE_ALPHA_BLEND_PATCH
    MS_BOOL             bDisAlpha = FALSE;
#endif
    GE_DBG("%s\n", __FUNCTION__);
    GE_ASSERT((src), E_GE_FAIL_PARAM, "%s: Null pointer\n", __FUNCTION__);
    GE_ASSERT((dst), E_GE_FAIL_PARAM, "%s: Null pointer\n", __FUNCTION__);

//	    GE_ENTRY(pGECtx);

    if ( (src->width == 0) || (src->height == 0) || (dst->dstblk.width == 0) || (dst->dstblk.height == 0) )
    {
        GE_RETURN(pGECtx, E_GE_FAIL_PARAM);
    }
    if ( GE_FastClip(pGECtx,dst->dstblk.x, dst->dstblk.y, dst->dstblk.x+dst->dstblk.width-1, dst->dstblk.y+dst->dstblk.height-1) )
    {
        if (flags & (
                      E_GE_FLAG_BLT_ITALIC      |
                      E_GE_FLAG_BLT_ROTATE_90   |
                      E_GE_FLAG_BLT_ROTATE_180  |
                      E_GE_FLAG_BLT_ROTATE_270  ))
        {
            // SKIP fast clipping

            //[TODO] Do fast clipping precisely for rotation and italic
        }
        else
        {
            GE_RETURN(pGECtx,E_GE_NOT_DRAW);
        }
    }

    //[TODO] check stretch bitblt base address and pitch alignment if we can prevent register
    // being READ in queue prolbme (ex. wait_idle or shadow_reg)

    // Check overlap
    if (flags & E_GE_FLAG_BLT_OVERLAP)
    {
        if (flags & (
                      E_GE_FLAG_BLT_STRETCH     |
                      E_GE_FLAG_BLT_ITALIC      |
                      E_GE_FLAG_BLT_MIRROR_H    |
                      E_GE_FLAG_BLT_MIRROR_V    |
                      E_GE_FLAG_BLT_ROTATE_90   |
                      E_GE_FLAG_BLT_ROTATE_180  |
                      E_GE_FLAG_BLT_ROTATE_270  ))
        {
            GE_RETURN(pGECtx,E_GE_FAIL_OVERLAP);
        }

        //[TODO] Cechk overlap precisely
        bOverlap = GE_RectOverlap(pGECtxLocal, src, dst);
    }

    // Check italic
    if (flags & E_GE_FLAG_BLT_ITALIC)
    {
        if (flags & (
                      E_GE_FLAG_BLT_MIRROR_H    |
                      E_GE_FLAG_BLT_MIRROR_V    |
                      E_GE_FLAG_BLT_ROTATE_90   |
                      E_GE_FLAG_BLT_ROTATE_180  |
                      E_GE_FLAG_BLT_ROTATE_270  ))
        {
            GE_RETURN(pGECtx,E_GE_FAIL_ITALIC);
        }
    }

    // clear dirty attributes ---------------------------------------------------------------------
    GE_SetRotate(&pGECtxLocal->halLocalCtx, E_GE_ROTATE_0);
    u16cfg = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_CFG) & ~(GE_CFG_BLT_STRETCH | GE_CFG_BLT_ITALIC);
    // clear dirty attributes ---------------------------------------------------------------------


    if(flags & E_GE_FLAG_BYPASS_STBCOEF)
    {
        if(scaleinfo == NULL)
        {
            GE_RETURN(pGECtx,E_GE_FAIL_PARAM);
        }

        u16cfg |= GE_CFG_BLT_STRETCH;
        if (flags & E_GE_FLAG_STRETCH_NEAREST)
        {
            u16cmd |= GE_STRETCH_NEAREST;
        }
        u16cmd |= GE_STRETCH_CLAMP;

    }
    else if (flags & E_GE_FLAG_BLT_STRETCH)
    {

        u16cfg |= GE_CFG_BLT_STRETCH;
        if (flags & E_GE_FLAG_STRETCH_NEAREST)
        {
            u16cmd |= GE_STRETCH_NEAREST;
        }
        u16cmd |= GE_STRETCH_CLAMP;

    }
    else
    {
        if ( (src->width != dst->dstblk.width) || (src->height != dst->dstblk.height) )
        {
            GE_RETURN(pGECtx,E_GE_FAIL_STRETCH);
        }

    }

    if( GE_SetBltScaleRatio(&pGECtxLocal->halLocalCtx, src, dst, (GE_Flag)flags, scaleinfo) == E_GE_FAIL_PARAM)
        GE_RETURN(pGECtx,E_GE_FAIL_PARAM);

    u16tmp = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_FMT);
    Srcfmt = (u16tmp&GE_SRC_FMT_MASK)>>GE_SRC_FMT_SHFT;
    Dstfmt = (u16tmp&GE_DST_FMT_MASK)>>GE_DST_FMT_SHFT;
    if((GE_FMT_I8 == Srcfmt) && (GE_FMT_I8 != Dstfmt) )
    {
        pGECtxLocal->bSrcPaletteOn = TRUE;
    }
    else
    {
        pGECtxLocal->bSrcPaletteOn = FALSE;
    }
//	    GE_Restore_PaletteContext(pGECtxLocal);

    v0.x = dst->dstblk.x;
    v0.y = dst->dstblk.y;
    v1.x = v0.x + dst->dstblk.width - 1;
    v1.y = v0.y + dst->dstblk.height - 1;
    v2.x = src->x;
    v2.y = src->y;


    if (bOverlap)
    {
        GE_DBG("%s Overlap\n", __FUNCTION__);

        if (v2.x < v0.x)
        {
            // right to left
            temp = v0.x;
            v0.x = v1.x;
            v1.x = temp;
            v2.x += src->width - 1;
            u16cmd |= GE_SRC_DIR_X_INV | GE_DST_DIR_X_INV;
        }
        if (v2.y < v0.y)
        {
            // bottom up
            temp = v0.y;
            v0.y = v1.y;
            v1.y = temp;
            v2.y += src->height - 1;
            u16cmd |= GE_SRC_DIR_Y_INV | GE_DST_DIR_Y_INV;
        }
    }

    if (flags & E_GE_FLAG_BLT_MIRROR_H)
    {
#if 0
        temp = v0.x;
        v0.x = v1.x;
        v1.x = temp;
#endif
        //v2.x += src->width -1;// show repect for history, comment it
        u16cmd |= GE_SRC_DIR_X_INV;
    }
    if (flags & E_GE_FLAG_BLT_MIRROR_V)
    {
#if 0
        temp = v0.y;
        v0.y = v1.y;
        v1.y = temp;
#endif
        //v2.y += src->height -1;// show repect for history, comment it
        u16cmd |= GE_SRC_DIR_Y_INV;
    }

    if (flags & E_GE_FLAG_BLT_DST_MIRROR_H)
    {
        temp = v0.x;
        v0.x = v1.x;
        v1.x = temp;
        u16cmd |= GE_DST_DIR_X_INV;
    }
    if (flags & E_GE_FLAG_BLT_DST_MIRROR_V)
    {
        temp = v0.y;
        v0.y = v1.y;
        v1.y = temp;
        u16cmd |= GE_DST_DIR_Y_INV;
    }

    if (flags & E_GE_FLAG_BLT_ITALIC)
    {
        u16cfg |= GE_CFG_BLT_ITALIC;
    }
    if (flags & GE_FLAG_BLT_ROTATE_MASK) // deal with ALL rotation mode
    {
        GE_SetRotate(&pGECtxLocal->halLocalCtx, (GE_RotateAngle)((flags & E_GE_FLAG_BLT_ROTATE_270)>>GE_FLAG_BLT_ROTATE_SHFT));
    }

    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_X, v0.x);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_Y, v0.y);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_X, v1.x);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_Y, v1.y);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V2_X, v2.x);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V2_Y, v2.y);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_BLT_SRC_W, src->width);

    temp = src->height;

    if (pGECtxLocal->halLocalCtx.bYScalingPatch)
    {
        if (temp>5)
        {
            temp-=5;
        }
    }

    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_BLT_SRC_H, temp);

    u8Rot = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_ROT_MODE ) & REG_GE_ROT_MODE_MASK;

    // Set dst coordinate
    if (u8Rot == 0)
    {
        if (!bOverlap)
        {
            if(!(flags & E_GE_FLAG_BLT_DST_MIRROR_H))
            {
            GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_X, v0.x);
                GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_X, v0.x + dst->dstblk.width - 1);
            }
            if(!(flags & E_GE_FLAG_BLT_DST_MIRROR_V))
            {
            GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_Y, v0.y);
            GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_Y, v0.y + dst->dstblk.height- 1);
        }
    }
    }
    else if (u8Rot == 1)
    {
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_X, v0.x + dst->dstblk.height - 1);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_Y, v0.y);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_X, v0.x + dst->dstblk.height + dst->dstblk.width - 2);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_Y, v0.y + dst->dstblk.height- 1);
    }
    else if (u8Rot == 2)
    {
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_X, v0.x + dst->dstblk.width - 1);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_Y, v0.y + dst->dstblk.height - 1);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_X, v0.x + dst->dstblk.width + dst->dstblk.width - 2);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_Y, v0.y + dst->dstblk.height + dst->dstblk.height - 2);
    }
    else if (u8Rot == 3)
    {
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_X, v0.x);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V0_Y, v0.y + dst->dstblk.width - 1);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_X, v0.x + dst->dstblk.width - 1);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_PRIM_V1_Y, v0.y + dst->dstblk.height + dst->dstblk.width - 2);
    }

    u16cfg |= GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_CFG) & GE_CFG_CMDQ_MASK;

    srcMiu = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_SRC_BASE_H) & (GE_SB_MIU_SEL);
    dstMiu = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_DST_BASE_H) & (GE_DB_MIU_SEL);
    u16enGeAlphaBlending = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_EN) & (GE_EN_BLEND);
    if ((u16enGeAlphaBlending != 0) && (srcMiu != dstMiu))
    {
        // mulan bug : different MIU bitblt with alpha blending need disable r/w split mode
        u16cfg &= (~GE_CFG_RW_SPLIT);
    }
    else if (*(&pGECtxLocal->halLocalCtx.pGeChipPro->bSupportSpiltMode))
    {
        // enable split mode
        u16cfg |= GE_CFG_RW_SPLIT;
    }

    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_CFG, u16cfg);

    // To check if 1p mode set to TRUE and Non-1p mode limitations
    u16en = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_EN);

    patchBitBltInfo.flags = flags;
    patchBitBltInfo.src.width = src->width;
    patchBitBltInfo.src.height= src->height;
    patchBitBltInfo.dst.dstblk.width =  dst->dstblk.width;
    patchBitBltInfo.dst.dstblk.height=  dst->dstblk.height;
    patchBitBltInfo.scaleinfo = scaleinfo;
    bNonOnePixelMode = GE_NonOnePixelModeCaps(&pGECtxLocal->halLocalCtx, &patchBitBltInfo);

    if(bNonOnePixelMode && (u16en & GE_EN_ONE_PIXEL_MODE))
        GE_SetOnePixelMode(&pGECtxLocal->halLocalCtx, FALSE);
    else if((!bNonOnePixelMode) && (!(u16en & GE_EN_ONE_PIXEL_MODE)))
        GE_SetOnePixelMode(&pGECtxLocal->halLocalCtx, TRUE);

#ifdef GE_ALPHA_BLEND_PATCH
    if( (u16GAlphaMode == 0x0) && (u16GAlphaConst == 0x0) )
    {
        GE_SetAlpha(&pGECtxLocal->halLocalCtx,E_GE_ALPHA_ROP8_INV_CONST);
        MDrv_GE_SetAlphaConst(pGECtx,0xFF);
        bDisAlpha = TRUE;
    }
#endif
    u16tmp = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_BLT_SRC_DX);
    u16tmp_dy = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_BLT_SRC_DY);
    if(((flags&E_GE_FLAG_BLT_STRETCH)&&((u16tmp!=0x1000)||(u16tmp_dy!=0x1000)))||(u8Rot!=0))
    {
        HAL_GE_EnableCalcSrc_WidthHeight(&pGECtxLocal->halLocalCtx, FALSE);
    }
    else
    {
        HAL_GE_EnableCalcSrc_WidthHeight(&pGECtxLocal->halLocalCtx, TRUE);
    }
    bDstXInv = (u16cmd & (GE_DST_DIR_X_INV))?TRUE:FALSE;
    if(!u8Rot)
    {
    HAL_GE_AdjustDstWin(&pGECtxLocal->halLocalCtx,bDstXInv);
    }
    else
    {
        HAL_GE_AdjustRotateDstWin(&pGECtxLocal->halLocalCtx,u8Rot);
    }

    //if srcfmt == dstfmt, dont use Dither
    u16tmp = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_FMT);
    Srcfmt = (u16tmp&GE_SRC_FMT_MASK)>>GE_SRC_FMT_SHFT;
    Dstfmt = (u16tmp&GE_DST_FMT_MASK)>>GE_DST_FMT_SHFT;
    if( (Srcfmt == Dstfmt)
      ||((Srcfmt == E_MS_FMT_ARGB1555)&&(Dstfmt == E_MS_FMT_ARGB1555_DST))
      ||((Srcfmt == E_MS_FMT_ARGB1555_DST)&&(Dstfmt == E_MS_FMT_ARGB1555))
      )
    {
       MDrv_GE_SetDither(pGECtx, FALSE);
    }

#ifdef GE_VQ_MIU_HANG_PATCH
    if ((GE_IsVcmdqEnabled(pGECtx) == TRUE) &&
        (u8DstMIU != pGECtxLocal->halLocalCtx.pHALShared->u8VCmdQMiu)
        )
    {
        //printf("\33[0;36m   %s:%d  u8VCmdQMiu = %d  u8SrcMIU = %d u8DstMIU = %d\33[m \n",__FUNCTION__,__LINE__,pGECtxLocal->halLocalCtx.pHALShared->u8VCmdQMiu,u8SrcMIU,u8DstMIU);
        MDrv_GE_WaitIdle(pGECtx);
    }
#endif

//	    _GE_SEMAPHORE_ENTRY(pGECtx,E_GE_POOL_ID_INTERNAL_REGISTER);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_CMD, GE_PRIM_BITBLT | u16cmd);
//	    _GE_SEMAPHORE_RETURN(pGECtx,E_GE_POOL_ID_INTERNAL_REGISTER);

#ifdef GE_VQ_MIU_HANG_PATCH
    if ((GE_IsVcmdqEnabled(pGECtx) == TRUE) &&
        (u8DstMIU != pGECtxLocal->halLocalCtx.pHALShared->u8VCmdQMiu)
        )
    {
        //printf("\33[0;36m   %s:%d  u8VCmdQMiu = %d  u8SrcMIU = %d u8DstMIU = %d\33[m \n",__FUNCTION__,__LINE__,pGECtxLocal->halLocalCtx.pHALShared->u8VCmdQMiu,u8SrcMIU,u8DstMIU);
        MDrv_GE_WaitIdle(pGECtx);
    }
#endif


    if( u16en & GE_EN_ONE_PIXEL_MODE)
        GE_SetOnePixelMode(&pGECtxLocal->halLocalCtx, TRUE);
    else if(!(u16en & GE_EN_ONE_PIXEL_MODE))
        GE_SetOnePixelMode(&pGECtxLocal->halLocalCtx, FALSE);

    GE_RETURN(pGECtx,E_GE_OK);
}

GE_Result MDrv_GE_Chip_Proprity_Init(GE_Context *pGECtx, GE_CHIP_PROPERTY **ppGeChipPro)
{
    if ( pGECtx == NULL )
    {
        GE_DBG("\n%s, %d, 1st param. can't be null pointer\n", __FUNCTION__, __LINE__);
        return E_GE_FAIL;
    }
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    GE_Chip_Proprity_Init(&pGECtxLocal->halLocalCtx);
    *ppGeChipPro = pGECtxLocal->halLocalCtx.pGeChipPro;

    GE_RETURN(pGECtx, E_GE_OK);
}

GE_Result MDrv_GE_SetOnePixelMode(GE_Context *pGECtx, MS_BOOL enable)
{

    if ( pGECtx == NULL )
    {
        GE_DBG("\n%s, %d, 1st param. can't be null pointer\n", __FUNCTION__, __LINE__);
        return E_GE_FAIL;
    }

    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;
    GE_Result geResult;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    geResult = GE_SetOnePixelMode(&pGECtxLocal->halLocalCtx, enable);

    GE_RETURN(pGECtx, geResult);
}

//-------------------------------------------------------------------------------------------------
/// Get Next GE TagID
/// Get the TAGID in bStepTagBefore steps.
/// @param  pGECtx                     \b IN: GE context
/// @param  bStepTagBefore         \b IN: Steps to the NEXT TAGID.
/// @return tagID
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_GetNextTAGID(GE_Context *pGECtx, MS_BOOL bStepTagBefore, MS_U16* u16NextTagID)
{
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_ENTRY(pGECtx);

    *u16NextTagID = GE_GetNextTAGID(&pGECtxLocal->halLocalCtx, bStepTagBefore);

    GE_RETURN(pGECtx,E_GE_OK);
}
//-------------------------------------------------------------------------------------------------
/// Set next TagID Auto to HW
/// Get the Next Tag ID auto and set it to HW.
/// @param  pGECtx                     \b IN: GE context
/// @param  tagID                      \b OUT: The Tag ID which has been sent to hw
/// @return @ref GE_Result
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetNextTAGID(GE_Context *pGECtx, MS_U16 *tagID, MS_BOOL bHighByte)
{
     GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;
     GE_ENTRY(pGECtx);
//	     MS_U8 i;

     *tagID = GE_GetNextTAGID(&pGECtxLocal->halLocalCtx, TRUE);

     {
         GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_TAG, *tagID);
         GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_INT_TAG_H, *tagID);
     }

     GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_TAG_L, bHighByte);
     GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_INT_TAG_L, bHighByte);
     GE_RETURN(pGECtx, E_GE_OK);
}

GE_Result MDrv_GE_GetCurTAGID(GE_Context *pGECtx, MS_U16 *tagID, MS_BOOL * bHighByte)
{
     GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;
     GE_ENTRY(pGECtx);

     *tagID = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_TAG);
     *bHighByte = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_TAG_L);

     GE_RETURN(pGECtx, E_GE_OK);
}
//-------------------------------------------------------------------------------------------------
/// Wait GE TagID back
/// @param pGECtx                      \b IN:  Pointer to GE context.
/// @param  tagID                     \b IN: tag id number for wating
/// @return @ref GE_Result
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_WaitTAGID(GE_Context *pGECtx, MS_U16 tagID, MS_BOOL bWaitForever, MS_BOOL * bwaitSuccess)
{
     GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

     GE_ENTRY(pGECtx);

     GE_WaitTAGID(&pGECtxLocal->halLocalCtx, tagID, bWaitForever, bwaitSuccess);

     GE_RETURN(pGECtx,E_GE_OK);
}

//-------------------------------------------------------------------------------------------------
/// Set GE source buffer info
/// @param  pGECtx                     \b IN: GE context
/// @param  src_fmt                 \b IN: source buffer format
/// @param  pix_width               \b IN: pixel width of source buffer (RESERVED)
/// @param  pix_height              \b IN: pixel height of source buffer (RESERVED)
/// @param  addr                    \b IN: source buffer start address [23:0]
/// @param  pitch                   \b IN: source buffer linear pitch in byte unit
/// @param  flags                   \b IN: option flag of dst buffer <b>(RESERVED)</b>
/// @return @ref GE_Result
/// @attention
/// <b>[URANUS] <em>E_GE_FMT_ARGB1555 is RGB555</em></b>
/// <b>[URANUS] <em>E_GE_FMT_* should be 8-byte addr/pitch aligned to do stretch bitblt</em></b>
/// @sa MDrv_GE_GetFmtCaps
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetSrcBuffer(GE_Context *pGECtx, GE_BufFmt src_fmt, MS_U16 pix_width, MS_U16 pix_height, MS_PHYADDR addr, MS_U16 pitch, MS_U32 flags)
{
    GE_Result           ret;
    GE_FmtCaps          caps;
    MS_U16              u16fmt,Srcfmt,Dstfmt;
    MS_U8 miu=0;
    MS_U8 tlb_miu=0;
    MS_U16 reg_val=0;
    MS_U32 u32offset=0;
    MS_U32 tlb_start_entry=0;
    MS_U32 miu_interval=0;
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    // pix_width, pix_height: reserved for future use.
    src_fmt = (GE_BufFmt) (0xFF & src_fmt);
    ret = GE_GetFmtCaps(&pGECtxLocal->halLocalCtx, src_fmt, E_GE_BUF_SRC, &caps);
    if (ret != E_GE_OK)
    {
        GE_ERR("%s: format fail\n", __FUNCTION__);
        GE_RETURN(pGECtx,ret);
    }
    if (addr != ((addr + (caps.u8BaseAlign-1)) & ~(caps.u8BaseAlign-1)))
    {
        GE_ERR("%s: address fail\n", __FUNCTION__);
        GE_RETURN(pGECtx,E_GE_FAIL_ADDR);
    }
    if (pitch != ((pitch + (caps.u8PitchAlign-1)) & ~(caps.u8PitchAlign-1)))
    {
        GE_ERR("%s: pitch fail\n", __FUNCTION__);
        GE_RETURN(pGECtx,E_GE_FAIL_PITCH);
    }
    /*
    if (flags & E_GE_FLAG_BUF_TILE)
    {
    }
    */

    #if 0
    if(E_GE_FMT_I8 == src_fmt)
    {
        pGECtxLocal->bSrcPaletteOn = TRUE;
    }
    else
    {
        pGECtxLocal->bSrcPaletteOn = FALSE;
    }
    #endif
    u16fmt = (GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_FMT) & ~GE_SRC_FMT_MASK) | ( GE_GetFmt(src_fmt) << GE_SRC_FMT_SHFT);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_FMT, u16fmt);

#ifdef GE_VQ_MIU_HANG_PATCH
    u8DstMIU = _GFXAPI_MIU_ID(addr);
#endif

    // Set source address
    GE_SetSrcBufMIUId(&pGECtxLocal->halLocalCtx, _GFXAPI_MIU_ID(addr));

    pGECtxLocal->PhySrcAddr = _GFXAPI_PHYS_ADDR_IN_MIU(addr) | (addr & HAL_MIU1_BASE);
//	#if !defined (MSOS_TYPE_NOS)
//	    if(pGECtxLocal->halLocalCtx.pGeChipPro->bSupportTLBMode &&(pGECtx->pBufInfo.tlbmode==E_GE_TLB_SRC||pGECtx->pBufInfo.tlbmode==E_GE_TLB_SRC_DST))
//	    {
//	        tlb_miu= _GFXAPI_MIU_ID(pGECtx->pBufInfo.tlbsrcaddr);
//	        if( tlb_miu > (pGECtxLocal->halLocalCtx.pGeChipPro->MIUSupportMaxNUM-1))
//	        {
//	            printf("[%s] TLB Entry Source MIU invalid (miu=%d)\n",__FUNCTION__,tlb_miu);
//	            GE_RETURN(pGECtx,E_GE_FAIL_ADDR);
//	        }
//	        if(!pGECtx->pBufInfo.tlbsrcaddr)
//	        {
//	            printf("[%s] TLB Entry Source addrees invalid(addr=0x%lx)\n",__FUNCTION__,pGECtx->pBufInfo.tlbsrcaddr);
//	            GE_RETURN(pGECtx,E_GE_FAIL_ADDR);
//	        }
//	        u32offset=_GFXAPI_PHYS_ADDR_IN_MIU(pGECtx->pBufInfo.tlbsrcaddr);
//	        if(pTLBSRCStartVA ==NULL)
//	        {
//	            if(MsOS_MPool_Mapping(tlb_miu, u32offset, TLB_ENRTY_SIZE, 1) == false)
//	            {
//	                printf("[%s] MsOS_MPool_Mapping fail\n",__FUNCTION__);
//	                return false;
//	            }
//	            GE_Get_MIU_INTERVAL(&pGECtxLocal->halLocalCtx,tlb_miu,&miu_interval);
//	            u32offset += miu_interval;
//	            pTLBSRCStartVA = (MS_U32*)MsOS_MPool_PA2KSEG1(u32offset);
//	            //tlb_start_entry = *(pTLBSRCStartVA);
//	        }
//	        //[31:26]valid tag; [25:21]reserved; [20:19]MIU selection; [18:0]physical address
//	        tlb_start_entry = *( pTLBSRCStartVA + ((addr/PAGE_SIZE)*TLB_PER_ENTRY_SIZE)/sizeof(MS_U32) );
//	        addr = GE_ConvertAPIAddr2HAL(&pGECtxLocal->halLocalCtx, ((tlb_start_entry&0x001f0000)>>19), _GFXAPI_PHYS_ADDR_IN_MIU(addr));
//	        miu=((tlb_start_entry&0x001f0000)>>19);
//	    }
//	    else
//	#endif
    {
        miu = _GFXAPI_MIU_ID(addr);
        addr = GE_ConvertAPIAddr2HAL(&pGECtxLocal->halLocalCtx, miu, _GFXAPI_PHYS_ADDR_IN_MIU(addr));
    }

    if(miu>=2)
    {
        reg_val = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_YUV_MODE) | (1<<GE_SRC_BUFFER_MIU_H_SHFT);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_YUV_MODE, reg_val);
    }
    else
    {
        reg_val = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_YUV_MODE) & (~(1<<GE_SRC_BUFFER_MIU_H_SHFT));
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_YUV_MODE, reg_val);
    }

    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_SRC_BASE_L, (addr & 0xFFFF));
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_SRC_BASE_H, (addr >> 16));

    // Set source pitch
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_SRC_PITCH, pitch);

//	    // Set buffer tile mode
//	    if (flags & E_GE_FLAG_BUF_TILE)
//	    {
//	        // if support tile mode
//	        ret = GE_SetSrcTile(&pGECtxLocal->halLocalCtx, TRUE);
//	        GE_RETURN(pGECtx,ret);
//	    }
//	    GE_SetSrcTile(&pGECtxLocal->halLocalCtx, FALSE);

    GE_RETURN(pGECtx,E_GE_OK);
}

//-------------------------------------------------------------------------------------------------
/// Set GE destination buffer info
/// @param  pGECtx                     \b IN: GE context
/// @param  dst_fmt                 \b IN: source buffer format
/// @param  pix_width               \b IN: pixel width of destination buffer (RESERVED)
/// @param  pix_height              \b IN: pixel height of destination buffer (RESERVED)
/// @param  addr                    \b IN: destination buffer start address [23:0]
/// @param  pitch                   \b IN: destination buffer linear pitch in byte unit
/// @param  flags                   \b IN: option flag of dst buffer <b>(RESERVED)</b>
/// @return @ref GE_Result
/// @attention
/// <b>[URANUS] <em>E_GE_FMT_ARGB1555 is RGB555, E_GE_FMT_YUV422 can not be destination.</em></b>
/// <b>[URANUS] <em>E_GE_FMT_* should be 8-byte addr/pitch aligned to do stretch bitblt</em></b>
/// @sa @ref MDrv_GE_GetFmtCaps
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetDstBuffer(GE_Context *pGECtx, GE_BufFmt dst_fmt, MS_U16 pix_width, MS_U16 pix_height, MS_PHYADDR addr, MS_U16 pitch, MS_U32 flags)
{
    GE_Result           ret;
    GE_FmtCaps          caps;
    MS_U16              u16fmt,Srcfmt,Dstfmt;
    MS_U8 miu=0;
    MS_U8 tlb_miu=0;
    MS_U16 reg_val=0;
    MS_U32 u32offset=0;
    MS_U32 tlb_start_entry=0;
    MS_U32 miu_interval=0;
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    // pix_width, pix_height: reserved for future use.
    dst_fmt = (GE_BufFmt) (0xFF & dst_fmt);

    ret = GE_GetFmtCaps(&pGECtxLocal->halLocalCtx, dst_fmt, E_GE_BUF_DST, &caps);
    if (ret != E_GE_OK)
    {
        GE_ERR("%s: format fail\n", __FUNCTION__);
        GE_RETURN(pGECtx,ret);
    }
    if (addr != ((addr + (caps.u8BaseAlign-1)) & ~(caps.u8BaseAlign-1)))
    {
        GE_ERR("%s: address fail\n", __FUNCTION__);
        GE_RETURN(pGECtx,E_GE_FAIL_ADDR);
    }
    if (pitch != ((pitch + (caps.u8PitchAlign-1)) & ~(caps.u8PitchAlign-1)))
    {
        GE_ERR("%s: pitch fail\n", __FUNCTION__);
        GE_RETURN(pGECtx,E_GE_FAIL_PITCH);
    }

#if 0
    if(E_GE_FMT_I8 == dst_fmt)
    {
        pGECtxLocal->bDstPaletteOn = TRUE;
    }
    else
    {
        pGECtxLocal->bDstPaletteOn = FALSE;
    }
#endif
    u16fmt = (GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_FMT) & ~GE_DST_FMT_MASK) | ( GE_GetFmt(dst_fmt) << GE_DST_FMT_SHFT);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_FMT, u16fmt);

    // Set destination address
    GE_SetDstBufMIUId(&pGECtxLocal->halLocalCtx, _GFXAPI_MIU_ID(addr));

    pGECtxLocal->PhyDstAddr = _GFXAPI_PHYS_ADDR_IN_MIU(addr) | (addr & HAL_MIU1_BASE);
//	#if !defined (MSOS_TYPE_NOS)
//	    if(pGECtxLocal->halLocalCtx.pGeChipPro->bSupportTLBMode &&(pGECtx->pBufInfo.tlbmode==E_GE_TLB_DST||pGECtx->pBufInfo.tlbmode==E_GE_TLB_SRC_DST))
//	    {
//	        tlb_miu= _GFXAPI_MIU_ID(pGECtx->pBufInfo.tlbdstaddr);
//	        if(tlb_miu > (pGECtxLocal->halLocalCtx.pGeChipPro->MIUSupportMaxNUM-1))
//	        {
//	            printf("[%s] TLB Entry destination MIU addrees invalid(miu=%d)\n",__FUNCTION__,tlb_miu);
//	            GE_RETURN(pGECtx,E_GE_FAIL_ADDR);
//	        }
//	        if(!pGECtx->pBufInfo.tlbdstaddr)
//	        {
//	            printf("[%s] TLB Entry destination addrees invalid(addr=0x%lx)\n",__FUNCTION__,pGECtx->pBufInfo.tlbdstaddr);
//	            GE_RETURN(pGECtx,E_GE_FAIL_ADDR);
//	        }
//	        u32offset=_GFXAPI_PHYS_ADDR_IN_MIU(pGECtx->pBufInfo.tlbdstaddr);
//	        if(pTLBDSTStartVA ==NULL)
//	        {
//	            if(MsOS_MPool_Mapping(tlb_miu, u32offset, TLB_ENRTY_SIZE, 1) == false)
//	            {
//	                printf("[%s] MsOS_MPool_Mapping fail\n",__FUNCTION__);
//	                return false;
//	            }
//	            GE_Get_MIU_INTERVAL(&pGECtxLocal->halLocalCtx,tlb_miu,&miu_interval);
//	            u32offset += miu_interval;
//	            pTLBDSTStartVA = (MS_U32*)MsOS_MPool_PA2KSEG1(u32offset);
//	            //tlb_start_entry = *(pTLBDSTStartVA);
//	        }
//	        //[31:26]valid tag; [25:21]reserved; [20:19]MIU selection; [18:0]physical address
//	        tlb_start_entry = *( pTLBDSTStartVA + ((addr/PAGE_SIZE)*TLB_PER_ENTRY_SIZE)/sizeof(MS_U32) );
//	        addr = GE_ConvertAPIAddr2HAL(&pGECtxLocal->halLocalCtx, ((tlb_start_entry&0x001f0000)>>19), _GFXAPI_PHYS_ADDR_IN_MIU(addr));
//	        miu=((tlb_start_entry&0x001f0000)>>19);
//	    }
//	    else
//	#endif
    {
        miu = _GFXAPI_MIU_ID(addr);
        addr = GE_ConvertAPIAddr2HAL(&pGECtxLocal->halLocalCtx, miu, _GFXAPI_PHYS_ADDR_IN_MIU(addr));
    }

    if(miu>=2)
    {
        reg_val = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_YUV_MODE) | (1<<GE_DST_BUFFER_MIU_H_SHFT);
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_YUV_MODE, reg_val);
    }
    else
    {
        reg_val = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_YUV_MODE) & (~(1<<GE_DST_BUFFER_MIU_H_SHFT));
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_YUV_MODE, reg_val);
    }
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_DST_BASE_L, (addr & 0xFFFF));
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_DST_BASE_H, (addr >> 16));

    // Set destination pitch
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_DST_PITCH, pitch);

//	    // Set buffer tile mode
//	    if (flags & E_GE_FLAG_BUF_TILE)
//	    {
//	        // if support tile mode
//	        ret = GE_SetDstTile(&pGECtxLocal->halLocalCtx, TRUE);
//	        GE_RETURN(pGECtx,ret);
//	    }
//	    GE_SetDstTile(&pGECtxLocal->halLocalCtx, FALSE);

    GE_RETURN(pGECtx,E_GE_OK);
}
//-------------------------------------------------------------------------------------------------
/// Set GE clipping window
/// @param  pGECtx                     \b IN: GE context
/// @param rect                     \b IN: pointer to RECT of clipping window
/// @return @ref GE_Result
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetClipWindow(GE_Context *pGECtx, GE_Rect *rect)
{
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ASSERT((rect), E_GE_FAIL_PARAM, "%s: Null pointer\n", __FUNCTION__);

    GE_ENTRY(pGECtx);

    pGECtxLocal->u16ClipL = rect->x;
    pGECtxLocal->u16ClipT = rect->y;
    pGECtxLocal->u16ClipR = (rect->x + rect->width - 1);
    pGECtxLocal->u16ClipB = (rect->y + rect->height - 1);

    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_CLIP_L, pGECtxLocal->u16ClipL);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_CLIP_T, pGECtxLocal->u16ClipT);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_CLIP_R, pGECtxLocal->u16ClipR);
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_CLIP_B, pGECtxLocal->u16ClipB);

    GE_RETURN(pGECtx,E_GE_OK);
}

//-------------------------------------------------------------------------------------------------
/// Set GE alpha source for alpha output. <b><em>Aout</em></b>
/// @param  pGECtx                     \b IN: GE context
/// @param  eAlphaSrc               \b IN: alpha channel comes from
/// @return @ref GE_Result
/// @note
/// <b>(REQUIREMENT) <em>E_GE_ALPHA_ADST requires AlphaBlending(TRUE, )</em></b>
/// @attention
/// <b>[URANUS] <em>It does not support ROP8 operation</em></b>
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetAlphaSrc(GE_Context *pGECtx, GE_AlphaSrc eAlphaSrc)
{
    GE_Result           ret;
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    ret = GE_SetAlpha(&pGECtxLocal->halLocalCtx, eAlphaSrc);

    GE_RETURN(pGECtx,ret);
}
//-------------------------------------------------------------------------------------------------
/// Set GE alpha blending of color output. <b><em>Cblend = (coef * Csrc) OP ((1-coef) * Cdst)</em></b>
/// @param  pGECtx                     \b IN: GE context
/// @param  enable                  \b IN: enable and update setting
/// @param  eBlendOp                \b IN: coef and op for blending
/// @return @ref GE_Result
/// @attention
/// <b>[URANUS] <em>It does not support ROP8 operation</em></b>
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetAlphaBlend(GE_Context *pGECtx,MS_BOOL enable, GE_BlendOp eBlendOp)
{
    MS_U16              u16en;
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);
#ifdef GE_SPLIT_SUPPORT
    bABL = enable;
#endif

    u16en = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_EN);
    if (enable)
    {
        u16en |= GE_EN_BLEND;
        if (GE_SetBlend(&pGECtxLocal->halLocalCtx, eBlendOp) != E_GE_OK)
        {
            GE_RETURN(pGECtx,E_GE_FAIL_PARAM);
        }
    }
    else
    {
        u16en &= ~GE_EN_BLEND;
    }
    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_EN, u16en);

    GE_RETURN(pGECtx,E_GE_OK);
}

GE_Result MDrv_GE_GetHwTag(GE_Context *pGECtx, U16 * phwtag)
{
    GE_Result           ret;
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    *phwtag = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_TAG);

    GE_RETURN(pGECtx,E_GE_OK);
}

GE_Result MDrv_GE_GetSrcBufferAddr(GE_Context *pGECtx, MS_PHYADDR * srcAddr)
{
    GE_Result           ret;
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;
    MS_U16              u16tmp;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    u16tmp = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_SRC_BASE_L);
    *srcAddr = u16tmp;
    u16tmp = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_SRC_BASE_H);
    *srcAddr |= ((MS_PHYADDR)(u16tmp & (~0x8000)) << 16);

    GE_RETURN(pGECtx,E_GE_OK);
}

GE_Result MDrv_GE_EnableTagIntr(GE_Context *pGECtx, MS_BOOL enable)
{
    return HAL_GE_EnableTagIntr(pGECtx, enable);
}
GE_Result MDrv_GE_MaskIRQ(GE_Context *pGECtx, U16 u16GeInterface, MS_BOOL enable)
{
    return HAL_GE_MaskIRQ(pGECtx, u16GeInterface, enable);
}
GE_Result MDrv_GE_ClearIRQ(GE_Context *pGECtx, U16 u16GeInterface)
{
    return HAL_GE_ClearIRQ(pGECtx, u16GeInterface);
}

//-------------------------------------------------------------------------------------------------
/// Set GE destination color key
/// @param  pGECtx                     \b IN: GE context
/// @param  enable                  \b IN: enable and update setting
/// @param  eCKOp                   \b IN: source color key mode
/// @param  ck_low                  \b IN: lower value of color key (E_GE_FMT_ARGB8888)
/// @param  ck_high                 \b IN: upper value of color key (E_GE_FMT_ARGB8888)
/// @return @ref GE_Result
/// @attention
/// <b>[URANUS] <em>color key does not check alpha channel</em></b>
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetDstColorKey(GE_Context *pGECtx, MS_BOOL enable, GE_CKOp eCKOp, MS_U32 ck_low, MS_U32 ck_high)
{
    MS_U16              u16en=0, u16op;
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    u16op = GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_OP_MODE);

    switch(eCKOp)
    {
        case E_GE_CK_EQ:
            u16en = GE_EN_DCK;
            u16op &= ~(GE_BIT1);
            break;
        case E_GE_CK_NE:
            u16en = GE_EN_DCK;
            u16op |= GE_BIT1;
            break;
        case E_GE_ALPHA_EQ:
            u16en = GE_EN_DSCK;
            u16op &= ~(GE_BIT3);
            break;
        case E_GE_ALPHA_NE:
            u16en = GE_EN_DSCK;
            u16op |= GE_BIT3;
            break;
        default:
            break;
    }

    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_OP_MODE, u16op);

    if (enable)
    {
        // Color key threshold
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_DCK_LTH_L, (ck_low & 0xFFFF));
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_DCK_LTH_H, (ck_high >> 16));
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_DCK_HTH_L, (ck_low & 0xFFFF));
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_DCK_HTH_H, (ck_high >> 16));
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_EN, (GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_EN)|u16en));
    }
    else
    {
        GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_EN, (GE_ReadReg(&pGECtxLocal->halLocalCtx, REG_GE_EN)&(~u16en)));
    }

    GE_RETURN(pGECtx,E_GE_OK);
}
//-------------------------------------------------------------------------------------------------
/// Set GE constant alpha value for any alpha operation
/// @param  pGECtx                     \b IN: GE context
/// @param  aconst                  \b IN: constant alpha value for output alpha and blending
/// @return @ref GE_Result
/// @sa MDrv_GE_SetAlphaBlend, MDrv_GE_SetAlphaSrc
//-------------------------------------------------------------------------------------------------
GE_Result MDrv_GE_SetAlphaConst(GE_Context *pGECtx, MS_U8 aconst)
{
    GE_CTX_LOCAL *pGECtxLocal = (GE_CTX_LOCAL*)pGECtx;

    GE_DBG("%s\n", __FUNCTION__);
    GE_ENTRY(pGECtx);

    GE_WriteReg(&pGECtxLocal->halLocalCtx, REG_GE_ALPHA_CONST, (MS_U16)(aconst&0xff));

    GE_RETURN(pGECtx,E_GE_OK);
}
#endif //_DRV_GE_C_

