//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// @file   drvGE.h
/// @brief  GE Driver Interface
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _DRV_GE_H_
#define _DRV_GE_H_

#define GFX_UTOPIA20                0
#define GE_LOCK_SUPPORT             0UL
#define GE_API_MUTEX                            1UL       //Using API mutex lock/unlock;  other is Drv mutex lock/unlock
#define GE_SWTABLE                              1UL       //Using virtul table as value stored

#define GE_FLAG_BLT_ROTATE_SHFT     16UL
#define GE_FLAG_BLT_ROTATE_MASK     (0x3UL << GE_FLAG_BLT_ROTATE_SHFT)

#define ByteSwap16(x) (((x) & 0x00ffUL) << 8 | ((x) & 0xff00UL) >> 8)
#define ByteSwap32(x) \
    ((((x) & 0xff000000UL) >> 24) | (((x) & 0x00ff0000UL) >>  8) | \
     (((x) & 0x0000ff00UL) <<  8) | (((x) & 0x000000ffUL) << 24))

#define getpid()                pInstance

#define GE_BIT0                         0x01UL
#define GE_BIT1                         0x02UL
#define GE_BIT2                         0x04UL
#define GE_BIT3                         0x08UL
#define GE_BIT4                         0x10UL
#define GE_BIT5                         0x20UL
#define GE_BIT6                         0x40UL
#define GE_BIT7                         0x80UL
#define GE_BIT8                         0x0100UL
#define GE_BIT9                         0x0200UL
#define GE_BIT10                        0x0400UL
#define GE_BIT11                        0x0800UL
#define GE_BIT12                        0x1000UL
#define GE_BIT13                        0x2000UL
#define GE_BIT14                        0x4000UL
#define GE_BIT15                        0x8000UL

//////////////////////////////// ENUMS

///Define GE Return Value
typedef enum
{
    /// fail
    E_GE_FAIL = 0,
    /// success
    E_GE_OK,
    /// address error
    E_GE_FAIL_ADDR,
    /// pitch error
    E_GE_FAIL_PITCH,
    /// function parameter error
    E_GE_FAIL_PARAM,

    /// not support
    E_GE_NOT_SUPPORT,

    /// pixel format error
    E_GE_FAIL_FORMAT,
    /// bitblit start address error
    E_GE_FAIL_BLTADDR,
    /// bitblt overlap (if STRETCH, ITALIC, MIRROR, ROTATE)
    E_GE_FAIL_OVERLAP,
    /// stretch bitblt fail
    E_GE_FAIL_STRETCH,
    /// italic bitblt fail (if MIRROR, ROTATE)
    E_GE_FAIL_ITALIC,

    /// engine is locked by others
    E_GE_FAIL_LOCKED,

    /// primitive will not be drawn
    E_GE_NOT_DRAW,

    /// Dependent functions are not linked
    E_GE_NO_DEPENDENT,

    /// context not inited.
    E_GE_CTXMAG_FAIL,

    E_GE_LAST_RESULT,

} GE_Result;

///Define MS FB Format, to share with GE,GOP
/// FIXME THE NAME NEED TO BE REFINED, AND MUST REMOVE UNNESSARY FMT
typedef enum
{
    /// color format I1
    E_MS_FMT_I1                     = 0x0,
    /// color format I2
    E_MS_FMT_I2                     = 0x1,
    /// color format I4
    E_MS_FMT_I4                     = 0x2,
    /// color format palette 256(I8)
    E_MS_FMT_I8                     = 0x4,
    /// color format blinking display
    E_MS_FMT_FaBaFgBg2266  = 0x6,
    /// color format for blinking display format
    E_MS_FMT_1ABFgBg12355           = 0x7,
    /// color format RGB565
    E_MS_FMT_RGB565                 = 0x8,
    /// color format ARGB1555
    /// @note <b>[URANUS] <em>ARGB1555 is only RGB555</em></b>
    E_MS_FMT_ARGB1555               = 0x9,
    /// color format ARGB4444
    E_MS_FMT_ARGB4444               = 0xa,
    /// color format ARGB1555 DST
    E_MS_FMT_ARGB1555_DST           = 0xc,
    /// color format YUV422
    E_MS_FMT_YUV422                 = 0xe,
    /// color format ARGB8888
    E_MS_FMT_ARGB8888               = 0xf,
    /// color format RGBA5551
    E_MS_FMT_RGBA5551               = 0x10,
    /// color format RGBA4444
    E_MS_FMT_RGBA4444               = 0x11,
    /// Start of New color define
    /// color format BGRA5551
    E_MS_FMT_BGRA5551               = 0x12,
    /// color format ABGR1555
    E_MS_FMT_ABGR1555               = 0x13,
    /// color format ABGR4444
    E_MS_FMT_ABGR4444               = 0x14,
    /// color format BGRA4444
    E_MS_FMT_BGRA4444               = 0x15,
    /// color format BGR565
    E_MS_FMT_BGR565                 = 0x16,
    /// color format RGBA8888
    E_MS_FMT_RGBA8888               = 0x1d,
    /// color format BGRA8888
    E_MS_FMT_BGRA8888               = 0x1e,
    /// color format ABGR8888
    E_MS_FMT_ABGR8888               = 0x1f,
    /// color format AYUV8888
    E_MS_FMT_AYUV8888               = 0x20,

    E_MS_FMT_GENERIC                = 0xFFFF,

} MS_ColorFormat;

///Define Buffer Format
typedef enum
{
    /// color format I1
    E_GE_FMT_I1                     = E_MS_FMT_I1,
    /// color format I2
    E_GE_FMT_I2                     = E_MS_FMT_I2,
    /// color format I4
    E_GE_FMT_I4                     = E_MS_FMT_I4,
    /// color format palette 256(I8)
    E_GE_FMT_I8                     = E_MS_FMT_I8,
    /// color format blinking display
    E_GE_FMT_FaBaFgBg2266           = E_MS_FMT_FaBaFgBg2266,
    /// color format for blinking display format
    E_GE_FMT_1ABFgBg12355           = E_MS_FMT_1ABFgBg12355,
    /// color format RGB565
    E_GE_FMT_RGB565                 = E_MS_FMT_RGB565,
    /// color format ARGB1555
    /// @note <b>[URANUS] <em>ARGB1555 is only RGB555</em></b>
    E_GE_FMT_ARGB1555               = E_MS_FMT_ARGB1555,
    /// color format ARGB4444
    E_GE_FMT_ARGB4444               = E_MS_FMT_ARGB4444,
    /// color format ARGB1555 DST
    E_GE_FMT_ARGB1555_DST           = E_MS_FMT_ARGB1555_DST,
    /// color format YUV422
    E_GE_FMT_YUV422                 = E_MS_FMT_YUV422,
    /// color format ARGB8888
    E_GE_FMT_ARGB8888               = E_MS_FMT_ARGB8888,
    /// color format RGBA5551
    E_GE_FMT_RGBA5551                = E_MS_FMT_RGBA5551,
     /// color format RGBA4444
    E_GE_FMT_RGBA4444               = E_MS_FMT_RGBA4444,
    /// color format ABGR8888
    E_GE_FMT_ABGR8888                = E_MS_FMT_ABGR8888,
    /// New Color Format
    /// color format BGRA5551
    E_GE_FMT_BGRA5551               = E_MS_FMT_BGRA5551,
    /// color format ABGR1555
    E_GE_FMT_ABGR1555               = E_MS_FMT_ABGR1555,
    /// color format ABGR4444
    E_GE_FMT_ABGR4444               = E_MS_FMT_ABGR4444,
    /// color format BGRA4444
    E_GE_FMT_BGRA4444               = E_MS_FMT_BGRA4444,
    /// color format BGR565
    E_GE_FMT_BGR565               = E_MS_FMT_BGR565,
    /// color format RGBA8888
    E_GE_FMT_RGBA8888               = E_MS_FMT_RGBA8888,
    /// color format RGBA8888
    E_GE_FMT_BGRA8888               = E_MS_FMT_BGRA8888,

    E_GE_FMT_GENERIC                = E_MS_FMT_GENERIC,

} GE_BufFmt;

///Define GE Primitive Drawing Flags
typedef enum
{
    /// Line color gradient enable, color is gradient by major-axis
    E_GE_FLAG_LINE_CONSTANT         = 0x00000000,

    /// Line color gradient enable, color is gradient by major-axis
    E_GE_FLAG_LINE_GRADIENT         = 0x00000001,

    /// Rectangle horizonal color gradient enable
    E_GE_FLAG_RECT_GRADIENT_X       = 0x00000002,
    /// Rectangle vertical color gradient enable
    E_GE_FLAG_RECT_GRADIENT_Y       = 0x00000004,

    /// Trapezoid horizonal color gradient enable
    E_GE_FLAG_TRAPEZOID_GRADIENT_X  = 0x00000010,
    /// Rectangle vertical color gradient enable
    E_GE_FLAG_TRAPEZOID_GRADIENT_Y  = 0x00000020,

    /// Trapezoid flag: top & bottom in X direction parallel Trapezoid, excluded with E_GE_FLAG_TRAPEZOID_Y
    E_GE_FLAG_TRAPEZOID_X             = 0x00000040,
    /// Trapezoid flag: top & bottom in Y direction parallel Trapezoid, excluded with E_GE_FLAG_TRAPEZOID_X
    E_GE_FLAG_TRAPEZOID_Y             = 0x00000080,
    /// Support SRC/DST RECT overlap
    ///@note OVERLAP does not support BLT_STRETCH, BLT_MIRROR, BLT_ROTATE, and different SRC/DST buffers.
    E_GE_FLAG_BLT_OVERLAP           = 0x00000100,

    /// Stretch bitblt enable
    ///@note STRETCH does not support BLT_OVERLAP
    E_GE_FLAG_BLT_STRETCH           = 0x00000200,

    /// Bitblt italic style enable
    ///@note ITALIC does not support BLT_MIRROR, BLT_ROTATE, BLT_OVERLAP
    E_GE_FLAG_BLT_ITALIC            = 0x00000400,

    /// Horizontal mirror
    ///@note MIRROR does not support FMT_I1, FMT_I2, FMT_I4, BLT_ITALIC, BLT_OVERLAP
    E_GE_FLAG_BLT_MIRROR_H          = 0x00001000,
    /// Vertical mirror
    E_GE_FLAG_BLT_MIRROR_V          = 0x00002000,

    E_GE_FLAG_BLT_DST_MIRROR_H      = 0x00004000,

    E_GE_FLAG_BLT_DST_MIRROR_V      = 0x00008000,

    /// 90 deree clockwise rotation
    ///@note ROTATE does not support italic, BLT_OVERLAP
    E_GE_FLAG_BLT_ROTATE_90         = (0x01 << GE_FLAG_BLT_ROTATE_SHFT),
    /// 180 degree clockwise rotation
    E_GE_FLAG_BLT_ROTATE_180        = (0x02 << GE_FLAG_BLT_ROTATE_SHFT),
    /// 270 degree clockwise rotation
    E_GE_FLAG_BLT_ROTATE_270        = (0x03 << GE_FLAG_BLT_ROTATE_SHFT),

    /// BLT_STRETCH by nearest filter (default: BILINEAR)
    E_GE_FLAG_STRETCH_NEAREST       = 0x00100000,
//    E_GE_FLAG_STRETCH_CLAMP         = 0x00200000,

    // (RESERVED)
    E_GE_FLAG_BUF_TILE              = 0x01000000,

    E_GE_FLAG_BYPASS_STBCOEF         = 0x02000000,

} GE_Flag;

typedef enum
{
    E_GE_ROTATE_0 = 0,
    E_GE_ROTATE_90 = 1,
    E_GE_ROTATE_180 = 2,
    E_GE_ROTATE_270 = 3,
} GE_RotateAngle;

///Define Blending Operation
typedef enum
{
    /// Cout = Csrc * A + Cdst * (1-A)
    /// 1
    E_GE_BLEND_ONE                  = 0,
    /// constant
    E_GE_BLEND_CONST                = 1,
    /// source alpha
    E_GE_BLEND_ASRC                 = 2,
    /// destination alpha
    E_GE_BLEND_ADST                 = 3,

    /// Cout = ( Csrc * (Asrc*Aconst) + Cdst * (1-Asrc*Aconst) ) / 2
    E_GE_BLEND_ROP8_ALPHA           = 4,
    /// Cout = ( Csrc * (Asrc*Aconst) + Cdst * (1-Asrc*Aconst) ) / ( Asrc*Aconst + Adst*(1-Asrc*Aconst) )
    E_GE_BLEND_ROP8_SRCOVER         = 5,
    /// Cout = ( Csrc * (Asrc*Aconst) + Cdst * (1-Asrc*Aconst) ) / ( Asrc*Aconst * (1-Adst) + Adst )
    ///@note
    /// <b>[URANUS] <em>It does not support BLEND_ALPHA, BLEND_SRCOVER, BLEND_DSTOVER</em></b>
    E_GE_BLEND_ROP8_DSTOVER         = 6,
    /// Cout = ( Csrc * Aconst)
    E_GE_BLEND_ALPHA_ADST           = 7,
    /// Cout = Csrc * A + Cdst * (1-A)
    /// 0
    E_GE_BLEND_ZERO                 = 8,
    /// 1 - constant
    E_GE_BLEND_CONST_INV            = 9,
    /// 1 - source alpha
    E_GE_BLEND_ASRC_INV             = 10,
    /// 1 - destination alpha
    E_GE_BLEND_ADST_INV             = 11,
    /// Csrc * Adst * Asrc * Aconst + Cdst * Adst * (1 - Asrc * Aconst)
    E_GE_BLEND_SRC_ATOP_DST         = 12,
    /// (Cdst * Asrc * Aconst * Adst + Csrc * Asrc * Aconst * (1 - Adst)) / ((Asrc * Aconst * (1 - Adst) + Asrc * Aconst * Adst)
    E_GE_BLEND_DST_ATOP_SRC         = 13,
    /// ((1 - Adst) * Csrc * Asrc * Aconst + Adst * Cdst * (1 - Asrc * Aconst)) / (Asrc * Aconst * (1 - Adst) + Adst * (1 - Asrc * Aconst))
    E_GE_BLEND_SRC_XOR_DST          = 14,
    /// Csrc * (1 - Aconst)
    E_GE_BLEND_INV_CONST            = 15,

} GE_BlendOp;


//-------------------------------------------------
/// Define Virtual Command Buffer Size
typedef enum
{
    /// 4K
    E_GE_VCMD_4K = 0,
    /// 8K
    E_GE_VCMD_8K = 1,
    /// 16K
    E_GE_VCMD_16K = 2,
    /// 32K
    E_GE_VCMD_32K = 3,
    /// 64K
    E_GE_VCMD_64K = 4,
    /// 128K
    E_GE_VCMD_128K = 5,
    /// 256K
    E_GE_VCMD_256K = 6,
    /// 512K
    E_GE_VCMD_512K = 7,
    /// 1024k
    E_GE_VCMD_1024K = 8,
} GE_VcmqBufSize;

///Define alpha output selection
typedef enum
{
    /// constant
    E_GE_ALPHA_CONST                = 0,
    /// source alpha
    E_GE_ALPHA_ASRC                 = 1,
    /// destination alpha
    E_GE_ALPHA_ADST                 = 2,
    /// Aout = Asrc*Aconst
    E_GE_ALPHA_ROP8_SRC             = 3,
    /// Aout = Asrc*Aconst * Adst
    E_GE_ALPHA_ROP8_IN              = 4,
    /// Aout = (1-Asrc*Aconst) * Adst
    E_GE_ALPHA_ROP8_DSTOUT          = 5,
    /// Aout = (1-Adst) * Asrc*Aconst
    E_GE_ALPHA_ROP8_SRCOUT          = 6,
    /// Aout = (Asrc*Aconst) + Adst*(1-Asrc*Aconst) or (Asrc*Aconst)*(1-Adst) + Adst
    E_GE_ALPHA_ROP8_OVER            = 7,
    /// 1 - Aconst
    E_GE_ALPHA_ROP8_INV_CONST       = 8,
    /// 1 - Asrc
    E_GE_ALPHA_ROP8_INV_ASRC        = 9,
    /// 1 - Adst
    E_GE_ALPHA_ROP8_INV_ADST        = 10,
    /// Adst * Asrc * Aconst + Adst * (1 - Asrc * Aconst) A atop B
    E_GE_ALPHA_ROP8_SRC_ATOP_DST    = 11,
    /// Asrc * Aconst * Adst + Asrc * Aconst * (1 - Adst) B atop A
    E_GE_ALPHA_ROP8_DST_ATOP_SRC    = 12,
    /// (1 - Adst) * Asrc * Aconst + Adst * (1 - Asrc * Aconst) A xor B
    E_GE_ALPHA_ROP8_SRC_XOR_DST     = 13,
    /// Asrc * Asrc * Aconst + Adst * (1 - Asrc * Aconst)
    E_GE_ALPHA_ROP8_INV_SRC_ATOP_DST= 14,
    /// Asrc * (1 - Asrc * Aconst) + Adst * Asrc * Aconst
    E_GE_ALPHA_ROP8_INV_DST_ATOP_SRC= 15,
} GE_AlphaSrc;

/// Define DFB Blending Related:
typedef enum
{
    E_GE_DFB_BLD_FLAG_COLORALPHA         = 0x0001,
    E_GE_DFB_BLD_FLAG_ALPHACHANNEL       = 0x0002,
    E_GE_DFB_BLD_FLAG_COLORIZE           = 0x0004,
    E_GE_DFB_BLD_FLAG_SRCPREMUL          = 0x0008,
    E_GE_DFB_BLD_FLAG_SRCPREMULCOL       = 0x0010,
    E_GE_DFB_BLD_FLAG_DSTPREMUL          = 0x0020,
    E_GE_DFB_BLD_FLAG_XOR                 = 0x0040,
    E_GE_DFB_BLD_FLAG_DEMULTIPLY         = 0x0080,
    E_GE_DFB_BLD_FLAG_SRCALPHAMASK       = 0x0100,
    E_GE_DFB_BLD_FLAG_SRCCOLORMASK       = 0x0200,
    E_GE_DFB_BLD_FLAG_ALL                 = 0x03FF,
}GE_DFBBldFlag;

typedef enum
{
    E_GE_DFB_BLD_OP_ZERO               = 0,
    E_GE_DFB_BLD_OP_ONE                = 1,
    E_GE_DFB_BLD_OP_SRCCOLOR          = 2,
    E_GE_DFB_BLD_OP_INVSRCCOLOR       = 3,
    E_GE_DFB_BLD_OP_SRCALPHA          = 4,
    E_GE_DFB_BLD_OP_INVSRCALPHA       = 5,
    E_GE_DFB_BLD_OP_DESTALPHA         = 6,
    E_GE_DFB_BLD_OP_INVDESTALPHA      = 7,
    E_GE_DFB_BLD_OP_DESTCOLOR         = 8,
    E_GE_DFB_BLD_OP_INVDESTCOLOR      = 9,
    E_GE_DFB_BLD_OP_SRCALPHASAT       = 10,
}GE_DFBBldOP;


typedef enum
{
    E_GE_WP_IN_RANGE                = 0,
    E_GE_WP_OUT_RANGE               = 1,
    E_GE_WP_DISABLE                 = 0xFF,

} GE_WPType;

///Define Buffer Usage Type
typedef enum
{
    /// Desitination buffer for LINE, RECT, BLT
    E_GE_BUF_DST                    = 0,
    /// Source buffer for BLT
    E_GE_BUF_SRC                    = 1,
} GE_BufType;

//-------------------------------------------------
/// Define rotation angle
typedef enum
{
    /// Do not rotate
    GEROTATE_0,
    /// Rotate 90 degree
    GEROTATE_90,
    /// Rotate 180 degree
    GEROTATE_180,
    /// Rotate 270 degree
    GEROTATE_270,
} GFX_RotateAngle;

///Define Blending Coefficient
typedef enum
{
    /// Csrc
    COEF_ONE                        = 0,
    /// Csrc * Aconst + Cdst * (1 - Aconst)
    COEF_CONST                      ,
    ///  Csrc * Asrc + Cdst * (1 - Asrc)
    COEF_ASRC                       ,
    /// Csrc * Adst + Cdst * (1 - Adst)
    COEF_ADST                       ,

    /// Cdst
    COEF_ZERO                       ,
    /// Csrc * (1 - Aconst) + Cdst * Aconst
    COEF_1_CONST                    ,
    /// Csrc * (1 - Asrc) + Cdst * Asrc
    COEF_1_ASRC                     ,
    ///  Csrc * (1 - Adst) + Cdst * Adst
    COEF_1_ADST                     ,

    /// ((Asrc * Aconst) * Csrc + (1-(Asrc *Aconst)) * Cdst) / 2
    COEF_ROP8_ALPHA                 ,
    /// ((Asrc * Aconst) * Csrc + Adst * Cdst * (1-(Asrc * Aconst))) / (Asrc * Aconst) + Adst * (1- Asrc * Aconst))
    COEF_ROP8_SRCOVER               ,
    /// ((Asrc * Aconst) * Csrc * (1-Adst) + Adst * Cdst) / (Asrc * Aconst) * (1-Adst) + Adst)
    COEF_ROP8_DSTOVER               ,

    /// Csrc * Aconst
    ///@note
    /// <b>[URANUS] <em>It does not support COEF_CONST_SRC</em></b>
    COEF_CONST_SRC              ,
    /// Csrc * (1 - Aconst)
    ///@note
    /// <b>[URANUS] <em>It does not support COEF_1_CONST_SRC</em></b>
    COEF_1_CONST_SRC            ,

    /// Csrc * Adst * Asrc * Aconst + Cdst * Adst * (1 - Asrc * Aconst)
    ///@note
    /// <b>[URANUS] <em>It does not support COEF_SRC_ATOP_DST</em></b>
    COEF_SRC_ATOP_DST               ,
    /// Cdst * Asrc * Aconst * Adst + Csrc * Asrc * Aconst * (1 - Adst)
    ///@note
    /// <b>[URANUS] <em>It does not support COEF_DST_ATOP_SRC</em></b>
    COEF_DST_ATOP_SRC               ,
    /// (1 - Adst) * Csrc * Asrc * Aconst + Adst * Cdst * (1 - Asrc * Aconst)
    ///@note
    /// <b>[URANUS] <em>It does not support COEF_SRC_XOR_DST</em></b>
    COEF_SRC_XOR_DST                ,
} GFX_BlendCoef;

typedef enum
{
    E_GFX_INIT_VER0             =0,
}EN_GFX_INIT_VER;


///Define Colorkey Mode
typedef enum
{
    /// color in coler key range is keying.
    E_GE_CK_EQ                      = 0, // Color EQ CK is color key
    /// color NOT in color key range is keing.
    E_GE_CK_NE                      = 1, // Color NE CK is color key
    /// Alpha in coler key range is keying.
    E_GE_ALPHA_EQ                   = 2, // Color EQ Alpha is color key
    /// Alpha NOT in color key range is keing.
    E_GE_ALPHA_NE                   = 3, // Color NE Alpha is color key
} GE_CKOp;

//////////////////////////////// STRUCTS
///Define the RECT
typedef struct
{
    /// X start address
    MS_U16                          x;
    /// Y start address
    MS_U16                          y;
    /// width
    MS_U16                          width;
    /// height
    MS_U16                          height;

} GE_Rect;

typedef struct
{
    /// x0 start address
    MS_U16                          u16X0;
    /// y0 start address
    MS_U16                          u16Y0;
    /// x1 start address
    MS_U16                          u16X1;
    /// y1 start address
    MS_U16                          u16Y1;
    /// delta of Top
    MS_U16                          u16DeltaTop;
    // delta of Bottom
    MS_U16                          u16DeltaBottom;
} GE_Trapezoid;

typedef struct
{
    /// Destination block
    union
    {
        GE_Rect dstblk;
        GE_Trapezoid dsttrapeblk;
    };

}GE_DstBitBltType;

///Define Src/Dst Buffer Information
typedef struct
{
    /// Specified format
    GE_BufFmt                       srcfmt;
    /// Specified format
    GE_BufFmt                       dstfmt;
    /// Base alignment in byte unit
    MS_U16                          srcpit;
    /// Pitch alignment in byte unit
    MS_U16                          dstpit;
    /// Height alignment in pixel unit <b>(RESERVED)</b>
    MS_PHYADDR                      srcaddr;
    /// StretchBlt base/pitch alignment in byte unit
    MS_PHYADDR                      dstaddr;
//	    /// GE TLB Mode
//	    GE_TLB_Mode                     tlbmode;
//	    /// GE TLB Entry source base address
//	    MS_PHYADDR                      tlbsrcaddr;
//	    /// GE TLB Entry destination base address
//	    MS_PHYADDR                      tlbdstaddr;
    /// GE TLB Entry Flush Table
    MS_BOOL                         bEnflushtlbtable;
} GE_BufInfo;

///Define the RECT
typedef struct
{
    /// STBB INIT X
    MS_U32                          init_x;
    /// STBB INIT Y
    MS_U32                          init_y;
    /// STBB X
    MS_U32                          x;
    /// STBB Y
    MS_U32                          y;

} GE_ScaleInfo;

///Define POINT
typedef struct
{
    MS_U16                          x;
    MS_U16                          y;

} GE_Point;

typedef struct
{
    MS_U32              flags;
    GE_Rect             src;
    GE_DstBitBltType    dst;
    GE_ScaleInfo*       scaleinfo;
}PatchBitBltInfo;

typedef struct
{
   void*            pInstance;
   U32              u32GE_DRV_Version;
   BOOL             bGEMode4MultiProcessAccess;
   S32              s32CurrentProcess;
   S32              s32CurrentThread;
   GE_BufInfo       pBufInfo;
}GE_Context;


///Define GE Init Configuration
typedef struct
{
    MS_U8                         u8Miu;
    MS_U8                         u8Dither;
    MS_U32                        u32VCmdQSize;                       /// MIN:4K, MAX:512K, <MIN:Disable
    MS_PHYADDR                    u32VCmdQAddr;                       // 8-byte aligned
    MS_BOOL                       bIsHK;                              /// Running as HK or Co-processor
    MS_BOOL                       bIsCompt;                           /// Running as compatible mode. In compatible mode, the parameter checking loose for NOS APP compatibility.  TRUE=>compatible with 51/Chakra , FALSE=>linux OS style
} GE_Config;

//-------------------------------------------------
/// Define RGB color in LE
typedef struct
{
    /// Blue
    MS_U8 b;
    /// Green
    MS_U8 g;
    /// Red
    MS_U8 r;
    /// Alpha
    MS_U8 a;
} GE_RgbColor;

///Define Buffer Format Information
typedef struct
{
    /// Specified format
    GE_BufFmt                       fmt;

    /// Base alignment in byte unit
    MS_U8                           u8BaseAlign;
    /// Pitch alignment in byte unit
    MS_U8                           u8PitchAlign;
    /// Non-1p mode base/pitch alignment in byte unit
    MS_U8                           u8Non1pAlign;
    /// Height alignment in pixel unit <b>(RESERVED)</b>
    MS_U8                           u8HeightAlign;
    /// StretchBlt base/pitch alignment in byte unit
    MS_U8                           u8StretchAlign;
    /// Tile mode base alignment in byte unit <b>(RESERVED)</b>
    MS_U8                           u8TileBaseAlign;
    /// Tile mode width alignment in pixel unit <b>(RESERVED)</b>
    MS_U8                           u8TileWidthAlign;
    /// Tile mode height alignment in pixel unit <b>(RESERVED)</b>
    MS_U8                           u8TileHeightAlign;

    /// Format capability flags <b>(RESERVED)</b>
    MS_U32                          u32CapFlags;

} GE_FmtCaps;

/// Define GE bitblt down-scaling caps
typedef struct
{
    /// Bitblt down scale range start
    MS_U8 u8RangeMax;
    /// Bitblt down scale range end
    MS_U8 u8RangeMin;
    /// Bitblt down scale continuous range end
    MS_U8 u8ContinuousRangeMin;
    /// Is full range support down scaling.
    /// - TRUE: The down scale value between u8RangeMax to u8RangeMin is fully supported.
    /// - FALSE: The down scale value between u8RangeMax to u8ContinuousRangeMin is fully supported.
    ///          The down scale value between u8ContinuousRangeMin to u8RangeMin is supported
    ///          if the value is power of two (e.g., 4, 8, 16, and 32).
    MS_BOOL bFullRangeSupport;

    /// Help Maintain Functions GE_SetBltScaleRatio/GE_CalcBltScaleRatio
    /// 1>>u8ShiftRangeMax = u8RangeMax
    MS_U8 u8ShiftRangeMax;
    /// 1>>u8ShiftRangeMin = u8RangeMin
    MS_U8 u8ShiftRangeMin;
    /// 1>>u8ShiftContinuousRangeMin = u8ContinuousRangeMin
    MS_U8 u8ShiftContinuousRangeMin;
}GE_BLT_DownScaleCaps;

/// Define GE chip property for different chip characteristic.
typedef struct
{
    MS_U16 WordUnit;
    MS_BOOL bSupportFourePixelMode;
    MS_BOOL bFourPixelModeStable;
    MS_BOOL bSupportMultiPixel;
    MS_BOOL bSupportSpiltMode;
    MS_BOOL bSupportTLBMode;
    MS_BOOL bSupportTwoSourceBitbltMode;
    GE_BLT_DownScaleCaps BltDownScaleCaps;
    MS_U16 MIUSupportMaxNUM;
}GE_CHIP_PROPERTY;

/*=================================================================================================================
    GFX Init
=================================================================================================================*/
typedef struct
{
    MS_U32                          u32VCmdQSize;                       /// MIN:4K, MAX:512K, <MIN:Disable
    MS_PHY                          u32VCmdQAddr;                       //  8-byte aligned
    MS_BOOL                         bIsHK;                              /// Running as HK or Co-processor
    MS_BOOL                         bIsCompt;                           /// Running as compatible mode. In compatible mode, the parameter checking loose for NOS APP compatibility.  TRUE=>compatible with 51/Chakra , FALSE=>linux OS style
}GFX_Init_Config;

typedef struct
{
    EN_GFX_INIT_VER             eVerType;
    GFX_Init_Config*            pGFX_Init;
    MS_U32                      u32Size;
}GFX_INIT_ARGS;


MS_U32    GE_Divide2Fixed(MS_U16 u16x, MS_U16 u16y, MS_U8 nInteger, MS_U8 nFraction);
MS_U32    _GFXAPI_PHYS_ADDR_IN_MIU(MS_U32 ge_fbaddr) ;
MS_U8     _GFXAPI_MIU_ID(MS_U32 ge_fbaddr);
GE_Result MDrv_GE_Init(void* pInstance, GE_Config *cfg, GE_Context **ppGECtx);
GE_Result MDrv_GE_BitBltEX(GE_Context *pGECtx, GE_Rect *src, GE_DstBitBltType *dst, U32 flags, GE_ScaleInfo* scaleinfo);
GE_Result MDrv_GE_Chip_Proprity_Init(GE_Context *pGECtx, GE_CHIP_PROPERTY **ppGeChipPro);
GE_Result MDrv_GE_SetOnePixelMode(GE_Context *pGECtx, MS_BOOL enable);
GE_Result MDrv_GE_GetNextTAGID(GE_Context *pGECtx, MS_BOOL bStepTagBefore, MS_U16* u16NextTagID);
GE_Result MDrv_GE_SetNextTAGID(GE_Context *pGECtx, MS_U16 *tagID, MS_BOOL bHighByte);
GE_Result MDrv_GE_GetCurTAGID(GE_Context *pGECtx, MS_U16 *tagID, MS_BOOL * bHighByte);
GE_Result MDrv_GE_WaitTAGID(GE_Context *pGECtx, MS_U16 tagID, MS_BOOL bWaitForever, MS_BOOL * bwaitSuccess);
GE_Result MDrv_GE_SetSrcBuffer(GE_Context *pGECtx, GE_BufFmt src_fmt, MS_U16 pix_width, MS_U16 pix_height, MS_PHYADDR addr, MS_U16 pitch, MS_U32 flags);
GE_Result MDrv_GE_SetDstBuffer(GE_Context *pGECtx, GE_BufFmt dst_fmt, MS_U16 pix_width, MS_U16 pix_height, MS_PHYADDR addr, MS_U16 pitch, MS_U32 flags);
GE_Result MDrv_GE_SetClipWindow(GE_Context *pGECtx, GE_Rect *rect);
GE_Result MDrv_GE_SetAlphaSrc(GE_Context *pGECtx, GE_AlphaSrc eAlphaSrc);
GE_Result MDrv_GE_SetAlphaBlend(GE_Context *pGECtx,MS_BOOL enable, GE_BlendOp eBlendOp);
GE_Result MDrv_GE_EnableTagIntr(GE_Context *pGECtx, MS_BOOL enable);
GE_Result MDrv_GE_MaskIRQ(GE_Context *pGECtx, U16 u16GeInterface, MS_BOOL enable);
GE_Result MDrv_GE_ClearIRQ(GE_Context *pGECtx, U16 u16GeInterface);
GE_Result MDrv_GE_GetHwTag(GE_Context *pGECtx, U16 * phwtag);
GE_Result MDrv_GE_GetSrcBufferAddr(GE_Context *pGECtx, MS_PHYADDR * srcAddr);
GE_Result MDrv_GE_SetDstColorKey(GE_Context *pGECtx, MS_BOOL enable, GE_CKOp eCKOp, MS_U32 ck_low, MS_U32 ck_high);
GE_Result MDrv_GE_SetAlphaConst(GE_Context *pGECtx, MS_U8 aconst);

#endif // _DRV_GE_H_

