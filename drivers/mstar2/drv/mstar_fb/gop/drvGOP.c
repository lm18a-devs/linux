//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (!MStar Confidential Information!L) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------
#include <linux/module.h>
#include <linux/fs.h>    // for MKDEV()
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/wait.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/delay.h>

#include "linux/kernel.h"
#include "mhal_regGOP.h"
#include "mhal_GOP.h"
#include "drvGOP.h"
#include "../../dlmalloc/dlmalloc.h"

#include "chip_int.h"
#include "chip_setup.h"

#include "film.h"

#ifdef LGX_MGW_FUNCTIONALITY
#include "khalGE.h"
#include "kdrvGE.h"
#include "kdrvGE_private.h"
#endif

#define VIDEO_SCALING_WITH_OSD 0
//-------------------------------------------------------------------------------------------------
//  Local Compiler Options
//-------------------------------------------------------------------------------------------------
#ifdef MSTARFB_DIP_INTERRUPT
irqreturn_t MDrv_MGW_IntHandler(int irq,void *devid);
#else
int MDrv_MGW_IntHandler(void);
#endif
void _MDrv_GOP_GetHScale(U8 u8GOP, U16 * pu16Ratio );
void _MDrv_GOP_GetVScale(U8 u8GOP, U16 * pu16Ratio );
void _MDrv_GOP_SetCropWin(U8 u8GOP, U16 x0, U16 x1, U16 y0, U16 y1);


//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------
#define ALIGN_CHECK(value,factor) ((value + factor-1) & (~(factor-1)))

#define KERNEL_FILM 0 //modify to 0 for lm17a bring up

//-------------------------------------------------------------------------------------------------
//  Local Structures
//-------------------------------------------------------------------------------------------------
typedef enum
{
    E_GOP0 = 0,
    E_GOP1 = 1,
    E_GOP2 = 2,
    E_GOP3 = 3,
    E_GOP_Dwin = 4,
    E_GOP_MIXER = 5,
}E_GOP_TYPE;

//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------
DRV_MPOOL_RECORD g_mpool_cmd_record[MAX_FB_CMD_RECORD];
U32 u32CuurentRecordIdx  = 0;
int bIsResume = 0;
static int bFirstCreate = 0;
//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------
U8 bForceWriteIn;


#ifdef LGX_MGW_FUNCTIONALITY
GFX_API_LOCAL g_apiGFXLocal =
{
u32dbglvl: -1,
//     fpGetBMP:NULL,
//     fpGetFont:NULL,
_blendcoef:COEF_ASRC,
_angle:GEROTATE_0,
_bNearest:FALSE,
_bPatchMode:FALSE,
_bMirrorH:FALSE,
_bMirrorV:FALSE,
_bDstMirrorH:FALSE,
_bDstMirrorV:FALSE,
_bItalic:FALSE,
_line_enable:FALSE,
_line_pattern:0x00,
_line_factor:0,
#ifdef DBGLOG
_bOutFileLog:FALSE,
_pu16OutLogAddr:NULL,
_u16LogCount:0,
#endif
g_pGEContext:NULL,
pGeChipProperty:NULL,
u32LockStatus:0,
_bInit:0,
u32geRgbColor:0,

};

#define MAIN_UI_GOP_ID   (E_GOP0)
#define MGW_GOP_ID       (E_GOP1)
#define MGW_GWIN_ID      2

#endif

//-------------------------------------------------------------------------------------------------
//  Debug Functions
//-------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//  Local Functions
//------------------------------------------------------------------------------

typedef struct
{
    bool   bUsed;
    mspace stMemoryPool;                /* dlmalloc handler */
    bool   bMPool;
} MsOS_MemoryPool_Info;

#define MSOS_MEMPOOL_MAX        8//(8+64)
#define MSOS_OS_MALLOC          (0x7654FFFF)
#define MSOS_MALLOC_ID          (0x0000FFFF)
#define MSOS_ID_PREFIX           0x76540000
#define MSOS_ID_PREFIX_MASK      0xFFFF0000
#define MSOS_ID_MASK             0x0000FFFF //~MSOS_ID_PREFIX_MASK
#define GOP_INVALID_ADDR         0xFFFFFFFF

typedef struct
{
    MS_GOP_CREATE_GWIN      gGWINInfo[4] ;
    MsOS_MemoryPool_Info    GOP_MemoryPool_Info[MSOS_MEMPOOL_MAX];
    U32 alloc_addr[MSOS_MEMPOOL_MAX][0x1000];
    U32 alloc_idx;
    int fScrollEnable;
}ST_MDRV_GOP_DATA_HANDLE;
static ST_MDRV_GOP_DATA_HANDLE gstMdrvGopDataHandle;

typedef struct
{
    bool             bUsed;
    spinlock_t          stMutex;
    U8               u8Name[50];
} MsOS_Mutex_Info;
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
/* unused variables */
#else
static MsOS_Mutex_Info          _MsOS_Mutex_Info[50];
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */

static                          DEFINE_SPINLOCK(_GOP_Mutex_Mutex);
#define MUTEX_MUTEX_LOCK()      spin_lock(&_GOP_Mutex_Mutex)
#define MUTEX_MUTEX_UNLOCK()    spin_unlock(&_GOP_Mutex_Mutex)


wait_queue_head_t  _fb_waitqueue[1];
bool g_bIsrAttached = FALSE;
bool g_bVsyncSleep = FALSE;
static spinlock_t gop_lock;
static spinlock_t flim_lock;


#ifdef LGX_MGW_FUNCTIONALITY
static bool g_bMGWRunning = FALSE;
static bool g_bMGWChangSourceCropWin = TRUE;
static U16 gbSkipDisplayFrameCount = 0x0;
static U16 MgwOnScreenWidth = 600;
static U16 MgwOnScreenHeight = 600;
static U16 g_u16MgwScalingRatio = 0xCCC;
static U16  g_u16CurAppCursorPosX = (SC_OP_TIMING_WIDTH >> 1);
static U16  g_u16CurAppCursorPosY = (SC_OP_TIMING_HEIGHT >> 1);
static const U16 g_u16PanelWidth = SC_OP_TIMING_WIDTH;
static const U16 g_u16PanelHeight = SC_OP_TIMING_HEIGHT;
static U16 gMgwDipWindow = 0x1;
static U16 gu16ChangeMgwEnState = TRUE;
static U16 gu16MgwEnState = TRUE;
static const U16 gu16MgwScGopBlendingDelay = 0x7C;
static U16  g_u16CaptureWinX = 0x0;
static U16  g_u16CaptureWinY = 0x0;
static U16  g_u16CaptureWinW = 0x0;
static U16  g_u16CaptureWinH = 0x0;
static U16 g_u16MgwFbPitch = 1920;
static U32 g_u32MgwFbSize = 0x0;
static const U16 DIP_LIMIT_WRITE_MINIMUM_LENGTH = 0x200;
static const U16 gu16DipAlignPixel = 16;

static void _S_MDrv_MGW_ConvertReal2CaptCoordinate(
    U16   u16RealWorldCoorX,
    U16   u16RealWorldCoorY,
    U16 * pu16CaptWorldX,
    U16 * pu16CaptWorldY
    );
static void _S_MDrv_MGW_GetNextCaptureCoordinate(
    U16   u16CurCursorCoorX,
    U16   u16CurCursorCoorY,
    U16   u16DipCapturedFrameWidth,
    U16   u16DipCapturedFrameHeight,
    U16 * pu16CropPosX0,
    U16 * pu16CropPosY0,
    U16 * pu16CropPosX1,
    U16 * pu16CropPosY1);

static void S_MDrv_Get_CaptureWinSize(
    U16 u16ApWantWidth,
    U16 u16ApWantHeight,
    U16 u16ScalingRatio,
    U16 *pu16DipCapturedFrameWidth,
    U16 *pu16DipCapturedFrameHeight,
    U16 *pu16DipCapturedFramePitch);

typedef struct
{
    U32  u32MGW_DispBuffAddr;
    U16  u16MGW_DispBuffPitch;
    U16  u16MGW_DispBuffWidth;
    U16  u16MGW_DispBuffHeight;
    U16  u16MGW_ScalingFactor;
    bool bDispIng;
    U16  u16MGW_DispBuffFmt;
} Mgw_DispBufSt;
Mgw_DispBufSt g_mgs_disp_buf[2];
typedef struct
{
    U32  u32MGW_CaptBuffAddr;
    U16  u32MGW_CaptBuffAddrMiu;
    U16  u16MGW_CaptTag;
    U16  u16MGW_CursorLocX;
    U16  u16MGW_CursorLocY;
} Mgw_CaptFrameInfo;
Mgw_CaptFrameInfo g_mgw_capt_buf[8];

U32  g_u32DipBufferAddr = 0xFFFFFFFF;

static spinlock_t mgw_lock;
#endif

struct irq_test {
        int irq;
        wait_queue_head_t q;
        atomic_t count;
};

static U16 _GOP_GetBPP(DRV_GOPColorType fbFmt)
{
    U16 bpp=0;

    switch ( fbFmt )
    {
        case E_DRV_GOP_COLOR_RGB555_BLINK :
        case E_DRV_GOP_COLOR_RGB565 :
        case E_DRV_GOP_COLOR_ARGB1555:
        case E_DRV_GOP_COLOR_RGBA5551:
        case E_DRV_GOP_COLOR_ARGB4444 :
        case E_DRV_GOP_COLOR_RGBA4444 :
        case E_DRV_GOP_COLOR_RGB555YUV422:
        case E_DRV_GOP_COLOR_YUV422:
        case E_DRV_GOP_COLOR_2266:
            bpp = 2;
            break;
        case E_DRV_GOP_COLOR_ARGB8888 :
        case E_DRV_GOP_COLOR_ABGR8888 :
            bpp = 4;
            break;
        case E_DRV_GOP_COLOR_I8 :
            bpp = 1;
            break;
        default :
            //print err
            bpp = 0xFFFF;
            break;
    }
    return bpp;
}

void _MDrv_GOP_UpdateReg(U8 u8Gop)
{
    U16 u16GOPOfst = 0;
    U32 bankoffset=0;

    if(bForceWriteIn)
    {
        _HAL_GOP_Write16Reg(GOP_BAK_SEL, GOP_VAL_FWR, GOP_REG_HW_MASK);
        _HAL_GOP_Write16Reg(GOP_BAK_SEL, 0x0000, GOP_REG_HW_MASK);
    }
    else
    {
        _HAL_GOP_GetBnkOfstByGop(u8Gop,&bankoffset);
        u16GOPOfst = (bankoffset>>8);
#ifdef GOP_BANK_SHIFT
        if((E_GOP_TYPE)u8Gop == E_GOP2)
            u16GOPOfst -= 1;
#endif
        _HAL_GOP_Write16Reg(GOP_BAK_SEL, u16GOPOfst | GOP_BIT10 , GOP_REG_WORD_MASK);
        _HAL_GOP_Write16Reg(GOP_BAK_SEL, u16GOPOfst, GOP_REG_WORD_MASK);
    }

}

void _MDrv_GOP_SetForceWrite(U8 bEnable)
{
    bForceWriteIn = bEnable;
}

void _MDrv_GOP_Init(U8 u8GOP)
{
    U8 u8Idx;
    U32 bankoffset=0;


    if(u8GOP >= MAX_GOP_SUPPORT)
    {
        printk("[%s] not support gop%d in this chip version!!\n",__FUNCTION__, u8GOP);
        return;
    }
    _HAL_GOP_GetBnkOfstByGop(u8GOP,&bankoffset);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, 0x1, 0x1);          // GOP rst
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, 0x0, 0x7);          // clear H/V sync reserve
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL1, 0x4100, 0xff00);     // Set REGDMA interval

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HSTRCH, 0x1000, 0xFFFF);   // Sanger 070713 For REGDMA Ready issue.
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VSTRCH, 0x1000, 0xFFFF);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, GOP_BIT14, GOP_BIT14);    // Set mask Hsync when VFDE is low
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, 0x000, 0x400);     // Output = RGB
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_RATE, 0x0090, GOP_REG_WORD_MASK);     // enable blink capability - for ttx usage
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, 0x0000, 0x400);       // Enable RGB output
    //_HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, GOP_BIT11, GOP_BIT11);       // Enable Transparent Color

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, GOP_BIT15, GOP_BIT15);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_BANK_FWR, GOP_BIT7, GOP_BIT7);

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_BW, GOP_FIFO_BURST_ALL, GOP_FIFO_BURST_MASK );  //set GOP DMA Burst length to "32"
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_BW, GOP_FIFO_THRESHOLD, GOP_REG_LW_MASK ); //set DMA FIFO threshold to 3/4 FIFO length

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_OLDADDR, 0x0, (GOP_BIT1 | GOP_BIT0) ); //temporally disable GOP clk dynamical gated - to avoid update palette problem

    /*set clamping value between 2 pixels, it can avoid some cases garbage happen.*/
    _HAL_GOP_Write16Reg((bankoffset+0x200)+GOP_4G_STRCH_HSZ, 0, GOP_BIT0);
    _HAL_GOP_Init(u8GOP);


    if (u8GOP==0)
    {
        for (u8Idx=0; u8Idx<MAX_GOP0_GWIN; u8Idx++)
        {
                _HAL_GOP_Write16Reg(GOP_4G_DRAM_VSTR_L(u8Idx),0, GOP_REG_WORD_MASK);
                _HAL_GOP_Write16Reg(GOP_4G_DRAM_VSTR_H(u8Idx),0, GOP_REG_WORD_MASK);
                _HAL_GOP_Write16Reg(GOP_4G_DRAM_HSTR(u8Idx),0, GOP_REG_WORD_MASK);
                _HAL_GOP_Write16Reg(GOP_4G_DRAM_HVSTOP_L(u8Idx),0, GOP_REG_WORD_MASK);
                _HAL_GOP_Write16Reg(GOP_4G_DRAM_HVSTOP_H(u8Idx),0, GOP_REG_WORD_MASK);
        }
    }
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, 0x0, GOP_BIT2);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VIPVOP_BLENDING, GOP_BIT10, GOP_BIT10);
}

void _MDrv_GOP_EnableTransClr(MS_U8 u8GOP, BOOL bEn, MS_U32 clr)
{
    MS_U16 regval;
    MS_U32 u32BankOffSet = 0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &u32BankOffSet);
    regval = (MS_U16)(clr & 0xffff);
    _HAL_GOP_Write16Reg(u32BankOffSet + GOP_4G_TRSCLR_L, regval, GOP_REG_WORD_MASK);

    regval = (MS_U16)((clr >> 16 & 0xff));
    _HAL_GOP_Write16Reg(u32BankOffSet + GOP_4G_TRSCLR_H, regval, GOP_REG_WORD_MASK);

    _HAL_GOP_Write16Reg(u32BankOffSet+GOP_4G_CTRL0, (bEn == TRUE) ? GOP_BIT11 : 0x0, GOP_BIT11);
}

void _MDrv_GOP_SetStretchWin(U8 u8GOP,S16 x, S16 y, U16 width, U16 height)
{
    U32 bankoffset=0;
    U16 u16GopCropWinX0 = 0x0;
    U16 u16GopCropWinY0 = 0x0;
    U16 u16GopCropWinX1 = 0x0;
    U16 u16GopCropWinY1 = 0x0;
    U16 u16dstScalingW = 0x0;
    U16 u16GopHRatio = 0x0;
    U16 u16dstScalingH = 0x0;
    U16 u16GopVRatio = 0x0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);

    _MDrv_GOP_GetHScale(u8GOP, &u16GopHRatio);
    _MDrv_GOP_GetVScale(u8GOP, &u16GopVRatio);
    if (u16GopHRatio > 0)
    {
        u16dstScalingW = ((U32)width * 0x1000)/u16GopHRatio;
    }
    else
    {
        u16dstScalingW = width;
    }

    if (u16GopVRatio > 0)
    {
        u16dstScalingH = ((U32)height * 0x1000)/u16GopVRatio;
    }
    else
    {
        u16dstScalingH = height;
    }

    if (x < 0) {
        u16GopCropWinX0 = 0;
        u16GopCropWinX1 = u16GopCropWinX0 + u16dstScalingW + x;
        x = 0;
    }  else {
        u16GopCropWinX0 = 0x0;
        u16GopCropWinX1 = g_u16PanelWidth;
    }

    if (y < 0) {
        u16GopCropWinY0 = 0;
        u16GopCropWinY1 = u16GopCropWinY0 + u16dstScalingH + y;
        y = 0x0;
    } else {
        u16GopCropWinY0 = 0x0;
        u16GopCropWinY1 = g_u16PanelHeight;
    }

    _HAL_GOP_Write16Reg(bankoffset + GOP_4G_STRCH_HSTR, x, GOP_REG_WORD_MASK);
    _HAL_GOP_Write16Reg(bankoffset + GOP_4G_STRCH_VSTR, y , GOP_REG_WORD_MASK);
    _HAL_GOP_Write16Reg(bankoffset + GOP_4G_STRCH_HSZ, (width/GOP_STRETCH_WIDTH_UNIT) , GOP_REG_WORD_MASK);
    _HAL_GOP_Write16Reg(bankoffset + GOP_4G_STRCH_VSZ, height , GOP_REG_WORD_MASK);
    _HAL_GOP_Write16Reg(bankoffset + GOP_4G_RDMA_HT, (width+3)/2, 0x07ff);

    _MDrv_GOP_SetCropWin(
        u8GOP,
        u16GopCropWinX0, u16GopCropWinX1,
        u16GopCropWinY0, u16GopCropWinY1);
}

void _MDrv_GOP_EnableCropWin(U8 u8GOP, BOOL bEnable)
{
    U32 bankoffset=0;
    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);
    if (bEnable)
    {
        _HAL_GOP_Write16Reg(bankoffset + GOP_4G_BANK_FWR, GOP_BIT4, GOP_BIT4);
    }
    else
    {
        _HAL_GOP_Write16Reg(bankoffset + GOP_4G_BANK_FWR, 0x0, GOP_BIT4);
    }
}

void _MDrv_GOP_SetCropWin(U8 u8GOP, U16 x0, U16 x1, U16 y0, U16 y1)
{
    U32 bankoffset=0;
    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);
        _HAL_GOP_Write16Reg(bankoffset + GOP_4G_CROP_X0, x0, GOP_REG_WORD_MASK);
        _HAL_GOP_Write16Reg(bankoffset + GOP_4G_CROP_X1, x1, GOP_REG_WORD_MASK);
        _HAL_GOP_Write16Reg(bankoffset + GOP_4G_CROP_Y0, y0, GOP_REG_WORD_MASK);
        _HAL_GOP_Write16Reg(bankoffset + GOP_4G_CROP_Y1, y1, GOP_REG_WORD_MASK);
    }

void _MDrv_GOP_SetHScale(U8 u8GOP,U8 bEnable, U16 src, U16 dst )
{
    U32 hratio =0x1000,bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);

    if (bEnable)
    {
        hratio = (U32)(src) * SCALING_MULITPLIER;
        hratio /= (U32)dst;
    }
    else
        hratio = SCALING_MULITPLIER;

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HSTRCH, hratio , GOP_REG_WORD_MASK);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HSTRCH_INI, 0 , GOP_REG_WORD_MASK);

}

void _MDrv_GOP_SetVScale(U8 u8GOP,U8 bEnable, U16 src, U16 dst )
{
    U32 vratio =0x1000,bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);


    if (bEnable)
    {
        vratio = (U32)(src) * SCALING_MULITPLIER;
        vratio /= (U32)dst;
    }
    else
        vratio = SCALING_MULITPLIER;

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VSTRCH, vratio , GOP_REG_WORD_MASK);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VSTRCH_INI, 0 , GOP_REG_WORD_MASK);

}

void _MDrv_GOP_GetHScale(U8 u8GOP, U16 * pu16Ratio )
{
    U32 bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_HSTRCH, pu16Ratio);
}

void _MDrv_GOP_GetVScale(U8 u8GOP, U16 * pu16Ratio )
{
    U32 bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_VSTRCH, pu16Ratio);
}

void _MDrv_GOP_SetGwinInfo(U8 u8GOP,U8 u8win,DRV_GWIN_INFO * pWinInfo)
{
    U32 bankoffset=0,u32RingBuffer;
    U16 bpp;
    U16 u16tmp = 0x0;
    BOOL u16PixelBase = 0x0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);

    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_BANK_FWR, &u16tmp);
    u16PixelBase = (u16tmp & 0x80) ? TRUE : FALSE;

    bpp = _GOP_GetBPP(pWinInfo->clrType);
    if(bpp == 0xFFFF)
    {
        printk("[%s] invalud color format\n",__FUNCTION__);
        return;
    }

    if (u16PixelBase == FALSE)
    {
        pWinInfo->u32Addr = ALIGN_CHECK(pWinInfo->u32Addr,GOP_WordUnit);
        pWinInfo->u16Pitch = ALIGN_CHECK(pWinInfo->u16Pitch,(GOP_WordUnit/bpp));
        pWinInfo->u16HStart = ALIGN_CHECK(pWinInfo->u16HStart,(GOP_WordUnit/bpp));
        pWinInfo->u16HEnd = ALIGN_CHECK(pWinInfo->u16HEnd,(GOP_WordUnit/bpp));

        //Color Fmt
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_GWIN0_CTRL(u8win), pWinInfo->clrType <<4,0x00f0);
        //Address
        _HAL_GOP_Write32Reg(bankoffset+GOP_4G_DRAM_RBLK_L(u8win), pWinInfo->u32Addr/GOP_WordUnit);
        //Pitch
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_DRAM_RBLK_HSIZE(u8win), (pWinInfo->u16Pitch*bpp)/GOP_WordUnit , GOP_REG_WORD_MASK);
        //HStart
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HSTR(u8win), (pWinInfo->u16HStart*bpp)/GOP_WordUnit , GOP_REG_WORD_MASK);
        //HEnd
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HEND(u8win), (pWinInfo->u16HEnd*bpp)/GOP_WordUnit , GOP_REG_WORD_MASK);
        //VStart
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VSTR(u8win), pWinInfo->u16VStart , GOP_REG_WORD_MASK);
        //VEnd
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VEND(u8win), pWinInfo->u16VEnd , GOP_REG_WORD_MASK);
        //Ring Buffer
        u32RingBuffer = (pWinInfo->u16Pitch*bpp*(pWinInfo->u16VEnd - pWinInfo->u16VStart))/GOP_WordUnit;
        _HAL_GOP_Write32Reg(bankoffset+GOP_4G_DRAM_RBLK_SIZE_L(u8win), u32RingBuffer);
        //printk("\33[0;36m   %s:%d  \33[m \n",__FUNCTION__,__LINE__);
    }
    else
    {
//	        pWinInfo->u32Addr = ALIGN_CHECK(pWinInfo->u32Addr,GOP_WordUnit);
//	        pWinInfo->u16Pitch = ALIGN_CHECK(pWinInfo->u16Pitch,(GOP_WordUnit/bpp));
//	        pWinInfo->u16HStart = ALIGN_CHECK(pWinInfo->u16HStart,(GOP_WordUnit/bpp));
//	        pWinInfo->u16HEnd = ALIGN_CHECK(pWinInfo->u16HEnd,(GOP_WordUnit/bpp));

        //Color Fmt
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_GWIN0_CTRL(u8win), pWinInfo->clrType <<4,0x00f0);
        //Address
        _HAL_GOP_Write32Reg(bankoffset+GOP_4G_DRAM_RBLK_L(u8win), pWinInfo->u32Addr);
        //Pitch
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_DRAM_RBLK_HSIZE(u8win), pWinInfo->u16Pitch, GOP_REG_WORD_MASK);
        //HStart
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HSTR(u8win), pWinInfo->u16HStart , GOP_REG_WORD_MASK);
        //HEnd
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HEND(u8win), pWinInfo->u16HEnd , GOP_REG_WORD_MASK);
        //VStart
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VSTR(u8win), pWinInfo->u16VStart , GOP_REG_WORD_MASK);
        //VEnd
        _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VEND(u8win), pWinInfo->u16VEnd , GOP_REG_WORD_MASK);
        //Ring Buffer
        u32RingBuffer = (pWinInfo->u16Pitch*bpp*(pWinInfo->u16VEnd - pWinInfo->u16VStart))/GOP_WordUnit;
        _HAL_GOP_Write32Reg(bankoffset+GOP_4G_DRAM_RBLK_SIZE_L(u8win), u32RingBuffer);
        //printk("\33[0;36m   %s:%d  \33[m \n",__FUNCTION__,__LINE__);
    }
}

void _MDrv_GOP_SetBlending(U8 u8GOP, U8 u8win, U8 bEnable, U8 u8coef)
{
    _HAL_GOP_SetBlending(u8GOP, u8win, bEnable, u8coef);
}
void _MDrv_GOP_SetDstPlane(U8 u8GOP,DRV_GOPDstType eDstType)
{
    U32 bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);

    if (u8GOP==0)
        _HAL_GOP_Write16Reg(GOP_GOPCLK, CKG_GOPG0_ODCLK, CKG_GOPG0_MASK);
    else if (u8GOP==1)
        _HAL_GOP_Write16Reg(GOP_GOPCLK, CKG_GOPG1_ODCLK, CKG_GOPG1_MASK);
    else if (u8GOP==2)
        _HAL_GOP_Write16Reg(GOP_GOP2CLK, CKG_GOPG2_ODCLK, CKG_GOPG2_MASK);
    else if (u8GOP==3)
        _HAL_GOP_Write16Reg(GOP_GOP3CLK, CKG_GOPG3_ODCLK, CKG_GOPG2_MASK);

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL1, eDstType, 0x0007);

    _HAL_GOP_SetGOPEnable2SC(u8GOP,TRUE);
}
void _MDrv_GOP_GWIN_Enable(U8 u8GOP,U8 u8win,u8 bEnable)
{
    U32 bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_GWIN0_CTRL(u8win), bEnable, 0x1);
}
void _MDrv_GOP_EnableHMirror(U8 u8GOP,u8 bEnable)
{
    U32 bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, bEnable?GOP_BIT12:0, GOP_BIT12);
}
void _MDrv_GOP_EnableVMirror(U8 u8GOP,u8 bEnable)
{
    U32 bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_CTRL0, bEnable?GOP_BIT13:0, GOP_BIT13);
}
U8 _MDrv_GOP_GetMux(Gop_MuxSel eGopMux)
{
    U16 u16GopMux;
    _HAL_GOP_Read16Reg(GOP_MUX, &u16GopMux);
    return (U8)(u16GopMux >> ((eGopMux%4)*GOP_MUX_SHIFT))& GOP_REGMUX_MASK;

}
void _MDrv_GOP_SetMux(GOP_MuxConfig GopMuxConfig)
{
    U8 i;

    if(GopMuxConfig.u8MuxCounts > MAX_GOP_MUX)
    {
        printk("\nMuxCounts over MAX_GOP_MUX number\n");
        return;
    }

    for(i=0; i<GopMuxConfig.u8MuxCounts; i++)
    {
        _HAL_GOP_Write16Reg(GOP_MUX, GopMuxConfig.GopMux[i].u8GopIndex<<(GOP_MUX_SHIFT*GopMuxConfig.GopMux[i].u8MuxIndex), GOP_REGMUX_MASK<<(GOP_MUX_SHIFT*GopMuxConfig.GopMux[i].u8MuxIndex));
    }
}
void _MDrv_GOP_SetPipeDelay(U8 u8GOP,U16 u16PnlHstart)
{
    int i;
    u8 u8GOPNum = 0xFF;
    u16 u16Offset = 0;
    U32 bankoffset=0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);

    for (i=0; i<MAX_GOP_MUX; i++)
    {
        u8GOPNum = _MDrv_GOP_GetMux((Gop_MuxSel)i);
        if (u8GOP == u8GOPNum)
        {
            switch(i)
            {
                case 0:
                    u16Offset = u16PnlHstart + GOP_Mux0_Offset + GOP_PD;
                break;
                case 1:
                    u16Offset = u16PnlHstart + GOP_Mux1_Offset + GOP_PD;
                break;
                case 2:
                    u16Offset = u16PnlHstart + GOP_Mux2_Offset + GOP_PD;
                break;
                case 3:
                    u16Offset = u16PnlHstart + GOP_Mux3_Offset + GOP_PD;
                break;
                default:
                break;
            }
            _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HS_PIPE, u16Offset, 0x3FF);
        }
    }
}
void _MDrv_GOP_Set_MIU_Sel(U8 u8GOP,E_DRV_GOP_SEL_TYPE miusel)
{
#if 1
    U32 bankoffset=0;
    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);

    if (miusel == E_DRV_GOP_SEL_MIU0)
        _HAL_GOP_Write16Reg(GOP_4G_MIU_INTERNAL_SEL + bankoffset, 0x0, 0x3F);
    else
        _HAL_GOP_Write16Reg(GOP_4G_MIU_INTERNAL_SEL + bankoffset, 0x15, 0x3F);
#else
    U16 mask_shift = 0;

    switch(u8GOP)
    {
        case E_GOP0:
            mask_shift = GOP_MIU_CLIENT_GOP0;
            break;
        case E_GOP1:
            mask_shift = GOP_MIU_CLIENT_GOP1;
            break;
        case E_GOP2:
            mask_shift = GOP_MIU_CLIENT_GOP2;
            break;
        case E_GOP3:
            mask_shift = GOP_MIU_CLIENT_GOP3;
            break;
        case E_GOP_Dwin:
            mask_shift = GOP_MIU_CLIENT_DWIN;
            break;
        default:
            mask_shift = 0xFF;
    }

    if(mask_shift == 0xFF)

    _HAL_GOP_Write16Reg(GOP_MIU_GROUP1, miusel<<mask_shift, 1<<mask_shift );  //set GOP DMA Burst length to "32"
#endif
}

E_DRV_GOP_SEL_TYPE _MDrv_GOP_Get_MIU_Sel(U8 u8GOP)
{

#if 1
    return E_DRV_GOP_SEL_MIU1;
#else

    U16 u16tmp = 0x0;
    U16 mask_shift = 0;
    E_DRV_GOP_SEL_TYPE miu;

    switch(u8GOP)
    {
        case E_GOP0:
            mask_shift = GOP_MIU_CLIENT_GOP0;
            break;
        case E_GOP1:
            mask_shift = GOP_MIU_CLIENT_GOP1;
            break;
        case E_GOP2:
            mask_shift = GOP_MIU_CLIENT_GOP2;
            break;
        case E_GOP3:
            mask_shift = GOP_MIU_CLIENT_GOP3;
            break;
        case E_GOP_Dwin:
            mask_shift = GOP_MIU_CLIENT_DWIN;
            break;
        default:
            mask_shift = 0xFF;
    }

    if(mask_shift == 0xFF)
        printk("ERROR gop miu client\n");

    _HAL_GOP_Read16Reg(GOP_MIU_GROUP1, &u16tmp);

    if ((u16tmp & (1 << mask_shift)) > 0)
    {
        miu = E_DRV_GOP_SEL_MIU1;
    }
    else
    {
        miu = E_DRV_GOP_SEL_MIU0;
    }
    return miu;
#endif

}

signed long MDrv_GOP_CreateMemoryPool(unsigned long u32PoolSize,
                           unsigned long u32MinAllocation,
                           void * pPoolAddr,
                           char *pPoolName)
{
    signed long s32Id;

    MUTEX_MUTEX_LOCK();
    for(s32Id=0;s32Id<MSOS_MEMPOOL_MAX;s32Id++)
    {
        if(gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32Id].bUsed == FALSE)
        {
            break;
        }
    }

    if(s32Id < MSOS_MEMPOOL_MAX)
    {
        gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32Id].bUsed = TRUE;
    }
    MUTEX_MUTEX_UNLOCK();

    if(s32Id >= MSOS_MEMPOOL_MAX)
    {
        return -1;
    }


    if (pPoolAddr)
    {
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
        printk("[%s] Pool: %s Size:%lx Addr:%lx\n",__FUNCTION__,pPoolName,u32PoolSize,(long unsigned int)(pPoolAddr));
#else
        printk("[%s] Pool: %s Size:%lx Addr:%lx\n",__FUNCTION__,pPoolName,u32PoolSize,(pPoolAddr));
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */

        gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32Id].bMPool= TRUE;

        MUTEX_MUTEX_LOCK();

        gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32Id].stMemoryPool = mstar_create_mspace_with_base(pPoolAddr, u32PoolSize, 0);

        if (NULL == gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32Id].stMemoryPool)
        {
            MUTEX_MUTEX_UNLOCK();
            return -1;
        }
        MUTEX_MUTEX_UNLOCK();
    }

    if (bFirstCreate == 0 && bIsResume == 0)
    {
        //printk("[%s][%d] bFirstCreate = %d, bIsResume = %d\n",__FUNCTION__,__LINE__,bFirstCreate,bIsResume);
        memset(&g_mpool_cmd_record,0x0,sizeof(DRV_MPOOL_RECORD)*MAX_FB_CMD_RECORD );
        bFirstCreate = 1;
        u32CuurentRecordIdx = 0;
    }
    return s32Id;

}

BOOL MDrv_GOP_DeleteMemoryPool (signed long s32PoolId)
{
    {
        s32PoolId &= MSOS_ID_MASK;
    }

    if ( (s32PoolId >= MSOS_MEMPOOL_MAX) )
    {
        if ( s32PoolId == MSOS_MALLOC_ID )
        {
            // herer ??
        }

        printk("Invalid memory pool ID:%ld, you must use the ID of the mpool you created\n",s32PoolId);
        return TRUE;
    }

    if(gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32PoolId].bUsed == FALSE)
    {
        printk("MEMORY POOL WITH MEMORYPOOL_ID:0x%lx NOT EXIST\n",s32PoolId);
        return FALSE;
    }
    if (gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32PoolId].bMPool)
    {
        MUTEX_MUTEX_LOCK();
        mstar_destroy_mspace(gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32PoolId].stMemoryPool);
        MUTEX_MUTEX_UNLOCK();
    }


    gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32PoolId].bUsed = FALSE;

    return TRUE;
}

U32 MDrv_GOP_AllocMemory(unsigned long u32Size, U32 s32PoolId, U32 align_unit)
{
    void  *pAddr;

    {
        s32PoolId &= MSOS_ID_MASK;
    }


    if (u32Size == 0)
    {
        printk("[%s] invalid size :%lx\n",__FUNCTION__,u32Size);
        return GOP_INVALID_ADDR;
    }


    if ( s32PoolId >= MSOS_MEMPOOL_MAX)
    {
        printk("Invalid memory pool ID:%d, you must use the default ID: MSOS_MALLOC_ID ,or the ID of the mpool you created\n",s32PoolId);
        return GOP_INVALID_ADDR;
    }

    if (FALSE== gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32PoolId].bMPool)
    {
        printk("Invalid memory pool ID:%d, you must use the default ID: MSOS_MALLOC_ID ,or the ID of the mpool you created\n",s32PoolId);
        printk("System will use default mpool to allocate memory here\n");
        //return (U32)kmalloc(u32Size);
        return GOP_INVALID_ADDR;
    }

    //pAddr = mstar_mspace_malloc(v[s32PoolId].stMemoryPool, u32Size);

    MUTEX_MUTEX_LOCK();

    //pAddr = mstar_mspace_malloc(gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32PoolId].stMemoryPool, u32Size);
    pAddr = mstar_mspace_memalign(gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32PoolId].stMemoryPool,align_unit, u32Size);

    //Chip_Flush_Cache_All();

    if(pAddr == 0x0)
    {
        printk("\n\n\n [%s] Error \n\n\n",__FUNCTION__);

    }



    if (( (unsigned long)pAddr & 0xF) || ( (unsigned long)pAddr == 0x0))
    {
        MUTEX_MUTEX_UNLOCK();
        return GOP_INVALID_ADDR;
    }
    if (bIsResume == 0)
    {
        if (u32CuurentRecordIdx >= MAX_FB_CMD_RECORD )
        {
            //printk("%s(),%d,u32CuurentRecordIdx >= MAX_FB_CMD_RECORD \n",__FUNCTION__,__LINE__);
        }
        else
        {
            g_mpool_cmd_record[u32CuurentRecordIdx].u32Addr = (U32)pAddr;
            g_mpool_cmd_record[u32CuurentRecordIdx].u32Size = u32Size;
            g_mpool_cmd_record[u32CuurentRecordIdx].u32PoolID = s32PoolId;
            g_mpool_cmd_record[u32CuurentRecordIdx].u32CMD = 0;
            g_mpool_cmd_record[u32CuurentRecordIdx].align_unit= align_unit;
            u32CuurentRecordIdx++;
        }
    }
    MUTEX_MUTEX_UNLOCK();
    return  (U32)pAddr;

}

BOOL MDrv_GOP_FreeMemory (void *pAddress,signed long s32PoolId)
{

    s32PoolId &= MSOS_ID_MASK;

    if (pAddress == NULL)
    {
        return FALSE;
    }

    if ( s32PoolId >= MSOS_MEMPOOL_MAX )
    {
        {
            printk("Invalid memory pool ID:%ld, you must use the default ID: MSOS_MALLOC_ID ,or the ID of the mpool you created\n",s32PoolId);
            return FALSE;
        }

    }


    MUTEX_MUTEX_LOCK();

    mstar_mspace_free( gstMdrvGopDataHandle.GOP_MemoryPool_Info[s32PoolId].stMemoryPool, pAddress);
    if (bIsResume == 0)
    {
        if (u32CuurentRecordIdx >= MAX_FB_CMD_RECORD )
        {
            //printk("%s(),%d,u32CuurentRecordIdx >= MAX_FB_CMD_RECORD \n",__FUNCTION__,__LINE__);
        }
        else
        {
            g_mpool_cmd_record[u32CuurentRecordIdx].u32Addr = (U32)pAddress;
            g_mpool_cmd_record[u32CuurentRecordIdx].u32Size = 0;
            g_mpool_cmd_record[u32CuurentRecordIdx].u32PoolID = s32PoolId;
            g_mpool_cmd_record[u32CuurentRecordIdx].u32CMD = 1;
            g_mpool_cmd_record[u32CuurentRecordIdx].align_unit= 0;
            u32CuurentRecordIdx++;
        }
    }
    MUTEX_MUTEX_UNLOCK();
    return TRUE;
}


#ifdef LGX_MGW_FUNCTIONALITY

static void _MDrv_GOP_SetPipe(U8 u8GOP, U16 u16Pipe)
{
    U32 bankoffset=0;
    U16 u16HstartReg=0;

    if(u8GOP >= MAX_GOP_SUPPORT)
    {
        printk("[%s] not support gop%d in this chip version!!\n",__FUNCTION__, u8GOP);
        return;
    }
    _HAL_GOP_GetBnkOfstByGop(u8GOP,&bankoffset);

    // need to check xc mute or it will show some garbage
    _HAL_GOP_Read16Reg(REG_XC_OP_HSTART, &u16HstartReg);
    u16Pipe += (u16HstartReg & 0x3FFF);

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HS_PIPE, u16Pipe, 0x3FF);
}

void MDrv_GOP_Start_MGW(
    U32 u32DispAddr, U32 u32DispSz,
    U16 u16MgwFBWidth, U16 u16MgwFBHeight, U16 u16MgwFBPitch, U16 u16MgwFBFmt,
    U16 u16MgwOnScreenWidth, U16 u16MgwOnScreenHeight,
    U16 u16MgwScalingRatio)
{
    GE_Rect  geClipWin;
    E_DRV_GOP_SEL_TYPE u16GopMiu = E_DRV_GOP_SEL_MIU0;
    U16 u16Bpp;

    g_u16MgwScalingRatio = u16MgwScalingRatio;
    MgwOnScreenWidth = u16MgwOnScreenWidth;
    MgwOnScreenHeight = u16MgwOnScreenHeight;
    g_bMGWChangSourceCropWin = TRUE;
    gbSkipDisplayFrameCount = 0x0;
    g_u16MgwFbPitch = u16MgwFBPitch;
    g_u32MgwFbSize = u32DispSz * ((u16MgwFBFmt == 8888) ? 4 : 2);

    g_u16CurAppCursorPosX = (SC_OP_TIMING_WIDTH >> 1);
    g_u16CurAppCursorPosY = (SC_OP_TIMING_HEIGHT >> 1);

//	printk("[%s, %d]: u32DispAddr %d \r\n", __FUNCTION__, __LINE__, u32DispAddr);
//	printk("[%s, %d]: u16MgwOnScreenWidth %d \r\n", __FUNCTION__, __LINE__, u16MgwOnScreenWidth);
//	printk("[%s, %d]: u16MgwOnScreenHeight %d \r\n", __FUNCTION__, __LINE__, u16MgwOnScreenHeight);
//	printk("[%s, %d]: u16MgwFBWidth %d \r\n", __FUNCTION__, __LINE__, u16MgwFBWidth);
//	printk("[%s, %d]: u16MgwFBHeight %d \r\n", __FUNCTION__, __LINE__, u16MgwFBHeight);
//	printk("[%s, %d]: u16MgwFBPitch %d \r\n", __FUNCTION__, __LINE__, u16MgwFBPitch);
//	printk("[%s, %d]: u16MgwScalingRatio %d \r\n", __FUNCTION__, __LINE__, u16MgwScalingRatio);
//	printk("[%s, %d]: g_u32MgwFbSize %d \r\n", __FUNCTION__, __LINE__, g_u32MgwFbSize);
//	printk("[%s, %d]: u16MgwFBFmt %d \r\n", __FUNCTION__, __LINE__, u16MgwFBFmt);

    g_mgs_disp_buf[0].u16MGW_DispBuffFmt = g_mgs_disp_buf[1].u16MGW_DispBuffFmt =
        (u16MgwFBFmt == 8888) ? E_DRV_GOP_COLOR_ARGB8888 : E_DRV_GOP_COLOR_YUV422;
    g_mgs_disp_buf[0].u32MGW_DispBuffAddr = u32DispAddr;
    g_mgs_disp_buf[1].u32MGW_DispBuffAddr = u32DispAddr;
    g_mgs_disp_buf[0].bDispIng = g_mgs_disp_buf[1].bDispIng = FALSE;
    g_mgs_disp_buf[0].u16MGW_DispBuffWidth = g_mgs_disp_buf[1].u16MGW_DispBuffWidth = u16MgwFBWidth;
    g_mgs_disp_buf[0].u16MGW_DispBuffHeight = g_mgs_disp_buf[1].u16MGW_DispBuffHeight = u16MgwFBHeight;

//	printk("[%s, %d]: u32MGW_DispBuffAddr = 0x%x, 0x%x \r\n", __FUNCTION__, __LINE__, g_mgs_disp_buf[0].u32MGW_DispBuffAddr, g_mgs_disp_buf[1].u32MGW_DispBuffAddr);
//	printk("[%s, %d]: u16MGW_DispBuffFmt %d \r\n", __FUNCTION__, __LINE__, g_mgs_disp_buf[0].u16MGW_DispBuffFmt);

    _MDrv_GOP_Init(MGW_GOP_ID);
    _MDrv_GOP_SetDstPlane(MGW_GOP_ID,E_DRV_GOP_DST_OP0);
    _MDrv_GOP_SetStretchWin(MGW_GOP_ID, 0, 0, u16MgwFBWidth, u16MgwFBHeight);
    _MDrv_GOP_SetHScale(MGW_GOP_ID, TRUE, u16MgwFBWidth, MgwOnScreenWidth);
    _MDrv_GOP_SetVScale(MGW_GOP_ID, TRUE, u16MgwFBHeight, MgwOnScreenHeight);
    _MDrv_GOP_EnableTransClr(MGW_GOP_ID, TRUE, 0x12345678);
    _MDrv_GOP_SetBlending(MGW_GOP_ID, MGW_GWIN_ID, TRUE, 0xFF);
    u16GopMiu = (u32DispAddr & 0x80000000) ? E_DRV_GOP_SEL_MIU1 : E_DRV_GOP_SEL_MIU0;
    _MDrv_GOP_Set_MIU_Sel(MGW_GOP_ID, u16GopMiu);
    _MDrv_GOP_EnableCropWin(MGW_GOP_ID, TRUE);

    _MDrv_GOP_SetPipe(MGW_GOP_ID, gu16MgwScGopBlendingDelay); // hard code patch, somebody would change the pipe before MGW on

    MDrv_KGE_Init();
    geClipWin.x = geClipWin.y = 0x0;
    geClipWin.width = u16MgwFBWidth;
    geClipWin.height = u16MgwFBHeight;
    MDrv_GE_SetClipWindow(g_apiGFXLocal.g_pGEContext, &geClipWin);
    MDrv_GE_SetAlphaSrc(g_apiGFXLocal.g_pGEContext, E_GE_ALPHA_CONST);
    MDrv_GE_SetAlphaConst(g_apiGFXLocal.g_pGEContext, 0xFF);
    MDrv_GE_EnableTagIntr(g_apiGFXLocal.g_pGEContext, TRUE);
    MDrv_GE_SetDstColorKey(g_apiGFXLocal.g_pGEContext, TRUE, E_GE_ALPHA_EQ, 0x12345678, 0x12345678);
    MDrv_GE_MaskIRQ(g_apiGFXLocal.g_pGEContext, 0, TRUE);
    MDrv_GE_MaskIRQ(g_apiGFXLocal.g_pGEContext, 1, FALSE);

#ifdef MSTARFB_DIP_INTERRUPT
#if (DIP_INTR_IN_SC == 0)
    int ret;
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
    /* ISO C90 forbids mixed declarations and code */
    struct irq_test *ip = (struct irq_test *) kmalloc(sizeof *ip, GFP_KERNEL);
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */

    static bool bDipIsrRunning = FALSE;

    if (bDipIsrRunning == FALSE)
    {
        ret = (request_threaded_irq(E_IRQEXPL_DIPW, _irq_top, MDrv_MGW_IntHandler, IRQF_SHARED | IRQF_ONESHOT | SA_INTERRUPT, "MGW_DIP", ip));
        if(0 != ret)
        {
            printk("DIP INT Handler init fail [%d]\n",ret);
            printk("[GFLIP] Fail to request IRQ:%d\n", E_IRQEXPL_DIPW);
            return EFAULT;
        }
        bDipIsrRunning = TRUE;
    }
#endif
#endif

    g_bMGWRunning = TRUE; // set enable at last step
}

void MDrv_GOP_Stop_MGW(void)
{
    unsigned long flags;

    if (g_bMGWRunning == FALSE)
        return ;

    spin_lock_irqsave(&mgw_lock, flags);
    g_bMGWRunning = FALSE;
    spin_unlock_irqrestore(&mgw_lock, flags);

    _MDrv_GOP_GWIN_Enable(MGW_GOP_ID, MGW_GWIN_ID, 0);
    _MDrv_GOP_EnableCropWin(MGW_GOP_ID, FALSE);
    _MDrv_GOP_UpdateReg(MGW_GOP_ID);

    MgwOnScreenWidth = 0x0;
    MgwOnScreenHeight = 0x0;
    g_mgs_disp_buf[0].u32MGW_DispBuffAddr = g_mgs_disp_buf[1].u32MGW_DispBuffAddr = 0x0;
    g_mgs_disp_buf[0].bDispIng = g_mgs_disp_buf[1].bDispIng = FALSE;
    g_mgs_disp_buf[0].u16MGW_DispBuffWidth = g_mgs_disp_buf[1].u16MGW_DispBuffWidth = 0x0;
    g_mgs_disp_buf[0].u16MGW_DispBuffHeight = g_mgs_disp_buf[1].u16MGW_DispBuffHeight = 0x0;

    MDrv_GE_MaskIRQ(g_apiGFXLocal.g_pGEContext, 0, TRUE);
    MDrv_GE_MaskIRQ(g_apiGFXLocal.g_pGEContext, 1, TRUE);

    msleep(30); // to wait GOP vsync, OP timing would be 50/60, delay 30ms should be enough
}

void MDrv_GOP_MGW_SetPos(U16 u16PosX, U16 u16PosY)
{
    unsigned long flags;
    spin_lock_irqsave(&mgw_lock, flags);
    g_u16CurAppCursorPosX = u16PosX;
    g_u16CurAppCursorPosY = u16PosY;
    //g_bMGWChangSourceCropWin = TRUE;
    spin_unlock_irqrestore(&mgw_lock, flags);
}

void MDrv_GOP_MGW_SetSz(U16 u16MgwW, U16 u16MgwH)
{
    unsigned long flags;
    spin_lock_irqsave(&mgw_lock, flags);
    MgwOnScreenWidth = u16MgwW;
    MgwOnScreenHeight = u16MgwH;
    //g_bMGWChangSourceCropWin = TRUE;
    spin_unlock_irqrestore(&mgw_lock, flags);
}

void MDrv_GOP_MGW_SetRatio(U16 u16Ratio)
{
    unsigned long flags;
    spin_lock_irqsave(&mgw_lock, flags);
    g_u16MgwScalingRatio = u16Ratio;
    //g_bMGWChangSourceCropWin = TRUE;
    spin_unlock_irqrestore(&mgw_lock, flags);
}

void MDrv_GOP_MGW_SetEnable(U16 ben)
{
    int nSleepIdx = 0x0;

    while (g_bMGWChangSourceCropWin == TRUE)
    {
        if (nSleepIdx > 20)
        {
            break;
        }
        nSleepIdx++;
        msleep(1);
    }

    if (ben == TRUE)
    {
        g_bMGWChangSourceCropWin = TRUE;

        gu16ChangeMgwEnState = TRUE;
        gu16MgwEnState = TRUE;
    }
    else
    {
        _MDrv_GOP_GWIN_Enable(MGW_GOP_ID, MGW_GWIN_ID, ben);
        gu16MgwEnState = FALSE;
    }
}

U16 MHal_DIP_getIntrStatus(U16 eDipWindow)
{
    U16 u16RegVal;

    if (eDipWindow == 0x0)
    {
        _HAL_GOP_Read16Reg(DIP_INTR_STATUS, &u16RegVal);
    }
    else if (eDipWindow == 0x1)
    {
        _HAL_GOP_Read16Reg(DWIN0_INTR_STATUS, &u16RegVal);
    }
    else
    {
        _HAL_GOP_Read16Reg(DWIN1_INTR_STATUS, &u16RegVal);
    }
    return (u16RegVal & 0xFF);
}

void MHal_DIP_ClearIntrStatus(U16 eDipWindow)
{
    if (eDipWindow == 0x0)
    {
        _HAL_GOP_Write16Reg(DIP_INTR_CLR, 0xFF, 0xFF);
    }
    else if (eDipWindow == 0x1)
    {
        _HAL_GOP_Write16Reg(DWIN0_INTR_CLR, 0xFF, 0xFF);
    }
    else
    {
        _HAL_GOP_Write16Reg(DWIN1_INTR_CLR, 0xFF, 0xFF);
    }
}

U16 MHal_DIP_SetCropWindow(U16 eDipWindow, U16 u16CropPosX0, U16 u16CropPosY0, U16 u16CropPosX1, U16 u16CropPosY1)
{
    U16 u16RegVal = 0;

    if (eDipWindow == 0x0)
    {
        _HAL_GOP_Write16Reg(DIP_CROP_WIN_X0, u16CropPosX0+1, 0xFFF);
        _HAL_GOP_Write16Reg(DIP_CROP_WIN_X1, u16CropPosX1, 0xFFF);
        _HAL_GOP_Write16Reg(DIP_CROP_WIN_Y0, u16CropPosY0+1, 0xFFF);
        _HAL_GOP_Write16Reg(DIP_CROP_WIN_Y1, u16CropPosY1, 0xFFF);

        _HAL_GOP_Write16Reg(DIP_CAPT_HEIGHT, u16CropPosY1-u16CropPosY0, 0xFFF);
    }
    else if (eDipWindow == 0x1)
    {
        _HAL_GOP_Write16Reg(DWIN0_CROP_WIN_X0, u16CropPosX0+1, 0xFFF);
        _HAL_GOP_Write16Reg(DWIN0_CROP_WIN_X1, u16CropPosX1, 0xFFF);
        _HAL_GOP_Write16Reg(DWIN0_CROP_WIN_Y0, u16CropPosY0+1, 0xFFF);
        _HAL_GOP_Write16Reg(DWIN0_CROP_WIN_Y1, u16CropPosY1, 0xFFF);

        _HAL_GOP_Write16Reg(DWIN0_CAPT_HEIGHT, u16CropPosY1-u16CropPosY0, 0xFFF);
    }
    else
    {
        _HAL_GOP_Write16Reg(DWIN1_CROP_WIN_X0, u16CropPosX0+1, 0xFFF);
        _HAL_GOP_Write16Reg(DWIN1_CROP_WIN_X1, u16CropPosX1, 0xFFF);
        _HAL_GOP_Write16Reg(DWIN1_CROP_WIN_Y0, u16CropPosY0+1, 0xFFF);
        _HAL_GOP_Write16Reg(DWIN1_CROP_WIN_Y1, u16CropPosY1, 0xFFF);

        _HAL_GOP_Write16Reg(DWIN1_CAPT_HEIGHT, u16CropPosY1-u16CropPosY0, 0xFFF);
    }
    return (u16RegVal & 0xFF);
}

void MHal_DIP_SingleShotTrigger(U16 eDipWindow)
{
    if (eDipWindow == 0x0)
    {
        _HAL_GOP_Write16Reg(DIP_CAPT_TRIGGER, 0x300, 0x300);
    }
    else if (eDipWindow == 0x1)
    {
        _HAL_GOP_Write16Reg(DWIN0_CAPT_TRIGGER, 0x300, 0x300);
    }
    else
    {
        _HAL_GOP_Write16Reg(DWIN1_CAPT_TRIGGER, 0x300, 0x300);
    }
}

U16 MHal_DIP_SetCaptureSize(U16 eDipWindow, U16 u16CaptureW, U16 u16CaptureH)
{
    U16 u16Ret = 0;

    if (eDipWindow == 0x0)
    {
        _HAL_GOP_Write16Reg(DIP_CAPT_WIDTH, u16CaptureW, 0xFFFF);
        _HAL_GOP_Write16Reg(DIP_CAPT_HEIGHT, u16CaptureH, 0xFFFF);
    }
    else if (eDipWindow == 0x1)
    {
        _HAL_GOP_Write16Reg(DWIN0_CAPT_WIDTH, u16CaptureW, 0xFFFF);
        _HAL_GOP_Write16Reg(DWIN0_CAPT_HEIGHT, u16CaptureH, 0xFFFF);
    }
    else
    {
        _HAL_GOP_Write16Reg(DWIN1_CAPT_WIDTH, u16CaptureW, 0xFFFF);
        _HAL_GOP_Write16Reg(DWIN1_CAPT_HEIGHT, u16CaptureH, 0xFFFF);
    }
    return u16Ret;
}

U16 MDrv_DIP_getIntrStatus(U16 eDipWindow)
{
    return MHal_DIP_getIntrStatus(eDipWindow);
}

void MDrv_DIP_ClearIntrStatus(U16 eDipWindow)
{
    MHal_DIP_ClearIntrStatus(eDipWindow);
}

void MDrv_DIP_setCropWindow(U16 eDipWindow, U16 u16CropPosX0, U16 u16CropPosY0, U16 u16CropPosX1, U16 u16CropPosY1)
{
    MHal_DIP_SetCropWindow(eDipWindow, u16CropPosX0, u16CropPosY0, u16CropPosX1, u16CropPosY1);
}

void MDrv_DIP_SingleShotTrigger(U16 eDipWindow)
{
    MHal_DIP_SingleShotTrigger(eDipWindow);
}

void MDrv_DIP_SetCaptureSize(U16 eDipWindow, U16 u16CW, U16 u16CH)
{
    u16CW = (u16CW + (gu16DipAlignPixel - 1)) & (~(gu16DipAlignPixel - 1));
    u16CW = (u16CW < DIP_LIMIT_WRITE_MINIMUM_LENGTH) ? DIP_LIMIT_WRITE_MINIMUM_LENGTH : u16CW;
    MHal_DIP_SetCaptureSize(eDipWindow, u16CW, u16CH);
}

static void _S_MDrv_MGW_SetCoor2Dummy(U16 cx, U16 cy, U16 cw, U16 ch, U16 ratio, U16 mgw_w, U16 mgw_h)
{
    U32 bankoffset=0;
    _HAL_GOP_GetBnkOfstByGop(MGW_GOP_ID, &bankoffset);

    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_DRAM_RBLK_L(1), cx, 0xFFFF);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_DRAM_RBLK_H(1), cy, 0xFFFF);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HEND(1), cw, 0xFFFF);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_VEND(1), ch, 0xFFFF);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_HSTR(1), ratio, 0xFFFF);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_DRAM_VSTR_L(1), mgw_w, 0xFFFF);
    _HAL_GOP_Write16Reg(bankoffset+GOP_4G_DRAM_VSTR_H(1), mgw_h, 0xFFFF);
}

static void _S_MDrv_MGW_GetCoorFromDummy(U16 * px, U16 * py, U16 * pw, U16 * ph, U16 * pratio, U16 * mgw_w, U16 * mgw_h)
{
    U32 bankoffset = 0x0;
    U16 u16RegTmp = 0x0;
    _HAL_GOP_GetBnkOfstByGop(MGW_GOP_ID, &bankoffset);

    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_DRAM_RBLK_L(1), &u16RegTmp);
    *px = u16RegTmp;
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_DRAM_RBLK_H(1), &u16RegTmp);
    *py = u16RegTmp;
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_HEND(1), &u16RegTmp);
    *pw = u16RegTmp;
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_VEND(1), &u16RegTmp);
    *ph = u16RegTmp;
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_HSTR(1), &u16RegTmp);
    *pratio = u16RegTmp;
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_DRAM_VSTR_L(1), &u16RegTmp);
    *mgw_w = u16RegTmp;
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_DRAM_VSTR_H(1), &u16RegTmp);
    *mgw_h = u16RegTmp;
}

U16 _s_Get_Dip_LatestFrameIdx(U16 eWindow, U16 * pu16LatestIdx)
{
    U16 i = 0x0;
    U16 u16DipIntr = 0x0;

    u16DipIntr = MDrv_DIP_getIntrStatus(eWindow);
    if (u16DipIntr == 0)
    {
        return FALSE;
    }

    for (i=0; i<8; i++)
    {
        if ((u16DipIntr & (0x1 << i)) > 0)
        {
            *pu16LatestIdx = i;
            break;
        }
    }

    MDrv_DIP_ClearIntrStatus(eWindow);

    return u16DipIntr;
}

typedef struct
{
    U32 u32BufAddr;
    U16 u16BufWidth;
    U16 u16BufHeight;
    U16 u16BufPicth;
} kGfx_Buffer_Info;
#endif

irqreturn_t _gop_interrupt(int irq, void *devid)
{
    //MHal_GOP_ProcessIRQ();  scrolling test
    unsigned long flags;

    spin_lock_irqsave(&gop_lock, flags);
    if(g_bVsyncSleep)
    {
        //printk("[%s] wake up\n",__FUNCTION__);
        wake_up_interruptible(&(_fb_waitqueue[0]));
        g_bVsyncSleep = FALSE;
    }
    spin_unlock_irqrestore(&gop_lock, flags);

    MHal_GOP_MaskIRQ();
    disable_irq_nosync(E_IRQ_GOP);

    return IRQ_HANDLED;

}

U32 MDrv_GOP_VsyncWait(U8 u8Gop)
{
    U32 sleep_timeout =0;
    unsigned long flags;
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
    /* to replace the deprecated interruptible_sleep_on_timeout() */
    DEFINE_WAIT(wait);
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */

    spin_lock_irqsave(&gop_lock, flags);
    if (g_bVsyncSleep == FALSE)
    {
        MHal_GOP_ClearIRQ();
        enable_irq(E_IRQ_GOP);
        g_bVsyncSleep = TRUE;
    }
    spin_unlock_irqrestore(&gop_lock, flags);

#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
    /* replace the deprecated interruptible_sleep_on_timeout() with direct
       wait-queue usage. */
    prepare_to_wait(&(_fb_waitqueue[0]), &wait, TASK_INTERRUPTIBLE);
    sleep_timeout = schedule_timeout(msecs_to_jiffies(50));
    finish_wait(&(_fb_waitqueue[0]), &wait);
#else
    sleep_timeout = interruptible_sleep_on_timeout(&(_fb_waitqueue[0]), msecs_to_jiffies(50));
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */
    if(sleep_timeout > 50)
    {
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
        printk("[%s] timeout:%ld\n", "MDrv_GOP_VsyncWait", (long int)sleep_timeout);
#else
        printk("[%s] timeout:%ld\n",sleep_timeout);
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */
    }
    return 0;

}

#ifdef LGX_MGW_FUNCTIONALITY
#include "khalGE.h"
#include "kdrvGE.h"
#include "kdrvGE_private.h"

Mgw_CaptFrameInfo * _s_FindCaptureWindow(U16 tag, U32 srcAddr)
{
    U16 i;
    Mgw_CaptFrameInfo * pInfo = NULL;
    for (i=0; i<8; i++)
    {
        if (//(g_mgw_capt_buf[i].u16MGW_CaptTag == tag) ||
            (g_mgw_capt_buf[i].u32MGW_CaptBuffAddr == srcAddr)
            )
        {
            pInfo = &g_mgw_capt_buf[i];
        }
    }
    return pInfo;
}

static void _S_MDrv_MGW_GetNextCaptureCoordinate(
    U16   u16CurCursorCoorX,
    U16   u16CurCursorCoorY,
    U16   u16DipCapturedFrameWidth,
    U16   u16DipCapturedFrameHeight,
    U16 * pu16CropPosX0,
    U16 * pu16CropPosY0,
    U16 * pu16CropPosX1,
    U16 * pu16CropPosY1)
{
    U16 u16DipCropWinX0 = 0x0;
    U16 u16DipCropWinY0 = 0x0;
    U16 u16DipCropWinX1 = 0x0;
    U16 u16DipCropWinY1 = 0x0;
    U16 u16DipCapturedFrameHalfWidth = 0x0;
    U16 u16DipCapturedFrameHalfHeight = 0x0;

    const U16 u16CaptCoornateMaxH = 1920;
    const U16 u16CaptCoornateMaxV = 2160;

    u16DipCapturedFrameHalfWidth = ((u16DipCapturedFrameWidth+1) >> 1);
    u16DipCapturedFrameHalfHeight = ((u16DipCapturedFrameHeight+1) >> 1);

    if (u16CurCursorCoorX < u16DipCapturedFrameHalfWidth)
    {
        u16DipCropWinX0 = 0x0;
        u16DipCropWinX1 = u16DipCropWinX0 + u16DipCapturedFrameWidth;
    } else if (u16CurCursorCoorX + u16DipCapturedFrameHalfWidth > u16CaptCoornateMaxH)
    {
        u16DipCropWinX0 = u16CurCursorCoorX - u16DipCapturedFrameHalfWidth;
        u16DipCropWinX1 = u16CaptCoornateMaxH;
    } else
    {
        u16DipCropWinX0 = u16CurCursorCoorX - u16DipCapturedFrameHalfWidth;
        u16DipCropWinX1 = u16DipCropWinX0 + u16DipCapturedFrameWidth;
    }

    if (u16CurCursorCoorY < u16DipCapturedFrameHalfHeight)
    {
        u16DipCropWinY0 = 0x0;
        u16DipCropWinY1 = u16DipCropWinY0 + u16DipCapturedFrameHeight;
    } else if (u16CurCursorCoorY + u16DipCapturedFrameHalfHeight > u16CaptCoornateMaxV)
    {
        u16DipCropWinY0 = u16CurCursorCoorY - u16DipCapturedFrameHalfHeight;
        u16DipCropWinY1 = u16CaptCoornateMaxV;
    } else
    {
        u16DipCropWinY0 = u16CurCursorCoorY - u16DipCapturedFrameHalfHeight;
        u16DipCropWinY1 = u16DipCropWinY0 + u16DipCapturedFrameHeight;
    }

    *pu16CropPosX0 = u16DipCropWinX0;
    *pu16CropPosX1 = u16DipCropWinX1;
    *pu16CropPosY0 = u16DipCropWinY0;
    *pu16CropPosY1 = u16DipCropWinY1;
}

static void S_MDrv_Get_CaptureWinSize(
    U16 u16ApWantWidth,
    U16 u16ApWantHeight,
    U16 u16ScalingRatio,
    U16 *pu16DipCapturedFrameWidth,
    U16 *pu16DipCapturedFrameHeight,
    U16 *pu16DipCapturedFramePitch)
{
    U16 width, height, picth;

    width = ((U32)u16ApWantWidth * u16ScalingRatio) >> 13;
    width &= (~0x1);
    *pu16DipCapturedFrameWidth = width;

    height = ((U32)u16ApWantHeight * u16ScalingRatio) >> 12;
    *pu16DipCapturedFrameHeight = height;

    picth = (u16ApWantWidth + (gu16DipAlignPixel - 1)) & (~(gu16DipAlignPixel - 1));
    *pu16DipCapturedFramePitch = picth;
}

static void _S_MDrv_MGW_ConvertReal2CaptCoordinate(
    U16   u16RealWorldCoorX,
    U16   u16RealWorldCoorY,
    U16 * pu16CaptWorldX,
    U16 * pu16CaptWorldY
    )
{
    *pu16CaptWorldX = u16RealWorldCoorX >> 1;
    *pu16CaptWorldY = u16RealWorldCoorY;
}

#ifdef MSTARFB_DIP_INTERRUPT
irqreturn_t MDrv_MGW_IntHandler(int irq,void *devid)
#else
int MDrv_MGW_IntHandler(void)
#endif
{
    U16  u16MGW_DipLatestFrame = 0xFF;
    unsigned long flags;
    U16  u16IntrStatus = 0x0;
    U16  u16RegXcMute = 0x0;

    if (g_bMGWRunning == FALSE)
    {
        return IRQ_HANDLED;
    }

    u16IntrStatus = _s_Get_Dip_LatestFrameIdx(gMgwDipWindow, &u16MGW_DipLatestFrame);
    if (u16IntrStatus == 0x0)
    {
        return IRQ_HANDLED;
    }

    // need to check xc mute or it will show some garbage
    _HAL_GOP_Read16Reg(REG_XC_MUTE, &u16RegXcMute);
    if (u16RegXcMute & 0x2)
    {
        MDrv_DIP_SingleShotTrigger(gMgwDipWindow);
        return IRQ_HANDLED;
    }

    spin_lock_irqsave(&mgw_lock, flags);
    {
        U16 u16DipCapturedFrameWidth = 0x0;
        U16 u16DipCapturedFrameHeight = 0x0;
        U16 u16DipCapturedFramePitch = 0x0;
        DRV_GWIN_INFO drvGwin;
        S16 s16GopStretchWinX, s16GopStretchWinY;
        U16 u16CurCaptureCenterX = 0;
        U16 u16CurCaptureCenterY = 0;
        U16 u16OnscreenWidth = 0x0;
        U16 u16OnscreenHeight = 0x0;
        U16 u16OnscreenHalfWidth = 0x0;
        U16 u16OnscreenHalfHeight = 0x0;
        U16 u16SavedRatio = 0x0;

        _S_MDrv_MGW_GetCoorFromDummy(
            &u16CurCaptureCenterX,
            &u16CurCaptureCenterY,
            &u16DipCapturedFrameWidth,
            &u16DipCapturedFrameHeight,
            &u16SavedRatio,
            &u16OnscreenWidth,
            &u16OnscreenHeight);


        if ((u16OnscreenWidth > 0) &&
            (u16OnscreenHeight > 0)
            )
        {
            u16OnscreenHalfWidth = (u16OnscreenWidth>>1);
            u16OnscreenHalfHeight = (u16OnscreenHeight>>1);

            // set gwin size
            drvGwin.u16HStart = 0x0;
            drvGwin.u16HEnd = (u16DipCapturedFrameWidth & (~0x1));
            drvGwin.u16VStart = 0x0;
            drvGwin.u16VEnd = u16DipCapturedFrameHeight;
            drvGwin.u16Pitch = g_u16MgwFbPitch;
            drvGwin.clrType = (u16MGW_DipLatestFrame == 0x0) ? g_mgs_disp_buf[0].u16MGW_DispBuffFmt : g_mgs_disp_buf[1].u16MGW_DispBuffFmt;
            drvGwin.u32Addr = g_mgs_disp_buf[0].u32MGW_DispBuffAddr + g_u32MgwFbSize * u16MGW_DipLatestFrame;
            _MDrv_GOP_SetGwinInfo(MGW_GOP_ID, MGW_GWIN_ID, &drvGwin);

            s16GopStretchWinX = ((S16)u16CurCaptureCenterX - u16OnscreenHalfWidth);
            s16GopStretchWinY = ((S16)u16CurCaptureCenterY - u16OnscreenHalfHeight);
            _MDrv_GOP_SetHScale(MGW_GOP_ID, TRUE, u16DipCapturedFrameWidth, u16OnscreenWidth);
            _MDrv_GOP_SetVScale(MGW_GOP_ID, TRUE, u16DipCapturedFrameHeight, u16OnscreenHeight);
            _MDrv_GOP_SetStretchWin(MGW_GOP_ID, s16GopStretchWinX, s16GopStretchWinY, u16DipCapturedFrameWidth, u16DipCapturedFrameHeight);

            if ((gbSkipDisplayFrameCount > 0x1) &&
                (gu16ChangeMgwEnState == TRUE)
                )
            {
                _MDrv_GOP_GWIN_Enable(MGW_GOP_ID, MGW_GWIN_ID, gu16MgwEnState);
            }
            _MDrv_GOP_SetPipe(MGW_GOP_ID, gu16MgwScGopBlendingDelay); // hard code patch, somebody would change the pipe before MGW on
            _MDrv_GOP_UpdateReg(MGW_GOP_ID);
        }
    }

    if (g_bMGWChangSourceCropWin == TRUE)
    {
        U16 u16DipCapturedFrameWidth = 0x0;
        U16 u16DipCapturedFrameHeight = 0x0;
        U16 u16DipCapturedFramePitch = 0x0;
        U16 u16NextCaptureCenterX = g_u16CurAppCursorPosX;
        U16 u16NextCaptureCenterY = g_u16CurAppCursorPosY;
        U16 u16OnscreenWidth = MgwOnScreenWidth;
        U16 u16OnscreenHeight = MgwOnScreenHeight;
        U16 u16GopCropWinX0, u16GopCropWinX1, u16GopCropWinY0, u16GopCropWinY1;

        S_MDrv_Get_CaptureWinSize(
            u16OnscreenWidth,
            u16OnscreenHeight,
            g_u16MgwScalingRatio,
            &u16DipCapturedFrameWidth,
            &u16DipCapturedFrameHeight,
            &u16DipCapturedFramePitch);

        _S_MDrv_MGW_SetCoor2Dummy(
            u16NextCaptureCenterX,
            u16NextCaptureCenterY,
            u16DipCapturedFrameWidth,
            u16DipCapturedFrameHeight,
            g_u16MgwScalingRatio,
            u16OnscreenWidth,
            u16OnscreenHeight);

        // set DIP next crop window
        // convert cursor Coordinate to capture coordinate
        _S_MDrv_MGW_ConvertReal2CaptCoordinate(
            u16NextCaptureCenterX, u16NextCaptureCenterY,
            &u16NextCaptureCenterX, &u16NextCaptureCenterY);
        _S_MDrv_MGW_GetNextCaptureCoordinate(
            u16NextCaptureCenterX, u16NextCaptureCenterY,
            u16DipCapturedFrameWidth, u16DipCapturedFrameHeight,
            &u16GopCropWinX0, &u16GopCropWinY0, &u16GopCropWinX1, &u16GopCropWinY1);
        MDrv_DIP_SetCaptureSize(
            gMgwDipWindow,
            u16DipCapturedFrameWidth,
            u16DipCapturedFrameHeight);
        MDrv_DIP_setCropWindow(
            gMgwDipWindow,
            u16GopCropWinX0,
            u16GopCropWinY0,
            u16GopCropWinX1,
            u16GopCropWinY1);
        if (gbSkipDisplayFrameCount > 0x1)
        {
            g_bMGWChangSourceCropWin = FALSE;
        }
        gbSkipDisplayFrameCount++;
    }

    MDrv_DIP_SingleShotTrigger(gMgwDipWindow);

    spin_unlock_irqrestore(&mgw_lock, flags);

    return IRQ_HANDLED;
}

EXPORT_SYMBOL(MDrv_MGW_IntHandler);

#endif

irqreturn_t MDrv_Film_IntHandler(int irq,void *devid)
{
    MDrv_GFLIP_FilmDriverHWVer2();
    return IRQ_HANDLED;
}

U32 MDrv_GOP_RegisterInterrupt(void)
{
    int ret;
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
    /* ISO C90 forbids mixed declarations and code */
    struct irq_test *ip;
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */
    if(g_bIsrAttached == TRUE)
        return 0;  // already attached GOP isr

#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
    /* ISO C90 forbids mixed declarations and code */
#else
    struct irq_test *ip;
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */
    ip = kmalloc(sizeof *ip, GFP_KERNEL);
    spin_lock_init(&gop_lock);
    spin_lock_init(&mgw_lock);
    spin_lock_init(&flim_lock);

    MHal_GOP_MaskIRQ();

#if 1//defined(CONFIG_MSTAR_PreX14) || defined(CONFIG_MSTAR_X14)
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
    /* remove deprecated IRQF_DISABLED flag */
    ret = (request_irq(E_IRQ_GOP,_gop_interrupt,0x0,"gop",ip));
#else
    ret = (request_irq(E_IRQ_GOP,_gop_interrupt,IRQF_DISABLED,"gop",ip));
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */
#else
    ret = (request_irq(E_IRQ_GOP,_gop_interrupt,SA_INTERRUPT,"gop",ip));
#endif

    if(0 != ret)
    {
        printk("[GOP] Fail to request IRQ %d ret:%d\n", E_IRQ_GOP,ret);
        return EFAULT;
    }
#if KERNEL_FILM
    printk("film driver good\n");
    ret = (request_irq(E_IRQ_DISP, MDrv_Film_IntHandler, IRQF_SHARED, "Kfilm", ip));
    if(0 != ret)
    {
        printk("film init fail [%d]\n",ret);
        printk("[GFLIP] Fail to request IRQ:%d\n", E_IRQ_DISP);
        return EFAULT;
    }
#endif

    g_bIsrAttached = TRUE;
    init_waitqueue_head(&(_fb_waitqueue[0]));

    MHal_GOP_ClearIRQ();
    return 0;
}

U32 MDrv_GOP_DeRegisterInterrupt(unsigned long arg)
{
    free_irq(E_IRQ_GOP,NULL);

    return 0;
}

void _MDrv_GOP_GetGwinInfo(U8 u8GOP,U8 u8win,DRV_GWIN_INFO* WinInfo)
{
    U32 bankoffset=0;
    U32 u32Addr = 0;
    U16 u16tmp=0;
    BOOL u16PixelBase = 0x0;

    _HAL_GOP_GetBnkOfstByGop(u8GOP, &bankoffset);

    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_BANK_FWR, &u16tmp);
    u16PixelBase = (u16tmp & 0x80) ? TRUE : FALSE;

    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_DRAM_RBLK_L(u8win),(U16 *)&u32Addr);
    WinInfo->u32Addr = (u32Addr & 0x0000FFFF);
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_DRAM_RBLK_H(u8win),(U16 *)&u32Addr);
    WinInfo->u32Addr |= (u32Addr & 0x0000FFFF) << 16;
    if (u16PixelBase == FALSE)
    {
        WinInfo->u32Addr *= GOP_WordUnit;
    }

    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_DRAM_RBLK_HSIZE(u8win), &u16tmp);
    WinInfo->u16Pitch = u16tmp;
    if (u16PixelBase == FALSE)
    {
        WinInfo->u16Pitch *= GOP_WordUnit;
    }

    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_HEND(u8win), &u16tmp);
    WinInfo->u16HEnd = u16tmp;
}

void _MDrv_GOP_STR_UpdateGOP(void)
{
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
    /* remove unused variables */
    U32 bankoffset=0;
#else
    U32 bankoffset=0,u32RingBuffer;
    U16 bpp;
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */
    U32 u32Reg50 = 0;

    //update main osd
    _MDrv_GOP_UpdateReg(0);

    //update cursor
    _HAL_GOP_GetBnkOfstByGop(3, &bankoffset);
#ifdef CONFIG_MSTAR_PORTING_KERNEL_4_4_3
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_BANK_FWR,(U16 *)&u32Reg50);
#else
    _HAL_GOP_Read16Reg(bankoffset+GOP_4G_BANK_FWR,&u32Reg50);
#endif /* CONFIG_MSTAR_PORTING_KERNEL_4_4_3 */
    //printk("GOP = 3, reg0x50 = 0x%lx\n",u32Reg50);
    _HAL_GOP_Write16Reg(bankoffset + GOP_4G_BANK_FWR,1<<7,1<<7);
    _MDrv_GOP_UpdateReg(3);
}

U32 MDrv_KGE_Init(void)
{
#ifdef LGX_MGW_FUNCTIONALITY
    GFX_Init_Config         cfg;
    GE_Rect                 geClipWin;

    cfg.bIsCompt = FALSE;
    cfg.bIsHK= 0;
    cfg.u32VCmdQSize = 0;
    cfg.u32VCmdQAddr = 0;

    MDrv_GE_Init(NULL, (GE_Config*)&cfg, &g_apiGFXLocal.g_pGEContext);
    MDrv_GE_Chip_Proprity_Init(g_apiGFXLocal.g_pGEContext, &g_apiGFXLocal.pGeChipProperty);
    MDrv_GE_SetOnePixelMode(g_apiGFXLocal.g_pGEContext,!(g_apiGFXLocal.pGeChipProperty->bFourPixelModeStable));

    geClipWin.x = geClipWin.y = 0x0;
    geClipWin.width = 1280;
    geClipWin.height = 720;
    MDrv_GE_SetClipWindow(g_apiGFXLocal.g_pGEContext, &geClipWin);

#endif
    return 0;
}

