//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (!¡±MStar Confidential Information!¡L) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
//==============================================================================
#ifndef _MDRV_XC_MENULOAD_C
#define _MDRV_XC_MENULOAD_C

// Common Definition
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/jiffies.h>
#include <linux/wait.h>
#include <linux/types.h>
#include <linux/semaphore.h>
#include <linux/hrtimer.h>

#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>

#if defined(CONFIG_MIPS)
#elif defined(CONFIG_ARM) || defined(CONFIG_ARM64)
#include <asm/io.h>
#endif
#include <linux/sched.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/time.h>
#include <linux/proc_fs.h>


#include "mdrv_mstypes.h"
#include "mhal_xc_chip_config.h"
#include "mdrv_xc_st.h"
#include "mdrv_xc_menuload.h"
#include "mhal_menuload.h"
#include "mhal_xc.h"
#include "mstar/mstar_chip.h"
#include <asm/io.h>
#include "mdrv_xc_dynamicscaling.h"

#define  MLDBG_WARN(x)   x // open when debug time for serious issue.
#define  MLThreadDBG(x) //(printk("[MLOAD %s %d] HWEngine=%d  ",__FUNCTION__,__LINE__,enHWEngine), x)
#define  MLDBG(x) //(printk("[MLOAD %s %d] ",__FUNCTION__,__LINE__), x)
#define  MLG(x) //(printk("[MLG] "), x)

#define _AlignTo(value, align)  ( ((value) + ((align)-1)) & ~((align)-1) )
#define UNUSED(x) (void )(x)

#ifndef MLOAD_ASYNCHRONOUS
#define MLOAD_ASYNCHRONOUS  0
#endif

static DEFINE_MUTEX(mload_main_xc_mutex);
static DEFINE_MUTEX(mload_main_hdr_mutex);
static DEFINE_MUTEX(mload_sub_xc_mutex);
static DEFINE_MUTEX(mload_sub_hdr_mutex);
static DEFINE_MUTEX(mload_mutex_fire);
static DEFINE_MUTEX(mload_sub_mutex_fire);
#if ENABLE_NMLOAD
//MLOAD2
static DEFINE_MUTEX(mload2_main_xc_mutex);
static DEFINE_MUTEX(mload2_main_hdr_mutex);
static DEFINE_MUTEX(mload2_sub_xc_mutex);
static DEFINE_MUTEX(mload2_sub_hdr_mutex);
static DEFINE_MUTEX(mload2_mutex_fire);
static DEFINE_MUTEX(mload2_sub_mutex_fire);
#endif

typedef enum
{
    E_CHIP_MIU_0 = 0,
    E_CHIP_MIU_1,
    E_CHIP_MIU_2,
    E_CHIP_MIU_3,
    E_CHIP_MIU_NUM,
} CHIP_MIU_ID;

#define _phy_to_miu_offset(MiuSel, Offset, PhysAddr) if (PhysAddr < ARM_MIU1_BASE_ADDR) \
                                                        {MiuSel = E_CHIP_MIU_0; Offset = PhysAddr;} \
                                                     else if ((PhysAddr >= ARM_MIU1_BASE_ADDR) && (PhysAddr < ARM_MIU2_BASE_ADDR)) \
                                                         {MiuSel = E_CHIP_MIU_1; Offset = PhysAddr - ARM_MIU1_BASE_ADDR;} \
                                                     else \
                                                         {MiuSel = E_CHIP_MIU_2; Offset = PhysAddr - ARM_MIU2_BASE_ADDR;}
//typedef struct
//{
//    MS_U16 u16OldValue[E_K_STORE_VALUE_MAX];
//    MS_U8 u8XCMLoadMIUSel;
//    MS_PHY g_u32MLoadPhyAddr_Suspend;
//    MS_U32 g_u32MLoadBufByteLen_Suspend;
//}MLOAD_PRIVATE;

MLOAD_PRIVATE _mload_private;

static KDRV_MS_MLoad_Info _client[E_CLIENT_MAX] = {0};
#if ENABLE_NMLOAD
//NMLOAD
static KDRV_MS_MLoad_Info _astNClient[E_MLOAD_MAX][E_CLIENT_MAX] = {{0}};
#endif


static struct timespec Ts0;
static struct timespec Ts1;
static struct timespec Ts2;
static struct timespec Ts3;
static struct timespec Ts4;
static struct timespec Ts5;
static struct timespec Ts6;
static struct timespec Ts7;
static struct timespec Ts8;
static struct timespec Ts9;
static struct timespec Ts10;
static struct timespec Ts11;
static struct timespec Ts12;
static struct timespec Ts13;
static struct timespec Ts14;
static struct timespec Ts15;
static struct timespec Ts16;
static struct timespec Ts17;
static struct timespec Ts18;
static struct timespec Ts19;
static struct timespec Ts20;

static MS_U32 t_d(struct timespec startTs, struct timespec endTs)
{
    return endTs.tv_sec * 1000 + endTs.tv_nsec/1000000 - startTs.tv_sec * 1000 - startTs.tv_nsec/1000000;
}

extern MS_U32 _XC_Device_Offset[2];
MS_BOOL _bImmediate[E_MLOAD_MAX][E_CLIENT_MAX] = {TRUE, TRUE, TRUE, TRUE};
//===================================================================================================
MS_BOOL KDrv_XC_MLoad_Check_Done(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient);
void KDrv_XC_MLoad_Wait_HW_Done(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient);
void  KDrv_XC_MLoad_Init(EN_MLOAD_CLIENT_TYPE enClient, MS_PHY phyAddr);
void KDrv_XC_MLoad_Trigger(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY startAddr, MS_U16 u16CmdCnt);
void KDrv_XC_MLoad_AddCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Cmd);
MS_U32 KDrv_XC_MLoad_GetCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY DstAddr); //NO_NEED
MS_U64 KDrv_XC_MLoad_GetCmd_64Bits(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY DstAddr);
MS_BOOL KDrv_XC_MLoad_BufferEmpty(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient);
MS_BOOL KDrv_XC_MLoad_KickOff(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient);
MS_BOOL KDrv_XC_MLoad_GetCaps(EN_MLOAD_TYPE enMloadType);
void KDrv_XC_MLoad_AddNull(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient);
void KDrv_XC_MLoad_Add_32Bits_Cmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Cmd, MS_U16 u16Mask);
void KDrv_XC_MLoad_Add_64Bits_Cmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient,MS_U64 u64Cmd, MS_U16 u16Mask);
MS_BOOL KDrv_XC_MLoad_WriteCommand(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask);
MS_BOOL KDrv_XC_MLoad_WriteCommand_NonXC(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient,MS_U32 u32Bank,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask);
MS_BOOL KDrv_XC_MLoad_Fire(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bImmediate);
EN_KDRV_MLOAD_TYPE KDrv_XC_MLoad_GetStatus(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient);
void KDrv_XC_MLoad_Enable(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient,MS_BOOL bEnable);
MS_BOOL KDrv_XC_MLoad_WriteCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask);
//===================================================================================================
void _MLOAD_ENTRY(EN_MLOAD_CLIENT_TYPE client)
{
    if(client == E_CLIENT_MAIN_XC)
        mutex_lock(&mload_main_xc_mutex);
    else if(client == E_CLIENT_MAIN_HDR)
        mutex_lock(&mload_main_hdr_mutex);
    else if(client == E_CLIENT_SUB_XC)
        mutex_lock(&mload_sub_xc_mutex);
    else if(client == E_CLIENT_SUB_HDR)
        mutex_lock(&mload_sub_hdr_mutex);
}
void _MLOAD_RETURN(EN_MLOAD_CLIENT_TYPE client)
{
    if(client == E_CLIENT_MAIN_XC)
        mutex_unlock(&mload_main_xc_mutex);
    else if(client == E_CLIENT_MAIN_HDR)
        mutex_unlock(&mload_main_hdr_mutex);
    else if(client == E_CLIENT_SUB_XC)
        mutex_unlock(&mload_sub_xc_mutex);
    else if(client == E_CLIENT_SUB_HDR)
        mutex_unlock(&mload_sub_hdr_mutex);
}
void _MLOAD_FIRE_ENTRY(EN_MLOAD_CLIENT_TYPE client)
{
    if(client <= E_CLIENT_MAIN_HDR)
        mutex_lock(&mload_mutex_fire);
    else if((client > E_CLIENT_MAIN_HDR)&&(client <= E_CLIENT_SUB_HDR))
        mutex_lock(&mload_sub_mutex_fire);
}
void _MLOAD_FIRE_RETURN(EN_MLOAD_CLIENT_TYPE client)
{
    if(client <= E_CLIENT_MAIN_HDR)
        mutex_unlock(&mload_mutex_fire);
    else if((client > E_CLIENT_MAIN_HDR)&&(client <= E_CLIENT_SUB_HDR))
        mutex_unlock(&mload_sub_mutex_fire);
}

#if ENABLE_NMLOAD
//NMLOAD
static void _MLOAD_EX_ENTRY(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enclient)
{
    if (enMloadType == E_MLOAD)
    {
        if(enclient == E_CLIENT_MAIN_XC)
            mutex_lock(&mload_main_xc_mutex);
        else if(enclient == E_CLIENT_MAIN_HDR)
            mutex_lock(&mload_main_hdr_mutex);
        else if(enclient == E_CLIENT_SUB_XC)
            mutex_lock(&mload_sub_xc_mutex);
        else if(enclient == E_CLIENT_SUB_HDR)
            mutex_lock(&mload_sub_hdr_mutex);
    }
    else if (enMloadType == E_MLOAD2)
    {
        if(enclient == E_CLIENT_MAIN_XC)
            mutex_lock(&mload2_main_xc_mutex);
        else if(enclient == E_CLIENT_MAIN_HDR)
            mutex_lock(&mload2_main_hdr_mutex);
        else if(enclient == E_CLIENT_SUB_XC)
            mutex_lock(&mload2_sub_xc_mutex);
        else if(enclient == E_CLIENT_SUB_HDR)
            mutex_lock(&mload2_sub_hdr_mutex);
    }
    else
    {
        printk("please add mload[%d] mutex\n", enMloadType);
    }
}

static void _MLOAD_EX_RETURN(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enclient)
{
    if (enMloadType == E_MLOAD)
    {
        if(enclient == E_CLIENT_MAIN_XC)
            mutex_unlock(&mload_main_xc_mutex);
        else if(enclient == E_CLIENT_MAIN_HDR)
            mutex_unlock(&mload_main_hdr_mutex);
        else if(enclient == E_CLIENT_SUB_XC)
            mutex_unlock(&mload_sub_xc_mutex);
        else if(enclient == E_CLIENT_SUB_HDR)
            mutex_unlock(&mload_sub_hdr_mutex);
    }
    else if (enMloadType == E_MLOAD2)
    {
        if(enclient == E_CLIENT_MAIN_XC)
            mutex_unlock(&mload2_main_xc_mutex);
        else if(enclient == E_CLIENT_MAIN_HDR)
            mutex_unlock(&mload2_main_hdr_mutex);
        else if(enclient == E_CLIENT_SUB_XC)
            mutex_unlock(&mload2_sub_xc_mutex);
        else if(enclient == E_CLIENT_SUB_HDR)
            mutex_unlock(&mload2_sub_hdr_mutex);
    }
    else
    {
        printk("please add mload[%d] mutex\n", enMloadType);
    }
}

static void _MLOAD_EX_FIRE_ENTRY(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enclient)
{
    if (enMloadType == E_MLOAD)
    {
        if(enclient <= E_CLIENT_MAIN_HDR)
            mutex_lock(&mload_mutex_fire);
        else if((enclient > E_CLIENT_MAIN_HDR)&&(enclient <= E_CLIENT_SUB_HDR))
            mutex_lock(&mload_sub_mutex_fire);
    }
    else if (enMloadType == E_MLOAD2)
    {
        if(enclient <= E_CLIENT_MAIN_HDR)
            mutex_lock(&mload2_mutex_fire);
        else if((enclient > E_CLIENT_MAIN_HDR)&&(enclient <= E_CLIENT_SUB_HDR))
            mutex_lock(&mload2_sub_mutex_fire);
    }
    else
    {
        printk("please add mload[%d] mutex\n", enMloadType);
    }
}

static void _MLOAD_EX_FIRE_RETURN(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enclient)
{
    if (enMloadType == E_MLOAD)
    {
        if(enclient <= E_CLIENT_MAIN_HDR)
            mutex_unlock(&mload_mutex_fire);
        else if((enclient > E_CLIENT_MAIN_HDR)&&(enclient <= E_CLIENT_SUB_HDR))
            mutex_unlock(&mload_sub_mutex_fire);
    }
    else if (enMloadType == E_MLOAD2)
    {
        if(enclient <= E_CLIENT_MAIN_HDR)
            mutex_unlock(&mload2_mutex_fire);
        else if((enclient > E_CLIENT_MAIN_HDR)&&(enclient <= E_CLIENT_SUB_HDR))
            mutex_unlock(&mload2_sub_mutex_fire);
    }
    else
    {
        printk("please add mload[%d] mutex\n", enMloadType);
    }
}
#endif

static KDRV_MS_MLoad_Info* _GetMloadClientInfo(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    if (enMloadType == E_MLOAD)
    {
        return &_client[enClient];
    }
#if ENABLE_NMLOAD
    else
    {
        return &_astNClient[enMloadType][enClient];
    }
#endif
}

//======PA 2 VA MAPPING=======
MS_VIRT _PA2VA(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, phys_addr_t phy, phys_addr_t len)
{
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    phys_addr_t tmp_phy;

    if(phy < ARM_MIU1_BASE_ADDR)
    {
        tmp_phy = phy + ARM_MIU0_BUS_BASE ;
    }
    else if((phy >= ARM_MIU1_BASE_ADDR) && (phy < ARM_MIU2_BASE_ADDR))
    {
        tmp_phy = phy - ARM_MIU1_BASE_ADDR + ARM_MIU1_BUS_BASE ;
    }
    else
    {
        tmp_phy = phy - ARM_MIU2_BASE_ADDR + ARM_MIU2_BUS_BASE ;
    }

    pstClient->len = len;
    if (pfn_valid(__phys_to_pfn(tmp_phy)))
    {
        pstClient->va_phy = __va(tmp_phy);
    }
    else
    {
        pstClient->va_phy =  ioremap(tmp_phy, len);
    }

    memset(pstClient->va_phy, 0, len);

    MLDBG(printk("%s: mload[%d] client = %d, pa= 0x%x, len= 0x%x, va = 0x%x\n", __FUNCTION__, enMloadType, enClient, (MS_U32)phy, len, (MS_U32)pstClient->va_phy));

    return (MS_VIRT)pstClient->va_phy;
}

//======Get PA 2 VA======
MS_VIRT _GET_PA2VA(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY DstAddr)
{
    MS_PHY tmp_diff = 0;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    if((DstAddr >= pstClient->PhyAddr) && (DstAddr < (pstClient->PhyAddr + pstClient->len)))
    {
        tmp_diff = DstAddr - pstClient->PhyAddr;

        return (pstClient->va_phy + tmp_diff);
    }
    else
    {
        printk("%s: Mload[%d] No PA 2 VA MAPPING! PA:0x%x, DstPA:0x%x, len:0x%x\n", __FUNCTION__,
               enMloadType, (MS_U32)pstClient->PhyAddr, (MS_U32)DstAddr, pstClient->len);

        return 0;
    }
}

void _DelayTask (MS_U32 u32Ms)
{
    // Refer to Documentation/timers/timers-howto.txt
    //
    // SLEEPING FOR ~USECS OR SMALL MSECS ( 10us - 20ms):
    //        * Use usleep_range
    // SLEEPING FOR LARGER MSECS ( 10ms+ )
    //        * Use msleep or possibly msleep_interruptible
    if(u32Ms <= 20)
    {
        usleep_range(u32Ms * 1000, u32Ms * 1000);
    }
    else
    {
        msleep_interruptible((unsigned int)u32Ms);
    }
}

void _DelayTaskUs (MS_U32 u32Us)
{
    struct timespec TS1, TS2;
    getnstimeofday(&TS1);
    getnstimeofday(&TS2);

    while((TS2.tv_nsec/1000 + TS2.tv_sec*1000000UL)-(TS1.tv_nsec/1000 + TS1.tv_sec*1000000UL) < u32Us)
    {
        getnstimeofday(&TS2);
    }
}

static MS_U32 _MDrv_XC_GetMLoadMemOffset(MS_U32 u32Index)
{
    MS_U32 u32Offset = 0;
    MS_U32 u32CmdCntPerMIUBus = (MS_MLOAD_BUS_WIDTH / MS_MLOAD_CMD_LEN_64BITS);

    u32Offset = (u32Index / u32CmdCntPerMIUBus) * MS_MLOAD_MEM_BASE_UNIT + (u32Index % u32CmdCntPerMIUBus) * MS_MLOAD_CMD_LEN_64BITS;

    return u32Offset;
}

MS_BOOL KDrv_XC_MLoad_Check_Done(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    MS_BOOL bEn;

    if (enMloadType == E_MLOAD)
    {
        if(KHal_XC_MLoad_get_status(enClient))
            bEn = FALSE;
        else
            bEn = TRUE;
    }
#if ENABLE_NMLOAD
    else
    {
        if(KHal_XC_MLoad_EX_get_status(enMloadType, enClient))
            bEn = FALSE;
        else
            bEn = TRUE;
    }
#endif

    return bEn;
}

void KDrv_XC_MLoad_Wait_HW_Done(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    MS_U32 u32Delayms = 0;
    while((KDrv_XC_MLoad_Check_Done(enMloadType, enClient) == FALSE) && u32Delayms < 600)
    {
        _DelayTask(1);
        u32Delayms++;
    }
}

#if MLOAD_ASYNCHRONOUS
typedef struct
{
    wait_queue_head_t stMloadWaitQueue;
    bool bMloadFire;
    EN_MLOAD_CLIENT_TYPE enClient_type;
    EN_MLOAD_CLIENT_TYPE enCurrentFireType;
    EN_MLOAD_CLIENT_TYPE enPreFireType;
} MS_MLoad_Fire_Info_st;

typedef enum
{
    EN_MLoad_HW_SC0 = 0,
    EN_MLoad_HW_SC1 = 1,
    EN_MLoad_HW_NUM = 2,
} EN_MLoad_HW_Engine;

static struct task_struct *pstMload_fire_tsk[EN_MLoad_HW_NUM];
static bool bInitMload = FALSE;
static MS_MLoad_Fire_Info_st _stMloadFireInfo[EN_MLoad_HW_NUM];

const EN_MLOAD_CLIENT_TYPE enEnginePriority[E_CLIENT_MAX] = {E_CLIENT_MAIN_XC, E_CLIENT_MAIN_HDR, E_CLIENT_SUB_XC, E_CLIENT_SUB_HDR};
EN_MLOAD_CLIENT_TYPE KDrv_XC_Mload_GetMaxPriorityEngine(EN_MLoad_HW_Engine enHWEngine)
{
    EN_MLOAD_CLIENT_TYPE enMaxPriority = E_CLIENT_MAX;
    MS_U32 u32Client = 0;

    for (u32Client = 0; u32Client < E_CLIENT_SUB_XC; u32Client++)
    {
        if (_client[enEnginePriority[u32Client]].u16FPoint > _client[enEnginePriority[u32Client]].u16RPoint)
        {
            if(   (enEnginePriority[u32Client] <= E_CLIENT_MAIN_HDR)
                  &&(enHWEngine == EN_MLoad_HW_SC0))
            {
                enMaxPriority = enEnginePriority[u32Client];
                break;
            }
            else if(  (enEnginePriority[u32Client] > E_CLIENT_MAIN_HDR)&&(enEnginePriority[u32Client] <= E_CLIENT_SUB_HDR)
                      &&(enHWEngine == EN_MLoad_HW_SC0))
            {
                enMaxPriority = enEnginePriority[u32Client];
                break;
            }
        }
    }

    return enMaxPriority;
}

static int mload_fire_task(void* arg)
{
    MS_U32 u32Jiffy_10ms = 0;
    MS_U32 u32Cycle_600ms = 0;
    MS_U32 u32Cycle = 0;
    MS_U16 u16CmdCnt = 0;
    MS_U32 u32ReadMemShift = 0;
    MS_PHY phyCmdBufAddr = 0;
    EN_MLoad_HW_Engine enHWEngine = *((EN_MLoad_HW_Engine*)arg);
    EN_MLOAD_CLIENT_TYPE enMaxPriority = E_CLIENT_MAX;

    if (HZ >= 1000)
    {
        u32Jiffy_10ms = HZ/100;
        u32Cycle_600ms = 60;
    }
    else if (HZ < 100)
    {
        u32Jiffy_10ms = HZ/100;
        u32Cycle_600ms = 600/((1000/HZ)*u32Jiffy_10ms);
    }
    else if (HZ > 0)
    {
        u32Jiffy_10ms = 1;
        u32Cycle_600ms = 600/(1000/HZ);
    }
    else
    {
        u32Jiffy_10ms = 10;
        u32Cycle_600ms = 10;
        printk("HZ is zero!");
    }

    while(TRUE)
    {
    Begine:
        wait_event_interruptible(_stMloadFireInfo[enHWEngine].stMloadWaitQueue, _stMloadFireInfo[enHWEngine].bMloadFire);
        MLThreadDBG(printk("_stMloadFireInfo.enClient_type=%u\n",_stMloadFireInfo[enHWEngine].enClient_type));
    NewEvent:
        _stMloadFireInfo[enHWEngine].enCurrentFireType = _stMloadFireInfo[enHWEngine].enClient_type;
        _MLOAD_FIRE_ENTRY(_stMloadFireInfo[enHWEngine].enCurrentFireType);
        if((KDrv_XC_MLoad_Check_Done(E_MLOAD, _stMloadFireInfo[enHWEngine].enCurrentFireType) == TRUE))
        {
            u16CmdCnt = _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16FPoint - _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16RPoint + 1;
            u16CmdCnt = KHal_XC_MLoad_Get_Depth(_stMloadFireInfo[enHWEngine].enCurrentFireType, u16CmdCnt);
            u32ReadMemShift = _MDrv_XC_GetMLoadMemOffset(_client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16RPoint);

            phyCmdBufAddr = (_client[_stMloadFireInfo[enHWEngine].enCurrentFireType].PhyAddr + u32ReadMemShift);
            MLThreadDBG(printk("phyCmdBufAddr = 0x%lx, u16CmdCnt = 0x%lx  enClient_type=%u PhyAddr=%llx\n", phyCmdBufAddr, u16CmdCnt, _stMloadFireInfo[enHWEngine].enCurrentFireType, _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].PhyAddr));

            KDrv_XC_MLoad_Trigger(E_MLOAD, _stMloadFireInfo[enHWEngine].enCurrentFireType, phyCmdBufAddr, u16CmdCnt);

            _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
        }
        else
        {
            if (_stMloadFireInfo[enHWEngine].enCurrentFireType == _stMloadFireInfo[enHWEngine].enPreFireType)
            {
                KHal_XC_MLoad_set_on_off(_stMloadFireInfo[enHWEngine].enCurrentFireType, DISABLE);

                u16CmdCnt = _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16FPoint - _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16RPoint + 1;
                u16CmdCnt = KHal_XC_MLoad_Get_Depth(_stMloadFireInfo[enHWEngine].enCurrentFireType, u16CmdCnt);
                u32ReadMemShift = _MDrv_XC_GetMLoadMemOffset(_client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16RPoint);

                phyCmdBufAddr = (_client[_stMloadFireInfo[enHWEngine].enCurrentFireType].PhyAddr + u32ReadMemShift);
                MLThreadDBG(printk("phyCmdBufAddr = 0x%lx, u16CmdCnt = 0x%lx\n", phyCmdBufAddr, u16CmdCnt));

                KDrv_XC_MLoad_Trigger(E_MLOAD, _stMloadFireInfo[enHWEngine].enCurrentFireType,phyCmdBufAddr, u16CmdCnt);
                _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
            }
            else
            {
                enMaxPriority = KDrv_XC_Mload_GetMaxPriorityEngine(enHWEngine);
                //printk("dirk %d max=%u\n",__LINE__,enMaxPriority);
                if (enMaxPriority == E_CLIENT_MAX)
                {
                    printk("Mload error!enMaxPriority = E_CLIENT_MAX!\n");
                    //assert(0);
                }
            ChooseHighPriority:
                if (_stMloadFireInfo[enHWEngine].enPreFireType != enMaxPriority)
                {
                    MLThreadDBG(printk("enMaxPriority=%u   enPreFireType=%u\n", enMaxPriority, _stMloadFireInfo[enHWEngine].enPreFireType));
                    _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
                    _MLOAD_FIRE_ENTRY(enMaxPriority);
                    _stMloadFireInfo[enHWEngine].enCurrentFireType = enMaxPriority;
                    KHal_XC_MLoad_set_on_off(_stMloadFireInfo[enHWEngine].enCurrentFireType, DISABLE);

                    u16CmdCnt = _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16FPoint - _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16RPoint + 1;
                    u16CmdCnt = KHal_XC_MLoad_Get_Depth(_stMloadFireInfo[enHWEngine].enCurrentFireType, u16CmdCnt);
                    u32ReadMemShift = _MDrv_XC_GetMLoadMemOffset(_client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16RPoint);

                    phyCmdBufAddr = (_client[_stMloadFireInfo[enHWEngine].enCurrentFireType].PhyAddr + u32ReadMemShift);
                    MLThreadDBG(printk("phyCmdBufAddr = 0x%lx, u16CmdCnt = 0x%lx\n", phyCmdBufAddr, u16CmdCnt));

                    KDrv_XC_MLoad_Trigger(E_MLOAD, _stMloadFireInfo[enHWEngine].enCurrentFireType, phyCmdBufAddr, u16CmdCnt);
                    _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
                }
                else
                {
                    _stMloadFireInfo[enHWEngine].bMloadFire = FALSE;
                    _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
                }
            }
        }
        u32Cycle = 0;
    WaitFireSuccess:
        _MLOAD_FIRE_ENTRY(_stMloadFireInfo[enHWEngine].enCurrentFireType);
        if (wait_event_interruptible_timeout(_stMloadFireInfo[enHWEngine].stMloadWaitQueue, _stMloadFireInfo[enHWEngine].bMloadFire, u32Jiffy_10ms) > 0)
        {
            MLThreadDBG(printk("New Mload task interrupt!\n"));
            _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
            goto NewEvent;
        }
        else
        {

            if((KDrv_XC_MLoad_Check_Done(E_MLOAD, _stMloadFireInfo[enHWEngine].enCurrentFireType) == TRUE))
            {
                // menuload fire done
                // here,you can print register
                if (_client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16FPoint != 0)
                {
                    _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16RPoint = _client[_stMloadFireInfo[enHWEngine].enCurrentFireType].u16FPoint+1;
                }

                enMaxPriority = KDrv_XC_Mload_GetMaxPriorityEngine(enHWEngine);
                //printk("dirk %d max=%u\n",__LINE__,enMaxPriority);
                if (enMaxPriority != E_CLIENT_MAX)
                {
                    MLThreadDBG(printk("Choose anther Max priority mload client, Client = %u !\n",enMaxPriority));
                    _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
                    goto ChooseHighPriority;
                }
                else
                {
                    _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
                    // printk("fire down = %u !\n",_stMloadFireInfo.enCurrentFireType);
                    goto Begine;
                }
            }
            else
            {
                if (u32Cycle > u32Cycle_600ms)
                {
                    MLDBG_WARN(printk("MLoad, TimeOut\n"));
                    KHal_XC_MLoad_set_on_off(_stMloadFireInfo[enHWEngine].enCurrentFireType, DISABLE);
                }
                u32Cycle ++;
                _MLOAD_FIRE_RETURN(_stMloadFireInfo[enHWEngine].enCurrentFireType);
                goto WaitFireSuccess;
            }
        }
        printk("mload_fire_task will never run here!\n");

    }
}
#endif

void KDrv_XC_MLoad_Init(EN_MLOAD_CLIENT_TYPE enClient, MS_PHY phyAddr)
{
    MS_PHY u32StartOffset;
    MS_U8 u8MIUSel = 0;
    MS_U16 u16Cmd;
    //MS_U32 i = 0;

    MLDBG(printk("%s: 0x%lx\n", __FUNCTION__, phyAddr));
    // remove the MIU offset
    if(1)//(!INITED[enClient/CLIENT_NUM])    //review later for better solution or is it needed?
    {
        _phy_to_miu_offset(u8MIUSel, u32StartOffset, phyAddr);
        UNUSED(u32StartOffset);
        KHal_XC_MLoad_set_opm_arbiter_bypass(enClient,TRUE);

        //set command format
        KHal_XC_MLoad_Command_Format_initial(enClient);

        //Hal_XC_MLoad_set_on_off(DISABLE);
        KHal_XC_MLoad_set_trigger_timing(enClient,TRIG_SRC_DELAY_LINE); //By REG_MLOAD_TRIG_DLY
        KHal_XC_MLoad_set_trigger_delay(enClient,0x05);
        KHal_XC_MLoad_set_trig_p(enClient,0x08, 0x0A);
        KHal_XC_MLoad_get_trig_p(enClient,&_mload_private.u16OldValue[E_K_STORE_VALUE_AUTO_TUNE_AREA_TRIG], &_mload_private.u16OldValue[E_K_STORE_VALUE_DISP_AREA_TRIG]);
#ifdef HW_DESIGN_4K2K_VER
#if (HW_DESIGN_4K2K_VER == 4)
        // set ip trig as hw default value
        KDrv_XC_MLoad_set_IP_trig_p(enClient,0x03, 0x05);
        KDrv_XC_MLoad_get_IP_trig_p(enClient,&_mload_private.u16OldValue[E_K_STORE_VALUE_IP_AUTO_TUNE_AREA_TRIG], &_mload_private.u16OldValue[E_K_STORE_VALUE_IP_DISP_AREA_TRIG]);
#endif
#endif
        //Hal_XC_MLoad_set_miu_bus_sel(pInstance, MS_MLOAD_MIU_BUS_SEL);
        KHal_XC_MLoad_set_miusel(enClient,u8MIUSel);
        _mload_private.u8XCMLoadMIUSel = u8MIUSel;

        //***********watch dog***************
        /*
        **enable watch dog
        */
        KHal_XC_MLoad_enable_watch_dog(enClient,TRUE);
        /*
        ** enlarge watch dog response time
        */
        KHal_XC_MLoad_set_watch_dog_time_delay(enClient,0x00F0);
        /*
        ** watch dog timer reset type 00: dma only, 01: miu, 11: all
        */
        KHal_XC_MLoad_enable_watch_dog_reset(enClient,MLoad_WD_Timer_Reset_ALL);
        //***********************************
        KHal_XC_MLoad_set_BitMask(enClient,TRUE);
        //INITED[enClient/CLIENT_NUM] = TRUE;
    }
    if (KDrv_XC_MLoad_GetCaps(E_MLOAD))
    {
        for (u16Cmd = 0; u16Cmd < 32; u16Cmd++)
        {
            if(IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
            {
                if(ENABLE_64BITS_SPREAD_MODE)
                {
                    KDrv_XC_MLoad_Add_64Bits_Cmd(E_MLOAD, enClient,(MS_U64)MS_MLOAD_NULL_CMD_SPREAD_MODE, 0xFFFF);
                }
                else
                {
                    KDrv_XC_MLoad_Add_64Bits_Cmd(E_MLOAD, enClient,(MS_U64)MS_MLOAD_NULL_CMD, 0xFFFF);
                }
            }
            else
            {
                KDrv_XC_MLoad_Add_32Bits_Cmd(E_MLOAD, enClient,(MS_U32)MS_MLOAD_NULL_CMD, 0xFFFF);
            }
        }
        KDrv_XC_MLoad_AddNull(E_MLOAD, enClient);
        _client[enClient].u16FPoint = _client[enClient].u16WPoint - 1;

        // when system is power on, the read count of MLoad may be not 0.
        // If we enable MLoad RIU_En, the garbage data in fifo will over-write
        // unexpected register.
        // Hence, add below code before Kickoff to avoid it.

        KHal_XC_MLoad_set_riu_cs(TRUE);
        _DelayTask(1);

        KHal_XC_MLoad_Set_riu(enClient,1);

        MDrv_WriteByte(0x102F00, 0xFF);

        KHal_XC_MLoad_set_riu_cs(FALSE);

        _DelayTask(1);

        KDrv_XC_MLoad_KickOff(E_MLOAD, enClient);
#if MLOAD_ASYNCHRONOUS
        MS_U32 u32Delayms = 0;
        while((KDrv_XC_MLoad_Check_Done(E_MLOAD, enClient) == FALSE) && u32Delayms < 600)
        {
            _DelayTask(1);
            u32Delayms++;
        }
        if(KDrv_XC_MLoad_Check_Done(E_MLOAD, enClient) == FALSE)
        {
            MLDBG_WARN(printk("MLoad Init, Client=%d TimeOut\n",enClient));
            KHal_XC_MLoad_set_on_off(enClient, DISABLE);
        }
#endif
        _client[enClient].bCleared = FALSE;
    }
#if MLOAD_ASYNCHRONOUS
    if (bInitMload == FALSE)
    {
        static EN_MLoad_HW_Engine enHWEngineSC0 = EN_MLoad_HW_SC0;
        init_waitqueue_head(&_stMloadFireInfo[EN_MLoad_HW_SC0].stMloadWaitQueue);
        pstMload_fire_tsk[EN_MLoad_HW_SC0] = kthread_create(mload_fire_task, &enHWEngineSC0, "Mload SC0 Fire Task");
        if (IS_ERR(pstMload_fire_tsk[EN_MLoad_HW_SC0]))
        {
            printk("create kthread for delay free fail\n");
            pstMload_fire_tsk[EN_MLoad_HW_SC0] = NULL;
        }
        else
            wake_up_process(pstMload_fire_tsk[EN_MLoad_HW_SC0]);

        static EN_MLoad_HW_Engine enHWEngineSC1 = EN_MLoad_HW_SC1;
        init_waitqueue_head(&_stMloadFireInfo[EN_MLoad_HW_SC1].stMloadWaitQueue);
        pstMload_fire_tsk[EN_MLoad_HW_SC1] = kthread_create(mload_fire_task, &enHWEngineSC1, "Mload SC1 Fire Task");
        if (IS_ERR(pstMload_fire_tsk[EN_MLoad_HW_SC1]))
        {
            printk("create kthread for delay free fail\n");
            pstMload_fire_tsk[EN_MLoad_HW_SC1] = NULL;
        }
        else
            wake_up_process(pstMload_fire_tsk[EN_MLoad_HW_SC1]);

        bInitMload = TRUE;
    }
#endif
}

MS_BOOL KDrv_XC_MLoad_GetCaps(EN_MLOAD_TYPE enMloadType)
{
    if (enMloadType == E_MLOAD)
    {
        return KHal_XC_MLoad_GetCaps();
    }
#if ENABLE_NMLOAD
    else
    {
        return KHal_XC_NMLoad_GetCaps(enMloadType);
    }
#endif
}

void KDrv_XC_MLoad_Trigger(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY startAddr, MS_U16 u16CmdCnt)
{
    MS_PHY u32StartOffset = 0;
    MS_U8 u8MIUSel = 0;

    MLDBG(printk("%s mloadType: %d, client: %d, addr: 0x%x, depth: %u\n", __FUNCTION__, enMloadType, enClient, (MS_U32)startAddr, u16CmdCnt));
    if (u16CmdCnt != 0)
    {
        //dumpcmd();
        _phy_to_miu_offset(u8MIUSel, u32StartOffset, startAddr);
        UNUSED(u8MIUSel);
        KHal_XC_MLoad_set_len(enClient, MS_MLOAD_REG_LEN);//length of DMA request
        if (enMloadType == E_MLOAD)
        {
            KHal_XC_MLoad_set_base_addr(enClient, u32StartOffset);
            KHal_XC_MLoad_set_depth(enClient, u16CmdCnt);
            KHal_XC_MLoad_set_on_off(enClient, ENABLE);
        }
#if ENABLE_NMLOAD
        else
        {
            KHal_XC_NMLoad_set_base_addr(enMloadType, enClient, u32StartOffset);
            KHal_XC_NMLoad_set_depth(enMloadType, enClient, u16CmdCnt);
            KHal_XC_NMLoad_set_on_off(enMloadType, enClient, ENABLE);
        }
#endif
        _DelayTaskUs(3);
        KHal_XC_MLoad_Set_riu(enClient, ENABLE);
    }
#if MLOAD_ASYNCHRONOUS
    _stMloadFireInfo[enClient/EN_MLoad_HW_NUM].bMloadFire = FALSE;
    _stMloadFireInfo[enClient/EN_MLoad_HW_NUM].enPreFireType = _stMloadFireInfo[enClient/EN_MLoad_HW_NUM].enCurrentFireType;
#endif
}

void KDrv_XC_MLoad_AddCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Cmd)
{
    MS_U32 *pu32Addr = NULL;
    MS_PHY DstAddr;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    DstAddr = pstClient->PhyAddr + pstClient->u16WPoint * MS_MLOAD_CMD_LEN;

    pu32Addr = (MS_U32 *)_GET_PA2VA(enMloadType, enClient, DstAddr);
    *pu32Addr = u32Cmd;

    pstClient->u16WPoint++;
}

void KDrv_XC_MLoad_SetCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY DstAddr, MS_U32 u32Cmd)
{
    MS_U32 *pu32Addr = NULL;

    pu32Addr = (MS_U32 *)_GET_PA2VA(enMloadType, enClient, DstAddr);
    *pu32Addr = u32Cmd;
}

void KDrv_XC_MLoad_SetCmd_64Bits(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY DstAddr, MS_U64 u64Cmd)
{
    MS_U64 *pu64Addr = NULL;

    pu64Addr = (MS_U64 *)_GET_PA2VA(enMloadType, enClient, DstAddr);
    *pu64Addr = u64Cmd;
}

MS_U32 KDrv_XC_MLoad_GetCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY DstAddr)
{
    MS_VIRT u32Addr;

    u32Addr = _GET_PA2VA(enMloadType, enClient, DstAddr);
    return (MS_U32)(*(MS_U32 *)u32Addr);
}

MS_U64 KDrv_XC_MLoad_GetCmd_64Bits(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_PHY DstAddr)
{
    MS_VIRT u32Addr;

    u32Addr = _GET_PA2VA(enMloadType, enClient, DstAddr);
    return (MS_U64)(*(MS_U64 *)u32Addr);
}

MS_BOOL KDrv_XC_MLoad_BufferEmpty(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    if(
#if MLOAD_ASYNCHRONOUS == 0
        KDrv_XC_MLoad_Check_Done(enMloadType, enClient) &&
#endif
        (pstClient->u16WPoint == pstClient->u16RPoint) &&
        (pstClient->u16WPoint == pstClient->u16FPoint+1))
        return TRUE;
    else
        return FALSE;
}

//-------------------------------------------------------------------------------------------------
/// Write command to the Menuload buffer
/// @param  u32Cmd                 \b IN: the command to write into the buffer
/// @return  TRUE if succeed, FALSE if failed
//-------------------------------------------------------------------------------------------------
static MS_BOOL _KDrv_XC_MLoad_WriteCmd_32Bits(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Cmd, MS_U16 u16Mask)
{
    MS_BOOL bRet;
    MS_U16 u16DummyCmdIdx;
    MS_U16 u16QueueCmd;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

#ifdef ENABLE_SCALING_WO_MUTE
    if ((pstClient->u16WPoint == pstClient->u16RPoint) &&
        (pstClient->u16WPoint == pstClient->u16FPoint+1))
    {
        KDrv_XC_MLoad_Wait_HW_Done(enMloadType, enClient);
    }
#endif

    if( KDrv_XC_MLoad_BufferEmpty(enMloadType, enClient) )
    {
        pstClient->u16RPoint = 0;
        pstClient->u16WPoint = 0;
        pstClient->u16FPoint = 0;
        KDrv_XC_MLoad_Add_32Bits_Cmd(enMloadType, enClient, u32Cmd, u16Mask);
        bRet = TRUE;
    }
    else
    {
        u16DummyCmdIdx = _AlignTo(pstClient->u16WPoint+1 + 16 , 4);

        if (u16DummyCmdIdx == pstClient->u16WPoint+1)
        {
            u16DummyCmdIdx = pstClient->u16WPoint+1+MS_MLOAD_CMD_ALIGN;
        }

        if(u16DummyCmdIdx < pstClient->u16MaxCmdCnt)
        {
            u16QueueCmd = pstClient->u16WPoint - pstClient->u16RPoint + 1;

            if(u16QueueCmd == MS_MLOAD_MAX_CMD_CNT(enClient) - 1) //max cmd depth is MS_MLOAD_MAX_CMD_CNT
            {
                KDrv_XC_MLoad_Add_32Bits_Cmd(enMloadType, enClient, MS_MLOAD_NULL_CMD, 0xFFFF);
            }
            KDrv_XC_MLoad_Add_32Bits_Cmd(enMloadType, enClient, u32Cmd, u16Mask);

            bRet = TRUE;
        }
        else
        {
            MLDBG(printk("WPoint=%x, MaxCnt=%x, DummyIdx=%x\n"
                         , pstClient->u16WPoint
                         , pstClient->u16MaxCmdCnt
                         , u16DummyCmdIdx));
            bRet = FALSE;
        }
    }

    return bRet;
}

//-------------------------------------------------------------------------------------------------
/// Write command to the Menuload buffer
/// @param  u64Cmd                 \b IN: the command to write into the buffer
/// @return  TRUE if succeed, FALSE if failed
//-------------------------------------------------------------------------------------------------
static MS_BOOL _KDrv_XC_MLoad_WriteCmd_64Bits(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U64 u64Cmd, MS_U16 u16Mask)
{
    MS_BOOL bRet;
    MS_U16 u16DummyCmdIdx;
    MS_U16 u16QueueCmd;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);
#ifdef ENABLE_SCALING_WO_MUTE
    if ((pstClient->u16WPoint == pXCResourcePrivate->pstClient->u16RPoint) &&
        (pstClient->u16WPoint == pXCResourcePrivate->pstClient->u16FPoint+1))
    {
        KDrv_XC_MLoad_Wait_HW_Done(enMloadType, enClient);
    }
#endif
#if MLOAD_ASYNCHRONOUS
BeginWriteCmd:
#endif
    if (  KDrv_XC_MLoad_BufferEmpty(enMloadType, enClient)
#if (MLOAD_ASYNCHRONOUS == 0)
          || pstClient->bCleared
#endif
       )
    {
        pstClient->u16RPoint = 0;
        pstClient->u16WPoint = 0;
        pstClient->u16FPoint = 0;
        MLDBG(printk(" W= %u, F= %u  R=%u  enClient=%u\n", pstClient->u16WPoint
                     ,pstClient->u16FPoint,pstClient->u16RPoint,enClient));
        getnstimeofday(&Ts3);
        KDrv_XC_MLoad_Add_64Bits_Cmd(enMloadType, enClient, u64Cmd, u16Mask);
        getnstimeofday(&Ts5);

        bRet = TRUE;
    }
    else
    {
        u16DummyCmdIdx = _AlignTo(pstClient->u16WPoint+1 + 16 , 4);

        if (u16DummyCmdIdx == pstClient->u16WPoint+1)
        {
            u16DummyCmdIdx = pstClient->u16WPoint+1+MS_MLOAD_CMD_ALIGN;
        }

        if(u16DummyCmdIdx < pstClient->u16MaxCmdCnt)
        {
            u16QueueCmd = pstClient->u16WPoint - pstClient->u16RPoint + 1;

            if(u16QueueCmd == MS_MLOAD_MAX_CMD_CNT(enClient) - 1) //max cmd depth is MS_MLOAD_MAX_CMD_CNT
            {
                KDrv_XC_MLoad_Add_64Bits_Cmd(enMloadType, enClient, MS_MLOAD_NULL_CMD_SPREAD_MODE, 0xFFFF);
            }
#if MLOAD_ASYNCHRONOUS
            MS_U32 u32CmdCntPerMIUBus = (MS_MLOAD_BUS_WIDTH/MS_MLOAD_CMD_LEN_64BITS);
            MS_U32 u32OneIndexMaxMemSize = (MS_MLOAD_MAX_CMD_CNT(enClient)/u32CmdCntPerMIUBus)*BYTE_PER_WORD;
            if (_mload_private.g_u32MLoadBufByteLen_Suspend - (pstClient->u16FPoint/u32CmdCntPerMIUBus)*BYTE_PER_WORD < u32OneIndexMaxMemSize)
            {
                _DelayTask(1);
                goto BeginWriteCmd;
            }
#endif
            getnstimeofday(&Ts3);
            KDrv_XC_MLoad_Add_64Bits_Cmd(enMloadType, enClient, u64Cmd, u16Mask);
            getnstimeofday(&Ts5);
            bRet = TRUE;
        }
        else
        {
            MLDBG(printk("WPoint=%x, MaxCnt=%x, DummyIdx=%x\n"
                         , pstClient->u16WPoint
                         , pstClient->u16MaxCmdCnt
                         , u16DummyCmdIdx));
            bRet = FALSE;
        }
    }

    return bRet;
}
extern void Chip_Flush_Miu_Pipe(void);

#if MLOAD_ASYNCHRONOUS
MS_BOOL KDrv_XC_MLoad_AddEndCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    //MS_MLOAD_END_CMD use instance, does not remove this
    MS_BOOL bRet;
    MS_U16 u16CmdCnt;
    MS_U16 u16QueueCmd, u16FireIdx;
    MS_PHY CmdBufAddr;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    MLDBG(printk("%s\n",__FUNCTION__));
    {
        if(pstClient->u16FPoint > pstClient->.u16RPoint)
        {
            u16QueueCmd = pstClient->u16WPoint - pstClient->u16RPoint;

            if(u16QueueCmd > MS_MLOAD_MAX_CMD_CNT(enClient))
            {
                MLDBG_WARN(printk("Queue Too Many enClient=%d!!!!!!!!!!!!!!!!!\n",enClient));
                MLDBG_WARN(printk("WPoint=%d, FirePoint=%d, RPoint=%d\n"
                                  ,pstClient->u16WPoint
                                  ,pstClient->u16FPoint
                                  ,pstClient->u16RPoint));
            }

            u16FireIdx = pstClient->u16FPoint;
            MLDBG(printk("W= %u, F= %u  R=%u enClient=%u\n", pstClient->u16WPoint,pstClient->u16FPoint,pstClient->u16RPoint,enClient));

            if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
            {
                MS_U64 u64EndCmd;
                MS_PHY u32FireMemShift = 0;

#if ENABLE_64BITS_SPREAD_MODE
                if((pstClient->u16FPoint % 2) != 0)
                {

                    u32FireMemShift = _MDrv_XC_GetMLoadMemOffset(u16FireIdx);
                }

                CmdBufAddr = pstClient->PhyAddr + u32FireMemShift;


                u64EndCmd = KDrv_XC_MLoad_GetCmd_64Bits(enMloadType, enClient, CmdBufAddr);
                if(u64EndCmd != MS_MLOAD_NULL_CMD_SPREAD_MODE)
                {
                    MLDBG_WARN(printk("WPoint=%d, FirePoint=%d, RPoint=%d\n"
                                      , pstClient->u16WPoint
                                      , pstClient->u16FPoint
                                      , pstClient->u16RPoint));
                    MLDBG_WARN(printk("KickOff: Non Null Cmd. Trigger is not executed!!\n"));
                    bRet = FALSE;
                }
                else
                {
                    u16CmdCnt = u16FireIdx - pstClient->u16RPoint + 1;
                    u16CmdCnt = KHal_XC_MLoad_Get_Depth(enClient,u16CmdCnt);
                    if((enClient/CLIENT_NUM)== 0)
                    {
                        u64EndCmd = MS_MLOAD_END_CMD_SPREAD_MODE(((MS_U64)u16CmdCnt));
                    }
                    else
                    {
                        u64EndCmd = MS_MLOAD_END_CMD_DEV1_SPREAD_MODE(((MS_U64)u16CmdCnt));
                    }
                    KDrv_XC_MLoad_SetCmd_64Bits(enMloadType, enClient, CmdBufAddr, u64EndCmd);

                    Chip_Flush_Miu_Pipe();

                    bRet = TRUE;
                    MLDBG(printk("after fire u16WPoint=%u u16RPoint=%u u16FPoint=%u\n",
                                 pstClient->u16WPoint,
                                 pstClient->u16RPoint,
                                 pstClient->u16FPoint));
                }
#else
                //check the last command to see whether it is null command
                CmdBufAddr = pstClient->PhyAddr + u16FireIdx * MS_MLOAD_CMD_LEN_64BITS;
                u64EndCmd = KDrv_XC_MLoad_GetCmd_64Bits(enMloadType, enClient, CmdBufAddr);
                if(u64EndCmd != MS_MLOAD_NULL_CMD)
                {
                    MLDBG_WARN(printk("WPoint=%d, FirePoint=%d, RPoint=%d\n"
                                      , pstClient->u16WPoint
                                      , pstClient->u16FPoint
                                      , pstClient->u16RPoint));
                    MLDBG_WARN(printk("KickOff: Non Null Cmd. Trigger is not executed!!\n"));
                    bRet = FALSE;
                }
                else
                {
                    u16CmdCnt = u16FireIdx - pstClient->u16RPoint + 1;
                    u16CmdCnt = KHal_XC_MLoad_Get_Depth(enClient,u16CmdCnt);
                    u64EndCmd = (MS_U64) MS_MLOAD_END_CMD(enClient);

                    KDrv_XC_MLoad_SetCmd_64Bits(enMloadType, enClient, CmdBufAddr, u64EndCmd);

                    Chip_Flush_Miu_Pipe();

                    CmdBufAddr = pstClient->PhyAddr + pstClient->u16RPoint * MS_MLOAD_CMD_LEN_64BITS;
                    MLDBG(printk("CmdBufAddr = 0x%lx, u16CmdCnt = 0x%lx\n", CmdBufAddr, u16CmdCnt));

                    KDrv_XC_MLoad_Trigger(E_MLOAD, enClient, CmdBufAddr, u16CmdCnt);

                    bRet = TRUE;
                    MLDBG(printk("after fire u16WPoint=%u u16RPoint=%u u16FPoint=%u\n",
                                 pstClient->u16WPoint,
                                 pstClient->u16RPoint,
                                 pstClient->u16FPoint));
                }
#endif
            }
            else
            {
                MS_U32 u32EndCmd;

                //check the last command to see whether it is null command
                CmdBufAddr = pstClient->PhyAddr + u16FireIdx * MS_MLOAD_CMD_LEN;
                u32EndCmd = KDrv_XC_MLoad_GetCmd(enMloadType, enClient, CmdBufAddr);
                if(u32EndCmd != MS_MLOAD_NULL_CMD)
                {
                    MLDBG_WARN(printk("WPoint=%d, FirePoint=%d, RPoint=%d\n"
                                      , pstClient->u16WPoint
                                      , pstClient->u16FPoint
                                      , pstClient->u16RPoint));
                    MLDBG_WARN(printk("KickOff: Non Null Cmd. Trigger is not executed!!\n"));
                    bRet = FALSE;
                }
                else
                {
                    u16CmdCnt = u16FireIdx - pstClient->u16RPoint + 1;

                    u32EndCmd = MS_MLOAD_END_CMD(enClient);
                    MLDBG(printk("CmdBufAddr = 0x%lx, u32EndCmd = 0x%lx\n", CmdBufAddr, u32EndCmd));
                    KDrv_XC_MLoad_SetCmd(enMloadType, enClient, CmdBufAddr, u32EndCmd);

                    Chip_Flush_Miu_Pipe();

                    bRet = TRUE;
                    MLDBG(printk("after fire u16WPoint=%u u16RPoint=%u u16FPoint=%u\n",
                                 pstClient->.u16WPoint,
                                 pstClient->u16RPoint,
                                 pstClient->u16FPoint));
                }
            }

        }
        else if(pstClient->u16FPoint == pstClient->u16RPoint &&
                pstClient->u16FPoint == pstClient->u16WPoint-1)
        {
            bRet = TRUE;
        }
        else
        {
            MLDBG(printk("MenuLoad: WPoint=%u, FPoint=%u, RPoint=%u !!\n",
                         pstClient->u16WPoint,
                         pstClient->u16FPoint,
                         pstClient->u16RPoint));
            bRet = FALSE;
        }
    }
    return bRet;
}
#endif

MS_BOOL KDrv_XC_MLoad_KickOff(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    //MS_MLOAD_END_CMD use instance, does not remove this
    MS_BOOL bRet;
    MS_U16 u16CmdCnt;
    MS_U16 u16QueueCmd, u16FireIdx;
    MS_PHY CmdBufAddr;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    MLDBG(printk("%s: mload[%d] client = %d\n",__FUNCTION__, enMloadType, enClient));
    if(KDrv_XC_MLoad_Check_Done(enMloadType, enClient) && pstClient->u16FPoint)
    {
        if(pstClient->u16FPoint > pstClient->u16RPoint)
        {
            u16QueueCmd = pstClient->u16WPoint - pstClient->u16RPoint;

            if(u16QueueCmd > MS_MLOAD_MAX_CMD_CNT(enClient))
            {
                MLDBG_WARN(printk("MLOAD[%d] Queue Too Many !!!!!!!!!!!!!!!!!\n", enMloadType));
                MLDBG_WARN(printk("MLOAD[%d] WPoint=%d, FirePoint=%d, RPoint=%d\n",enMloadType
                                  ,pstClient->u16WPoint
                                  ,pstClient->u16FPoint
                                  ,pstClient->u16RPoint));
            }

            u16FireIdx = pstClient->u16FPoint;
            MLDBG(printk("MLOAD[%d] u16QueueCmd= %u, u16FireIdx= %u\n", enMloadType, u16QueueCmd, u16FireIdx));

            if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
            {
                MS_U64 u64EndCmd;
                MS_PHY u32FireMemShift = 0;
                MS_PHY u32ReadMemShift = 0;

#if ENABLE_64BITS_SPREAD_MODE
                if((pstClient->u16FPoint % 2) != 0)
                {

                    u32FireMemShift = _MDrv_XC_GetMLoadMemOffset(u16FireIdx);
                }

                CmdBufAddr = pstClient->PhyAddr + u32FireMemShift;


                u64EndCmd = KDrv_XC_MLoad_GetCmd_64Bits(enMloadType, enClient, CmdBufAddr);
                if(u64EndCmd != MS_MLOAD_NULL_CMD_SPREAD_MODE)
                {
                    MLDBG_WARN(printk("MLOAD[%d] WPoint=%d, FirePoint=%d, RPoint=%d\n", enMloadType
                                      , pstClient->u16WPoint
                                      , pstClient->u16FPoint
                                      , pstClient->u16RPoint));
                    MLDBG_WARN(printk("MLOAD[%d] KickOff: Non Null Cmd. Trigger is not executed!!\n", enMloadType));
                    bRet = FALSE;
                }
                else
                {
                    u16CmdCnt = u16FireIdx - pstClient->u16RPoint + 1;
                    u16CmdCnt = KHal_XC_MLoad_Get_Depth(enClient,u16CmdCnt);
                    if (enMloadType == E_MLOAD)
                    {
                        if((enClient/CLIENT_NUM)== 0)
                        {
                            u64EndCmd = MS_MLOAD_END_CMD_SPREAD_MODE(((MS_U64)u16CmdCnt));
                        }
                        else
                        {
                            u64EndCmd = MS_MLOAD_END_CMD_DEV1_SPREAD_MODE(((MS_U64)u16CmdCnt));
                        }
                    }
#if ENABLE_NMLOAD
                    else if (enMloadType == E_MLOAD2)
                    {
                        if((enClient/CLIENT_NUM)== 0)
                        {
                            u64EndCmd = MS_MLOAD2_END_CMD_SPREAD_MODE(((MS_U64)u16CmdCnt));
                        }
                        else
                        {
                            u64EndCmd = MS_MLOAD2_END_CMD_DEV1_SPREAD_MODE(((MS_U64)u16CmdCnt));
                        }
                    }
#endif
                    KDrv_XC_MLoad_SetCmd_64Bits(enMloadType, enClient, CmdBufAddr, u64EndCmd);

                    getnstimeofday(&Ts13);
                    Chip_Flush_Miu_Pipe();
                    getnstimeofday(&Ts14);

                    u32ReadMemShift = _MDrv_XC_GetMLoadMemOffset(pstClient->u16RPoint);

                    CmdBufAddr = (pstClient->PhyAddr + u32ReadMemShift);
                    MLDBG(printk("MLOAD[%d] CmdBufAddr = 0x%lx, u16CmdCnt = 0x%lx\n", enMloadType, CmdBufAddr, u16CmdCnt));

                    KDrv_XC_MLoad_Trigger(enMloadType, enClient, CmdBufAddr, u16CmdCnt);
#if MLOAD_ASYNCHRONOUS == 0
                    pstClient->u16RPoint = u16FireIdx + 1;
#endif
                    bRet = TRUE;
                    MLDBG(printk("MLOAD[%d] after fire u16WPoint=%u u16RPoint=%u u16FPoint=%u\n", enMloadType,
                                 pstClient->u16WPoint,
                                 pstClient->u16RPoint,
                                 pstClient->u16FPoint));
                    getnstimeofday(&Ts15);
                }
#else
                //check the last command to see whether it is null command
                CmdBufAddr = pstClient->PhyAddr + u16FireIdx * MS_MLOAD_CMD_LEN_64BITS;
                u64EndCmd = KDrv_XC_MLoad_GetCmd_64Bits(enMloadType, enClient, CmdBufAddr);
                if(u64EndCmd != MS_MLOAD_NULL_CMD)
                {
                    MLDBG_WARN(printk("MLOAD[%d] WPoint=%d, FirePoint=%d, RPoint=%d\n", enMloadType
                                      , pstClient->u16WPoint
                                      , pstClient->u16FPoint
                                      , pstClient->.u16RPoint));
                    MLDBG_WARN(printk("MLOAD[%d] KickOff: Non Null Cmd. Trigger is not executed!!\n", enMloadType));
                    bRet = FALSE;
                }
                else
                {
                    u16CmdCnt = u16FireIdx - pstClient->u16RPoint + 1;
                    u16CmdCnt = KHal_XC_MLoad_Get_Depth(enClient,u16CmdCnt);
                    u64EndCmd = (MS_U64) MS_MLOAD_END_CMD(enClient);

                    KDrv_XC_MLoad_SetCmd_64Bits(enMloadType, enClient, CmdBufAddr, u64EndCmd);

                    Chip_Flush_Miu_Pipe();

                    CmdBufAddr = pstClient->PhyAddr + pstClient->u16RPoint * MS_MLOAD_CMD_LEN_64BITS;
                    MLDBG(printk("MLOAD[%d] enMloadTypeCmdBufAddr = 0x%lx, u16CmdCnt = 0x%lx\n", enMloadType, CmdBufAddr, u16CmdCnt));

                    KDrv_XC_MLoad_Trigger(enMloadType, enClient, CmdBufAddr, u16CmdCnt);
                    pstClient->u16RPoint = u16FireIdx + 1;
                    bRet = TRUE;
                    MLDBG(printk("MLOAD[%d] after fire u16WPoint=%u u16RPoint=%u u16FPoint=%u\n", enMloadType,
                                 pstClient->u16WPoint,
                                 pstClient->u16RPoint,
                                 pstClient->u16FPoint));
                }
#endif
                getnstimeofday(&Ts16);
            }
            else
            {
                MS_U32 u32EndCmd;

                //check the last command to see whether it is null command
                CmdBufAddr = pstClient->PhyAddr + u16FireIdx * MS_MLOAD_CMD_LEN;
                u32EndCmd = KDrv_XC_MLoad_GetCmd(enMloadType, enClient, CmdBufAddr);
                if(u32EndCmd != MS_MLOAD_NULL_CMD)
                {
                    MLDBG_WARN(printk("MLOAD[%d] WPoint=%d, FirePoint=%d, RPoint=%d\n", enMloadType
                                      , pstClient->u16WPoint
                                      , pstClient->u16FPoint
                                      , pstClient->u16RPoint));
                    MLDBG_WARN(printk("MLOAD[%d] KickOff: Non Null Cmd. Trigger is not executed!!\n", enMloadType));
                    bRet = FALSE;
                }
                else
                {
                    u16CmdCnt = u16FireIdx - pstClient->u16RPoint + 1;

                    u32EndCmd = MS_MLOAD_END_CMD(enClient);
                    MLDBG(printk("MLOAD[%d] CmdBufAddr = 0x%lx, u32EndCmd = 0x%lx\n", enMloadType, CmdBufAddr, u32EndCmd));
                    KDrv_XC_MLoad_SetCmd(enMloadType, enClient, CmdBufAddr, u32EndCmd);

                    Chip_Flush_Miu_Pipe();

                    CmdBufAddr = pstClient->PhyAddr + pstClient->u16RPoint * MS_MLOAD_CMD_LEN;
                    MLDBG(printk("MLOAD[%d] CmdBufAddr = 0x%lx, u16CmdCnt = 0x%lx\n", enMloadType, CmdBufAddr, u16CmdCnt));
                    KDrv_XC_MLoad_Trigger(enMloadType, enClient, CmdBufAddr, u16CmdCnt);
                    pstClient->u16RPoint = u16FireIdx + 1;
                    bRet = TRUE;
                    MLDBG(printk("MLOAD[%d] after fire u16WPoint=%u u16RPoint=%u u16FPoint=%u\n", enMloadType,
                                 pstClient->u16WPoint,
                                 pstClient->u16RPoint,
                                 pstClient->u16FPoint));
                }
            }

        }
        else if(pstClient->u16FPoint == pstClient->u16RPoint &&
                pstClient->u16FPoint == pstClient->u16WPoint-1)
        {
            bRet = TRUE;
        }
        else
        {
            MLDBG(printk("MLOAD[%d] MenuLoad: WPoint=%u, FPoint=%u, RPoint=%u !!\n", enMloadType,
                         pstClient->u16WPoint,
                         pstClient->u16FPoint,
                         pstClient->u16RPoint));
            bRet = FALSE;
        }
    }
    else
    {
        bRet = TRUE;
    }

    return bRet;
}

void KDrv_XC_MLoad_AddNull(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    MLDBG(printk("%s: MLOAD[%d] \n",__FUNCTION__, enMloadType));
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    do
    {
        if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
        {
            if(ENABLE_64BITS_SPREAD_MODE)
            {
                KDrv_XC_MLoad_Add_64Bits_Cmd(enMloadType, enClient, (MS_U64)MS_MLOAD_NULL_CMD_SPREAD_MODE, 0xFFFF);
            }
            else
            {
                KDrv_XC_MLoad_Add_64Bits_Cmd(enMloadType, enClient, (MS_U64)MS_MLOAD_NULL_CMD, 0xFFFF);
            }
        }
        else
        {
            KDrv_XC_MLoad_Add_32Bits_Cmd(enMloadType, enClient, (MS_U32)MS_MLOAD_NULL_CMD, 0xFFFF);
        }

    }
    while (pstClient->u16WPoint % MS_MLOAD_CMD_ALIGN != 0);
}

//-------------------------------------------------------------------------------------------------
/// Initialize the Menuload
/// @param  PhyAddr                 \b IN: the physical address for the menuload
/// @param  u32BufByteLen           \b IN: the buffer length of the menu load commands
//-------------------------------------------------------------------------------------------------
void KApi_XC_MLoad_Init(EN_MLOAD_CLIENT_TYPE enClient, MS_U64 PhyAddr, MS_U32 u32BufByteLen)
{
    MLDBG(printk("%s: MLOAD[%d] client[%d] 0x%lx, 0x%x\n", __FUNCTION__, E_MLOAD, enClient, PhyAddr, u32BufByteLen));
    if(KDrv_XC_MLoad_GetCaps(E_MLOAD))
    {
        _MLOAD_ENTRY(enClient);

        MS_U32 u32MemSize = u32BufByteLen;
#if ENABLE_NMLOAD
        MS_U32 u32RemainSize = 0;

        if (enClient == E_CLIENT_MAIN_HDR)
        {
            if (u32BufByteLen > MS_MLOAD2_CFD_MEM_SIZE)
            {
                u32RemainSize = u32BufByteLen - MS_MLOAD2_CFD_MEM_SIZE;
                u32MemSize = MS_MLOAD2_CFD_MEM_SIZE;
            }
            else
            {
                u32RemainSize = 0;
                u32MemSize = u32BufByteLen;
            }
        }
#endif
        if(_client[enClient].bInit == FALSE) //prevent vmalloc leak for ioremap
        {
            _PA2VA(E_MLOAD, enClient, PhyAddr, u32MemSize);
            _client[enClient].bInit = TRUE;
        }

        _client[enClient].bEnable = FALSE;
        _client[enClient].u16RPoint = 0;
        _client[enClient].u16WPoint = 0;
        _client[enClient].u16FPoint = 0;
        if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
        {
            if ((u32BufByteLen / MS_MLOAD_CMD_LEN_64BITS) < 0x3FFFC)
            {
                _client[enClient].u16MaxCmdCnt = (MS_U16)(u32BufByteLen / MS_MLOAD_CMD_LEN_64BITS);
            }
            else
            {
                _client[enClient].u16MaxCmdCnt = 0xFFFF;
                MLDBG_WARN(printk("MLOAD[%d] client[%d] Too Large menuload u32BufByteLen. u16MaxCmdCnt might overflow!\n", E_MLOAD, enClient));
            }
        }
        else
        {
            if ((u32BufByteLen / MS_MLOAD_CMD_LEN) < 0xFFFF)
            {
                _client[enClient].u16MaxCmdCnt = (MS_U16)(u32BufByteLen / MS_MLOAD_CMD_LEN);
            }
            else
            {
                _client[enClient].u16MaxCmdCnt = 0xFFFF;
                MLDBG_WARN(printk("MLOAD[%d] client[%d] Too Large menuload u32BufByteLen. u16MaxCmdCnt might overflow!\n", E_MLOAD, enClient));
            }
        }
        _client[enClient].PhyAddr = PhyAddr;
        _mload_private.g_u32MLoadPhyAddr_Suspend = PhyAddr;
        _mload_private.g_u32MLoadBufByteLen_Suspend = u32BufByteLen;
        KDrv_XC_MLoad_Init(enClient ,PhyAddr);

        _MLOAD_RETURN(enClient);

#if ENABLE_NMLOAD
        if (enClient == E_CLIENT_MAIN_HDR)
        {
            // init MLOAD2 memory
            if (u32RemainSize > 0)
            {
                KApi_XC_NMLoad_Init(E_MLOAD2, enClient, PhyAddr + u32MemSize, u32RemainSize);
            }
        }
#endif
    }
}


//-------------------------------------------------------------------------------------------------
/// Enable/Disable the MLoad
/// @return  void
//-------------------------------------------------------------------------------------------------
void KDrv_XC_MLoad_Enable(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_BOOL bEnable)
{
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    if (pstClient->bInit)
    {
#ifdef PIP_PATCH_USING_SC1_MAIN_AS_SC0_SUB
#if PIP_PATCH_USING_SC1_MAIN_AS_SC0_SUB
        if( _mload_private.g_u32MLoadPhyAddr_Suspend != 0)
#endif
#endif
        {
            pstClient->bEnable = bEnable;
        }
    }
    else
    {
        MLDBG_WARN(printk("%s: Mload[%d] client %d is not inited.\n", __func__, enMloadType, enClient));
    }
}

void KApi_XC_MLoad_Enable(EN_MLOAD_CLIENT_TYPE enClient, MS_BOOL bEnable)
{
    _MLOAD_ENTRY(enClient);
    KDrv_XC_MLoad_Enable(E_MLOAD, enClient, bEnable);
    _MLOAD_RETURN(enClient);

#if ENABLE_NMLOAD
    // enable mload2
    if (enClient == E_CLIENT_MAIN_HDR)
    {
        KApi_XC_NMLoad_Enable(E_MLOAD2, enClient, bEnable);
    }
#endif
}


//-------------------------------------------------------------------------------------------------
/// Get the status of MLoad
/// @return  MLOAD_TYPE
//-------------------------------------------------------------------------------------------------
EN_KDRV_MLOAD_TYPE KDrv_XC_MLoad_GetStatus(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    EN_KDRV_MLOAD_TYPE type = E_KDRV_MLOAD_UNSUPPORTED;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    if (KDrv_XC_MLoad_GetCaps(enMloadType))
    {
        if (pstClient->bEnable)
        {
            type = E_KDRV_MLOAD_ENABLED;
        }
        else
        {
            type = E_KDRV_MLOAD_DISABLED;
        }
    }

    return type;
}

EN_KDRV_MLOAD_TYPE KApi_XC_MLoad_GetStatus(EN_MLOAD_CLIENT_TYPE enClient)
{
    EN_KDRV_MLOAD_TYPE eMode = E_KDRV_MLOAD_UNSUPPORTED;

    eMode = KDrv_XC_MLoad_GetStatus(E_MLOAD, enClient);

    return eMode;
}

//-------------------------------------------------------------------------------------------------
/// Fire the Menuload commands
/// @return  TRUE if succeed, FALSE if failed
//-------------------------------------------------------------------------------------------------
MS_BOOL KApi_XC_MLoad_Fire(EN_MLOAD_CLIENT_TYPE enClient, MS_BOOL bImmediate)
{
    MS_BOOL bReturn = FALSE;
    _MLOAD_ENTRY(enClient);
    _MLOAD_FIRE_ENTRY(enClient);
    _bImmediate[E_MLOAD][enClient] = bImmediate;
    bReturn = KDrv_XC_MLoad_Fire(E_MLOAD, enClient, bImmediate);
    _MLOAD_FIRE_RETURN(enClient);
    _MLOAD_RETURN(enClient);
    return bReturn;
}

MS_BOOL KDrv_XC_MLoad_Fire(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_BOOL bImmediate)
{
    MS_BOOL bRet = FALSE;
    MS_U16 u16Boundary = 16, i = 0;

    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    // SC1 mload may not enable
    if (E_KDRV_MLOAD_ENABLED != KDrv_XC_MLoad_GetStatus(enMloadType, enClient))
    {
        return bRet;
    }
    MLDBG(printk("%s, MLoad[%d] u16WPoint=%u u16RPoint=%u u16FPoint=%u\n", __FUNCTION__, enMloadType,
                 pstClient->u16WPoint,
                 pstClient->u16RPoint,
                 pstClient->u16FPoint));

    if (  KDrv_XC_MLoad_BufferEmpty(enMloadType, enClient)
#if (MLOAD_ASYNCHRONOUS == 0)
          || pstClient->bCleared
#endif
       )
    {
        pstClient->u16RPoint = 0;
        pstClient->u16WPoint = 0;
        pstClient->u16FPoint = 0;
        pstClient->bCleared = TRUE;
        MLDBG(printk("%s MLoad[%d] buffer is empty, Skip menuload fire.\n", __FUNCTION__, enMloadType));
        bRet = TRUE;
    }
    else
    {
        // 16 entry as boundary for menuload to prevent cmd sent unpredictable.
        for (i = 0 ; i < u16Boundary ; i++)
        {
            if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND,enClient/CLIENT_NUM))
            {
                if(ENABLE_64BITS_SPREAD_MODE)
                {
                    KDrv_XC_MLoad_Add_64Bits_Cmd(enMloadType, enClient,(MS_U64)MS_MLOAD_NULL_CMD_SPREAD_MODE, 0xFFFF);
                }
                else
                {
                    KDrv_XC_MLoad_Add_64Bits_Cmd(enMloadType, enClient,(MS_U64)MS_MLOAD_NULL_CMD, 0xFFFF);
                }
            }
            else
            {
                KDrv_XC_MLoad_Add_32Bits_Cmd(enMloadType, enClient,(MS_U32)MS_MLOAD_NULL_CMD, 0xFFFF);
            }

        }

        getnstimeofday(&Ts11);
        KDrv_XC_MLoad_AddNull(enMloadType, enClient);
        getnstimeofday(&Ts12);

        pstClient->u16FPoint = pstClient->u16WPoint - 1;

        MLDBG(printk("MLoad[%d] u16WPoint=%u u16RPoint=%u u16FPoint=%u\n", enMloadType,
                     pstClient->u16WPoint,
                     pstClient->u16RPoint,
                     pstClient->u16FPoint));
#if MLOAD_ASYNCHRONOUS
        KDrv_XC_MLoad_AddEndCmd(enMloadType, enClient);
        _stMloadFireInfo[enClient/EN_MLoad_HW_NUM].enClient_type = enClient;
        // fix me, set value must before call wakeup,because:
        // ************************************ wrong *************************************************
        // set condition after call wakeup,this is wait_event_interruptible flow:
        // for (;;) {
        //  prepare_to_wait(&wq, &__wait, TASK_INTERRUPTIBLE);    // 3--> condition is true ,but no use
        //  if (condition)                                        // 1--> maybe condition is false
        //     break;
        //     if (!signal_pending(current)) {
        //         schedule();
        //         continue;                                     // 2-->here confition is true
        //     }
        //     ret = -ERESTARTSYS;
        //     break;
        // }
        // ***************************************right**********************************************
        // set condition before call wakeup,this is wait_event_interruptible flow:
        // for (;;) {
        //     prepare_to_wait(&wq, &__wait, TASK_INTERRUPTIBLE);    // 1--> condition is true
        //     if (condition)                                        // 2--> condition is true
        //         break;                                            //3--> break
        //     if (!signal_pending(current)) {
        //         schedule();
        //         continue;
        //     }
        //     ret = -ERESTARTSYS;
        //     break;
        // }
        // *************************************************************************************
        _stMloadFireInfo[enClient/EN_MLoad_HW_NUM].bMloadFire = TRUE;
        wake_up_interruptible(&_stMloadFireInfo[enClient/EN_MLoad_HW_NUM].stMloadWaitQueue);
    }
    return TRUE;
#else
        KDrv_XC_MLoad_KickOff(enMloadType, enClient);

        //if(bImmediate)
        if(_bImmediate[enMloadType][enClient])
        {
            // we need to make sure the value is
            MS_U32 u32Delayms = 0;
            while((KDrv_XC_MLoad_Check_Done(enMloadType, enClient) == FALSE) && u32Delayms < 600)
            {
                _DelayTask(1);
                u32Delayms++;
            }

            // Patch HW outpu Vsync plus width too short and cause MLoad missing.
            // T3 U06 will fix it.
            if(KDrv_XC_MLoad_Check_Done(enMloadType, enClient) == FALSE)
            {
                MLDBG_WARN(printk("MLoad[%d], TimeOut\n", enMloadType));
                KHal_XC_MLoad_set_on_off(enClient,DISABLE);

                bRet = FALSE;
            }
            else
            {
                bRet = TRUE;
            }
            pstClient->u16RPoint = 0;
            pstClient->u16WPoint = 0;
            pstClient->u16FPoint = 0;
            pstClient->bCleared = TRUE;
        }
        else
        {
            pstClient->u16RPoint = 0;
            pstClient->u16WPoint = 0;
            pstClient->u16FPoint = 0;
            pstClient->bCleared = TRUE;
            bRet = TRUE;
        }
    }

    getnstimeofday(&Ts17);

    return bRet;
#endif
}

//-------------------------------------------------------------------------------------------------
/// Write command to the Menuload buffer by WORD
/// @param  u32Addr                 \b IN: the address (sub-bank + 8-bit address)
/// @param  u16Data                 \b IN: the data
/// @param  u16Mask                 \b IN: the mask
/// @return  TRUE if succeed, FALSE if failed
/// how to use this function
/// if you want write REG_SC_BK01_01_L bitN and bitM
/// 1. you must:
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(N)|BIT(M), BIT(N)|BIT(M));
/// MApi_XC_MLoad_Fire();
/// 2. you mustn't:
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(N), BIT(N));
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(M), BIT(M));
/// MApi_XC_MLoad_Fire();
/// only REG_SC_BKXX_XX_L bitM will be download reg if you do like step2
//-------------------------------------------------------------------------------------------------
MS_BOOL KApi_XC_MLoad_WriteCmd(EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_BOOL bReturn = FALSE;
    _MLOAD_ENTRY(enClient);
    bReturn = KDrv_XC_MLoad_WriteCmd(E_MLOAD, enClient, u32Addr, u16Data, u16Mask);
    _MLOAD_RETURN(enClient);
    return bReturn;
}

//-------------------------------------------------------------------------------------------------
/// how to use this function
/// if you want write REG_SC_BKXX_XX_L bitN and bitM
/// 1. you must:
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(N)|BIT(M), BIT(N)|BIT(M));
/// MApi_XC_MLoad_Fire();
/// 2. you mustn't:
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(N), BIT(N));
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(M), BIT(M));
/// MApi_XC_MLoad_Fire();
/// only REG_SC_BKXX_XX_L bitM will be download reg if you do like step2
//-------------------------------------------------------------------------------------------------

MS_BOOL KDrv_XC_MLoad_WriteCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_BOOL bRet;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

#ifdef A3_MLG  // need to refine later, test the capibility first
    MS_U8 u8MLoadMIUSel = KHal_XC_MLoad_get_miusel();

    if (u8MLoadMIUSel != pXCResourcePrivate->stdrvXC_MVideo_Context.stdrvXC_Menuload[psXCInstPri->u32DeviceID].u8XCMLoadMIUSel)
        KHal_XC_MLoad_set_miusel(pXCResourcePrivate->stdrvXC_MVideo_Context.stdrvXC_Menuload[psXCInstPri->u32DeviceID].u8XCMLoadMIUSel);
#endif
    MS_BOOL bReturn;

    if((_bImmediate[enMloadType][E_CLIENT_MAIN_XC]==FALSE) && (KDrv_XC_MLoad_Check_Done(enMloadType, E_CLIENT_MAIN_XC) == FALSE) 
        && (enMloadType == E_MLOAD ))
    {
        KDrv_XC_MLoad_Wait_HW_Done(enMloadType, enClient);
    }
    if((_bImmediate[enMloadType][E_CLIENT_MAIN_HDR]==FALSE) && (KDrv_XC_MLoad_Check_Done(enMloadType, E_CLIENT_MAIN_HDR) == FALSE) 
        && (enMloadType == E_MLOAD ))
    {
        KDrv_XC_MLoad_Wait_HW_Done(enMloadType, enClient);
    }
     K_XC_GETDS_Info stDsInfo;
     KApi_XC_GetDSInfo(0, &stDsInfo, E_KDRV_MAIN_WINDOW);
     if(stDsInfo.bDynamicScalingEnable == TRUE)
     {
	    if((_bImmediate[E_CLIENT_MAIN_HDR]==FALSE) && (KDrv_XC_MLoad_Check_Done(enMloadType, E_CLIENT_MAIN_HDR) == FALSE)&&(MHal_XC_GetOSDSettingFlag()==TRUE))
	    {
	        KDrv_XC_MLoad_Wait_HW_Done(enMloadType, enClient);
		  MHal_XC_SetOSDSettingFlag(FALSE);
	    }
     }

    bReturn = KDrv_XC_MLoad_WriteCommand(enMloadType, enClient, u32Addr, u16Data, u16Mask);
    if(bReturn == TRUE)
    {
        pstClient->bCleared = FALSE;
        //printk("MLoad[%d] Cmd %4x %04x %04x \n", enMloadType, u32Addr, u16Data, u16Mask);
        bRet = TRUE;
    }
    else
    {
        MLDBG(printk("%d, MLoad[%d] Fail: %04lx %04x %04x \n", __LINE__, enMloadType, u32Addr, u16Data, u16Mask));
        bRet = FALSE;
    }

    return bRet;
}

MS_BOOL KApi_XC_MLoad_WriteCmd_And_Fire(EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_BOOL bRet = TRUE;
    _MLOAD_ENTRY(enClient);

    if (!KDrv_XC_MLoad_WriteCmd(E_MLOAD, enClient, u32Addr, u16Data, u16Mask))
    {
        bRet = FALSE;
        MLDBG(printk("%d, MLoad[%d] Fail: Cmd %4lx %x %x \n", __LINE__, E_MLOAD, u32Addr, u16Data, u16Mask));
    }
    else
    {
        _MLOAD_FIRE_ENTRY(enClient);
        if (!KDrv_XC_MLoad_Fire(E_MLOAD, enClient, TRUE))
        {
            bRet = FALSE;
            MLDBG(printk("MLoad[%d] Fire Error!!!!\n ", E_MLOAD));
        }
        _MLOAD_FIRE_RETURN(enClient);
    }
    _MLOAD_RETURN(enClient);

    return bRet;
}


MS_BOOL KApi_XC_MLoad_WriteCmds_And_Fire(EN_MLOAD_CLIENT_TYPE enClient,MS_U32 *pu32Addr, MS_U16 *pu16Data, MS_U16 *pu16Mask, MS_U16 u16CmdCnt)
{
    MS_BOOL bRet = TRUE;
    MS_U16 i = 0, j = 0;
    _MLOAD_ENTRY(enClient);

    //Note: If there are some same regs in the Mload cmd queue,
    //  MLoad will use the value of the last reg address to fire. others same regs will be ignore.
    // So we handle this situation here, to combine every same reg's setting value to the same.
    for(i=0; i<u16CmdCnt; i++)
    {
        for(j=i+1; j<u16CmdCnt; j++)
        {
            if(pu32Addr[i] == pu32Addr[j])
            {
                pu16Mask[i] |= pu16Mask[j];
                pu16Data[i] |= pu16Data[j];
                pu16Mask[j] = pu16Mask[i];
                pu16Data[j] = pu16Data[i];
            }
        }
    }

    for(i=0; i<u16CmdCnt; i++)
    {

        if (!KDrv_XC_MLoad_WriteCmd(E_MLOAD, enClient, pu32Addr[i], pu16Data[i], pu16Mask[i]))
        {
            bRet = FALSE;
            MLDBG(printk("%d, MLoad[%d] Fail: Cmd %4lx %x %x \n", __LINE__, E_MLOAD, pu32Addr[i], pu16Data[i], pu16Mask[i]));
        }
    }
    _MLOAD_FIRE_ENTRY(enClient);
    if (!KDrv_XC_MLoad_Fire(E_MLOAD, enClient, TRUE))
    {
        bRet = FALSE;
        MLDBG(printk("MLoad[%d] Fire Error!!!!\n ", E_MLOAD));
    }
    _MLOAD_FIRE_RETURN(enClient);
    _MLOAD_RETURN(enClient);

    return bRet;
}

//___|T_________________........__|T____ VSync
//__________|T__________________         ATP(refer the size befor memory to cal the pip sub and main length)
//_________________|T___________         Disp

//Generate TRAIN_TRIG_P from delayed line of Vsync(Setting the delay line for Auto tune area)(monaco ip DI point add trig engine)
//Generate DISP_TRIG_P from delayed line of Vsync(Setting the delay line for Display area)(monaco ip DI point add trig engine)
MS_BOOL KDrv_XC_MLoad_set_IP_trig_p(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 u16train, MS_U16 u16disp)
{
#ifdef HW_DESIGN_4K2K_VER
#if (HW_DESIGN_4K2K_VER == 4)
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x57,0x1A), u16train, 0x0FFF);
    SC_W2BYTEMSK(enClient, XC_ADDR_L(0x57,0x1B), u16disp,  0x0FFF);
#endif
#endif
    return TRUE;
}

//Get the delay line for Auto tune area
//Get the delay line for Display area
MS_BOOL KDrv_XC_MLoad_get_IP_trig_p(EN_MLOAD_CLIENT_TYPE enClient,MS_U16 *pu16Train, MS_U16 *pu16Disp)
{
#ifdef HW_DESIGN_4K2K_VER
#if (HW_DESIGN_4K2K_VER == 4)
    *pu16Train = SC_R2BYTEMSK(enClient, XC_ADDR_L(0x57,0x1A), 0x0FFF);
    *pu16Disp = SC_R2BYTEMSK(enClient, XC_ADDR_L(0x57,0x1B), 0x0FFF);
#endif
#endif
    return TRUE;
}

void KDrv_XC_MLoad_Add_32Bits_Cmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Cmd, MS_U16 u16Mask)
{
    MS_U32 *pu32Addr = NULL;
    MS_PHY DstAddr;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    if (  (ENABLE_MLOAD_SAME_REG_COMBINE == 1)
          &&(u32Cmd != MS_MLOAD_NULL_CMD))
    {
        MS_U16 u16WPoint = pstClient->u16WPoint;
        MS_U16 u16CmdIndex = pstClient->u16RPoint;
        MS_U32 u32CurrentAddr = 0;
        MS_U16 u16CurrentData = 0;
        MS_U32 u32OldAddr = 0;
        MS_U16 u16OldData = 0;
        for (u16CmdIndex = 0; u16CmdIndex < u16WPoint; u16CmdIndex++)
        {
            DstAddr = pstClient->PhyAddr + u16CmdIndex * MS_MLOAD_CMD_LEN;
            pu32Addr = (MS_U32 *)_GET_PA2VA(enMloadType, enClient, DstAddr);
            KHal_XC_MLoad_parsing_32bits_subBankMode(enClient, u32Cmd, &u32CurrentAddr, &u16CurrentData);
            KHal_XC_MLoad_parsing_32bits_subBankMode(enClient, *pu32Addr, &u32OldAddr, &u16OldData);
            if (u32OldAddr == u32CurrentAddr)
            {
                if (u16OldData != u16CurrentData)
                {
                    MS_U16 u16NewData = 0;
                    u16NewData = (u16OldData & ~u16Mask) | (u16CurrentData & u16Mask);
                    MLDBG(printk("[%s][line %d] MLoad[%d] Old Cmd=%x \n",__FUNCTION__, __LINE__, enMloadType, u32Cmd));
                    u32Cmd =  KHal_XC_MLoad_Gen_32bits_subBankMode(enClient, u32OldAddr, u16NewData, 0xFFFF);
                    MLDBG(printk("[%s][line %d] MLoad[%d] Old Cmd=%x \n",__FUNCTION__, __LINE__, enMloadType, u32Cmd));
                    *pu32Addr = u32Cmd;
                }

                return;
            }
        }
    }
    DstAddr = pstClient->PhyAddr + pstClient->u16WPoint * MS_MLOAD_CMD_LEN;
    pu32Addr = (MS_U32 *)_GET_PA2VA(enMloadType, enClient, DstAddr);
    *pu32Addr = u32Cmd;
    pstClient->u16WPoint++;
}

void KDrv_XC_MLoad_Add_64Bits_Cmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U64 u64Cmd, MS_U16 u16Mask)
{
    MS_U64 *pu64Addr = NULL;
    MS_PHY DstAddr;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    MS_U32 u32WriteMemShift = 0;
    if (  (ENABLE_MLOAD_SAME_REG_COMBINE == 1)
          &&(u64Cmd != MS_MLOAD_NULL_CMD_SPREAD_MODE)
          &&(MLOAD_ASYNCHRONOUS == 0))
    {
        MS_U16 u16CmdIndex = pstClient->u16RPoint;
        MS_U16 u16WPoint = pstClient->u16WPoint;

        for (; u16CmdIndex < u16WPoint; u16CmdIndex++)
        {
            MS_U32 u32CurrentAddr = 0;
            MS_U16 u16CurrentData = 0;
            MS_U32 u32OldAddr = 0;
            MS_U16 u16OldData = 0;
            u32WriteMemShift = _MDrv_XC_GetMLoadMemOffset(u16CmdIndex);

            DstAddr = pstClient->PhyAddr + u32WriteMemShift;
            pu64Addr = (MS_U64 *)_GET_PA2VA(enMloadType, enClient, DstAddr);

            KHal_XC_MLoad_parsing_64bits_spreadMode_NonXC(*pu64Addr, &u32OldAddr, &u16OldData);
            KHal_XC_MLoad_parsing_64bits_spreadMode_NonXC(u64Cmd, &u32CurrentAddr, &u16CurrentData);
            if (  (u32OldAddr == u32CurrentAddr)
                  &&((u32CurrentAddr&REG_SCALER_BASE) == REG_SCALER_BASE)) //only xc reg check
            {
                if (u16OldData != u16CurrentData)
                {
                    MS_U16 u16NewData = 0;
                    u16NewData = (u16OldData & ~u16Mask) | (u16CurrentData & u16Mask);
                    MLDBG(printk("[%s][line %d] MLoad[%d] Old Cmd=%llx \n",__FUNCTION__, __LINE__, enMloadType, u64Cmd));
                    u64Cmd = KHal_XC_MLoad_Gen_64bits_spreadMode_NonXC((u32CurrentAddr>>8)&0xFFFF, u32CurrentAddr&0xFF, u16NewData, 0xFFFF);
                    MLDBG(printk("[%s][line %d] MLoad[%d] New Cmd=%llx \n",__FUNCTION__, __LINE__, enMloadType, u64Cmd));
                    *pu64Addr = u64Cmd;
                }
                return;
            }
        }
    }
    getnstimeofday(&Ts4);
    u32WriteMemShift = _MDrv_XC_GetMLoadMemOffset(pstClient->u16WPoint);

    DstAddr = pstClient->PhyAddr + u32WriteMemShift;
    pu64Addr = (MS_U64 *)_GET_PA2VA(enMloadType, enClient, DstAddr);
    *pu64Addr = u64Cmd;
    pstClient->u16WPoint++;
}

MS_BOOL KDrv_XC_MLoad_WriteCommand(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    getnstimeofday(&Ts1);
    MS_BOOL bReturn = FALSE;
    // SC1 mload may not enable
    if (E_KDRV_MLOAD_ENABLED != KDrv_XC_MLoad_GetStatus(enMloadType, enClient))
    {
        SC_W2BYTEMSK(enClient, (0x130000 | u32Addr), u16Data, u16Mask);
    }
    else
    {
        if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND,enClient/CLIENT_NUM))
        {
            MS_U64 u64Command = 0;

            if(ENABLE_64BITS_SPREAD_MODE)//64 bits spread mode
            {
                u64Command = KHal_XC_MLoad_Gen_64bits_spreadMode(enClient,u32Addr,u16Data,u16Mask);
            }
            else//64 bits sub-bank mode
            {
                u64Command = KHal_XC_MLoad_Gen_64bits_subBankMode(enClient,u32Addr,u16Data,u16Mask);
            }
            getnstimeofday(&Ts2);
            if(_KDrv_XC_MLoad_WriteCmd_64Bits(enMloadType, enClient, u64Command, u16Mask))
            {
                bReturn = TRUE;
            }
            else
            {
                MLDBG_WARN(printk("%d, MLoad[%d] Fail: %04lx %04x %04x \n", __LINE__, enMloadType, u32Addr, u16Data, u16Mask));
                bReturn = FALSE;
            }
            getnstimeofday(&Ts6);
        }
        else//32 bits sub-bank mode
        {
            MS_U32 u32Command = 0;
            u32Command = KHal_XC_MLoad_Gen_32bits_subBankMode(enClient,u32Addr,u16Data,u16Mask);

            if(_KDrv_XC_MLoad_WriteCmd_32Bits(enMloadType, enClient, u32Command, u16Mask))
            {
                bReturn = TRUE;
            }
            else
            {
                MLDBG_WARN(printk("%d, MLoad[%d] Fail: %04lx %04x %04x \n", __LINE__, enMloadType, u32Addr, u16Data, u16Mask));
                bReturn = FALSE;
            }
        }
    }

    getnstimeofday(&Ts7);

    return bReturn;
}

//-------------------------------------------------------------------------------------------------
/// Write command to the Menuload buffer by WORD
/// @param  u32Bank                 \b IN: the address (direct-bank)
/// @param  u32Addr                 \b IN: the address (8-bit address)
/// @param  u16Data                 \b IN: the data
/// @param  u16Mask                 \b IN: the mask
/// @return  TRUE if succeed, FALSE if failed

// This function can support write non xc registers with menuload.
// If you want to write xc registers,you should use KApi_XC_MLoad_WriteCmd.

//-------------------------------------------------------------------------------------------------
MS_BOOL KApi_XC_MLoad_WriteCmd_NonXC(EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Bank,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_BOOL bReturn = FALSE;

    _MLOAD_ENTRY(enClient);
    bReturn = KDrv_XC_MLoad_WriteCommand_NonXC(E_MLOAD, enClient,u32Bank,u32Addr, u16Data, u16Mask);
    _MLOAD_RETURN(enClient);

    return bReturn;
}

MS_BOOL KDrv_XC_MLoad_WriteCommand_NonXC(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Bank, MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_BOOL bReturn;
    KDRV_MS_MLoad_Info* pstClient = _GetMloadClientInfo(enMloadType, enClient);

    if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
    {
        MS_U64 u64Command = 0;

#if(ENABLE_64BITS_SPREAD_MODE)//64 bits spread mode
        u64Command = KHal_XC_MLoad_Gen_64bits_spreadMode_NonXC(u32Bank,u32Addr,u16Data,u16Mask);
#else//64 bits sub-bank mode
        printk("Not support sub bank mode for non xc bank!");
#endif

        if(_KDrv_XC_MLoad_WriteCmd_64Bits(enMloadType, enClient, u64Command, u16Mask))
        {
            pstClient->bCleared = FALSE;
            bReturn = TRUE;
        }
        else
        {
            MLDBG_WARN(printk("%d, MLoad[%d] Fail: %04lx %04x %04x \n", __LINE__, enMloadType, u32Addr, u16Data, u16Mask));
            bReturn = FALSE;
        }
    }
    else //32 bits sub-bank mode
    {
        printk("Not support sub bank mode for non xc bank!");
        bReturn = FALSE;
    }

    return bReturn;
}

void KDrv_XC_MLoad_set_trigger_sync(EN_MLOAD_CLIENT_TYPE enClient,EN_MLOAD_TRIG_SYNC eTriggerSync)
{
    _MLOAD_FIRE_ENTRY(enClient);

    KHal_XC_MLoad_set_trigger_sync(enClient,eTriggerSync);

    _MLOAD_FIRE_RETURN(enClient);
}

#if ENABLE_NMLOAD
//-------------------------------------------------------------------------------------------------
/// Get the status of MLoad
/// @return  MLOAD_TYPE
//-------------------------------------------------------------------------------------------------
EN_KDRV_MLOAD_TYPE KApi_XC_NMLoad_GetStatus(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient)
{
    EN_KDRV_MLOAD_TYPE eMode = E_KDRV_MLOAD_UNSUPPORTED;

    eMode = KDrv_XC_MLoad_GetStatus(enMloadType, enClient);

    return eMode;
}

//-------------------------------------------------------------------------------------------------
/// Initialize the Menuload ex
/// @param  PhyAddr                 \b IN: the physical address for the menuload
/// @param  u32BufByteLen           \b IN: the buffer length of the menu load commands
//-------------------------------------------------------------------------------------------------
void KApi_XC_NMLoad_Init(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U64 u64Addr, MS_U32 u32BufByteLen)
{
    MLDBG(printk("%s: Mload[%d] client: %d, 0x%x, 0x%x\n", __FUNCTION__, enMloadType, enClient, (MS_U32)u64Addr, u32BufByteLen));
    if(KDrv_XC_MLoad_GetCaps(enMloadType))
    {
        _MLOAD_EX_ENTRY(enMloadType, enClient);

        if(_astNClient[enMloadType][enClient].bInit == FALSE) //prevent vmalloc leak for ioremap
        {
            _PA2VA(enMloadType, enClient, u64Addr, u32BufByteLen);
            _astNClient[enMloadType][enClient].bInit = TRUE;
        }

        _astNClient[enMloadType][enClient].PhyAddr = u64Addr;
        _astNClient[enMloadType][enClient].bEnable = FALSE;
        _astNClient[enMloadType][enClient].u16RPoint = 0;
        _astNClient[enMloadType][enClient].u16WPoint = 0;
        _astNClient[enMloadType][enClient].u16FPoint = 0;

        if (IS_SUPPORT_64BITS_COMMAND(ENABLE_64BITS_COMMAND, enClient/CLIENT_NUM))
        {
            if ((u32BufByteLen / MS_MLOAD_CMD_LEN_64BITS) < 0x3FFFC)
            {
                _astNClient[enMloadType][enClient].u16MaxCmdCnt = (MS_U16)(u32BufByteLen / MS_MLOAD_CMD_LEN_64BITS);
            }
            else
            {
                _astNClient[enMloadType][enClient].u16MaxCmdCnt = 0x3FFFC;
                MLDBG_WARN(printk("MLOAD[%d] client[%d] Too Large menuload u32BufByteLen. u16MaxCmdCnt might overflow!\n", enMloadType, enClient));
            }
        }
        else
        {
            if ((u32BufByteLen / MS_MLOAD_CMD_LEN) < 0xFFFF)
            {
                _astNClient[enMloadType][enClient].u16MaxCmdCnt = (MS_U16)(u32BufByteLen / MS_MLOAD_CMD_LEN);
            }
            else
            {
                _astNClient[enMloadType][enClient].u16MaxCmdCnt = 0xFFFF;
                MLDBG_WARN(printk("MLOAD[%d] client[%d] Too Large menuload u32BufByteLen. u16MaxCmdCnt might overflow!\n", enMloadType, enClient));
            }
        }

        KHal_XC_NMLoad_set_trigger_sync(enMloadType, enClient);

        _MLOAD_EX_RETURN(enMloadType, enClient);
    }
}

//-------------------------------------------------------------------------------------------------
/// Enable/Disable the MLoad EX
/// @return  void
//-------------------------------------------------------------------------------------------------
void KApi_XC_NMLoad_Enable(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_BOOL bEnable)
{
    if(KDrv_XC_MLoad_GetCaps(enMloadType))
    {
        _MLOAD_EX_ENTRY(enMloadType, enClient);

        KDrv_XC_MLoad_Enable(enMloadType, enClient, bEnable);

        _MLOAD_EX_RETURN(enMloadType, enClient);
    }
}

//-------------------------------------------------------------------------------------------------
/// Fire the Menuload commands
/// @return  TRUE if succeed, FALSE if failed
//-------------------------------------------------------------------------------------------------
MS_BOOL KApi_XC_NMLoad_Fire(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_BOOL bImmediate)
{
    getnstimeofday(&Ts10);
    MS_BOOL bReturn = FALSE;
    if (KDrv_XC_MLoad_GetCaps(enMloadType))
    {
        if (E_KDRV_MLOAD_ENABLED != KDrv_XC_MLoad_GetStatus(enMloadType, enClient))
        {
            printk("%d, Mload[%d] [client %d] please enable mload.\n", __LINE__, enMloadType, enClient);
            return bReturn;
        }

        _MLOAD_EX_ENTRY(enMloadType, enClient);
        _MLOAD_EX_FIRE_ENTRY(enMloadType, enClient);

        if(KDrv_XC_MLoad_Check_Done(enMloadType, enClient))
        {
            bReturn = KDrv_XC_MLoad_Fire(enMloadType, enClient, bImmediate);
        }

        _MLOAD_EX_FIRE_RETURN(enMloadType, enClient);
        _MLOAD_EX_RETURN(enMloadType, enClient);
    }
#if 0
    printk("bob write(%u, %u, %u, %u, %u, %u, %u, %u), fire(%u, %u, %u, %u, %u, %u, %u, %u)\n",
        t_d(Ts0,Ts7),t_d(Ts0,Ts1),t_d(Ts1,Ts2),t_d(Ts2,Ts3),t_d(Ts3,Ts4),t_d(Ts4,Ts5),t_d(Ts5,Ts6),t_d(Ts6,Ts7),
        t_d(Ts10,Ts17),t_d(Ts10,Ts11),t_d(Ts11,Ts12),t_d(Ts12,Ts13),t_d(Ts13,Ts14),t_d(Ts14,Ts15),t_d(Ts15,Ts16),t_d(Ts16,Ts17));
#endif
    return bReturn;
}

//-------------------------------------------------------------------------------------------------
/// Write command to the Menuload buffer by WORD
/// @param  u32Addr                 \b IN: the address (sub-bank + 8-bit address)
/// @param  u16Data                 \b IN: the data
/// @param  u16Mask                 \b IN: the mask
/// @return  TRUE if succeed, FALSE if failed
/// how to use this function
/// if you want write REG_SC_BK01_01_L bitN and bitM
/// 1. you must:
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(N)|BIT(M), BIT(N)|BIT(M));
/// MApi_XC_MLoad_Fire();
/// 2. you mustn't:
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(N), BIT(N));
/// MDrv_XC_MLoad_WriteCmd(pInstance, REG_SC_BK01_01_L, BIT(M), BIT(M));
/// MApi_XC_MLoad_Fire();
/// only REG_SC_BKXX_XX_L bitM will be download reg if you do like step2
//-------------------------------------------------------------------------------------------------
MS_BOOL KApi_XC_NMLoad_WriteCmd(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    getnstimeofday(&Ts0);
    MS_BOOL bReturn = FALSE;
    if (KDrv_XC_MLoad_GetCaps(enMloadType))
    {
        if (E_KDRV_MLOAD_ENABLED != KDrv_XC_MLoad_GetStatus(enMloadType, enClient))
        {
            printk("%d, Mload[%d] [client %d] please enable mload.\n", __LINE__, enMloadType, enClient);
            return bReturn;
        }

        _MLOAD_EX_ENTRY(enMloadType, enClient);

        bReturn = KDrv_XC_MLoad_WriteCmd(enMloadType, enClient, u32Addr, u16Data, u16Mask);;

        _MLOAD_EX_RETURN(enMloadType, enClient);
    }

    return bReturn;
}

MS_BOOL KApi_XC_MLoad_EX_WriteCmd_And_Fire(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_BOOL bRet = TRUE;

    if (KDrv_XC_MLoad_GetCaps(enMloadType))
    {
        if (E_KDRV_MLOAD_ENABLED != KDrv_XC_MLoad_GetStatus(enMloadType, enClient))
        {
            printk("%d, Mload[%d] [client %d] please enable mload.\n", __LINE__, enMloadType, enClient);
            return FALSE;
        }

        _MLOAD_EX_ENTRY(enMloadType, enClient);

        if (!KDrv_XC_MLoad_WriteCmd(enMloadType, enClient, u32Addr, u16Data, u16Mask))
        {
            bRet = FALSE;
            MLDBG(printk("%d, MLoad[%d] Fail: Cmd %4lx %x %x \n", __LINE__, enMloadType, u32Addr, u16Data, u16Mask));
        }
        else
        {
            _MLOAD_EX_FIRE_ENTRY(enMloadType, enClient);

            if (!KDrv_XC_MLoad_Fire(enMloadType, enClient, TRUE))
            {
                bRet = FALSE;
                MLDBG(printk("MLoad[%d] Fire Error!!!!\n ", enMloadType));
            }

            _MLOAD_EX_FIRE_RETURN(enMloadType, enClient);
        }

        _MLOAD_EX_RETURN(enMloadType, enClient);
    }
    else
    {
        bRet = FALSE;
    }

    return bRet;
}

MS_BOOL KApi_XC_NMLoad_WriteCmds_And_Fire(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 *pu32Addr, MS_U16 *pu16Data, MS_U16 *pu16Mask, MS_U16 u16CmdCnt)
{
    MS_BOOL bRet = TRUE;
    MS_U16 i = 0, j = 0;
    if (KDrv_XC_MLoad_GetCaps(enMloadType))
    {
        if (E_KDRV_MLOAD_ENABLED != KDrv_XC_MLoad_GetStatus(enMloadType, enClient))
        {
            printk("%d, Mload[%d] [client %d] please enable mload.\n", __LINE__, enMloadType, enClient);
            return FALSE;
        }

        _MLOAD_EX_ENTRY(enMloadType, enClient);

        //Note: If there are some same regs in the Mload cmd queue,
        //  MLoad will use the value of the last reg address to fire. others same regs will be ignore.
        // So we handle this situation here, to combine every same reg's setting value to the same.
        for(i = 0; i < u16CmdCnt; i++)
        {
            for(j = i + 1; j<u16CmdCnt; j++)
            {
                if(pu32Addr[i] == pu32Addr[j])
                {
                    pu16Mask[i] |= pu16Mask[j];
                    pu16Data[i] |= pu16Data[j];
                    pu16Mask[j] = pu16Mask[i];
                    pu16Data[j] = pu16Data[i];
                }
            }
        }

        for(i = 0; i < u16CmdCnt; i++)
        {

            if (!KDrv_XC_MLoad_WriteCmd(enMloadType, enClient, pu32Addr[i], pu16Data[i], pu16Mask[i]))
            {
                bRet = FALSE;
                MLDBG(printk("%d, MLoad[%d] Fail: Cmd %4lx %x %x \n", __LINE__, enMloadType, pu32Addr[i], pu16Data[i], pu16Mask[i]));
            }
        }

        _MLOAD_EX_FIRE_ENTRY(enMloadType, enClient);

        if (!KDrv_XC_MLoad_Fire(enMloadType, enClient, TRUE))
        {
            bRet = FALSE;
            MLDBG(printk("MLoad[%d] Fire Error!!!!\n ", enMloadType));
        }

        _MLOAD_EX_FIRE_RETURN(enMloadType, enClient);
        _MLOAD_EX_RETURN(enMloadType, enClient);
    }
    else
    {
        bRet = FALSE;
    }

    return bRet;
}

//-------------------------------------------------------------------------------------------------
/// Write command to the Menuload buffer by WORD
/// @param  u32Bank                 \b IN: the address (direct-bank)
/// @param  u32Addr                 \b IN: the address (8-bit address)
/// @param  u16Data                 \b IN: the data
/// @param  u16Mask                 \b IN: the mask
/// @return  TRUE if succeed, FALSE if failed

// This function can support write non xc registers with menuload.
// If you want to write xc registers,you should use KApi_XC_MLoad_WriteCmd.

//-------------------------------------------------------------------------------------------------
MS_BOOL KApi_XC_NMLoad_WriteCmd_NonXC(EN_MLOAD_TYPE enMloadType, EN_MLOAD_CLIENT_TYPE enClient, MS_U32 u32Bank,MS_U32 u32Addr, MS_U16 u16Data, MS_U16 u16Mask)
{
    MS_BOOL bReturn = FALSE;

    if (KDrv_XC_MLoad_GetCaps(enMloadType))
    {
        if (E_KDRV_MLOAD_ENABLED != KDrv_XC_MLoad_GetStatus(enMloadType, enClient))
        {
            printk("%d, Mload[%d] [client %d] please enable mload.\n", __LINE__, enMloadType, enClient);
            return FALSE;
        }

        _MLOAD_EX_ENTRY(enMloadType, enClient);

        bReturn = KDrv_XC_MLoad_WriteCommand_NonXC(enMloadType, enClient, u32Bank, u32Addr, u16Data, u16Mask);

        _MLOAD_EX_RETURN(enMloadType, enClient);
    }

    return bReturn;
}
#endif

EXPORT_SYMBOL(KApi_XC_MLoad_Init);
EXPORT_SYMBOL(KApi_XC_MLoad_Enable);
EXPORT_SYMBOL(KApi_XC_MLoad_GetStatus);
EXPORT_SYMBOL(KApi_XC_MLoad_Fire);
EXPORT_SYMBOL(KApi_XC_MLoad_WriteCmd);
EXPORT_SYMBOL(KApi_XC_MLoad_WriteCmd_NonXC);
EXPORT_SYMBOL(KDrv_XC_MLoad_set_IP_trig_p);
EXPORT_SYMBOL(KDrv_XC_MLoad_get_IP_trig_p);
EXPORT_SYMBOL(KDrv_XC_MLoad_set_trigger_sync);
#endif
