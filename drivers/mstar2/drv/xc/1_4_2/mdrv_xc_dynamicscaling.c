//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
//==============================================================================
// [mdrv_sc_dynamicscaling.c]
// Date: 2012/10/29
// Descriptions: dynamic scaling related functions
//==============================================================================
#ifndef  _MDRV_XC_DYNAMICSCALING_C_
#define  _MDRV_XC_DYNAMICSCALING_C_

// Common Definition
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/slab.h>

#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/jiffies.h>
#include <linux/wait.h>
#include <linux/types.h>
#include <linux/semaphore.h>
#include <linux/hrtimer.h>

#include "mdrv_mstypes.h"
#include "mhal_xc_chip_config.h"
#include "mdrv_xc_st.h"
#include "mdrv_xc_dynamicscaling.h"
#include "mhal_dynamicscaling.h"
#include "mhal_xc.h"

MS_BOOL _bDSClientInited[E_KDRV_XC_MAX][E_KDRV_MAX_WINDOW] = {FALSE};
E_K_APIXC_ReturnValue KDrv_XC_Get_DSForceIndexSupported(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW  eWindow);
E_K_APIXC_ReturnValue KDrv_XC_Set_DSIndexSourceSelect(EN_KDRV_SC_DEVICE u32DeviceID, E_K_XC_DS_INDEX_SOURCE eDSIdxSrc, EN_KDRV_WINDOW  eWindow);
E_K_APIXC_ReturnValue KDrv_XC_Set_DSForceIndex(EN_KDRV_SC_DEVICE u32DeviceID, MS_BOOL bEnable, MS_U8 u8Index, EN_KDRV_WINDOW  eWindow);
MS_BOOL KDrv_XC_Set_DynamicScaling(EN_KDRV_SC_DEVICE u32DeviceID, MS_PHY u32MemBaseAddr, MS_U8 u8MIU_Select, MS_U8 u8IdxDepth, MS_BOOL bOP_On, MS_BOOL bIPS_On, MS_BOOL bIPM_On, MS_U32 u32DSBufferSize, EN_KDRV_WINDOW  eWindow);

E_K_APIXC_ReturnValue KDrv_XC_Get_DSForceIndexSupported(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW  eWindow)
{
    MS_BOOL bReturn = FALSE;
	bReturn = KHal_XC_Get_DSForceIndexSupported(u32DeviceID, eWindow);
    return bReturn?E_K_APIXC_RET_OK:E_K_APIXC_RET_FAIL;
}

E_K_APIXC_ReturnValue KDrv_XC_Set_DSIndexSourceSelect(EN_KDRV_SC_DEVICE u32DeviceID, E_K_XC_DS_INDEX_SOURCE eDSIdxSrc, EN_KDRV_WINDOW  eWindow)
{
	KHal_XC_Set_DSIndexSourceSelect(u32DeviceID, eDSIdxSrc, eWindow);
    return E_K_APIXC_RET_OK;
}

E_K_APIXC_ReturnValue KDrv_XC_Set_DSForceIndex(EN_KDRV_SC_DEVICE u32DeviceID, MS_BOOL bEnable, MS_U8 u8Index, EN_KDRV_WINDOW  eWindow)
{
    KHal_XC_Set_DSForceIndex(u32DeviceID, bEnable, u8Index, eWindow);
    return E_K_APIXC_RET_OK;
}

E_K_APIXC_ReturnValue KApi_XC_Get_DSForceIndexSupported(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW  eWindow)
{
    return KDrv_XC_Get_DSForceIndexSupported(u32DeviceID, eWindow);
}


E_K_APIXC_ReturnValue KApi_XC_Set_DSIndexSourceSelect(EN_KDRV_SC_DEVICE u32DeviceID, E_K_XC_DS_INDEX_SOURCE eDSIdxSrc, EN_KDRV_WINDOW  eWindow)
{
    E_K_APIXC_ReturnValue eReturn = E_K_APIXC_RET_FAIL;
    eReturn = KDrv_XC_Set_DSIndexSourceSelect(u32DeviceID, eDSIdxSrc, eWindow);
    return eReturn;
}


E_K_APIXC_ReturnValue KApi_XC_Set_DSForceIndex(EN_KDRV_SC_DEVICE u32DeviceID, MS_BOOL bEnable, MS_U8 u8Index, EN_KDRV_WINDOW  eWindow)
{
    E_K_APIXC_ReturnValue eReturn = E_K_APIXC_RET_FAIL;
    eReturn = KDrv_XC_Set_DSForceIndex(u32DeviceID, bEnable, u8Index, eWindow);
    return eReturn;
}



MS_BOOL KDrv_XC_GetDynamicScalingStatus(EN_KDRV_SC_DEVICE u32DeviceID)
{
    return KHAL_SC_Get_DynamicScaling_Status(u32DeviceID);
}

MS_BOOL KDrv_XC_EnableIPMTuneAfterDS(EN_KDRV_SC_DEVICE u32DeviceID, MS_BOOL bEnable)
{
    return KHAL_SC_Enable_IPMTuneAfterDS(u32DeviceID, bEnable);
}

extern MS_BOOL ll_delete_all(void);
//-------------------------------------------------------------------------------------------------
/// Set Dynamic Scaling
/// @param  pstDSInfo              \b IN: the information of Dynamic Scaling
/// @param  u32DSInforLen          \b IN: the length of the pstDSInfo
/// @param  eWindow                \b IN: which window we are going to set
/// @return @ref MS_BOOL
//-------------------------------------------------------------------------------------------------
MS_BOOL KApi_XC_SetDynamicScaling(EN_KDRV_SC_DEVICE u32DeviceID, K_XC_DynamicScaling_Info *pstDSInfo,MS_U32 u32DSInfoLen, EN_KDRV_WINDOW  eWindow)
{
#if 0
    printf("\033[1;32m[%s:%d]u32DeviceID=%d , u32DSInfoLen = %d, eWindow =%d\033[m\n",__FUNCTION__,__LINE__,u32DeviceID,u32DSInfoLen,eWindow);
    printf("\033[1;32m[%s:%d]%ld , %d, %d,%d,%d,%d, %d\033[m\n",__FUNCTION__,__LINE__,
        pstDSInfo->u64DS_Info_BaseAddr,
        pstDSInfo->u8MIU_Select,
        pstDSInfo->u8DS_Index_Depth,
        pstDSInfo->bOP_DS_On,
        pstDSInfo->bIPS_DS_On,
        pstDSInfo->bIPM_DS_On,
        pstDSInfo->u32DSBufferSize);
#endif
    MS_BOOL bStatus;
    if(u32DSInfoLen != sizeof(K_XC_DynamicScaling_Info))
    {
        printk("[error:%s,%d] unmatched DS Info size, exit!\n", __func__, __LINE__);
        return FALSE;
    }

    bStatus = KDrv_XC_Set_DynamicScaling(
        u32DeviceID,
        pstDSInfo->u64DS_Info_BaseAddr,
        pstDSInfo->u8MIU_Select,
        pstDSInfo->u8DS_Index_Depth,
        pstDSInfo->bOP_DS_On,
        pstDSInfo->bIPS_DS_On,
        pstDSInfo->bIPM_DS_On,
        pstDSInfo->u32DSBufferSize,
        eWindow);
        //printf("\033[1;32m[%s:%d]\033[m\n",__FUNCTION__,__LINE__);

    if ((_bDSClientInited[u32DeviceID][eWindow] == FALSE) && (pstDSInfo->u64DS_Info_BaseAddr != 0)
        && (pstDSInfo->bOP_DS_On || pstDSInfo->bIPM_DS_On || pstDSInfo->bIPS_DS_On))
    {
        KApi_DS_set_client(u32DeviceID, E_DS_CLIENT_XC, 16 * 4);
        KApi_DS_set_client(u32DeviceID, E_DS_CLIENT_HDR, 200 * 4);
        _bDSClientInited[u32DeviceID][eWindow] = TRUE;
    }
    else if ((pstDSInfo->bOP_DS_On == FALSE) && (pstDSInfo->bIPM_DS_On == FALSE) && (pstDSInfo->bIPS_DS_On == FALSE))
    {
        ll_delete_all();
        _bDSClientInited[u32DeviceID][eWindow] = FALSE;
    }
    return bStatus;
}


MS_BOOL KDrv_XC_Set_DynamicScaling(EN_KDRV_SC_DEVICE u32DeviceID,MS_PHY u32MemBaseAddr, MS_U8 u8MIU_Select, MS_U8 u8IdxDepth, MS_BOOL bOP_On, MS_BOOL bIPS_On, MS_BOOL bIPM_On, MS_U32 u32DSBufferSize, EN_KDRV_WINDOW  eWindow)
{
    KHal_XC_InitDynamicScalingIndex(u32DeviceID, eWindow);

    return KHAL_SC_Set_DynamicScaling(u32DeviceID,u32MemBaseAddr, u8MIU_Select,u8IdxDepth, bOP_On, bIPS_On, bIPM_On, u32DSBufferSize, eWindow);
}

void KApi_XC_Set_DynamicScalingFlag(EN_KDRV_SC_DEVICE u32DeviceID,MS_BOOL bEnable)
{
    KHAL_SC_Set_DynamicScalingFlag(u32DeviceID, bEnable);
}
void KApi_XC_WriteSWDSCommand(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW eWindow, E_DS_CLIENT client, MS_U32 u32CmdRegAddr, MS_U16 u16CmdRegValue, k_ds_reg_ip_op_sel IPOP_Sel, k_ds_reg_source_sel Source_Select, K_XC_DS_CMDCNT *pstXC_DS_CmdCnt)
{
    MS_U8 u8DSIndex = 0;

    KHal_XC_GetDynamicScalingCurrentIndex(u32DeviceID, eWindow, &u8DSIndex);
    KHal_SC_WriteSWDSCommand(u32DeviceID,eWindow,client,u32CmdRegAddr,u16CmdRegValue,IPOP_Sel,Source_Select,pstXC_DS_CmdCnt,u8DSIndex);
}
void KApi_XC_Add_NullCommand(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW eWindow, E_DS_CLIENT client, k_ds_reg_ip_op_sel IPOP_Sel, K_XC_DS_CMDCNT *pstXC_DS_CmdCnt)
{
    MS_U8 u8DSIndex = 0;

    KHal_XC_GetDynamicScalingCurrentIndex(u32DeviceID, eWindow, &u8DSIndex);
    KHal_SC_Add_NullCommand(u32DeviceID,eWindow,client,IPOP_Sel,pstXC_DS_CmdCnt,u8DSIndex);
}
MS_BOOL KApi_DS_set_client(EN_KDRV_SC_DEVICE u32DeviceID,E_DS_CLIENT client,MS_U32 max_num)
{
    return KHal_DS_set_client(u32DeviceID,client,max_num);
}
MS_U32 KApi_DS_get_support_index_num(void)
{
    return DS_BUFFER_NUM_EX;
}

void KApi_XC_WriteSWDSCommandNonXC(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW eWindow, E_DS_CLIENT client, MS_U32 u32Bank, MS_U16 u32Addr, MS_U32 u16Data, MS_U16 u16Mask, k_ds_reg_ip_op_sel IPOP_Sel, K_XC_DS_CMDCNT *pstXC_DS_CmdCnt)
{
    MS_U8 u8DSIndex = 0;

    KHal_XC_GetDynamicScalingCurrentIndex(u32DeviceID, eWindow, &u8DSIndex);
    KHal_XC_WriteSWDSCommandNonXC(u32DeviceID, eWindow, client, u32Bank, u32Addr, u16Data, u16Mask, IPOP_Sel, pstXC_DS_CmdCnt, u8DSIndex);
}

void KApi_XC_WriteSWDSCommand_Mask(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW eWindow, E_DS_CLIENT client, MS_U32 u32CmdRegAddr, MS_U16 u16CmdRegValue, k_ds_reg_ip_op_sel IPOP_Sel, k_ds_reg_source_sel Source_Select, K_XC_DS_CMDCNT *pstXC_DS_CmdCnt,MS_U16 u16Mask)
{
    MS_U8 u8DSIndex = 0;

    KHal_XC_GetDynamicScalingCurrentIndex(u32DeviceID, eWindow, &u8DSIndex);
    KHal_SC_WriteSWDSCommand_Mask(u32DeviceID,eWindow,client,u32CmdRegAddr,u16CmdRegValue,IPOP_Sel,Source_Select,pstXC_DS_CmdCnt,u8DSIndex,u16Mask);
}

MS_BOOL KApi_XC_GetDynamicScalingIndex(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW eWindow, MS_U8* pu8DSIndex)
{
    return KHal_XC_GetDynamicScalingFireIndex(u32DeviceID, eWindow, pu8DSIndex);
}

MS_BOOL KApi_XC_FireDynamicScalingIndex(EN_KDRV_SC_DEVICE u32DeviceID, EN_KDRV_WINDOW eWindow)
{
    return KHal_XC_FireDynamicScalingIndex(u32DeviceID, eWindow);
}

void KApi_XC_SetDSInfo(EN_KDRV_SC_DEVICE u32DeviceID, K_XC_SETDS_Info *pstDSInfo, EN_KDRV_WINDOW  eWindow)
{
    KHal_XC_SetDSInfo(u32DeviceID, pstDSInfo, eWindow);
}

void KApi_XC_GetDSInfo(EN_KDRV_SC_DEVICE u32DeviceID, K_XC_GETDS_Info *pstDSInfo, EN_KDRV_WINDOW  eWindow)
{
    KHal_XC_GetDSInfo(u32DeviceID, pstDSInfo, eWindow);
}
EXPORT_SYMBOL(KApi_XC_Get_DSForceIndexSupported);
EXPORT_SYMBOL(KApi_XC_Set_DSIndexSourceSelect);
EXPORT_SYMBOL(KApi_XC_Set_DSForceIndex);
EXPORT_SYMBOL(KApi_XC_SetDynamicScaling);
EXPORT_SYMBOL(KApi_XC_Set_DynamicScalingFlag);
EXPORT_SYMBOL(KDrv_XC_GetDynamicScalingStatus);
EXPORT_SYMBOL(KDrv_XC_EnableIPMTuneAfterDS);
EXPORT_SYMBOL(KApi_XC_WriteSWDSCommand);
EXPORT_SYMBOL(KApi_XC_Add_NullCommand);
EXPORT_SYMBOL(KApi_DS_set_client);
EXPORT_SYMBOL(KApi_DS_get_support_index_num);
EXPORT_SYMBOL(KApi_XC_WriteSWDSCommandNonXC);
EXPORT_SYMBOL(KApi_XC_WriteSWDSCommand_Mask);
EXPORT_SYMBOL(KApi_XC_GetDynamicScalingIndex);
EXPORT_SYMBOL(KApi_XC_FireDynamicScalingIndex);
EXPORT_SYMBOL(KApi_XC_GetDSInfo);
EXPORT_SYMBOL(KApi_XC_SetDSInfo);
#undef _MDRV_SC_DYNAMICSCALING_C_
#endif //_MDRV_SC_DYNAMICSCALING_C_
