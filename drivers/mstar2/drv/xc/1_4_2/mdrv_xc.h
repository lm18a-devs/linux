////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2008 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (¡§MStar Confidential Information¡¨) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// @file   mdrv_temp.h
/// @brief  TEMP Driver Interface
/// @author MStar Semiconductor Inc.
///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _MDRV_XC_H_
#define _MDRV_XC_H_

#include <linux/fs.h>
#include <linux/cdev.h>
#include "mdrv_types.h"
#include <linux/version.h>
#include "mdrv_xc_st.h"
#include "mhal_xc.h"

//-------------------------------------------------------------------------------------------------
//  Driver Capability
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Macro and Define
//-------------------------------------------------------------------------------------------------

//#define XC_DEBUG_ENABLE


//-------------------------------------------------------------------------------------------------
//  Type and Structure
//-------------------------------------------------------------------------------------------------



//-------------------------------------------------------------------------------------------------
//  Function and Variable
//-------------------------------------------------------------------------------------------------
irqreturn_t MDrv_XC_IntHandler(int irq,void *devid);

INTERFACE void MDrv_XC_FilmDriverHWVer1(void);
INTERFACE void MDrv_XC_FilmDriverHWVer2(void);
INTERFACE void MDrv_XC_ProcessCFDIRQ(void);
INTERFACE MS_BOOL MDrv_XC_SetHDRType(EN_KDRV_XC_HDR_TYPE enHDRType);
INTERFACE MS_BOOL MDrv_XC_GetHDRType(EN_KDRV_XC_HDR_TYPE *penHDRType);
INTERFACE MS_BOOL MDrv_XC_SetInputSourceType(EN_KDRV_XC_INPUT_SOURCE_TYPE enInputSourceType);
INTERFACE MS_BOOL MDrv_XC_GetInputSourceType(EN_KDRV_XC_INPUT_SOURCE_TYPE *penInputSourceType);
INTERFACE MS_BOOL MDrv_XC_SetOpenMetadataInfo(ST_KDRV_XC_OPEN_METADATA_INFO *pstMetadataInfo);
INTERFACE MS_BOOL MDrv_XC_Set3DLutInfo(ST_KDRV_XC_3DLUT_INFO *pst3DLutInfo);
INTERFACE MS_BOOL MDrv_XC_GetShareMemInfo(ST_KDRV_XC_SHARE_MEMORY_INFO *pstShmemInfo);
INTERFACE MS_BOOL MDrv_XC_SetShareMemInfo(ST_KDRV_XC_SHARE_MEMORY_INFO *pstShmemInfo);
INTERFACE MS_BOOL MDrv_XC_EnableHDR(MS_BOOL bEnableHDR);
INTERFACE MS_BOOL KMDrv_XC_SetVRAddress(ST_KDRV_XC_DLC_SET_VR_ADDRESS stDlcVRInfo);
INTERFACE MS_BOOL MDrv_XC_SetHDRWindow(ST_KDRV_XC_WINDOW_INFO *pstWindowInfo);
INTERFACE MS_BOOL MDrv_XC_ConfigAutoDownload(ST_KDRV_XC_AUTODOWNLOAD_CONFIG_INFO *pstConfigInfo);
INTERFACE MS_BOOL MDrv_XC_WriteAutoDownload(ST_KDRV_XC_AUTODOWNLOAD_DATA_INFO *pstDataInfo);
INTERFACE MS_BOOL MDrv_XC_FireAutoDownload(EN_KDRV_XC_AUTODOWNLOAD_CLIENT enClient);
INTERFACE MS_BOOL KDrv_XC_GetAutoDownloadCaps(ST_KDRV_XC_AUTODOWNLOAD_CLIENT_SUPPORTED_CAPS *pstAutoDownlaodCaps);
INTERFACE MS_BOOL MDrv_XC_SetColorFormat(EN_KDRV_XC_HDR_COLOR_FORMAT enColorFormat);
INTERFACE MS_BOOL MDrv_XC_CFDControl(ST_KDRV_XC_CFD_CONTROL_INFO *pstKdrvCFDCtrlInfo);
INTERFACE MS_BOOL MDrv_XC_SetDSHDRInfo(ST_KDRV_XC_DS_HDRInfo *pstDSHDRInfo);
INTERFACE MS_BOOL MDrv_XC_Init(void);
INTERFACE MS_BOOL MDrv_XC_Exit(void);
INTERFACE int MDrv_XC_Suspend(void);
INTERFACE int MDrv_XC_Resume(void);
INTERFACE void Mdrv_PQ_DLC_TMO_ProcessIRQ(void);
INTERFACE MS_U32 MDrv_XC_GetDispIRQ(void);
INTERFACE void MDrv_XC_ClearDispIRQ(MS_U8 u8IrqNum);
INTERFACE MS_BOOL KDrv_XC_SetSWDRInfo(ST_KDRV_XC_SWDR_INFO *pstSWDRInfo);
INTERFACE MS_BOOL KDrv_XC_GetSWDRInfo(ST_KDRV_XC_SWDR_INFO *pstSWDRInfo);
INTERFACE MS_BOOL KDrv_XC_SetDeFlickerControl(ST_KDRV_PQ_DEFLICKER *pstDeFlickerControl);
INTERFACE MS_BOOL MDrv_XC_SetDolbyConfigPath(ST_KDRV_XC_DOLBYPICTURECONFIG *pstDolbyConfigPath);
INTERFACE MS_BOOL MDrv_XC_GetDolbyVersion(ST_KDRV_XC_DOLBYSWVERSION *pstDolbySWVersion);
INTERFACE MS_BOOL MDrv_XC_GetRGBHistogram(ST_KDRV_XC_RGBHistogram *pstRGBHistogram);

#endif // _MDRV_TEMP_H_

