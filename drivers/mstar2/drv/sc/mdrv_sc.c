////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2008 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (��MStar Confidential Information��) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// file    mdrv_temp.c
/// @brief  TEMP Driver Interface
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------------------------
//  Include Files
//-------------------------------------------------------------------------------------------------
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/interrupt.h>
#include <linux/string.h>
#include <linux/poll.h>
#include <linux/wait.h>
#include <asm/io.h>
#include <linux/sched.h>
#include <linux/version.h>

//drver header files
#include "chip_int.h"
#include "mdrv_mstypes.h"
#include "reg_sc.h"
#include "mhal_sc.h"
#include "mdrv_sc.h"

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
#include <linux/slab.h>
#include <linux/seq_file.h>
#include <linux/spinlock.h>
#endif

//-------------------------------------------------------------------------------------------------
//  Driver Compiler Options
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Local Defines
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Local Structurs
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Global Variables
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Local Variables
//-------------------------------------------------------------------------------------------------
static SC_Info  _scInfo[SC_DEV_NUM] = {
    {
        .bCardIn        = FALSE,
        .bLastCardIn    = FALSE,
        .u8CardStatus   = 0,
        .u16FifoRxRead  = 0,
        .u16FifoRxWrite = 0,
        .u16FifoTxRead  = 0,
        .u16FifoTxWrite = 0,
    },
#if (SC_DEV_NUM > 1) // no more than 2
    {
        .bCardIn        = FALSE,
        .bLastCardIn    = FALSE,
        .u8CardStatus   = 0,
        .u16FifoRxRead  = 0,
        .u16FifoRxWrite = 0,
        .u16FifoTxRead  = 0,
        .u16FifoTxWrite = 0,
    }
#endif
};

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
static SC_MdbgInfo _scMdbInfo[SC_DEV_NUM] = {
    {
        .bOpened    = FALSE,
        .bCardIn    = FALSE,
        .u8Protocol = 0,
    },
#if (SC_DEV_NUM > 1) // no more than 2
    {
        .bOpened    = FALSE,
        .bCardIn    = FALSE,
        .u8Protocol = 0,
    },
#endif
};

static int mdb_sc_node_open(struct inode *inode, struct file *file);
static ssize_t mdb_sc_node_write(struct file *file, const char __user *buffer, size_t count, loff_t *pos);

const struct file_operations mdb_sc_node_operations = {
    .owner      = THIS_MODULE,
    .open       = mdb_sc_node_open,
    .read       = seq_read,
    .write      = mdb_sc_node_write,
    .llseek     = seq_lseek,
    .release    = single_release,
};

#define MDB_CMD_NUM     1
#define MBD_CMD_SIZE    16

typedef enum {
    MDB_SC_CMD_UNKNOW,
    MDB_SC_CMD_HELP,
} MDB_SC_CMD;

struct mdb_sc_cmd {
    char command[MBD_CMD_SIZE];
    MDB_SC_CMD index;
};

static struct mdb_sc_cmd mdb_sc_cmd_table[MDB_CMD_NUM] = {
    {
        .command = "help",
        .index = MDB_SC_CMD_HELP,
    },
};
#endif

//-------------------------------------------------------------------------------------------------
//  Debug Functions
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Local Functions
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Global Functions
//-------------------------------------------------------------------------------------------------

BOOL MDrv_SC_ISR_Proc(U8 u8SCID)
{
    U8  u8Reg;
    U32 cnt;
    U32 idx;
    BOOL bWakeUp = FALSE;

    u8Reg = SC_READ(u8SCID, UART_IIR);
    if (!(u8Reg & UART_IIR_NO_INT))
    {
        u8Reg = SC_READ(u8SCID, UART_LSR);
        while (u8Reg & (UART_LSR_DR | UART_LSR_BI))
        {
            bWakeUp = TRUE;
            _scInfo[u8SCID].u8FifoRx[_scInfo[u8SCID].u16FifoRxWrite] = SC_READ(u8SCID, UART_RX);

            idx = _scInfo[u8SCID].u16FifoRxWrite + 1;
            if ((idx == SC_FIFO_SIZE) && (_scInfo[u8SCID].u16FifoRxRead != 0))
            {
                // Not overflow but wrap
                _scInfo[u8SCID].u16FifoRxWrite = 0;
            }
            else if (idx != _scInfo[u8SCID].u16FifoRxRead)
            {
                // Not overflow
                _scInfo[u8SCID].u16FifoRxWrite = idx;
            }
            else
            {
                // overflow
                printk("[%s][%d] RX buffer Overflow\n", __FUNCTION__, __LINE__);
                break;
            }

            u8Reg = SC_READ(u8SCID, UART_LSR);
        }

        if (u8Reg & UART_LSR_THRE)
        {
            cnt = 16;
            do
            {
                if (_scInfo[u8SCID].u16FifoTxRead == _scInfo[u8SCID].u16FifoTxWrite)
                    break;

                bWakeUp = TRUE;
                SC_WRITE(u8SCID, UART_TX, _scInfo[u8SCID].u8FifoTx[_scInfo[u8SCID].u16FifoTxRead++]);
                if (_scInfo[u8SCID].u16FifoTxRead == SC_FIFO_SIZE)
                {
                    _scInfo[u8SCID].u16FifoTxRead = 0;
                }

            } while (--cnt > 0);
        }
    }

    // Check special event from SMART
    u8Reg = SC_READ(u8SCID, UART_SCSR);
    if (u8Reg & (UART_SCSR_INT_CARDIN | UART_SCSR_INT_CARDOUT))
    {
        SC_WRITE(u8SCID, UART_SCSR, u8Reg); // clear interrupt
        _scInfo[u8SCID].u8CardStatus |= u8Reg & (UART_SCSR_INT_CARDIN | UART_SCSR_INT_CARDOUT);
        bWakeUp = TRUE;
    }

    if (bWakeUp)
    {
        wake_up_interruptible(&_scInfo[u8SCID].stWaitQue);
    }

    return TRUE; // handled
}

//-------------------------------------------------------------------------------------------------
/// Handle smart card Interrupt notification handler
/// @param  irq             \b IN: interrupt number
/// @param  devid           \b IN: device id
/// @return IRQ_HANDLED
/// @attention
//-------------------------------------------------------------------------------------------------
irqreturn_t MDrv_SC_ISR1(int irq, void *devid)
{
    if (!MDrv_SC_ISR_Proc(0))
    {
        SC_PRINT("ISR proc is failed\n");
    }

    return IRQ_HANDLED;
}

//-------------------------------------------------------------------------------------------------
/// Handle smart card Interrupt notification handler
/// @param  irq             \b IN: interrupt number
/// @param  devid           \b IN: device id
/// @return IRQ_HANDLED
/// @attention
//-------------------------------------------------------------------------------------------------
irqreturn_t MDrv_SC_ISR2(int irq, void *devid)
{
    if (!MDrv_SC_ISR_Proc(1))
    {
        SC_PRINT("ISR proc is failed\n");
    }

    return IRQ_HANDLED;
}

int MDrv_SC_Open(struct inode *inode, struct file *filp)
{
    size_t u8SCID = (size_t)filp->private_data;

    SC_PRINT("%s is invoked\n", __FUNCTION__);
    init_waitqueue_head(&_scInfo[u8SCID].stWaitQue);

    return 0;
}

ssize_t MDrv_SC_Read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
    size_t u8SCID = (size_t)filp->private_data;
    ssize_t idx = 0;

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
    spin_lock(&_scInfo[u8SCID].lock);
#endif

    for (idx = 0; idx < count; idx++)
    {
        if (_scInfo[u8SCID].u16FifoRxWrite == _scInfo[u8SCID].u16FifoRxRead)
            break;

        buf[idx] = _scInfo[u8SCID].u8FifoRx[_scInfo[u8SCID].u16FifoRxRead++];
        if (_scInfo[u8SCID].u16FifoRxRead == SC_FIFO_SIZE)
        {
            _scInfo[u8SCID].u16FifoRxRead = 0;
        }
    }

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
    _scMdbInfo[u8SCID].u16HistLen = count;
    spin_unlock(&_scInfo[u8SCID].lock);
#endif

    return idx;
}

ssize_t MDrv_SC_Write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
    size_t u8SCID = (size_t)filp->private_data;
    ssize_t idx = 0;
    U32 tmp;

    for (idx = 0; idx < count; idx++)
    {
        _scInfo[u8SCID].u8FifoTx[_scInfo[u8SCID].u16FifoTxWrite] = buf[idx];

        tmp = _scInfo[u8SCID].u16FifoTxWrite + 1;
        if ((tmp == SC_FIFO_SIZE) && (_scInfo[u8SCID].u16FifoTxRead != 0))
        {
            // Not overflow but wrap
            _scInfo[u8SCID].u16FifoTxWrite = 0;
        }
        else if (tmp != _scInfo[u8SCID].u16FifoTxRead)
        {
            // Not overflow
            _scInfo[u8SCID].u16FifoTxWrite = tmp;
        }
        else
        {
            printk("[%s][%d] TX buffer Overflow\n", __FUNCTION__, __LINE__);
            break;
        }
    }

    if ((SC_READ(u8SCID, UART_LSR) & UART_LSR_THRE) &&
            (_scInfo[u8SCID].u16FifoTxRead != _scInfo[u8SCID].u16FifoTxWrite))
    {
        SC_WRITE(u8SCID, UART_TX, _scInfo[u8SCID].u8FifoTx[_scInfo[u8SCID].u16FifoTxRead++]);
        if (_scInfo[u8SCID].u16FifoTxRead == SC_FIFO_SIZE)
        {
            _scInfo[u8SCID].u16FifoTxRead = 0;
        }

    }

    return idx;
}

unsigned int MDrv_SC_Poll(struct file *filp, poll_table *wait)
{
    size_t u8SCID = (size_t)filp->private_data;
    unsigned int mask = 0;

    poll_wait(filp, &_scInfo[u8SCID].stWaitQue, wait);
    if (_scInfo[u8SCID].u16FifoRxRead != _scInfo[u8SCID].u16FifoRxWrite)
    {
        mask |= POLLIN;
    }
    if (_scInfo[u8SCID].u8CardStatus)
    {
        mask |= POLLPRI;
    }

    return mask;
}

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
int MDrv_SC_AttachInterrupt(struct file *filp, unsigned long arg)
#else
int MDrv_SC_AttachInterrupt(struct inode *inode, struct file *filp, unsigned long arg)
#endif
{
    size_t u8SCID = (size_t)filp->private_data;

    SC_PRINT("%s is invoked\n", __FUNCTION__);

    if (u8SCID == 0)
    {
        if (request_irq(SC_IRQ, MDrv_SC_ISR1, SA_INTERRUPT, "SC", NULL))
        {
            SC_PRINT("SC IRQ1 registartion ERROR\n");
        }
        else
        {
            SC_PRINT("SC IRQ1 registartion OK\n");
        }
    }
#if (SC_DEV_NUM > 1) // no more than 2
    else if (u8SCID == 1)
    {
        if (request_irq(SC_IRQ2, MDrv_SC_ISR2, SA_INTERRUPT, "SC", NULL))
        {
            SC_PRINT("SC IRQ2 registartion ERROR\n");
        }
        else
        {
            SC_PRINT("SC IRQ2 registartion OK\n");
        }
    }
#endif
    return 0;
}

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
int MDrv_SC_DetachInterrupt(struct file *filp, unsigned long arg)
#else
int MDrv_SC_DetachInterrupt(struct inode *inode, struct file *filp, unsigned long arg)
#endif
{
    size_t u8SCID = (size_t)filp->private_data;

    SC_PRINT("%s is invoked\n", __FUNCTION__);
    if (u8SCID == 0)
    {
        free_irq(SC_IRQ, NULL);
    }
#if (SC_DEV_NUM > 1) // no more than 2
    else if (u8SCID == 1)
    {
        free_irq(SC_IRQ2, NULL);
    }
#endif

    return 0;
}

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
int MDrv_SC_ResetFIFO(struct file *filp, unsigned long arg)
#else
int MDrv_SC_ResetFIFO(struct inode *inode, struct file *filp, unsigned long arg)
#endif
{
    size_t u8SCID = (size_t)filp->private_data;

    SC_PRINT("%s is invoked\n", __FUNCTION__);
    _scInfo[u8SCID].u16FifoRxRead   = 0;
    _scInfo[u8SCID].u16FifoRxWrite  = 0;
    _scInfo[u8SCID].u16FifoTxRead   = 0;
    _scInfo[u8SCID].u16FifoTxWrite  = 0;

    return 0;
}

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
int MDrv_SC_GetEvent(struct file *filp, unsigned long arg)
#else
int MDrv_SC_GetEvent(struct inode *inode, struct file *filp, unsigned long arg)
#endif
{
    size_t u8SCID = (size_t)filp->private_data;

    put_user(_scInfo[u8SCID].u8CardStatus, (int __user *)arg);
    _scInfo[u8SCID].u8CardStatus = 0;

    return 0;
}

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
int mdb_sc_write_info(struct file *filp, unsigned long arg)
#else
int mdb_sc_write_info(struct inode *inode, struct file *filp, unsigned long arg)
#endif
{
    int err = 0;
    size_t u8SCID = (size_t)filp->private_data;
    SC_MdbgInfo info;

    if (copy_from_user(&info, (int __user *)arg, sizeof(SC_MdbgInfo)))
    {
        printk("[SC] %s: Failed to get info data from user\n", __FUNCTION__);
        err = -EFAULT;
        goto exit;
    }

    _scMdbInfo[u8SCID].bOpened = info.bOpened;
    _scMdbInfo[u8SCID].bCardIn = info.bCardIn;
    _scMdbInfo[u8SCID].u8Fi = info.u8Fi;
    _scMdbInfo[u8SCID].u8Di = info.u8Di;
    _scMdbInfo[u8SCID].u8Protocol = info.u8Protocol;
    _scMdbInfo[u8SCID].eVccCtrl = info.eVccCtrl;
    _scMdbInfo[u8SCID].eCardClk = info.eCardClk;
    _scMdbInfo[u8SCID].u16AtrLen = info.u16AtrLen;
    _scMdbInfo[u8SCID].u8PadConfig = info.u8PadConfig;
    memcpy(_scMdbInfo[u8SCID].pu8Atr, info.pu8Atr, _scMdbInfo[u8SCID].u16AtrLen);

exit:
    return err;
}

static void mdb_sc_print_ctrl_type(struct seq_file *m, MS_U8 u8SCID)
{
    switch(_scMdbInfo[u8SCID].eVccCtrl)
    {
        case E_SC_VCC_CTRL_8024_ON:
            seq_printf(m, "  Control type: 8024\n");
        break;
        case E_SC_VCC_CTRL_LOW:
            seq_printf(m, "  Control type: Active low\n");
        break;
        case E_SC_VCC_CTRL_HIGH:
            seq_printf(m, "  Control type: Active high\n");
        break;
        case E_SC_OCP_VCC_HIGH:
            seq_printf(m, "  Control type: ocp high\n");
        break;
        case E_SC_VCC_VCC_ONCHIP_8024:
            seq_printf(m, "  Control type: OnChip 8024\n");
        break;
        default:
            seq_printf(m, "  Control type: Unknow\n");
    }
}

static void mdb_sc_print_buffer(struct seq_file *m, char *name, MS_U8 *buf, MS_U32 len)
{
    MS_U8 i;

    seq_printf(m, "  %s:", name);
    for (i = 0; i < len; i++)
    {
        seq_printf(m, " 0x%02x", buf[i]);
    }
    seq_printf(m, "\n");
}

static void mdb_sc_print_rxfifo(struct seq_file *m, MS_U8 u8SCID, MS_U32 len)
{
    MS_U32 i;
    MS_U32 index = 0;

    spin_lock(&_scInfo[u8SCID].lock);

    seq_printf(m, "  History buffer:");
    if (len < _scInfo[u8SCID].u16FifoRxRead)
        index = _scInfo[u8SCID].u16FifoRxRead - len;
    else
        index = SC_FIFO_SIZE - (len - _scInfo[u8SCID].u16FifoRxRead);

    for (i = 0; i < len; i++)
    {
        if (index == SC_FIFO_SIZE)
            index = 0;

        seq_printf(m, " 0x%02x", _scInfo[u8SCID].u8FifoRx[index]);
        index++;
    }
    seq_printf(m, "\n");

    spin_unlock(&_scInfo[u8SCID].lock);
}

static void mdb_sc_print_clk_type(struct seq_file *m, MS_U8 u8SCID)
{
    switch(_scMdbInfo[u8SCID].eCardClk)
    {
        case E_SC_CLK_3M:
            seq_printf(m, "  Clock: 3M\n");
            break;
        case E_SC_CLK_4P5M:
            seq_printf(m, "  Clock: 4P5M\n");
            break;
        case E_SC_CLK_6M:
            seq_printf(m, "  Clock: 6M\n");
            break;
        case E_SC_CLK_13M:
            seq_printf(m, "  Clock: 13M\n");
            break;
        case E_SC_CLK_4M:
            seq_printf(m, "  Clock: 4M\n");
            break;
        default:
            seq_printf(m, "  Clock: Unknow\n");
    }
}

static int mdb_sc_node_show(struct seq_file *m, void *v)
{
    MS_U8 i;

    seq_printf(m, "---------MStar SmartCard Info---------\n");

    for (i = 0; i < SC_DEV_NUM; i++)
    {
        seq_printf(m, "SMC %d:\n", i);

        if (_scMdbInfo[i].bOpened == TRUE)
            seq_printf(m, "  Enable: True\n");
        else
            seq_printf(m, "  Enable: False\n");

        if (_scMdbInfo[i].bCardIn == TRUE)
            seq_printf(m, "  Status: Card in\n");
        else
            seq_printf(m, "  Status: Card out\n");

         mdb_sc_print_ctrl_type(m, i);

         seq_printf(m, "  Pad config: %u\n", _scMdbInfo[i].u8PadConfig);
         seq_printf(m, "  Protocol: T=%u\n", _scMdbInfo[i].u8Protocol);

         mdb_sc_print_buffer(m, "ATR", _scMdbInfo[i].pu8Atr,  _scMdbInfo[i].u16AtrLen);
         mdb_sc_print_rxfifo(m, i, _scMdbInfo[i].u16HistLen);

         mdb_sc_print_clk_type(m, i);

         seq_printf(m, "  Fi: %u\n", _scMdbInfo[i].u8Fi);
         seq_printf(m, "  Di: %u\n", _scMdbInfo[i].u8Di);
    }

    return 0;
}

static int mbd_sc_cmd_handler(char *cmd)
{
    MS_U8 i;

    for (i = 0; i < MDB_CMD_NUM; i++)
    {
        if (strcmp(cmd, mdb_sc_cmd_table[i].command) == 0)
            break;
    }

    if (i == MDB_CMD_NUM)
        return -EINVAL;

    switch(mdb_sc_cmd_table[i].index)
    {
        case MDB_SC_CMD_HELP:
            printk("*************** echo help ***************\n");
            printk("mdebug smc only support read command now\n");
            printk("*****************************************\n");
            break;
        case MDB_SC_CMD_UNKNOW:
            return -EINVAL;
    }

    return 0;
}


static ssize_t mdb_sc_node_write(struct file *file, const char __user *buffer, size_t count, loff_t *pos)
{
    char cmd[16];
    int ret = 0;

    if (count > MBD_CMD_SIZE - 1)
    {
        printk("Invalid command\n");
        return -EINVAL;
    }

    if (copy_from_user(cmd, buffer, count))
    {
        return -EFAULT;
    }

    if (cmd[count - 1] == '\n')
        cmd[count - 1] = '\0';

    ret = mbd_sc_cmd_handler(cmd);

    if (ret)
        return ret;
    else
        return count;
}

static int mdb_sc_node_open(struct inode *inode, struct file *file)
{
    return single_open(file, mdb_sc_node_show, NULL);
}
#endif
