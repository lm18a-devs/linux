////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2008 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (��MStar Confidential Information��) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// @file   mdrv_temp.h
/// @brief  TEMP Driver Interface
/// @author MStar Semiconductor Inc.
///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _MDRV_SC_H_
#define _MDRV_SC_H_

#include <linux/fs.h>
#include <linux/cdev.h>
#include "mdrv_types.h"
#include <linux/version.h>

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
#include <linux/spinlock.h>
#endif

//-------------------------------------------------------------------------------------------------
//  Driver Capability
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
//  Macro and Define
//-------------------------------------------------------------------------------------------------
#define SC_FIFO_SIZE                512                                 // Rx fifo size

// #define SC_DEBUG
#ifdef SC_DEBUG
#define SC_PRINT(_fmt, _args...)    printk(KERN_WARNING "[%s][%d] " _fmt, __FUNCTION__, __LINE__, ## _args)
#define SC_ASSERT(_con) \
    do { \
        if (!(_con)) { \
            printk(KERN_CRIT "BUG at %s:%d assert(%s)\n", \
                    __FILE__, __LINE__, #_con); \
            BUG(); \
        } \
    } while (0)
#else
#define SC_PRINT(fmt, args...)
#define SC_ASSERT(arg)
#endif

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
#define SC_ATR_LEN_MAX              33                                  ///< Maximum length of ATR
#endif

//-------------------------------------------------------------------------------------------------
//  Type and Structure
//-------------------------------------------------------------------------------------------------

typedef struct
{
    int                         s32Major;
    int                         s32Minor;
    struct cdev                 stCDev;
    struct file_operations      fops;
    struct fasync_struct        *async_queue; /* asynchronous readers */
} SC_DEV;

/// SmartCard Info
typedef struct
{
    BOOL                        bCardIn;                            ///Status care in
    BOOL                        bLastCardIn;
    U8                          u8CardStatus;
    wait_queue_head_t           stWaitQue;

    U8                          u8FifoRx[SC_FIFO_SIZE];
    U16                         u16FifoRxRead;
    U16                         u16FifoRxWrite;

    U8                          u8FifoTx[SC_FIFO_SIZE];
    U16                         u16FifoTxRead;
    U16                         u16FifoTxWrite;

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
    spinlock_t                  lock;
#endif
} SC_Info;

#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
/// SmartCard VCC control mode
typedef enum
{
    E_SC_VCC_CTRL_8024_ON,                                              ///< by external 8024 on
    E_SC_VCC_CTRL_LOW,                                                  ///< by direct Vcc (low active)
    E_SC_VCC_CTRL_HIGH,                                                 ///< by direct Vcc (high active)
    E_SC_OCP_VCC_HIGH,
    E_SC_VCC_VCC_ONCHIP_8024,
} SC_VccCtrl;

/// SmartCard CLK setting
typedef enum
{
    E_SC_CLK_3M,                                                        ///< 3 MHz
    E_SC_CLK_4P5M,                                                      ///< 4.5 MHz
    E_SC_CLK_6M,                                                        ///< 6 MHz
    E_SC_CLK_13M,                                                        ///< 6 MHz
    E_SC_CLK_4M,                                                        ///< 4 MHz
} SC_ClkCtrl;

// SmartCard mdebug info
typedef struct __attribute__((packed))
{
    MS_U8                       u8Protocol;                         ///T= Protocol
    MS_U8                       pu8Atr[SC_ATR_LEN_MAX];             ///Atr buffer
    MS_U8                       u8Fi;                               ///Fi
    MS_U8                       u8Di;                               ///Di
    MS_U8                       u8PadConfig;                        ///Pad config
    MS_U16                      u16AtrLen;                          ///Atr length
    MS_U16                      u16HistLen;                         ///History length
    MS_BOOL                     bOpened;                            ///Open
    MS_BOOL                     bCardIn;                            ///Status care in
    SC_VccCtrl                  eVccCtrl;
    SC_ClkCtrl                  eCardClk;                           ///< Clock
} SC_MdbgInfo;
#endif

//-------------------------------------------------------------------------------------------------
//  Function and Variable
//-------------------------------------------------------------------------------------------------
int MDrv_SC_Open(struct inode *inode, struct file *filp);
ssize_t MDrv_SC_Read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
ssize_t MDrv_SC_Write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos);
unsigned int MDrv_SC_Poll(struct file *filp, poll_table *wait);
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
int MDrv_SC_AttachInterrupt(struct file *filp, unsigned long arg);
int MDrv_SC_DetachInterrupt(struct file *filp, unsigned long arg);
int MDrv_SC_ResetFIFO(struct file *filp, unsigned long arg);
int MDrv_SC_GetEvent(struct file *filp, unsigned long arg);
#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
int mdb_sc_write_info(struct file *filp, unsigned long arg);
#endif
#else
int MDrv_SC_AttachInterrupt(struct inode *inode, struct file *filp, unsigned long arg);
int MDrv_SC_DetachInterrupt(struct inode *inode, struct file *filp, unsigned long arg);
int MDrv_SC_ResetFIFO(struct inode *inode, struct file *filp, unsigned long arg);
int MDrv_SC_GetEvent(struct inode *inode, struct file *filp, unsigned long arg);
#if defined(CONFIG_MSTAR_UTOPIA2K_MDEBUG)
int mdb_sc_write_info(struct inode *inode, struct file *filp, unsigned long arg);
#endif
#endif


#endif // _MDRV_TEMP_H_

