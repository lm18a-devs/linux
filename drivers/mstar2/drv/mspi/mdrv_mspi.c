//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2008-2010 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// ("MStar Confidential Information") by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
/// file    mdrv_mspi.c
/// @brief  mspi Driver
/// @author MStar Semiconductor Inc.
///////////////////////////////////////////////////////////////////////////////////////////////////




#include <linux/autoconf.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <linux/kdev_t.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/poll.h>
#include <linux/wait.h>
#include <linux/time.h>
#include <linux/timer.h>
#include <linux/types.h>
#include <linux/input.h>
#include <linux/spinlock.h>
#include <linux/semaphore.h>
#include <linux/platform_device.h>
#include <asm/io.h>

#include "mdrv_mspi.h"
#include "mhal_mspi.h"

//-------------------------------------------------------------------------------------------------
//  Local Functions
//-------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Global Functions
//-------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// Description : Set detailed level of MSPI driver debug message
/// @param u8DbgLevel    \b IN  debug level for Serial Flash driver
/// @return TRUE : succeed
/// @return FALSE : failed to set the debug level
//------------------------------------------------------------------------------
MS_BOOL MDrv_MSPI_SetDbgLevel(MS_U8 u8DbgLevel)
{
    u8DbgLevel = u8DbgLevel;

    return TRUE;
}
EXPORT_SYMBOL(MDrv_MSPI_SetDbgLevel);

//------------------------------------------------------------------------------
/// Description : MSPI initial
/// @return E_MSPI_OK :
/// @return >1 : failed to initial
//------------------------------------------------------------------------------
MSPI_ErrorNo MDrv_MSPI_Init(MSPI_CH eChannel)
{
    MSPI_ErrorNo errorno = E_MSPI_OK;
    HAL_MSPI_Init(eChannel);
    HAL_MSPI_IntEnable(eChannel,TRUE);
    return errorno;
}
EXPORT_SYMBOL(MDrv_MSPI_Init);

//-------------------------------------------------------------------------------------------------
/// Description : read and write  MSPI
/// @param pData \b IN :pointer to receive data from MSPI read buffer
/// @param u32Size \ b OTU : read data size
/// @return the errorno of operation
//-------------------------------------------------------------------------------------------------
MS_U32  MDrv_MSPI_Read_Write(MSPI_CH eChannel,MS_U8 *pReadData,MS_U8 *pWriteData, MS_U32 u32Size)
{
    MS_U32  u32Index = 0;
    MS_U32  u32TempFrameCnt=0;
    MS_U32  u32TempLastFrameCnt=0;
    MS_BOOL ret = false;
    u32TempFrameCnt = u32Size/MAX_WRITE_BUF_SIZE; //Cut data to frame by max frame size
    u32TempLastFrameCnt = u32Size%MAX_WRITE_BUF_SIZE; //Last data less than a MAX_WRITE_BUF_SIZE fame
    for (u32Index = 0; u32Index < u32TempFrameCnt; u32Index++) {
        ret = HAL_MSPI_Read_Write(eChannel,pReadData+u32Index*MAX_WRITE_BUF_SIZE,pWriteData+u32Index*MAX_WRITE_BUF_SIZE,MAX_WRITE_BUF_SIZE);
        if (!ret) {
            return false;
        }
    }
    if(u32TempLastFrameCnt) {
        ret = HAL_MSPI_Read_Write(eChannel,pReadData+u32TempFrameCnt*MAX_WRITE_BUF_SIZE,pWriteData+u32TempFrameCnt*MAX_WRITE_BUF_SIZE,u32TempLastFrameCnt);
    }
    return ret;
}
EXPORT_SYMBOL(MDrv_MSPI_Read_Write);
//-------------------------------------------------------------------------------------------------
/// Description : read data from MSPI
/// @param pData \b IN :pointer to receive data from MSPI read buffer
/// @param u32Size \ b OTU : read data size
/// @return the errorno of operation
//-------------------------------------------------------------------------------------------------
MSPI_ErrorNo MDrv_MSPI_Read(MSPI_CH eChannel, MS_U8 *pData, MS_U32 u32Size)
{
    MS_U32  u32Index = 0;
    MS_U32  u32TempFrameCnt=0;
    MS_U32  u32TempLastFrameCnt=0;
    int  ret = 0;
    if(pData == NULL) {
        return E_MSPI_NULL;
    }
    HAL_MSPI_SetChipSelect( eChannel,  TRUE,  E_MSPI_ChipSelect_0);
    u32TempFrameCnt = u32Size/MAX_WRITE_BUF_SIZE; //Cut data to frame by max frame size
    u32TempLastFrameCnt = u32Size%MAX_WRITE_BUF_SIZE; //Last data less than a MAX_WRITE_BUF_SIZE fame
    for (u32Index = 0; u32Index < u32TempFrameCnt; u32Index++) {
        ret = HAL_MSPI_Read(eChannel,pData+u32Index*MAX_WRITE_BUF_SIZE,MAX_WRITE_BUF_SIZE);
        if (!ret) {
            return E_MSPI_OPERATION_ERROR;
        }
    }
    if(u32TempLastFrameCnt) {
        ret = HAL_MSPI_Read(eChannel,pData+u32TempFrameCnt*MAX_WRITE_BUF_SIZE,u32TempLastFrameCnt);
    }
    HAL_MSPI_SetChipSelect( eChannel,  false,  E_MSPI_ChipSelect_0);
    if (!ret) {
        return E_MSPI_OPERATION_ERROR;
    }
    return E_MSPI_OK;
}
EXPORT_SYMBOL(MDrv_MSPI_Read);

//------------------------------------------------------------------------------
/// Description : read data from MSPI
/// @param pData \b OUT :pointer to write  data to MSPI write buffer
/// @param u32Size \ b OTU : write data size
/// @return the errorno of operation
//------------------------------------------------------------------------------
MSPI_ErrorNo MDrv_MSPI_Write(MSPI_CH eChannel, MS_U8 *pData, MS_U32 u32Size)
{
    MS_U32  u32Index = 0;
    MS_U32  u32TempFrameCnt=0;
    MS_U32  u32TempLastFrameCnt=0;
    MS_BOOL  ret = false;
    u32TempFrameCnt = u32Size/MAX_WRITE_BUF_SIZE; //Cut data to frame by max frame size
    u32TempLastFrameCnt = u32Size%MAX_WRITE_BUF_SIZE; //Last data less than a MAX_WRITE_BUF_SIZE fame
    for (u32Index = 0; u32Index < u32TempFrameCnt; u32Index++) {
        ret = HAL_MSPI_Write(eChannel,pData+u32Index*MAX_WRITE_BUF_SIZE,MAX_WRITE_BUF_SIZE);
        if (!ret) {
            return E_MSPI_OPERATION_ERROR;
        }
    }

    if(u32TempLastFrameCnt) {
        ret = HAL_MSPI_Write(eChannel,pData+u32TempFrameCnt*MAX_WRITE_BUF_SIZE,u32TempLastFrameCnt);
    }

    if (!ret) {
        return E_MSPI_OPERATION_ERROR;
    }
    return E_MSPI_OK;
}
EXPORT_SYMBOL(MDrv_MSPI_Write);

MSPI_ErrorNo MDrv_MSPI_SetReadBufferSize(MSPI_CH eChannel,  MS_U8 u8Size)
{
    HAL_MSPI_SetReadBufferSize( eChannel,  u8Size);
    return E_MSPI_OK;

}
EXPORT_SYMBOL(MDrv_MSPI_SetReadBufferSize);


MSPI_ErrorNo MDrv_MSPI_SetWriteBufferSize(MSPI_CH eChannel,  MS_U8 u8Size)
{
        HAL_MSPI_SetWriteBufferSize( eChannel,  u8Size);
        return E_MSPI_OK;
}
EXPORT_SYMBOL(MDrv_MSPI_SetWriteBufferSize);
//------------------------------------------------------------------------------
/// Description : config spi transfer timing
/// @param ptDCConfig    \b OUT  struct pointer of transfer timing config
/// @return E_MSPI_OK : succeed
/// @return E_MSPI_DCCONFIG_ERROR : failed to config transfer timing
//------------------------------------------------------------------------------
MSPI_ErrorNo MDrv_MSPI_DCConfig(MSPI_CH eChannel, MSPI_DCConfig *ptDCConfig)
{
    MSPI_ErrorNo errnum = E_MSPI_OK;

    if(ptDCConfig == NULL) {
        HAL_MSPI_Reset_DCConfig(eChannel);
        return E_MSPI_OK;
    }
    HAL_MSPI_SetDcTiming(eChannel, E_MSPI_TRSTART ,ptDCConfig->u8TrStart);
    HAL_MSPI_SetDcTiming(eChannel, E_MSPI_TREND ,ptDCConfig->u8TrEnd);
    HAL_MSPI_SetDcTiming(eChannel, E_MSPI_TB ,ptDCConfig->u8TB);
    HAL_MSPI_SetDcTiming(eChannel, E_MSPI_TRW ,ptDCConfig->u8TRW);
    return errnum;
}
EXPORT_SYMBOL(MDrv_MSPI_DCConfig);

//------------------------------------------------------------------------------
/// Description : config spi clock setting
/// @param ptCLKConfig    \b OUT  struct pointer of clock config
/// @return E_MSPI_OK : succeed
/// @return E_MSPI_CLKCONFIG_ERROR : failed to config spi clock
//------------------------------------------------------------------------------
MSPI_ErrorNo  MDrv_MSPI_SetMode(MSPI_CH eChannel, MSPI_Mode_Config_e eMode)
{
    if (eMode >= E_MSPI_MODE_MAX) {
        return E_MSPI_PARAM_OVERFLOW;
    }

    switch (eMode) {
    case E_MSPI_MODE0:
        HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_POL ,false);
        HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_PHA ,false);

        break;
    case E_MSPI_MODE1:
        HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_POL ,false);
        HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_PHA ,true);
        break;
    case E_MSPI_MODE2:
        HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_POL ,true);
        HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_PHA ,false);
        break;
    case E_MSPI_MODE3:
        HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_POL ,true);
        HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_PHA ,true);
        break;
    default:
        return E_MSPI_OPERATION_ERROR;
    }

    return E_MSPI_OK;
}
EXPORT_SYMBOL(MDrv_MSPI_SetMode);

MSPI_ErrorNo MDrv_MSPI_SetCLK(MSPI_CH eChannel, MS_U8 U8ClockDiv)
{
    MSPI_ErrorNo errnum = E_MSPI_OK;
    HAL_MSPI_SetCLKTiming(eChannel, E_MSPI_CLK ,U8ClockDiv);
    return errnum;

}
EXPORT_SYMBOL(MDrv_MSPI_SetCLK);

// add to sync code from utopia for localdimming to set clk by ini
MSPI_ErrorNo MDrv_MSPI_SetCLKByINI(MSPI_CH eChannel, MS_U32 u32MspiClk)
{
    HAL_MSPI_LD_CLK_Config(eChannel,u32MspiClk);
    return E_MSPI_OK;
}
EXPORT_SYMBOL(MDrv_MSPI_SetCLKByINI);


MS_BOOL MDrv_MSPI_CLOCK_Config(MSPI_CH eChannel, MS_U32 u32MaxClock)
{
    return  HAL_MSPI_CLOCK_Config( eChannel,  u32MaxClock);
}
EXPORT_SYMBOL(MDrv_MSPI_CLOCK_Config);

//------------------------------------------------------------------------------
/// Description : config spi transfer timing
/// @param ptDCConfig    \b OUT  struct pointer of bits of buffer tranferred to slave config
/// @return E_MSPI_OK : succeed
/// @return E_MSPI_FRAMECONFIG_ERROR : failed to config transfered bit per buffer
//------------------------------------------------------------------------------
MSPI_ErrorNo MDrv_MSPI_FRAMEConfig(MSPI_CH eChannel, MSPI_FrameConfig  *ptFrameConfig)
{
    MSPI_ErrorNo errnum = E_MSPI_OK;
    MS_U8 u8Index = 0;

    if(ptFrameConfig == NULL) {
        HAL_MSPI_Reset_FrameConfig(eChannel);
        return E_MSPI_OK;
    }
    // read buffer bit config
    for(u8Index = 0; u8Index < MAX_READ_BUF_SIZE; u8Index++) {
        if(ptFrameConfig->u8RBitConfig[u8Index] > MSPI_FRAME_BIT_MAX) {
            errnum = E_MSPI_PARAM_OVERFLOW;
        } else {
            HAL_MSPI_SetPerFrameSize(eChannel, MSPI_READ_INDEX,  u8Index, ptFrameConfig->u8RBitConfig[u8Index]);
        }
    }
    //write buffer bit config
    for(u8Index = 0; u8Index < MAX_WRITE_BUF_SIZE; u8Index++) {
        if(ptFrameConfig->u8WBitConfig[u8Index] > MSPI_FRAME_BIT_MAX) {
            errnum = E_MSPI_PARAM_OVERFLOW;
        } else {
            HAL_MSPI_SetPerFrameSize(eChannel, MSPI_WRITE_INDEX,  u8Index, ptFrameConfig->u8WBitConfig[u8Index]);
        }
    }
    return errnum;
}
EXPORT_SYMBOL(MDrv_MSPI_FRAMEConfig);

//-------------------------------------------------------
// Description : MSPI Power state
//-------------------------------------------------------
MS_U32 MDrv_MSPI_SetPowerState(void)
{
    return 0;
}
EXPORT_SYMBOL(MDrv_MSPI_SetPowerState);
