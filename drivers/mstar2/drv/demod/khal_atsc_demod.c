//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
/**********************************************************************
 Copyright (c) 2006-2010 MStar Semiconductor, Inc.
 All rights reserved.

 Unless otherwise stipulated in writing, any and all information contained
 herein regardless in any format shall remain the sole proprietary of
 MStar Semiconductor Inc. and be kept in strict confidence
 (MStar Confidential Information) by the recipient.
 Any unauthorized act including without limitation unauthorized disclosure,
 copying, use, reproduction, sale, distribution, modification, disassembling,
 reverse engineering and compiling of the contents of MStar Confidential
 Information is unlawful and strictly prohibited. MStar hereby reserves the
 rights to any and all damages, losses, costs and expenses resulting therefrom.

* Class :
* File  :
**********************************************************************/
/******************************************************************************
    File Inclusions
******************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/time.h>
#include <linux/delay.h>

#include "drvDMD_ATSC.h"
#include "mhal_demod.h"
#include "mdrv_types.h"
#include "khal_demod_vqi.h"
#include "khal_demod_common.h"
#include "mdrv_system_LG.h"

/******************************************************************************
    Macro Definitions
******************************************************************************/



/******************************************************************************
    Extern Variables & Function Prototype Declarations
******************************************************************************/



/******************************************************************************
    Local Constant Definitions
******************************************************************************/

// unit : ms
#define MSB_LOCK_DEALY_50       50
#define MSB_LOCK_DEALY_100      100
#define MSB_LOCK_DEALY_150      150
#define MSB_LOCK_DEALY_170      170
#define MSB_LOCK_DEALY_200      200
#define MSB_LOCK_DEALY_250      250
#define MSB_LOCK_DEALY_300      300
#define MSB_LOCK_DEALY_400      400
#define MSB_LOCK_DEALY_450      450
#define MSB_LOCK_DEALY_500      500
#define MSB_LOCK_DEALY_600      600
#define MSB_LOCK_DEALY_700      700
#define MSB_LOCK_DEALY_800      800
#define MSB_LOCK_DEALY_1000     1000
#define MSB_LOCK_DEALY_1500     1500
#define MSB_LOCK_DEALY_2000     2000
#define MSB_LOCK_DEALY_3000     3000
#define MSB_LOCK_DEALY_5000     5000

/******************************************************************************
    Local Type Definitions
******************************************************************************/

typedef enum
{
    // =============================================== //
    // ------- these locks are treated as 'locked' when scan mode --------- //
    MSB_LOCK_OK         = 0x00, // 0000 XXXX    // keep the position and value !!!

    MSB_LOCK_WEAK_LOCK  = 0x10, // 0001 XXXX        // keep the position and value !!!
    MSB_LOCK_FEC_LOCK,
    MSB_LOCK_TPS_LOCK,
    MSB_LOCK_SYM_LOCK,
    // =============================================== //

    // =============================================== //
    // ------- these locks are treated as 'not locked' when scan mode ------- //
    MSB_LOCK_POOR_LOCK  = 0x20, // 0100 XXXX    // keep the position and value !!!
    MSB_LOCK_AGC_LOCK,

    MSB_LOCK_PILOT_LOCK,
    MSB_LOCK_FSYNC_LOCK,
    MSB_LOCK_TR_LOCK,

    MSB_LOCK_NOT_LOCKED = 0x40, // 1000 XXXX    // keep the position and value !!!
    MSB_LOCK_UNLOCK,
    // =============================================== //

    MSB_LOCK_UNKNOWN    = 0x80, // 0010 XXXX    // keep the position and value !!!
    MSB_LOCK_OTHERS,            // 0010 XXXX    // keep the position and value

    MSB_LOCK_MASK       = 0xf0
} MSB_LOCK_STATE_T;

/******************************************************************************
    Global Type Definitions
******************************************************************************/



/******************************************************************************
    Static Variables & Function Prototypes Declarations
******************************************************************************/
static UINT16 _gUnlockCount = 0;
static KHAL_DEMOD_TUNE_MODE_T _gtuneMode = KHAL_DEMOD_TUNE_NORMAL;
static KHAL_DEMOD_TPS_CONSTELLATION_T _gconstMode = KHAL_DEMOD_TPS_CONST_UNKNOWN;
static BOOLEAN _gbSpectrumInv = FALSE;
static KHAL_DEMOD_TRANS_SYSTEM_T _gtransSystem = KHAL_DEMOD_TRANS_SYS_UNKNOWN;
static KHAL_DEMOD_LOCK_STATE_T _glockState = KHAL_DEMOD_LOCK_UNKNOWN;

static KHAL_DEMOD_LOCK_STATE_T MSB_ConvertLockState(MSB_LOCK_STATE_T tunerLock);
static MSB_LOCK_STATE_T MSB_CheckVsbLock(void);
static MSB_LOCK_STATE_T MSB_CheckQamLock(void);
static int MSB_GetSignalState(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState);
static int MSB_OutputOn(void);
static int MSB_OutputOff(void);
static MS_U32 MSB_Get_System_Time(void);
static void MSB_Delay_MS(MS_U32 delay_time);
static MS_BOOL MSB_Cretate_mutex(MS_BOOL on);
static void MSB_LockDMD(MS_BOOL LOCK);
static int MSB_Demod_ATSC_Suspend(void);
static int MSB_Demod_ATSC_Resume(void);


/******************************************************************************
    Global Variables & Function Prototypes Declarations
******************************************************************************/



/******************************************************************************
    Local Variables & Function Prototypes Declarations
******************************************************************************/



/******************************************************************************
    Function Definitions
******************************************************************************/

/**=========================================================================**/
/**         F U N C T I O N S                                               **/
/**=========================================================================**/

/**
 * brief description.
 * detailed description. a normal member taking two.. bla bla.
 *
 * @param tunerLock [IN] MSB1501_LOCK_STATE_T
 * @return OK/WEAK/POOR/FAIL/UNKNOWN
 * @see
 * @see
 * @author  Goldman(jhgold@lge.com)
*/
static KHAL_DEMOD_LOCK_STATE_T MSB_ConvertLockState(MSB_LOCK_STATE_T tunerLock)
{
    if (MSB_LOCK_OK == tunerLock)
        return KHAL_DEMOD_LOCK_OK;
    /* LG Need*/
    else if (MSB_LOCK_WEAK_LOCK == (tunerLock & MSB_LOCK_MASK))
        return KHAL_DEMOD_LOCK_FAIL;
    else if (MSB_LOCK_POOR_LOCK == (tunerLock & MSB_LOCK_MASK))
        return KHAL_DEMOD_LOCK_FAIL;
    else if (MSB_LOCK_NOT_LOCKED == (tunerLock & MSB_LOCK_MASK))
        return KHAL_DEMOD_LOCK_FAIL;
    else
        return KHAL_DEMOD_LOCK_FAIL;
}
/**
 * check sync lock for VSB mode.
 * detailed description. a normal member taking two.. bla bla.
 *
 * @param tunerNo [IN] TUNER_NUMBER_T
 * @return if Succeeded - OK else-NOT OK
 * @see
 * @see
 * @author  Goldman(jhgold@lge.com) : ok
*/
static MSB_LOCK_STATE_T MSB_CheckVsbLock(void)
{
    BOOLEAN prLock=FALSE, fSyncLock=FALSE, fecLock=FALSE;
    DMD_ATSC_GETLOCK_TYPE sDMD_ATSC_GETLOCK_TYPE;

    sDMD_ATSC_GETLOCK_TYPE = DMD_ATSC_GETLOCK_VSB_FECLOCK;
    if (_MDrv_DMD_ATSC_MD_GetLock(0, sDMD_ATSC_GETLOCK_TYPE) == DMD_ATSC_LOCK)
    {
       // PRINT( "^r^[TU DTV] FEC Lock OK Vsb_FEC_Lock !!!\n");
        fecLock = TRUE;
    }
    else
    {
       // PRINT("^r^[TU DTV] FEC Lock NG Vsb_FEC_Lock !!!\n");
        fecLock = FALSE;
    }

    if (fecLock == TRUE) return MSB_LOCK_OK;

    /* check Pilot lock */
    sDMD_ATSC_GETLOCK_TYPE = DMD_ATSC_GETLOCK_VSB_PRELOCK;
    if (_MDrv_DMD_ATSC_MD_GetLock(0, sDMD_ATSC_GETLOCK_TYPE) == DMD_ATSC_LOCK)
    {
        //PRINT("^r^[TU DTV] Pre Lock OK () Vsb_PreLock !!!\n");
        prLock = TRUE;
    }
    else
    {
        //PRINT("^r^[TU DTV] Pre Lock NG () Vsb_PreLock !!!\n");
        prLock = FALSE;
    }

    /* check F-Sync  lock */
    sDMD_ATSC_GETLOCK_TYPE = DMD_ATSC_GETLOCK_VSB_FSYNCLOCK;
    if (_MDrv_DMD_ATSC_MD_GetLock(0, sDMD_ATSC_GETLOCK_TYPE) == DMD_ATSC_LOCK)
    {
        //PRINT("^r^[TU DTV] FSync Lock OK () Vsb_FSync_Lock !!!\n");
        fSyncLock = TRUE;
    }
    else
    {
        //PRINT("^r^[TUDTV] FSync Lock NG () Vsb_FSync_Lock !!!\n");
        fSyncLock = FALSE;
    }

    if (fSyncLock==TRUE)
        return MSB_LOCK_FSYNC_LOCK;
    else if (prLock==TRUE)
        return MSB_LOCK_PILOT_LOCK;
    else return MSB_LOCK_NOT_LOCKED;

    return (MSB_LOCK_UNKNOWN);
}
/**
 * check sync lock for QAM mode.
 * detailed description. a normal member taking two.. bla bla.
 *
 * @param tunerNo [IN] TUNER_NUMBER_T
 * @return if Succeeded - OK else-NOT OK
 * @see
 * @see
 * @author  Goldman(jhgold@lge.com) : ok
*/
static MSB_LOCK_STATE_T MSB_CheckQamLock(void)
{
    BOOLEAN trLock=FALSE, fecLock=FALSE;
    DMD_ATSC_GETLOCK_TYPE sDMD_ATSC_GETLOCK_TYPE;

    /* check TR lock */
    sDMD_ATSC_GETLOCK_TYPE = DMD_ATSC_GETLOCK_QAM_PRELOCK;
    if (_MDrv_DMD_ATSC_MD_GetLock(0, sDMD_ATSC_GETLOCK_TYPE) == DMD_ATSC_LOCK)
    {
       // PRINT("^r^[TU DTV] TR Lock OK () MSB_QAM_PreLock !!!\n");
        trLock=TRUE;
    }
    else
    {
 //       PRINT("^r^[TU DTV] TR Lock NG () MSB_QAM_PreLock !!!\n");
        trLock=FALSE;
    }

    /* check FEC lock */
    sDMD_ATSC_GETLOCK_TYPE = DMD_ATSC_GETLOCK_QAM_MAINLOCK;
    if (_MDrv_DMD_ATSC_MD_GetLock(0, sDMD_ATSC_GETLOCK_TYPE) == DMD_ATSC_LOCK)
    {
       // PRINT("^r^[TU DTV] FEC Lock OK () MSB_QAM_Main_Lock !!!\n");
        fecLock=TRUE;
    }
    else
    {
     //   PRINT("^r^[TU DTV] FEC Lock NG () MSB_QAM_Main_Lock !!!\n");
        fecLock=FALSE;
    }

    if(fecLock==TRUE)
        return MSB_LOCK_OK;
    else if(trLock==TRUE)
        return MSB_LOCK_TR_LOCK;
    else
        return MSB_LOCK_NOT_LOCKED;

    return(MSB_LOCK_UNKNOWN);
}

static U8 MSB_CheckATSCStatus(void)
{
    U8 u8Data=0;
    _MDrv_DMD_ATSC_MD_GetReg(0, 0x20C1, &u8Data);
    return u8Data;
}
/**
 * Get Signal State.
 *
 * @param tunerNo       [IN] TUNER_NUMBER_T
 * @param pSignalState  [OUT] TU_SIGNAL_STATE_T
 * @return if succeed - TU_RET_OK, else - TU_RET_FAIL
 * @see
 * @see
 * @author  Goldman(jhgold@lge.com)
*/
static int MSB_GetSignalState(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
    KHAL_DEMOD_SIGNAL_STATE_T signalState;
    DMD_ATSC_BER_DATA sDMD_ATSC_BER_DATA;

    signalState.bSignalValid = FALSE;
    signalState.unSNR        = 0xFFFFFFFF;
    signalState.unBER        = 0xFFFFFFFF;
    signalState.packetError  = 0;
    signalState.unAGC        = 0;
    signalState.strength     = 0;
    signalState.quality      = 0;
   // signalState.unSQI        = 0;
   // PRINT("kavana MSB_GetSignalState _gtransSystem = %d!!\n",_gtransSystem);
   // PRINT("kavana MSB_GetSignalState _glockState = %d!!\n",_glockState);
    if (KHAL_DEMOD_LOCK_OK == _glockState)
    {
        /* VSB or QAM */
        if ((_gtransSystem == KHAL_DEMOD_TRANS_SYS_VSB) || (_gtransSystem == KHAL_DEMOD_TRANS_SYS_QAM))
        {
            signalState.unSNR = _MDrv_DMD_ATSC_MD_GetSNRPercentage(0);
            
            _MDrv_DMD_ATSC_MD_GetPostViterbiBer(0, &sDMD_ATSC_BER_DATA);
            signalState.unBER=sDMD_ATSC_BER_DATA.BitErr;
        
            _MDrv_DMD_ATSC_MD_Read_uCPKT_ERR(0, (MS_U16 *)&signalState.packetError);
            _MDrv_DMD_ATSC_MD_GetSignalStrength(0,(MS_U16 *)&signalState.unAGC);
        }
        else 
        {
            pSignalState->bSignalValid = FALSE;
            return (NOT_OK);
        }
    }
    signalState.unSNR = signalState.unSNR*4/10;
    signalState.bSignalValid = TRUE;
    *pSignalState = signalState;
    return (OK);
}
/**
 * brief description.
 * detailed description. a normal member taking two.. bla bla.
 *
 * @param   tunerNo [IN] tuner number
 * @return  TU_RET_OK
 * @see
 * @see
 * @author  Goldman(jhgold@lge.com)
*/
static int MSB_OutputOn(void)
{
    return (OK);
}
/**
 * brief description.
 * detailed description. a normal member taking two.. bla bla.
 *
 * @param   tunerNo [IN] tuner number
 * @return  TU_RET_OK
 * @see
 * @see
 * @author  Goldman(jhgold@lge.com)
*/
static int MSB_OutputOff(void)
{
    return (OK);
}

static MS_U32 MSB_Get_System_Time(void)
{
    static struct timeval curr;
    
    do_gettimeofday(&curr);

    return (curr.tv_sec*1000 + curr.tv_usec/1000);
}

static void MSB_Delay_MS(MS_U32 delay_time)
{
    mdelay(delay_time);
}



static MS_BOOL MSB_Cretate_mutex(MS_BOOL on)
{
    return TRUE;
}

static void MSB_LockDMD(MS_BOOL LOCK)
{
    if (LOCK)
        MHal_Demod_Mutex_Lock();
    else MHal_Demod_Mutex_Unlock();
}
static int MSB_Demod_ATSC_Suspend(void)
{
    return _MDrv_DMD_ATSC_MD_SetPowerState(0,E_POWER_SUSPEND);
}
static int MSB_Demod_ATSC_Resume(void)
{
    return _MDrv_DMD_ATSC_MD_SetPowerState(0,E_POWER_RESUME);
}

UINT32 KHAL_DEMOD_VQI_Probe(struct i2c_client *client)
{
    
    return (OK);
}

/**
Initializes the specific demodulator
*/
int KHAL_DEMOD_VQI_Initialize(void)
{
    return (OK);
}
/**
Changes demodulator setting according to transmissi media.
*/
int KHAL_DEMOD_VQI_ChangeTransSystem(KHAL_DEMOD_TRANS_SYSTEM_T transSystem)
{
    DMD_ATSC_InitData sDMD_ATSC_InitData;
    PRINT("kavana ATSC_QAM KHAL_DEMOD_VQI_ChangeTransSystem=%d!!\n", transSystem);
   
    if ((transSystem == KHAL_DEMOD_TRANS_SYS_VSB) || (transSystem == KHAL_DEMOD_TRANS_SYS_QAM))
    {
        if(MHal_Demod_VDMcuHandler(DEMOD_STANDARD_ATSC))
            PRINT("kavana change DEMOD_STANDARD_ATSC !!!\n");
        
        PRINT("kenji ATSC_QAM KHAL_DEMOD_VQI_Initialize!!\n");
        memset(&sDMD_ATSC_InitData, 0, sizeof(DMD_ATSC_InitData));
       
        sDMD_ATSC_InitData.u16VSBAGCLockCheckTime    = 150;//50; from set config to 1st time at least 50 up
        sDMD_ATSC_InitData.u16VSBPreLockCheckTime    = 300; //300;
        sDMD_ATSC_InitData.u16VSBFSyncLockCheckTime  = 900;//1200;//1200;
        sDMD_ATSC_InitData.u16VSBFECLockCheckTime    = 2000;//5000;//5000;
        sDMD_ATSC_InitData.u16QAMAGCLockCheckTime    = 60;//50;
        sDMD_ATSC_InitData.u16QAMPreLockCheckTime    = 1000;//1000;
        sDMD_ATSC_InitData.u16QAMMainLockCheckTime   = 3000;
       
        // register init
        sDMD_ATSC_InitData.u8DMD_ATSC_DSPRegInitExt  = NULL; // TODO use system variable type
        sDMD_ATSC_InitData.u8DMD_ATSC_DSPRegInitSize = 0;
        sDMD_ATSC_InitData.u8DMD_ATSC_InitExt        = NULL;
       
        //By Tuners:
        sDMD_ATSC_InitData.u16IF_KHZ                 = 6000;
        sDMD_ATSC_InitData.bIQSwap                   = 1;
        sDMD_ATSC_InitData.u16AGC_REFERENCE          = 0;
        sDMD_ATSC_InitData.bTunerGainInvert          = 0;
        sDMD_ATSC_InitData.bIsQPad                   = 0; //Don't beed used anymore
       
        //By IC:
        sDMD_ATSC_InitData.u8IS_DUAL                 = 0;
        sDMD_ATSC_InitData.bIsExtDemod               = 0;
       
        sDMD_ATSC_InitData.GetSystemTimeMS           = MSB_Get_System_Time;
        sDMD_ATSC_InitData.DelayMS                   = MSB_Delay_MS;
        sDMD_ATSC_InitData.CreateMutex               = MSB_Cretate_mutex;
        sDMD_ATSC_InitData.LockDMD                   = MSB_LockDMD;
        PRINT("kavana ATSC_QAM _MDrv_DMD_ATSC_MD_Init!!\n");
        if (_MDrv_DMD_ATSC_MD_Init(0, &sDMD_ATSC_InitData, sizeof(sDMD_ATSC_InitData)) != TRUE)
        {
            PRINT("^r^[TUDTV] FAIL () MDrv_DMD_ATSC_MD_Init !!!\n");
       
            return(NOT_OK);
        }
        MSB_LockDMD(TRUE);    
        SetDemodSTRHandle(MSB_Demod_ATSC_Suspend,MSB_Demod_ATSC_Resume);    
        MSB_LockDMD(FALSE);
    }
    else
    {
        if (MHal_Demod_VDMcuHandler(DEMOD_STANDARD_UNKNOWN))//
            PRINT("kavana change SYSTEM_TYPE_UNKNOWN !!!\n");
    }
    return (OK);
}
/**
Set Demod. with the specified parameters
*/
int KHAL_DEMOD_VQI_ATSC_SetDemod(KHAL_DEMOD_ATSC_SET_PARAM_T paramStruct)
{
    DMD_ATSC_DEMOD_TYPE sDMD_ATSC_DEMOD_TYPE = DMD_ATSC_DEMOD_NULL;

    _gbSpectrumInv = paramStruct.bSpectrumInv;
    _gtransSystem  = paramStruct.transSystem;
    _gconstMode    = paramStruct.constellation;
    _gtuneMode     = paramStruct.tuneMode;

   // PRINT("kavana check KHAL_DEMOD_VQI_ATSC_SetDemod _gconstMode= %d!!\n",_gconstMode);
    PRINT("kavana check KHAL_DEMOD_VQI_ATSC_SetDemod _gtransSystem = %d!!\n",_gtransSystem);

    if (_gconstMode == KHAL_DEMOD_TPS_CONST_VSB_8)
        sDMD_ATSC_DEMOD_TYPE = DMD_ATSC_DEMOD_ATSC_VSB;
    else if (_gconstMode == KHAL_DEMOD_TPS_CONST_QAM_64)
        sDMD_ATSC_DEMOD_TYPE = DMD_ATSC_DEMOD_ATSC_64QAM;
    else if (_gconstMode == KHAL_DEMOD_TPS_CONST_QAM_256)
        sDMD_ATSC_DEMOD_TYPE = DMD_ATSC_DEMOD_ATSC_256QAM;
    else return (NOT_OK);

    if (_MDrv_DMD_ATSC_MD_SetConfig(0, sDMD_ATSC_DEMOD_TYPE , TRUE) == FALSE)
    {
        PRINT("^r^%s[TU DTV] FAIL () MDrv_DMD_ATSC_MD_SetConfig !!!\n", __FUNCTION__);

        return  (NOT_OK);
    }

    _gUnlockCount   = 0;

    return (OK);
}
/**
Check the Demod. lock status after demod stting.
If signal lock status is valid, set *pFishisd value as true.
*/
int KHAL_DEMOD_VQI_VSB_TunePostJob(BOOLEAN *pFinished)
{
    DMD_ATSC_GETLOCK_TYPE sDMD_ATSC_GETLOCK_TYPE;
    sDMD_ATSC_GETLOCK_TYPE = DMD_ATSC_GETLOCK;
    if(_MDrv_DMD_ATSC_MD_GetLock(0, sDMD_ATSC_GETLOCK_TYPE) == DMD_ATSC_CHECKING)
       *pFinished=FALSE;
    else  //DMD_ATSC_LOCK or DMD_ATSC_UNLOCK
       *pFinished=TRUE;
    printk(KERN_DEBUG "\n\rVSB for DEBUG state=0x%x\n",MSB_CheckATSCStatus());
    return (OK);
}
/**
Check the Demod. lock status after demod stting.
If signal lock status is valid, set *pFishisd value as true.
*/
int KHAL_DEMOD_VQI_QAM_TunePostJob(BOOLEAN *pFinished)
{
    DMD_ATSC_GETLOCK_TYPE sDMD_ATSC_GETLOCK_TYPE;
    
    
    /* check TR lock */
    sDMD_ATSC_GETLOCK_TYPE = DMD_ATSC_GETLOCK;
    if(_MDrv_DMD_ATSC_MD_GetLock(0, sDMD_ATSC_GETLOCK_TYPE) == DMD_ATSC_CHECKING)
       *pFinished=FALSE;
    else
       *pFinished=TRUE;
    
    printk(KERN_DEBUG "\n\rJ83B for DEBUG state=0x%x\n",MSB_CheckATSCStatus());
    return (OK);
}
/**
Check the Demod. Lock state.
*/
int KHAL_DEMOD_VQI_VSB_CheckLock(KHAL_DEMOD_LOCK_STATE_T *pLockState)
{
    MSB_LOCK_STATE_T tunerLock = MSB_LOCK_UNKNOWN;

    //PRINT("[TU DTV] TU_ATSC_MSB_CheckLockState !!!\n");
    if (KHAL_DEMOD_TRANS_SYS_VSB == _gtransSystem)
    {
        tunerLock = MSB_CheckVsbLock();
    }

    /* LG Need modify*/
    _glockState = MSB_ConvertLockState(tunerLock);
    *pLockState = _glockState;
    printk(KERN_DEBUG "\n\r8VSB for DEBUG state=0x%x\n",MSB_CheckATSCStatus());
    return (OK);
}
/*
Check the Demod. Lock state.
*/
int KHAL_DEMOD_VQI_QAM_CheckLock(KHAL_DEMOD_LOCK_STATE_T *pLockState)
{
    MSB_LOCK_STATE_T tunerLock = MSB_LOCK_UNKNOWN;

   // PRINT("[TUDTV] TU_ATSC_MSB_CheckLockState !!!\n");
    if (KHAL_DEMOD_TRANS_SYS_QAM == _gtransSystem)
    {
        tunerLock = MSB_CheckQamLock();
    }

    _glockState = MSB_ConvertLockState(tunerLock);
    *pLockState = _glockState;
    printk(KERN_DEBUG "\n\rJ83B for DEBUG state=0x%x\n",MSB_CheckATSCStatus());
    return (OK);
}
/*
Check signal state with the specified parameters.
*/
int KHAL_DEMOD_VQI_QAM_CheckSpecialData(KHAL_DEMOD_SPECDATA_QAM_T *pSpecQAM)
{
    //Need, because DIL did@@
    DMD_ATSC_DEMOD_TYPE seType = DMD_ATSC_DEMOD_NULL;
    //PRINT("[TUDTV] KHAL_DEMOD_VQI_QAM_CheckSpecialData !!!\n");
    seType = _MDrv_DMD_ATSC_MD_GetModulationMode(0);
    if(seType == DMD_ATSC_DEMOD_ATSC_VSB)
        pSpecQAM->constellation = KHAL_DEMOD_TPS_CONST_VSB_8;
    else if (seType == DMD_ATSC_DEMOD_ATSC_256QAM)
        pSpecQAM->constellation = KHAL_DEMOD_TPS_CONST_QAM_256;
    else if (seType == DMD_ATSC_DEMOD_ATSC_64QAM)
        pSpecQAM->constellation = KHAL_DEMOD_TPS_CONST_QAM_64;
    
    return (OK);
}
/*
Check Specia Data with the specified parameters.
*/
int KHAL_DEMOD_VQI_CheckSignalStatus(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
   return MSB_GetSignalState(pSignalState);
}
/*
Gets frequency offset
*/
int KHAL_DEMOD_VQI_CheckFrequencyOffset(SINT32 *pFreqOffset)
{
    DMD_ATSC_CFO_DATA sDMD_ATSC_CFO_DATA;

    _MDrv_DMD_ATSC_MD_ReadFrequencyOffset(0, &sDMD_ATSC_CFO_DATA);

    *pFreqOffset = sDMD_ATSC_CFO_DATA.FF;

    return (OK);
}
int KHAL_DEMOD_VQI_ControlTSMode(BOOLEAN bIsSerial)
{
    return (OK);
}

int KHAL_DEMOD_VQI_ControlOutput(BOOLEAN bEnableOutput)
{
    if (bEnableOutput)
    {
        if (OK != MSB_OutputOn())
        {
            PRINT("^r^[TUDTV] FAIL () _TU_MSB_OutputOn() !!!\n");
            return (NOT_OK);
        }
    }
    else
    {
        if (OK != MSB_OutputOff())
        {
            PRINT( "^r^[TU DTV] FAIL () MSB_OutputOff() !!!\n");
            return (NOT_OK);
        }
    }
    return (OK);
}

int KHAL_DEMOD_VQI_DebugMenu(void)
{
    return (OK);
}
