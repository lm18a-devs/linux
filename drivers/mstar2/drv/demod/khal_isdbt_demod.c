/******************************************************************************
 *   DTV LABORATORY, LG ELECTRONICS INC., SEOUL, KOREA
 *   Copyright(c) 2008 by LG Electronics Inc.
 *
 *   All rights reserved. No part of this work may be reproduced, stored in a
 *   retrieval system, or transmitted by any means without prior written
 *   permission of LG Electronics Inc.
 *****************************************************************************/

/** @file demod_isdbt.c
 *
 *  Brief description.
 *  Detailed description starts here.
 * @author        K.D HONG(hszdch@lge.com)
 *  @author        Goldman(jhgold@lge.com)
 *  @version    0.5
 *  @date        2009.04.10
 *  @note        Additional information.
 *  @see        reference (file/function/URL/etc)
 */

/******************************************************************************
    Control Constants
******************************************************************************/

/******************************************************************************
    File Inclusions
******************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/time.h>
#include <linux/delay.h>

#include "mhal_demod_reg.h"
#include "mhal_demod.h"
#include "mdrv_types.h"
#include "drvDMD_ISDBT.h"
#include <asm/setup.h> //need follow sequence
#include "mdrv_system.h"
#include "khal_demod_vqi.h"
#include "khal_demod_common.h"
#include "mdrv_system_LG.h"
/******************************************************************************
    Macro Definitions
******************************************************************************/



/******************************************************************************
    Extern Variables & Function Prototype Declarations
******************************************************************************/

extern struct tag_custom customTAG;

/******************************************************************************
    Local Constant Definitions
******************************************************************************/

#define MMAP_INDEX_ISDBT_TDI    62
#define MMAP_INDEX_MEM_ADDR     1
#define MMAP_INDEX_MEM_LEN      3

/******************************************************************************
    Local Type Definitions
******************************************************************************/

typedef enum
{
    /// lock
    E_DEMOD_LOCK,
    /// is checking
    E_DEMOD_CHECKING,
    /// after checking
    E_DEMOD_CHECKEND,
    /// unlock
    E_DEMOD_UNLOCK,
    /// NULL state
    E_DEMOD_NULL,
} EN_LOCK_STATUS;

/******************************************************************************
    Global Type Definitions
******************************************************************************/



/******************************************************************************
    Static Variables & Function Prototypes Declarations
******************************************************************************/

static KHAL_DEMOD_LOCK_STATE_T MSB_ConvertLockState(EN_LOCK_STATUS tunerLock);
static EN_LOCK_STATUS MSB_CheckISDBTLock(void);
static MS_U32 MSB_Get_System_Time(void);
static void MSB_Delay_MS(MS_U32 delay_time);
static MS_BOOL MSB_Cretate_mutex(MS_BOOL on);
static void MSB_LockDMD(MS_BOOL LOCK);
static int MSB_Demod_ISDBT_Suspend(void);
static int MSB_Demod_ISDBT_Resume(void);

/******************************************************************************
    Global Variables & Function Prototypes Declarations
******************************************************************************/



/******************************************************************************
    Local Variables & Function Prototypes Declarations
******************************************************************************/



/******************************************************************************
    Function Definitions
******************************************************************************/

static KHAL_DEMOD_LOCK_STATE_T MSB_ConvertLockState(EN_LOCK_STATUS tunerLock)
{
    if (E_DEMOD_LOCK == tunerLock)
        return KHAL_DEMOD_LOCK_OK;
    else if (E_DEMOD_CHECKING == tunerLock)
        return KHAL_DEMOD_LOCK_FAIL;
    else if (E_DEMOD_UNLOCK == tunerLock)
        return KHAL_DEMOD_LOCK_FAIL;
    else
        return KHAL_DEMOD_LOCK_FAIL;
}

static EN_LOCK_STATUS MSB_CheckISDBTLock(void)
{
    BOOLEAN hasIsdbtSignal=FALSE, fecLock=FALSE;
    DMD_ISDBT_GETLOCK_TYPE sDMD_ISDBT_GETLOCK_TYPE;

    sDMD_ISDBT_GETLOCK_TYPE = DMD_ISDBT_GETLOCK_ICFO_CH_EXIST_LOCK;
    if (_MDrv_DMD_ISDBT_MD_GetLock(0, sDMD_ISDBT_GETLOCK_TYPE) == DMD_ISDBT_LOCK)
        hasIsdbtSignal = TRUE;
    sDMD_ISDBT_GETLOCK_TYPE = DMD_ISDBT_GETLOCK_FEC_LOCK;
    if (_MDrv_DMD_ISDBT_MD_GetLock(0, sDMD_ISDBT_GETLOCK_TYPE) == DMD_ISDBT_LOCK)
        fecLock = TRUE;

    if (fecLock==TRUE)
        return E_DEMOD_LOCK;

    if (hasIsdbtSignal==TRUE)
        return E_DEMOD_CHECKING;
    else return E_DEMOD_UNLOCK;
}

static U8 MSB_CheckISDBTStatus(void)
{
    U8 u8Data=0;
    _MDrv_DMD_ISDBT_MD_GetReg(0, 0x20C1, &u8Data);
    return u8Data;
}

static U32 MS_MSB_Get_10xSNR(void)
{
    DMD_ISDBT_SNR_DATA sDMD_ISDBT_SNR_DATA;
    
    _MDrv_DMD_ISDBT_MD_GetSNR(0, &sDMD_ISDBT_SNR_DATA);
    
    if (sDMD_ISDBT_SNR_DATA.RegSNR > 5011 * 9984)
        return 370;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 4466 * 9984)
        return 365;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 3981 * 9984)
        return 360;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 3548 * 9984)
        return 355;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 3162 * 9984)
        return 350;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 2818 * 9984)
        return 345;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 2511 * 9984)
        return 340;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 2238 * 9984)
        return 335;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 1995 * 9984)
        return 330;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 1778 * 9984)
        return 325;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 1584 * 9984)
        return 320;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 1412 * 9984)
        return 315;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 1258 * 9984)
        return 310;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 1122 * 9984)
        return 305;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 1000 * 9984)
        return 300;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 891 * 9984)
        return 295;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 794 * 9984)
        return 290;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 707 * 9984)
        return 285;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 630 * 9984)
        return 280;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 562 * 9984)
        return 275;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 501 * 9984)
        return 270;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 446 * 9984)
        return 265;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 398 * 9984)
        return 260;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 354 * 9984)
        return 255;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 316 * 9984)
        return 250;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 281 * 9984)
        return 245;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 251 * 9984)
        return 240;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 223 * 9984)
        return 235;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 199 * 9984)
        return 230;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 177 * 9984)
        return 225;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 158 * 9984)
        return 220;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 141 * 9984)
        return 215;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 125 * 9984)
        return 210;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 112 * 9984)
        return 205;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 100 * 9984)
        return 200;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 89 * 9984)
        return 195;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 79 * 9984)
        return 190;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 70 * 9984)
        return 185;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 63 * 9984)
        return 180;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 56 * 9984)
        return 175;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 50 * 9984)
        return 170;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 44 * 9984)
        return 165;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 39 * 9984)
        return 160;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 35 * 9984)
        return 155;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 31 * 9984)
        return 150;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 28 * 9984)
        return 145;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 25 * 9984)
        return 140;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 22 * 9984)
        return 135;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 19 * 9984)
        return 130;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 17 * 9984)
        return 125;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 15 * 9984)
        return 120;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 14 * 9984)
        return 115;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 12* 9984)
        return 110;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 11* 9984)
        return 105;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 10* 9984)
        return 100;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 9* 9984)
        return 95;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 8* 9984)
        return 90;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 7* 9984)
        return 85;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 6* 9984)
        return 80;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 5* 9984)
        return 75;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 4* 9984)
        return 65;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 3* 9984)
        return 55;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 2* 9984)
        return 35;
    else if (sDMD_ISDBT_SNR_DATA.RegSNR > 1* 9984)
        return 30;
    else return 20; 
}

static SINT32 MS_MSB_Get_CFO(void)
{
    DMD_ISDBT_CFO_DATA ISDBT_CFO_Value;
    _MDrv_DMD_ISDBT_MD_GetFreqOffset(0,&ISDBT_CFO_Value);
    ISDBT_CFO_Value.TdCfoRegValue = (ISDBT_CFO_Value.TdCfoRegValue * 8126980) / 17179869184;
    ISDBT_CFO_Value.FdCfoRegValue = (ISDBT_CFO_Value.FdCfoRegValue * 8126980) / 17179869184;
    if((ISDBT_CFO_Value.FFT_Mode & 0x30) == 0x0000) // 2k
        ISDBT_CFO_Value.IcfoRegValue = (ISDBT_CFO_Value.IcfoRegValue * 250000) / 63;
    else if((ISDBT_CFO_Value.FFT_Mode & 0x0030) == 0x0010) // 4k
        ISDBT_CFO_Value.IcfoRegValue = (ISDBT_CFO_Value.IcfoRegValue * 125000) / 63;
    else //if(FFT_Mode & 0x0030 == 0x0020) // 8k
        ISDBT_CFO_Value.IcfoRegValue = (ISDBT_CFO_Value.IcfoRegValue * 125000) /126;

    return ISDBT_CFO_Value.TdCfoRegValue + ISDBT_CFO_Value.FdCfoRegValue + ISDBT_CFO_Value.IcfoRegValue;
}

static MS_U32 MSB_Get_System_Time(void)
{
    static struct timeval curr;
    
    do_gettimeofday(&curr);

    return (curr.tv_sec*1000+curr.tv_usec/1000);
}

static void MSB_Delay_MS(MS_U32 delay_time)
{
    mdelay(delay_time);
}

static MS_BOOL MSB_Cretate_mutex(MS_BOOL on)
{
    return TRUE;
}

static void MSB_LockDMD(MS_BOOL LOCK)
{
    if (LOCK)
        MHal_Demod_Mutex_Lock();
    else MHal_Demod_Mutex_Unlock();
}
static int MSB_Demod_ISDBT_Suspend(void)
{
    return _MDrv_DMD_ISDBT_MD_SetPowerState(0,E_POWER_SUSPEND);
}
static int MSB_Demod_ISDBT_Resume(void)
{
    return _MDrv_DMD_ISDBT_MD_SetPowerState(0,E_POWER_RESUME);
}

UINT32 KHAL_DEMOD_VQI_ISDBT_Probe(struct i2c_client *client)
{
    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_GetFWVersion(UINT32 *pFWVersion)
{
    MS_U8 data1 = 0;
    MS_U8 data2 = 0;
    MS_U8 data3 = 0;
    
    _MDrv_DMD_ISDBT_MD_GetReg(0,0x20C4, &data1);
    _MDrv_DMD_ISDBT_MD_GetReg(0,0x20C5, &data2);
    _MDrv_DMD_ISDBT_MD_GetReg(0,0x20C6, &data3);
    PRINT("youda KHAL_DEMOD_VQI_ISDBT_GetFWVersion = %x.%x!!\n",data2,data3);
    *pFWVersion = data2 << 8 | data3;
    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_Initialize(void)
{
    
    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_ChangeTransSystem(KHAL_DEMOD_TRANS_SYSTEM_T transSystem)
{
    DMD_ISDBT_InitData sDMD_ISDBT_InitData;
    
    U32 phyaddr=0;
    U32 blksize=0;
    U32 u32MIUInterval;
    U8  udatatemp = 0x00;

    if(transSystem == KHAL_DEMOD_TRANS_SYS_ISDBT)
    {
        if(MHal_Demod_VDMcuHandler(DEMOD_STANDARD_ISDBT))
            PRINT("kavana change DEMOD_STANDARD_ISDBT !!!\n");

    
    memset(&sDMD_ISDBT_InitData, 0, sizeof(DMD_ISDBT_InitData));
    
    sDMD_ISDBT_InitData.u16ISDBTIcfoChExistCheckTime = 300;
    sDMD_ISDBT_InitData.u16ISDBTFECLockCheckTime     = 2000;
    
    // register init 
    sDMD_ISDBT_InitData.u8DMD_ISDBT_DSPRegInitExt    = NULL; // TODO use system variable type
    sDMD_ISDBT_InitData.u8DMD_ISDBT_DSPRegInitSize   = 0;
    sDMD_ISDBT_InitData.u8DMD_ISDBT_InitExt          = NULL;
    
    //By Tuners:
    sDMD_ISDBT_InitData.u16IF_KHZ                    = 5000;
    sDMD_ISDBT_InitData.bIQSwap                      = 0;
    sDMD_ISDBT_InitData.u16AgcReferenceValue         = 0x400;
    sDMD_ISDBT_InitData.bTunerGainInvert             = 0;
    
    //By IC:
    sDMD_ISDBT_InitData.bIsExtDemod                  = 0;
    
    u32MIUInterval = MS_MIU_INTERVAL;
    phyaddr = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_ADDR];
    blksize = customTAG.mmapger[MMAP_INDEX_ISDBT_TDI][MMAP_INDEX_MEM_LEN];
    
    if (blksize < 0x500000)
    {
        PRINT("Error TDI DRAM size!!![%d]\n", blksize);
        return (NOT_OK);
    }
    if (MHal_Demod_VDMcuHandler(DEMOD_STANDARD_ISDBT))
    	PRINT("Youda change DEMOD_STANDARD_ISDBT !!!\n");
    udatatemp = _MHal_R1B(0x112000);
    if (u32MIUInterval & phyaddr)
    {
        _MHal_W1B(0x112000,(udatatemp|0x03)); // miu1
        phyaddr &= ~u32MIUInterval;
    }
    else _MHal_W1B(0x112000,(udatatemp&~0x03)); // miu0
    
    PRINT("[TDI address]: %x\n", phyaddr);
    
    sDMD_ISDBT_InitData.u32TdiStartAddr = ((~u32MIUInterval & phyaddr)/ 16);

    sDMD_ISDBT_InitData.GetSystemTimeMS              = MSB_Get_System_Time;
    sDMD_ISDBT_InitData.DelayMS                      = MSB_Delay_MS;
    sDMD_ISDBT_InitData.CreateMutex                  = MSB_Cretate_mutex;
    sDMD_ISDBT_InitData.LockDMD                      = MSB_LockDMD;
    
         
    if (_MDrv_DMD_ISDBT_MD_Init(0, &sDMD_ISDBT_InitData, sizeof(sDMD_ISDBT_InitData)) != TRUE)
    {
        PRINT("^r^[TUDTV] FAIL () MDrv_DMD_ISDBT_MD_Init !!!\n");

        return(NOT_OK);
    }
    MSB_LockDMD(TRUE);    
    SetDemodSTRHandle(MSB_Demod_ISDBT_Suspend,MSB_Demod_ISDBT_Resume);    
    MSB_LockDMD(FALSE);
   }
   else
   {
        if (MHal_Demod_VDMcuHandler(DEMOD_STANDARD_UNKNOWN))
            PRINT("[Demod][ISDBT] change DEMOD_STANDARD_UNKNOWN !!!\n");
   }

   return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_SetDemod(KHAL_DEMOD_ISDBT_SET_PARAM_T paramStruct)
{
    DMD_ISDBT_DEMOD_TYPE sDMD_ISDBT_DEMOD_TYPE = DMD_ISDBT_DEMOD_6M;
    
    if (_MDrv_DMD_ISDBT_MD_AdvSetConfig(0, sDMD_ISDBT_DEMOD_TYPE, TRUE) == FALSE)
    {
        PRINT("^r^%s[TU DTV] FAIL () MDrv_DMD_ISDBT_MD_AdvSetConfig !!!\n", __FUNCTION__);
        
        return (NOT_OK);
    }
    
    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_TunePostJob(BOOLEAN *pFinished)
{
    DMD_ISDBT_GETLOCK_TYPE sDMD_ISDBT_GETLOCK_TYPE;

    sDMD_ISDBT_GETLOCK_TYPE = DMD_ISDBT_GETLOCK;
    if(_MDrv_DMD_ISDBT_MD_GetLock(0, sDMD_ISDBT_GETLOCK_TYPE) == DMD_ISDBT_CHECKING)
       *pFinished=FALSE;
    else
       *pFinished=TRUE;
    printk(KERN_DEBUG "\n\rISDBT for DEBUG state=0x%x\n",MSB_CheckISDBTStatus());
    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_CheckLock(KHAL_DEMOD_LOCK_STATE_T *pLockState)
{
    EN_LOCK_STATUS tunerLock = E_DEMOD_NULL;
    
    //PRINT("[DTV] KHAL_DEMOD_VQI_ISDBT_CheckLockState !!!\n");

    tunerLock = MSB_CheckISDBTLock();

    //if (E_DEMOD_LOCK != tunerLock)
      //  PRINT("[DTV] KHAL_DEMOD_ISDBT_CheckLockState !!! tunerLock = 0x%X\n", tunerLock);

    *pLockState = MSB_ConvertLockState(tunerLock);
    printk(KERN_DEBUG "\n\rISDBT for DEBUG state=0x%x\n",MSB_CheckISDBTStatus());
    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_CheckSpecialData(KHAL_DEMOD_SPECDATA_ISDBT_T *pSpecISDB)
{
    EN_ISDBT_Layer eLayerIndex= E_ISDBT_Layer_B;
    KHAL_DEMOD_SPECDATA_ISDBT_T specISDBT= {0};
    sISDBT_MODULATION_MODE sIsdbtModulationMode;
    
    /* CheckTuner Lock state */
    if (E_DEMOD_LOCK != MSB_CheckISDBTLock())
        return (NOT_OK);
    
    _MDrv_DMD_ISDBT_MD_GetModulationMode(0, eLayerIndex, &sIsdbtModulationMode);

    // Code Rate
    if(sIsdbtModulationMode.eIsdbtCodeRate==E_ISDBT_CODERATE_1_2)
        specISDBT.codeRate = KHAL_DEMOD_TPS_CODE_1_2;
    else if(sIsdbtModulationMode.eIsdbtCodeRate==E_ISDBT_CODERATE_2_3)
        specISDBT.codeRate = KHAL_DEMOD_TPS_CODE_2_3;
    else if(sIsdbtModulationMode.eIsdbtCodeRate==E_ISDBT_CODERATE_3_4)
        specISDBT.codeRate = KHAL_DEMOD_TPS_CODE_3_4;
    else if(sIsdbtModulationMode.eIsdbtCodeRate==E_ISDBT_CODERATE_5_6)
        specISDBT.codeRate = KHAL_DEMOD_TPS_CODE_5_6;
    else if(sIsdbtModulationMode.eIsdbtCodeRate==E_ISDBT_CODERATE_7_8)
        specISDBT.codeRate = KHAL_DEMOD_TPS_CODE_7_8;
    else specISDBT.codeRate = KHAL_DEMOD_TPS_CODE_UNKNOWN;

    // Guard Interval
    if(sIsdbtModulationMode.eIsdbtGI==E_ISDBT_GUARD_INTERVAL_1_4)
        specISDBT.guardInterval = KHAL_DEMOD_TPS_GUARD_1_4;
    else if(sIsdbtModulationMode.eIsdbtGI==E_ISDBT_GUARD_INTERVAL_1_8)
        specISDBT.guardInterval = KHAL_DEMOD_TPS_GUARD_1_8;
    else if(sIsdbtModulationMode.eIsdbtGI==E_ISDBT_GUARD_INTERVAL_1_16)
        specISDBT.guardInterval = KHAL_DEMOD_TPS_GUARD_1_16;
    else if(sIsdbtModulationMode.eIsdbtGI==E_ISDBT_GUARD_INTERVAL_1_32)
        specISDBT.guardInterval = KHAL_DEMOD_TPS_GUARD_1_32;
    else specISDBT.guardInterval = KHAL_DEMOD_TPS_GUARD_UNKNOWN;

    // Modulation
    if(sIsdbtModulationMode.eIsdbtConstellation==E_ISDBT_DQPSK)
        specISDBT.constellation = E_ISDBT_DQPSK;
    else if(sIsdbtModulationMode.eIsdbtConstellation==E_ISDBT_QPSK)
        specISDBT.constellation = KHAL_DEMOD_TPS_CONST_QPSK;
    else if(sIsdbtModulationMode.eIsdbtConstellation==E_ISDBT_16QAM)
        specISDBT.constellation = KHAL_DEMOD_TPS_CONST_QAM_16;
    else if(sIsdbtModulationMode.eIsdbtConstellation==E_ISDBT_64QAM)
        specISDBT.constellation = KHAL_DEMOD_TPS_CONST_QAM_64;
    else specISDBT.constellation = KHAL_DEMOD_TPS_GUARD_UNKNOWN;

    // Carrior mode
    if(sIsdbtModulationMode.eIsdbtFFT==E_ISDBT_FFT_2K)
        specISDBT.carrierMode = KHAL_DEMOD_TPS_CARR_2K;
    else if(sIsdbtModulationMode.eIsdbtFFT==E_ISDBT_FFT_4K)
        specISDBT.carrierMode = KHAL_DEMOD_TPS_CARR_4K;
    else if(sIsdbtModulationMode.eIsdbtFFT==E_ISDBT_FFT_8K)
        specISDBT.carrierMode = KHAL_DEMOD_TPS_CARR_8K;
    else specISDBT.carrierMode = KHAL_DEMOD_TPS_CARR_UNKNOWN;

    *pSpecISDB = specISDBT;

    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_CheckSignalStatus(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
    KHAL_DEMOD_SIGNAL_STATE_T signalState;
    U16 u16PktErrValue;
    BOOLEAN u8Status = TRUE;
    DMD_ISDBT_GET_BER_VALUE sDMD_ISDBT_GET_BER_VALUE;
    MS_U32 ISDBT_BER_DATA_LAYER_A;
    MS_U32 ISDBT_BER_DATA_LAYER_B;
    MS_U32 ISDBT_BER_DATA_LAYER_C;
    signalState.bSignalValid = FALSE;
    signalState.unSNR        = 0xFFFFFFFF;
    signalState.unBER        = 0xFFFFFFFF;
    signalState.packetError  = 0xFFFFFFFF;
    signalState.unAGC        = 0xFFFFFFFF;
    signalState.strength     = 0;
    signalState.quality      = 0;
    
    if(E_DEMOD_LOCK != MSB_CheckISDBTLock())
    {
        u8Status = FALSE;
    }
    else
    {
        

        // NG!!!!! CoreDump
        //ISDBT_BER_DATA = (sDMD_ISDBT_GET_BER_VALUE.BerValue) * 10000000;
        //ISDBT_BER_DATA = ISDBT_BER_DATA/(sDMD_ISDBT_GET_BER_VALUE.BerPeriod *192512);
        // NG!!!!! CoreDump
        sDMD_ISDBT_GET_BER_VALUE.eIsdbtLayer = E_ISDBT_Layer_A;
        _MDrv_DMD_ISDBT_MD_GetPostViterbiBer(0,&sDMD_ISDBT_GET_BER_VALUE);
        if(sDMD_ISDBT_GET_BER_VALUE.BerPeriod == 4) //QPSK  Case
        ISDBT_BER_DATA_LAYER_A = (sDMD_ISDBT_GET_BER_VALUE.BerValue)*8;
        else
        ISDBT_BER_DATA_LAYER_A = (sDMD_ISDBT_GET_BER_VALUE.BerValue);
    
        sDMD_ISDBT_GET_BER_VALUE.eIsdbtLayer = E_ISDBT_Layer_B;
        _MDrv_DMD_ISDBT_MD_GetPostViterbiBer(0,&sDMD_ISDBT_GET_BER_VALUE);
        if(sDMD_ISDBT_GET_BER_VALUE.BerPeriod == 4) //QPSK  Case
        ISDBT_BER_DATA_LAYER_B = (sDMD_ISDBT_GET_BER_VALUE.BerValue)*8;
        else
        ISDBT_BER_DATA_LAYER_B = (sDMD_ISDBT_GET_BER_VALUE.BerValue);
    
        sDMD_ISDBT_GET_BER_VALUE.eIsdbtLayer = E_ISDBT_Layer_C;
        _MDrv_DMD_ISDBT_MD_GetPostViterbiBer(0,&sDMD_ISDBT_GET_BER_VALUE);
        if(sDMD_ISDBT_GET_BER_VALUE.BerPeriod == 4) //QPSK  Case
        ISDBT_BER_DATA_LAYER_C = (sDMD_ISDBT_GET_BER_VALUE.BerValue)*8;
        else
        ISDBT_BER_DATA_LAYER_C = (sDMD_ISDBT_GET_BER_VALUE.BerValue);
    

        if( ISDBT_BER_DATA_LAYER_A!=1)
        {
            if(ISDBT_BER_DATA_LAYER_B!=1)
            {
                if(ISDBT_BER_DATA_LAYER_A > ISDBT_BER_DATA_LAYER_B)
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_A;
                else
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_B;   
            }
            else if(ISDBT_BER_DATA_LAYER_C!=1)
            {
                if(ISDBT_BER_DATA_LAYER_A > ISDBT_BER_DATA_LAYER_C)
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_A;
                else
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_C;  
            
            }
            else
            signalState.unBER           = (U32)ISDBT_BER_DATA_LAYER_A;
        }
        
        if( ISDBT_BER_DATA_LAYER_B!=1)
        {
            if(ISDBT_BER_DATA_LAYER_C!=1)
            {
                if(ISDBT_BER_DATA_LAYER_B > ISDBT_BER_DATA_LAYER_C)
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_B;
                else
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_C;   
            }
            else if (ISDBT_BER_DATA_LAYER_A!=1)
            {
                if(ISDBT_BER_DATA_LAYER_B > ISDBT_BER_DATA_LAYER_A)
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_B;
                else
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_A;  
            
            }
            else
            signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_B;
            
        }
        
        if( ISDBT_BER_DATA_LAYER_C!=1)
        {
            if(ISDBT_BER_DATA_LAYER_B!=1)
            {
                if(ISDBT_BER_DATA_LAYER_B > ISDBT_BER_DATA_LAYER_C)
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_B;
                else
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_C;   
            }
            else if(ISDBT_BER_DATA_LAYER_A!=1)
            {
                if(ISDBT_BER_DATA_LAYER_C > ISDBT_BER_DATA_LAYER_A)
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_C;
                else
                signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_A;  
            
            }
            else
            signalState.unBER        = (U32)ISDBT_BER_DATA_LAYER_C;
        }
        
        if(signalState.unBER==ISDBT_BER_DATA_LAYER_A)
        _MDrv_DMD_ISDBT_MD_Read_PKT_ERR(0, E_ISDBT_Layer_A, &u16PktErrValue);
        else if(signalState.unBER==ISDBT_BER_DATA_LAYER_B)
        _MDrv_DMD_ISDBT_MD_Read_PKT_ERR(0, E_ISDBT_Layer_B, &u16PktErrValue);
        else
        _MDrv_DMD_ISDBT_MD_Read_PKT_ERR(0, E_ISDBT_Layer_C, &u16PktErrValue);
    
        signalState.bSignalValid = TRUE;
        signalState.unSNR        = MS_MSB_Get_10xSNR()/10;
        signalState.packetError  = u16PktErrValue;
        signalState.unAGC        = 0;
        signalState.strength     = 0;
        //signalState.unBER        = (U32)ISDBT_BER_DATA;
        //printk("\n\r[ISDBT_BER_DATA_LAYER_A=%ld]",ISDBT_BER_DATA_LAYER_A);
        //printk("\n\r[ISDBT_BER_DATA_LAYER_B=%ld]",ISDBT_BER_DATA_LAYER_B);
        //printk("\n\r[ISDBT_BER_DATA_LAYER_C=%ld]",ISDBT_BER_DATA_LAYER_C);
        //printk("\n\r[signalState.unBER=%d]",signalState.unBER);
        //printk("\n\r[signalState.packetError=%d]",signalState.packetError);
        signalState.quality      = _MDrv_DMD_ISDBT_MD_GetSignalQualityCombine(0);
        //printk("\n\r ISDBT_BER_DATA=%llu",ISDBT_BER_DATA);
        //printk("\n\r [ISDBT signalState.unBER=%d]",signalState.unBER);
    }
    
    if (u8Status != TRUE)
    {
        signalState.bSignalValid = FALSE;
        signalState.unSNR        = 0xFFFFFFFF;
        signalState.unBER        = 0xFFFFFFFF;
        signalState.packetError  = 0xFFFFFFFF;
        signalState.unAGC        = 0xFFFFFFFF;
        signalState.strength     = 0;
        signalState.quality      = 0;
    }
    
    *pSignalState = signalState;

    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_CheckFrequencyOffset(SINT32 *pFreqOffset)
{
    *pFreqOffset = MS_MSB_Get_CFO();
    
    return (OK);
}

BOOLEAN KHAL_DEMOD_GetEmergencyAlertFlagStatus(void)
{
    BOOLEAN bRet = FALSE;
    U8 u8Data = 0;

    if ((_MDrv_DMD_ISDBT_MD_GetLock(0, DMD_ISDBT_GETLOCK_FEC_LOCK) != DMD_ISDBT_LOCK))
    {
        bRet = FALSE;
    }
    else
    {
        _MDrv_DMD_ISDBT_MD_GetReg(0, 0x1500 + 0x04*2, &u8Data);
        
        if (u8Data & 0x01)
            bRet = TRUE;
        else bRet = FALSE;
    }

    return (bRet);
}

int KHAL_DEMOD_VQI_ISDBT_Monitor (KHAL_DEMOD_LOCK_STATE_T *pLockState)
{
    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_ControlTSMode(BOOLEAN bIsSerial)
{
    return (OK);
}

int KHAL_DEMOD_VQI_ISDBT_ControlOutput(BOOLEAN bEnableOutput)
{
    return (OK);
}

int KHAL_DEMOD_ISDBT_DebugMenu (void)
{
    return OK;
}

