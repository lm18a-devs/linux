//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
/**********************************************************************
 Copyright (c) 2006-2010 MStar Semiconductor, Inc.
 All rights reserved.

 Unless otherwise stipulated in writing, any and all information contained
 herein regardless in any format shall remain the sole proprietary of
 MStar Semiconductor Inc. and be kept in strict confidence
 (MStar Confidential Information) by the recipient.
 Any unauthorized act including without limitation unauthorized disclosure,
 copying, use, reproduction, sale, distribution, modification, disassembling,
 reverse engineering and compiling of the contents of MStar Confidential
 Information is unlawful and strictly prohibited. MStar hereby reserves the
 rights to any and all damages, losses, costs and expenses resulting therefrom.

* Class :
* File  :
**********************************************************************/
/******************************************************************************
	File Inclusions
******************************************************************************/
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/jiffies.h>
#include "mhal_demod.h"
#include "mhal_demod_DVBTxC.h"
#include "khal_demod_common.h"
#include "MsFixpLib.h"
#include "mhal_demod_DVBS.h"
#include "mdrv_system_LG.h"

/******************************************************************************
	Macro Definitions
******************************************************************************/
#define DDI_DEMOD_LOG(format, args...) do \
{ \
    if (sg_fPrintDbg) \
    { \
        printk("[%s %d] "format, __FUNCTION__, __LINE__, ##args); \
    } \
} while (0)

#define DEMOD_MUTEX_LOCK() MHal_Demod_Mutex_Lock()
#define DEMOD_MUTEX_UNLOCK() MHal_Demod_Mutex_Unlock()

#define KHAL_DVB_TUNE_STATUS_UNKNOWN   0
#define KHAL_DVB_TUNE_STATUS_LOCK      1
#define KHAL_DVB_TUNE_STATUS_UNLOCK    2
#define KHAL_DVB_TUNE_STATUS_LOCK_HOLD 3
#define LOCK_HOLD_COUNT_MAX 5
#define LOCK_HOLD_TIMEOUT   2000

/* GBB DVB-C Const. */
#define GBB_DVB_DVBC_CONST_16QAM			0
#define GBB_DVB_DVBC_CONST_32QAM			1
#define GBB_DVB_DVBC_CONST_64QAM			2
#define GBB_DVB_DVBC_CONST_128QAM			3
#define GBB_DVB_DVBC_CONST_256QAM			4

// unit : need to be modify for SOC_DEMOD
#define	GBB_DVB_DVBT_LOCK_DELAY_DEFAULT		1500	// for AGC/SYM Lock
#define	GBB_DVB_DVBT_LOCK_DELAY_OFDM		5000	// for OFDM
#define	GBB_DVB_DVBT_LOCK_DELAY_FEC			3000	// for FEC
#define	GBB_DVB_DVBT_LOCK_DELAY_AUTO		1500	// for Auto Scan
#define GBB_DVB_DVBT_LOCK_DELAY_TPS			5000
#define GBB_DVB_DVBT_LOCK_DELAY_7MHZ		2000	// MSB1228 not support Never for 7MHz

/* T2 Lock Time	*/ //add update 0823
#define GBB_DVB_DVBT2_LOCK_DELAY_DEFAULT    3000
#define GBB_DVB_DVBT2_LOCK_DELAY_L1         2000
#define GBB_DVB_DVBT2_LOCK_DELAY_P1         1500	//700		//2000
#define GBB_DVB_DVBT2_LOCK_DELAY_FEC        3000

/* DVB-C Lock Time */
#define	GBB_DVB_DVBC_LOCK_DELAY_DEFAULT		2000		// Frame Lock Waiting Lock
#define	GBB_DVB_DVBC_LOCK_DELAY_CONST		800		//2000	// for AGC/SYM Lock
#define	GBB_DVB_DVBC_LOCK_DELAY_TR			5000	// for DVBC
#define	GBB_DVB_DVBC_LOCK_DELAY_AUTO_DETECT	500	// for AGC/SYM Lock
#define	GBB_DVB_DVBC_LOCK_DELAY_DEMOD		4000	// for DEMOC

/******************************************************************************
	Extern Variables & Function Prototype Declarations
******************************************************************************/

/******************************************************************************
	Local Constant Definitions
******************************************************************************/

/******************************************************************************
	Local Type Definitions
******************************************************************************/
typedef struct
{
	// common
	KHAL_DEMOD_TRANS_SYSTEM_T			transSystem;
	KHAL_DEMOD_TUNE_MODE_T				tuneMode;
	BOOLEAN							bSpectrumInv;
	// DVBT
	KHAL_DEMOD_CHANNEL_BW_T 			channelBW;
	BOOLEAN								bProfileHP;
	KHAL_DEMOD_TPS_HIERARCHY_T		hierarchy;
	KHAL_DEMOD_TPS_CARRIER_MODE_T	carrierMode;
	KHAL_DEMOD_TPS_GUARD_INTERVAL_T	guardInterval;
	KHAL_DEMOD_TPS_CODERATE_T		codeRate;

	// DVBC
	UINT32								DVBC_tunedFreq;
	UINT16 								symbolRate;
	KHAL_DEMOD_TPS_CONSTELLATION_T		constellation;
} KHAL_DEMOD_DVB_INFORMATION_T;

typedef enum KHAL_DEMOD_DVB_TU_FLAG
{
	KHAL_DEMOD_DVB_TU_FLAG_DVBC_AUTO_MODE = 0x10,
	KHAL_DEMOD_DVB_TU_FLAG_DVBC_MANUAL_MODE = 0x20,

	KHAL_DEMOD_DVB_TU_FLAG_START		= 0x90,
	KHAL_DEMOD_DVB_TU_FLAG_ING,
	KHAL_DEMOD_DVB_TU_FLAG_STOP,
	KHAL_DEMOD_DVB_TU_FLAG_FINISH,

//	TU_FLAG_UNKNOWN 	= 0xF0
} KHAL_DEMOD_DVB_TU_FLAG_T;

typedef enum
{
	// =============================================== //
	// ------- these locks are treated as 'locked' when scan mode --------- //
	SOC_DEMOD_LOCK_OK			= 0x00,	// 0000 XXXX	// keep the position and value !!!

	SOC_DEMOD_LOCK_FEC_LOCK	= 0x10,		// DVB-T
	SOC_DEMOD_LOCK_SYNC_LOCK,				// DVB-T
	SOC_DEMOD_LOCK_TPS_LOCK,				// DVB-T
	SOC_DEMOD_LOCK_OFDM_LOCK,				// DVB-T
	SOC_DEMOD_LOCK_DEMOD_LOCK,			// DVB-C
	SOC_DEMOD_LOCK_TR_LOCK,				// DVB-C
	SOC_DEMOD_LOCK_P1_LOCK,				//DVB-T2
	// =============================================== //

	// =============================================== //
	// ------- these locks are treated as 'not locked' when scan mode ------- //
	SOC_DEMOD_LOCK_NOT_LOCKED	= 0x20,		// keep the position and value !!!
	SOC_DEMOD_LOCK_UNLOCK,
	SOC_DEMOD_LOCK_NEVER_LOCK,
	SOC_DEMOD_LOCK_NO_CH,
	SOC_DEMOD_LOCK_FOUND_DVBT2,
	// =============================================== //

	SOC_DEMOD_LOCK_UNKNOWN		= 0x80,	// 0010 XXXX	// keep the position and value !!!
	SOC_DEMOD_LOCK_OTHERS,				// 0010 XXXX	// keep the position and value
	//SOC_DEMOD_LOCK_NEVER_LOCK,

	SOC_DEMOD_LOCK_MASK			= 0xF0
} SOC_DEMOD_LOCK_STATE_T;

typedef enum KHAL_DEMOD_DVB_STR_STATE
{
	KHAL_DEMOD_DVB_STR_AWAKE = 0,
	KHAL_DEMOD_DVB_STR_SUSPEND_ING,
	KHAL_DEMOD_DVB_STR_SLEEP,
	KHAL_DEMOD_DVB_STR_RESUME_ING,
	KHAL_DEMOD_DVB_STR_STATE_NUM
} KHAL_DEMOD_DVB_STR_STATE_T;

/******************************************************************************
	Global Type Definitions
******************************************************************************/

/******************************************************************************
	Static Variables & Function Prototypes Declarations
******************************************************************************/
static BOOLEAN sg_fPrintDbg = FALSE;
static E_SYSTEM _gDVB_CurrenType, _gDVB_ResumeType;
static KHAL_DEMOD_DVB_INFORMATION_T _gDVB_INFORMATION;
static KHAL_DEMOD_DVB_TU_FLAG_T	_gSOC_DEMOD_FlagPostJob			= 0;	// START, ING, FINISH, COMPLETE
static UINT32 _gSOC_DEMOD_Lock_postJob		= KHAL_DVB_TUNE_STATUS_UNKNOWN;
static UINT8 _sgDVBT2_PLP;
static UINT32 CURRENT_SYSTIME; // ms
static UINT32 gDVBLockHoldCount = 0;
static UINT32 gDVBLockDelayStartSysTime;
static DMD_DVBS_Info_T sDMD_DVBS_Info;
static BOOLEAN M2R_DEBUG_ON = 0;
static BOOLEAN FFT_CAPTURE_EN = 0;
static BOOLEAN M2R_DEBUG_TS_CLK_ON = 0;
//static BOOLEAN M2R_DEBUG_CFO_ON = 0;
static UINT32 TS_CLK_Val = 0;
#if 0 // disable unused DebugMenu for pass coverity
static UINT32 pkterr_monitor_period = 1000;
static BOOLEAN M2R_DEBUG_CFO_ON = 0;
#endif
static KHAL_DEMOD_DVB_STR_STATE_T _gDVB_STRState;
/******************************************************************************
	Global Variables & Function Prototypes Declarations
******************************************************************************/


/******************************************************************************
	Local Variables & Function Prototypes Declarations
******************************************************************************/
static int _KHAL_DEMOD_DVB_Suspend(void);
static int _KHAL_DEMOD_DVB_Resume(void);

/******************************************************************************
	Function Definitions
******************************************************************************/


/**=========================================================================**/
/**			F U N C T I O N S												**/
/**=========================================================================**/
/* 4.0
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_Probe(UINT8 portI2C)
{
    DDI_DEMOD_LOG("jiffies=%d\n", jiffies_to_msecs(jiffies));

    DDI_DEMOD_LOG("sizeof(MS_BOOL)=%d\n", sizeof(MS_BOOL));
    DDI_DEMOD_LOG("sizeof(MS_S8)=%d\n", sizeof(MS_S8));
    DDI_DEMOD_LOG("sizeof(MS_U8)=%d\n", sizeof(MS_U8));
    DDI_DEMOD_LOG("sizeof(MS_S16)=%d\n", sizeof(MS_S16));
    DDI_DEMOD_LOG("sizeof(MS_U16)=%d\n", sizeof(MS_U16));
    DDI_DEMOD_LOG("sizeof(MS_S32)=%d\n", sizeof(MS_S32));
    DDI_DEMOD_LOG("sizeof(MS_S64)=%d\n", sizeof(MS_S64));
    DDI_DEMOD_LOG("sizeof(MS_U64)=%d\n", sizeof(MS_U64));
    DDI_DEMOD_LOG("sizeof(MS_FLOAT_ST)=%d\n", sizeof(MS_FLOAT_ST));
    DDI_DEMOD_LOG("int_sqrt(123456)=%lu\n", int_sqrt(123456));

    return OK;
}

/* 4.1
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_Initialize(void)
{
	DDI_DEMOD_LOG("Check\n");

    DEMOD_MUTEX_LOCK();
    _gDVB_CurrenType = E_SYS_UNKOWN;
    _gDVB_ResumeType = E_SYS_UNKOWN;
    _gDVB_STRState = KHAL_DEMOD_DVB_STR_AWAKE;
    SetDemodSTRHandle(_KHAL_DEMOD_DVB_Suspend, _KHAL_DEMOD_DVB_Resume);
    DEMOD_MUTEX_UNLOCK();
    return OK;
}

/* 
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
static E_TUNER_TYPE _KHAL_Demod_DVB_EU_AU_TW_ConvConst(KHAL_DEMOD_CHANNEL_BW_T channelBW)
{
	E_TUNER_TYPE MODEL_SEL = 0;

	if(KHAL_DEMOD_CH_BW_8M == channelBW)
	{
		MODEL_SEL = E_TUNER_SI2178;

	}
	else if(KHAL_DEMOD_CH_BW_7M == channelBW)
	{
		MODEL_SEL = E_TUNER_SI2178;

	}
	else if(KHAL_DEMOD_CH_BW_6M == channelBW)
	{
		MODEL_SEL = E_TUNER_SI2178;

	}
	else
	{
		MODEL_SEL = E_TUNER_SI2178;
	}

	return MODEL_SEL;
}

/* 
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
static int _KHAL_Demod_DVB_LoadDSPCode(E_SYSTEM eSystems)
{
	U16			msbVersion = 0xffff;
	E_TUNER_TYPE		tuner_type;

    if(eSystems == E_SYS_DVBS)
    {
#if 1 // TODO DVBS download
        DVBS_InitClkgen();
        mdelay(50);

        if (TRUE != DVBS_Download())
        {
            printk( " DVB-S DSP Load code Fail!!\n");
            return NOT_OK;
        }
        mdelay(50);

        DVBS_System_Init();
        if(DVBS_Get_Version(&msbVersion) != TRUE)
        {
            printk("[DTV] ERROR MSB1228 !!!\n");
            return NOT_OK;
        }
#endif
    }
	else
    {
        MHal_DVBTxC_InitClkgen(eSystems, TS_PARALLEL);

        if (TRUE != MHal_DVBTxC_Download(eSystems))
        {
            printk( "[%s] DVB DSP Load code Fail!!\n",__FUNCTION__);
            return NOT_OK;
        }

        tuner_type = _KHAL_Demod_DVB_EU_AU_TW_ConvConst(_gDVB_INFORMATION.channelBW);
        if (MHal_DVBTxC_System_Init(eSystems,tuner_type) != TRUE)
        {
            printk("[%s] System_Init Fail!!\n", __FUNCTION__);
            return NOT_OK;
        }

        if(MHal_DVBTxC_Get_Version(&msbVersion) != TRUE)
        {
            printk("[DTV] ERROR SOC_DEMOD !!!\n");
            return NOT_OK;
        }
    }

	DDI_DEMOD_LOG("[DTV] FOUND SOC_DEMOD (u-code Ver. : 0x%4x) !!!\n", msbVersion);
	return OK;
	
}

/* 4.2
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_ChangeTransMedia(KHAL_DEMOD_TRANS_SYSTEM_T transSystem)
{

    DDI_DEMOD_LOG("transSystem=%d,_gDVB_CurrenType=%d\n", transSystem, _gDVB_CurrenType);
    DEMOD_MUTEX_LOCK();

#if 1 // TODO: DVBS power saving
    if ((_gDVB_CurrenType==E_SYS_DVBS)&&(transSystem!=KHAL_DEMOD_TRANS_SYS_DVBS)&&(transSystem!=KHAL_DEMOD_TRANS_SYS_DVBS2))
        Power_Saving();
#endif

    switch(transSystem)
    {
        case KHAL_DEMOD_TRANS_SYS_DVBT:
#ifdef DEMOD_DVB_T2_MERGE_T
            if (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
            {
                _gDVB_ResumeType = E_SYS_DVBT_T2;
				DEMOD_MUTEX_UNLOCK();
				DDI_DEMOD_LOG("STR not awake _gDVB_ResumeType=%d\n", _gDVB_ResumeType);
				return OK;
            }

            if (_gDVB_CurrenType == E_SYS_DVBT_T2)
            {
				DEMOD_MUTEX_UNLOCK();
				DDI_DEMOD_LOG("_gDVB_CurrenType already %d -> skip\n", _gDVB_CurrenType);
				return OK;
            }

            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT2)) // SYSTEM_TYPE_DVBT2 maybe T or T2
			{
				DEMOD_MUTEX_UNLOCK();
				return NOT_OK;
			}

            if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT_T2))
			{
                printk("[%s] DVB-T T2 DSP Load Fail!!\n",  __FUNCTION__);
                _gDVB_CurrenType = E_SYS_UNKOWN;
				DEMOD_MUTEX_UNLOCK();
				return NOT_OK;
			}
            _gDVB_CurrenType = E_SYS_DVBT_T2;
#else
            if (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
            {
                _gDVB_ResumeType = E_SYS_DVBT;
				DEMOD_MUTEX_UNLOCK();
				return OK;
            }

            if (_gDVB_CurrenType == E_SYS_DVBT)
            {
				DEMOD_MUTEX_UNLOCK();
				return OK;
            }

            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT))
            {
                DEMOD_MUTEX_UNLOCK();
                return NOT_OK;
            }

            if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT))
            {
                printk("[%s] DVB-T DSP Load Fail!!\n",  __FUNCTION__);
                _gDVB_CurrenType = E_SYS_UNKOWN;
                DEMOD_MUTEX_UNLOCK();	//mail box crash protection
                return NOT_OK;
            }
            _gDVB_CurrenType = E_SYS_DVBT;
#endif
            break;

        case KHAL_DEMOD_TRANS_SYS_DVBC:
            SetDemodSTRHandle(_KHAL_DEMOD_DVB_Suspend, _KHAL_DEMOD_DVB_Resume); // in case DTMB -> DVBC

            if (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
            {
                _gDVB_ResumeType = E_SYS_DVBC;
				DEMOD_MUTEX_UNLOCK();
				DDI_DEMOD_LOG("STR not awake _gDVB_ResumeType=%d\n", _gDVB_ResumeType);
				return OK;
            }

            if ((_gDVB_CurrenType == E_SYS_DVBC) && (DEMOD_STANDARD_DVBC == MHal_Demod_VDMcuGetType()))
            {
				DEMOD_MUTEX_UNLOCK();
				DDI_DEMOD_LOG("_gDVB_CurrenType already %d -> skip\n", _gDVB_CurrenType);
				return OK;
            }

            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBC))
            {
                DEMOD_MUTEX_UNLOCK();//mail box crash
                return NOT_OK;
            }
            if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBC))
            {
                printk("[%s] DVB-C DSP Load Fail!!\n", __FUNCTION__);
                _gDVB_CurrenType = E_SYS_UNKOWN;
                DEMOD_MUTEX_UNLOCK();//mail box crash protection
                return NOT_OK;
            }
            _gDVB_CurrenType = E_SYS_DVBC;
            break;

        case KHAL_DEMOD_TRANS_SYS_DVBT2:
#ifdef DEMOD_DVB_T2_MERGE_T
            if (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
            {
                _gDVB_ResumeType = E_SYS_DVBT_T2;
				DEMOD_MUTEX_UNLOCK();
				DDI_DEMOD_LOG("STR not awake _gDVB_ResumeType=%d\n", _gDVB_ResumeType);
				return OK;
            }

            if (_gDVB_CurrenType == E_SYS_DVBT_T2)
            {
				DEMOD_MUTEX_UNLOCK();
				DDI_DEMOD_LOG("_gDVB_CurrenType already %d -> skip\n", _gDVB_CurrenType);
				return OK;
            }

            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT2))
            {
                DEMOD_MUTEX_UNLOCK();
                return NOT_OK;
            }

            if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT_T2))
            {
                printk("[%s] DVB-T_T2 DSP Load Fail!!\n",  __FUNCTION__);
                _gDVB_CurrenType = E_SYS_UNKOWN;
                DEMOD_MUTEX_UNLOCK();
                return NOT_OK;
            }
            _gDVB_CurrenType = E_SYS_DVBT_T2;
#else
            if (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
            {
                _gDVB_ResumeType = E_SYS_DVBT2;
				DEMOD_MUTEX_UNLOCK();
				return OK;
            }

            if (_gDVB_CurrenType == E_SYS_DVBT2)
            {
				DEMOD_MUTEX_UNLOCK();
				return OK;
            }

            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT2))
            {
                DEMOD_MUTEX_UNLOCK();
                return NOT_OK;
            }

            if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT2))
            {
                printk("[%s] DVB-T2 DSP Load Fail!!\n",  __FUNCTION__);
                _gDVB_CurrenType = E_SYS_UNKOWN;
                DEMOD_MUTEX_UNLOCK();   //mail box crash protection
                return NOT_OK;
            }
            _gDVB_CurrenType = E_SYS_DVBT2;
#endif
            break;

        case KHAL_DEMOD_TRANS_SYS_DVBS:
        case KHAL_DEMOD_TRANS_SYS_DVBS2:		
            if (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
            {
                _gDVB_ResumeType = E_SYS_DVBS;
				DEMOD_MUTEX_UNLOCK();
				return OK;
            }

            if (_gDVB_CurrenType == E_SYS_DVBS)
            {
				DEMOD_MUTEX_UNLOCK();
				return OK;
            }

            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBS))
            {
                DEMOD_MUTEX_UNLOCK();
                return NOT_OK;
            }
            if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBS))
            {
                printk("[%s] DVB-S DSP Load Fail!!\n",  __FUNCTION__);
                _gDVB_CurrenType = E_SYS_UNKOWN;
                DEMOD_MUTEX_UNLOCK();   //mail box crash protection
                return NOT_OK;
            }
            _gDVB_CurrenType = E_SYS_DVBS;
            break;

        default:
            if (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
            {
                _gDVB_ResumeType = E_SYS_UNKOWN;
				DEMOD_MUTEX_UNLOCK();
				DDI_DEMOD_LOG("STR not awake _gDVB_ResumeType=%d\n", _gDVB_ResumeType);
				return OK;
            }

            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_UNKNOWN))
            {
                DEMOD_MUTEX_UNLOCK();
                return NOT_OK;
            }
            _gDVB_CurrenType = E_SYS_UNKOWN;
            printk("[%s] NOT DVBT and Not DVBC\n", __FUNCTION__);
            break;
    }

	DEMOD_MUTEX_UNLOCK();//mail box crash protection
	return OK;
}

/* 4.3-1 DVBT
* Description: Set Demod. with the specified parameters.
* Syntax: TU_RETURN_VALUE_T HAL_DEMOD_XXX_SetDemode(HAL_DEMOD_XXX_SET_PARAM_T paramStruct)
* Parameters paramStruct [in] parameter structure for tuning
* Return Value If this function succeeds, the return value is OK.If this function fails, the return value is NOT_OK.
*/
int KHAL_DEMOD_DVB_DVBT_SetDemod(KHAL_DEMOD_DVBT_SET_PARAM_T paramStruct)
{

	UINT16 						status = OK;
#ifdef DEMOD_DVB_T2_MERGE_T
    DMD_DVBT2_RF_CHANNEL_BANDWIDTH BWSoc;

#else
	BOOLEAN						bIsAutoMode;
	UINT16						channelBW;
#endif

    DEMOD_MUTEX_LOCK();

    while (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
    {
    	DDI_DEMOD_LOG("wait STR awake\n");
        DEMOD_MUTEX_UNLOCK();
        msleep(10);
        DEMOD_MUTEX_LOCK();
    }

	_gDVB_INFORMATION.tuneMode = paramStruct.tuneMode;
	_gDVB_INFORMATION.transSystem = KHAL_DEMOD_TRANS_SYS_DVBT;
	_gDVB_INFORMATION.channelBW = paramStruct.eChannelBW;
	_gDVB_INFORMATION.bSpectrumInv = paramStruct.bSpectrumInv;
	_gDVB_INFORMATION.bProfileHP = paramStruct.bProfileHP;
	_gDVB_INFORMATION.hierarchy= paramStruct.hierarchy;
	_gDVB_INFORMATION.carrierMode= paramStruct.carrierMode;
	_gDVB_INFORMATION.guardInterval= paramStruct.guardInterval;
	_gDVB_INFORMATION.codeRate= paramStruct.codeRate;
	_gDVB_INFORMATION.constellation= paramStruct.constellation;

    DDI_DEMOD_LOG("tuneMode=%d,_gDVB_CurrenType=%d,eChannelBW=%d\n", _gDVB_INFORMATION.tuneMode, _gDVB_CurrenType, paramStruct.eChannelBW);

#ifdef DEMOD_DVB_T2_MERGE_T
    if (_gDVB_CurrenType != E_SYS_DVBT_T2)
    {
         printk( "[Error][%s]Wrong DVB type %d!!!\n", __FUNCTION__, _gDVB_CurrenType);
         DEMOD_MUTEX_UNLOCK();
         return NOT_OK;
    }

    switch(paramStruct.eChannelBW)
    {
        case KHAL_DEMOD_CH_BW_7M:
            BWSoc = E_DMD_T2_RF_BAND_7MHz;
            break;
        case KHAL_DEMOD_CH_BW_6M:
            BWSoc = E_DMD_T2_RF_BAND_6MHz;
            break;
        case KHAL_DEMOD_CH_BW_8M:
        default:
            BWSoc = E_DMD_T2_RF_BAND_8MHz;
            break;
    }

    if(_gDVB_INFORMATION.tuneMode == KHAL_DEMOD_TUNE_NORMAL)
    {    
        status &= MHal_DVBTxC_Config_DVBT2(BWSoc, 5000, 0xff, 1, !paramStruct.bProfileHP);
    }
    else
    {
        status &= MHal_DVBTxC_Config_DVBT2(BWSoc, 5000, 0xff, 0, !paramStruct.bProfileHP);
    }
    status &= MHal_DVBTxC_Active(1);
#else
    if (_gDVB_CurrenType != E_SYS_DVBT)
    {
		if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT))
		{
			DEMOD_MUTEX_UNLOCK();
			return NOT_OK;
		}
		if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT))
		{
			printk("[%s] DVB-T DSP Load Fail!!\n",  __FUNCTION__);
            _gDVB_CurrenType = E_SYS_UNKOWN;
			DEMOD_MUTEX_UNLOCK();
			return NOT_OK;
		}
        _gDVB_CurrenType = E_SYS_DVBT;
    }

	bIsAutoMode = (KHAL_DEMOD_TUNE_MANUAL == (paramStruct.tuneMode))?(0):(1);

	switch((paramStruct.eChannelBW))
	{
		case KHAL_DEMOD_CH_BW_8M:
			channelBW = 8000;
		break;

		case KHAL_DEMOD_CH_BW_7M:
			channelBW = 7000;
		break;

		case KHAL_DEMOD_CH_BW_6M:
			channelBW = 6000;
		break;

		case KHAL_DEMOD_CH_BW_UNKNOWN:
		default:
			channelBW = 0;
			printk( "[SOC DEMOD]  HAL_DEMOD_CH_BW_UNKNOWN [%s]!!!\n", __FUNCTION__);
		break;
	}

		 printk("[SOC DEMOD] [%s] DVB-T BW(%d),\n", __FUNCTION__, paramStruct.eChannelBW);

	if(paramStruct.bProfileHP)
	{
		printk("[SOC DEMOD] TS HP !!! [%s]\n", __FUNCTION__);
        status &= MHal_DVBTxC_Config_DVBT(channelBW, 0, 0, bIsAutoMode, 1, 0);
    }
    else
    {
        printk("[SOC DEMOD] TS LP !!! [%s]\n",  __FUNCTION__);
        status &= MHal_DVBTxC_Config_DVBT(channelBW, 1, 0, bIsAutoMode, 1, 0);
    }

    status &= MHal_DVBTxC_Active(1);
#endif

	DDI_DEMOD_LOG("[SOC DEMOD] TU_DVB_SOC_Tune: SUCCESS - DVB-T\n");

#if 0 // remove unknown delay
	if(KHAL_DEMOD_TUNE_SCAN == (paramStruct.tuneMode))
	{
		// TU_DELAY_MS(100); need fix
		usleep(100000);	//100 msec = 100*1000 usec = 100000 usec
	}
#endif

	_gSOC_DEMOD_FlagPostJob		= KHAL_DEMOD_DVB_TU_FLAG_START;
	_gSOC_DEMOD_Lock_postJob	= KHAL_DVB_TUNE_STATUS_UNKNOWN;

    DEMOD_MUTEX_UNLOCK();
	return OK;
}

int KHAL_DEMOD_DVB_DVBT2_SetDemod(KHAL_DEMOD_DVBT2_SET_PARAM_T paramStruct)
{
    int status = OK;
    DMD_DVBT2_RF_CHANNEL_BANDWIDTH BWSoc;

    DEMOD_MUTEX_LOCK();

    while (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
    {
    	DDI_DEMOD_LOG("wait STR awake\n");
        DEMOD_MUTEX_UNLOCK();
        msleep(10);
        DEMOD_MUTEX_LOCK();
    }

    _gDVB_INFORMATION.tuneMode = paramStruct.tuneMode;
    _gDVB_INFORMATION.transSystem = KHAL_DEMOD_TRANS_SYS_DVBT2;
    _gDVB_INFORMATION.channelBW = paramStruct.eChannelBW;
    _gDVB_INFORMATION.bSpectrumInv = paramStruct.bSpectrumInv;

    _gDVB_INFORMATION.carrierMode= paramStruct.carrierMode;
    _gDVB_INFORMATION.guardInterval= paramStruct.guardInterval;
    _gDVB_INFORMATION.codeRate= paramStruct.codeRate;
    _gDVB_INFORMATION.constellation= paramStruct.constellation;

    _sgDVBT2_PLP = paramStruct.unPLP;

    DDI_DEMOD_LOG("_gDVB_CurrenType=%d, channelBW=%d, tuneMode=%d, unPLP=%d\n", _gDVB_CurrenType, _gDVB_INFORMATION.channelBW, _gDVB_INFORMATION.tuneMode, paramStruct.unPLP);

#ifdef DEMOD_DVB_T2_MERGE_T
    if (_gDVB_CurrenType != E_SYS_DVBT_T2)
    {
        printk( "[Error][%s]Wrong DVB type %d!!!\n", __FUNCTION__, _gDVB_CurrenType);
        DEMOD_MUTEX_UNLOCK();
        return NOT_OK;
    }
#else
    if (_gDVB_CurrenType != E_SYS_DVBT2)
    {
		if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT2))
		{
			DEMOD_MUTEX_UNLOCK();
			return NOT_OK;
		}
		if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT2))
		{
			printk("[%s] DVB-T2 DSP Load Fail!!\n",  __FUNCTION__);
            _gDVB_CurrenType = E_SYS_UNKOWN;
			DEMOD_MUTEX_UNLOCK();
			return NOT_OK;
		}
        _gDVB_CurrenType = E_SYS_DVBT2;
    }
#endif

    switch(_gDVB_INFORMATION.channelBW)
    {
        case KHAL_DEMOD_CH_BW_7M:
            BWSoc = E_DMD_T2_RF_BAND_7MHz;
            break;
        case KHAL_DEMOD_CH_BW_6M:
            BWSoc = E_DMD_T2_RF_BAND_6MHz;
            break;
        case KHAL_DEMOD_CH_BW_8M:
        default:
            BWSoc = E_DMD_T2_RF_BAND_8MHz;
            break;
    }

    if(_gDVB_INFORMATION.tuneMode == KHAL_DEMOD_TUNE_NORMAL)
    {    
#ifdef DEMOD_DVB_T2_MERGE_T
        status &= MHal_DVBTxC_Config_DVBT2(BWSoc, 5000, paramStruct.unPLP, 2, !_gDVB_INFORMATION.bProfileHP);
#else
        status &= MHal_DVBTxC_Config_DVBT2(BWSoc, 5000, paramStruct.unPLP, 1);
#endif
    }
    else
    {
#ifdef DEMOD_DVB_T2_MERGE_T
        status &= MHal_DVBTxC_Config_DVBT2(BWSoc, 5000, paramStruct.unPLP, 0, !_gDVB_INFORMATION.bProfileHP);
#else
        status &= MHal_DVBTxC_Config_DVBT2(BWSoc, 5000, paramStruct.unPLP, 0);
#endif
    }
    status &= MHal_DVBTxC_Active(1);

     _gSOC_DEMOD_FlagPostJob    = KHAL_DEMOD_DVB_TU_FLAG_START;
     _gSOC_DEMOD_Lock_postJob   = KHAL_DVB_TUNE_STATUS_UNKNOWN;

    DEMOD_MUTEX_UNLOCK();
    return status;
}

/**
 * check lock state.
 *
 * @param	constMode [in] 16/64/128/256
 * @return	OK/FAIL
 * @author
*/
static UINT8 _KHAL_Demod_DVB_DVBC_ConvConst(KHAL_DEMOD_TPS_CONSTELLATION_T constMode)
{
	UINT8 constellation = 0;

	if(KHAL_DEMOD_TPS_CONST_QAM_256 == constMode)
	{
		constellation = GBB_DVB_DVBC_CONST_256QAM;
		DDI_DEMOD_LOG( "COST SCAN:TU_TPS_CONST_QAM_256 !!!\n");
	}
	else if(KHAL_DEMOD_TPS_CONST_QAM_128 == constMode)
	{
		constellation = GBB_DVB_DVBC_CONST_128QAM;
		DDI_DEMOD_LOG( "COST SCAN:TU_TPS_CONST_QAM_128 !!!\n");
	}
	else if(KHAL_DEMOD_TPS_CONST_QAM_64 == constMode)
	{
		constellation = GBB_DVB_DVBC_CONST_64QAM;
		DDI_DEMOD_LOG( "COST SCAN:TU_TPS_CONST_QAM_64 !!!\n");
	}
	else if(KHAL_DEMOD_TPS_CONST_QAM_32 == constMode)
	{
		constellation = GBB_DVB_DVBC_CONST_32QAM;
		DDI_DEMOD_LOG( "COST SCAN:TU_TPS_CONST_QAM_32 !!!\n");
	}
	else if(KHAL_DEMOD_TPS_CONST_QAM_16 == constMode)
	{
		constellation = GBB_DVB_DVBC_CONST_16QAM;
		DDI_DEMOD_LOG( "COST SCAN:TU_TPS_CONST_QAM_16 !!!\n");
	}
	else
	{
		constellation = GBB_DVB_DVBC_CONST_256QAM;
    	DDI_DEMOD_LOG( "XXXXXXX else COST SCAN:TU_TPS_CONST_QAM_256 !!!\n");
	}

	return constellation;
}

/* 4.3-2 DVBC
* Description: Set Demod. with the specified parameters.
* Syntax: TU_RETURN_VALUE_T HAL_DEMOD_XXX_SetDemode(HAL_DEMOD_XXX_SET_PARAM_T paramStruct)
* Parameters paramStruct [in] parameter structure for tuning
* Return Value If this function succeeds, the return value is OK.If this function fails, the return value is NOT_OK.
*/
int KHAL_DEMOD_DVB_DVBC_SetDemod(KHAL_DEMOD_DVBC_SET_PARAM_T paramStruct)
{
	UINT8	constellation;
	UINT16	status = TRUE;

    DEMOD_MUTEX_LOCK();

    while (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
    {
    	DDI_DEMOD_LOG("wait STR awake\n");
        DEMOD_MUTEX_UNLOCK();
        msleep(10);
        DEMOD_MUTEX_LOCK();
    }

	_gDVB_INFORMATION.tuneMode = paramStruct.tuneMode;
	_gDVB_INFORMATION.transSystem = KHAL_DEMOD_TRANS_SYS_DVBC;
	_gDVB_INFORMATION.DVBC_tunedFreq = paramStruct.frequency;
	_gDVB_INFORMATION.symbolRate = paramStruct.symbolRate;
	_gDVB_INFORMATION.constellation = paramStruct.constellation;
	_gDVB_INFORMATION.bSpectrumInv = paramStruct.bSpectrumInv;
	DDI_DEMOD_LOG("[SOC_DEMOD] DVB-C Scan Start %d!!!\n", paramStruct.tuneMode);

    if (KHAL_DEMOD_TUNE_SCAN != paramStruct.tuneMode)
	{
		DDI_DEMOD_LOG("Auto Detection Disable symbolRate=%d, constellation=%d!!!\n", paramStruct.symbolRate, paramStruct.constellation);
        status &= MHal_DVBTxC_Set_Config_dvbc_auto (OFF);
//        status &= MHal_DVBTxC_Set_Config_dvbc_atv_detector(OFF);

    }
    else
    {
        DDI_DEMOD_LOG("Auto Detection Enable symbolRate=%d, constellation=%d!!!\n", paramStruct.symbolRate, paramStruct.constellation);
        status &= MHal_DVBTxC_Set_Config_dvbc_auto (ON);
//        status &= MHal_DVBTxC_Set_Config_dvbc_atv_detector(ON);
    }


    constellation = _KHAL_Demod_DVB_DVBC_ConvConst(paramStruct.constellation);

    status &= MHal_DVBTxC_SetDvbcParam(constellation);

    status &= MHal_DVBTxC_Config_dvbc((paramStruct.symbolRate), (paramStruct.frequency), 1);

    status &= MHal_DVBTxC_Active(ON);

	_gSOC_DEMOD_FlagPostJob	= KHAL_DEMOD_DVB_TU_FLAG_START;
	_gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_UNKNOWN;

	if(status)
	{
        DEMOD_MUTEX_UNLOCK();
		return OK;
	}
	else
	{
        DEMOD_MUTEX_UNLOCK();
		return NOT_OK;
	}
}

/* 4.5-1 DVBT
* Description: Check the Demod. Lock state
* Syntax:TU_RETURN_VALUE_T HAL_DEMOD_XXX_CheckLocke(HAL_DEMOD_LOCK_STATE_T *pLockState)
* Parameters: pLockState [out] lock state
* Return Value: If this function succeeds, the return value is OK.If this function fails, the return value is NOT_OK.
*/
static SOC_DEMOD_LOCK_STATE_T _KHAL_Demod_DVB_DVBT_CheckLock(void)
{
	BOOLEAN bLock = FALSE;
    UINT32 elapsedTime	= 0;
   
    if(_gDVB_INFORMATION.transSystem == KHAL_DEMOD_TRANS_SYS_DVBT)
    {
        bLock = MHal_DVBTxC_Lock(E_SYS_DVBT, COFDM_LOCK_STABLE_DVBT);
#ifdef DEMOD_DVB_T2_MERGE_T
        if (!bLock && MHal_DVBTxC_Lock(E_SYS_DVBT, COFDM_T2_AUTO_DETECT_FOUND_T2))
        {
            _gDVB_INFORMATION.transSystem = KHAL_DEMOD_TRANS_SYS_DVBT2;
            DDI_DEMOD_LOG("[SOC_DEMOD] DVBT self switch to DVBT2\n");
            return SOC_DEMOD_LOCK_FOUND_DVBT2;
        }
#endif
    }
    else // HAL_DEMOD_TRANS_SYS_DVBT2
    {
        bLock = MHal_DVBTxC_Lock(E_SYS_DVBT2, COFDM_FEC_LOCK_T2);
#ifdef DEMOD_DVB_T2_MERGE_T
        if (!bLock && MHal_DVBTxC_Lock(E_SYS_DVBT, COFDM_TPS_LOCK))
        {
            _gDVB_INFORMATION.transSystem = KHAL_DEMOD_TRANS_SYS_DVBT;
            DDI_DEMOD_LOG("[SOC_DEMOD] DVBT2 self switch to DVBT\n");
            return SOC_DEMOD_LOCK_TPS_LOCK;
        }
#endif
    }

	if (bLock)
	{
		DDI_DEMOD_LOG("SOC_DEMOD STABLE LOCKED(%d)!!!\n", _gDVB_INFORMATION.transSystem);
        if (_gSOC_DEMOD_Lock_postJob == KHAL_DVB_TUNE_STATUS_LOCK_HOLD)
            gDVBLockHoldCount++;
        _gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_LOCK;
        gDVBLockDelayStartSysTime = (UINT32)jiffies_to_msecs(jiffies);
		return SOC_DEMOD_LOCK_OK;
	}
	else
	{
		if ((KHAL_DEMOD_TUNE_SCAN == _gDVB_INFORMATION.tuneMode) &&
		    ((_gSOC_DEMOD_Lock_postJob == KHAL_DVB_TUNE_STATUS_LOCK) || (_gSOC_DEMOD_Lock_postJob == KHAL_DVB_TUNE_STATUS_LOCK_HOLD)))
		{
            CURRENT_SYSTIME = jiffies_to_msecs(jiffies);
            elapsedTime = CURRENT_SYSTIME - gDVBLockDelayStartSysTime;

            if ((elapsedTime < LOCK_HOLD_TIMEOUT) && (gDVBLockHoldCount < LOCK_HOLD_COUNT_MAX))
            {
                DDI_DEMOD_LOG("SOC_DEMOD LOCK HOLD(%d)!!!\n", gDVBLockHoldCount);
                _gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_LOCK_HOLD;
                return SOC_DEMOD_LOCK_OK;
            }

            DDI_DEMOD_LOG("SOC_DEMOD NOT LOCKED(%d)!!!\n", _gDVB_INFORMATION.transSystem);
            if (_gSOC_DEMOD_Lock_postJob == KHAL_DVB_TUNE_STATUS_LOCK_HOLD)
                gDVBLockHoldCount++;
            _gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_UNLOCK;
            return SOC_DEMOD_LOCK_NOT_LOCKED;
        }

        if(_gDVB_INFORMATION.transSystem == KHAL_DEMOD_TRANS_SYS_DVBT)
        {
            if ((KHAL_DEMOD_TUNE_MANUAL == _gDVB_INFORMATION.tuneMode) || (KHAL_DEMOD_TUNE_SCAN == _gDVB_INFORMATION.tuneMode))
            {
                if (MHal_DVBTxC_Lock(E_SYS_DVBT, COFDM_T2_AUTO_DETECT_NO_CH)) // no DVBT or DVBT2
                {
                    DDI_DEMOD_LOG("[SOC_DEMOD] COFDM_T2_AUTO_DETECT_NO_CH!!!\n");
                    return SOC_DEMOD_LOCK_NO_CH;
                }
#ifdef DEMOD_DVB_T2_MERGE_T
                // do nothing...
#else
                if (MHal_DVBTxC_Lock(E_SYS_DVBT, COFDM_T2_AUTO_DETECT_FOUND_T2)) // found T2
                {
                    DDI_DEMOD_LOG("[SOC_DEMOD] COFDM_T2_AUTO_DETECT_FOUND_T2!!!\n");
                    return SOC_DEMOD_LOCK_FOUND_DVBT2;
                }
#endif
            }
            if (MHal_DVBTxC_Lock(E_SYS_DVBT,COFDM_TPS_LOCK))
            {
                DDI_DEMOD_LOG("[SOC_DEMOD] TPS LOCKED!!!\n");
                return SOC_DEMOD_LOCK_TPS_LOCK;
            }
        }
        else // KHAL_DEMOD_TRANS_SYS_DVBT2
        {
            if (MHal_DVBTxC_Lock(E_SYS_DVBT2, COFDM_P1_LOCK_HISTORY))
            {
                DDI_DEMOD_LOG("[SOC_DEMOD] DVBT2_LOCK_P1_LOCK!!!\n");
                return SOC_DEMOD_LOCK_P1_LOCK;
            }
        }

		DDI_DEMOD_LOG("SOC_DEMOD NOT LOCKED(%d)!!!\n", _gDVB_INFORMATION.transSystem);
		return SOC_DEMOD_LOCK_NOT_LOCKED;
	}
}

/* 
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
#ifndef DEMOD_DVB_T2_MERGE_T
static int _KHAL_DEMOD_DVB_DVBT2_SetDemod(void)
{
    int status = OK;
    DMD_DVBT2_RF_CHANNEL_BANDWIDTH BWSoc;

    DDI_DEMOD_LOG("channelBW=%d, tuneMode=%d\n", _gDVB_INFORMATION.channelBW, _gDVB_INFORMATION.tuneMode);

	switch(_gDVB_INFORMATION.channelBW)
	{
		case KHAL_DEMOD_CH_BW_8M:
		    BWSoc = E_DMD_T2_RF_BAND_8MHz;
		    break;
		case KHAL_DEMOD_CH_BW_7M:
		    BWSoc = E_DMD_T2_RF_BAND_7MHz;
		    break;
		case KHAL_DEMOD_CH_BW_6M:
		    BWSoc = E_DMD_T2_RF_BAND_6MHz;
    		break;
		default:
		    BWSoc = E_DMD_T2_RF_BAND_8MHz;
            printk( "[SOC_DEMOD]  KHAL_DEMOD_CH_BW_UNKNOWN [%s]!!!\n", __FUNCTION__);
    		break;
	}

    status &= MHal_DVBTxC_Config_DVBT2(BWSoc, 5000, 0xff, 0);
    status &= MHal_DVBTxC_Active(1);

	_gSOC_DEMOD_FlagPostJob		= KHAL_DEMOD_DVB_TU_FLAG_START;
	_gSOC_DEMOD_Lock_postJob		= KHAL_DVB_TUNE_STATUS_UNKNOWN;

	return status;
}
#endif

/* 4.4-3 DVBT
* Description: Check the Demod. lock status after demod setting. If signal lock status is valid, set *pFishisd value as true.
* Syntax TU_RETURN_VALUE_T KHAL_DEMOD_XXX_TunePostJob (BOOLEAN *pFinished)
* Parameters: None
* Return Value: If this function succeeds, the return value is OK, If this function fails, the return value is NOT_OK.
*/
static int _KHAL_Demod_DVB_DVBT_TunePostJob(void)
{

	SOC_DEMOD_LOCK_STATE_T tunerLock		= SOC_DEMOD_LOCK_UNKNOWN;

	UINT16			elapsedTime	= 0;
	static UINT32 _startTime;
	static 	UINT16			_maxDelay		= GBB_DVB_DVBT_LOCK_DELAY_DEFAULT;
    BOOLEAN bIsFoundT2 = FALSE;


	if(KHAL_DEMOD_DVB_TU_FLAG_START == _gSOC_DEMOD_FlagPostJob)
	{
	    if(_gDVB_INFORMATION.transSystem == KHAL_DEMOD_TRANS_SYS_DVBT2)
        {
            _maxDelay = GBB_DVB_DVBT2_LOCK_DELAY_DEFAULT;
        }
        else
        {
            _maxDelay = GBB_DVB_DVBT_LOCK_DELAY_DEFAULT;
        }
		_startTime = (UINT32)jiffies_to_msecs(jiffies);

		_gSOC_DEMOD_FlagPostJob	= KHAL_DEMOD_DVB_TU_FLAG_ING;
	}
    else
    {
        CURRENT_SYSTIME = (UINT32)jiffies_to_msecs(jiffies);
        elapsedTime = CURRENT_SYSTIME - _startTime;
    }

	tunerLock = _KHAL_Demod_DVB_DVBT_CheckLock();

	switch (tunerLock)
	{
		case SOC_DEMOD_LOCK_OK:
            DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_OK(%d)\n", elapsedTime);

			_gSOC_DEMOD_FlagPostJob	= KHAL_DEMOD_DVB_TU_FLAG_FINISH;
			_gSOC_DEMOD_Lock_postJob	= KHAL_DVB_TUNE_STATUS_LOCK;
			gDVBLockHoldCount = 0;
			return OK;

		case SOC_DEMOD_LOCK_NO_CH:
			_gSOC_DEMOD_FlagPostJob	= KHAL_DEMOD_DVB_TU_FLAG_FINISH;
			_gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_UNLOCK;
			DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_NO_CHANNEL_DETECT(Nerver Lock)(%d)\n", elapsedTime);
			return OK;

        case SOC_DEMOD_LOCK_FOUND_DVBT2:
		    bIsFoundT2 = TRUE;
			DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_FOUND_DVBT2(%d)\n", elapsedTime);
			break;

		case SOC_DEMOD_LOCK_TPS_LOCK:
			if (_maxDelay < GBB_DVB_DVBT_LOCK_DELAY_FEC)
				_maxDelay = GBB_DVB_DVBT_LOCK_DELAY_FEC;
			DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_TPS_LOCK \n");
			break;

        case SOC_DEMOD_LOCK_P1_LOCK:
			if (_maxDelay < GBB_DVB_DVBT2_LOCK_DELAY_FEC)
				_maxDelay = GBB_DVB_DVBT2_LOCK_DELAY_FEC;
			DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_P1_LOCK \n");
			break;

		case SOC_DEMOD_LOCK_NOT_LOCKED:
			DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_NOT_LOCKED \n");
			break;

		default :
			break;
	}

    if ((bIsFoundT2) && (_gDVB_INFORMATION.transSystem == KHAL_DEMOD_TRANS_SYS_DVBT)) // switch to DVBT2 when DVBT timeout
    {
#ifdef DEMOD_DVB_T2_MERGE_T
        // do nothing...
#else
        DDI_DEMOD_LOG("Found T2 signal, switch DVB-T to DVB-T2!!!\n");
        _gDVB_INFORMATION.transSystem = KHAL_DEMOD_TRANS_SYS_DVBT2;

		if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT2))
			return NOT_OK;
		if(OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT2))
		{
			printk("[%s] DVB-T2 DSP Load Fail!!\n",  __FUNCTION__);
            _gDVB_CurrenType = E_SYS_UNKOWN;
			return NOT_OK;
		}
        _gDVB_CurrenType = E_SYS_DVBT2;
        _KHAL_DEMOD_DVB_DVBT2_SetDemod();
        return OK;
#endif
    }

	if (elapsedTime < _maxDelay)	// continue
	{
		_gSOC_DEMOD_FlagPostJob	= KHAL_DEMOD_DVB_TU_FLAG_ING;

		return OK;
	}

    DDI_DEMOD_LOG("Timeout(%d)\n", elapsedTime);
	_gSOC_DEMOD_FlagPostJob	= KHAL_DEMOD_DVB_TU_FLAG_FINISH;

	return OK;
}

/* 4.4-1
* Description: Check the Demod lock status after demod setting. If signal lock status is valid, set *pFishisd value as true.
* Syntax TU_RETURN_VALUE_T KHAL_DEMOD_XXX_TunePostJob (BOOLEAN *pFinished)
* Parameters: None
* Return Value: If this function succeeds, the return value is OK, If this function fails, the return value is NOT_OK.
*/
int KHAL_DEMOD_DVB_DVBT_TunePostJob(BOOLEAN *pFinished)
{

	*pFinished = FALSE;

    DEMOD_MUTEX_LOCK();
	// DVBT demod:
	// 1.  FEC lock
	// 2.  no channel flag rise
	// 3.  time out
	// 1/2/3  ==> Tuning finish [Signal lock status is valid]

	if(KHAL_DEMOD_DVB_TU_FLAG_FINISH == _gSOC_DEMOD_FlagPostJob)
	{
		*pFinished	= TRUE;

		DDI_DEMOD_LOG("Already Done !!!\n");
        DEMOD_MUTEX_UNLOCK();
		return OK;
	}

    _KHAL_Demod_DVB_DVBT_TunePostJob();


	if(KHAL_DEMOD_DVB_TU_FLAG_FINISH == _gSOC_DEMOD_FlagPostJob)
	{
		*pFinished	= TRUE;
		DDI_DEMOD_LOG("DVB-T finish\n");
	}

    DEMOD_MUTEX_UNLOCK();
	return OK;
}

/* 4.5-2 DVBc
* Description: Check the Demod. Lock state
* Syntax:TU_RETURN_VALUE_T HAL_DEMOD_XXX_CheckLocke(HAL_DEMOD_LOCK_STATE_T *pLockState)
* Parameters: pLockState [out] lock state
* Return Value: If this function succeeds, the return value is OK.If this function fails, the return value is NOT_OK.
*/
static SOC_DEMOD_LOCK_STATE_T _KHAL_Demod_DVB_DVBC_CheckLock(void)
{
    BOOLEAN bLock;
    UINT32 elapsedTime	= 0;

    bLock = MHal_DVBTxC_Lock(E_SYS_DVBC,COFDM_FEC_LOCK_DVBC);

    if (bLock)
    {
        DDI_DEMOD_LOG("SOC_DEMOD FEC LOCKED!!!\n");
        if (_gSOC_DEMOD_Lock_postJob == KHAL_DVB_TUNE_STATUS_LOCK_HOLD)
            gDVBLockHoldCount++;
        _gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_LOCK;
        gDVBLockDelayStartSysTime = (UINT32)jiffies_to_msecs(jiffies);
        return SOC_DEMOD_LOCK_OK;
    }
    else
    {
        if ((KHAL_DEMOD_TUNE_SCAN == _gDVB_INFORMATION.tuneMode) &&
            ((_gSOC_DEMOD_Lock_postJob == KHAL_DVB_TUNE_STATUS_LOCK) || (_gSOC_DEMOD_Lock_postJob == KHAL_DVB_TUNE_STATUS_LOCK_HOLD)))
        {
            CURRENT_SYSTIME = (UINT32)jiffies_to_msecs(jiffies);
            elapsedTime = CURRENT_SYSTIME - gDVBLockDelayStartSysTime;

            if ((elapsedTime < LOCK_HOLD_TIMEOUT) && (gDVBLockHoldCount < LOCK_HOLD_COUNT_MAX))
            {
                DDI_DEMOD_LOG("SOC_DEMOD LOCK HOLD(%d)!!!\n", gDVBLockHoldCount);
                _gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_LOCK_HOLD;
                return SOC_DEMOD_LOCK_OK;
            }

            DDI_DEMOD_LOG("SOC_DEMOD NOT LOCKED(%d)!!!\n", _gDVB_INFORMATION.transSystem);
            _gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_UNLOCK;
            return SOC_DEMOD_LOCK_NOT_LOCKED;
        }

        if (MHal_DVBTxC_Lock(E_SYS_DVBC,COFDM_TR_LOCK_DVBC))
        {
            DDI_DEMOD_LOG("SOC_DEMOD TR LOCKED!!!\n");
            return SOC_DEMOD_LOCK_TR_LOCK;
        }

        DDI_DEMOD_LOG("SOC_DEMOD NOT LOCKED!!!\n");
        return SOC_DEMOD_LOCK_NOT_LOCKED;
    }

}

/* 4.4-4 DVBC
* Description: Check the Demod. lock status after demod setting. If signal lock status is valid, set *pFishisd value as true.
* Syntax TU_RETURN_VALUE_T KHAL_DEMOD_XXX_TunePostJob (BOOLEAN *pFinished)
* Parameters: None
* Return Value: If this function succeeds, the return value is OK, If this function fails, the return value is NOT_OK.
*/
static int _KHAL_Demod_DVB_DVBC_TunePostJob(void)
{
	SOC_DEMOD_LOCK_STATE_T		tunerLock	= SOC_DEMOD_LOCK_UNKNOWN;

	UINT16	elapsedTime = 0;
	static UINT32 _startTime;
	static UINT32 _TR_Lock_Time;
	static 	UINT16	_maxDelay 	= GBB_DVB_DVBC_LOCK_DELAY_DEFAULT;
	static 	BOOL	TR_LOCK_indicator = 0;


	if(KHAL_DEMOD_DVB_TU_FLAG_START == _gSOC_DEMOD_FlagPostJob)
	{
		TR_LOCK_indicator = 0;

		_startTime = (UINT32)jiffies_to_msecs(jiffies);
		_maxDelay	= GBB_DVB_DVBC_LOCK_DELAY_AUTO_DETECT;

		_gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_ING;
	}
    else
    {
        CURRENT_SYSTIME = (UINT32)jiffies_to_msecs(jiffies);
        elapsedTime = CURRENT_SYSTIME - _startTime;
    }

	tunerLock = _KHAL_Demod_DVB_DVBC_CheckLock();

	switch (tunerLock)
	{
		case SOC_DEMOD_LOCK_OK:
            DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_OK(%d)\n", elapsedTime);
			_gSOC_DEMOD_FlagPostJob	= KHAL_DEMOD_DVB_TU_FLAG_FINISH;
			_gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_LOCK;
			gDVBLockHoldCount = 0;
			return OK;

		case SOC_DEMOD_LOCK_TR_LOCK:
			if(KHAL_DEMOD_TUNE_MANUAL == _gDVB_INFORMATION.tuneMode)
			{
				if (_maxDelay < GBB_DVB_DVBC_LOCK_DELAY_TR)
					_maxDelay = GBB_DVB_DVBC_LOCK_DELAY_TR;
			}
			else
			{
                if(TR_LOCK_indicator==0)
                {
                    if (_maxDelay < GBB_DVB_DVBC_LOCK_DELAY_TR)
                        _maxDelay = GBB_DVB_DVBC_LOCK_DELAY_TR;
                    _TR_Lock_Time = (UINT32)jiffies_to_msecs(jiffies);
                }
                TR_LOCK_indicator = 1;
			}
			DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_TR_LOCK(%d)\n", elapsedTime);
			break;

		case SOC_DEMOD_LOCK_DEMOD_LOCK:
			if (_maxDelay < GBB_DVB_DVBC_LOCK_DELAY_DEMOD)
				_maxDelay = GBB_DVB_DVBC_LOCK_DELAY_DEMOD;
			DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_DEMOD_LOCK \n");
			break;

		case SOC_DEMOD_LOCK_NOT_LOCKED: // go to next step.
			DDI_DEMOD_LOG("POST JOB : SOC_DEMOD_LOCK_NOT_LOCKED \n");
			break;

		case SOC_DEMOD_LOCK_NO_CH:
			_gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_FINISH;
			_gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_UNLOCK;
			DDI_DEMOD_LOG("SOC_DEMOD_NO_CHANNEL_DETECT(Nerver Lock)(%d)\n", elapsedTime);
			return OK;

		default:
			break;
	}

	if((KHAL_DEMOD_TUNE_MANUAL != (_gDVB_INFORMATION.tuneMode)) && (TR_LOCK_indicator))
	{
        elapsedTime = CURRENT_SYSTIME - _TR_Lock_Time;
	}
	if (elapsedTime < _maxDelay)
	{
		_gSOC_DEMOD_FlagPostJob	= KHAL_DEMOD_DVB_TU_FLAG_ING;
	}
	else
	{
		_gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_FINISH;
	}

	return OK;
}

/* 4.4-2
* Description: Check the Demod. lock status after demod setting. If signal lock status is valid, set *pFishisd value as true.
* Syntax TU_RETURN_VALUE_T KHAL_DEMOD_XXX_TunePostJob (BOOLEAN *pFinished)
* Parameters: None
* Return Value: If this function succeeds, the return value is OK, If this function fails, the return value is NOT_OK.
*/
int KHAL_DEMOD_DVB_DVBC_TunePostJob(BOOLEAN *pFinished)
{
	*pFinished = FALSE;

    DEMOD_MUTEX_LOCK();
	// DVBC demod:
	// 1.  FEC lock
	// 2.  time out
	// 1/2  ==> Tuning finish [Signal lock status is valid]

	if(KHAL_DEMOD_DVB_TU_FLAG_FINISH == _gSOC_DEMOD_FlagPostJob)
	{
		*pFinished	= TRUE;

		DDI_DEMOD_LOG("Already Done !!!\n");
        DEMOD_MUTEX_UNLOCK();
		return OK;
	}

	_KHAL_Demod_DVB_DVBC_TunePostJob();


	if(KHAL_DEMOD_DVB_TU_FLAG_FINISH == _gSOC_DEMOD_FlagPostJob)
	{
		*pFinished	= TRUE;
		DDI_DEMOD_LOG("DVB-C finish\n");
	}

    DEMOD_MUTEX_UNLOCK();
	return OK;
}

/**
 * convert SOC_DEMOD lock state to common lock state.
 *
 * @param	tunerLock [in] SOC_DEMOD lock sate
 * @return	common lock state
 * @author
*/
static KHAL_DEMOD_LOCK_STATE_T _KHAL_Demod_SOC_DEMOD_ConvertLockState(SOC_DEMOD_LOCK_STATE_T lockstate)
{
    KHAL_DEMOD_LOCK_STATE_T hal_lock_state;

    switch(lockstate)
    {
        case SOC_DEMOD_LOCK_OK:
            DDI_DEMOD_LOG("KHAL_DEMOD_LOCK_OK\n");
            hal_lock_state = KHAL_DEMOD_LOCK_OK;
            break;

        case SOC_DEMOD_LOCK_SYNC_LOCK:
            DDI_DEMOD_LOG("KHAL_DEMOD_LOCK_UNSTABLE\n");
            hal_lock_state = KHAL_DEMOD_LOCK_UNSTABLE;
            break;

        case SOC_DEMOD_LOCK_TPS_LOCK:
        case SOC_DEMOD_LOCK_TR_LOCK:
        case SOC_DEMOD_LOCK_P1_LOCK:
        case SOC_DEMOD_LOCK_FOUND_DVBT2:
            if (_gSOC_DEMOD_FlagPostJob == KHAL_DEMOD_DVB_TU_FLAG_ING)
            {
                DDI_DEMOD_LOG("KHAL_DEMOD_LOCK_UNSTABLE\n");
                hal_lock_state = KHAL_DEMOD_LOCK_UNSTABLE;
            }
            else
            {
                DDI_DEMOD_LOG("KHAL_DEMOD_LOCK_FAIL\n");
                hal_lock_state = KHAL_DEMOD_LOCK_FAIL;
            }
            break;

        case SOC_DEMOD_LOCK_NOT_LOCKED:
        case SOC_DEMOD_LOCK_NO_CH:
            DDI_DEMOD_LOG("KHAL_DEMOD_LOCK_FAIL\n");
            hal_lock_state = KHAL_DEMOD_LOCK_FAIL;
            break;

        default:
            DDI_DEMOD_LOG("KHAL_DEMOD_LOCK_UNKNOWN %d\n", lockstate);
            hal_lock_state = KHAL_DEMOD_LOCK_UNKNOWN;
            break;
    }

    return hal_lock_state;
}

/* 4.5-1
* Description: Check the Demod. Lock state
* Syntax:TU_RETURN_VALUE_T KHAL_DEMOD_XXX_CheckLocke(KHAL_DEMOD_LOCK_STATE_T *pLockState)
* Parameters: pLockState [out] lock state
* Return Value: If this function succeeds, the return value is OK.If this function fails, the return value is NOT_OK.
*/
int KHAL_DEMOD_DVB_DVBT_CheckLock(KHAL_DEMOD_LOCK_STATE_T *pLockState)
{
	SOC_DEMOD_LOCK_STATE_T	lockstate	= SOC_DEMOD_LOCK_UNKNOWN;
	U8 current_stat = 0;
	U32 f_unlock = 0;

    DEMOD_MUTEX_LOCK();
	lockstate = _KHAL_Demod_DVB_DVBT_CheckLock();


	*pLockState = _KHAL_Demod_SOC_DEMOD_ConvertLockState(lockstate);

    if ((KHAL_DEMOD_LOCK_OK != *pLockState) && MHal_DVBTxC_GetUnlockStatus(E_SYS_DVBT_T2, &current_stat, &f_unlock))
    {
        if (_gDVB_INFORMATION.transSystem == KHAL_DEMOD_TRANS_SYS_DVBT2)
            printk(KERN_DEBUG "[%s %d] dvbt2 unlock %d %08x\n", __FUNCTION__, __LINE__, current_stat, f_unlock);
        else
            printk(KERN_DEBUG "[%s %d] dvbt unlock %d %08x\n", __FUNCTION__, __LINE__, current_stat, f_unlock);
    }

    DEMOD_MUTEX_UNLOCK();
	return OK;
}

/* 4.5-2
* Description: Check the Demod. Lock state
* Syntax:TU_RETURN_VALUE_T HAL_DEMOD_XXX_CheckLocke(HAL_DEMOD_LOCK_STATE_T *pLockState)
* Parameters: pLockState [out] lock state
* Return Value: If this function succeeds, the return value is OK.If this function fails, the return value is NOT_OK.
*/
int KHAL_DEMOD_DVB_DVBC_CheckLock(KHAL_DEMOD_LOCK_STATE_T *pLockState)
{
	SOC_DEMOD_LOCK_STATE_T	lockstate	= SOC_DEMOD_LOCK_UNKNOWN;
	U8 current_stat = 0;
	U32 f_unlock = 0;

    DEMOD_MUTEX_LOCK();
	lockstate = _KHAL_Demod_DVB_DVBC_CheckLock();


	*pLockState = _KHAL_Demod_SOC_DEMOD_ConvertLockState(lockstate);


    if ((KHAL_DEMOD_LOCK_OK != *pLockState) && MHal_DVBTxC_GetUnlockStatus(E_SYS_DVBC, &current_stat, &f_unlock))
    {
        printk(KERN_DEBUG "[%s %d] dvbc unlock %d %08x\n", __FUNCTION__, __LINE__, current_stat, f_unlock);
    }

    DEMOD_MUTEX_UNLOCK();
	return OK;
}

/*
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_PKT_ERR(UINT32 *pPKT_Err_Acu, BOOLEAN bReset)
{
    static UINT32 u32PKT_Err_acu = 0;

    DEMOD_MUTEX_LOCK();
    *pPKT_Err_Acu = u32PKT_Err_acu;
    DEMOD_MUTEX_UNLOCK();

    return OK;
}

/**
 * check siganal state.
 *
 * @param	tunerNo				[in] 	tuner number
 * @param	bBoosterCheckMode	[in] 	Booster Mode
 * @param	*pSignalState			[out] siganal state
 * @return	OK/FAIL
 * @author
*/
static void _KHAL_DEMOD_DVB_DVBT_CheckSignalState(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
	UINT16	status		= TRUE;
	UINT16	quality		= 0;
//	UINT16	pktErr		= 0;
    S32     snr_e2      = 0;
    S32     ber_e7      = 0;
    UINT16  if_agc_gain = 0xFFFF;
    S32     IFGain_e3   = 0;
    U32     Accu_PktErr = 0;

#ifdef DEMOD_DVB_T2_MERGE_T
    if (_gDVB_INFORMATION.transSystem == KHAL_DEMOD_TRANS_SYS_DVBT2)
#else
    if (_gDVB_CurrenType == E_SYS_DVBT2)
#endif
    {
        status &= MHal_DVBTxC_DVBT2_GetSNR(&snr_e2);
        status &= MHal_DVBTxC_DVBT2_GetPostLdpcBer(&ber_e7);
        status &= MHal_DVBTxC_DVBT2_GetSignalQuality(&quality);
//        status &= MHal_DVBTxC_DVBT2_GetPacketErr(&pktErr);
        status &= MHal_DVBTxC_GetPacketErrAccu(&Accu_PktErr, TRUE, E_SYS_DVBT2);
        status &= MHal_DVBTxC_GetIFAGCGain(&if_agc_gain);
    }
    else // dvbt
    {
        status &= MHal_DVBTxC_DVBTGetSNR(&snr_e2);
        status &= MHal_DVBTxC_GetPostViterbiBer(&ber_e7);
        status &= MHal_DVBTxC_GetSignalQuality(E_SYS_DVBT,&quality);
//        status &= MHal_DVBTxC_Get_Packet_Error(&pktErr);
        status &= MHal_DVBTxC_GetPacketErrAccu(&Accu_PktErr, TRUE, E_SYS_DVBT);
        status &= MHal_DVBTxC_GetIFAGCGain(&if_agc_gain);
    }

	pSignalState->bSignalValid	= TRUE;
	pSignalState->quality = (UINT8) quality;
    pSignalState->packetError		= Accu_PktErr;
	pSignalState->unBER 			= (UINT32) ber_e7/10;
	pSignalState->unAGC 			= if_agc_gain;
	pSignalState->unSNR 			= (UINT32)snr_e2/100;

    IFGain_e3 = (S32)if_agc_gain * 1000 / 32768;
    DDI_DEMOD_LOG("SQI(%d), PktErr(%d), SNR(%d), BER(%d), AGC(%d.%03d)\n",quality, Accu_PktErr, snr_e2, ber_e7/10, (IFGain_e3/1000), (IFGain_e3%1000));

	return;
}

/*
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_ControlTSMode(BOOLEAN bIsSerial)
{
    DDI_DEMOD_LOG("bIsSerial=%d\n", bIsSerial);
    return OK;
}

/* 4.6-1
* Description: Check signal state with the specified parameters.
* Syntax: TU_RETURN_VALUE_T HAL_DEMOD_XXX_CheckSignalState(HAL_DEMOD_SIGNAL_STATE_T*pSignalState)
* Parameters: pSignalState [out] signal state
* Return Value: If this function succeeds, the return value is OK. If this function fails, the return value is NOT_OK.
*/
int	KHAL_DEMOD_DVB_DVBT_GetSignalState(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
	SOC_DEMOD_LOCK_STATE_T	lockstate	= SOC_DEMOD_LOCK_UNKNOWN;

    DEMOD_MUTEX_LOCK();
	lockstate = _KHAL_Demod_DVB_DVBT_CheckLock();

	DDI_DEMOD_LOG("dvbt lock state is %d  !!!\n", lockstate);

	if(SOC_DEMOD_LOCK_OK == lockstate)	// state lock, change demod lock information
	{
		 _KHAL_DEMOD_DVB_DVBT_CheckSignalState(pSignalState);
	}
	else
	{
		pSignalState->bSignalValid	= FALSE;
		pSignalState->quality = 0x00;
		pSignalState->packetError		= 0xFFFFFFFF;
		pSignalState->unBER 			= 0xFFFFFFFF;
		pSignalState->unAGC 			= 0xFFFFFFFF;
		pSignalState->unSNR 			= 0xFFFFFFFF;
	}

    DEMOD_MUTEX_UNLOCK();
	return OK;
}

/**
 * check siganal state.
 *
 * @param	tunerNo				[in] 	tuner number
 * @param	bBoosterCheckMode	[in] 	Booster Mode
 * @param	*pSignalState			[out] siganal state
 * @return	OK/FAIL
 * @author
*/
static void _KHAL_DEMOD_DVB_DVBC_CheckSignalState(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
    UINT16	status		= TRUE;
    UINT16	quality		= 0;
//    UINT16	pktErr		= 0;
    S32     snr_e2      = 0;
    S32     ber_e7      = 0;
    UINT16  if_agc_gain = 0xFFFF;
    S32     IFGain_e3   = 0;
    U32     Accu_PktErr = 0;

    status &= MHal_DVBTxC_DVBCGetSNR(&snr_e2);
    status &= MHal_DVBTxC_GetPostViterbiBer(&ber_e7);
    status &= MHal_DVBTxC_GetSignalQuality(E_SYS_DVBC,&quality);
//    status &= MHal_DVBTxC_Get_Packet_Error(&pktErr);
    status &= MHal_DVBTxC_GetIFAGCGain(&if_agc_gain);
    status &= MHal_DVBTxC_GetPacketErrAccu(&Accu_PktErr, TRUE, E_SYS_DVBC);

	pSignalState->bSignalValid	= TRUE;
	pSignalState->quality = (UINT8) quality;
	pSignalState->packetError		= Accu_PktErr;
	pSignalState->unBER 			= (UINT32) ber_e7/10;
	pSignalState->unAGC 			= if_agc_gain;
	pSignalState->unSNR 			= (UINT32)snr_e2/100;


    IFGain_e3 = (S32)if_agc_gain * 1000 / 32768;
    DDI_DEMOD_LOG("SQI(%d), PktErr(%d), SNR(%ddB), BER(%d), AGC(%d.%03d)\n", quality, Accu_PktErr, snr_e2, ber_e7/10, (IFGain_e3/1000), (IFGain_e3%1000));
	return;
}

/* 4.6-2
* Description: Check signal state with the specified parameters.
* Syntax: TU_RETURN_VALUE_T HAL_DEMOD_XXX_CheckSignalState(HAL_DEMOD_SIGNAL_STATE_T*pSignalState)
* Parameters: pSignalState [out] signal state
* Return Value: If this function succeeds, the return value is OK. If this function fails, the return value is NOT_OK.
*/

int	KHAL_DEMOD_DVB_DVBC_GetSignalState(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
	SOC_DEMOD_LOCK_STATE_T lockstate = SOC_DEMOD_LOCK_UNKNOWN;

    DEMOD_MUTEX_LOCK();
	lockstate = _KHAL_Demod_DVB_DVBC_CheckLock();

	DDI_DEMOD_LOG("dvbc lock state is %d  !!!\n", lockstate);

	if(SOC_DEMOD_LOCK_OK == lockstate)	// state lock, change demod lock information
	{
		 _KHAL_DEMOD_DVB_DVBC_CheckSignalState(pSignalState);
	}
	else
	{
		pSignalState->bSignalValid	= FALSE;
		pSignalState->quality = 0x00;
		pSignalState->packetError		= 0xFFFFFFFF;
		pSignalState->unBER 			= 0xFFFFFFFF;
		pSignalState->unAGC 			= 0xFFFFFFFF;
		pSignalState->unSNR 			= 0xFFFFFFFF;
	}

    DEMOD_MUTEX_UNLOCK();
	return OK;
}

/**
 * read DVB-T TPS Info.
 *
 * @param	tunerNo 	[in] tuner number
 * @return	TU_SPECDATA_T
 * @author
*/
static void _KHAL_DEMOD_DVB_DVBT_CheckSpecialData(KHAL_DEMOD_SPECDATA_DVBT_T *pSpecDVBT)
{
    KHAL_DEMOD_SPECDATA_DVBT_T specDVBT;
    UINT16 tps_info = 0;
    UINT16 u16temp = 0;

    specDVBT.carrierMode = KHAL_DEMOD_TPS_CARR_UNKNOWN;
    specDVBT.bSpectrumInv = FALSE;
    specDVBT.hierarchy = KHAL_DEMOD_TPS_HIERA_UNKNOWN;
    specDVBT.guardInterval = KHAL_DEMOD_TPS_GUARD_UNKNOWN;
    specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_UNKNOWN;
    specDVBT.constellation = KHAL_DEMOD_TPS_CONST_UNKNOWN;
    specDVBT.bProfileHP = pSpecDVBT->bProfileHP;

	/* CheckTuner Lock state */
	if(SOC_DEMOD_LOCK_OK != _KHAL_Demod_DVB_DVBT_CheckLock())
		return;

	/* Get TPS Information */
    if (TRUE !=  MHal_DVBTxC_GetTpsInfo(&tps_info))
		return;

	/* TPS Information followed by EN 300 744 Spec. */
	/* 4.6.2.9 TRANSMISSION MODE */
	u16temp = (tps_info >> 13) & 0x03;

	if (u16temp == _T_FFT_2K)
	{
		specDVBT.carrierMode = KHAL_DEMOD_TPS_CARR_2K;
	}
	else if (u16temp == _T_FFT_8K)
	{
		specDVBT.carrierMode = KHAL_DEMOD_TPS_CARR_8K;
	}
	else if (u16temp == _T_FFT_4K)
	{
		specDVBT.carrierMode = KHAL_DEMOD_TPS_CARR_4K;
	}
	else
	{
		specDVBT.carrierMode = KHAL_DEMOD_TPS_CARR_2K;

	}

	/* 4.6.2.8 GUARD INTERVALS */
	u16temp = (tps_info >> 11) & 0x03;

	if (u16temp == _T_GI_1Y32)
	{
		specDVBT.guardInterval = KHAL_DEMOD_TPS_GUARD_1_32;
	}
	else if (u16temp == _T_GI_1Y16)
	{
		specDVBT.guardInterval	= KHAL_DEMOD_TPS_GUARD_1_16;
	}
	else if (u16temp == _T_GI_1Y8)
	{
		specDVBT.guardInterval = KHAL_DEMOD_TPS_GUARD_1_8;
	}
	else if (u16temp == _T_GI_1Y4)
	{
		specDVBT.guardInterval = KHAL_DEMOD_TPS_GUARD_1_4;
	}

	/* ETC PRIORITY */
	if(specDVBT.bProfileHP)
	{
		/* 4.6.2.7 CODERATEATE HP */
		u16temp = (tps_info >> 8) & 0x07;

		if (u16temp == _CR1Y2)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_1_2;
		}
		else if (u16temp == _CR2Y3)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_2_3;
		}
		else if (u16temp == _CR3Y4)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_3_4;
		}
		else if (u16temp == _CR5Y6)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_5_6;
		}
		else if (u16temp == _CR7Y8)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_7_8;
		}
		else
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_1_2;

		}
	}
	else
	{
		/* 4.6.2.7 CODERATEATE LP */
		u16temp = (tps_info >> 5) & 0x07;

		if (u16temp == _CR1Y2)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_1_2;
		}
		else if (u16temp == _CR2Y3)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_2_3;
		}
		else if (u16temp == _CR3Y4)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_3_4;
		}
		else if (u16temp == _CR5Y6)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_5_6;
		}
		else if (u16temp == _CR7Y8)
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_7_8;
		}
		else
		{
			specDVBT.codeRate = KHAL_DEMOD_TPS_CODE_1_2;

		}
	}

	/* 4.6.2.6 HIERACHY INFORMATION */
	u16temp = (tps_info >> 2) & 0x07;


	if (u16temp == _T_HIERA_NONE)
	{
		specDVBT.hierarchy = KHAL_DEMOD_TPS_HIERA_NONE;
	}
	else if (u16temp == _T_HIERA_1)
	{
		specDVBT.hierarchy = KHAL_DEMOD_TPS_HIERA_1;
	}
	else if (u16temp == _T_HIERA_2)
	{
		specDVBT.hierarchy = KHAL_DEMOD_TPS_HIERA_2;
	}
	else if (u16temp == _T_HIERA_4)
	{
		specDVBT.hierarchy = KHAL_DEMOD_TPS_HIERA_4;
	}
	else
	{
		specDVBT.hierarchy = KHAL_DEMOD_TPS_HIERA_NONE;
	}

	/* 4.6.2.5 CONSTELLATION */
	u16temp = tps_info & 0x0003;

	if (u16temp == _QPSK)
	{
		specDVBT.constellation = KHAL_DEMOD_TPS_CONST_QPSK;
	}
	else if (u16temp == _16QAM)
	{
		specDVBT.constellation	= KHAL_DEMOD_TPS_CONST_QAM_16;
	}
	else if (u16temp == _64QAM)
	{
		specDVBT.constellation = KHAL_DEMOD_TPS_CONST_QAM_64;
	}
	else
	{
		specDVBT.constellation = KHAL_DEMOD_TPS_CONST_QPSK;
	}

    specDVBT.bSpectrumInv = MHal_DVBTxC_GetSpectrumInv(E_SYS_DVBT);

	*pSpecDVBT 	= specDVBT;

	DDI_DEMOD_LOG("constellation %d,hierarchy %d,codeRate %d\n", specDVBT.constellation, specDVBT.hierarchy, specDVBT.codeRate);
	DDI_DEMOD_LOG("bProfileHP %d,guardInterval %d,carrierMode %d\n", specDVBT.bProfileHP, specDVBT.guardInterval, specDVBT.carrierMode);
	return;
}

/* 4.7-1
* Description: Check Specia Data with the specified parameters.
* Syntax: TU_RETURN_VALUE_T KHAL_DEMOD_XXX_CheckSpecialData(KHAL_DEMOD_SPECDATA_XXX_T*pSpecData)
* Parameters: None
* Return Value: If this function succeeds, the return value is OK.If this function fails, the return value is NOT_OK.
*/
int KHAL_DEMOD_DVB_DVBT_CheckSpecialData(KHAL_DEMOD_SPECDATA_DVBT_T *pSpecDVBT)
{

    DEMOD_MUTEX_LOCK();
    pSpecDVBT->bSpectrumInv = _gDVB_INFORMATION.bSpectrumInv;
    pSpecDVBT->bProfileHP= _gDVB_INFORMATION.bProfileHP;
    pSpecDVBT->hierarchy= _gDVB_INFORMATION.hierarchy;
    pSpecDVBT->carrierMode= _gDVB_INFORMATION.carrierMode ;
    pSpecDVBT->guardInterval = _gDVB_INFORMATION.guardInterval;
    pSpecDVBT->codeRate= _gDVB_INFORMATION.codeRate;
    pSpecDVBT->constellation = _gDVB_INFORMATION.constellation;

    _KHAL_DEMOD_DVB_DVBT_CheckSpecialData(pSpecDVBT);

    DEMOD_MUTEX_UNLOCK();
    return OK;
}

/*
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
static void _KHAL_DEMOD_DVB_DVBT2_CheckSpecialData(KHAL_DEMOD_SPECDATA_DVBT2_T *pSpecDVBT2)
{
    UINT8  u8PlpBitMap[32];
    int    i,j;
    UINT8  PLPCount =0;
    KHAL_DEMOD_SPECDATA_DVBT2_T specDVBT2;
    U8  status = TRUE;
    U16 u16Retry = 0;
    U16 u16L1_parameter;

    KHAL_DEMOD_TPS_GUARD_INTERVAL_T eT2GI[7] = {
        KHAL_DEMOD_TPS_GUARD_1_32,
        KHAL_DEMOD_TPS_GUARD_1_16,
        KHAL_DEMOD_TPS_GUARD_1_8,
        KHAL_DEMOD_TPS_GUARD_1_4,
        KHAL_DEMOD_TPS_GUARD_1_128,
        KHAL_DEMOD_TPS_GUARD_19_128,
        KHAL_DEMOD_TPS_GUARD_19_256};
    KHAL_DEMOD_TPS_CODERATE_T eT2CR[6] = {
        KHAL_DEMOD_TPS_CODE_1_2,
        KHAL_DEMOD_TPS_CODE_3_5,
        KHAL_DEMOD_TPS_CODE_2_3,
        KHAL_DEMOD_TPS_CODE_3_4,
        KHAL_DEMOD_TPS_CODE_4_5,
        KHAL_DEMOD_TPS_CODE_5_6};
    KHAL_DEMOD_TPS_CONSTELLATION_T eT2Const[4] = {
        KHAL_DEMOD_TPS_CONST_QPSK,
        KHAL_DEMOD_TPS_CONST_QAM_16,
        KHAL_DEMOD_TPS_CONST_QAM_64,
        KHAL_DEMOD_TPS_CONST_QAM_256};
    KHAL_DEMOD_TPS_CARRIER_MODE_T T2FFT[8] = {
        KHAL_DEMOD_TPS_CARR_2K,
        KHAL_DEMOD_TPS_CARR_8K,
        KHAL_DEMOD_TPS_CARR_4K,
        KHAL_DEMOD_TPS_CARR_1K,
        KHAL_DEMOD_TPS_CARR_16K,
        KHAL_DEMOD_TPS_CARR_32K,
        KHAL_DEMOD_TPS_CARR_8K,
        KHAL_DEMOD_TPS_CARR_32K};

    specDVBT2.carrierMode = KHAL_DEMOD_TPS_CARR_UNKNOWN;
    specDVBT2.guardInterval = KHAL_DEMOD_TPS_GUARD_UNKNOWN;
    specDVBT2.codeRate = KHAL_DEMOD_TPS_CODE_UNKNOWN;
    specDVBT2.constellation = KHAL_DEMOD_TPS_CONST_UNKNOWN;
    specDVBT2.bSpectrumInv = FALSE;

    /* CheckTuner Lock state */
    if(SOC_DEMOD_LOCK_OK != _KHAL_Demod_DVB_DVBT_CheckLock())
        return ;

    status = MHal_DVBTxC_DVBT2_GetPlpBitMap(u8PlpBitMap);
    while ((status == FALSE) && (u16Retry < 10))
    {
        u16Retry++;
        printk("GetPlpBitMap retry %d \n", u16Retry);
        msleep(100);
        status = MHal_DVBTxC_DVBT2_GetPlpBitMap(u8PlpBitMap);
    }
    if (status)
    {
        for(i = 0; i < 32; i++)
        {
            for(j = 0; j < 8; j++)
            {
                if((u8PlpBitMap[i] >> j) & 1)
                {
                    PLPCount++;
                }
            }
        }
    }
    specDVBT2.unPLP = PLPCount;

    if (MHal_DVBTxC_DVBT2_Get_L1_Parameter(&u16L1_parameter, MHal_DVBTxC_T2_GUARD_INTERVAL))
    {
        u16L1_parameter &= 0x07;
        if (u16L1_parameter < sizeof(eT2GI)/sizeof(KHAL_DEMOD_TPS_GUARD_INTERVAL_T))
            specDVBT2.guardInterval = eT2GI[u16L1_parameter];
    }

    if (MHal_DVBTxC_DVBT2_Get_L1_Parameter(&u16L1_parameter, MHal_DVBTxC_T2_CODE_RATE))
    {
        u16L1_parameter &= 0x07;
        if (u16L1_parameter < sizeof(eT2CR)/sizeof(KHAL_DEMOD_TPS_CODERATE_T))
            specDVBT2.codeRate = eT2CR[u16L1_parameter];
    }

    if (MHal_DVBTxC_DVBT2_Get_L1_Parameter(&u16L1_parameter, MHal_DVBTxC_T2_MODUL_MODE))
    {
        u16L1_parameter &= 0x07;
        if (u16L1_parameter < sizeof(eT2Const)/sizeof(KHAL_DEMOD_TPS_CONSTELLATION_T))
            specDVBT2.constellation = eT2Const[u16L1_parameter];
    }

    if (MHal_DVBTxC_DVBT2_Get_L1_Parameter(&u16L1_parameter, MHal_DVBTxC_T2_FFT_VALUE))
    {
        u16L1_parameter &= 0x07;
        if (u16L1_parameter < sizeof(T2FFT)/sizeof(KHAL_DEMOD_TPS_CARRIER_MODE_T))
            specDVBT2.carrierMode = T2FFT[u16L1_parameter];
    }

    specDVBT2.bSpectrumInv = MHal_DVBTxC_GetSpectrumInv(E_SYS_DVBT2);

    *pSpecDVBT2 = specDVBT2;

	DDI_DEMOD_LOG("unPLP %d,guardInterval %d,codeRate %d\n", specDVBT2.unPLP, specDVBT2.guardInterval, specDVBT2.codeRate);
	DDI_DEMOD_LOG("constellation %d,carrierMode %d\n", specDVBT2.constellation, specDVBT2.carrierMode);
    return;
}

/*
* Description:
* Syntax:
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBT2_CheckSpecialData(KHAL_DEMOD_SPECDATA_DVBT2_T *pSpecDVBT2)
{

    DEMOD_MUTEX_LOCK();
    pSpecDVBT2->carrierMode= _gDVB_INFORMATION.carrierMode ;
    pSpecDVBT2->guardInterval = _gDVB_INFORMATION.guardInterval;
    pSpecDVBT2->codeRate= _gDVB_INFORMATION.codeRate;
    pSpecDVBT2->constellation = _gDVB_INFORMATION.constellation;

    _KHAL_DEMOD_DVB_DVBT2_CheckSpecialData(pSpecDVBT2);

    DEMOD_MUTEX_UNLOCK();
    return OK;
}

/**
 * decode DVB-C const/symbolrate.
 *
 * @param	tunerNo 	[in] tuner number
 * @return	TU_SPECDATA_T
 * @author
*/
static void _KHAL_DEMOD_DVB_DVBC_CheckSpecialData(KHAL_DEMOD_SPECDATA_DVBC_T *pSpecDVBC)
{
	KHAL_DEMOD_SPECDATA_DVBC_T 		specDVBC;
	UINT32				dvbc_info;
	UINT8				u8temp;

    specDVBC.constellation = KHAL_DEMOD_TPS_CONST_UNKNOWN;
    specDVBC.bSpectrumInv = FALSE;
    specDVBC.symbolRate = 0;

	if(SOC_DEMOD_LOCK_OK != _KHAL_Demod_DVB_DVBC_CheckLock())
	{
		DDI_DEMOD_LOG("Not Locked !!!\n");
		return;
	}

	/* Get DVB-C Information */
	if(TRUE != MHal_DVBTxC_GetDvbcInfo(&dvbc_info))
	{
		printk("[SOC_DEMOD] (%s:%d) Fail Getting DVB-C info !!!\n", __FUNCTION__, __LINE__);
		return;
	}

	DDI_DEMOD_LOG( "DVB-C Info [0x%08x]\n", dvbc_info);

	/* Symbolrate */
    specDVBC.symbolRate = (dvbc_info & 0xFFFF);

	/* Constellation */
	u8temp = (dvbc_info >> 16) & 0xFF;

	if (u8temp == _C_16QAM)
	{
		specDVBC.constellation = KHAL_DEMOD_TPS_CONST_QAM_16;
	}
	else if (u8temp == _C_32QAM)
	{
		specDVBC.constellation = KHAL_DEMOD_TPS_CONST_QAM_32;
	}
	else if (u8temp == _C_64QAM)
	{
		specDVBC.constellation = KHAL_DEMOD_TPS_CONST_QAM_64;
	}
	else if (u8temp == _C_128QAM)
	{
		specDVBC.constellation = KHAL_DEMOD_TPS_CONST_QAM_128;
	}
	else if (u8temp == _C_256QAM)
	{
		specDVBC.constellation = KHAL_DEMOD_TPS_CONST_QAM_256;
	}
	else
	{
		specDVBC.constellation = KHAL_DEMOD_TPS_CONST_QAM_64;
	}

    specDVBC.bSpectrumInv = MHal_DVBTxC_GetSpectrumInv(E_SYS_DVBC);

	*pSpecDVBC	= specDVBC;

	return;
}

/* 4.7-2
* Description: Check Specia Data with the specified parameters.
* Syntax: TU_RETURN_VALUE_T KHAL_DEMOD_XXX_CheckSpecialData(KHAL_DEMOD_SPECDATA_XXX_T*pSpecData)
* Parameters: None
* Return Value: If this function succeeds, the return value is OK.If this function fails, the return value is NOT_OK.
*/
int KHAL_DEMOD_DVB_DVBC_CheckSpecialData(KHAL_DEMOD_SPECDATA_DVBC_T *pSpecDVBC)
{

    DEMOD_MUTEX_LOCK();
    pSpecDVBC->bSpectrumInv = _gDVB_INFORMATION.bSpectrumInv;
    pSpecDVBC->constellation	= _gDVB_INFORMATION.constellation;
    pSpecDVBC->symbolRate 	= _gDVB_INFORMATION.symbolRate;

    _KHAL_DEMOD_DVB_DVBC_CheckSpecialData(pSpecDVBC);

    DEMOD_MUTEX_UNLOCK();
	return OK;
}

/* 4.8
* Description: Gets frequency offset
*Syntax TU_RETURN_VALUE_T KHAL_DEMOD_XXX_CheckFrequencyOffset(SINT32 *pFreqOffset)
*Parameters: pFreqoffset [out] frequency offset
* Return Value If this function succeeds, the return value is OK. If this function fails, the return value is NOT_OK.
*/
int	KHAL_DEMOD_DVB_CheckFrequencyOffset(SINT32 *pFreqOffset)
{

	UINT16 status = TRUE;
	S32 readFreqOffset = 0;
	UINT8 u8BW = 8;

    DEMOD_MUTEX_LOCK();

	if (KHAL_DEMOD_TRANS_SYS_DVBC == _gDVB_INFORMATION.transSystem)
	{
		/* SOC_DEMOD does not support DVB-C Frequency offset */
        status &=  MHal_DVBTxC_Get_FreqOffset(E_SYS_DVBC, &readFreqOffset, 0);
	}
    else if ((KHAL_DEMOD_TRANS_SYS_DVBT == _gDVB_INFORMATION.transSystem) || (KHAL_DEMOD_TRANS_SYS_DVBT2 == _gDVB_INFORMATION.transSystem))
	{
		switch(_gDVB_INFORMATION.channelBW)
		{
			case KHAL_DEMOD_CH_BW_6M:
				u8BW = 6;
				break;

			case KHAL_DEMOD_CH_BW_7M:
				u8BW = 7;
				break;

			case KHAL_DEMOD_CH_BW_8M: /* go down */
			default:
				u8BW = 8;
				break;
		}
        status &= MHal_DVBTxC_Get_FreqOffset(_gDVB_CurrenType, &readFreqOffset, u8BW);
	}
#if 1 // TODO: DVBS FreqOffset
	else if ((KHAL_DEMOD_TRANS_SYS_DVBS == (_gDVB_INFORMATION.transSystem))||(KHAL_DEMOD_TRANS_SYS_DVBS2 == (_gDVB_INFORMATION.transSystem)))	// KHAL_DEMOD_TRANS_SYS_DVBS
	{
        INTERN_DVBS_Get_FreqOffset(&readFreqOffset,u8BW);
	}
#endif

	if (status == TRUE)
	{
		*pFreqOffset = readFreqOffset;
		DDI_DEMOD_LOG("FreqOffset=%d\n", *pFreqOffset);
        DEMOD_MUTEX_UNLOCK();
		return OK;
	}

	DDI_DEMOD_LOG("return error!!!\n");
    DEMOD_MUTEX_UNLOCK();
	return NOT_OK;
}

/* 4.9
*
 */
int KHAL_DEMOD_DVB_GetCellID(UINT16 *pCellID)
{

    DEMOD_MUTEX_LOCK();
	/* 4.6.2.10 CELL IDENTIFIER */
    MHal_DVBTxC_GetCellId(_gDVB_CurrenType, pCellID);

    DEMOD_MUTEX_UNLOCK();

	DDI_DEMOD_LOG("Cell ID 0x%x\n", *pCellID);
	return	OK;
}

/* 4.10
*
*/
int KHAL_DEMOD_DVB_GetPacketError(UINT32 *pFecPkerr)
{
	U8	status = TRUE;
	U16 U16_temp = 0;

    DEMOD_MUTEX_LOCK();

#ifdef DEMOD_DVB_T2_MERGE_T
    if (_gDVB_INFORMATION.transSystem == KHAL_DEMOD_TRANS_SYS_DVBT2)
#else
    if (_gDVB_CurrenType == E_SYS_DVBT2)
#endif
        status = MHal_DVBTxC_DVBT2_GetPacketErr(&U16_temp);
#if 1 // TODO: DVBS GetPacketError
    else if (_gDVB_CurrenType == E_SYS_DVBS)
        status = DVBS_GetPacketErr(&U16_temp);
#endif
    else // DVBT or DVBC
    {
        status = MHal_DVBTxC_Get_Packet_Error(&U16_temp);
    }

	*pFecPkerr = (U32)U16_temp;

	DDI_DEMOD_LOG("PacketError %d\n", *pFecPkerr);
	if(status==TRUE)
	{
        DEMOD_MUTEX_UNLOCK();
		return OK;
	}
	else
	{
        DEMOD_MUTEX_UNLOCK();
		return NOT_OK;
    }
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBT2_ChangePLP(UINT8 unPLP)
{
    U8 status = TRUE;

    DEMOD_MUTEX_LOCK();

#ifdef DEMOD_DVB_T2_MERGE_T
    if ((MHal_DVBTxC_Lock(E_SYS_DVBT2, COFDM_FEC_LOCK_T2)) && (_gDVB_CurrenType == E_SYS_DVBT_T2))
#else
    if ((MHal_DVBTxC_Lock(E_SYS_DVBT2, COFDM_FEC_LOCK_T2)) && (_gDVB_CurrenType == E_SYS_DVBT2))
#endif
    {
        DDI_DEMOD_LOG("Change PLP %d\n", unPLP);
        if (unPLP != 0xFF)
        {
            U16 u16Retry = 0;
            U8 u8GroupId = 0;
            msleep(500);

            status = MHal_DVBTxC_DVBT2_GetPlpGroupID(unPLP, &u8GroupId);
            while ((status == FALSE) && (u16Retry < 60))
            {
                u16Retry++;
                printk("DoSet_DVBT2 get groupid retry %d \n", u16Retry);
                msleep(100);
                status = MHal_DVBTxC_DVBT2_GetPlpGroupID(unPLP, &u8GroupId);
            }
            if (status == FALSE)
            {
                printk("DoSet_DVBT2 MHal_DVBTxC_DVBT2_GetPlpGroupID(%d) Error \n", unPLP);
                DEMOD_MUTEX_UNLOCK();
                return NOT_OK;
            }
            else
            {
                DDI_DEMOD_LOG("[MPLP] Get PLP Group ID %d %d !!!\n", unPLP, u8GroupId);
            }

            status = MHal_DVBTxC_DVBT2_SetPlpGroupID(unPLP, u8GroupId);
            if (status == FALSE)
            {
                printk("DoSet_DVBT2 MHal_DVBTxC_DVBT2_SetPlpGroupID(%d,%d) Error", unPLP, u8GroupId);
                DEMOD_MUTEX_UNLOCK();
                return NOT_OK;
            }
            else
            {
                _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_START;
                _gSOC_DEMOD_Lock_postJob = KHAL_DVB_TUNE_STATUS_UNKNOWN;
                DDI_DEMOD_LOG("[MPLP] Set PLP Group ID %d %d !!!\n", unPLP, u8GroupId);
            }
        }
    }
    else
    {
        DDI_DEMOD_LOG("%d return error!!!\n", unPLP);
        DEMOD_MUTEX_UNLOCK();
        return NOT_OK;
    }

    DEMOD_MUTEX_UNLOCK();
    return OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBT2_GetMultiPLPInfo(KHAL_MULTI_TS_INFO_T *pPlpInfo)
{
    U8  status = TRUE;
    U8  u8PlpBitMap[32];
    U8  PLPCount =0;
    int i, j;
    U16 u16Retry = 0;

    DEMOD_MUTEX_LOCK();

    status = MHal_DVBTxC_DVBT2_GetPlpBitMap(u8PlpBitMap);
    while ((status == FALSE) && (u16Retry < 10))
    {
        u16Retry++;
        printk("GetPlpBitMap retry %d \n", u16Retry);
        msleep(100);
        status = MHal_DVBTxC_DVBT2_GetPlpBitMap(u8PlpBitMap);
    }

    if (status == FALSE)
    {
        DDI_DEMOD_LOG("return error!!!\n");
        DEMOD_MUTEX_UNLOCK();
        return NOT_OK;
    }

    for(i = 0; i < 32; i++)
    {
        for(j = 0; j < 8; j++)
        {
            if((u8PlpBitMap[i] >> j) & 1)
            {
                pPlpInfo->paPLPID[PLPCount++] = (i * 8 + j);
                DDI_DEMOD_LOG("[MPLP] BitMap %d %d !!!\n",PLPCount,(i * 8 + j));
            }
        }
    }
    pPlpInfo->PLPCount = PLPCount;

    DEMOD_MUTEX_UNLOCK();
    return OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_OperMode(KHAL_DEMOD_TRANS_SYSTEM_T *pOperMode)
{

    DEMOD_MUTEX_LOCK();

    switch(_gDVB_CurrenType)
    {
        case E_SYS_DVBT:
            *pOperMode = KHAL_DEMOD_TRANS_SYS_DVBT;
            break;
        case E_SYS_DVBC:
            *pOperMode = KHAL_DEMOD_TRANS_SYS_DVBC;
            break;
        case E_SYS_DVBT2:
            *pOperMode = KHAL_DEMOD_TRANS_SYS_DVBT2;
            break;
        case E_SYS_DVBS:
            *pOperMode = KHAL_DEMOD_TRANS_SYS_DVBS;
            break;
#ifdef DEMOD_DVB_T2_MERGE_T
        case E_SYS_DVBT_T2:
            if (_gDVB_INFORMATION.transSystem == KHAL_DEMOD_TRANS_SYS_DVBT2)
                *pOperMode = KHAL_DEMOD_TRANS_SYS_DVBT2;
            else
                *pOperMode = KHAL_DEMOD_TRANS_SYS_DVBT;
            break;
#endif
        default:
            *pOperMode = KHAL_DEMOD_TRANS_SYS_UNKNOWN;
            break;
    }

    DEMOD_MUTEX_UNLOCK();
    DDI_DEMOD_LOG("*pOperMode=%d\n", *pOperMode);

    return OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_GetFWVersion(UINT32 *pFWVersion)
{
    int status=OK;
    U16 u16_version = 0x0000;

    DEMOD_MUTEX_LOCK();

    if(_gDVB_CurrenType == E_SYS_DVBS)
    {
#if 1 // TODO: DVBS GetVersion
        DVBS_Get_Version(&u16_version);
#endif
    }
    else
    {
        status = MHal_DVBTxC_Get_Version(&u16_version);
    }
    *pFWVersion = u16_version;

    DEMOD_MUTEX_UNLOCK();

    DDI_DEMOD_LOG("FW Version %04xx\n", u16_version);
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_ControlOutput(BOOLEAN bEnableOutput)
{
    DDI_DEMOD_LOG("bEnableOutput=%d\n", bEnableOutput);
	return OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_GetSQI(UINT8 *pSQI)
{
    U16 status = TRUE;
    U16 quality = 0;

    DEMOD_MUTEX_LOCK();
    switch(_gDVB_CurrenType)
    {
        case E_SYS_DVBT:
            if (MHal_DVBTxC_Lock(E_SYS_DVBT, COFDM_LOCK_STABLE_DVBT))
        	{
                status &= MHal_DVBTxC_GetSignalQuality(E_SYS_DVBT,&quality);
            }
            break;

        case E_SYS_DVBT2:
            if (MHal_DVBTxC_Lock(E_SYS_DVBT2, COFDM_FEC_LOCK_T2))
        	{
                status &= MHal_DVBTxC_DVBT2_GetSignalQuality(&quality);
            }
            break;

        case E_SYS_DVBC:
        	if (MHal_DVBTxC_Lock(E_SYS_DVBC, COFDM_FEC_LOCK_DVBC))
        	{
                status &= MHal_DVBTxC_GetSignalQuality(E_SYS_DVBC, &quality);
            }
            break;

        default:
            break;
    }

    *pSQI = (UINT8)quality;
    DEMOD_MUTEX_UNLOCK();

    if (status)
    	return OK;
    return NOT_OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_GetBER(UINT32 *pBER)
{
    S32 ber_e7 = 0;
    U16 status = TRUE;

    DEMOD_MUTEX_LOCK();
    switch(_gDVB_CurrenType)
    {
        case E_SYS_DVBT:
            status &= MHal_DVBTxC_GetPostViterbiBer(&ber_e7);
            break;

        case E_SYS_DVBT2:
            status &= MHal_DVBTxC_DVBT2_GetPostLdpcBer(&ber_e7);
            break;

        case E_SYS_DVBC:
            status &= MHal_DVBTxC_GetPostViterbiBer(&ber_e7);
            break;

        default:
            break;
    }

	*pBER = (UINT32)ber_e7/10;
    DEMOD_MUTEX_UNLOCK();

    if (status)
    	return OK;
    return NOT_OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_GetAGC(UINT32 *pAGC)
{
    U16 if_agc_gain = 0xFFFF;
    U16 status = TRUE;

    DEMOD_MUTEX_LOCK();
    status &= MHal_DVBTxC_GetIFAGCGain(&if_agc_gain);
	*pAGC = if_agc_gain;
    DEMOD_MUTEX_UNLOCK();

    if (status)
    	return OK;
    return NOT_OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_GetSNR(UINT32 *pSNR )
{
    U16 status = TRUE;
    S32 snr_e2 = 0;

    DEMOD_MUTEX_LOCK();
    switch(_gDVB_CurrenType)
    {
        case E_SYS_DVBT:
            status &= MHal_DVBTxC_DVBTGetSNR(&snr_e2);
            break;

        case E_SYS_DVBT2:
        status &= MHal_DVBTxC_DVBT2_GetSNR(&snr_e2);
            break;

        case E_SYS_DVBC:
            status &= MHal_DVBTxC_DVBCGetSNR(&snr_e2);
            break;

        default:
            break;
    }

    *pSNR = (UINT32)snr_e2/100;
    DEMOD_MUTEX_UNLOCK();

    if (status)
    	return OK;
    return NOT_OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBT2_GetSignalState(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
    U16 status = TRUE;
	U16	quality = 0;
//	U16 pktErr = 0;
    S32 snr_e2 = 0;
    S32 ber_e7 = 0;
    U16 if_agc_gain = 0xFFFF;
    S32 IFGain_e3 = 0;
    U32 Accu_PktErr = 0;

    DEMOD_MUTEX_LOCK();
	if (MHal_DVBTxC_Lock(E_SYS_DVBT2, COFDM_FEC_LOCK_T2))
	{
        status &= MHal_DVBTxC_DVBT2_GetSNR(&snr_e2);
        status &= MHal_DVBTxC_DVBT2_GetPostLdpcBer(&ber_e7);
        status &= MHal_DVBTxC_DVBT2_GetSignalQuality(&quality);
//        status &= MHal_DVBTxC_DVBT2_GetPacketErr(&pktErr);
        status &= MHal_DVBTxC_GetIFAGCGain(&if_agc_gain);
        status &= MHal_DVBTxC_GetPacketErrAccu(&Accu_PktErr, TRUE, E_SYS_DVBT2);

        pSignalState->bSignalValid	= TRUE;
        pSignalState->quality       = (UINT8) quality;
        pSignalState->packetError	= Accu_PktErr;
        pSignalState->unBER 		= (UINT32) ber_e7/10;
        pSignalState->unAGC 		= if_agc_gain;
        pSignalState->unSNR 		= (UINT32)snr_e2/100;
	}
	else
	{
        pSignalState->bSignalValid	= FALSE;
        pSignalState->quality       = 0x00;
        pSignalState->packetError	= 0xFFFFFFFF;
        pSignalState->unBER 		= 0xFFFFFFFF;
        pSignalState->unAGC 		= 0xFFFFFFFF;
        pSignalState->unSNR 		= 0xFFFFFFFF;
	}
    DEMOD_MUTEX_UNLOCK();

    IFGain_e3 = (S32)if_agc_gain * 1000 / 32768;
    DDI_DEMOD_LOG("SQI(%d), PktErr(%d), SNR(%d), BER(%d), AGC(%d.%03d)\n", quality, Accu_PktErr, snr_e2, ber_e7/10, (IFGain_e3/1000), (IFGain_e3%1000));

    if (status)
    	return OK;
    return NOT_OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_SetDemod(KHAL_DEMOD_DVBS_SET_PARAM_T halDVBSParam)
{
    UINT16  status = TRUE;
    UINT32 symbol_rate_bps = 0 ;
    DEMOD_MUTEX_LOCK();

    while (_gDVB_STRState != KHAL_DEMOD_DVB_STR_AWAKE)
    {
        DEMOD_MUTEX_UNLOCK();
        msleep(10);
        DEMOD_MUTEX_LOCK();
    }

    symbol_rate_bps = halDVBSParam.symbolRate* 1000; // symbol per second
    //_gDVB_INFORMATION.tuneMode = halDVBSParam.tuneMode;
    //_gDVB_INFORMATION.transSystem = halDVBSParam.transSystem;
    //_gDVB_INFORMATION.symbolRate = halDVBSParam.symbolRate;
    //_gDVB_INFORMATION.constellation = halDVBSParam.constellation;
    //_gDVB_INFORMATION.bSpectrumInv = halDVBSParam.bSpectrumInv;
    printk("[M2R]  DVB-S Scan Start !!!\n");
    printk("[M2R]  frequency = %d   Symbolrate = %d\n",halDVBSParam.freqKHz,halDVBSParam.symbolRate);
/*
    if (device_demodulator_extend_GetCurrentDemodulatorType() != mapi_demodulator_datatype_E_DEVICE_DEMOD_DVB_S)
    {
        device_demodulator_extend_SetCurrentDemodulatorType(mapi_demodulator_datatype_E_DEVICE_DEMOD_DVB_S);
    }
    status &= device_demodulator_extend_MSB1240_DVBS_Demod_Restart(halDVBSParam.freqKHz,halDVBSParam.symbolRate);
*/
    sDMD_DVBS_Info.eQamMode = halDVBSParam.constellation;
    sDMD_DVBS_Info.u32SymbolRate = symbol_rate_bps;
    sDMD_DVBS_Info.u32IFFreq = 0;
    sDMD_DVBS_Info.bSpecInv = FALSE;
    sDMD_DVBS_Info.bSerialTS = FALSE;
    sDMD_DVBS_Info.u8TSClk = 0xFF;

    _gSOC_DEMOD_FlagPostJob    = KHAL_DEMOD_DVB_TU_FLAG_START;
    _gSOC_DEMOD_Lock_postJob   = KHAL_DVB_TUNE_STATUS_UNKNOWN;

    status &= DVBS_Config(&sDMD_DVBS_Info, NULL, 0);

    status &= DVBS_Active(TRUE);

    if(status==0)
    {
        DEMOD_MUTEX_UNLOCK();
        printk("HAL_DEMOD_DVB_DVBS_SetDemod nok\n");
        return NOT_OK;
    }
    else
    {
        DEMOD_MUTEX_UNLOCK();
        printk("HAL_DEMOD_DVB_DVBS_SetDemod OK\n");
        return OK;
    }

}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_TunePostJob(BOOLEAN *pFinished)
{
    UINT16  elapsedTime = 0;
    static UINT32 _startTime;
    static  UINT16  _maxDelay  = GBB_DVB_DVBC_LOCK_DELAY_DEFAULT;

    *pFinished = FALSE;
    
    DEMOD_MUTEX_LOCK();
    // DVBS demod:
    // 1.  FEC lock
    // 2.  time out
    // 1/2  ==> Tuning finish [Signal lock status is valid]
    
    if(KHAL_DEMOD_DVB_TU_FLAG_FINISH == _gSOC_DEMOD_FlagPostJob)
    {
        *pFinished = TRUE;
        
        DDI_DEMOD_LOG("Already Done !!!\n");
        DEMOD_MUTEX_UNLOCK();
        return OK;
    }
    
    //_HAL_Demod_DVB_DVBS_TunePostJob();

    if(KHAL_DEMOD_DVB_TU_FLAG_START == _gSOC_DEMOD_FlagPostJob)
    {
        _startTime = (UINT32)jiffies_to_msecs(jiffies);//MAdp_SYS_GetSystemTime();
        //_maxDelay = GBB_DVB_DVBC_LOCK_DELAY_AUTO_DETECT;

        _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_ING;
    }

    if(DVBS_TR_indicator())
        _maxDelay=3000;
    else
        _maxDelay=1500;

    CURRENT_SYSTIME = (UINT32)jiffies_to_msecs(jiffies);//MAdp_SYS_GetSystemTime();
    elapsedTime = CURRENT_SYSTIME - _startTime;

    if (elapsedTime < _maxDelay)
    {
        _gSOC_DEMOD_FlagPostJob  = KHAL_DEMOD_DVB_TU_FLAG_ING;
    }
    else
    {
        _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_FINISH;
        *pFinished = TRUE;
        DDI_DEMOD_LOG("DVB-S finish\n");
        DEMOD_MUTEX_UNLOCK();
        return OK;
    }

    if(M2R_DEBUG_ON==1)
        MSD_sw_info(FFT_CAPTURE_EN);

    if (DVBS_GetLock(DMD_DVBS_GETLOCK, 0)==TRUE)
        _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_FINISH;
    else
        _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_ING;
   
    
    if(KHAL_DEMOD_DVB_TU_FLAG_FINISH == _gSOC_DEMOD_FlagPostJob)
    {
        *pFinished = TRUE;
        DDI_DEMOD_LOG("DVB-S finish\n");
    }
    
    DEMOD_MUTEX_UNLOCK();
    return OK;

}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS2_TunePostJob(BOOLEAN *pFinished)
{
    UINT16  elapsedTime = 0;
    static UINT32 _startTime;
    static  UINT16  _maxDelay  = GBB_DVB_DVBC_LOCK_DELAY_DEFAULT;

    *pFinished = FALSE;
    
    DEMOD_MUTEX_LOCK();
    // DVBS demod:
    // 1.  FEC lock
    // 2.  time out
    // 1/2  ==> Tuning finish [Signal lock status is valid]
    
    if(KHAL_DEMOD_DVB_TU_FLAG_FINISH == _gSOC_DEMOD_FlagPostJob)
    {
        *pFinished = TRUE;
        
        DDI_DEMOD_LOG("Already Done !!!\n");
        DEMOD_MUTEX_UNLOCK();
    return OK;
}

    //_HAL_Demod_DVB_DVBS_TunePostJob();

    if(KHAL_DEMOD_DVB_TU_FLAG_START == _gSOC_DEMOD_FlagPostJob)
    {
        _startTime = (UINT32)jiffies_to_msecs(jiffies);//MAdp_SYS_GetSystemTime();
        //_maxDelay = GBB_DVB_DVBC_LOCK_DELAY_AUTO_DETECT;

        _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_ING;
    }

    if(DVBS_TR_indicator())
        _maxDelay=3000;
    else
        _maxDelay=1500;

    CURRENT_SYSTIME = (UINT32)jiffies_to_msecs(jiffies);//MAdp_SYS_GetSystemTime();
    elapsedTime = CURRENT_SYSTIME - _startTime;

    if (elapsedTime < _maxDelay)
    {
        _gSOC_DEMOD_FlagPostJob  = KHAL_DEMOD_DVB_TU_FLAG_ING;
    }
    else
    {
        _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_FINISH;
        *pFinished = TRUE;
        DDI_DEMOD_LOG("DVB-S finish\n");
        DEMOD_MUTEX_UNLOCK();
        return OK;
    }

    if(M2R_DEBUG_ON==1)
        MSD_sw_info(FFT_CAPTURE_EN);

    if (DVBS_GetLock(DMD_DVBS_GETLOCK, 0)==TRUE)
        _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_FINISH;
    else
        _gSOC_DEMOD_FlagPostJob = KHAL_DEMOD_DVB_TU_FLAG_ING;
   
    
    if(KHAL_DEMOD_DVB_TU_FLAG_FINISH == _gSOC_DEMOD_FlagPostJob)
    {
        *pFinished = TRUE;
        DDI_DEMOD_LOG("DVB-S finish\n");
    }
    
    DEMOD_MUTEX_UNLOCK();
    return OK;

}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_CheckLock(KHAL_DEMOD_LOCK_STATE_T *pLockState)
{
    //MAPI_BOOL IsDVBS2;

    DDI_DEMOD_LOG("!!!\n");

    DEMOD_MUTEX_LOCK();
    
    if(M2R_DEBUG_ON==1)
        MSD_sw_info(FFT_CAPTURE_EN);

    if(M2R_DEBUG_TS_CLK_ON==1)
        Ts_Clock_Setting_Set(M2R_DEBUG_TS_CLK_ON,TS_CLK_Val*1000000);
    
    if (DVBS_GetLock(DMD_DVBS_GETLOCK, 0)==TRUE)
    {
        *pLockState = KHAL_DEMOD_LOCK_OK;
        DDI_DEMOD_LOG("*pLockState = HAL_DEMOD_LOCK_OK;\n");
    }
    else
    {
        *pLockState = KHAL_DEMOD_LOCK_FAIL;
        DDI_DEMOD_LOG("*pLockState = HAL_DEMOD_LOCK_FAIL;\n");
    }

    DEMOD_MUTEX_UNLOCK();
    return OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_GetSignalState(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
    UINT16  status      = TRUE;
    UINT16  quality     = 0;
    UINT16 strength  =0;
    UINT16  pktErr      = 0;
    UINT32   snr         = 0;
    UINT32   ber         = 0;
    //MAPI_BOOL IsDVBS2;
    UINT32  fIFAGCGain   = 0.0;
    UINT16  if_agc_gain = 0xFFFF;

    DEMOD_MUTEX_LOCK();
    if (DVBS_GetLock(DMD_DVBS_GETLOCK, 0)==TRUE)
    {
        status &= DVBS_GetSignalQuality(&quality);
        status &= DVBS_GetPacketErr(&pktErr);
        status &= DVBS_GetSNR(&snr);
        status &= DVBS_GetPostViterbiBer(&ber);
        status &= DVBS_GetSignalStrength(&strength);
        status &= DVBS_IF_AGC(&if_agc_gain);
        fIFAGCGain = if_agc_gain/32768;
        
        pSignalState->bSignalValid      = TRUE;
        pSignalState->quality = (UINT8) quality;
        pSignalState->packetError       = (UINT32) pktErr;
        pSignalState->unAGC             = fIFAGCGain;
        pSignalState->unSNR              = snr;
        pSignalState->unBER              = ber;
        pSignalState->strength            = strength;
    }
    else
    {
        pSignalState->bSignalValid  = FALSE;
        pSignalState->quality = 0x00;
        pSignalState->packetError       = 0xFFFFFFFF;
        pSignalState->unBER             = 0xFFFFFFFF;
        pSignalState->unAGC             = 0xFFFFFFFF;
        pSignalState->unSNR             = 0xFFFFFFFF;
    }
    //printf("SQI(%d), PktErr(%d), SNR(%ddB))\n",pSignalState->quality, pSignalState->packetError);
    DDI_DEMOD_LOG("SQI(%d), SSI(%d), PktErr(%d), SNR(%ddB), BER(%d), AGC(%d)\n",quality, strength,pktErr, snr, ber, fIFAGCGain);

    DEMOD_MUTEX_UNLOCK();
    return OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_CheckSpecialData(KHAL_DEMOD_SPECDATA_DVBS_T *pSpecDVBS)
{
    UINT16 status =0;
    DMD_DVBS_CODE_RATE_TYPE code_rate=0;
    DMD_DVBS_MODULATION_TYPE QAM_mode;
    UINT32 symbol_rate=0;
    //_HAL_DEMOD_DVB_DVBS_CheckSpecialData(pSpecDVBS);
    KHAL_DEMOD_SPECDATA_DVBS_T    specDVBS;
    
    DEMOD_MUTEX_LOCK();
    pSpecDVBS->bSpectrumInv  = _gDVB_INFORMATION.bSpectrumInv;
    pSpecDVBS->constellation = _gDVB_INFORMATION.constellation;
    pSpecDVBS->symbolRate    = _gDVB_INFORMATION.symbolRate;
    pSpecDVBS->codeRate      = _gDVB_INFORMATION.codeRate;

    specDVBS.bIsDVBS2 =FALSE ;
    specDVBS.bSpectrumInv = FALSE;
    specDVBS.symbolRate = 0;
    specDVBS.codeRate = 0;
    specDVBS.constellation = 0;
    //MAPI_BOOL IsDVBS2 = FALSE;
    //mapi_demodulator_datatype_EN_DVBS_CODE_RATE demod_ext_cr;
    //mapi_demodulator_datatype_EN_DVBS_CONSTEL_TYPE demod_ext_constel;

    if (DVBS_GetLock(DMD_DVBS_GETLOCK, 0)==TRUE)
    {
        DDI_DEMOD_LOG("DVB-S HAL_DEMOD_LOCK_OK;\n");
    }
    else
    {
        DDI_DEMOD_LOG("DVB-S HAL_DEMOD_LOCK_FAIL;\n");
    }
    //DDI_DEMOD_LOG("IsDVBS2=%d\n",IsDVBS2);
    //specDVBS.bIsDVBS2=IsDVBS2;
    status&=INTERN_DVBS_GetCurrentDemodCodeRate(&code_rate);
    specDVBS.codeRate=code_rate;
    DDI_DEMOD_LOG("specDVBS.codeRate=%d\n",code_rate);
    status&=DVBS_GetCurrentModulationType(&QAM_mode);
    specDVBS.constellation =	QAM_mode;
    DDI_DEMOD_LOG("specDVBS.constellation=%d\n",QAM_mode);
    status&=INTERN_DVBS_GetCurrentSymbolRate(&symbol_rate);
    specDVBS.symbolRate =symbol_rate;
    DDI_DEMOD_LOG("specDVBS.symbolRate=%d\n",symbol_rate);
    *pSpecDVBS = specDVBS;
    
    DEMOD_MUTEX_UNLOCK();
    return OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_22Khz_Tone (BOOLEAN b22kOn)
{
    int status = OK;
    DEMOD_MUTEX_LOCK();
    status&=DVBS_DiSEqC_Set22kOnOff(b22kOn);
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_Send_Diseqc (UINT8* pCmd, UINT8 u8CmdSize)
{
    int status = OK;
    DEMOD_MUTEX_LOCK();
    status&=DVBS_DiSEqC_SendCmd(pCmd, u8CmdSize);
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_Send_Tone_Burst (BOOLEAN bTone1)
{
    int status = OK;
    DEMOD_MUTEX_LOCK();
    status&=DVBS_DiSEqC_SetTone(bTone1);
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_BlindScan_Init(UINT32 StartFreqMhz, UINT32 EndFreqMhz)
{
    int status = OK;
    DEMOD_MUTEX_LOCK();
    status&=DTV_DVB_S_BlindScan_Init(StartFreqMhz,EndFreqMhz);
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_BlindScan_GetTunerFreq(UINT32 *TunerCenterFreqMhz, UINT32 *TunerCutOffFreqKhz)
{
    UINT32 TunerCenterFreq=0,TunerCutOffFreq=0;
    int status = OK;
    DEMOD_MUTEX_LOCK();
    status&=DVBS_BlindScan_GetTunerFreq(&TunerCenterFreq, &TunerCutOffFreq);
    *TunerCenterFreqMhz=TunerCenterFreq;
    *TunerCutOffFreqKhz=TunerCutOffFreq;
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_BlindScan_NextFreq(BOOLEAN *bBlindScanEnd)
{
    BOOL ScanEnd=FALSE;
    int status = OK;
    DEMOD_MUTEX_LOCK();
    status&=DTV_DVB_S_BlindScan_ScanNextFreq(&ScanEnd);
    *bBlindScanEnd=ScanEnd;
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_BlindScan_GetStatus(UINT8 *Status, BOOLEAN *bBlindScanLock)
{
    U8 ScanLock=FALSE;
    mapi_demodulator_datatype_EN_BLINDSCAN_STATUS ScanStatus=mapi_demodulator_datatype_E_BLINDSCAN_NOTREADY;
    int status = OK;
    //U8 u8Progress;
    //u8ChannelNum;
    DEMOD_MUTEX_LOCK();
    status&=DTV_DVB_S_BlindScan_GetStatus(&ScanStatus,&ScanLock);
    *bBlindScanLock=ScanLock;
    *Status=ScanStatus;
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_BlindScan_End(void)
{
    int status = OK;
    DEMOD_MUTEX_LOCK();
    status&=DTV_DVB_S_BlindScan_End();
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_BlindScan_GetFoundTP(UINT32 *FreqMhz, UINT32 *SymbolRateKhz)
{
    UINT32 GetFreq=0,GetSymbolRate=0;
    int status = OK;
    DEMOD_MUTEX_LOCK();
    status&=DVBS_BlindScan_GetFoundTP(&GetFreq, &GetSymbolRate);
    *FreqMhz=GetFreq;
    *SymbolRateKhz=GetSymbolRate;
    DEMOD_MUTEX_UNLOCK();
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_GetPacketError(UINT32 *pPacketError)
{
    int status=OK;
    U16 pktErr;
    DEMOD_MUTEX_LOCK();
    status = DVBS_GetPacketErr(&pktErr);
    *pPacketError=pktErr;
    DEMOD_MUTEX_UNLOCK();   
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_GetSQI(UINT8 *pSQI)
{
     int status=OK;
     U16 quality;
     DEMOD_MUTEX_LOCK();
     status = DVBS_GetSignalQuality(&quality);
     *pSQI=quality;
     DEMOD_MUTEX_UNLOCK();   
     return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_GetBER(UINT32 *pBER)
{
    int status=OK;
    UINT32 postber;
    DEMOD_MUTEX_LOCK();
    status = DVBS_GetPostViterbiBer(&postber);
    *pBER=postber;
    DEMOD_MUTEX_UNLOCK(); 
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_GetAGC(UINT32 *pAGC)
{
    int status=OK;
    U16 ifagc;
    DEMOD_MUTEX_LOCK();
    status = DVBS_IF_AGC(&ifagc);
    *pAGC=ifagc;
    DEMOD_MUTEX_UNLOCK(); 
    return status;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
int KHAL_DEMOD_DVB_DVBS_GetSNR (UINT32 *pSNR)
{
    int status=OK;
    UINT32 f_snr;
    DEMOD_MUTEX_LOCK();
    status = DVBS_GetSNR (&f_snr);
    *pSNR=f_snr;
    DEMOD_MUTEX_UNLOCK(); 
    return status;
}

/* 4.12
 * SOC_DEMOD debug
 *
 * @param	void
 * @return	OK/FAIL
 * @author
*/
int KHAL_DEMOD_DVB_DebugMenu (void)
{
#if 0 // disable unused DebugMenu for pass coverity
    UINT8 index = 0;
    UINT32 command = 0x00;
    UINT8  i=0, j=0;
    UINT32 bank=0, debug_status=0, FFT_debug_status=0;
    //UINT32 packet_error_time;
    UINT16 data16=0;
    UINT32 u32tmp;
    //SINT32 arg1, arg2;
    BOOL ts_source=0;
    U32 ts_rate, ftmp, TS_CLK_Val_ori;
    UINT8 IS_ID_Table[32];
    UINT8 IS_ID = 0;
    char *pDebugMessage[] = {
        "",
        "",
        "[TU  DBG] ************************************************************",
        "[TU  DBG]    Demod : SOC_DEMOD",
        "[TU  DBG] ************************************************************",
        "[TU  DBG] *",
        "[TU  DBG] *    0x01: Enable debug message",
        "[TU  DBG] *    0x02: Disable debug message",
        "[TU  DBG] *    0x03: Read demod register",
        "[TU  DBG] *    0x04: Setting DVBS debug information",
        "[TU  DBG] *    0x05: Start/Stop DVBS PktErr monitor thread",
        "[TU  DBG] *    0x06: DVBS/S2 Get current TS clock rate",
        "[TU  DBG] *    0x07: DVBS/S2 Set TS clock rate",
        "[TU  DBG] *    0x08: DVBT/C Get current TS clock rate",
        "[TU  DBG] *    0x09: DVBT/C Set TS clock rate",
        "[TU  DBG] *    0x0a: Start/Stop PktErr monitor thread",
        "[TU  DBG] *    0x0b: Start/Stop locking time test thread",
        "[TU  DBG] *    0x0c: DVBS/S2 Show CFO value",
        "[TU  DBG] *    0x0d: Test KHAL One",
        "[TU  DBG] *    0x0e: Test KHAL Two",
#if 0
        "[TU  DBG] *    0x03: MSPI RIU Read Byte(x16)",
        "[TU  DBG] *    0x04: RIU Write Byte",
        "[TU  DBG] *    0x05: RIU Read Byte(x16)",
        "[TU  DBG] *    0x06: Read DSPReg Byte(x16)",
        "[TU  DBG] *    0x07: RIU Continue Read Byte",
        "[TU  DBG] *    0x08: GetSignalQuality",
        "[TU  DBG] *    0x09: TPS Info",
        "[TU  DBG] *    0x0a: GetPacketErr",
        "[TU  DBG] *    0x0b: Reset External Demod",
        "[TU  DBG] *    0x0c: Write DSPReg Byte",
        "[TU  DBG] *",
#endif
        "[TU  DBG] *    0xff    EXIT",
        "[TU  DBG] *",
        "[TU  DBG] ************************************************************",
        "",
    };

    while(1)
    {
        for (index = 0; index < sizeof(pDebugMessage)/sizeof(char *); index++)
        {
            printk("%s\n", pDebugMessage[index]);
        }

        printk("===> Select Menu: 0x");
#if 0 // TODO: command line input
        if (scanf("%x", &command) <= 0)
            continue;
#endif

        switch (command)
        {
            case 0x01:
                sg_fPrintDbg = TRUE;
                printk("KHAL_DVB_DEMOD print on\n");
                return OK;

            case 0x02:
                sg_fPrintDbg = FALSE;
                printk("KHAL_DVB_DEMOD print off\n");
                return OK;

            case 0x03:
                printk("===> Select bank: 0x");
                #if 0 // TODO: command line input
                if (scanf("%x", &bank) <= 0)
                    continue;
                #endif
                printk("bank 0x%x\n",bank);
                printk("       00      01      02      03      04      05      06      07      08      09      0A      0B      0C      0D      0E      0F");
                for(j=0 ; j<8 ; j++)
                {
                    printk("\n%d0    ",j);
                    for(i=0 ; i<16 ; i++)
                    {
                        DVBS_ReadReg2bytes((bank<<8)+i*2+j*32, &data16);
                        if(data16<=0xF)
                            printk("000%x    ",data16);
                        else if(data16<=0xFF)
                            printk("00%x    ",data16);
                        else if(data16<=0xFFF)
                            printk("0%x    ",data16);
                        else
                            printk("%x    ",data16);
                    }
                }
                break;
            
            case 0x04:
                printk("DVBS debug information\n");
                printk("==> M2R_DEBUG_ON= \n");
                #if 0 // TODO: command line input
                if (scanf("%x", &debug_status) <= 0)
                    break;
                #endif
                M2R_DEBUG_ON=debug_status;

                printk("==> FFT_CAPTURE_EN= \n");
                #if 0 // TODO: command line input
                if (scanf("%x", &FFT_debug_status) <= 0)
                    break;
                #endif
                FFT_CAPTURE_EN=FFT_debug_status;
                printk("M2R_DEBUG_ON=%d\n",M2R_DEBUG_ON);
                printk("FFT_CAPTURE_EN=%d\n",FFT_CAPTURE_EN);
                break;
            case 0x05:
                printk("===> Reset monitor thread (1:Enable, 0:Disable)\n");
                #if 0 // TODO: command line input
                if (scanf("%d", &u32tmp) <= 0)
                    continue;
                #endif
                if (u32tmp == 0)
                {
                    //pkterr_monitor_processor_stop();
                }
                else
                {
                    printk("===> Enter watch period (Second): ");
                    #if 0 // TODO: command line input
                    if (scanf("%d", &u32tmp) <= 0)
                        u32tmp = 1;
                    #endif
                    pkterr_monitor_period = 1000 * u32tmp;
                    printk("pkterr_monitor_period=%d\n",pkterr_monitor_period);
                    //pkterr_monitor_processor_start_dvbs(0);
                }
                break;
            case 0x06:
                printk("DVBS/S2 Get current TS clock rate\n");
                printk("==> TS Clock setting DIV number= \n");

                Ts_Clock_Setting_Get(&TS_CLK_Val_ori,&ts_source);
                
                if (ts_source==1)
                    printk("Current TS source is 432M\n");
                else
                    printk("Current TS source is 288M\n");
                TS_CLK_Val_ori /= 1000;
                printk("Current TS clock rate %d KHz\n",TS_CLK_Val_ori);
                break;
            case 0x07:
                printk("===> Enter target rate (MHz): \n");
                M2R_DEBUG_TS_CLK_ON=1;
                #if 0 // TODO: command line input
                if (scanf("%f", &ftmp) <= 0)
                    break;
                #endif
                TS_CLK_Val=ftmp;        
                ts_rate =Ts_Clock_Setting_Set(M2R_DEBUG_TS_CLK_ON,ftmp* 1000000.0);
                ts_rate /= 1000000;
                printk("TS clock rate set to %d MHz\n",ts_rate);
                printk("==> M2R_DEBUG_TS_CLK_OFF(please set 0 if you want to disable Debug TS CLK mode)= \n");
                #if 0 // TODO: command line input
                if (scanf("%x", &debug_status) <= 0)
                    continue;
                #endif
                M2R_DEBUG_TS_CLK_ON=debug_status;
                break;

            case 0x08:
                //MST260A_GetTSClkRate(&ts_rate);
                //ts_rate /= 1000000.0;
                //printf("Current TS clock rate %.3f MHz\n", ts_rate);
                break;

            case 0x09:
                //printf("===> Enter target rate (MHz): ");
                //if (scanf("%f", &ftmp) <= 0)
                //    continue;
                //ts_rate = MST260A_SetTSClkRate(ftmp * 1000000.0);
                //ts_rate /= 1000000.0;
                //printf("TS clock rate set to %.3f MHz\n", ts_rate);
                break;

            case 0x0a:
                printk("===> Reset monitor thread (1:Enable, 0:Disable)\n");
                //if (scanf("%d", &u32tmp) <= 0)
                //    continue;
                if (u32tmp == 0)
                {
                    //pkterr_monitor_processor_stop();
                }
                else
                {
                    printk("===> Enter watch period (Second): ");
                    //if (scanf("%d", &u32tmp) <= 0)
                    //    u32tmp = 1;
                    //pkterr_monitor_period = 1000 * u32tmp;
                    //pkterr_monitor_processor_start(0);
                }
                break;

            case 0x0b:
                printk("===> Reset locking time test thread (1:Enable, 0:Disable)\n");
                //if (scanf("%d", &u32tmp) <= 0)
                //    continue;
                if (u32tmp == 0)
                {
                    //locking_time_test_processor_stop();
                }
                else
                {
                    i = u32tmp & 0xff;
                    printk("===> Enter test count (times): ");
                    //if (scanf("%d", &u32tmp) <= 0)
                    //    u32tmp = 1;
                    //locking_time_test_count = u32tmp;
                    //locking_time_test_processor_start(i);
                }
                break;

             case 0x0c:
                printk("===> Show DVBS CFO value\n");
                M2R_DEBUG_CFO_ON=1;
                CFO_Show(M2R_DEBUG_CFO_ON);
                break;

            case 0x0d:
                printk("===> check vcm signal\n");
                if(DVBS2_VCM_CHECK())
                    printk("===> vcm signal\n");
                else
                    printk("===> ccm signal\n");
                break;

            case 0x0e:
                printk("===> get ISID table\n");
                DVBS2_Get_IS_ID_INFO(&IS_ID, IS_ID_Table);
                for(i=0 ; i>32 ; i++)
                {
                    printk("IS_ID_Table [%d] = %d", i , IS_ID_Table[i]);
                }
                break;   
            case 0x0f:
                printk("===> set ISID\n");
                //if (scanf("%d", &u32tmp) <= 0)
                //    continue;
                DVBS2_Set_IS_ID(u32tmp);
                break;

            case 0xff:
                printk("\n");
                M2R_DEBUG_TS_CLK_ON=0;
                return OK;

            default:
                continue;
        }
    }
#endif

    return OK;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
static int _KHAL_DEMOD_DVB_Suspend(void)
{
    DDI_DEMOD_LOG("_gDVB_CurrenType=%d\n", _gDVB_CurrenType);
    DEMOD_MUTEX_LOCK();

    _gDVB_ResumeType = _gDVB_CurrenType;
    if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_UNKNOWN))
    {
        printk("[%s %d] Set VDMcuHandler Error\n", __FUNCTION__, __LINE__);
    }
    _gDVB_CurrenType = E_SYS_UNKOWN;

    _gDVB_STRState = KHAL_DEMOD_DVB_STR_SLEEP;
    DEMOD_MUTEX_UNLOCK();
    return 0;
}

/*
* Description:
* Syntax
* Parameters:
* Return Value:
*/
static int _KHAL_DEMOD_DVB_Resume(void)
{
    DDI_DEMOD_LOG("_gDVB_ResumeType=%d\n", _gDVB_ResumeType);
    DEMOD_MUTEX_LOCK();

    switch(_gDVB_ResumeType)
    {
        case E_SYS_DVBT:
            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT))
            {
                printk("[%s %d] Set VDMcuHandler Error\n", __FUNCTION__, __LINE__);
                break;
            }
            if (OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT))
            {
                printk("[%s %d] LoadDSPCode Error\n", __FUNCTION__, __LINE__);
                break;
            }
            _gDVB_CurrenType = E_SYS_DVBT;
            break;

        case E_SYS_DVBC:
            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBC))
            {
                printk("[%s %d] Set VDMcuHandler Error\n", __FUNCTION__, __LINE__);
                break;
            }
            if (OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBC))
            {
                printk("[%s %d] LoadDSPCode Error\n", __FUNCTION__, __LINE__);
                break;
            }
            _gDVB_CurrenType = E_SYS_DVBC;
            break;

        case E_SYS_DVBT2:
            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT2))
            {
                printk("[%s %d] Set VDMcuHandler Error\n", __FUNCTION__, __LINE__);
                break;
            }
            if (OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT2))
            {
                printk("[%s %d] LoadDSPCode Error\n", __FUNCTION__, __LINE__);
                break;
            }
            _gDVB_CurrenType = E_SYS_DVBT2;
            break;

        case E_SYS_DVBS:
            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBS))
            {
                printk("[%s %d] Set VDMcuHandler Error\n", __FUNCTION__, __LINE__);
                break;
            }
            if (OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBS))
            {
                printk("[%s %d] LoadDSPCode Error\n", __FUNCTION__, __LINE__);
                break;
            }
            _gDVB_CurrenType = E_SYS_DVBS;
            break;

        case E_SYS_DVBT_T2:
            if (FALSE == MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DVBT2))
            {
                printk("[%s %d] Set VDMcuHandler Error\n", __FUNCTION__, __LINE__);
                break;
            }
            if (OK != _KHAL_Demod_DVB_LoadDSPCode(E_SYS_DVBT_T2))
            {
                printk("[%s %d] LoadDSPCode Error\n", __FUNCTION__, __LINE__);
                break;
            }
            _gDVB_CurrenType = E_SYS_DVBT_T2;
            break;

        default:
            _gDVB_CurrenType = E_SYS_UNKOWN;
            break;
    }

    _gDVB_STRState = KHAL_DEMOD_DVB_STR_AWAKE;
    DEMOD_MUTEX_UNLOCK();
    return 0;
}

