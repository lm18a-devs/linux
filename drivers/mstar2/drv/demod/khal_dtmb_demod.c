//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all
// or part of MStar Software is expressly prohibited, unless prior written
// permission has been granted by MStar.
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software.
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s
//    confidential information in strictest confidence and not disclose to any
//    third party.
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
/**********************************************************************
 Copyright (c) 2006-2010 MStar Semiconductor, Inc.
 All rights reserved.

 Unless otherwise stipulated in writing, any and all information contained
 herein regardless in any format shall remain the sole proprietary of
 MStar Semiconductor Inc. and be kept in strict confidence
 (MStar Confidential Information) by the recipient.
 Any unauthorized act including without limitation unauthorized disclosure,
 copying, use, reproduction, sale, distribution, modification, disassembling,
 reverse engineering and compiling of the contents of MStar Confidential
 Information is unlawful and strictly prohibited. MStar hereby reserves the
 rights to any and all damages, losses, costs and expenses resulting therefrom.

* Class :
* File  :
**********************************************************************/
/******************************************************************************
    File Inclusions
******************************************************************************/

#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/time.h>
#include <linux/delay.h>

#include <asm/setup.h>

#include "drvDMD_DTMB.h"
#include "mhal_demod.h"
#include "mdrv_types.h"
#include "khal_demod_dtmb.h"
#include "khal_demod_common.h"
#include "mdrv_system.h"
#include "mhal_demod_reg.h"
#include "mdrv_system_LG.h"
/******************************************************************************
    Macro Definitions
******************************************************************************/




/******************************************************************************
    Extern Variables & Function Prototype Declarations
******************************************************************************/

extern struct tag_custom customTAG;

/******************************************************************************
    Local Constant Definitions
******************************************************************************/



/******************************************************************************
    Local Type Definitions
******************************************************************************/



/******************************************************************************
    Global Type Definitions
******************************************************************************/



/******************************************************************************
    Static Variables & Function Prototypes Declarations
******************************************************************************/

static KHAL_DEMOD_LOCK_STATE_T MSB_ConvertLockState(EN_LOCK_STATUS tunerLock);
static EN_LOCK_STATUS MSB_CheckDTMBLock(void);
static MS_U32 MSB_Get_System_Time(void);
static void MSB_Delay_MS(MS_U32 delay_time);
static MS_BOOL MSB_Cretate_mutex(MS_BOOL on);
static void MSB_LockDMD(MS_BOOL LOCK);
static int MSB_Demod_DTMB_Suspend(void);
static int MSB_Demod_DTMB_Resume(void);

/******************************************************************************
    Global Variables & Function Prototypes Declarations
******************************************************************************/



/******************************************************************************
    Local Variables & Function Prototypes Declarations
******************************************************************************/



/******************************************************************************
    Function Definitions
******************************************************************************/

/**=========================================================================**/
/**         F U N C T I O N S                                               **/
/**=========================================================================**/

static KHAL_DEMOD_LOCK_STATE_T MSB_ConvertLockState(EN_LOCK_STATUS tunerLock)
{
    if (E_DEMOD_LOCK == tunerLock)
        return KHAL_DEMOD_LOCK_OK;
    else if (E_DEMOD_CHECKING == tunerLock)
        return KHAL_DEMOD_LOCK_FAIL;
    else if (E_DEMOD_UNLOCK == tunerLock)
        return KHAL_DEMOD_LOCK_FAIL;
    else
        return KHAL_DEMOD_LOCK_FAIL;
}

static EN_LOCK_STATUS MSB_CheckDTMBLock(void)
{
    BOOLEAN hasDtmbSignal = FALSE, fecLock=FALSE;
    DMD_DTMB_GETLOCK_TYPE sDMD_DTMB_GETLOCK_TYPE;

    sDMD_DTMB_GETLOCK_TYPE = DMD_DTMB_GETLOCK_DTMB_PNPLOCK;
    if(_MDrv_DMD_DTMB_MD_GetLock(0, sDMD_DTMB_GETLOCK_TYPE) == DMD_DTMB_LOCK)
        hasDtmbSignal = TRUE;
    sDMD_DTMB_GETLOCK_TYPE = DMD_DTMB_GETLOCK_DTMB_FECLOCK;
    if(_MDrv_DMD_DTMB_MD_GetLock(0, sDMD_DTMB_GETLOCK_TYPE) == DMD_DTMB_LOCK)
        fecLock = TRUE;

    if(fecLock == TRUE)
       return E_DEMOD_LOCK;

    if(hasDtmbSignal == TRUE)
       return E_DEMOD_CHECKING;
    else return E_DEMOD_UNLOCK;
}
static U8 MSB_CheckDTMBStatus(void)
{
    U8 u8Data=0;
    _MDrv_DMD_DTMB_MD_GetReg(0, 0x20C1, &u8Data);
    return u8Data;
}

static MS_U32 MSB_Get_System_Time(void)
{
    static struct timeval curr;
    
    do_gettimeofday(&curr);

    return (curr.tv_sec*1000 + curr.tv_usec/1000);
}

static void MSB_Delay_MS(MS_U32 delay_time)
{
    mdelay(delay_time);
}

static MS_BOOL MSB_Cretate_mutex(MS_BOOL on)
{
    if (on)
        MHal_Demod_Init();
    else MHal_Demod_Exit();

    return TRUE;
}

static void MSB_LockDMD(MS_BOOL LOCK)
{
    if (LOCK)
        MHal_Demod_Mutex_Lock();
    else MHal_Demod_Mutex_Unlock();
}
static int MSB_Demod_DTMB_Suspend(void)
{
    return _MDrv_DMD_DTMB_MD_SetPowerState(0,E_POWER_SUSPEND);
}
static int MSB_Demod_DTMB_Resume(void)
{
    return _MDrv_DMD_DTMB_MD_SetPowerState(0,E_POWER_RESUME);
}

UINT32 KHAL_DEMOD_VQI_DTMB_Probe(struct i2c_client *client)
{
    return (OK);
}

int KHAL_DEMOD_VQI_DTMB_GetFWVersion(UINT32 *pFWVersion)
{
    MS_U8 data1 = 0;
    MS_U8 data2 = 0;
    MS_U8 data3 = 0;
    
    _MDrv_DMD_DTMB_MD_GetReg(0,0x20C4, &data1);
    _MDrv_DMD_DTMB_MD_GetReg(0,0x20C5, &data2);
    _MDrv_DMD_DTMB_MD_GetReg(0,0x20C6, &data3);
    PRINT("kavana _MDrv_DMD_DTMB_MD_GetReg = %x.%x!!\n",data2,data3);
    *pFWVersion = data2 << 8 | data3;
    return (OK);
}
/*
 * Initializes the specific demodulator
*/
int KHAL_DEMOD_VQI_DTMB_Initialize(void)
{
    return (OK);
}
/**
 *Changes demodulator setting according to transmissi media.
*/
int KHAL_DEMOD_VQI_DTMB_ChangeTransSystem(KHAL_DEMOD_TRANS_SYSTEM_T transSystem)
{
    DMD_DTMB_InitData sDMD_DTMB_InitData;
    PRINT("kenji KHAL_DEMOD_VQI_DTMB_ChangeTransSystem = %d!!\n",transSystem);
    if (transSystem == KHAL_DEMOD_TRANS_SYS_DTMB)
    {
        
        U32 phyaddr=0, blksize=0;
        U32 u32MIUInterval;
        U8  udatatemp = 0x00;
        
        memset(&sDMD_DTMB_InitData, 0, sizeof(DMD_DTMB_InitData));
        
        sDMD_DTMB_InitData.u16DTMBAGCLockCheckTime   = 50;
        sDMD_DTMB_InitData.u16DTMBPreLockCheckTime   = 300;
        sDMD_DTMB_InitData.u16DTMBPNMLockCheckTime   = 500;
        sDMD_DTMB_InitData.u16DTMBFECLockCheckTime   = 5000;
        
        sDMD_DTMB_InitData.u16QAMAGCLockCheckTime    = 50;
        sDMD_DTMB_InitData.u16QAMPreLockCheckTime    = 1000;
        sDMD_DTMB_InitData.u16QAMMainLockCheckTime   = 3000;
        
        // register init 
        sDMD_DTMB_InitData.u8DMD_DTMB_DSPRegInitExt  = NULL; // TODO use system variable type
        sDMD_DTMB_InitData.u8DMD_DTMB_DSPRegInitSize = 0;
        sDMD_DTMB_InitData.u8DMD_DTMB_InitExt        = NULL;
        
        //By Tuners:
        sDMD_DTMB_InitData.u16IF_KHZ                 = DTMB_TUNER_IF;
        sDMD_DTMB_InitData.bIQSwap                   = DTMB_IQ_SWAP;
        sDMD_DTMB_InitData.u16AGC_REFERENCE          = DTMB_IFAGC_REF;
        sDMD_DTMB_InitData.bTunerGainInvert          = 0;
        
        //By IC:
        sDMD_DTMB_InitData.u8IS_DUAL                 = 0;
        sDMD_DTMB_InitData.bIsExtDemod               = 0;
        
        sDMD_DTMB_InitData.u5TsConfigByte_DivNum     = DTMB_TS_CLK_DIVNUM;
        sDMD_DTMB_InitData.u1TsConfigByte_ClockInv   = DTMB_TS_CLK_INV;
        sDMD_DTMB_InitData.u1TsConfigByte_DataSwap   = DTMB_TS_DATA_SWAP;
        sDMD_DTMB_InitData.u1TsConfigByte_SerialMode = DTMB_TS_SERIAL;
        
        u32MIUInterval = MS_MIU_INTERVAL;
        phyaddr = customTAG.mmapger[MMAP_INDEX_DTMB_TDI][MMAP_INDEX_MEM_ADDR];
        blksize = customTAG.mmapger[MMAP_INDEX_DTMB_TDI][MMAP_INDEX_MEM_LEN];
        
        if (blksize < 0x500000)
        {
            PRINT("Error TDI DRAM size!!![%d]\n", blksize);
            return (NOT_OK);
        }
        if (MHal_Demod_VDMcuHandler(DEMOD_STANDARD_DTMB))
            PRINT("kavana change DEMOD_STANDARD_DTMB !!!\n");
        udatatemp = _MHal_R1B(0x112000);
        if (u32MIUInterval & phyaddr)
        {
            _MHal_W1B(0x112000,(udatatemp|0x03)); // miu1
            phyaddr &= ~u32MIUInterval;
        }
        else _MHal_W1B(0x112000,(udatatemp&~0x03)); // miu0
        
        PRINT("TDI address: %x\n", phyaddr);
        
        sDMD_DTMB_InitData.u32TdiStartAddr = ((~u32MIUInterval & phyaddr)/ 16);
        
        sDMD_DTMB_InitData.GetSystemTimeMS           = MSB_Get_System_Time;
        sDMD_DTMB_InitData.DelayMS                   = MSB_Delay_MS;
        sDMD_DTMB_InitData.CreateMutex               = MSB_Cretate_mutex;
        sDMD_DTMB_InitData.LockDMD                   = MSB_LockDMD;
        
        if (_MDrv_DMD_DTMB_MD_Init(0, &sDMD_DTMB_InitData, sizeof(sDMD_DTMB_InitData)) != TRUE)
        {
            PRINT("Demod FAIL! () MDrv_DMD_DTMB_MD_Init !!!\n");
        
            return (NOT_OK);
        }
        MSB_LockDMD(TRUE);    
        SetDemodSTRHandle(MSB_Demod_DTMB_Suspend,MSB_Demod_DTMB_Resume);    
        MSB_LockDMD(FALSE);
    }  
    else 
    {
        if (MHal_Demod_VDMcuHandler(DEMOD_STANDARD_UNKNOWN))
            PRINT("kavana change SYSTEM_TYPE_UNKNOWN !!!\n");
    }     
    return (OK);
}
/*
 * Set Demod. with the specified parameters
*/
int KHAL_DEMOD_VQI_DTMB_SetDemod(KHAL_DEMOD_DTMB_SET_PARAM_T paramStruct)
{
    DMD_DTMB_DEMOD_TYPE sDMD_DTMB_DEMOD_TYPE = DMD_DTMB_DEMOD_DTMB;
//    PRINT("kavana check KHAL_DEMOD_VQI_ATSC_SetDemod _gconstMode= %d!!\n",paramStruct.constellation);
    PRINT("kavana check KHAL_DEMOD_VQI_ATSC_SetDemod _gtransSystem = %d!!\n",paramStruct.transSystem);
    if (_MDrv_DMD_DTMB_MD_SetConfig(0, sDMD_DTMB_DEMOD_TYPE, TRUE) == FALSE)
    {
        PRINT("^r^%s[TU DTV] FAIL () MDrv_DMD_DTMB_MD_SetConfig !!!\n", __FUNCTION__);
        
        return (NOT_OK);
    }
    
    return (OK);
}
/*
 * Check the Demod. lock status after demod stting.
 * If signal lock status is valid, set *pFishisd value as true.
*/
int KHAL_DEMOD_VQI_DTMB_TunePostJob(BOOLEAN *pFinished)
{
    DMD_DTMB_GETLOCK_TYPE sDMD_DTMB_GETLOCK_TYPE;

    sDMD_DTMB_GETLOCK_TYPE = DMD_DTMB_GETLOCK;
    if(_MDrv_DMD_DTMB_MD_GetLock(0, sDMD_DTMB_GETLOCK_TYPE) == DMD_DTMB_CHECKING)
       *pFinished=FALSE;
    else
       *pFinished=TRUE;
    printk(KERN_DEBUG "\n\rDTMB for DEBUG state=0x%x\n",MSB_CheckDTMBStatus());    
    return (OK);
}

int KHAL_DEMOD_VQI_DTMB_CheckLock(KHAL_DEMOD_LOCK_STATE_T *pLockState)
{
    EN_LOCK_STATUS tunerLock = E_DEMOD_NULL;

   // PRINT("[DTV] KHAL_DEMOD_VQI_DTMB_CheckLockState !!!\n");

    tunerLock = MSB_CheckDTMBLock();

    if (E_DEMOD_LOCK != tunerLock)
        PRINT("[DTV] KHAL_DEMOD_DTMB_CheckLockState !!! tunerLock = 0x%X\n", tunerLock);

    *pLockState = MSB_ConvertLockState(tunerLock);
    printk(KERN_DEBUG "\n\rDTMB for DEBUG state=0x%x\n",MSB_CheckDTMBStatus());
    return (OK);
}

int KHAL_DEMOD_VQI_DTMB_CheckSpecialData(KHAL_DEMOD_SPECDATA_DTMB_T *pSpecDTMB)
{
    KHAL_DEMOD_SPECDATA_DTMB_T specDTMB;
    DMD_DTMB_MODULATION_INFO sDMD_DTMB_MODULATION_INFO;
    
    /* CheckTuner Lock state */
    if (E_DEMOD_LOCK != MSB_CheckDTMBLock())
        return (NOT_OK);

    //Get Carrier mode
    //bit 3 ~ bit 0: code rate
    //bit 7 ~ bit 4: Guard Interval
    //bit 11 ~ bit 8: Interleaving
    //bit 15 ~ bit 12: Constellation
    //bit 19 ~ bit 16: Carrier mode
    if (FALSE == _MDrv_DMD_DTMB_MD_GetReg(0, 0x11BB, (MS_U8 *)&sDMD_DTMB_MODULATION_INFO.u16PNM))
        return (NOT_OK);
    sDMD_DTMB_MODULATION_INFO.u16PNM = sDMD_DTMB_MODULATION_INFO.u16PNM&0x03;
    if (FALSE == _MDrv_DMD_DTMB_MD_GetReg(0, 0x11BA, &sDMD_DTMB_MODULATION_INFO.u8PNC))
        return (NOT_OK);
    if (_MDrv_DMD_DTMB_MD_GetModulationMode(0, &sDMD_DTMB_MODULATION_INFO) != TRUE)
        return (NOT_OK);
    //  code rate
    if (sDMD_DTMB_MODULATION_INFO.fSiCodeRate == 4)
        specDTMB.codeRate = KHAL_DEMOD_TPS_CODE_2_5;   //0.4
    else if(sDMD_DTMB_MODULATION_INFO.fSiCodeRate == 6)
        specDTMB.codeRate = KHAL_DEMOD_TPS_CODE_3_5;   //0.6
    else if(sDMD_DTMB_MODULATION_INFO.fSiCodeRate == 8)
        specDTMB.codeRate = KHAL_DEMOD_TPS_CODE_4_5;   //0.8
    else
        return (NOT_OK);
    // Interleaving
    if (sDMD_DTMB_MODULATION_INFO.u8SiInterLeaver == 720)
        specDTMB.bM720 = TRUE;
    else if(sDMD_DTMB_MODULATION_INFO.u8SiInterLeaver == 240)
        specDTMB.bM720 = FALSE;
    else
        return (NOT_OK);
    // Guard Interval
    if ((sDMD_DTMB_MODULATION_INFO.u16PNM == 0) && (sDMD_DTMB_MODULATION_INFO.u8PNC == 0)) //PN Mode: 420sDMD_DTMB_MODULATION_INFO.u8PNC = 0 var
        specDTMB.guardInterval = KHAL_DEMOD_TPS_GUARD_420_V;
    else if ((sDMD_DTMB_MODULATION_INFO.u16PNM == 0) && (sDMD_DTMB_MODULATION_INFO.u8PNC == 1))
        specDTMB.guardInterval = KHAL_DEMOD_TPS_GUARD_420_C;
    else if (sDMD_DTMB_MODULATION_INFO.u16PNM == 1)
        specDTMB.guardInterval = KHAL_DEMOD_TPS_GUARD_595;
    else if ((sDMD_DTMB_MODULATION_INFO.u16PNM == 2) && (sDMD_DTMB_MODULATION_INFO.u8PNC == 0))
        specDTMB.guardInterval = KHAL_DEMOD_TPS_GUARD_945_V;
    else if ((sDMD_DTMB_MODULATION_INFO.u16PNM == 2) && (sDMD_DTMB_MODULATION_INFO.u8PNC == 1))
        specDTMB.guardInterval = KHAL_DEMOD_TPS_GUARD_945_C;
    else
        return (NOT_OK);
    //Constellation
    if (sDMD_DTMB_MODULATION_INFO.u8SiQamMode == 4)
        specDTMB.constellation = KHAL_DEMOD_TPS_CONST_QAM_4;
    else if(sDMD_DTMB_MODULATION_INFO.u8SiQamMode == 16)
        specDTMB.constellation = KHAL_DEMOD_TPS_CONST_QAM_16;
    else if(sDMD_DTMB_MODULATION_INFO.u8SiQamMode == 32)
        specDTMB.constellation = KHAL_DEMOD_TPS_CONST_QAM_32;
    else if(sDMD_DTMB_MODULATION_INFO.u8SiQamMode == 64)
        specDTMB.constellation = KHAL_DEMOD_TPS_CONST_QAM_64;
    else
        return (NOT_OK);
    // Carrier mode
    if (sDMD_DTMB_MODULATION_INFO.u8SiCarrierMode == 1)
        specDTMB.carrierMode = KHAL_DEMOD_TPS_CARR_SC;
    else if (sDMD_DTMB_MODULATION_INFO.u8SiCarrierMode == 0)
        specDTMB.carrierMode = KHAL_DEMOD_TPS_CARR_MC;
    else
        return (NOT_OK);
    
    *pSpecDTMB = specDTMB;

    return (OK);
}

int KHAL_DEMOD_VQI_DTMB_CheckSignalStatus(KHAL_DEMOD_SIGNAL_STATE_T *pSignalState)
{
    KHAL_DEMOD_SIGNAL_STATE_T signalState;
    DMD_DTMB_SNR_DATA sDMD_DTMB_SNR_DATA;
    DMD_DTMB_BER_DATA sDMD_DTMB_BER_DATA;
    BOOLEAN u8Status = TRUE;

    signalState.bSignalValid = FALSE;
    signalState.unSNR        = 0xFFFFFFFF;
    signalState.unBER        = 0xFFFFFFFF;
    signalState.packetError  = 0xFFFFFFFF;
    signalState.unAGC        = 0xFFFFFFFF;
    signalState.strength     = 0;
    signalState.quality      = 0;
    //signalState.unSQI        = 0;

   // PRINT("[KHAL_DEMOD_DTMB_CheckSignalState] (%s:%d)\n", __F__, __L__);
    if (E_DEMOD_LOCK != MSB_CheckDTMBLock())
    {
        u8Status = FALSE;
    }
    else
    {
        u8Status &= _MDrv_DMD_DTMB_MD_GetSignalStrength(0, (MS_U16 *)&signalState.strength);
        u8Status &= _MDrv_DMD_DTMB_MD_GetSNR(0, &sDMD_DTMB_SNR_DATA);
        u8Status &= _MDrv_DMD_DTMB_MD_GetPreLdpcBer(0, &sDMD_DTMB_BER_DATA);
        
        signalState.bSignalValid = TRUE;
        //signalState.quality      = _MDrv_DMD_DTMB_MD_GetSignalQuality(0);
        signalState.unSNR        = _MDrv_DMD_DTMB_MD_GetSignalQuality(0);
        signalState.unSNR        = signalState.unSNR*3/10;
        signalState.unBER        = sDMD_DTMB_BER_DATA.BitErr;
        signalState.packetError  = 0;
        signalState.unAGC        = 0xFFFFFFFF;
    }

    if (u8Status != TRUE)
    {
        signalState.bSignalValid = FALSE;
        signalState.unSNR        = 0xFFFFFFFF;
        signalState.unBER        = 0xFFFFFFFF;
        signalState.packetError  = 0xFFFFFFFF;
        signalState.unAGC        = 0xFFFFFFFF;
        signalState.strength     = 0;
        signalState.quality      = 0;
    }
   // PRINT("kavana MSB_GetSignalState signalState.unSNR = %d!!\n",signalState.unSNR);
    *pSignalState = signalState;

     return (OK);
}

int KHAL_DEMOD_VQI_DTMB_CheckFrequencyOffset(SINT32 *pFreqOffset)
{
    DMD_DTMB_CFO_DATA sDMD_DTMB_CFO_DATA;
	  
    _MDrv_DMD_DTMB_MD_ReadFrequencyOffset(0,&sDMD_DTMB_CFO_DATA);

    *pFreqOffset = (sDMD_DTMB_CFO_DATA.fftfirstCfo*2 + sDMD_DTMB_CFO_DATA.fftSecondCfo) * sDMD_DTMB_CFO_DATA.sr/0x20000;
    
    PRINT("kavana KHAL_DEMOD_VQI_DTMB_CheckFrequencyOffset= %d!!\n",*pFreqOffset);
    return (OK);
}

int KHAL_DEMOD_VQI_DTMB_ControlOutput(BOOLEAN bEnableOutput)
{
    PRINT("^r^[DTV] OK (%s:%d) DTMB don't support to ControlOutput !!!\n", __F__, __L__);
    return (OK);
}

int KHAL_DEMOD_VQI_DTMB_ControlTSMode(BOOLEAN bIsSerial)
{
    PRINT("^r^[DTV] OK (%s:%d) DTMB don't support to switch TS !!!\n", __F__, __L__);
    return (OK);
}

int KHAL_DEMOD_DTMB_DebugMenu (void)
{
    return (OK);
}
