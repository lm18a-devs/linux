////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2008 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (��MStar Confidential Information��) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////

// PUT THIS FILE under INLCUDE path

///////////////////////////////////////////////////////////////////////////////////////////////////
///
/// @file   mdrv_temp_io.h
/// @brief  TEMP Driver Interface.
/// @author MStar Semiconductor Inc.
///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _MDRV_SEMUTEX_IO_H_
#define _MDRV_SEMUTEX_IO_H_

//-------------------------------------------------------------------------------------------------
//  Driver Capability
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Macro and Define
//-------------------------------------------------------------------------------------------------

#define SEMUTEX_IOC_MAGIC                'S'
#define MUTEX_INDEX_BEGIN                 0xFF
#define SEMAPHORE_INDEX_BEGIN			0x55660000

/* IOCTL functions. */
/* int ioctl(int fd,
             int request == MDRV_SEMUTEX_LOCK);
*/
#define MDRV_SEMUTEX_LOCK                (int)_IO(SEMUTEX_IOC_MAGIC, 0U)

/* int ioctl(int fd,
             int request == MDRV_SC_DETACH_INTERRUPT);
*/
#define MDRV_SEMUTEX_UNLOCK                (int)_IO(SEMUTEX_IOC_MAGIC, 1U)

/* int ioctl(int fd,
             int request == MDRV_SC_RESET_FIFO);
*/
#define MDRV_SEMUTEX_CREATE_SHAREMEMORY                      (int)_IO(SEMUTEX_IOC_MAGIC, 2U)

/* int ioctl(int fd,
             int request == MDRV_SC_GET_EVENTS,
             int *events);
*/
#define MDRV_SEMUTEX_MAP_SHAREMEMORY                      (int)_IOR(SEMUTEX_IOC_MAGIC, 3U, int)

#define MDRV_SEMUTEX_UNMAP_SHAREMEMORY                      (int)_IOR(SEMUTEX_IOC_MAGIC, 4U, int)

#define MDRV_SEMUTEX_CREATE_MUTEX                      _IOR(SEMUTEX_IOC_MAGIC, 5U, int)

#define MDRV_SEMUTEX_DEL_MUTEX                      _IOR(SEMUTEX_IOC_MAGIC, 6U, int)

#define MDRV_SEMUTEX_TRY_LOCK                     _IOR(SEMUTEX_IOC_MAGIC, 7U, int)

#define MDRV_SEMUTEX_QUERY_ISFIRST_SHAREMEMORY                      (int)_IO(SEMUTEX_IOC_MAGIC, 8U)

#define MDRV_SEMUTEX_LOCK_WITHTIMEOUT                      _IOR(SEMUTEX_IOC_MAGIC, 9U, int)

#define MDRV_SEMUTEX_EXPAND_SHAREMEMORY                      _IOR(SEMUTEX_IOC_MAGIC, 10U, int)

#define MDRV_SEMUTEX_GET_SHMSIZE		             _IOR(SEMUTEX_IOC_MAGIC, 11U, unsigned int)

#define MDRV_SEMUTEX_CREATE_SEMAPHORE               _IOR(SEMUTEX_IOC_MAGIC, 12U, int)

#define MDRV_SEMUTEX_SEMA_LOCK                      _IOR(SEMUTEX_IOC_MAGIC, 13U, int)

#define MDRV_SEMUTEX_SEMA_TRY_LOCK                  _IOR(SEMUTEX_IOC_MAGIC, 14U, int)

#define MDRV_SEMUTEX_SEMA_UNLOCK                    _IOR(SEMUTEX_IOC_MAGIC, 15U, int)

#define MDRV_SEMUTEX_SEMA_RESET                     _IOR(SEMUTEX_IOC_MAGIC, 16U, int)

#define MDRV_SEMUTEX_SET_CROSS_THREAD_UNLOCK        _IOR(SEMUTEX_IOC_MAGIC, 17U, CROSS_THREAD_UNLOCK_INFO)

#define SEMA_NAME_LEN   64

typedef struct{
int index;
int time;
}LOCKARG;

typedef struct{
    int     semanum;
    char    semaname[SEMA_NAME_LEN];
}CREATE_SEMA_ARG;

typedef enum
{
    E_CROSS_THREAD_UNLOCK_ENABLE = 0,
    E_CROSS_THREAD_UNLOCK_DISABLE,
}CROSS_THREAD_UNLOCK_FLAG;

typedef struct{
    int index;
    CROSS_THREAD_UNLOCK_FLAG flag;
}CROSS_THREAD_UNLOCK_INFO;

//-------------------------------------------------------------------------------------------------
//  Type and Structure
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//  Function and Variable
//-------------------------------------------------------------------------------------------------

#endif // SEMUTEX
