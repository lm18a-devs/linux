#ifndef PINCTRL_LG1K_H
#define PINCTRL_LG1K_H

#include <linux/types.h>

struct pinmux_info {
	u16 pin;
	u8 pins;
	phys_addr_t pa;
	u32 mask;
	u32 enable;
	bool savable;
	u32 save;

	void __iomem *va;
};

#define MAX_PINS	32

struct function_desc {
	char *name[MAX_PINS];
	unsigned int pin[MAX_PINS];
	size_t npins;
};

#endif
