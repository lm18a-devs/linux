#include <linux/kernel.h>

#include <linux/io.h>
#include <linux/pinctrl/pinctrl.h>
#include <linux/pinctrl/pinmux.h>
#include <linux/platform_device.h>

#include "pinctrl-lg1k.h"

static struct pinmux_info lg1313c0_pinmux_info[] = {
	/* CTOP_FMC_R06 */
	{  69, 1, 0xc9309418, 0x00010000, 0x00010000,  true, },
	{  70, 1, 0xc9309418, 0x00020000, 0x00020000,  true, },
	{  71, 1, 0xc9309418, 0x00040000, 0x00040000,  true, },
	{  80, 1, 0xc9309418, 0x00080000, 0x00080000,  true, },
	{  85, 1, 0xc9309418, 0x00100000, 0x00100000,  true, },
	{  86, 1, 0xc9309418, 0x00200000, 0x00200000,  true, },
	{  87, 1, 0xc9309418, 0x00400000, 0x00400000,  true, },
	{  68, 1, 0xc9309418, 0x00800000, 0x00800000,  true, },
	{  56, 1, 0xc9309418, 0x01000000, 0x01000000,  true, },
	{  57, 1, 0xc9309418, 0x02000000, 0x02000000,  true, },
	{  58, 1, 0xc9309418, 0x04000000, 0x04000000,  true, },
	{  59, 1, 0xc9309418, 0x08000000, 0x08000000,  true, },
	{  60, 1, 0xc9309418, 0x10000000, 0x10000000,  true, },
	{  61, 1, 0xc9309418, 0x20000000, 0x20000000,  true, },
	{  62, 1, 0xc9309418, 0x40000000, 0x40000000,  true, },
	{  63, 1, 0xc9309418, 0x80000000, 0x80000000,  true, },
	/* CTOP_FMC_R07 */
	{  18, 2, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  18, 2, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  16, 1, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  22, 2, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  16, 8, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  16, 8, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  16, 8, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  16, 8, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  16, 8, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  21, 1, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  16, 2, 0xc930941c, 0x00000000, 0x00000000,  true, },
	{  84, 1, 0xc930941c, 0x08000000, 0x00000000,  true, },
	{  83, 1, 0xc930941c, 0x10000000, 0x00000000,  true, },
	{  82, 1, 0xc930941c, 0x20000000, 0x00000000,  true, },
	{  81, 1, 0xc930941c, 0x40000000, 0x00000000,  true, },
	/* CTOP_SR_R04 */
	{  74, 1, 0xc9305410, 0x00000080, 0x00000080,  true, },
	{  78, 1, 0xc9305410, 0x00000100, 0x00000100,  true, },
	{ 136, 1, 0xc9305410, 0x00000200, 0x00000200,  true, },
	{ 138, 1, 0xc9305410, 0x00000400, 0x00000400,  true, },
	{  75, 1, 0xc9305410, 0x00000800, 0x00000800,  true, },
	{  79, 1, 0xc9305410, 0x00001000, 0x00001000,  true, },
	{ 137, 1, 0xc9305410, 0x00002000, 0x00002000,  true, },
	{ 139, 1, 0xc9305410, 0x00004000, 0x00004000,  true, },
	{  72, 1, 0xc9305410, 0x00008000, 0x00008000,  true, },
	{  73, 1, 0xc9305410, 0x00010000, 0x00010000,  true, },
	{  76, 1, 0xc9305410, 0x00020000, 0x00020000,  true, },
	{  77, 1, 0xc9305410, 0x00040000, 0x00040000,  true, },
	{  32, 1, 0xc9305410, 0x01000000, 0x01000000,  true, },
	{  33, 1, 0xc9305410, 0x02000000, 0x02000000,  true, },
	{  34, 1, 0xc9305410, 0x04000000, 0x04000000,  true, },
	{  35, 1, 0xc9305410, 0x08000000, 0x08000000,  true, },
	{  36, 1, 0xc9305410, 0x10000000, 0x10000000,  true, },
	{  37, 1, 0xc9305410, 0x20000000, 0x20000000,  true, },
	{  38, 1, 0xc9305410, 0x40000000, 0x40000000,  true, },
	{  39, 1, 0xc9305410, 0x80000000, 0x80000000,  true, },
	/* CTOP_SR_R09 */
	{  32, 4, 0xc9305424, 0x08000000, 0x08000000,  true, },
	{  39, 1, 0xc9305424, 0x08000000, 0x08000000,  true, },
	/* CTOP_FMS_R11 */
	{  48, 1, 0xc930c42c, 0x00002000, 0x00002000,  true, },
	{  49, 1, 0xc930c42c, 0x00004000, 0x00004000,  true, },
	{  50, 1, 0xc930c42c, 0x00008000, 0x00008000,  true, },
	{  51, 1, 0xc930c42c, 0x00010000, 0x00010000,  true, },
	{  52, 1, 0xc930c42c, 0x00020000, 0x00020000,  true, },
	{  53, 1, 0xc930c42c, 0x00040000, 0x00040000,  true, },
	{  96, 1, 0xc930c42c, 0x00100000, 0x00100000,  true, },
	{  97, 1, 0xc930c42c, 0x00200000, 0x00200000,  true, },
	{  98, 1, 0xc930c42c, 0x00400000, 0x00400000,  true, },
	{  99, 1, 0xc930c42c, 0x00800000, 0x00800000,  true, },
	{ 100, 1, 0xc930c42c, 0x01000000, 0x01000000,  true, },
	{ 101, 1, 0xc930c42c, 0x02000000, 0x02000000,  true, },
	{ 102, 1, 0xc930c42c, 0x04000000, 0x04000000,  true, },
	{ 103, 1, 0xc930c42c, 0x08000000, 0x08000000,  true, },
	{  44, 1, 0xc930c42c, 0x10000000, 0x10000000,  true, },
	{  45, 1, 0xc930c42c, 0x20000000, 0x20000000,  true, },
	{  46, 1, 0xc930c42c, 0x40000000, 0x40000000,  true, },
	{  47, 1, 0xc930c42c, 0x80000000, 0x80000000,  true, },
	/* CTOP_FMS_R12 */
	{  18, 2, 0xc930c430, 0x02000000, 0x00000000,  true, },
	/* CTOP_FME0_R12 */
	{ 130, 1, 0xc9308830, 0x00000004, 0x00000004,  true, },
	{ 131, 1, 0xc9308830, 0x00000008, 0x00000008,  true, },
	{ 132, 1, 0xc9308830, 0x00000010, 0x00000010,  true, },
	{ 133, 1, 0xc9308830, 0x00000020, 0x00000020,  true, },
	{ 134, 1, 0xc9308830, 0x00000040, 0x00000040,  true, },
	{ 135, 1, 0xc9308830, 0x00000080, 0x00000080,  true, },
	{ 120, 1, 0xc9308830, 0x00000100, 0x00000100,  true, },
	{ 121, 1, 0xc9308830, 0x00000200, 0x00000200,  true, },
	{ 122, 1, 0xc9308830, 0x00000400, 0x00000400,  true, },
	{ 123, 1, 0xc9308830, 0x00000800, 0x00000800,  true, },
	{ 124, 1, 0xc9308830, 0x00001000, 0x00001000,  true, },
	{ 125, 1, 0xc9308830, 0x00002000, 0x00002000,  true, },
	{ 126, 1, 0xc9308830, 0x00004000, 0x00004000,  true, },
	{ 127, 1, 0xc9308830, 0x00008000, 0x00008000,  true, },
	{ 112, 1, 0xc9308830, 0x00010000, 0x00010000,  true, },
	{ 113, 1, 0xc9308830, 0x00020000, 0x00020000,  true, },
	{ 114, 1, 0xc9308830, 0x00040000, 0x00040000,  true, },
	{ 115, 1, 0xc9308830, 0x00080000, 0x00080000,  true, },
	{ 116, 1, 0xc9308830, 0x00100000, 0x00100000,  true, },
	{ 117, 1, 0xc9308830, 0x00200000, 0x00200000,  true, },
	{ 118, 1, 0xc9308830, 0x00400000, 0x00400000,  true, },
	{ 119, 1, 0xc9308830, 0x00800000, 0x00800000,  true, },
	{  88, 1, 0xc9308830, 0x01000000, 0x00000000,  true, },
	{  89, 1, 0xc9308830, 0x02000000, 0x02000000,  true, },
	{  90, 1, 0xc9308830, 0x04000000, 0x00000000,  true, },
	{  91, 1, 0xc9308830, 0x08000000, 0x00000000,  true, },
	{  92, 1, 0xc9308830, 0x10000000, 0x00000000,  true, },
	{  93, 1, 0xc9308830, 0x20000000, 0x00000000,  true, },
	{  94, 1, 0xc9308830, 0x40000000, 0x40000000,  true, },
	{  95, 1, 0xc9308830, 0x80000000, 0x80000000,  true, },
	/* CTOP_FME0_R16 */
	{  24, 6, 0xc9308840, 0x00000100, 0x00000000,  true, },
	/* CTOP_DPE_R32 */
	{   8, 8, 0xc930a480, 0x00780680, 0x00000000,  true, },
	{   0, 8, 0xc930a480, 0x0007f900, 0x00000000,  true, },
	{ 105, 1, 0xc930a480, 0x04000000, 0x04000000,  true, },
	{ 106, 1, 0xc930a480, 0x08000000, 0x08000000,  true, },
	{ 107, 1, 0xc930a480, 0x10000000, 0x10000000,  true, },
	{ 108, 1, 0xc930a480, 0x20000000, 0x20000000,  true, },
	{ 109, 1, 0xc930a480, 0x40000000, 0x40000000,  true, },
	{ 110, 1, 0xc930a480, 0x80000000, 0x80000000,  true, },
	/* CTOP_DPE_R33 */
	{  24, 2, 0xc930a484, 0x00000040, 0x00000000,  true, },
	{   0, 8, 0xc930a484, 0x00000180, 0x00000000,  true, },
	{  24, 8, 0xc930a484, 0x00000600, 0x00000000,  true, },
	{  24, 1, 0xc930a484, 0x00001000, 0x00000000,  true, },
};

static struct pinctrl_pin_desc const lg1313c0_pin_desc[] = {
	PINCTRL_PIN(0, "AJ12:GPIO0/SARADC0"),
	PINCTRL_PIN(1, "AH11:GPIO1/SARADC1"),
	PINCTRL_PIN(2, "AH9:GPIO2/SARADC2"),
	PINCTRL_PIN(3, "AJ10:GPIO3/SARADC3"),
	PINCTRL_PIN(4, "AH10:GPIO4/SARADC4"),
	PINCTRL_PIN(5, "AJ11:GPIO5/SARADC5"),
	PINCTRL_PIN(6, "AG9:GPIO6"),
	PINCTRL_PIN(7, "AJ9:GPIO7"),
	PINCTRL_PIN(8, "AH18:GPIO8"),
	PINCTRL_PIN(9, "AH19:GPIO9"),
	PINCTRL_PIN(10, "AK20:GPIO10"),
	PINCTRL_PIN(11, "AJ19:GPIO11"),
	PINCTRL_PIN(12, "AK19:GPIO12"),
	PINCTRL_PIN(13, "AK16:GPIO13"),
	PINCTRL_PIN(14, "AJ18:GPIO14"),
	PINCTRL_PIN(15, "AH20:GPIO15"),
	PINCTRL_PIN(16, "AD7:GPIO16"),
	PINCTRL_PIN(17, "AD5:GPIO17/MPRT_PWM_IN"),
	PINCTRL_PIN(18, "AC8:GPIO18"),
	PINCTRL_PIN(19, "AB6:GPIO19"),
	PINCTRL_PIN(20, "AC6:GPIO20"),
	PINCTRL_PIN(21, "AB7:GPIO21/DACRLRCH"),
	PINCTRL_PIN(22, "AD6:GPIO22"),
	PINCTRL_PIN(23, "AC7:GPIO23"),
	PINCTRL_PIN(24, "AJ14:GPIO24"),
	PINCTRL_PIN(25, "AH13:GPIO25"),
	PINCTRL_PIN(26, "AK14:GPIO26"),
	PINCTRL_PIN(27, "AK13:GPIO27"),
	PINCTRL_PIN(28, "AJ16:GPIO28"),
	PINCTRL_PIN(29, "AH14:GPIO29"),
	PINCTRL_PIN(30, "AJ13:GPIO30"),
	PINCTRL_PIN(31, "AH12:GPIO31"),
	PINCTRL_PIN(32, "AJ27:JTMS1/SPI_CS1/GPIO32"),
	PINCTRL_PIN(33, "AK28:JTCK1/SPI_SCLK1/GPIO33"),
	PINCTRL_PIN(34, "AJ25:JTDO1/SPI_DO1/GPIO34"),
	PINCTRL_PIN(35, "AK26:JTDI1/SPI_DI1/GPIO35"),
	PINCTRL_PIN(36, "AJ26:SPI_CS0/GPIO36"),
	PINCTRL_PIN(37, "AK25:SPI_SCLK0/GPIO37"),
	PINCTRL_PIN(38, "AH27:SPI_DO0/GPIO38"),
	PINCTRL_PIN(39, "AJ28:JTRST1/SPI_DI0/GPIO39"),
	PINCTRL_PIN(45, "AF28:STPI0_VAL/GPIO45/PWM_OUT1"),
	PINCTRL_PIN(46, "AE28:STPI0_SOP/GPIO46/PWM_OUT0"),
	PINCTRL_PIN(47, "AG28:STPI0_CLK/GPIO47"),
	PINCTRL_PIN(56, "AE6:DIM1_MOSI/GPIO56"),
	PINCTRL_PIN(57, "AF6:DIM1_SCLK/GPIO57"),
	PINCTRL_PIN(58, "AF7:DIM0_MOSI/GPIO58"),
	PINCTRL_PIN(59, "AE7:DIM0_SCLK/GPIO59"),
	PINCTRL_PIN(60, "AC3:L_VSOUT_LD/GPIO60"),
	PINCTRL_PIN(61, "AB5:PWM2/GPIO61"),
	PINCTRL_PIN(62, "AA6:PWM1/GPIO62"),
	PINCTRL_PIN(63, "AA5:PWM_IN/GPIO63"),
	PINCTRL_PIN(68, "AE8:EXT_INTR0/GPIO68"),
	PINCTRL_PIN(72, "AH26:SDA5/GPIO72"),
	PINCTRL_PIN(73, "AH25:SCL5/GPIO73"),
	PINCTRL_PIN(74, "AH21:SDA4/GPIO74"),
	PINCTRL_PIN(75, "AJ21:SCL4/GPIO75"),
	PINCTRL_PIN(76, "AJ24:SDA3/GPIO76"),
	PINCTRL_PIN(77, "AH24:SCL3/GPIO77"),
	PINCTRL_PIN(78, "AH22:SDA2/GPIO78"),
	PINCTRL_PIN(79, "AH23:SCL2/GPIO79"),
	PINCTRL_PIN(88, "E30:GPIO88/EB_WAIT"),
	PINCTRL_PIN(89, "A27:GPIO89/EB_ADDR15"),
	PINCTRL_PIN(94, "AA29:GPIO94"),
	PINCTRL_PIN(95, "AB29:GPIO95"),
	PINCTRL_PIN(96, "AG29:STPI0_DATA0/GPIO96/PWM_OUT2"),
	PINCTRL_PIN(97, "AH29:STPI0_DATA1/GPIO97"),
	PINCTRL_PIN(98, "AH30:STPI0_DATA2/GPIO98"),
	PINCTRL_PIN(99, "AJ29:STPI0_DATA3/GPIO99"),
	PINCTRL_PIN(100, "AJ30:STPI0_DATA4/GPIO100"),
	PINCTRL_PIN(101, "AH28:STPI0_DATA5/GPIO101"),
	PINCTRL_PIN(102, "AK29:STPI0_DATA6/GPIO102"),
	PINCTRL_PIN(103, "AK30:STPI0_DATA7/GPIO103"),
	PINCTRL_PIN(105, "AJ17:UART0_TXD/GPIO105"),
	PINCTRL_PIN(106, "AK17:UART0_RXD/GPIO106"),
	PINCTRL_PIN(107, "AH17:GPIO107/UART1_RTS"),
	PINCTRL_PIN(108, "AH16:GPIO108/UART1_CTS"),
	PINCTRL_PIN(109, "AJ15:UART1_TXD/GPIO109"),
	PINCTRL_PIN(110, "AH15:UART1_RXD/GPIO110"),
	PINCTRL_PIN(112, "E26:EB_ADDR0/GPIO112"),
	PINCTRL_PIN(113, "F26:EB_ADDR1/GPIO113"),
	PINCTRL_PIN(114, "D24:EB_ADDR2/GPIO114"),
	PINCTRL_PIN(115, "E24:EB_ADDR3/GPIO115"),
	PINCTRL_PIN(116, "F24:EB_ADDR4/GPIO116"),
	PINCTRL_PIN(117, "D27:EB_ADDR5/GPIO117"),
	PINCTRL_PIN(118, "C27:EB_ADDR6/GPIO118"),
	PINCTRL_PIN(119, "A28:EB_ADDR7/GPIO119"),
	PINCTRL_PIN(120, "C30:EB_DATA0/GPIO120"),
	PINCTRL_PIN(121, "A30:EB_DATA1/GPIO121"),
	PINCTRL_PIN(122, "B30:EB_DATA2/GPIO122"),
	PINCTRL_PIN(123, "D30:EB_DATA3/GPIO123"),
	PINCTRL_PIN(124, "C29:EB_DATA4/GPIO124"),
	PINCTRL_PIN(125, "C28:EB_DATA5/GPIO125"),
	PINCTRL_PIN(126, "B28:EB_DATA6/GPIO126"),
	PINCTRL_PIN(127, "D29:EB_DATA7/GPIO127"),
	PINCTRL_PIN(130, "E23:SD_DATA2/GPIO130"),
	PINCTRL_PIN(131, "F22:SD_DATA1/GPIO131"),
	PINCTRL_PIN(132, "E22:SD_DATA0/GPIO132"),
	PINCTRL_PIN(133, "D22:SD_CMD/GPIO133"),
	PINCTRL_PIN(134, "D21:SD_WP_N/GPIO134"),
	PINCTRL_PIN(135, "E21:SD_CD_N/GPIO135"),
	PINCTRL_PIN(136, "AJ22:SDA1/GPIO136"),
	PINCTRL_PIN(137, "AK22:SCL1/GPIO137"),
	PINCTRL_PIN(138, "AJ23:SDA0/GPIO138"),
	PINCTRL_PIN(139, "AK23:SCL0/GPIO139"),
};

static struct function_desc const lg1313c0_func_desc[] = {
	{
		.name = { "audio-earc", },
		.pin = { 21, },
		.npins = 1,
	}, {
		.name = { "ext_bus", },
		.pin = { 88, 89, 112, 113, 114, 115, 116, 117, 118, 119, 120,
		         121, 122, 123, 124, 125, 126, 127, },
		.npins = 18,
	}, {
		.name = { "ext_intr0", },
		.pin = { 68, },
		.npins = 1,
	}, {
		.name = { "hdmi2", },
		.pin = { 56, 57, 58, 59, 60, },
		.npins = 5,
	}, {
		.name = { "i2c0", },
		.pin = { 138, 139, },
		.npins = 2,
	}, {
		.name = { "i2c1", },
		.pin = { 136, 137, },
		.npins = 2,
	}, {
		.name = { "i2c2", },
		.pin = { 78, 79, },
		.npins = 2,
	}, {
		.name = { "i2c3", },
		.pin = { 76, 77, },
		.npins = 2,
	}, {
		.name = { "i2c4", },
		.pin = { 74, 75, },
		.npins = 2,
	}, {
		.name = { "i2c5", },
		.pin = { 72, 73, },
		.npins = 2,
	}, {
		.name = { "pwm", },
		.pin = { 17, 45, 46, 61, 62, 63, 96, },
		.npins = 7,
	}, {
		.name = { "sdio", },
		.pin = { 130, 131, 132, 133, 134, 135, },
		.npins = 6,
	}, {
		.name = { "spi0", },
		.pin = { 36, 37, 38, 39, },
		.npins = 4,
	}, {
		.name = { "spi1", },
		.pin = { 32, 33, 34, 35, },
		.npins = 4,
	}, {
		.name = { "stpi0", },
		.pin = { 45, 46, 47, 96, 97, 98, 99, 100, 101, 102, 103, },
		.npins = 11,
	}, {
		.name = { "uart0", },
		.pin = { 105, 106, },
		.npins = 2,
	}, {
		.name = { "uart1", },
		.pin = { 107, 108, 109, 110, },
		.npins = 4,
	},
};

static int lg1313c0_pinctrl_get_groups_count(struct pinctrl_dev *pctldev)
{
	return ARRAY_SIZE(lg1313c0_func_desc);
}

static char const *lg1313c0_pinctrl_get_group_name(struct pinctrl_dev *pctldev,
                                                   unsigned selector)
{
	return lg1313c0_func_desc[selector].name[0];
}

static struct pinctrl_ops lg1313c0_pinctrl_ops = {
	.get_groups_count = lg1313c0_pinctrl_get_groups_count,
	.get_group_name = lg1313c0_pinctrl_get_group_name,
};

static int lg1313c0_pinmux_get_functions_count(struct pinctrl_dev *pctldev)
{
	return ARRAY_SIZE(lg1313c0_func_desc);
}

static char const *
lg1313c0_pinmux_get_function_name(struct pinctrl_dev *pctldev,
                                  unsigned selector)
{
	return lg1313c0_func_desc[selector].name[0];
}

static int lg1313c0_pinmux_get_function_groups(struct pinctrl_dev *pctldev,
                                               unsigned selector,
                                               char const *const **groups,
                                               unsigned *num_groups)
{
	*groups = (char const *const *)lg1313c0_func_desc[selector].name;
	*num_groups = 1;

	return 0;
}

static int lg1313c0_pinmux_set_mux(struct pinctrl_dev *pctldev,
                                   unsigned func_selector,
                                   unsigned group_selector)
{
	return 0;
}

static int lg1313c0_pinmux_gpio_request_enable(struct pinctrl_dev *pctldev,
                                               struct pinctrl_gpio_range *range,
                                               unsigned offset)
{
	struct pinmux_info const *info;
	u32 v;
	int i;

	if (range->pins) {
		for (i = 0; i < range->npins; i++)
			if (offset == range->pins[i])
				break;
	} else {
		for (i = 0; i < range->npins; i++)
			if (offset == range->pin_base + i)
				break;
	}

	if (i == range->npins)
		return 0;

	for (i = 0; i < ARRAY_SIZE(lg1313c0_pinmux_info); i++) {
		info = &lg1313c0_pinmux_info[i];
		if (offset >= info->pin && offset < info->pin + info->pins) {
			v = readl_relaxed(info->va);
			v &= ~info->mask;
			v |= info->enable;
			writel_relaxed(v, info->va);
		}
	}

	return 0;
}

static void lg1313c0_pinmux_gpio_disable_free(struct pinctrl_dev *pctldev,
                                              struct pinctrl_gpio_range *range,
                                              unsigned offset)
{
}

static struct pinmux_ops lg1313c0_pinmux_ops = {
	.get_functions_count = lg1313c0_pinmux_get_functions_count,
	.get_function_name = lg1313c0_pinmux_get_function_name,
	.get_function_groups = lg1313c0_pinmux_get_function_groups,
	.set_mux = lg1313c0_pinmux_set_mux,
	.gpio_request_enable = lg1313c0_pinmux_gpio_request_enable,
	.gpio_disable_free = lg1313c0_pinmux_gpio_disable_free,
};

static int lg1313c0_pinctrl_suspend(struct device *dev)
{
	struct pinmux_info *info;
	int i;

	for (i = 0; i < ARRAY_SIZE(lg1313c0_pinmux_info); i++) {
		info = &lg1313c0_pinmux_info[i];
		if (info->savable)
			info->save = readl_relaxed(info->va);
	}

	return 0;
}

static int lg1313c0_pinctrl_resume(struct device *dev)
{
	struct pinmux_info *info;
	int i;

	for (i = 0; i < ARRAY_SIZE(lg1313c0_pinmux_info); i++) {
		info = &lg1313c0_pinmux_info[i];
		if (info->savable)
			writel_relaxed(info->save, info->va);
	}

	return 0;
}

static struct dev_pm_ops lg1313c0_pinctrl_pm_ops = {
	SET_LATE_SYSTEM_SLEEP_PM_OPS(lg1313c0_pinctrl_suspend,
	                             lg1313c0_pinctrl_resume)
};

static struct pinctrl_desc lg1313c0_pinctrl_desc = {
	.name = "lg1313c0-pinctrl",
	.pins = lg1313c0_pin_desc,
	.npins = ARRAY_SIZE(lg1313c0_pin_desc),
	.pctlops = &lg1313c0_pinctrl_ops,
	.pmxops = &lg1313c0_pinmux_ops,
	.owner = THIS_MODULE,
};

#define PAGE_MAPS	8

static void lg1313c0_pinctrl_iomap(struct platform_device *pdev)
{
	struct pinmux_info *info;
	phys_addr_t ppa[PAGE_MAPS];
	void __iomem *pva[PAGE_MAPS];
	size_t maps = 0;
	int i, j;

	for (i = 0; i < ARRAY_SIZE(lg1313c0_pinmux_info); i++) {
		info = &lg1313c0_pinmux_info[i];

		for (j = 0; j < maps; j++) {
			if ((info->pa & PAGE_MASK) == (ppa[j] & PAGE_MASK))
				break;
		}

		if (j == maps) {
			ppa[maps] = info->pa & PAGE_MASK;
			pva[maps] = ioremap(ppa[maps], PAGE_SIZE);
			maps++;
		}

		info->va = pva[j] + (info->pa & ~PAGE_MASK);

		dev_dbg(&pdev->dev, "PA: %x, VA: %p\n", info->pa, info->va);
	}
}

static int lg1313c0_pinctrl_probe(struct platform_device *pdev)
{
	struct pinctrl_dev *pctldev;

	lg1313c0_pinctrl_iomap(pdev);

	pctldev = pinctrl_register(&lg1313c0_pinctrl_desc, &pdev->dev, NULL);
	if (IS_ERR(pctldev))
		return PTR_ERR(pctldev);

	dev_info(&pdev->dev, "LG1313 rev.C0 pinctrl driver\n");

	return 0;
}

static struct of_device_id const lg1313c0_pinctrl_ids[] = {
	{ .compatible = "lge,lg1313c0-pinctrl", },
	{ /* sentinel */ }
};

static struct platform_driver lg1313c0_pinctrl_driver = {
	.driver = {
		.name = "lg1313c0-pinctrl",
		.of_match_table = lg1313c0_pinctrl_ids,
		.pm = &lg1313c0_pinctrl_pm_ops,
	},
	.probe = lg1313c0_pinctrl_probe,
};

static int __init lg1313c0_pinctrl_init(void)
{
	return platform_driver_register(&lg1313c0_pinctrl_driver);
}
arch_initcall(lg1313c0_pinctrl_init);
