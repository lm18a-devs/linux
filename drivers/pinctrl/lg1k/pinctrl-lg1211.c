#include <linux/kernel.h>

#include <linux/io.h>
#include <linux/pinctrl/pinctrl.h>
#include <linux/pinctrl/pinmux.h>
#include <linux/platform_device.h>

#include "pinctrl-lg1k.h"

static struct pinmux_info lg1211_pinmux_info[] = {
	/* CTOP_MEU CTR00 */
	{  88, 1, 0xc5fe0000, 0x00000008, 0x00000000,  true, },
	{  89, 1, 0xc5fe0000, 0x00000010, 0x00000000,  true, },
	{  90, 1, 0xc5fe0000, 0x00000020, 0x00000000,  true, },
	{  91, 1, 0xc5fe0000, 0x00000040, 0x00000000,  true, },
	{  92, 1, 0xc5fe0000, 0x00000080, 0x00000000,  true, },
	{  93, 1, 0xc5fe0000, 0x00000100, 0x00000000,  true, },
	{ 112, 1, 0xc5fe0000, 0x00000200, 0x00000200,  true, },
	{ 113, 1, 0xc5fe0000, 0x00000400, 0x00000400,  true, },
	{ 114, 1, 0xc5fe0000, 0x00000800, 0x00000800,  true, },
	{ 115, 1, 0xc5fe0000, 0x00001000, 0x00001000,  true, },
	{ 116, 1, 0xc5fe0000, 0x00002000, 0x00002000,  true, },
	{ 117, 1, 0xc5fe0000, 0x00004000, 0x00004000,  true, },
	{ 118, 1, 0xc5fe0000, 0x00008000, 0x00008000,  true, },
	{ 119, 1, 0xc5fe0000, 0x00010000, 0x00010000,  true, },
	{ 120, 1, 0xc5fe0000, 0x00020000, 0x00020000,  true, },
	{ 121, 1, 0xc5fe0000, 0x00040000, 0x00040000,  true, },
	{ 122, 1, 0xc5fe0000, 0x00080000, 0x00080000,  true, },
	{ 123, 1, 0xc5fe0000, 0x00100000, 0x00100000,  true, },
	{ 124, 1, 0xc5fe0000, 0x00200000, 0x00200000,  true, },
	{ 125, 1, 0xc5fe0000, 0x00400000, 0x00400000,  true, },
	{ 126, 1, 0xc5fe0000, 0x00800000, 0x00800000,  true, },
	{ 127, 1, 0xc5fe0000, 0x01000000, 0x01000000,  true, },
	/* CTOP_DPE CTR33 */
	{  17, 1, 0xc97c8084, 0x00000001, 0x00000000,  true, },
	{  16, 1, 0xc97c8084, 0x00000002, 0x00000000,  true, },
	{  22, 2, 0xc97c8084, 0x00000004, 0x00000000,  true, },
	{  21, 1, 0xc97c8084, 0x00000008, 0x00000000,  true, },
	{  14, 2, 0xc97c8084, 0x00000010, 0x00000000,  true, },
	{  18, 2, 0xc97c8084, 0x00000060, 0x00000000,  true, },
	{ 108, 1, 0xc97c8084, 0x00000080, 0x00000000,  true, },
	{  10, 1, 0xc97c8084, 0x00000100, 0x00000000,  true, },
	{  11, 1, 0xc97c8084, 0x00000200, 0x00000000,  true, },
	{  12, 1, 0xc97c8084, 0x00000400, 0x00000000,  true, },
	{  13, 1, 0xc97c8084, 0x00000800, 0x00000000,  true, },
	{  14, 1, 0xc97c8084, 0x00001000, 0x00000000,  true, },
	{  15, 1, 0xc97c8084, 0x00002000, 0x00000000,  true, },
	{  16, 1, 0xc97c8084, 0x00004000, 0x00000000,  true, },
	{  17, 1, 0xc97c8084, 0x00008000, 0x00000000,  true, },
	{  18, 1, 0xc97c8084, 0x00010000, 0x00000000,  true, },
	{  19, 1, 0xc97c8084, 0x00020000, 0x00000000,  true, },
	{  20, 1, 0xc97c8084, 0x00040000, 0x00000000,  true, },
	{  21, 1, 0xc97c8084, 0x00080000, 0x00000000,  true, },
	{  22, 1, 0xc97c8084, 0x00100000, 0x00000000,  true, },
	{  23, 1, 0xc97c8084, 0x00200000, 0x00000000,  true, },
	{  24, 1, 0xc97c8084, 0x00400000, 0x00000000,  true, },
	{  25, 1, 0xc97c8084, 0x00800000, 0x00000000,  true, },
	{  26, 1, 0xc97c8084, 0x01000000, 0x00000000,  true, },
	{  27, 1, 0xc97c8084, 0x02000000, 0x00000000,  true, },
	{  28, 1, 0xc97c8084, 0x04000000, 0x00000000,  true, },
	{  29, 1, 0xc97c8084, 0x08000000, 0x00000000,  true, },
	{  22, 2, 0xc97c8084, 0x10000000, 0x00000000,  true, },
	/* CTOP_DPE CTR48 */
	{ 105, 1, 0xc97c80c0, 0x00000001, 0x00000001,  true, },
	{ 106, 1, 0xc97c80c0, 0x00000002, 0x00000002,  true, },
	{ 107, 1, 0xc97c80c0, 0x00000004, 0x00000000,  true, },
	{ 108, 1, 0xc97c80c0, 0x00000008, 0x00000000,  true, },
	/* CTOP_BMC CTR00 */
	{  96, 1, 0xc7fc0094, 0x00000001, 0x00000001,  true, },
	{  97, 1, 0xc7fc0094, 0x00000002, 0x00000002,  true, },
	{  98, 1, 0xc7fc0094, 0x00000004, 0x00000004,  true, },
	{  99, 1, 0xc7fc0094, 0x00000008, 0x00000008,  true, },
	{ 100, 1, 0xc7fc0094, 0x00000010, 0x00000010,  true, },
	{ 101, 1, 0xc7fc0094, 0x00000020, 0x00000020,  true, },
	{ 102, 1, 0xc7fc0094, 0x00000040, 0x00000040,  true, },
	{ 103, 1, 0xc7fc0094, 0x00000080, 0x00000080,  true, },
	{  32, 1, 0xc7fc0094, 0x00000100, 0x00000100,  true, },
	{  33, 1, 0xc7fc0094, 0x00000200, 0x00000200,  true, },
	{  34, 1, 0xc7fc0094, 0x00000400, 0x00000400,  true, },
	{  35, 1, 0xc7fc0094, 0x00000800, 0x00000800,  true, },
	{  36, 1, 0xc7fc0094, 0x00001000, 0x00001000,  true, },
	{  37, 1, 0xc7fc0094, 0x00002000, 0x00002000,  true, },
	{  38, 1, 0xc7fc0094, 0x00004000, 0x00004000,  true, },
	{  39, 1, 0xc7fc0094, 0x00008000, 0x00008000,  true, },
	{  45, 1, 0xc7fc0094, 0x00010000, 0x00010000,  true, },
	{  46, 1, 0xc7fc0094, 0x00020000, 0x00020000,  true, },
	{  47, 1, 0xc7fc0094, 0x00040000, 0x00040000,  true, },
	{  72, 1, 0xc7fc0094, 0x00080000, 0x00080000,  true, },
	{  73, 1, 0xc7fc0094, 0x00100000, 0x00100000,  true, },
	{  74, 1, 0xc7fc0094, 0x00200000, 0x00200000,  true, },
	{  75, 1, 0xc7fc0094, 0x00400000, 0x00400000,  true, },
	{  76, 1, 0xc7fc0094, 0x00800000, 0x00800000,  true, },
	{  77, 1, 0xc7fc0094, 0x01000000, 0x01000000,  true, },
	/* CTOP_BMC CTR03 */
	{  94, 1, 0xc7fc00a0, 0x00000001, 0x00000001,  true, },
	{  95, 1, 0xc7fc00a0, 0x00000002, 0x00000002,  true, },
	/* CTOP_CCO CTR00 */
	{  80, 1, 0xc97d0000, 0x00000001, 0x00000001,  true, },
	{  81, 1, 0xc97d0000, 0x00000002, 0x00000002,  true, },
	{  82, 1, 0xc97d0000, 0x00000004, 0x00000004,  true, },
	{  83, 1, 0xc97d0000, 0x00000008, 0x00000008,  true, },
	{  84, 1, 0xc97d0000, 0x00000010, 0x00000010,  true, },
	{  85, 1, 0xc97d0000, 0x00000020, 0x00000020,  true, },
	{  86, 1, 0xc97d0000, 0x00000040, 0x00000040,  true, },
	{  87, 1, 0xc97d0000, 0x00000080, 0x00000080,  true, },
	{  56, 1, 0xc97d0000, 0x00000100, 0x00000100,  true, },
	{  57, 1, 0xc97d0000, 0x00000200, 0x00000200,  true, },
	{  58, 1, 0xc97d0000, 0x00000400, 0x00000400,  true, },
	{  59, 1, 0xc97d0000, 0x00000800, 0x00000800,  true, },
	{  60, 1, 0xc97d0000, 0x00001000, 0x00001000,  true, },
	{  61, 1, 0xc97d0000, 0x00002000, 0x00002000,  true, },
	{  62, 1, 0xc97d0000, 0x00004000, 0x00004000,  true, },
	{  63, 1, 0xc97d0000, 0x00008000, 0x00008000,  true, },
	{  68, 1, 0xc97d0000, 0x00010000, 0x00010000,  true, },
	{  30, 1, 0xc97d0000, 0x00020000, 0x00000000,  true, },
	{  31, 1, 0xc97d0000, 0x00040000, 0x00000000,  true, },
	/* CTOP_ME1 CTR00 */
	{ 130, 1, 0xc5ff0000, 0x00000001, 0x00000001,  true, },
	{ 131, 1, 0xc5ff0000, 0x00000002, 0x00000002,  true, },
	{ 132, 1, 0xc5ff0000, 0x00000004, 0x00000004,  true, },
	{ 133, 1, 0xc5ff0000, 0x00000008, 0x00000008,  true, },
	{ 134, 1, 0xc5ff0000, 0x00000010, 0x00000010,  true, },
	{ 135, 1, 0xc5ff0000, 0x00000020, 0x00000020,  true, },
	/* CTOP_ND0 CTR04 */
	{ 109, 1, 0xc7fe0010, 0x00000010, 0x00000010,  true, },
	{ 110, 1, 0xc7fe0010, 0x00000020, 0x00000020,  true, },
	{ 136, 1, 0xc7fe0010, 0x00000040, 0x00000040,  true, },
	{ 137, 1, 0xc7fe0010, 0x00000080, 0x00000080,  true, },
	{ 138, 1, 0xc7fe0010, 0x00000100, 0x00000100,  true, },
	{ 139, 1, 0xc7fe0010, 0x00000200, 0x00000200,  true, },
	{  78, 1, 0xc7fe0010, 0x00000400, 0x00000400,  true, },
	{  79, 1, 0xc7fe0010, 0x00000800, 0x00000800,  true, },
	{   0, 1, 0xc7fe0010, 0x00001000, 0x00000000,  true, },
	{   1, 1, 0xc7fe0010, 0x00002000, 0x00000000,  true, },
	{   2, 1, 0xc7fe0010, 0x00004000, 0x00000000,  true, },
	{   3, 1, 0xc7fe0010, 0x00008000, 0x00000000,  true, },
	{   4, 1, 0xc7fe0010, 0x00010000, 0x00000000,  true, },
	{   5, 1, 0xc7fe0010, 0x00020000, 0x00000000,  true, },
	{   6, 1, 0xc7fe0010, 0x00040000, 0x00000000,  true, },
	{   7, 1, 0xc7fe0010, 0x00080000, 0x00000000,  true, },
	{   8, 1, 0xc7fe0010, 0x00100000, 0x00000000,  true, },
	{   9, 1, 0xc7fe0010, 0x00200000, 0x00000000,  true, },
	/* CTOP_SYN EDID_BND01 */
	{  48, 1, 0xc68f0004, 0x00000002, 0x00000002, false, },
	{  49, 1, 0xc68f0004, 0x00000004, 0x00000004, false, },
	{  50, 1, 0xc68f0004, 0x00000008, 0x00000008, false, },
	{  51, 1, 0xc68f0004, 0x00000010, 0x00000010, false, },
	{  52, 1, 0xc68f0004, 0x00000020, 0x00000020, false, },
	{  53, 1, 0xc68f0004, 0x00000040, 0x00000040, false, },
};

static struct pinctrl_pin_desc const lg1211_pin_desc[] = {
	PINCTRL_PIN(0, "AM18:GPIO0"),
	PINCTRL_PIN(1, "AL18:GPIO1"),
	PINCTRL_PIN(2, "AN17:GPIO2"),
	PINCTRL_PIN(3, "AM17:GPIO3"),
	PINCTRL_PIN(4, "AL17:GPIO4"),
	PINCTRL_PIN(5, "AN16:GPIO5"),
	PINCTRL_PIN(6, "AM16:GPIO6"),
	PINCTRL_PIN(7, "AL16:GPIO7"),
	PINCTRL_PIN(8, "AL15:GPIO8"),
	PINCTRL_PIN(9, "AM15:GPIO9"),
	PINCTRL_PIN(10, "AM13:GPIO10"),
	PINCTRL_PIN(11, "AM14:GPIO11"),
	PINCTRL_PIN(12, "AN13:GPIO12"),
	PINCTRL_PIN(13, "AL11:GPIO13"),
	PINCTRL_PIN(14, "AN10:GPIO14"),
	PINCTRL_PIN(15, "AM10:GPIO15"),
	PINCTRL_PIN(16, "AL10:GPIO16"),
	PINCTRL_PIN(17, "AM9:GPIO17"),
	PINCTRL_PIN(18, "AL9:GPIO18"),
	PINCTRL_PIN(19, "AL8:GPIO19"),
	PINCTRL_PIN(20, "AM8:GPIO20"),
	PINCTRL_PIN(21, "AN8:GPIO21/DACRLRCH"),
	PINCTRL_PIN(22, "AN7:GPIO22"),
	PINCTRL_PIN(23, "AL7:GPIO23"),
	PINCTRL_PIN(30, "AM7:GPIO30/PWM_IN_TRIG"),
	PINCTRL_PIN(31, "AK8:GPIO31"),
	PINCTRL_PIN(32, "AL24:JTMS1/SPI_CS1/GPIO32"),
	PINCTRL_PIN(33, "AM24:JTCK1/SPI_SCLK1/GPIO33"),
	PINCTRL_PIN(34, "AL25:JTDO1/SPI_DO1/GPIO34"),
	PINCTRL_PIN(35, "AN23:JTDI1/SPI_DI1/GPIO35"),
	PINCTRL_PIN(36, "AN25:SPI_CS0/GPIO36"),
	PINCTRL_PIN(37, "AM25:SPI_SCLK0/GPIO37"),
	PINCTRL_PIN(38, "AL26:SPI_DO0/GPIO38"),
	PINCTRL_PIN(39, "AN26:JTRST1/SPI_DI0/GPIO39"),
	PINCTRL_PIN(45, "AM29:STPI0_VAL/GPIO45/S.PWM_IN8"),
	PINCTRL_PIN(46, "AM30:STPI0_SOP/GPIO46/S.PWM_IN7"),
	PINCTRL_PIN(47, "AL30:STPI0_CLK/GPIO47"),
	PINCTRL_PIN(48, "AJ34:HDMI2_2_SDA/GPIO48"),
	PINCTRL_PIN(49, "AJ33:HDMI2_2_SCL/GPIO49"),
	PINCTRL_PIN(50, "AF34:JTAG0_TDI/HDMI2_1_SDA/GPIO50"),
	PINCTRL_PIN(51, "AF33:JTAG0_TDO/HDMI2_1_SCL/GPIO51"),
	PINCTRL_PIN(52, "AD33:JTAG0_TCK/HDMI2_0_SDA/GPIO52"),
	PINCTRL_PIN(53, "AE33:JTAG0_TMS/HDMI2_0_SCL/GPIO53"),
	PINCTRL_PIN(56, "AE6:DIM1_MOSI/GPIO56"),
	PINCTRL_PIN(57, "AC6:DIM1_SCLK/GPIO57"),
	PINCTRL_PIN(58, "AF7:DIM0_MOSI/GPIO58"),
	PINCTRL_PIN(59, "AE7:DIM0_SCLK/GPIO59"),
	PINCTRL_PIN(60, "AD5:L_VSOUT_LD/GPIO60"),
	PINCTRL_PIN(61, "AB6:PWM2/GPIO61"),
	PINCTRL_PIN(62, "AB8:PWM1/GPIO62"),
	PINCTRL_PIN(63, "AA7:PWM_IN/GPIO63"),
	PINCTRL_PIN(68, "AC2:EPI_MCLK/GPIO68"),
	PINCTRL_PIN(72, "AM22:SDA5/GPIO72"),
	PINCTRL_PIN(73, "AN22:SCL5/GPIO73"),
	PINCTRL_PIN(74, "AM21:SDA4/GPIO74"),
	PINCTRL_PIN(75, "AL21:SCL4/GPIO75"),
	PINCTRL_PIN(76, "AL22:SDA3/GPIO76"),
	PINCTRL_PIN(77, "AL23:SCL3/GPIO77"),
	PINCTRL_PIN(78, "AN20:SDA2/GPIO78"),
	PINCTRL_PIN(79, "AM20:SCL2/GPIO79"),
	PINCTRL_PIN(80, "AD6:GPIO80/FRC_LRSYNC"),
	PINCTRL_PIN(81, "AK5:DACSCK/GPIO81"),
	PINCTRL_PIN(82, "AJ8:AUDCLK_OUT/GPIO82"),
	PINCTRL_PIN(83, "AJ7:DACCLFCH/GPIO83"),
	PINCTRL_PIN(84, "AK6:DACSLRCH/GPIO84"),
	PINCTRL_PIN(85, "AF6:PCMI3SCK/GPIO85"),
	PINCTRL_PIN(86, "AG5:PCMI3LRCK/GPIO86"),
	PINCTRL_PIN(87, "AF5:PCMI3LRCH/GPIO87"),
	PINCTRL_PIN(88, "T37:GPIO88/EB_WAIT"),
	PINCTRL_PIN(89, "N35:GPIO89/EB_ADDR15"),
	PINCTRL_PIN(90, "W36:GPIO90/EB_CS0"),
	PINCTRL_PIN(91, "W37:GPIO91/EB_CS1"),
	PINCTRL_PIN(92, "V36:GPIO92/EB_CS2"),
	PINCTRL_PIN(93, "V37:GPIO93/EB_CS3"),
	PINCTRL_PIN(94, "AM33:GPIO94"),
	PINCTRL_PIN(95, "AM34:GPIO95"),
	PINCTRL_PIN(96, "AN29:STPI0_DATA0/GPIO96/S.PWM_IN9"),
	PINCTRL_PIN(97, "AM28:STPI0_DATA1/GPIO97"),
	PINCTRL_PIN(98, "AN28:STPI0_DATA2/GPIO98"),
	PINCTRL_PIN(99, "AL29:STPI0_DATA3/GPIO99"),
	PINCTRL_PIN(100, "AL28:STPI0_DATA4/GPIO100"),
	PINCTRL_PIN(101, "AL27:STPI0_DATA5/GPIO101"),
	PINCTRL_PIN(102, "AM27:STPI0_DATA6/GPIO102"),
	PINCTRL_PIN(103, "AM26:STPI0_DATA7/GPIO103"),
	PINCTRL_PIN(105, "AL12:UART0_TXD/GPIO105"),
	PINCTRL_PIN(106, "AM12:UART0_RXD/GPIO106"),
	PINCTRL_PIN(107, "AM11:GPIO107/UART1_RTS"),
	PINCTRL_PIN(108, "AN11:GPIO108/UART1_CTS"),
	PINCTRL_PIN(109, "AL14:UART1_TXD/GPIO109"),
	PINCTRL_PIN(110, "AL13:UART1_RXD/GPIO110"),
	PINCTRL_PIN(112, "N31:EB_ADDR0/GPIO112"),
	PINCTRL_PIN(113, "P31:EB_ADDR1/GPIO113"),
	PINCTRL_PIN(114, "P33:EB_ADDR2/GPIO114"),
	PINCTRL_PIN(115, "P32:EB_ADDR3/GPIO115"),
	PINCTRL_PIN(116, "R31:EB_ADDR4/GPIO116"),
	PINCTRL_PIN(117, "R32:EB_ADDR5/GPIO117"),
	PINCTRL_PIN(118, "T32:EB_ADDR6/GPIO118"),
	PINCTRL_PIN(119, "K35:EB_ADDR7/GPIO119"),
	PINCTRL_PIN(120, "N37:EB_DATA0/GPIO120"),
	PINCTRL_PIN(121, "N36:EB_DATA1/GPIO121"),
	PINCTRL_PIN(122, "P35:EB_DATA2/GPIO122"),
	PINCTRL_PIN(123, "P36:EB_DATA3/GPIO123"),
	PINCTRL_PIN(124, "R35:EB_DATA4/GPIO124"),
	PINCTRL_PIN(125, "R36:EB_DATA5/GPIO125"),
	PINCTRL_PIN(126, "R37:EB_DATA6/GPIO126"),
	PINCTRL_PIN(127, "T35:EB_DATA7/GPIO127"),
	PINCTRL_PIN(130, "J32:SD_DATA2/GPIO130"),
	PINCTRL_PIN(131, "K32:SD_DATA3/GPIO131"),
	PINCTRL_PIN(132, "H31:TRST0/SD_DATA0/SC_DATA/GPIO132"),
	PINCTRL_PIN(133, "H33:TDI0/SD_CMD/SC_RST/GPIO133"),
	PINCTRL_PIN(134, "H32:TMS0/SD_WP_N/SC_VCC_SEL/GPIO134"),
	PINCTRL_PIN(135, "G32:TDO0/SD_CD_N/SC_VCCEN/GPIO135"),
	PINCTRL_PIN(136, "AN19:SDA1/GPIO136"),
	PINCTRL_PIN(137, "AM19:SCL1/GPIO137"),
	PINCTRL_PIN(138, "AL19:SDA0/GPIO138"),
	PINCTRL_PIN(139, "AL20:SCL0/GPIO139"),
};

static struct function_desc const lg1211_func_desc[] = {
	{
		.name = { "audio", },
		.pin = { 81, 82, 83, 84, 85, 86, 87, },
		.npins = 7,
	}, {
		.name = { "audio-earc", },
		.pin = { 21, },
		.npins = 1,
	}, {
		.name = { "epi", },
		.pin = { 68, },
		.npins = 1,
	}, {
		.name = { "ext_bus", },
		.pin = { 48, 49, 50, 51, 52, 53, },
		.npins = 6,
	}, {
		.name = { "frc", },
		.pin = { 80, },
		.npins = 1,
	}, {
		.name = { "hdmi2", },
		.pin = { 88, 89, 90, 91, 92, 93, 112, 113, 114, 115, 116, 117,
		         118, 119, 120, 121, 122, 123, 124, 125, 126, 127, },
		.npins = 22,
	}, {
		.name = { "i2c0", },
		.pin = { 138, 139, },
		.npins = 2,
	}, {
		.name = { "i2c1", },
		.pin = { 136, 137, },
		.npins = 2,
	}, {
		.name = { "i2c2", },
		.pin = { 78, 79, },
		.npins = 2,
	}, {
		.name = { "i2c3", },
		.pin = { 76, 77, },
		.npins = 2,
	}, {
		.name = { "i2c4", },
		.pin = { 74, 75, },
		.npins = 2,
	}, {
		.name = { "i2c5", },
		.pin = { 72, 73, },
		.npins = 2,
	}, {
		.name = { "oled", },
		.pin = { 56, 57, 58, 59, 60, },
		.npins = 5,
	}, {
		.name = { "pwm", },
		.pin = { 30, 45, 46, 61, 62, 63, 96, },
		.npins = 7,
	}, {
		.name = { "sdio", },
		.pin = { 130, 131, 132, 133, 134, 135, },
		.npins = 6,
	}, {
		.name = { "spi0", },
		.pin = { 36, 37, 38, 39, },
		.npins = 4,
	}, {
		.name = { "spi1", },
		.pin = { 32, 33, 34, 35, },
		.npins = 4,
	}, {
		.name = { "stpi0", },
		.pin = { 45, 46, 47, 96, 97, 98, 99, 100, 101, 102, 103, },
		.npins = 11,
	}, {
		.name = { "uart0", },
		.pin = { 105, 106, },
		.npins = 2,
	}, {
		.name = { "uart1", },
		.pin = { 107, 108, 109, 110, },
		.npins = 4,
	},
};

static int lg1211_pinctrl_get_groups_count(struct pinctrl_dev *pctldev)
{
	return ARRAY_SIZE(lg1211_func_desc);
}

static char const *lg1211_pinctrl_get_group_name(struct pinctrl_dev *pctldev,
                                                 unsigned selector)
{
	return lg1211_func_desc[selector].name[0];
}

static struct pinctrl_ops lg1211_pinctrl_ops = {
	.get_groups_count = lg1211_pinctrl_get_groups_count,
	.get_group_name = lg1211_pinctrl_get_group_name,
};

static int lg1211_pinmux_get_functions_count(struct pinctrl_dev *pctldev)
{
	return ARRAY_SIZE(lg1211_func_desc);
}

static char const *lg1211_pinmux_get_function_name(struct pinctrl_dev *pctldev,
                                                   unsigned selector)
{
	return lg1211_func_desc[selector].name[0];
}

static int lg1211_pinmux_get_function_groups(struct pinctrl_dev *pctldev,
                                             unsigned selector,
                                             char const *const **groups,
                                             unsigned *num_groups)
{
	*groups = (char const *const *)lg1211_func_desc[selector].name;
	*num_groups = 1;

	return 0;
}

static int lg1211_pinmux_set_mux(struct pinctrl_dev *pctldev,
                                 unsigned func_selector,
                                 unsigned group_selector)
{
	return 0;
}

static int lg1211_pinmux_gpio_request_enable(struct pinctrl_dev *pctldev,
                                             struct pinctrl_gpio_range *range,
                                             unsigned offset)
{
	struct pinmux_info const *info;
	u32 v;
	int i;

	if (range->pins) {
		for (i = 0; i < range->npins; i++)
			if (offset == range->pins[i])
				break;
	} else {
		for (i = 0; i < range->npins; i++)
			if (offset == range->pin_base + i)
				break;
	}

	if (i == range->npins)
		return 0;

	for (i = 0; i < ARRAY_SIZE(lg1211_pinmux_info); i++) {
		info = &lg1211_pinmux_info[i];
		if (offset >= info->pin && offset < info->pin + info->pins) {
			v = readl_relaxed(info->va);
			v &= ~info->mask;
			v |= info->enable;
			writel_relaxed(v, info->va);
		}
	}

	return 0;
}

static void lg1211_pinmux_gpio_disable_free(struct pinctrl_dev *pctldev,
                                            struct pinctrl_gpio_range *range,
                                            unsigned offset)
{
}

static struct pinmux_ops lg1211_pinmux_ops = {
	.get_functions_count = lg1211_pinmux_get_functions_count,
	.get_function_name = lg1211_pinmux_get_function_name,
	.get_function_groups = lg1211_pinmux_get_function_groups,
	.set_mux = lg1211_pinmux_set_mux,
	.gpio_request_enable = lg1211_pinmux_gpio_request_enable,
	.gpio_disable_free = lg1211_pinmux_gpio_disable_free,
};

static int lg1211_pinctrl_suspend(struct device *dev)
{
	struct pinmux_info *info;
	int i;

	for (i = 0; i < ARRAY_SIZE(lg1211_pinmux_info); i++) {
		info = &lg1211_pinmux_info[i];
		if (info->savable)
			info->save = readl_relaxed(info->va);
	}

	return 0;
}

static int lg1211_pinctrl_resume(struct device *dev)
{
	struct pinmux_info *info;
	int i;

	for (i = 0; i < ARRAY_SIZE(lg1211_pinmux_info); i++) {
		info = &lg1211_pinmux_info[i];
		if (info->savable)
			writel_relaxed(info->save, info->va);
	}

	return 0;
}

static struct dev_pm_ops lg1211_pinctrl_pm_ops = {
	SET_LATE_SYSTEM_SLEEP_PM_OPS(lg1211_pinctrl_suspend,
	                             lg1211_pinctrl_resume)
};

static struct pinctrl_desc lg1211_pinctrl_desc = {
	.name = "lg1211-pinctrl",
	.pins = lg1211_pin_desc,
	.npins = ARRAY_SIZE(lg1211_pin_desc),
	.pctlops = &lg1211_pinctrl_ops,
	.pmxops = &lg1211_pinmux_ops,
	.owner = THIS_MODULE,
};

#define PAGE_MAPS	8

static void lg1211_pinctrl_iomap(struct platform_device *pdev)
{
	struct pinmux_info *info;
	phys_addr_t ppa[PAGE_MAPS];
	void __iomem *pva[PAGE_MAPS];
	size_t maps = 0;
	int i, j;

	for (i = 0; i < ARRAY_SIZE(lg1211_pinmux_info); i++) {
		info = &lg1211_pinmux_info[i];

		for (j = 0; j < maps; j++) {
			if ((info->pa & PAGE_MASK) == (ppa[j] & PAGE_MASK))
				break;
		}

		if (j == maps) {
			ppa[maps] = info->pa & PAGE_MASK;
			pva[maps] = ioremap(ppa[maps], PAGE_SIZE);
			maps++;
		}

		info->va = pva[j] + (info->pa & ~PAGE_MASK);

		dev_dbg(&pdev->dev, "PA: %x, VA: %p\n", info->pa, info->va);
	}
}

static int lg1211_pinctrl_probe(struct platform_device *pdev)
{
	struct pinctrl_dev *pctldev;

	lg1211_pinctrl_iomap(pdev);

	pctldev = pinctrl_register(&lg1211_pinctrl_desc, &pdev->dev, NULL);
	if (IS_ERR(pctldev))
		return PTR_ERR(pctldev);

	dev_info(&pdev->dev, "LG1211 pinctrl driver\n");

	return 0;
}

static struct of_device_id const lg1211_pinctrl_ids[] = {
	{ .compatible = "lge,lg1211-pinctrl", },
	{ /* sentinel */ }
};

static struct platform_driver lg1211_pinctrl_driver = {
	.driver = {
		.name = "lg1211-pinctrl",
		.of_match_table = lg1211_pinctrl_ids,
		.pm = &lg1211_pinctrl_pm_ops,
	},
	.probe = lg1211_pinctrl_probe,
};

static int __init lg1211_pinctrl_init(void)
{
	return platform_driver_register(&lg1211_pinctrl_driver);
}
arch_initcall(lg1211_pinctrl_init);
