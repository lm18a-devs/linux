#include <linux/kernel.h>

#include <linux/delay.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/of.h>
#include <linux/phy.h>

static int omniphy_fixup_run(struct phy_device *phydev)
{
	int v;

	/* enable access to Analog and DSP register bank */
	phy_write(phydev, 0x14, 0x0400);
	phy_write(phydev, 0x14, 0x0000);
	phy_write(phydev, 0x14, 0x0400);

	/* apply some Tx-tuning values */
	if (of_machine_is_compatible("lge,lg1211") ||
	    of_machine_is_compatible("lge,lg1313c0")) {
		/* start ZQ calibration */
		phy_write(phydev, 0x17, 0x0001);
		phy_write(phydev, 0x14, 0x7800);
		/* wait for zq_cal_done */
		do {
			phy_write(phydev, 0x14, 0xb820);
			v = phy_read(phydev, 0x15);
		} while (!(v & 0x0001));
		/* adjust 100BT amplitude */
		phy_write(phydev, 0x17, 0xe000);
		phy_write(phydev, 0x14, 0x441c);
		/* adjust 10BT amplitude */
		phy_write(phydev, 0x17, 0x002f);
		phy_write(phydev, 0x14, 0x4418);
	} else if (of_machine_is_compatible("lge,lg1313")) {
		phy_write(phydev, 0x17, 0x804e);
		phy_write(phydev, 0x14, 0x4416);
		phy_write(phydev, 0x17, 0x3c00);
		phy_write(phydev, 0x14, 0x4417);
		phy_write(phydev, 0x17, 0x0036);
		phy_write(phydev, 0x14, 0x4418);
		phy_write(phydev, 0x17, 0x1000);
		phy_write(phydev, 0x14, 0x441c);
	}

	dev_info(&phydev->dev, "Tx-tuning applied\n");

	return 0;
}

static int rtl8201_fixup_run(struct phy_device *phydev)
{
	/* adjust RMII mode setting */
	mdiobus_write(phydev->bus, phydev->addr, 0x1f, 0x0007);
	mdiobus_write(phydev->bus, phydev->addr, 0x10, 0x077a);

	/* return to page 0 */
	mdiobus_write(phydev->bus, phydev->addr, 0x1f, 0x0000);

	return 0;
}

static int omniphy_lg1211_reset(struct phy_device *phydev, int enable)
{
	static void __iomem *base;
	u32 v;

	if (!base)
		base = ioremap(0xc7501000, 0x1000);

	v = readl_relaxed(base + 0x0004);

	if (enable)
		v |= 0x00000101;
	else
		v &= 0xfffffefe;

	writel_relaxed(v, base + 0x0004);

	return 0;
}

static int omniphy_lg1313_reset(struct phy_device *phydev, int enable)
{
	static void __iomem *base;
	u32 v;

	if (!base)
		base = ioremap(0xc3501000, 0x1000);

	v = readl_relaxed(base + 0x0004);

	if (enable)
		v |= 0x00000404;
	else
		v &= 0xfffffbfb;

	writel_relaxed(v, base + 0x0004);

	return 0;
}

int omniphy_arch_reset(struct phy_device *phydev, int enable)
{
	static int (*arch_reset_func)(struct phy_device *, int);

	if (enable) {
		if (phydev)
			dev_info(&phydev->dev, "hard-reset by CTOP\n");
		else
			pr_info("hard-reset by CTOP\n");
	}

	if (!arch_reset_func) {
		if (of_machine_is_compatible("lge,lg1211"))
			arch_reset_func = omniphy_lg1211_reset;
		if (of_machine_is_compatible("lge,lg1313"))
			arch_reset_func = omniphy_lg1313_reset;
		if (of_machine_is_compatible("lge,lg1313c0"))
			arch_reset_func = omniphy_lg1313_reset;
	}

	return arch_reset_func ? arch_reset_func(phydev, enable) : -ENOENT;
}
EXPORT_SYMBOL(omniphy_arch_reset);

static int lg1k_init_eth(void)
{
	phy_register_fixup_for_uid(0x01814570, 0xfffffff0, omniphy_fixup_run);
	phy_register_fixup_for_uid(0x001cc816, 0xfffffff0, rtl8201_fixup_run);
	return 0;
}
#ifndef	CONFIG_USER_INITCALL_NET
device_initcall(lg1k_init_eth);
#else	/* CONFIG_USER_INITCALL_NET */
user_initcall_grp("NET", lg1k_init_eth);
#endif	/* CONFIG_USER_INITCALL_NET */
